-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 18, 2019 at 02:31 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `boilerplate`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `a_id` int(11) NOT NULL,
  `a_clinic_id` int(11) NOT NULL,
  `a_name` varchar(150) NOT NULL COMMENT 'Name',
  `a_description` text NOT NULL COMMENT 'Description',
  `a_added` datetime NOT NULL,
  `a_updated` datetime NOT NULL,
  `a_created_by` int(11) NOT NULL,
  `a_updated_by` int(11) NOT NULL,
  `a_is_deleted` char(1) NOT NULL,
  `a_status` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `admin_users`
--

CREATE TABLE `admin_users` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `pwd` varchar(100) NOT NULL,
  `full_name` varchar(100) DEFAULT NULL,
  `comp_name` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `phone` varchar(20) NOT NULL,
  `status` enum('Enable','Disable') NOT NULL DEFAULT 'Enable',
  `date_created` datetime NOT NULL,
  `last_modified` datetime DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `ip` varchar(25) DEFAULT NULL,
  `user_agent` text,
  `permissions` text NOT NULL,
  `user_role_id` int(11) NOT NULL,
  `activation_key` varchar(255) NOT NULL,
  `is_deleted` char(1) NOT NULL,
  `clinic_owner` varchar(120) NOT NULL,
  `clinic_id` int(11) NOT NULL,
  `salary` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_users`
--

INSERT INTO `admin_users` (`id`, `user_name`, `pwd`, `full_name`, `comp_name`, `email`, `phone`, `status`, `date_created`, `last_modified`, `last_login`, `ip`, `user_agent`, `permissions`, `user_role_id`, `activation_key`, `is_deleted`, `clinic_owner`, `clinic_id`, `salary`) VALUES
(1, 'admin', '4297f44b13955235245b2497399d7a93', 'Talib Allauddin', NULL, 'talib_570@live.com', '', 'Enable', '2015-06-14 23:32:08', '2019-03-20 07:01:06', '2019-05-11 22:02:03', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '', 1, '', '0', '0', 0, 0),
(2, 'taliba', '96054cfefbccf8eec7a2257e3bccafb0', 'Talib Allauddin', NULL, 'taliballauddin@gmail.com', '', 'Enable', '2019-03-20 01:11:59', NULL, '2019-03-20 01:15:48', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '', 1, '', '0', '0', 0, 0),
(3, 'pilu', '39f8c6c6d2ee7187e43f4a16ce462dc9', 'pilus', NULL, 'pilu@live.com', '', 'Enable', '2019-03-20 07:00:52', '2019-04-07 11:16:58', '2019-03-26 04:31:39', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '', 2, '', '0', '1', 1, 15000),
(4, 'alhaaj', '4297f44b13955235245b2497399d7a93', 'alhaaj', NULL, 'alhaaj@live.com', '', 'Enable', '2019-04-08 14:17:38', NULL, '2019-04-08 14:18:01', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', '', 2, '', '0', '1', 1, 120),
(5, 'arr', '4297f44b13955235245b2497399d7a93', 'Abdur Rafay', NULL, 'rafay@Live.com', '', 'Enable', '2019-04-11 11:04:47', NULL, NULL, NULL, NULL, '', 2, '', '0', '1', 0, 12000),
(6, 'rafay', '4297f44b13955235245b2497399d7a93', 'Abdur Rafay', NULL, 'm.abdurrafay03@gmail.com', '', 'Enable', '2019-04-18 15:14:44', '2019-04-19 12:53:32', '2019-05-18 17:23:08', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.157 Safari/537.36', '', 2, '', '0', '1', 0, 50000),
(7, 'anwar', '4297f44b13955235245b2497399d7a93', 'Anwar Ul Qamar', NULL, 'Qamar@gmail.com', '', 'Enable', '2019-04-18 16:51:32', '2019-04-18 17:02:35', '2019-04-19 12:19:28', '192.168.0.118', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', '', 2, '', '0', '1', 2, 100000),
(8, 'daim', '4297f44b13955235245b2497399d7a93', 'Daim gafoor', NULL, 'Daim@gmail.com', '', 'Enable', '2019-04-18 17:15:23', NULL, NULL, NULL, NULL, '', 2, '', '0', '0', 2, 16000),
(9, 'jabar', '4297f44b13955235245b2497399d7a93', 'Jabar ', NULL, 'jazsoftpakistan@gmail.com', '', 'Enable', '2019-04-18 17:47:13', NULL, '2019-04-19 12:23:59', '192.168.0.118', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', '', 2, '', '0', '0', 3, 100000),
(10, 'jamal', '4297f44b13955235245b2497399d7a93', 'Jamal', NULL, 'jamal@live.com', '', 'Enable', '2019-04-19 10:36:10', NULL, '2019-04-19 10:36:20', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', '', 4, '', '0', '0', 1, 50000),
(11, 'najeeb', '4297f44b13955235245b2497399d7a93', 'Najeeb', NULL, 'Najeeb@live.com', '', 'Enable', '2019-05-03 12:26:24', NULL, NULL, NULL, NULL, '', 4, '', '0', '0', 1, 50000),
(12, 'marif', '4297f44b13955235245b2497399d7a93', 'Marif Khan', NULL, 'marifkhan@gmail.com', '', 'Enable', '2019-05-18 13:18:10', NULL, '2019-05-18 13:39:41', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.157 Safari/537.36', '', 4, '', '0', '0', 1, 150000);

-- --------------------------------------------------------

--
-- Table structure for table `appointments`
--

CREATE TABLE `appointments` (
  `appointment_id` int(11) NOT NULL,
  `appointment_patient_id` int(11) NOT NULL,
  `appointment_dtr_id` int(11) NOT NULL,
  `appointment_start_time` time NOT NULL,
  `appointment_end_time` time NOT NULL,
  `appointment_date` date NOT NULL,
  `appointment_procedure_id` int(11) NOT NULL,
  `appointment_comment` text NOT NULL,
  `appointment_send_confirmation` char(1) NOT NULL,
  `appointment_added` datetime NOT NULL,
  `appointment_updated` datetime NOT NULL,
  `appointment_created_by` int(11) NOT NULL,
  `appointment_modified_by` int(11) NOT NULL,
  `appointment_status` varchar(15) NOT NULL,
  `appointment_is_deleted` char(1) NOT NULL,
  `appointment_clinic_id` int(11) NOT NULL,
  `appointment_is_canceled` char(1) NOT NULL,
  `appointment_checked` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `appointments`
--

INSERT INTO `appointments` (`appointment_id`, `appointment_patient_id`, `appointment_dtr_id`, `appointment_start_time`, `appointment_end_time`, `appointment_date`, `appointment_procedure_id`, `appointment_comment`, `appointment_send_confirmation`, `appointment_added`, `appointment_updated`, `appointment_created_by`, `appointment_modified_by`, `appointment_status`, `appointment_is_deleted`, `appointment_clinic_id`, `appointment_is_canceled`, `appointment_checked`) VALUES
(1, 2, 1, '12:58:00', '12:28:00', '2019-04-18', 1, '', '0', '2019-04-18 15:58:48', '2019-04-19 09:13:01', 6, 6, 'Enable', '0', 1, '0', ''),
(2, 2, 2, '12:00:00', '12:30:00', '2019-04-18', 2, '', '0', '2019-04-18 16:00:27', '2019-04-18 16:00:27', 6, 6, 'Enable', '0', 1, '0', ''),
(3, 3, 4, '12:00:00', '12:30:00', '2019-04-18', 1, '', '0', '2019-04-18 16:00:55', '2019-04-18 16:00:55', 6, 6, 'Enable', '0', 1, '0', ''),
(4, 4, 3, '12:01:00', '12:31:00', '2019-04-18', 2, '', '0', '2019-04-18 16:01:36', '2019-04-18 16:01:36', 6, 6, 'Enable', '0', 1, '0', ''),
(5, 5, 1, '12:18:00', '12:48:00', '2019-04-18', 2, '', '0', '2019-04-18 16:19:03', '2019-04-18 16:19:03', 6, 6, 'Enable', '0', 1, '0', ''),
(6, 5, 4, '12:19:00', '12:49:00', '2019-04-18', 1, '', '0', '2019-04-18 16:19:31', '2019-04-19 09:07:39', 6, 6, 'Enable', '0', 1, '0', ''),
(7, 5, 7, '12:01:00', '12:31:00', '2019-04-30', 1, '', '0', '2019-04-18 18:01:51', '2019-04-18 18:01:51', 9, 9, 'Enable', '0', 3, '0', ''),
(8, 17, 8, '12:01:00', '12:31:00', '2019-04-30', 2, '', '0', '2019-04-18 18:02:19', '2019-04-18 18:02:19', 9, 9, 'Enable', '0', 3, '0', ''),
(9, 13, 8, '12:03:00', '12:33:00', '2019-04-26', 2, '', '0', '2019-04-18 18:04:11', '2019-04-18 18:04:11', 9, 9, 'Enable', '0', 3, '0', ''),
(10, 5, 8, '12:04:00', '12:34:00', '2019-05-15', 2, '', '0', '2019-04-18 18:04:51', '2019-04-18 18:04:51', 9, 9, 'Enable', '0', 3, '0', ''),
(11, 1, 1, '09:22:00', '10:02:00', '0000-00-00', 1, '', '0', '2019-04-19 09:08:42', '2019-04-19 09:24:38', 6, 6, 'Enable', '0', 1, '0', ''),
(12, 1, 3, '09:31:00', '09:31:00', '2019-04-19', 1, '', '0', '2019-04-19 09:25:04', '2019-04-19 09:31:33', 6, 6, 'Enable', '0', 1, '1', ''),
(13, 2, 10, '12:33:00', '12:03:00', '2019-04-19', 1, '', '0', '2019-04-19 12:33:37', '2019-04-19 12:33:37', 9, 9, 'Enable', '0', 1, '0', ''),
(14, 4, 10, '12:35:00', '12:05:00', '2019-04-19', 2, '', '0', '2019-04-19 12:35:33', '2019-04-19 12:35:33', 9, 9, 'Enable', '0', 1, '0', ''),
(15, 7, 10, '12:08:00', '12:38:00', '2019-04-19', 1, '', '0', '2019-04-19 13:08:46', '2019-04-19 13:08:46', 9, 9, 'Enable', '0', 1, '0', ''),
(16, 2, 10, '12:28:00', '12:58:00', '2019-04-22', 1, '<p>asdf</p>\r\n', '0', '2019-04-22 17:28:28', '2019-04-22 17:28:28', 6, 6, 'Enable', '0', 1, '1', ''),
(17, 2, 10, '12:28:00', '12:58:00', '2019-04-22', 1, '<p>asdf</p>\r\n', '0', '2019-04-22 17:28:38', '2019-04-22 17:28:38', 6, 6, 'Enable', '0', 1, '1', ''),
(18, 2, 10, '12:29:00', '12:59:00', '2019-04-22', 1, '<p>asdf</p>\r\n', '0', '2019-04-22 17:29:15', '2019-04-22 17:29:15', 6, 6, 'Enable', '0', 1, '0', ''),
(19, 2, 10, '12:29:00', '12:59:00', '2019-04-22', 1, '<p>asdf</p>\r\n', '0', '2019-04-22 18:00:37', '2019-04-22 18:00:37', 6, 6, 'Enable', '0', 1, '0', ''),
(20, 2, 10, '12:05:00', '12:35:00', '2019-04-22', 1, '', '0', '2019-04-22 20:05:52', '2019-04-22 20:05:52', 6, 6, 'Enable', '0', 1, '0', ''),
(21, 5, 10, '12:06:00', '12:36:00', '2019-04-22', 1, '', '0', '2019-04-22 20:06:42', '2019-04-22 20:06:42', 6, 6, 'Enable', '0', 1, '0', ''),
(22, 2, 10, '12:07:00', '12:37:00', '2019-04-22', 1, '', '0', '2019-04-22 20:07:16', '2019-04-22 20:07:16', 6, 6, 'Enable', '0', 1, '0', ''),
(23, 2, 10, '12:07:00', '12:37:00', '2019-04-22', 1, '', '0', '2019-04-22 20:07:36', '2019-04-22 20:07:36', 6, 6, 'Enable', '0', 1, '0', ''),
(24, 2, 10, '12:08:00', '12:38:00', '2019-04-22', 1, '', '0', '2019-04-22 20:08:43', '2019-04-22 20:08:43', 6, 6, 'Enable', '0', 1, '0', ''),
(25, 2, 10, '12:08:00', '12:38:00', '2019-04-22', 1, '', '0', '2019-04-22 20:11:09', '2019-04-22 20:11:09', 6, 6, 'Enable', '0', 1, '0', ''),
(26, 2, 10, '12:08:00', '12:38:00', '2019-04-22', 1, '', '0', '2019-04-22 20:11:49', '2019-04-22 20:11:49', 6, 6, 'Enable', '0', 1, '0', ''),
(27, 2, 10, '12:08:00', '12:38:00', '2019-04-22', 1, '', '0', '2019-04-22 20:12:01', '2019-04-22 20:12:01', 6, 6, 'Enable', '0', 1, '0', ''),
(28, 6, 10, '12:15:00', '12:45:00', '2019-04-22', 1, '', '0', '2019-04-22 20:15:44', '2019-04-22 20:15:44', 6, 6, 'Enable', '0', 1, '0', ''),
(29, 2, 10, '12:17:00', '12:47:00', '2019-04-22', 1, '', '0', '2019-04-22 20:17:42', '2019-04-22 20:17:42', 6, 6, 'Enable', '0', 1, '0', ''),
(30, 2, 10, '10:24:00', '10:54:00', '2019-04-23', 1, '', '0', '2019-04-23 10:24:50', '2019-04-23 10:24:50', 6, 6, 'Enable', '0', 1, '0', ''),
(31, 2, 10, '10:26:00', '10:56:00', '2019-04-23', 1, '', '0', '2019-04-23 10:26:22', '2019-04-23 10:26:22', 6, 6, 'Enable', '0', 1, '0', ''),
(32, 5, 10, '10:34:00', '11:04:00', '2019-04-23', 1, '', '0', '2019-04-23 10:34:34', '2019-04-23 10:34:34', 6, 6, 'Enable', '0', 1, '0', ''),
(33, 7, 10, '10:46:00', '11:16:00', '2019-04-23', 1, '', '0', '2019-04-23 10:46:37', '2019-04-23 10:46:37', 6, 6, 'Enable', '0', 1, '0', ''),
(34, 7, 10, '10:46:00', '11:16:00', '2019-04-23', 1, '', '0', '2019-04-23 10:46:48', '2019-04-23 10:46:48', 6, 6, 'Enable', '0', 1, '0', ''),
(35, 7, 10, '10:46:00', '11:16:00', '2019-04-23', 1, '', '0', '2019-04-23 10:47:08', '2019-04-23 10:47:08', 6, 6, 'Enable', '0', 1, '0', ''),
(36, 19, 10, '10:48:00', '11:18:00', '2019-04-23', 1, '', '', '2019-04-23 10:48:38', '2019-04-23 10:48:38', 6, 6, 'Enable', '0', 1, '0', ''),
(37, 4, 10, '10:49:00', '11:19:00', '2019-04-23', 1, '', '0', '2019-04-23 10:49:50', '2019-04-23 10:49:50', 6, 6, 'Enable', '0', 1, '0', ''),
(38, 4, 10, '10:49:00', '11:19:00', '2019-04-23', 1, '', '0', '2019-04-23 10:50:43', '2019-04-23 10:50:43', 6, 6, 'Enable', '0', 1, '0', ''),
(39, 5, 10, '10:51:00', '11:21:00', '2019-04-23', 1, '', '0', '2019-04-23 10:51:30', '2019-04-23 10:51:30', 6, 6, 'Enable', '0', 1, '0', ''),
(40, 5, 10, '10:51:00', '11:21:00', '2019-04-23', 1, '', '0', '2019-04-23 10:52:11', '2019-04-23 10:52:11', 6, 6, 'Enable', '0', 1, '0', ''),
(41, 4, 10, '11:10:00', '11:40:00', '2019-04-24', 1, '', '0', '2019-04-23 11:10:40', '2019-04-23 11:10:40', 6, 6, 'Enable', '0', 1, '0', ''),
(42, 5, 10, '12:24:00', '12:54:00', '2019-04-25', 1, '', '', '2019-04-25 14:24:27', '2019-04-25 14:24:27', 6, 6, 'Enable', '0', 1, '0', ''),
(43, 2, 10, '12:15:00', '12:45:00', '2019-04-26', 1, '', '0', '2019-04-26 21:15:16', '2019-04-26 21:15:16', 6, 6, 'Enable', '0', 1, '0', ''),
(44, 2, 10, '12:35:00', '12:05:00', '2019-04-28', 1, '', '0', '2019-04-28 17:35:55', '2019-04-28 17:35:55', 6, 6, 'Enable', '0', 1, '0', ''),
(45, 2, 10, '02:55:00', '03:25:00', '2019-04-29', 1, '', '0', '2019-04-29 02:55:47', '2019-04-29 02:55:47', 6, 6, 'Enable', '0', 1, '0', ''),
(46, 6, 10, '12:39:00', '12:09:00', '2019-04-29', 1, '', '', '2019-04-29 18:40:28', '2019-04-29 18:40:28', 6, 6, 'Enable', '0', 1, '0', ''),
(47, 2, 10, '12:28:00', '12:58:00', '2019-04-30', 1, '', '0', '2019-04-30 20:28:40', '2019-04-30 20:28:40', 6, 6, 'Enable', '0', 1, '0', ''),
(48, 2, 10, '02:03:00', '02:33:00', '2019-05-01', 1, '', '0', '2019-05-01 02:03:35', '2019-05-01 02:03:35', 6, 6, 'Enable', '0', 1, '0', ''),
(49, 2, 10, '11:58:00', '12:28:00', '2019-05-03', 1, '', '0', '2019-05-03 11:58:45', '2019-05-03 11:58:45', 6, 6, 'Enable', '0', 1, '0', ''),
(50, 2, 10, '12:15:00', '12:45:00', '2019-05-03', 1, '', '0', '2019-05-03 12:15:59', '2019-05-03 12:15:59', 6, 6, 'Enable', '0', 1, '1', ''),
(51, 2, 10, '12:16:00', '12:46:00', '2019-05-03', 1, '', '0', '2019-05-03 12:16:50', '2019-05-03 12:16:50', 6, 6, 'Enable', '0', 1, '1', '0'),
(52, 2, 10, '12:23:00', '12:53:00', '2019-05-04', 1, '', '0', '2019-05-04 17:23:21', '2019-05-04 17:23:21', 6, 6, 'Enable', '0', 1, '0', '0'),
(53, 8, 11, '12:53:00', '12:23:00', '2019-05-15', 1, '<p>Test Comment</p>\r\n', '0', '2019-05-14 12:55:34', '2019-05-15 23:57:13', 6, 6, 'Enable', '0', 1, '0', '1'),
(54, 2, 10, '01:21:00', '01:21:00', '2019-02-15', 1, '', '0', '2019-05-14 12:56:09', '2019-05-14 13:21:54', 6, 6, 'Enable', '0', 1, '0', '0'),
(55, 4, 11, '12:15:00', '12:45:00', '2019-05-08', 1, '', '0', '2019-05-14 13:16:08', '2019-05-14 13:16:08', 6, 6, 'Enable', '0', 1, '0', '0'),
(56, 2, 10, '12:54:00', '12:24:00', '2019-05-14', 1, '', '0', '2019-05-14 20:54:37', '2019-05-14 20:54:37', 6, 6, 'Enable', '0', 1, '0', '0'),
(57, 7, 10, '12:54:00', '12:24:00', '2019-05-14', 1, '', '0', '2019-05-14 20:54:42', '2019-05-14 20:54:42', 6, 6, 'Enable', '0', 1, '0', '0'),
(58, 8, 11, '12:54:00', '12:24:00', '2019-05-18', 1, '', '0', '2019-05-14 20:54:47', '2019-05-18 15:18:07', 6, 6, 'Enable', '0', 1, '0', '0'),
(59, 2, 11, '04:41:00', '05:11:00', '2019-05-16', 1, '', '0', '2019-05-16 04:41:08', '2019-05-16 23:33:28', 6, 6, 'Enable', '0', 1, '0', ''),
(60, 2, 10, '04:41:00', '05:11:00', '2019-05-16', 1, '', '0', '2019-05-16 04:41:48', '2019-05-16 04:41:48', 6, 6, 'Enable', '1', 1, '0', '0'),
(61, 18, 10, '12:34:00', '01:04:00', '2019-05-16', 1, '', '0', '2019-05-16 23:34:15', '2019-05-16 23:34:15', 6, 6, 'Enable', '0', 1, '0', '0'),
(62, 25, 10, '12:21:00', '12:51:00', '2019-05-18', 2, '<p>i have pain</p>\r\n', '0', '2019-05-18 13:22:24', '2019-05-18 13:22:24', 6, 6, 'Enable', '0', 1, '0', '0');

-- --------------------------------------------------------

--
-- Table structure for table `bed`
--

CREATE TABLE `bed` (
  `b_id` int(11) NOT NULL,
  `b_number` varchar(120) NOT NULL,
  `b_bed_type_id` varchar(15) NOT NULL,
  `b_is_booked` char(1) NOT NULL,
  `b_status` varchar(15) NOT NULL,
  `b_created_by` int(11) NOT NULL,
  `b_updated_by` int(11) NOT NULL,
  `b_added` datetime NOT NULL,
  `b_updated` datetime NOT NULL,
  `b_is_deleted` char(1) NOT NULL,
  `b_clinic_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bed`
--

INSERT INTO `bed` (`b_id`, `b_number`, `b_bed_type_id`, `b_is_booked`, `b_status`, `b_created_by`, `b_updated_by`, `b_added`, `b_updated`, `b_is_deleted`, `b_clinic_id`) VALUES
(2, 'Bed # 1', '2', '1', 'Enabled', 6, 6, '2019-04-25 17:47:43', '2019-04-25 17:47:43', '0', 1),
(3, 'Bed # 2', '3', '0', 'Enabled', 6, 6, '2019-04-26 22:01:10', '2019-04-26 22:01:10', '0', 1),
(4, 'Bed # 3', '2', '0', 'Enabled', 6, 6, '2019-04-26 22:02:17', '2019-04-26 22:02:17', '0', 1),
(5, 'Bed # 4', '5', '1', 'Enabled', 6, 6, '2019-05-15 22:17:58', '2019-05-15 22:17:58', '0', 1),
(6, 'Bed # 5', 'Select', '0', 'Enabled', 6, 6, '2019-05-18 13:27:17', '2019-05-18 13:27:17', '0', 1),
(7, 'Bed # 5', '6', '0', 'Enabled', 6, 6, '2019-05-18 13:29:11', '2019-05-18 13:29:11', '0', 1);

-- --------------------------------------------------------

--
-- Table structure for table `bed_patient`
--

CREATE TABLE `bed_patient` (
  `bp_id` int(11) NOT NULL,
  `bp_bed_id` int(11) NOT NULL,
  `bp_patient_id` int(11) NOT NULL,
  `bp_alloted_by` int(11) NOT NULL,
  `bp_status` varchar(15) NOT NULL,
  `bp_created_by` int(11) NOT NULL,
  `bp_updated_by` int(11) NOT NULL,
  `bp_added` datetime NOT NULL,
  `bp_admission_date` datetime NOT NULL,
  `bp_discharge_date` datetime NOT NULL,
  `bp_updated` datetime NOT NULL,
  `bp_is_deleted` char(1) NOT NULL,
  `bp_clinic_id` int(11) NOT NULL,
  `bp_amount` int(11) NOT NULL,
  `bp_discount` int(11) NOT NULL,
  `bp_payment_status` varchar(15) NOT NULL,
  `bp_invoice_id` int(11) NOT NULL,
  `bp_discount_type` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bed_patient`
--

INSERT INTO `bed_patient` (`bp_id`, `bp_bed_id`, `bp_patient_id`, `bp_alloted_by`, `bp_status`, `bp_created_by`, `bp_updated_by`, `bp_added`, `bp_admission_date`, `bp_discharge_date`, `bp_updated`, `bp_is_deleted`, `bp_clinic_id`, `bp_amount`, `bp_discount`, `bp_payment_status`, `bp_invoice_id`, `bp_discount_type`) VALUES
(1, 2, 3, 3, 'Enabled', 6, 6, '2019-05-01 00:05:35', '2019-04-28 00:00:00', '2019-05-01 00:00:00', '2019-05-01 00:05:35', '0', 1, 1500, 200, 'paid', 1, '0'),
(2, 2, 3, 3, 'Enabled', 6, 6, '2019-05-01 00:09:51', '2019-04-28 00:00:00', '2019-05-01 00:00:00', '2019-05-01 00:09:51', '0', 1, 1500, 200, 'paid', 2, '0'),
(3, 2, 3, 3, 'Enabled', 6, 6, '2019-05-01 00:18:32', '2019-04-28 00:00:00', '2019-05-01 00:00:00', '2019-05-01 00:18:32', '0', 1, 1500, 200, 'paid', 3, '0'),
(4, 3, 2, 3, 'Enabled', 6, 6, '2019-05-01 02:04:48', '2019-04-28 00:00:00', '2019-05-01 00:00:00', '2019-05-01 02:04:48', '0', 1, 600, 20, 'paid', 10, '0'),
(5, 4, 2, 3, 'Enabled', 6, 6, '2019-05-01 02:21:12', '2019-04-28 00:00:00', '2019-05-01 00:00:00', '2019-05-01 02:21:12', '0', 1, 1500, 100, 'paid', 11, '0'),
(6, 2, 4, 0, 'Enabled', 6, 6, '2019-05-04 17:24:20', '2019-05-15 00:00:00', '2019-05-29 00:00:00', '2019-05-04 17:24:20', '0', 1, 7000, 0, 'paid', 13, '0'),
(7, 2, 5, 3, 'Enabled', 6, 6, '2019-05-10 18:32:50', '2019-04-29 00:00:00', '2019-05-03 00:00:00', '2019-05-10 18:32:50', '0', 1, 2000, 0, 'paid', 16, '0'),
(8, 3, 3, 0, 'Enabled', 6, 6, '2019-05-10 18:48:19', '2019-04-28 00:00:00', '2019-05-02 00:00:00', '2019-05-10 18:48:19', '0', 1, 800, 0, 'paid', 17, 'Rs.'),
(9, 2, 6, 4, 'Enabled', 6, 6, '2019-05-10 18:56:05', '2019-04-27 00:00:00', '2019-05-08 00:00:00', '2019-05-10 18:56:05', '0', 1, 5500, 10, 'paid', 18, '%'),
(10, 5, 25, 12, 'Enabled', 6, 6, '2019-05-18 13:26:53', '1970-01-01 05:00:00', '1970-01-01 05:00:00', '2019-05-18 13:26:53', '0', 1, 0, 0, 'unpaid', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `bed_type`
--

CREATE TABLE `bed_type` (
  `bt_id` int(11) NOT NULL,
  `bt_name` varchar(120) NOT NULL,
  `bt_per_day_charges` varchar(15) NOT NULL,
  `bt_billing_type` char(1) NOT NULL,
  `bt_description` text NOT NULL,
  `bt_status` varchar(15) NOT NULL,
  `bt_created_by` int(11) NOT NULL,
  `bt_updated_by` int(11) NOT NULL,
  `bt_added` datetime NOT NULL,
  `bt_updated` datetime NOT NULL,
  `bt_is_deleted` char(1) NOT NULL,
  `bt_clinic_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bed_type`
--

INSERT INTO `bed_type` (`bt_id`, `bt_name`, `bt_per_day_charges`, `bt_billing_type`, `bt_description`, `bt_status`, `bt_created_by`, `bt_updated_by`, `bt_added`, `bt_updated`, `bt_is_deleted`, `bt_clinic_id`) VALUES
(2, 'General', '500', '1', '&lt;p&gt;test&lt;/p&gt;\r\n', 'Enabled', 6, 6, '2019-04-25 17:45:23', '2019-04-25 17:45:23', '0', 1),
(3, 'VIP', '200', '1', '', 'Enabled', 6, 6, '2019-04-26 21:24:06', '2019-04-26 21:24:06', '0', 1),
(4, 'wer', '123', '0', '', 'Enabled', 6, 6, '2019-04-26 21:26:44', '2019-04-26 21:26:44', '0', 1),
(5, 'VIP', '3000', '1', '', 'Enabled', 6, 6, '2019-05-15 22:17:30', '2019-05-15 22:17:30', '0', 1),
(6, 'private', '3000', '0', '', 'Enabled', 6, 6, '2019-05-18 13:28:17', '2019-05-18 13:28:17', '0', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `user_data` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('01a5db9616c1b08d359f880d7e743054', '192.168.10.24', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 1557946638, 'a:13:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"rafay";s:15:"admin_full_name";s:11:"Abdur Rafay";s:8:"admin_id";s:1:"6";s:10:"last_login";s:19:"2019-05-15 21:47:39";s:7:"last_ip";s:3:"::1";s:12:"user_role_id";s:1:"2";s:9:"clinic_id";s:1:"1";s:10:"permission";s:3263:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"clinics":{"create":"false","read":"false","update":"false","delete":"false"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"appointments":{"create":"true","read":"true","update":"true","delete":"true"},"patients":{"create":"true","read":"true","update":"true","delete":"true","profile":"true"},"procedures":{"create":"true","read":"true","update":"true","delete":"true"},"labtracking":{"create":"true","read":"true","update":"true","delete":"true"},"reports":{"create":"false","read":"true","update":"false","delete":"false"},"treatments":{"create":"true","read":"true","update":"true","delete":"true"},"invoices":{"create":"true","read":"true","update":"true","delete":"true"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"true"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"true"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"worktype":{"create":"true","read":"true","update":"true","delete":"true"},"work":{"create":"true","read":"true","update":"true","delete":"true"},"suppliers":{"create":"true","read":"true","update":"true","delete":"true"},"lab":{"create":"true","read":"true","update":"true","delete":"true"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"patientfiles":{"create":"true","read":"true","update":"true","delete":"true"},"expenses":{"create":"true","read":"true","update":"true","delete":"true"},"purchases":{"create":"true","read":"true","update":"true","delete":"true"},"items":{"create":"true","read":"true","update":"true","delete":"true","issue":"true"},"stocks":{"create":"true","read":"true","update":"true","delete":"true"},"accounts":{"create":"true","read":"true","update":"true","delete":"true"},"purchasedetails":{"create":"true","read":"true","update":"true","delete":"true"},"stockreleases":{"create":"true","read":"true","update":"true","delete":"true"},"employeepayroll":{"create":"true","read":"true","update":"true","delete":"true"},"employeeattendance":{"create":"true","read":"true","update":"true","delete":"true"},"tokens":{"create":"true","read":"true","update":"true","delete":"true"},"backup":{"create":"false","read":"true","update":"false","delete":"false"},"reporttype":{"create":"true","read":"true","update":"true","delete":"true"},"":{"create":"false","read":"false","update":"false","delete":"false"},"roomtype":{"create":"true","read":"true","update":"true","delete":"true"},"room":{"create":"true","read":"true","update":"true","delete":"true"},"bedtype":{"create":"true","read":"true","update":"true","delete":"true"},"bed":{"create":"true","read":"true","update":"true","delete":"true"},"bedpatient":{"create":"true","read":"true","update":"true","delete":"true"},"roompatient":{"create":"true","read":"true","update":"true","delete":"true"},"doctordays":{"create":"true","read":"true","update":"true","delete":"true"},"patientprocedures":{"create":"true","read":"true","update":"true","delete":"true"},"doctors":{"create":"true","read":"true","update":"true","delete":"true"}}";s:14:"doctor_role_id";s:1:"4";s:10:"role_title";s:5:"Admin";}'),
('028881e2a3983f6f9218b273d6f002d6', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 1557450055, ''),
('031417422ffdacab35cd2a3da93780e8', '::1', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1547806415, 'a:10:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"admin";s:15:"admin_full_name";s:5:"5-STI";s:8:"admin_id";s:1:"1";s:10:"last_login";s:19:"2019-01-18 11:01:42";s:7:"last_ip";s:3:"::1";s:10:"permission";s:1942:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"customers":{"create":"true","read":"true","update":"true","delete":"true"},"orders":{"create":"true","read":"true","update":"true","delete":"true","history":"false","fullaccess":"false"},"payments":{"create":"false","read":"false","update":"false","delete":"false"},"invoice":{"create":"true","read":"true","update":"true","delete":"true"},"menu":{"create":"true","read":"true","update":"true","delete":"true"},"pages":{"create":"true","read":"true","update":"true","delete":"true"},"faqs":{"create":"true","read":"true","update":"true","delete":"true"},"about":{"create":"true","read":"true","update":"true","delete":"true"},"homecontent":{"create":"false","read":"false","update":"false","delete":"false"},"services":{"create":"true","read":"true","update":"true","delete":"true"},"gallery":{"create":"true","read":"true","update":"true","delete":"true"},"testimonial":{"create":"true","read":"true","update":"true","delete":"true"},"sliders":{"create":"true","read":"true","update":"true","delete":"true"},"slider":{"create":"true","read":"true","update":"true","delete":"true"},"products":{"create":"true","read":"true","update":"true","delete":"true"},"productcategory":{"create":"true","read":"true","update":"true","delete":"true"},"sales":{"create":"false","read":"true","update":"false","delete":"true","history":"true","fullaccess":"true"},"contact":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"payment":{"create":"true","read":"true","update":"true","delete":"true","history":"true","fullaccess":"true"},"shipment":{"create":"true","read":"true","update":"true","delete":"true"}}";s:10:"role_title";s:5:"Admin";}'),
('03a2dd2541e6e47416bda73970e4431e', '::1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36', 1461683889, 'a:10:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"admin";s:15:"admin_full_name";s:5:"5-STI";s:8:"admin_id";s:1:"1";s:10:"last_login";s:19:"2016-04-26 17:12:16";s:7:"last_ip";s:3:"::1";s:10:"permission";s:1785:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"customers":{"create":"true","read":"true","update":"true","delete":"true"},"orders":{"create":"true","read":"true","update":"true","delete":"true","history":"false","fullaccess":"false"},"payments":{"create":"false","read":"false","update":"false","delete":"false"},"invoice":{"create":"true","read":"true","update":"true","delete":"true"},"menu":{"create":"true","read":"true","update":"true","delete":"true"},"pages":{"create":"true","read":"true","update":"true","delete":"true"},"faqs":{"create":"true","read":"true","update":"true","delete":"true"},"about":{"create":"true","read":"true","update":"true","delete":"true"},"services":{"create":"true","read":"true","update":"true","delete":"true"},"gallery":{"create":"true","read":"true","update":"true","delete":"true"},"testimonial":{"create":"true","read":"true","update":"true","delete":"true"},"sliders":{"create":"true","read":"true","update":"true","delete":"true"},"slider":{"create":"true","read":"true","update":"true","delete":"true"},"products":{"create":"true","read":"true","update":"true","delete":"true"},"productcategory":{"create":"true","read":"true","update":"true","delete":"true"},"sales":{"create":"false","read":"true","update":"false","delete":"true","history":"true","fullaccess":"true"},"contact":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"payment":{"create":"true","read":"true","update":"true","delete":"true","history":"true","fullaccess":"true"}}";s:10:"role_title";s:5:"Admin";}'),
('03d465f33a1d4dc24ba8bebea9455744', '192.168.10.9', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36', 1460113059, ''),
('0493e922d1e2a1aa8eb38b29682299c6', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', 1554714943, ''),
('0533548d19480b1b7bda05cb089d0473', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 1555933962, 'a:13:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"admin";s:15:"admin_full_name";s:15:"Talib Allauddin";s:8:"admin_id";s:1:"1";s:10:"last_login";s:19:"2019-04-22 13:56:15";s:7:"last_ip";s:13:"192.168.0.104";s:12:"user_role_id";s:1:"1";s:9:"clinic_id";s:1:"0";s:10:"permission";s:2565:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"clinics":{"create":"true","read":"true","update":"true","delete":"true"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"appointments":{"create":"true","read":"true","update":"true","delete":"true"},"doctors":{"create":"true","read":"true","update":"true","delete":"true"},"patients":{"create":"true","read":"true","update":"true","delete":"true"},"procedures":{"create":"true","read":"true","update":"true","delete":"true"},"labtracking":{"create":"true","read":"true","update":"true","delete":"true"},"reports":{"create":"false","read":"true","update":"false","delete":"false"},"treatments":{"create":"true","read":"true","update":"true","delete":"true"},"invoices":{"create":"true","read":"true","update":"true","delete":"true"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"true"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"true"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"worktype":{"create":"true","read":"true","update":"true","delete":"true"},"work":{"create":"true","read":"true","update":"true","delete":"true"},"suppliers":{"create":"true","read":"true","update":"true","delete":"true"},"lab":{"create":"true","read":"true","update":"true","delete":"true"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"patientfiles":{"create":"true","read":"true","update":"true","delete":"true"},"expenses":{"create":"true","read":"true","update":"true","delete":"true"},"purchases":{"create":"true","read":"true","update":"true","delete":"true"},"items":{"create":"true","read":"true","update":"true","delete":"true","issue":"true"},"stocks":{"create":"true","read":"true","update":"true","delete":"true"},"accounts":{"create":"true","read":"true","update":"true","delete":"true"},"purchasedetails":{"create":"true","read":"true","update":"true","delete":"true"},"stockreleases":{"create":"true","read":"true","update":"true","delete":"true"},"employeepayroll":{"create":"true","read":"true","update":"true","delete":"true"},"employeeattendance":{"create":"true","read":"true","update":"true","delete":"true"},"tokens":{"create":"true","read":"true","update":"true","delete":"true"},"backup":{"create":"false","read":"true","update":"false","delete":"false"},"reporttype":{"create":"true","read":"true","update":"true","delete":"true"}}";s:14:"doctor_role_id";s:1:"4";s:10:"role_title";s:11:"Super Admin";}'),
('05b11a8d6ebe53a51fa1f28ef100775d', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.157 Safari/537.36', 1558182171, 'a:13:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"rafay";s:15:"admin_full_name";s:11:"Abdur Rafay";s:8:"admin_id";s:1:"6";s:10:"last_login";s:19:"2019-05-18 17:22:16";s:7:"last_ip";s:3:"::1";s:12:"user_role_id";s:1:"2";s:9:"clinic_id";s:1:"1";s:10:"permission";s:3597:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"clinics":{"create":"false","read":"false","update":"false","delete":"false"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"appointments":{"create":"true","read":"true","update":"true","delete":"true"},"patients":{"create":"true","read":"true","update":"true","delete":"true","profile":"true"},"procedures":{"create":"true","read":"true","update":"true","delete":"true"},"labtracking":{"create":"true","read":"true","update":"true","delete":"true"},"reports":{"create":"false","read":"true","update":"false","delete":"false"},"treatments":{"create":"true","read":"true","update":"true","delete":"true"},"invoices":{"create":"true","read":"true","update":"true","delete":"true"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"true"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"true"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"worktype":{"create":"true","read":"true","update":"true","delete":"true"},"work":{"create":"true","read":"true","update":"true","delete":"true"},"suppliers":{"create":"true","read":"true","update":"true","delete":"true"},"lab":{"create":"true","read":"true","update":"true","delete":"true"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"patientfiles":{"create":"true","read":"true","update":"true","delete":"true"},"expenses":{"create":"true","read":"true","update":"true","delete":"true"},"purchases":{"create":"true","read":"true","update":"true","delete":"true"},"items":{"create":"true","read":"true","update":"true","delete":"true","issue":"true"},"stocks":{"create":"true","read":"true","update":"true","delete":"true"},"accounts":{"create":"true","read":"true","update":"true","delete":"true"},"purchasedetails":{"create":"true","read":"true","update":"true","delete":"true"},"stockreleases":{"create":"true","read":"true","update":"true","delete":"true"},"employeepayroll":{"create":"true","read":"true","update":"true","delete":"true"},"employeeattendance":{"create":"true","read":"true","update":"true","delete":"true"},"tokens":{"create":"true","read":"true","update":"true","delete":"true"},"backup":{"create":"false","read":"true","update":"false","delete":"false"},"reporttype":{"create":"true","read":"true","update":"true","delete":"true"},"":{"create":"false","read":"false","update":"false","delete":"false"},"roomtype":{"create":"true","read":"true","update":"true","delete":"true"},"room":{"create":"true","read":"true","update":"true","delete":"true"},"bedtype":{"create":"true","read":"true","update":"true","delete":"true"},"bed":{"create":"true","read":"true","update":"true","delete":"true"},"bedpatient":{"create":"true","read":"true","update":"true","delete":"true"},"roompatient":{"create":"true","read":"true","update":"true","delete":"true"},"doctordays":{"create":"true","read":"true","update":"true","delete":"true"},"patientprocedures":{"create":"true","read":"true","update":"true","delete":"true"},"facilities":{"create":"true","read":"true","update":"true","delete":"true"},"patientcase":{"create":"true","read":"true","update":"true","delete":"true","details":"true"},"relations":{"create":"true","read":"true","update":"true","delete":"true"},"patientfacility":{"create":"false","read":"false","update":"false","delete":"false"},"doctors":{"create":"true","read":"true","update":"true","delete":"true"}}";s:14:"doctor_role_id";s:1:"4";s:10:"role_title";s:5:"Admin";}'),
('065729e8174ec31ac292e06897b229e8', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 1555508760, 'a:12:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"admin";s:15:"admin_full_name";s:15:"Talib Allauddin";s:8:"admin_id";s:1:"1";s:10:"last_login";s:19:"2019-04-17 13:24:33";s:7:"last_ip";s:3:"::1";s:12:"user_role_id";s:1:"1";s:9:"clinic_id";s:1:"0";s:10:"permission";s:2565:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"clinics":{"create":"true","read":"true","update":"true","delete":"true"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"appointments":{"create":"true","read":"true","update":"true","delete":"true"},"doctors":{"create":"true","read":"true","update":"true","delete":"true"},"patients":{"create":"true","read":"true","update":"true","delete":"true"},"procedures":{"create":"true","read":"true","update":"true","delete":"true"},"labtracking":{"create":"true","read":"true","update":"true","delete":"true"},"reports":{"create":"false","read":"true","update":"false","delete":"false"},"treatments":{"create":"true","read":"true","update":"true","delete":"true"},"invoices":{"create":"true","read":"true","update":"true","delete":"true"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"true"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"true"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"worktype":{"create":"true","read":"true","update":"true","delete":"true"},"work":{"create":"true","read":"true","update":"true","delete":"true"},"suppliers":{"create":"true","read":"true","update":"true","delete":"true"},"lab":{"create":"true","read":"true","update":"true","delete":"true"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"patientfiles":{"create":"true","read":"true","update":"true","delete":"true"},"expenses":{"create":"true","read":"true","update":"true","delete":"true"},"purchases":{"create":"true","read":"true","update":"true","delete":"true"},"items":{"create":"true","read":"true","update":"true","delete":"true","issue":"true"},"stocks":{"create":"true","read":"true","update":"true","delete":"true"},"accounts":{"create":"true","read":"true","update":"true","delete":"true"},"purchasedetails":{"create":"true","read":"true","update":"true","delete":"true"},"stockreleases":{"create":"true","read":"true","update":"true","delete":"true"},"employeepayroll":{"create":"true","read":"true","update":"true","delete":"true"},"employeeattendance":{"create":"true","read":"true","update":"true","delete":"true"},"tokens":{"create":"true","read":"true","update":"true","delete":"true"},"backup":{"create":"false","read":"true","update":"false","delete":"false"},"reporttype":{"create":"true","read":"true","update":"true","delete":"true"}}";s:10:"role_title";s:11:"Super Admin";}'),
('08aaa3bfae367b02ad5f3fc67de2c338', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 1555386346, 'a:12:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"admin";s:15:"admin_full_name";s:15:"Talib Allauddin";s:8:"admin_id";s:1:"1";s:10:"last_login";s:19:"2019-04-15 17:37:16";s:7:"last_ip";s:3:"::1";s:12:"user_role_id";s:1:"1";s:9:"clinic_id";s:1:"0";s:10:"permission";s:2412:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"clinics":{"create":"true","read":"true","update":"true","delete":"true"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"appointments":{"create":"true","read":"true","update":"true","delete":"true"},"doctors":{"create":"true","read":"true","update":"true","delete":"true"},"patients":{"create":"true","read":"true","update":"true","delete":"true"},"procedures":{"create":"true","read":"true","update":"true","delete":"true"},"labtracking":{"create":"true","read":"true","update":"true","delete":"true"},"reports":{"create":"false","read":"true","update":"false","delete":"false"},"treatments":{"create":"true","read":"true","update":"true","delete":"true"},"invoices":{"create":"true","read":"true","update":"true","delete":"true"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"true"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"true"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"worktype":{"create":"true","read":"true","update":"true","delete":"true"},"work":{"create":"true","read":"true","update":"true","delete":"true"},"suppliers":{"create":"true","read":"true","update":"true","delete":"true"},"lab":{"create":"true","read":"true","update":"true","delete":"true"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"patientfiles":{"create":"true","read":"true","update":"true","delete":"true"},"expenses":{"create":"true","read":"true","update":"true","delete":"true"},"purchases":{"create":"true","read":"true","update":"true","delete":"true"},"items":{"create":"true","read":"true","update":"true","delete":"true","issue":"true"},"stocks":{"create":"true","read":"true","update":"true","delete":"true"},"accounts":{"create":"true","read":"true","update":"true","delete":"true"},"purchasedetails":{"create":"true","read":"true","update":"true","delete":"true"},"stockreleases":{"create":"true","read":"true","update":"true","delete":"true"},"employeepayroll":{"create":"true","read":"true","update":"true","delete":"true"},"employeeattendance":{"create":"true","read":"true","update":"true","delete":"true"},"tokens":{"create":"true","read":"true","update":"true","delete":"true"}}";s:10:"role_title";s:11:"Super Admin";}'),
('0c51c23205a78a016c4f9658365d0cbf', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 1555340072, 'a:12:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"admin";s:15:"admin_full_name";s:15:"Talib Allauddin";s:8:"admin_id";s:1:"1";s:10:"last_login";s:19:"2019-04-15 12:09:50";s:7:"last_ip";s:3:"::1";s:12:"user_role_id";s:1:"1";s:9:"clinic_id";s:1:"0";s:10:"permission";s:2412:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"clinics":{"create":"true","read":"true","update":"true","delete":"true"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"appointments":{"create":"true","read":"true","update":"true","delete":"true"},"doctors":{"create":"true","read":"true","update":"true","delete":"true"},"patients":{"create":"true","read":"true","update":"true","delete":"true"},"procedures":{"create":"true","read":"true","update":"true","delete":"true"},"labtracking":{"create":"true","read":"true","update":"true","delete":"true"},"reports":{"create":"false","read":"true","update":"false","delete":"false"},"treatments":{"create":"true","read":"true","update":"true","delete":"true"},"invoices":{"create":"true","read":"true","update":"true","delete":"true"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"true"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"true"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"worktype":{"create":"true","read":"true","update":"true","delete":"true"},"work":{"create":"true","read":"true","update":"true","delete":"true"},"suppliers":{"create":"true","read":"true","update":"true","delete":"true"},"lab":{"create":"true","read":"true","update":"true","delete":"true"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"patientfiles":{"create":"true","read":"true","update":"true","delete":"true"},"expenses":{"create":"true","read":"true","update":"true","delete":"true"},"purchases":{"create":"true","read":"true","update":"true","delete":"true"},"items":{"create":"true","read":"true","update":"true","delete":"true","issue":"true"},"stocks":{"create":"true","read":"true","update":"true","delete":"true"},"accounts":{"create":"true","read":"true","update":"true","delete":"true"},"purchasedetails":{"create":"true","read":"true","update":"true","delete":"true"},"stockreleases":{"create":"true","read":"true","update":"true","delete":"true"},"employeepayroll":{"create":"true","read":"true","update":"true","delete":"true"},"employeeattendance":{"create":"true","read":"true","update":"true","delete":"true"},"tokens":{"create":"true","read":"true","update":"true","delete":"true"}}";s:10:"role_title";s:11:"Super Admin";}'),
('10647cb177a46104d13515e673ba44df', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', 1552835311, 'a:10:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"admin";s:15:"admin_full_name";s:15:"Talib Allauddin";s:8:"admin_id";s:1:"1";s:10:"last_login";s:19:"2019-03-15 08:42:56";s:7:"last_ip";s:3:"::1";s:10:"permission";s:1942:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"customers":{"create":"true","read":"true","update":"true","delete":"true"},"orders":{"create":"true","read":"true","update":"true","delete":"true","history":"false","fullaccess":"false"},"payments":{"create":"false","read":"false","update":"false","delete":"false"},"invoice":{"create":"true","read":"true","update":"true","delete":"true"},"menu":{"create":"true","read":"true","update":"true","delete":"true"},"pages":{"create":"true","read":"true","update":"true","delete":"true"},"faqs":{"create":"true","read":"true","update":"true","delete":"true"},"about":{"create":"true","read":"true","update":"true","delete":"true"},"homecontent":{"create":"false","read":"false","update":"false","delete":"false"},"services":{"create":"true","read":"true","update":"true","delete":"true"},"gallery":{"create":"true","read":"true","update":"true","delete":"true"},"testimonial":{"create":"true","read":"true","update":"true","delete":"true"},"sliders":{"create":"true","read":"true","update":"true","delete":"true"},"slider":{"create":"true","read":"true","update":"true","delete":"true"},"products":{"create":"true","read":"true","update":"true","delete":"true"},"productcategory":{"create":"true","read":"true","update":"true","delete":"true"},"sales":{"create":"false","read":"true","update":"false","delete":"true","history":"true","fullaccess":"true"},"contact":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"payment":{"create":"true","read":"true","update":"true","delete":"true","history":"true","fullaccess":"true"},"shipment":{"create":"true","read":"true","update":"true","delete":"true"}}";s:10:"role_title";s:5:"Admin";}'),
('1920b2c63ca8079095db0bbcedc5de6e', '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36', 1458922422, ''),
('1c56fab3ffeac4bb57adf9d388ff5468', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', 1553553845, 'a:12:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"admin";s:15:"admin_full_name";s:15:"Talib Allauddin";s:8:"admin_id";s:1:"1";s:10:"last_login";s:19:"2019-03-26 03:02:19";s:7:"last_ip";s:3:"::1";s:12:"user_role_id";s:1:"1";s:9:"clinic_id";s:1:"0";s:10:"permission";s:1463:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"clinics":{"create":"true","read":"true","update":"true","delete":"true"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"appointments":{"create":"true","read":"true","update":"true","delete":"true"},"doctors":{"create":"true","read":"true","update":"true","delete":"true"},"patients":{"create":"true","read":"true","update":"true","delete":"true"},"procedures":{"create":"true","read":"true","update":"true","delete":"true"},"labtracking":{"create":"true","read":"true","update":"true","delete":"true"},"reports":{"create":"true","read":"true","update":"true","delete":"true"},"treatments":{"create":"true","read":"true","update":"true","delete":"true"},"invoices":{"create":"true","read":"true","update":"true","delete":"true"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"true"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"true"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"true","read":"true","update":"true","delete":"true"},"worktype":{"create":"true","read":"true","update":"true","delete":"true"},"work":{"create":"true","read":"true","update":"true","delete":"true"},"lab":{"create":"true","read":"true","update":"true","delete":"true"},"site":{"create":"true","read":"true","update":"true","delete":"true"}}";s:10:"role_title";s:11:"Super Admin";}'),
('1f577a610777deb43af538462ea1dbf1', '::1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36', 1460647430, 'a:8:{s:10:"admin_auth";s:5:"allow";s:10:"admin_role";s:11:"Super Admin";s:15:"admin_user_name";s:5:"admin";s:15:"admin_full_name";s:5:"5-STI";s:8:"admin_id";s:1:"1";s:10:"last_login";s:19:"2016-04-13 09:05:53";s:7:"last_ip";s:3:"::1";s:20:"useractivitylog_data";a:3:{s:7:"page_id";b:0;s:8:"activity";s:185:"&lt;p&gt;&amp;lt;p&amp;gt;&amp;amp;lt;p&amp;amp;gt;&amp;amp;amp;lt;p&amp;amp;amp;gt;asdf&amp;amp;amp;lt;/p&amp;amp;amp;gt;sadfasf&amp;amp;lt;/p&amp;amp;gt;&amp;lt;/p&amp;gt;&lt;/p&gt;\r\n";s:9:"timestamp";s:19:"2016-04-14 16:33:58";}}'),
('20c057f48152b73f14faa0d118d0beae', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 1557962471, ''),
('262f48d8c5c51435d3b3c1d116addecb', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 1555063015, 'a:12:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"admin";s:15:"admin_full_name";s:15:"Talib Allauddin";s:8:"admin_id";s:1:"1";s:10:"last_login";s:19:"2019-04-12 07:45:07";s:7:"last_ip";s:3:"::1";s:12:"user_role_id";s:1:"1";s:9:"clinic_id";s:1:"0";s:10:"permission";s:2412:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"clinics":{"create":"true","read":"true","update":"true","delete":"true"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"appointments":{"create":"true","read":"true","update":"true","delete":"true"},"doctors":{"create":"true","read":"true","update":"true","delete":"true"},"patients":{"create":"true","read":"true","update":"true","delete":"true"},"procedures":{"create":"true","read":"true","update":"true","delete":"true"},"labtracking":{"create":"true","read":"true","update":"true","delete":"true"},"reports":{"create":"false","read":"true","update":"false","delete":"false"},"treatments":{"create":"true","read":"true","update":"true","delete":"true"},"invoices":{"create":"true","read":"true","update":"true","delete":"true"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"true"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"true"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"worktype":{"create":"true","read":"true","update":"true","delete":"true"},"work":{"create":"true","read":"true","update":"true","delete":"true"},"suppliers":{"create":"true","read":"true","update":"true","delete":"true"},"lab":{"create":"true","read":"true","update":"true","delete":"true"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"patientfiles":{"create":"true","read":"true","update":"true","delete":"true"},"expenses":{"create":"true","read":"true","update":"true","delete":"true"},"purchases":{"create":"true","read":"true","update":"true","delete":"true"},"items":{"create":"true","read":"true","update":"true","delete":"true","issue":"true"},"stocks":{"create":"true","read":"true","update":"true","delete":"true"},"accounts":{"create":"true","read":"true","update":"true","delete":"true"},"purchasedetails":{"create":"true","read":"true","update":"true","delete":"true"},"stockreleases":{"create":"true","read":"true","update":"true","delete":"true"},"employeepayroll":{"create":"true","read":"true","update":"true","delete":"true"},"employeeattendance":{"create":"true","read":"true","update":"true","delete":"true"},"tokens":{"create":"true","read":"true","update":"true","delete":"true"}}";s:10:"role_title";s:11:"Super Admin";}'),
('28078e63d90515c5d05d9d27df89d3dd', '::1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36', 1460461257, 'a:8:{s:10:"admin_auth";s:5:"allow";s:10:"admin_role";s:11:"Super Admin";s:15:"admin_user_name";s:5:"admin";s:15:"admin_full_name";s:5:"5-STI";s:8:"admin_id";s:1:"1";s:10:"last_login";s:19:"2016-04-10 13:43:07";s:7:"last_ip";s:9:"127.0.0.1";s:8:"per_page";s:1:"0";}'),
('284e5ed01034e0e7eb32e7c531ac6b3e', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', 1553183228, 'a:12:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"admin";s:15:"admin_full_name";s:15:"Talib Allauddin";s:8:"admin_id";s:1:"1";s:10:"last_login";s:19:"2019-03-21 16:10:17";s:7:"last_ip";s:3:"::1";s:12:"user_role_id";s:1:"1";s:9:"clinic_id";s:1:"0";s:10:"permission";s:1328:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"clinics":{"create":"true","read":"true","update":"true","delete":"true"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"appointments":{"create":"true","read":"true","update":"true","delete":"true"},"doctors":{"create":"true","read":"true","update":"true","delete":"true"},"patients":{"create":"true","read":"true","update":"true","delete":"true"},"procedures":{"create":"true","read":"true","update":"true","delete":"true"},"labtracking":{"create":"true","read":"true","update":"true","delete":"true"},"reports":{"create":"false","read":"true","update":"false","delete":"false"},"treatments":{"create":"true","read":"true","update":"true","delete":"true"},"invoices":{"create":"true","read":"true","update":"true","delete":"true"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"true"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"true"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"worktype":{"create":"true","read":"true","update":"true","delete":"true"},"site":{"create":"true","read":"true","update":"true","delete":"true"}}";s:10:"role_title";s:11:"Super Admin";}'),
('2b5ffc8bfdee10911676460cd74dbd7f', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 1556188585, ''),
('2d6f6b4a12469ac17a69afaed439cb6f', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', 1553803842, 'a:12:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"admin";s:15:"admin_full_name";s:15:"Talib Allauddin";s:8:"admin_id";s:1:"1";s:10:"last_login";s:19:"2019-03-28 19:35:24";s:7:"last_ip";s:3:"::1";s:12:"user_role_id";s:1:"1";s:9:"clinic_id";s:1:"0";s:10:"permission";s:1628:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"clinics":{"create":"true","read":"true","update":"true","delete":"true"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"appointments":{"create":"true","read":"true","update":"true","delete":"true"},"doctors":{"create":"true","read":"true","update":"true","delete":"true"},"patients":{"create":"true","read":"true","update":"true","delete":"true"},"procedures":{"create":"true","read":"true","update":"true","delete":"true"},"labtracking":{"create":"true","read":"true","update":"true","delete":"true"},"reports":{"create":"false","read":"true","update":"false","delete":"false"},"treatments":{"create":"true","read":"true","update":"true","delete":"true"},"invoices":{"create":"true","read":"true","update":"true","delete":"true"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"true"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"true"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"worktype":{"create":"true","read":"true","update":"true","delete":"true"},"work":{"create":"true","read":"true","update":"true","delete":"true"},"suppliers":{"create":"false","read":"false","update":"false","delete":"false"},"lab":{"create":"true","read":"true","update":"true","delete":"true"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"patientfiles":{"create":"true","read":"true","update":"true","delete":"true"}}";s:10:"role_title";s:11:"Super Admin";}'),
('2e0e40ad2c9de54482815ad8b0aeec7d', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', 1551950561, 'a:9:{s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"admin";s:15:"admin_full_name";s:15:"Talib Allauddin";s:8:"admin_id";s:1:"1";s:10:"last_login";s:19:"2019-01-19 09:10:09";s:7:"last_ip";s:3:"::1";s:10:"permission";s:1942:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"customers":{"create":"true","read":"true","update":"true","delete":"true"},"orders":{"create":"true","read":"true","update":"true","delete":"true","history":"false","fullaccess":"false"},"payments":{"create":"false","read":"false","update":"false","delete":"false"},"invoice":{"create":"true","read":"true","update":"true","delete":"true"},"menu":{"create":"true","read":"true","update":"true","delete":"true"},"pages":{"create":"true","read":"true","update":"true","delete":"true"},"faqs":{"create":"true","read":"true","update":"true","delete":"true"},"about":{"create":"true","read":"true","update":"true","delete":"true"},"homecontent":{"create":"false","read":"false","update":"false","delete":"false"},"services":{"create":"true","read":"true","update":"true","delete":"true"},"gallery":{"create":"true","read":"true","update":"true","delete":"true"},"testimonial":{"create":"true","read":"true","update":"true","delete":"true"},"sliders":{"create":"true","read":"true","update":"true","delete":"true"},"slider":{"create":"true","read":"true","update":"true","delete":"true"},"products":{"create":"true","read":"true","update":"true","delete":"true"},"productcategory":{"create":"true","read":"true","update":"true","delete":"true"},"sales":{"create":"false","read":"true","update":"false","delete":"true","history":"true","fullaccess":"true"},"contact":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"payment":{"create":"true","read":"true","update":"true","delete":"true","history":"true","fullaccess":"true"},"shipment":{"create":"true","read":"true","update":"true","delete":"true"}}";s:10:"role_title";s:5:"Admin";}'),
('2f22b5a889b7fc100933419b84e4154e', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 1556650412, ''),
('311ce82a894bd4586fa4c44affa8d6f6', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', 1553205245, 'a:12:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"admin";s:15:"admin_full_name";s:15:"Talib Allauddin";s:8:"admin_id";s:1:"1";s:10:"last_login";s:19:"2019-03-21 22:03:52";s:7:"last_ip";s:3:"::1";s:12:"user_role_id";s:1:"1";s:9:"clinic_id";s:1:"0";s:10:"permission";s:1469:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"clinics":{"create":"true","read":"true","update":"true","delete":"true"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"appointments":{"create":"true","read":"true","update":"true","delete":"true"},"doctors":{"create":"true","read":"true","update":"true","delete":"true"},"patients":{"create":"true","read":"true","update":"true","delete":"true"},"procedures":{"create":"true","read":"true","update":"true","delete":"true"},"labtracking":{"create":"true","read":"true","update":"true","delete":"true"},"reports":{"create":"false","read":"true","update":"false","delete":"false"},"treatments":{"create":"true","read":"true","update":"true","delete":"true"},"invoices":{"create":"true","read":"true","update":"true","delete":"true"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"true"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"true"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"worktype":{"create":"true","read":"true","update":"true","delete":"true"},"work":{"create":"true","read":"true","update":"true","delete":"true"},"lab":{"create":"true","read":"true","update":"true","delete":"true"},"site":{"create":"true","read":"true","update":"true","delete":"true"}}";s:10:"role_title";s:11:"Super Admin";}'),
('3187cb1163320625bb1d02c1cee90d6e', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', 1553690955, 'a:12:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"admin";s:15:"admin_full_name";s:15:"Talib Allauddin";s:8:"admin_id";s:1:"1";s:10:"last_login";s:19:"2019-03-26 21:01:16";s:7:"last_ip";s:3:"::1";s:12:"user_role_id";s:1:"1";s:9:"clinic_id";s:1:"0";s:10:"permission";s:1463:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"clinics":{"create":"true","read":"true","update":"true","delete":"true"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"appointments":{"create":"true","read":"true","update":"true","delete":"true"},"doctors":{"create":"true","read":"true","update":"true","delete":"true"},"patients":{"create":"true","read":"true","update":"true","delete":"true"},"procedures":{"create":"true","read":"true","update":"true","delete":"true"},"labtracking":{"create":"true","read":"true","update":"true","delete":"true"},"reports":{"create":"true","read":"true","update":"true","delete":"true"},"treatments":{"create":"true","read":"true","update":"true","delete":"true"},"invoices":{"create":"true","read":"true","update":"true","delete":"true"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"true"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"true"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"true","read":"true","update":"true","delete":"true"},"worktype":{"create":"true","read":"true","update":"true","delete":"true"},"work":{"create":"true","read":"true","update":"true","delete":"true"},"lab":{"create":"true","read":"true","update":"true","delete":"true"},"site":{"create":"true","read":"true","update":"true","delete":"true"}}";s:10:"role_title";s:11:"Super Admin";}'),
('31914682122ae23ce2c6d09ea67f8c3e', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', 1552023902, 'a:10:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"admin";s:15:"admin_full_name";s:15:"Talib Allauddin";s:8:"admin_id";s:1:"1";s:10:"last_login";s:19:"2019-03-06 22:34:50";s:7:"last_ip";s:3:"::1";s:10:"permission";s:1942:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"customers":{"create":"true","read":"true","update":"true","delete":"true"},"orders":{"create":"true","read":"true","update":"true","delete":"true","history":"false","fullaccess":"false"},"payments":{"create":"false","read":"false","update":"false","delete":"false"},"invoice":{"create":"true","read":"true","update":"true","delete":"true"},"menu":{"create":"true","read":"true","update":"true","delete":"true"},"pages":{"create":"true","read":"true","update":"true","delete":"true"},"faqs":{"create":"true","read":"true","update":"true","delete":"true"},"about":{"create":"true","read":"true","update":"true","delete":"true"},"homecontent":{"create":"false","read":"false","update":"false","delete":"false"},"services":{"create":"true","read":"true","update":"true","delete":"true"},"gallery":{"create":"true","read":"true","update":"true","delete":"true"},"testimonial":{"create":"true","read":"true","update":"true","delete":"true"},"sliders":{"create":"true","read":"true","update":"true","delete":"true"},"slider":{"create":"true","read":"true","update":"true","delete":"true"},"products":{"create":"true","read":"true","update":"true","delete":"true"},"productcategory":{"create":"true","read":"true","update":"true","delete":"true"},"sales":{"create":"false","read":"true","update":"false","delete":"true","history":"true","fullaccess":"true"},"contact":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"payment":{"create":"true","read":"true","update":"true","delete":"true","history":"true","fullaccess":"true"},"shipment":{"create":"true","read":"true","update":"true","delete":"true"}}";s:10:"role_title";s:5:"Admin";}');
INSERT INTO `ci_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('362d881bfd985dc16507752220593b1d', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 1555302928, 'a:12:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"admin";s:15:"admin_full_name";s:15:"Talib Allauddin";s:8:"admin_id";s:1:"1";s:10:"last_login";s:19:"2019-04-14 11:48:27";s:7:"last_ip";s:3:"::1";s:12:"user_role_id";s:1:"1";s:9:"clinic_id";s:1:"0";s:10:"permission";s:2412:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"clinics":{"create":"true","read":"true","update":"true","delete":"true"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"appointments":{"create":"true","read":"true","update":"true","delete":"true"},"doctors":{"create":"true","read":"true","update":"true","delete":"true"},"patients":{"create":"true","read":"true","update":"true","delete":"true"},"procedures":{"create":"true","read":"true","update":"true","delete":"true"},"labtracking":{"create":"true","read":"true","update":"true","delete":"true"},"reports":{"create":"false","read":"true","update":"false","delete":"false"},"treatments":{"create":"true","read":"true","update":"true","delete":"true"},"invoices":{"create":"true","read":"true","update":"true","delete":"true"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"true"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"true"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"worktype":{"create":"true","read":"true","update":"true","delete":"true"},"work":{"create":"true","read":"true","update":"true","delete":"true"},"suppliers":{"create":"true","read":"true","update":"true","delete":"true"},"lab":{"create":"true","read":"true","update":"true","delete":"true"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"patientfiles":{"create":"true","read":"true","update":"true","delete":"true"},"expenses":{"create":"true","read":"true","update":"true","delete":"true"},"purchases":{"create":"true","read":"true","update":"true","delete":"true"},"items":{"create":"true","read":"true","update":"true","delete":"true","issue":"true"},"stocks":{"create":"true","read":"true","update":"true","delete":"true"},"accounts":{"create":"true","read":"true","update":"true","delete":"true"},"purchasedetails":{"create":"true","read":"true","update":"true","delete":"true"},"stockreleases":{"create":"true","read":"true","update":"true","delete":"true"},"employeepayroll":{"create":"true","read":"true","update":"true","delete":"true"},"employeeattendance":{"create":"true","read":"true","update":"true","delete":"true"},"tokens":{"create":"true","read":"true","update":"true","delete":"true"}}";s:10:"role_title";s:11:"Super Admin";}'),
('37260b372ff73995722359864f6c7dd7', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 1556004799, ''),
('3bda45aed037fddb53efa62f3f1fcb48', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 1557715183, 'a:13:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"rafay";s:15:"admin_full_name";s:11:"Abdur Rafay";s:8:"admin_id";s:1:"6";s:10:"last_login";s:19:"2019-05-13 06:03:00";s:7:"last_ip";s:3:"::1";s:12:"user_role_id";s:1:"2";s:9:"clinic_id";s:1:"1";s:10:"permission";s:3263:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"clinics":{"create":"false","read":"false","update":"false","delete":"false"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"appointments":{"create":"true","read":"true","update":"true","delete":"true"},"patients":{"create":"true","read":"true","update":"true","delete":"true","profile":"true"},"procedures":{"create":"true","read":"true","update":"true","delete":"true"},"labtracking":{"create":"true","read":"true","update":"true","delete":"true"},"reports":{"create":"false","read":"true","update":"false","delete":"false"},"treatments":{"create":"true","read":"true","update":"true","delete":"true"},"invoices":{"create":"true","read":"true","update":"true","delete":"true"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"true"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"true"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"worktype":{"create":"true","read":"true","update":"true","delete":"true"},"work":{"create":"true","read":"true","update":"true","delete":"true"},"suppliers":{"create":"true","read":"true","update":"true","delete":"true"},"lab":{"create":"true","read":"true","update":"true","delete":"true"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"patientfiles":{"create":"true","read":"true","update":"true","delete":"true"},"expenses":{"create":"true","read":"true","update":"true","delete":"true"},"purchases":{"create":"true","read":"true","update":"true","delete":"true"},"items":{"create":"true","read":"true","update":"true","delete":"true","issue":"true"},"stocks":{"create":"true","read":"true","update":"true","delete":"true"},"accounts":{"create":"true","read":"true","update":"true","delete":"true"},"purchasedetails":{"create":"true","read":"true","update":"true","delete":"true"},"stockreleases":{"create":"true","read":"true","update":"true","delete":"true"},"employeepayroll":{"create":"true","read":"true","update":"true","delete":"true"},"employeeattendance":{"create":"true","read":"true","update":"true","delete":"true"},"tokens":{"create":"true","read":"true","update":"true","delete":"true"},"backup":{"create":"false","read":"true","update":"false","delete":"false"},"reporttype":{"create":"true","read":"true","update":"true","delete":"true"},"":{"create":"false","read":"false","update":"false","delete":"false"},"roomtype":{"create":"true","read":"true","update":"true","delete":"true"},"room":{"create":"true","read":"true","update":"true","delete":"true"},"bedtype":{"create":"true","read":"true","update":"true","delete":"true"},"bed":{"create":"true","read":"true","update":"true","delete":"true"},"bedpatient":{"create":"true","read":"true","update":"true","delete":"true"},"roompatient":{"create":"true","read":"true","update":"true","delete":"true"},"doctordays":{"create":"true","read":"true","update":"true","delete":"true"},"patientprocedures":{"create":"true","read":"true","update":"true","delete":"true"},"doctors":{"create":"true","read":"true","update":"true","delete":"true"}}";s:14:"doctor_role_id";s:1:"4";s:10:"role_title";s:5:"Admin";}'),
('3d19eb7c58d312d4194d0db53539bdad', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 1556645579, 'a:13:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"rafay";s:15:"admin_full_name";s:11:"Abdur Rafay";s:8:"admin_id";s:1:"6";s:10:"last_login";s:19:"2019-04-29 15:19:35";s:7:"last_ip";s:13:"192.168.0.109";s:12:"user_role_id";s:1:"2";s:9:"clinic_id";s:1:"1";s:10:"permission";s:3166:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"clinics":{"create":"false","read":"false","update":"false","delete":"false"},"rolemanagement":{"create":"false","read":"false","update":"false","delete":"false"},"appointments":{"create":"true","read":"true","update":"true","delete":"true"},"patients":{"create":"true","read":"true","update":"true","delete":"true"},"procedures":{"create":"true","read":"true","update":"true","delete":"true"},"labtracking":{"create":"true","read":"true","update":"true","delete":"true"},"reports":{"create":"false","read":"true","update":"false","delete":"false"},"treatments":{"create":"true","read":"true","update":"true","delete":"true"},"invoices":{"create":"true","read":"true","update":"true","delete":"true"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"true"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"true"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"worktype":{"create":"true","read":"true","update":"true","delete":"true"},"work":{"create":"true","read":"true","update":"true","delete":"true"},"suppliers":{"create":"true","read":"true","update":"true","delete":"true"},"lab":{"create":"true","read":"true","update":"true","delete":"true"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"patientfiles":{"create":"true","read":"true","update":"true","delete":"true"},"expenses":{"create":"true","read":"true","update":"true","delete":"true"},"purchases":{"create":"true","read":"true","update":"true","delete":"true"},"items":{"create":"true","read":"true","update":"true","delete":"true","issue":"true"},"stocks":{"create":"true","read":"true","update":"true","delete":"true"},"accounts":{"create":"true","read":"true","update":"true","delete":"true"},"purchasedetails":{"create":"true","read":"true","update":"true","delete":"true"},"stockreleases":{"create":"true","read":"true","update":"true","delete":"true"},"employeepayroll":{"create":"true","read":"true","update":"true","delete":"true"},"employeeattendance":{"create":"true","read":"true","update":"true","delete":"true"},"tokens":{"create":"true","read":"true","update":"true","delete":"true"},"backup":{"create":"false","read":"true","update":"false","delete":"false"},"reporttype":{"create":"true","read":"true","update":"true","delete":"true"},"":{"create":"false","read":"false","update":"false","delete":"false"},"roomtype":{"create":"true","read":"true","update":"true","delete":"true"},"room":{"create":"true","read":"true","update":"true","delete":"true"},"bedtype":{"create":"true","read":"true","update":"true","delete":"true"},"bed":{"create":"true","read":"true","update":"true","delete":"true"},"bedpatient":{"create":"true","read":"true","update":"true","delete":"true"},"roompatient":{"create":"true","read":"true","update":"true","delete":"true"},"doctordays":{"create":"true","read":"true","update":"true","delete":"true"},"doctors":{"create":"true","read":"true","update":"true","delete":"true"}}";s:14:"doctor_role_id";s:1:"4";s:10:"role_title";s:5:"Admin";}'),
('3dd106fc6acb8ef83c3b3802b5605404', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 1555489465, ''),
('3dfef293341a864f92ee1f77a5112cc9', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 1555727398, 'a:13:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"admin";s:15:"admin_full_name";s:15:"Talib Allauddin";s:8:"admin_id";s:1:"1";s:10:"last_login";s:19:"2019-04-19 22:18:45";s:7:"last_ip";s:3:"::1";s:12:"user_role_id";s:1:"1";s:9:"clinic_id";s:1:"0";s:10:"permission";s:2565:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"clinics":{"create":"true","read":"true","update":"true","delete":"true"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"appointments":{"create":"true","read":"true","update":"true","delete":"true"},"doctors":{"create":"true","read":"true","update":"true","delete":"true"},"patients":{"create":"true","read":"true","update":"true","delete":"true"},"procedures":{"create":"true","read":"true","update":"true","delete":"true"},"labtracking":{"create":"true","read":"true","update":"true","delete":"true"},"reports":{"create":"false","read":"true","update":"false","delete":"false"},"treatments":{"create":"true","read":"true","update":"true","delete":"true"},"invoices":{"create":"true","read":"true","update":"true","delete":"true"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"true"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"true"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"worktype":{"create":"true","read":"true","update":"true","delete":"true"},"work":{"create":"true","read":"true","update":"true","delete":"true"},"suppliers":{"create":"true","read":"true","update":"true","delete":"true"},"lab":{"create":"true","read":"true","update":"true","delete":"true"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"patientfiles":{"create":"true","read":"true","update":"true","delete":"true"},"expenses":{"create":"true","read":"true","update":"true","delete":"true"},"purchases":{"create":"true","read":"true","update":"true","delete":"true"},"items":{"create":"true","read":"true","update":"true","delete":"true","issue":"true"},"stocks":{"create":"true","read":"true","update":"true","delete":"true"},"accounts":{"create":"true","read":"true","update":"true","delete":"true"},"purchasedetails":{"create":"true","read":"true","update":"true","delete":"true"},"stockreleases":{"create":"true","read":"true","update":"true","delete":"true"},"employeepayroll":{"create":"true","read":"true","update":"true","delete":"true"},"employeeattendance":{"create":"true","read":"true","update":"true","delete":"true"},"tokens":{"create":"true","read":"true","update":"true","delete":"true"},"backup":{"create":"false","read":"true","update":"false","delete":"false"},"reporttype":{"create":"true","read":"true","update":"true","delete":"true"}}";s:14:"doctor_role_id";s:1:"4";s:10:"role_title";s:11:"Super Admin";}'),
('3e14822c48887b960454a25255679cdf', '::1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36', 1461567197, ''),
('3e16b79fa722bb511d698522fdd542f7', '::1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.75 Safari/537.36', 1461045026, ''),
('3ec8bac5b9724c0d7fc4ca856ba8248c', '::1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36', 1461145375, 'a:10:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"admin";s:15:"admin_full_name";s:5:"5-STI";s:8:"admin_id";s:1:"1";s:10:"last_login";s:19:"2016-04-20 10:20:54";s:7:"last_ip";s:3:"::1";s:10:"permission";s:1714:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"customers":{"create":"true","read":"true","update":"true","delete":"true"},"orders":{"create":"true","read":"true","update":"true","delete":"true","history":"false","fullaccess":"false"},"payments":{"create":"false","read":"false","update":"false","delete":"false"},"invoice":{"create":"true","read":"true","update":"true","delete":"true"},"menu":{"create":"true","read":"true","update":"true","delete":"true"},"pages":{"create":"true","read":"true","update":"true","delete":"true"},"about":{"create":"true","read":"true","update":"true","delete":"true"},"services":{"create":"true","read":"true","update":"true","delete":"true"},"gallery":{"create":"true","read":"true","update":"true","delete":"true"},"testimonial":{"create":"true","read":"true","update":"true","delete":"true"},"sliders":{"create":"true","read":"true","update":"true","delete":"true"},"slider":{"create":"true","read":"true","update":"true","delete":"true"},"products":{"create":"true","read":"true","update":"true","delete":"true"},"productcategory":{"create":"true","read":"true","update":"true","delete":"true"},"sales":{"create":"false","read":"true","update":"false","delete":"true","history":"true","fullaccess":"true"},"contact":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"payment":{"create":"true","read":"true","update":"true","delete":"true","history":"true","fullaccess":"true"}}";s:10:"role_title";s:5:"Admin";}'),
('40541df884cede9f2ee96e893728b760', '192.168.10.9', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 1557945564, ''),
('41937601d39fdadb9f74fa21e31e21d4', '::1', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1547890186, 'a:10:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"admin";s:15:"admin_full_name";s:15:"Talib Allauddin";s:8:"admin_id";s:1:"1";s:10:"last_login";s:19:"2019-01-18 16:28:22";s:7:"last_ip";s:3:"::1";s:10:"permission";s:1942:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"customers":{"create":"true","read":"true","update":"true","delete":"true"},"orders":{"create":"true","read":"true","update":"true","delete":"true","history":"false","fullaccess":"false"},"payments":{"create":"false","read":"false","update":"false","delete":"false"},"invoice":{"create":"true","read":"true","update":"true","delete":"true"},"menu":{"create":"true","read":"true","update":"true","delete":"true"},"pages":{"create":"true","read":"true","update":"true","delete":"true"},"faqs":{"create":"true","read":"true","update":"true","delete":"true"},"about":{"create":"true","read":"true","update":"true","delete":"true"},"homecontent":{"create":"false","read":"false","update":"false","delete":"false"},"services":{"create":"true","read":"true","update":"true","delete":"true"},"gallery":{"create":"true","read":"true","update":"true","delete":"true"},"testimonial":{"create":"true","read":"true","update":"true","delete":"true"},"sliders":{"create":"true","read":"true","update":"true","delete":"true"},"slider":{"create":"true","read":"true","update":"true","delete":"true"},"products":{"create":"true","read":"true","update":"true","delete":"true"},"productcategory":{"create":"true","read":"true","update":"true","delete":"true"},"sales":{"create":"false","read":"true","update":"false","delete":"true","history":"true","fullaccess":"true"},"contact":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"payment":{"create":"true","read":"true","update":"true","delete":"true","history":"true","fullaccess":"true"},"shipment":{"create":"true","read":"true","update":"true","delete":"true"}}";s:10:"role_title";s:5:"Admin";}'),
('459f5f69ffb73bda5588e4900e31ca38', '::1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36', 1461136650, 'a:10:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"admin";s:15:"admin_full_name";s:5:"5-STI";s:8:"admin_id";s:1:"1";s:10:"last_login";s:19:"2016-04-19 08:18:06";s:7:"last_ip";s:3:"::1";s:10:"permission";s:1714:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"customers":{"create":"true","read":"true","update":"true","delete":"true"},"orders":{"create":"true","read":"true","update":"true","delete":"true","history":"false","fullaccess":"false"},"payments":{"create":"false","read":"false","update":"false","delete":"false"},"invoice":{"create":"true","read":"true","update":"true","delete":"true"},"menu":{"create":"true","read":"true","update":"true","delete":"true"},"pages":{"create":"true","read":"true","update":"true","delete":"true"},"about":{"create":"true","read":"true","update":"true","delete":"true"},"services":{"create":"true","read":"true","update":"true","delete":"true"},"gallery":{"create":"true","read":"true","update":"true","delete":"true"},"testimonial":{"create":"true","read":"true","update":"true","delete":"true"},"sliders":{"create":"true","read":"true","update":"true","delete":"true"},"slider":{"create":"true","read":"true","update":"true","delete":"true"},"products":{"create":"true","read":"true","update":"true","delete":"true"},"productcategory":{"create":"true","read":"true","update":"true","delete":"true"},"sales":{"create":"false","read":"true","update":"false","delete":"true","history":"true","fullaccess":"true"},"contact":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"payment":{"create":"true","read":"true","update":"true","delete":"true","history":"true","fullaccess":"true"}}";s:10:"role_title";s:5:"Admin";}'),
('48e92d96f94e9780d335bb2c6c073383', '::1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36', 1461567197, ''),
('4dea9e45dc33b542d743e852405cc720', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', 1554614753, ''),
('4f1d16243028e75ef87702ce88eb8a74', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 1557709921, ''),
('52ac54905da78512087409e6a47d14a2', '::1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36', 1461141510, 'a:10:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"admin";s:15:"admin_full_name";s:5:"5-STI";s:8:"admin_id";s:1:"1";s:10:"last_login";s:19:"2016-04-20 09:28:58";s:7:"last_ip";s:3:"::1";s:10:"permission";s:1714:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"customers":{"create":"true","read":"true","update":"true","delete":"true"},"orders":{"create":"true","read":"true","update":"true","delete":"true","history":"false","fullaccess":"false"},"payments":{"create":"false","read":"false","update":"false","delete":"false"},"invoice":{"create":"true","read":"true","update":"true","delete":"true"},"menu":{"create":"true","read":"true","update":"true","delete":"true"},"pages":{"create":"true","read":"true","update":"true","delete":"true"},"about":{"create":"true","read":"true","update":"true","delete":"true"},"services":{"create":"true","read":"true","update":"true","delete":"true"},"gallery":{"create":"true","read":"true","update":"true","delete":"true"},"testimonial":{"create":"true","read":"true","update":"true","delete":"true"},"sliders":{"create":"true","read":"true","update":"true","delete":"true"},"slider":{"create":"true","read":"true","update":"true","delete":"true"},"products":{"create":"true","read":"true","update":"true","delete":"true"},"productcategory":{"create":"true","read":"true","update":"true","delete":"true"},"sales":{"create":"false","read":"true","update":"false","delete":"true","history":"true","fullaccess":"true"},"contact":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"payment":{"create":"true","read":"true","update":"true","delete":"true","history":"true","fullaccess":"true"}}";s:10:"role_title";s:5:"Admin";}'),
('53e3835e472ff5090e8e126c54d8bf52', '192.168.0.104', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 1555923655, 'a:13:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"rafay";s:15:"admin_full_name";s:11:"Abdur Rafay";s:8:"admin_id";s:1:"6";s:10:"last_login";s:19:"2019-04-19 22:28:35";s:7:"last_ip";s:3:"::1";s:12:"user_role_id";s:1:"2";s:9:"clinic_id";s:1:"1";s:10:"permission";s:2692:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"false"},"clinics":{"create":"false","read":"false","update":"false","delete":"false"},"rolemanagement":{"create":"false","read":"false","update":"false","delete":"false"},"appointments":{"create":"true","read":"true","update":"true","delete":"true"},"doctors":{"create":"true","read":"true","update":"true","delete":"true"},"patients":{"create":"true","read":"true","update":"true","delete":"true"},"procedures":{"create":"false","read":"true","update":"false","delete":"false"},"labtracking":{"create":"true","read":"true","update":"true","delete":"true"},"reports":{"create":"false","read":"true","update":"false","delete":"false"},"treatments":{"create":"true","read":"true","update":"true","delete":"true"},"invoices":{"create":"true","read":"true","update":"true","delete":"true"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"true"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"true"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"false","update":"false","delete":"false"},"worktype":{"create":"true","read":"true","update":"true","delete":"true"},"work":{"create":"true","read":"true","update":"true","delete":"true"},"suppliers":{"create":"true","read":"true","update":"true","delete":"true"},"lab":{"create":"true","read":"true","update":"true","delete":"true"},"site":{"create":"true","read":"true","update":"true","delete":"false"},"patientfiles":{"create":"false","read":"false","update":"false","delete":"false"},"expenses":{"create":"false","read":"false","update":"false","delete":"false"},"purchases":{"create":"false","read":"false","update":"false","delete":"false"},"items":{"create":"false","read":"false","update":"false","delete":"false","issue":"false"},"stocks":{"create":"false","read":"false","update":"false","delete":"false"},"accounts":{"create":"false","read":"false","update":"false","delete":"false"},"purchasedetails":{"create":"false","read":"false","update":"false","delete":"false"},"stockreleases":{"create":"false","read":"false","update":"false","delete":"false"},"employeepayroll":{"create":"false","read":"false","update":"false","delete":"false"},"employeeattendance":{"create":"true","read":"true","update":"true","delete":"true"},"tokens":{"create":"true","read":"true","update":"true","delete":"true"},"backup":{"create":"false","read":"false","update":"false","delete":"false"},"reporttype":{"create":"false","read":"false","update":"false","delete":"false"},"":{"create":"false","read":"false","update":"false","delete":"false"}}";s:14:"doctor_role_id";s:1:"4";s:10:"role_title";s:5:"Admin";}'),
('557cd6c744b4b7ff1209246fdb6a3958', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 1556002926, 'a:13:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"rafay";s:15:"admin_full_name";s:11:"Abdur Rafay";s:8:"admin_id";s:1:"6";s:10:"last_login";s:19:"2019-04-23 11:01:32";s:7:"last_ip";s:3:"::1";s:12:"user_role_id";s:1:"2";s:9:"clinic_id";s:1:"1";s:10:"permission";s:2644:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"clinics":{"create":"false","read":"false","update":"false","delete":"false"},"rolemanagement":{"create":"false","read":"false","update":"false","delete":"false"},"appointments":{"create":"true","read":"true","update":"true","delete":"true"},"patients":{"create":"true","read":"true","update":"true","delete":"true"},"procedures":{"create":"true","read":"true","update":"true","delete":"true"},"labtracking":{"create":"true","read":"true","update":"true","delete":"true"},"reports":{"create":"false","read":"true","update":"false","delete":"false"},"treatments":{"create":"true","read":"true","update":"true","delete":"true"},"invoices":{"create":"true","read":"true","update":"true","delete":"true"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"true"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"true"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"worktype":{"create":"true","read":"true","update":"true","delete":"true"},"work":{"create":"true","read":"true","update":"true","delete":"true"},"suppliers":{"create":"true","read":"true","update":"true","delete":"true"},"lab":{"create":"true","read":"true","update":"true","delete":"true"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"patientfiles":{"create":"true","read":"true","update":"true","delete":"true"},"expenses":{"create":"true","read":"true","update":"true","delete":"true"},"purchases":{"create":"true","read":"true","update":"true","delete":"true"},"items":{"create":"true","read":"true","update":"true","delete":"true","issue":"true"},"stocks":{"create":"true","read":"true","update":"true","delete":"true"},"accounts":{"create":"true","read":"true","update":"true","delete":"true"},"purchasedetails":{"create":"true","read":"true","update":"true","delete":"true"},"stockreleases":{"create":"true","read":"true","update":"true","delete":"true"},"employeepayroll":{"create":"true","read":"true","update":"true","delete":"true"},"employeeattendance":{"create":"true","read":"true","update":"true","delete":"true"},"tokens":{"create":"true","read":"true","update":"true","delete":"true"},"backup":{"create":"false","read":"true","update":"false","delete":"false"},"reporttype":{"create":"true","read":"true","update":"true","delete":"true"},"":{"create":"false","read":"false","update":"false","delete":"false"},"doctors":{"create":"true","read":"true","update":"true","delete":"true"}}";s:14:"doctor_role_id";s:1:"4";s:10:"role_title";s:5:"Admin";}'),
('55dd0ecf546863ac89f00a5f2048f3fb', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', 1552635769, 'a:10:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"admin";s:15:"admin_full_name";s:15:"Talib Allauddin";s:8:"admin_id";s:1:"1";s:10:"last_login";s:19:"2019-03-10 15:38:47";s:7:"last_ip";s:3:"::1";s:10:"permission";s:1942:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"customers":{"create":"true","read":"true","update":"true","delete":"true"},"orders":{"create":"true","read":"true","update":"true","delete":"true","history":"false","fullaccess":"false"},"payments":{"create":"false","read":"false","update":"false","delete":"false"},"invoice":{"create":"true","read":"true","update":"true","delete":"true"},"menu":{"create":"true","read":"true","update":"true","delete":"true"},"pages":{"create":"true","read":"true","update":"true","delete":"true"},"faqs":{"create":"true","read":"true","update":"true","delete":"true"},"about":{"create":"true","read":"true","update":"true","delete":"true"},"homecontent":{"create":"false","read":"false","update":"false","delete":"false"},"services":{"create":"true","read":"true","update":"true","delete":"true"},"gallery":{"create":"true","read":"true","update":"true","delete":"true"},"testimonial":{"create":"true","read":"true","update":"true","delete":"true"},"sliders":{"create":"true","read":"true","update":"true","delete":"true"},"slider":{"create":"true","read":"true","update":"true","delete":"true"},"products":{"create":"true","read":"true","update":"true","delete":"true"},"productcategory":{"create":"true","read":"true","update":"true","delete":"true"},"sales":{"create":"false","read":"true","update":"false","delete":"true","history":"true","fullaccess":"true"},"contact":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"payment":{"create":"true","read":"true","update":"true","delete":"true","history":"true","fullaccess":"true"},"shipment":{"create":"true","read":"true","update":"true","delete":"true"}}";s:10:"role_title";s:5:"Admin";}'),
('55fe2db80227b875564d07ae5b32504f', '192.168.10.2', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 1556867065, 'a:13:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"rafay";s:15:"admin_full_name";s:11:"Abdur Rafay";s:8:"admin_id";s:1:"6";s:10:"last_login";s:19:"2019-05-03 12:02:49";s:7:"last_ip";s:13:"192.168.10.11";s:12:"user_role_id";s:1:"2";s:9:"clinic_id";s:1:"1";s:10:"permission";s:3320:"{"admins":{"create":"false","read":"true","update":"false","delete":"false","fullaccess":"false"},"clinics":{"create":"false","read":"false","update":"false","delete":"false"},"rolemanagement":{"create":"false","read":"false","update":"false","delete":"false"},"appointments":{"create":"false","read":"false","update":"false","delete":"false"},"patients":{"create":"false","read":"false","update":"false","delete":"false"},"procedures":{"create":"false","read":"false","update":"false","delete":"false"},"labtracking":{"create":"false","read":"false","update":"false","delete":"false"},"reports":{"create":"false","read":"false","update":"false","delete":"false"},"treatments":{"create":"false","read":"false","update":"false","delete":"false"},"invoices":{"create":"false","read":"false","update":"false","delete":"false"},"healthrecord":{"create":"false","read":"false","update":"false","delete":"false"},"dentalchart":{"create":"false","read":"false","update":"false","delete":"false"},"doctortiming":{"create":"false","read":"false","update":"false","delete":"false"},"useractivitylog":{"create":"false","read":"false","update":"false","delete":"false"},"worktype":{"create":"false","read":"false","update":"false","delete":"false"},"work":{"create":"false","read":"false","update":"false","delete":"false"},"suppliers":{"create":"false","read":"false","update":"false","delete":"false"},"lab":{"create":"false","read":"false","update":"false","delete":"false"},"site":{"create":"false","read":"false","update":"false","delete":"false"},"patientfiles":{"create":"false","read":"false","update":"false","delete":"false"},"expenses":{"create":"false","read":"false","update":"false","delete":"false"},"purchases":{"create":"false","read":"false","update":"false","delete":"false"},"items":{"create":"false","read":"false","update":"false","delete":"false","issue":"false"},"stocks":{"create":"false","read":"false","update":"false","delete":"false"},"accounts":{"create":"false","read":"false","update":"false","delete":"false"},"purchasedetails":{"create":"false","read":"false","update":"false","delete":"false"},"stockreleases":{"create":"false","read":"false","update":"false","delete":"false"},"employeepayroll":{"create":"false","read":"false","update":"false","delete":"false"},"employeeattendance":{"create":"false","read":"false","update":"false","delete":"false"},"tokens":{"create":"false","read":"false","update":"false","delete":"false"},"backup":{"create":"false","read":"false","update":"false","delete":"false"},"reporttype":{"create":"false","read":"false","update":"false","delete":"false"},"":{"create":"false","read":"false","update":"false","delete":"false"},"roomtype":{"create":"false","read":"false","update":"false","delete":"false"},"room":{"create":"false","read":"false","update":"false","delete":"false"},"bedtype":{"create":"false","read":"false","update":"false","delete":"false"},"bed":{"create":"false","read":"false","update":"false","delete":"false"},"bedpatient":{"create":"false","read":"false","update":"false","delete":"false"},"roompatient":{"create":"false","read":"false","update":"false","delete":"false"},"doctordays":{"create":"false","read":"false","update":"false","delete":"false"},"patientprocedures":{"create":"false","read":"false","update":"false","delete":"false"}}";s:14:"doctor_role_id";s:1:"4";s:10:"role_title";s:5:"Admin";}'),
('572cbeb418431d0523cbb400e2da25b5', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 1558139662, 'a:13:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"rafay";s:15:"admin_full_name";s:11:"Abdur Rafay";s:8:"admin_id";s:1:"6";s:10:"last_login";s:19:"2019-05-16 08:11:39";s:7:"last_ip";s:3:"::1";s:12:"user_role_id";s:1:"2";s:9:"clinic_id";s:1:"1";s:10:"permission";s:3494:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"clinics":{"create":"false","read":"false","update":"false","delete":"false"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"appointments":{"create":"true","read":"true","update":"true","delete":"true"},"patients":{"create":"true","read":"true","update":"true","delete":"true","profile":"true"},"procedures":{"create":"true","read":"true","update":"true","delete":"true"},"labtracking":{"create":"true","read":"true","update":"true","delete":"true"},"reports":{"create":"false","read":"true","update":"false","delete":"false"},"treatments":{"create":"true","read":"true","update":"true","delete":"true"},"invoices":{"create":"true","read":"true","update":"true","delete":"true"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"true"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"true"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"worktype":{"create":"true","read":"true","update":"true","delete":"true"},"work":{"create":"true","read":"true","update":"true","delete":"true"},"suppliers":{"create":"true","read":"true","update":"true","delete":"true"},"lab":{"create":"true","read":"true","update":"true","delete":"true"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"patientfiles":{"create":"true","read":"true","update":"true","delete":"true"},"expenses":{"create":"true","read":"true","update":"true","delete":"true"},"purchases":{"create":"true","read":"true","update":"true","delete":"true"},"items":{"create":"true","read":"true","update":"true","delete":"true","issue":"true"},"stocks":{"create":"true","read":"true","update":"true","delete":"true"},"accounts":{"create":"true","read":"true","update":"true","delete":"true"},"purchasedetails":{"create":"true","read":"true","update":"true","delete":"true"},"stockreleases":{"create":"true","read":"true","update":"true","delete":"true"},"employeepayroll":{"create":"true","read":"true","update":"true","delete":"true"},"employeeattendance":{"create":"true","read":"true","update":"true","delete":"true"},"tokens":{"create":"true","read":"true","update":"true","delete":"true"},"backup":{"create":"false","read":"true","update":"false","delete":"false"},"reporttype":{"create":"true","read":"true","update":"true","delete":"true"},"":{"create":"false","read":"false","update":"false","delete":"false"},"roomtype":{"create":"true","read":"true","update":"true","delete":"true"},"room":{"create":"true","read":"true","update":"true","delete":"true"},"bedtype":{"create":"true","read":"true","update":"true","delete":"true"},"bed":{"create":"true","read":"true","update":"true","delete":"true"},"bedpatient":{"create":"true","read":"true","update":"true","delete":"true"},"roompatient":{"create":"true","read":"true","update":"true","delete":"true"},"doctordays":{"create":"true","read":"true","update":"true","delete":"true"},"patientprocedures":{"create":"true","read":"true","update":"true","delete":"true"},"facilities":{"create":"true","read":"true","update":"true","delete":"true"},"patientcase":{"create":"true","read":"true","update":"true","delete":"true"},"relations":{"create":"true","read":"true","update":"true","delete":"true"},"doctors":{"create":"true","read":"true","update":"true","delete":"true"}}";s:14:"doctor_role_id";s:1:"4";s:10:"role_title";s:5:"Admin";}'),
('5797d89ea7f8fbb893875d1d4a053a2e', '192.168.10.17', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2486.0 Safari/537.36 Edge/1', 1460729865, ''),
('595f1884ef7954aad70dc9d026a17a80', '::1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36', 1461163968, 'a:10:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"admin";s:15:"admin_full_name";s:5:"5-STI";s:8:"admin_id";s:1:"1";s:10:"last_login";s:19:"2016-04-20 11:30:11";s:7:"last_ip";s:3:"::1";s:10:"permission";s:1714:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"customers":{"create":"true","read":"true","update":"true","delete":"true"},"orders":{"create":"true","read":"true","update":"true","delete":"true","history":"false","fullaccess":"false"},"payments":{"create":"false","read":"false","update":"false","delete":"false"},"invoice":{"create":"true","read":"true","update":"true","delete":"true"},"menu":{"create":"true","read":"true","update":"true","delete":"true"},"pages":{"create":"true","read":"true","update":"true","delete":"true"},"about":{"create":"true","read":"true","update":"true","delete":"true"},"services":{"create":"true","read":"true","update":"true","delete":"true"},"gallery":{"create":"true","read":"true","update":"true","delete":"true"},"testimonial":{"create":"true","read":"true","update":"true","delete":"true"},"sliders":{"create":"true","read":"true","update":"true","delete":"true"},"slider":{"create":"true","read":"true","update":"true","delete":"true"},"products":{"create":"true","read":"true","update":"true","delete":"true"},"productcategory":{"create":"true","read":"true","update":"true","delete":"true"},"sales":{"create":"false","read":"true","update":"false","delete":"true","history":"true","fullaccess":"true"},"contact":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"payment":{"create":"true","read":"true","update":"true","delete":"true","history":"true","fullaccess":"true"}}";s:10:"role_title";s:5:"Admin";}'),
('5f21575c468fb9483033ac902213fe4a', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 1555148430, 'a:12:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"admin";s:15:"admin_full_name";s:15:"Talib Allauddin";s:8:"admin_id";s:1:"1";s:10:"last_login";s:19:"2019-04-12 18:18:41";s:7:"last_ip";s:3:"::1";s:12:"user_role_id";s:1:"1";s:9:"clinic_id";s:1:"0";s:10:"permission";s:2412:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"clinics":{"create":"true","read":"true","update":"true","delete":"true"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"appointments":{"create":"true","read":"true","update":"true","delete":"true"},"doctors":{"create":"true","read":"true","update":"true","delete":"true"},"patients":{"create":"true","read":"true","update":"true","delete":"true"},"procedures":{"create":"true","read":"true","update":"true","delete":"true"},"labtracking":{"create":"true","read":"true","update":"true","delete":"true"},"reports":{"create":"false","read":"true","update":"false","delete":"false"},"treatments":{"create":"true","read":"true","update":"true","delete":"true"},"invoices":{"create":"true","read":"true","update":"true","delete":"true"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"true"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"true"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"worktype":{"create":"true","read":"true","update":"true","delete":"true"},"work":{"create":"true","read":"true","update":"true","delete":"true"},"suppliers":{"create":"true","read":"true","update":"true","delete":"true"},"lab":{"create":"true","read":"true","update":"true","delete":"true"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"patientfiles":{"create":"true","read":"true","update":"true","delete":"true"},"expenses":{"create":"true","read":"true","update":"true","delete":"true"},"purchases":{"create":"true","read":"true","update":"true","delete":"true"},"items":{"create":"true","read":"true","update":"true","delete":"true","issue":"true"},"stocks":{"create":"true","read":"true","update":"true","delete":"true"},"accounts":{"create":"true","read":"true","update":"true","delete":"true"},"purchasedetails":{"create":"true","read":"true","update":"true","delete":"true"},"stockreleases":{"create":"true","read":"true","update":"true","delete":"true"},"employeepayroll":{"create":"true","read":"true","update":"true","delete":"true"},"employeeattendance":{"create":"true","read":"true","update":"true","delete":"true"},"tokens":{"create":"true","read":"true","update":"true","delete":"true"}}";s:10:"role_title";s:11:"Super Admin";}'),
('647d2c34f9d18929a4ccc2d629d33260', '::1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36', 1461567197, ''),
('66204637b417f4ed166e0cf1faa4e1dc', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 1555320150, 'a:12:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"admin";s:15:"admin_full_name";s:15:"Talib Allauddin";s:8:"admin_id";s:1:"1";s:10:"last_login";s:19:"2019-04-15 08:58:28";s:7:"last_ip";s:3:"::1";s:12:"user_role_id";s:1:"1";s:9:"clinic_id";s:1:"0";s:10:"permission";s:2412:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"clinics":{"create":"true","read":"true","update":"true","delete":"true"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"appointments":{"create":"true","read":"true","update":"true","delete":"true"},"doctors":{"create":"true","read":"true","update":"true","delete":"true"},"patients":{"create":"true","read":"true","update":"true","delete":"true"},"procedures":{"create":"true","read":"true","update":"true","delete":"true"},"labtracking":{"create":"true","read":"true","update":"true","delete":"true"},"reports":{"create":"false","read":"true","update":"false","delete":"false"},"treatments":{"create":"true","read":"true","update":"true","delete":"true"},"invoices":{"create":"true","read":"true","update":"true","delete":"true"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"true"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"true"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"worktype":{"create":"true","read":"true","update":"true","delete":"true"},"work":{"create":"true","read":"true","update":"true","delete":"true"},"suppliers":{"create":"true","read":"true","update":"true","delete":"true"},"lab":{"create":"true","read":"true","update":"true","delete":"true"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"patientfiles":{"create":"true","read":"true","update":"true","delete":"true"},"expenses":{"create":"true","read":"true","update":"true","delete":"true"},"purchases":{"create":"true","read":"true","update":"true","delete":"true"},"items":{"create":"true","read":"true","update":"true","delete":"true","issue":"true"},"stocks":{"create":"true","read":"true","update":"true","delete":"true"},"accounts":{"create":"true","read":"true","update":"true","delete":"true"},"purchasedetails":{"create":"true","read":"true","update":"true","delete":"true"},"stockreleases":{"create":"true","read":"true","update":"true","delete":"true"},"employeepayroll":{"create":"true","read":"true","update":"true","delete":"true"},"employeeattendance":{"create":"true","read":"true","update":"true","delete":"true"},"tokens":{"create":"true","read":"true","update":"true","delete":"true"}}";s:10:"role_title";s:11:"Super Admin";}');
INSERT INTO `ci_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('690b068646083581a640c21bac463ed3', '192.168.0.109', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 1556533128, 'a:13:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"rafay";s:15:"admin_full_name";s:11:"Abdur Rafay";s:8:"admin_id";s:1:"6";s:10:"last_login";s:19:"2019-04-29 01:23:01";s:7:"last_ip";s:3:"::1";s:12:"user_role_id";s:1:"2";s:9:"clinic_id";s:1:"1";s:10:"permission";s:3166:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"clinics":{"create":"false","read":"false","update":"false","delete":"false"},"rolemanagement":{"create":"false","read":"false","update":"false","delete":"false"},"appointments":{"create":"true","read":"true","update":"true","delete":"true"},"patients":{"create":"true","read":"true","update":"true","delete":"true"},"procedures":{"create":"true","read":"true","update":"true","delete":"true"},"labtracking":{"create":"true","read":"true","update":"true","delete":"true"},"reports":{"create":"false","read":"true","update":"false","delete":"false"},"treatments":{"create":"true","read":"true","update":"true","delete":"true"},"invoices":{"create":"true","read":"true","update":"true","delete":"true"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"true"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"true"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"worktype":{"create":"true","read":"true","update":"true","delete":"true"},"work":{"create":"true","read":"true","update":"true","delete":"true"},"suppliers":{"create":"true","read":"true","update":"true","delete":"true"},"lab":{"create":"true","read":"true","update":"true","delete":"true"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"patientfiles":{"create":"true","read":"true","update":"true","delete":"true"},"expenses":{"create":"true","read":"true","update":"true","delete":"true"},"purchases":{"create":"true","read":"true","update":"true","delete":"true"},"items":{"create":"true","read":"true","update":"true","delete":"true","issue":"true"},"stocks":{"create":"true","read":"true","update":"true","delete":"true"},"accounts":{"create":"true","read":"true","update":"true","delete":"true"},"purchasedetails":{"create":"true","read":"true","update":"true","delete":"true"},"stockreleases":{"create":"true","read":"true","update":"true","delete":"true"},"employeepayroll":{"create":"true","read":"true","update":"true","delete":"true"},"employeeattendance":{"create":"true","read":"true","update":"true","delete":"true"},"tokens":{"create":"true","read":"true","update":"true","delete":"true"},"backup":{"create":"false","read":"true","update":"false","delete":"false"},"reporttype":{"create":"true","read":"true","update":"true","delete":"true"},"":{"create":"false","read":"false","update":"false","delete":"false"},"roomtype":{"create":"true","read":"true","update":"true","delete":"true"},"room":{"create":"true","read":"true","update":"true","delete":"true"},"bedtype":{"create":"true","read":"true","update":"true","delete":"true"},"bed":{"create":"true","read":"true","update":"true","delete":"true"},"bedpatient":{"create":"true","read":"true","update":"true","delete":"true"},"roompatient":{"create":"true","read":"true","update":"true","delete":"true"},"doctordays":{"create":"true","read":"true","update":"true","delete":"true"},"doctors":{"create":"true","read":"true","update":"true","delete":"true"}}";s:14:"doctor_role_id";s:1:"4";s:10:"role_title";s:5:"Admin";}'),
('69c40008183619c6ce3eb4c2044a77ec', '192.168.10.17', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36', 1460982544, ''),
('6eaab985be7d8bc69c16e14fc4d784c4', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', 1554804745, 'a:12:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"admin";s:15:"admin_full_name";s:15:"Talib Allauddin";s:8:"admin_id";s:1:"1";s:10:"last_login";s:19:"2019-04-09 10:46:14";s:7:"last_ip";s:3:"::1";s:12:"user_role_id";s:1:"1";s:9:"clinic_id";s:1:"0";s:10:"permission";s:2339:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"clinics":{"create":"true","read":"true","update":"true","delete":"true"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"appointments":{"create":"true","read":"true","update":"true","delete":"true"},"doctors":{"create":"true","read":"true","update":"true","delete":"true"},"patients":{"create":"true","read":"true","update":"true","delete":"true"},"procedures":{"create":"true","read":"true","update":"true","delete":"true"},"labtracking":{"create":"true","read":"true","update":"true","delete":"true"},"reports":{"create":"false","read":"true","update":"false","delete":"false"},"treatments":{"create":"true","read":"true","update":"true","delete":"true"},"invoices":{"create":"true","read":"true","update":"true","delete":"true"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"true"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"true"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"worktype":{"create":"true","read":"true","update":"true","delete":"true"},"work":{"create":"true","read":"true","update":"true","delete":"true"},"suppliers":{"create":"true","read":"true","update":"true","delete":"true"},"lab":{"create":"true","read":"true","update":"true","delete":"true"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"patientfiles":{"create":"true","read":"true","update":"true","delete":"true"},"expenses":{"create":"true","read":"true","update":"true","delete":"true"},"purchases":{"create":"true","read":"true","update":"true","delete":"true"},"items":{"create":"true","read":"true","update":"true","delete":"true","issue":"true"},"stocks":{"create":"true","read":"true","update":"true","delete":"true"},"accounts":{"create":"true","read":"true","update":"true","delete":"true"},"purchasedetails":{"create":"true","read":"true","update":"true","delete":"true"},"stockreleases":{"create":"true","read":"true","update":"true","delete":"true"},"employeepayroll":{"create":"true","read":"true","update":"true","delete":"true"},"employeeattendance":{"create":"true","read":"true","update":"true","delete":"true"}}";s:10:"role_title";s:11:"Super Admin";}'),
('70799849072d5ef59a1eb51d049200ba', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 1555595665, 'a:12:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"rafay";s:15:"admin_full_name";s:11:"Abdur Rafay";s:8:"admin_id";s:1:"6";s:10:"last_login";s:19:"2019-04-18 15:57:07";s:7:"last_ip";s:13:"192.168.0.102";s:12:"user_role_id";s:1:"2";s:9:"clinic_id";s:1:"1";s:10:"permission";s:2692:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"false"},"clinics":{"create":"false","read":"false","update":"false","delete":"false"},"rolemanagement":{"create":"false","read":"false","update":"false","delete":"false"},"appointments":{"create":"true","read":"true","update":"true","delete":"true"},"doctors":{"create":"true","read":"true","update":"true","delete":"true"},"patients":{"create":"true","read":"true","update":"true","delete":"true"},"procedures":{"create":"false","read":"true","update":"false","delete":"false"},"labtracking":{"create":"true","read":"true","update":"true","delete":"true"},"reports":{"create":"false","read":"true","update":"false","delete":"false"},"treatments":{"create":"true","read":"true","update":"true","delete":"true"},"invoices":{"create":"true","read":"true","update":"true","delete":"true"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"true"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"true"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"false","update":"false","delete":"false"},"worktype":{"create":"true","read":"true","update":"true","delete":"true"},"work":{"create":"true","read":"true","update":"true","delete":"true"},"suppliers":{"create":"true","read":"true","update":"true","delete":"true"},"lab":{"create":"true","read":"true","update":"true","delete":"true"},"site":{"create":"true","read":"true","update":"true","delete":"false"},"patientfiles":{"create":"false","read":"false","update":"false","delete":"false"},"expenses":{"create":"false","read":"false","update":"false","delete":"false"},"purchases":{"create":"false","read":"false","update":"false","delete":"false"},"items":{"create":"false","read":"false","update":"false","delete":"false","issue":"false"},"stocks":{"create":"false","read":"false","update":"false","delete":"false"},"accounts":{"create":"false","read":"false","update":"false","delete":"false"},"purchasedetails":{"create":"false","read":"false","update":"false","delete":"false"},"stockreleases":{"create":"false","read":"false","update":"false","delete":"false"},"employeepayroll":{"create":"false","read":"false","update":"false","delete":"false"},"employeeattendance":{"create":"true","read":"true","update":"true","delete":"true"},"tokens":{"create":"true","read":"true","update":"true","delete":"true"},"backup":{"create":"false","read":"false","update":"false","delete":"false"},"reporttype":{"create":"false","read":"false","update":"false","delete":"false"},"":{"create":"false","read":"false","update":"false","delete":"false"}}";s:10:"role_title";s:5:"Admin";}'),
('70996401331df8180c2f94e69e40004b', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 1557094644, 'a:13:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"admin";s:15:"admin_full_name";s:15:"Talib Allauddin";s:8:"admin_id";s:1:"1";s:10:"last_login";s:19:"2019-05-03 12:04:00";s:7:"last_ip";s:3:"::1";s:12:"user_role_id";s:1:"1";s:9:"clinic_id";s:1:"0";s:10:"permission";s:3242:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"clinics":{"create":"true","read":"true","update":"true","delete":"true"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"appointments":{"create":"true","read":"true","update":"true","delete":"true"},"patients":{"create":"true","read":"true","update":"true","delete":"true"},"procedures":{"create":"true","read":"true","update":"true","delete":"true"},"labtracking":{"create":"true","read":"true","update":"true","delete":"true"},"reports":{"create":"false","read":"true","update":"false","delete":"false"},"treatments":{"create":"true","read":"true","update":"true","delete":"true"},"invoices":{"create":"true","read":"true","update":"true","delete":"true"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"true"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"true"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"worktype":{"create":"true","read":"true","update":"true","delete":"true"},"work":{"create":"true","read":"true","update":"true","delete":"true"},"suppliers":{"create":"true","read":"true","update":"true","delete":"true"},"lab":{"create":"true","read":"true","update":"true","delete":"true"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"patientfiles":{"create":"true","read":"true","update":"true","delete":"true"},"expenses":{"create":"true","read":"true","update":"true","delete":"true"},"purchases":{"create":"true","read":"true","update":"true","delete":"true"},"items":{"create":"true","read":"true","update":"true","delete":"true","issue":"true"},"stocks":{"create":"true","read":"true","update":"true","delete":"true"},"accounts":{"create":"true","read":"true","update":"true","delete":"true"},"purchasedetails":{"create":"true","read":"true","update":"true","delete":"true"},"stockreleases":{"create":"true","read":"true","update":"true","delete":"true"},"employeepayroll":{"create":"true","read":"true","update":"true","delete":"true"},"employeeattendance":{"create":"true","read":"true","update":"true","delete":"true"},"tokens":{"create":"true","read":"true","update":"true","delete":"true"},"backup":{"create":"false","read":"true","update":"false","delete":"false"},"reporttype":{"create":"true","read":"true","update":"true","delete":"true"},"":{"create":"false","read":"false","update":"false","delete":"false"},"roomtype":{"create":"true","read":"true","update":"true","delete":"true"},"room":{"create":"true","read":"true","update":"true","delete":"true"},"bedtype":{"create":"true","read":"true","update":"true","delete":"true"},"bed":{"create":"true","read":"true","update":"true","delete":"true"},"bedpatient":{"create":"true","read":"true","update":"true","delete":"true"},"roompatient":{"create":"true","read":"true","update":"true","delete":"true"},"doctordays":{"create":"true","read":"true","update":"true","delete":"true"},"patientprocedures":{"create":"true","read":"true","update":"true","delete":"true"},"doctors":{"create":"true","read":"true","update":"true","delete":"true"}}";s:14:"doctor_role_id";s:1:"4";s:10:"role_title";s:11:"Super Admin";}'),
('7145e63aeb8197c01e79246ff5e28732', '192.168.0.118', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 1555661682, 'a:13:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"jabar";s:15:"admin_full_name";s:6:"Jabar ";s:8:"admin_id";s:1:"9";s:10:"last_login";s:19:"2019-04-18 17:47:45";s:7:"last_ip";s:13:"192.168.0.102";s:12:"user_role_id";s:1:"2";s:9:"clinic_id";s:1:"1";s:10:"permission";s:2692:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"false"},"clinics":{"create":"false","read":"false","update":"false","delete":"false"},"rolemanagement":{"create":"false","read":"false","update":"false","delete":"false"},"appointments":{"create":"true","read":"true","update":"true","delete":"true"},"doctors":{"create":"true","read":"true","update":"true","delete":"true"},"patients":{"create":"true","read":"true","update":"true","delete":"true"},"procedures":{"create":"false","read":"true","update":"false","delete":"false"},"labtracking":{"create":"true","read":"true","update":"true","delete":"true"},"reports":{"create":"false","read":"true","update":"false","delete":"false"},"treatments":{"create":"true","read":"true","update":"true","delete":"true"},"invoices":{"create":"true","read":"true","update":"true","delete":"true"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"true"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"true"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"false","update":"false","delete":"false"},"worktype":{"create":"true","read":"true","update":"true","delete":"true"},"work":{"create":"true","read":"true","update":"true","delete":"true"},"suppliers":{"create":"true","read":"true","update":"true","delete":"true"},"lab":{"create":"true","read":"true","update":"true","delete":"true"},"site":{"create":"true","read":"true","update":"true","delete":"false"},"patientfiles":{"create":"false","read":"false","update":"false","delete":"false"},"expenses":{"create":"false","read":"false","update":"false","delete":"false"},"purchases":{"create":"false","read":"false","update":"false","delete":"false"},"items":{"create":"false","read":"false","update":"false","delete":"false","issue":"false"},"stocks":{"create":"false","read":"false","update":"false","delete":"false"},"accounts":{"create":"false","read":"false","update":"false","delete":"false"},"purchasedetails":{"create":"false","read":"false","update":"false","delete":"false"},"stockreleases":{"create":"false","read":"false","update":"false","delete":"false"},"employeepayroll":{"create":"false","read":"false","update":"false","delete":"false"},"employeeattendance":{"create":"true","read":"true","update":"true","delete":"true"},"tokens":{"create":"true","read":"true","update":"true","delete":"true"},"backup":{"create":"false","read":"false","update":"false","delete":"false"},"reporttype":{"create":"false","read":"false","update":"false","delete":"false"},"":{"create":"false","read":"false","update":"false","delete":"false"}}";s:14:"doctor_role_id";s:1:"4";s:10:"role_title";s:5:"Admin";}'),
('7599104749c774bec1a591b3977ff059', '192.168.10.5', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 1558031549, 'a:13:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"rafay";s:15:"admin_full_name";s:11:"Abdur Rafay";s:8:"admin_id";s:1:"6";s:10:"last_login";s:19:"2019-05-16 21:15:15";s:7:"last_ip";s:12:"192.168.10.5";s:12:"user_role_id";s:1:"2";s:9:"clinic_id";s:1:"1";s:10:"permission";s:3494:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"clinics":{"create":"false","read":"false","update":"false","delete":"false"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"appointments":{"create":"true","read":"true","update":"true","delete":"true"},"patients":{"create":"true","read":"true","update":"true","delete":"true","profile":"true"},"procedures":{"create":"true","read":"true","update":"true","delete":"true"},"labtracking":{"create":"true","read":"true","update":"true","delete":"true"},"reports":{"create":"false","read":"true","update":"false","delete":"false"},"treatments":{"create":"true","read":"true","update":"true","delete":"true"},"invoices":{"create":"true","read":"true","update":"true","delete":"true"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"true"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"true"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"worktype":{"create":"true","read":"true","update":"true","delete":"true"},"work":{"create":"true","read":"true","update":"true","delete":"true"},"suppliers":{"create":"true","read":"true","update":"true","delete":"true"},"lab":{"create":"true","read":"true","update":"true","delete":"true"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"patientfiles":{"create":"true","read":"true","update":"true","delete":"true"},"expenses":{"create":"true","read":"true","update":"true","delete":"true"},"purchases":{"create":"true","read":"true","update":"true","delete":"true"},"items":{"create":"true","read":"true","update":"true","delete":"true","issue":"true"},"stocks":{"create":"true","read":"true","update":"true","delete":"true"},"accounts":{"create":"true","read":"true","update":"true","delete":"true"},"purchasedetails":{"create":"true","read":"true","update":"true","delete":"true"},"stockreleases":{"create":"true","read":"true","update":"true","delete":"true"},"employeepayroll":{"create":"true","read":"true","update":"true","delete":"true"},"employeeattendance":{"create":"true","read":"true","update":"true","delete":"true"},"tokens":{"create":"true","read":"true","update":"true","delete":"true"},"backup":{"create":"false","read":"true","update":"false","delete":"false"},"reporttype":{"create":"true","read":"true","update":"true","delete":"true"},"":{"create":"false","read":"false","update":"false","delete":"false"},"roomtype":{"create":"true","read":"true","update":"true","delete":"true"},"room":{"create":"true","read":"true","update":"true","delete":"true"},"bedtype":{"create":"true","read":"true","update":"true","delete":"true"},"bed":{"create":"true","read":"true","update":"true","delete":"true"},"bedpatient":{"create":"true","read":"true","update":"true","delete":"true"},"roompatient":{"create":"true","read":"true","update":"true","delete":"true"},"doctordays":{"create":"true","read":"true","update":"true","delete":"true"},"patientprocedures":{"create":"true","read":"true","update":"true","delete":"true"},"facilities":{"create":"true","read":"true","update":"true","delete":"true"},"patientcase":{"create":"true","read":"true","update":"true","delete":"true"},"relations":{"create":"true","read":"true","update":"true","delete":"true"},"doctors":{"create":"true","read":"true","update":"true","delete":"true"}}";s:14:"doctor_role_id";s:1:"4";s:10:"role_title";s:5:"Admin";}'),
('7669be1298c4b2018ce26ef90c1b2754', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', 1552179402, ''),
('7ab2df3f45404d3bc48c2111425304dc', '::1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.75 Safari/537.36', 1461044832, 'a:11:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"admin";s:15:"admin_full_name";s:5:"5-STI";s:8:"admin_id";s:1:"1";s:10:"last_login";s:19:"2016-04-18 15:35:31";s:7:"last_ip";s:3:"::1";s:10:"permission";s:1714:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"customers":{"create":"true","read":"true","update":"true","delete":"true"},"orders":{"create":"true","read":"true","update":"true","delete":"true","history":"false","fullaccess":"false"},"payments":{"create":"false","read":"false","update":"false","delete":"false"},"invoice":{"create":"true","read":"true","update":"true","delete":"true"},"menu":{"create":"true","read":"true","update":"true","delete":"true"},"pages":{"create":"true","read":"true","update":"true","delete":"true"},"about":{"create":"true","read":"true","update":"true","delete":"true"},"services":{"create":"true","read":"true","update":"true","delete":"true"},"gallery":{"create":"true","read":"true","update":"true","delete":"true"},"testimonial":{"create":"true","read":"true","update":"true","delete":"true"},"sliders":{"create":"true","read":"true","update":"true","delete":"true"},"slider":{"create":"true","read":"true","update":"true","delete":"true"},"products":{"create":"true","read":"true","update":"true","delete":"true"},"productcategory":{"create":"true","read":"true","update":"true","delete":"true"},"sales":{"create":"false","read":"true","update":"false","delete":"true","history":"true","fullaccess":"true"},"contact":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"payment":{"create":"true","read":"true","update":"true","delete":"true","history":"true","fullaccess":"true"}}";s:10:"role_title";s:5:"Admin";s:8:"per_page";s:3:"100";}'),
('7f87aad2ef7e152bac05412dbdaa64cf', '192.168.10.17', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36', 1460729877, ''),
('8347a633655f53e213e4e12401f44733', '192.168.10.5', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 1558031549, 'a:13:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"rafay";s:15:"admin_full_name";s:11:"Abdur Rafay";s:8:"admin_id";s:1:"6";s:10:"last_login";s:19:"2019-05-16 21:13:52";s:7:"last_ip";s:12:"192.168.10.6";s:12:"user_role_id";s:1:"2";s:9:"clinic_id";s:1:"1";s:10:"permission";s:3494:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"clinics":{"create":"false","read":"false","update":"false","delete":"false"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"appointments":{"create":"true","read":"true","update":"true","delete":"true"},"patients":{"create":"true","read":"true","update":"true","delete":"true","profile":"true"},"procedures":{"create":"true","read":"true","update":"true","delete":"true"},"labtracking":{"create":"true","read":"true","update":"true","delete":"true"},"reports":{"create":"false","read":"true","update":"false","delete":"false"},"treatments":{"create":"true","read":"true","update":"true","delete":"true"},"invoices":{"create":"true","read":"true","update":"true","delete":"true"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"true"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"true"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"worktype":{"create":"true","read":"true","update":"true","delete":"true"},"work":{"create":"true","read":"true","update":"true","delete":"true"},"suppliers":{"create":"true","read":"true","update":"true","delete":"true"},"lab":{"create":"true","read":"true","update":"true","delete":"true"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"patientfiles":{"create":"true","read":"true","update":"true","delete":"true"},"expenses":{"create":"true","read":"true","update":"true","delete":"true"},"purchases":{"create":"true","read":"true","update":"true","delete":"true"},"items":{"create":"true","read":"true","update":"true","delete":"true","issue":"true"},"stocks":{"create":"true","read":"true","update":"true","delete":"true"},"accounts":{"create":"true","read":"true","update":"true","delete":"true"},"purchasedetails":{"create":"true","read":"true","update":"true","delete":"true"},"stockreleases":{"create":"true","read":"true","update":"true","delete":"true"},"employeepayroll":{"create":"true","read":"true","update":"true","delete":"true"},"employeeattendance":{"create":"true","read":"true","update":"true","delete":"true"},"tokens":{"create":"true","read":"true","update":"true","delete":"true"},"backup":{"create":"false","read":"true","update":"false","delete":"false"},"reporttype":{"create":"true","read":"true","update":"true","delete":"true"},"":{"create":"false","read":"false","update":"false","delete":"false"},"roomtype":{"create":"true","read":"true","update":"true","delete":"true"},"room":{"create":"true","read":"true","update":"true","delete":"true"},"bedtype":{"create":"true","read":"true","update":"true","delete":"true"},"bed":{"create":"true","read":"true","update":"true","delete":"true"},"bedpatient":{"create":"true","read":"true","update":"true","delete":"true"},"roompatient":{"create":"true","read":"true","update":"true","delete":"true"},"doctordays":{"create":"true","read":"true","update":"true","delete":"true"},"patientprocedures":{"create":"true","read":"true","update":"true","delete":"true"},"facilities":{"create":"true","read":"true","update":"true","delete":"true"},"patientcase":{"create":"true","read":"true","update":"true","delete":"true"},"relations":{"create":"true","read":"true","update":"true","delete":"true"},"doctors":{"create":"true","read":"true","update":"true","delete":"true"}}";s:14:"doctor_role_id";s:1:"4";s:10:"role_title";s:5:"Admin";}'),
('87a4210227a5904b3b03623276ae99a0', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 1557847371, 'a:12:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"admin";s:15:"admin_full_name";s:15:"Talib Allauddin";s:8:"admin_id";s:1:"1";s:10:"last_login";s:19:"2019-04-13 14:41:14";s:7:"last_ip";s:3:"::1";s:12:"user_role_id";s:1:"1";s:9:"clinic_id";s:1:"0";s:10:"permission";s:2412:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"clinics":{"create":"true","read":"true","update":"true","delete":"true"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"appointments":{"create":"true","read":"true","update":"true","delete":"true"},"doctors":{"create":"true","read":"true","update":"true","delete":"true"},"patients":{"create":"true","read":"true","update":"true","delete":"true"},"procedures":{"create":"true","read":"true","update":"true","delete":"true"},"labtracking":{"create":"true","read":"true","update":"true","delete":"true"},"reports":{"create":"false","read":"true","update":"false","delete":"false"},"treatments":{"create":"true","read":"true","update":"true","delete":"true"},"invoices":{"create":"true","read":"true","update":"true","delete":"true"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"true"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"true"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"worktype":{"create":"true","read":"true","update":"true","delete":"true"},"work":{"create":"true","read":"true","update":"true","delete":"true"},"suppliers":{"create":"true","read":"true","update":"true","delete":"true"},"lab":{"create":"true","read":"true","update":"true","delete":"true"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"patientfiles":{"create":"true","read":"true","update":"true","delete":"true"},"expenses":{"create":"true","read":"true","update":"true","delete":"true"},"purchases":{"create":"true","read":"true","update":"true","delete":"true"},"items":{"create":"true","read":"true","update":"true","delete":"true","issue":"true"},"stocks":{"create":"true","read":"true","update":"true","delete":"true"},"accounts":{"create":"true","read":"true","update":"true","delete":"true"},"purchasedetails":{"create":"true","read":"true","update":"true","delete":"true"},"stockreleases":{"create":"true","read":"true","update":"true","delete":"true"},"employeepayroll":{"create":"true","read":"true","update":"true","delete":"true"},"employeeattendance":{"create":"true","read":"true","update":"true","delete":"true"},"tokens":{"create":"true","read":"true","update":"true","delete":"true"}}";s:10:"role_title";s:11:"Super Admin";}'),
('8860cb1d98c5a7d23f1232bf3756ebf1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 1557938706, 'a:13:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"rafay";s:15:"admin_full_name";s:11:"Abdur Rafay";s:8:"admin_id";s:1:"6";s:10:"last_login";s:19:"2019-05-13 21:24:31";s:7:"last_ip";s:3:"::1";s:12:"user_role_id";s:1:"2";s:9:"clinic_id";s:1:"1";s:10:"permission";s:3263:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"clinics":{"create":"false","read":"false","update":"false","delete":"false"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"appointments":{"create":"true","read":"true","update":"true","delete":"true"},"patients":{"create":"true","read":"true","update":"true","delete":"true","profile":"true"},"procedures":{"create":"true","read":"true","update":"true","delete":"true"},"labtracking":{"create":"true","read":"true","update":"true","delete":"true"},"reports":{"create":"false","read":"true","update":"false","delete":"false"},"treatments":{"create":"true","read":"true","update":"true","delete":"true"},"invoices":{"create":"true","read":"true","update":"true","delete":"true"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"true"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"true"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"worktype":{"create":"true","read":"true","update":"true","delete":"true"},"work":{"create":"true","read":"true","update":"true","delete":"true"},"suppliers":{"create":"true","read":"true","update":"true","delete":"true"},"lab":{"create":"true","read":"true","update":"true","delete":"true"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"patientfiles":{"create":"true","read":"true","update":"true","delete":"true"},"expenses":{"create":"true","read":"true","update":"true","delete":"true"},"purchases":{"create":"true","read":"true","update":"true","delete":"true"},"items":{"create":"true","read":"true","update":"true","delete":"true","issue":"true"},"stocks":{"create":"true","read":"true","update":"true","delete":"true"},"accounts":{"create":"true","read":"true","update":"true","delete":"true"},"purchasedetails":{"create":"true","read":"true","update":"true","delete":"true"},"stockreleases":{"create":"true","read":"true","update":"true","delete":"true"},"employeepayroll":{"create":"true","read":"true","update":"true","delete":"true"},"employeeattendance":{"create":"true","read":"true","update":"true","delete":"true"},"tokens":{"create":"true","read":"true","update":"true","delete":"true"},"backup":{"create":"false","read":"true","update":"false","delete":"false"},"reporttype":{"create":"true","read":"true","update":"true","delete":"true"},"":{"create":"false","read":"false","update":"false","delete":"false"},"roomtype":{"create":"true","read":"true","update":"true","delete":"true"},"room":{"create":"true","read":"true","update":"true","delete":"true"},"bedtype":{"create":"true","read":"true","update":"true","delete":"true"},"bed":{"create":"true","read":"true","update":"true","delete":"true"},"bedpatient":{"create":"true","read":"true","update":"true","delete":"true"},"roompatient":{"create":"true","read":"true","update":"true","delete":"true"},"doctordays":{"create":"true","read":"true","update":"true","delete":"true"},"patientprocedures":{"create":"true","read":"true","update":"true","delete":"true"},"doctors":{"create":"true","read":"true","update":"true","delete":"true"}}";s:14:"doctor_role_id";s:1:"4";s:10:"role_title";s:5:"Admin";}'),
('8b36bf71d3bedc469ffff1daf0ea1dd3', '192.168.10.11', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 1556870553, 'a:13:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"admin";s:15:"admin_full_name";s:15:"Talib Allauddin";s:8:"admin_id";s:1:"1";s:10:"last_login";s:19:"2019-05-03 06:57:03";s:7:"last_ip";s:3:"::1";s:12:"user_role_id";s:1:"1";s:9:"clinic_id";s:1:"0";s:10:"permission";s:2565:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"clinics":{"create":"true","read":"true","update":"true","delete":"true"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"appointments":{"create":"true","read":"true","update":"true","delete":"true"},"doctors":{"create":"true","read":"true","update":"true","delete":"true"},"patients":{"create":"true","read":"true","update":"true","delete":"true"},"procedures":{"create":"true","read":"true","update":"true","delete":"true"},"labtracking":{"create":"true","read":"true","update":"true","delete":"true"},"reports":{"create":"false","read":"true","update":"false","delete":"false"},"treatments":{"create":"true","read":"true","update":"true","delete":"true"},"invoices":{"create":"true","read":"true","update":"true","delete":"true"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"true"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"true"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"worktype":{"create":"true","read":"true","update":"true","delete":"true"},"work":{"create":"true","read":"true","update":"true","delete":"true"},"suppliers":{"create":"true","read":"true","update":"true","delete":"true"},"lab":{"create":"true","read":"true","update":"true","delete":"true"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"patientfiles":{"create":"true","read":"true","update":"true","delete":"true"},"expenses":{"create":"true","read":"true","update":"true","delete":"true"},"purchases":{"create":"true","read":"true","update":"true","delete":"true"},"items":{"create":"true","read":"true","update":"true","delete":"true","issue":"true"},"stocks":{"create":"true","read":"true","update":"true","delete":"true"},"accounts":{"create":"true","read":"true","update":"true","delete":"true"},"purchasedetails":{"create":"true","read":"true","update":"true","delete":"true"},"stockreleases":{"create":"true","read":"true","update":"true","delete":"true"},"employeepayroll":{"create":"true","read":"true","update":"true","delete":"true"},"employeeattendance":{"create":"true","read":"true","update":"true","delete":"true"},"tokens":{"create":"true","read":"true","update":"true","delete":"true"},"backup":{"create":"false","read":"true","update":"false","delete":"false"},"reporttype":{"create":"true","read":"true","update":"true","delete":"true"}}";s:14:"doctor_role_id";s:1:"4";s:10:"role_title";s:11:"Super Admin";}'),
('8f35d8e416169e339eb14229b439c06e', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36', 1460288572, 'a:8:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";s:11:"Super Admin";s:15:"admin_user_name";s:5:"admin";s:15:"admin_full_name";s:5:"5-STI";s:8:"admin_id";s:1:"1";s:10:"last_login";s:19:"2016-03-25 03:19:18";s:7:"last_ip";s:3:"::1";}'),
('8f6f56aa378411ad81b2c0e1de4f4a4c', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', 1554939586, 'a:12:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"admin";s:15:"admin_full_name";s:15:"Talib Allauddin";s:8:"admin_id";s:1:"1";s:10:"last_login";s:19:"2019-04-10 10:34:58";s:7:"last_ip";s:3:"::1";s:12:"user_role_id";s:1:"1";s:9:"clinic_id";s:1:"0";s:10:"permission";s:2339:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"clinics":{"create":"true","read":"true","update":"true","delete":"true"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"appointments":{"create":"true","read":"true","update":"true","delete":"true"},"doctors":{"create":"true","read":"true","update":"true","delete":"true"},"patients":{"create":"true","read":"true","update":"true","delete":"true"},"procedures":{"create":"true","read":"true","update":"true","delete":"true"},"labtracking":{"create":"true","read":"true","update":"true","delete":"true"},"reports":{"create":"false","read":"true","update":"false","delete":"false"},"treatments":{"create":"true","read":"true","update":"true","delete":"true"},"invoices":{"create":"true","read":"true","update":"true","delete":"true"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"true"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"true"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"worktype":{"create":"true","read":"true","update":"true","delete":"true"},"work":{"create":"true","read":"true","update":"true","delete":"true"},"suppliers":{"create":"true","read":"true","update":"true","delete":"true"},"lab":{"create":"true","read":"true","update":"true","delete":"true"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"patientfiles":{"create":"true","read":"true","update":"true","delete":"true"},"expenses":{"create":"true","read":"true","update":"true","delete":"true"},"purchases":{"create":"true","read":"true","update":"true","delete":"true"},"items":{"create":"true","read":"true","update":"true","delete":"true","issue":"true"},"stocks":{"create":"true","read":"true","update":"true","delete":"true"},"accounts":{"create":"true","read":"true","update":"true","delete":"true"},"purchasedetails":{"create":"true","read":"true","update":"true","delete":"true"},"stockreleases":{"create":"true","read":"true","update":"true","delete":"true"},"employeepayroll":{"create":"true","read":"true","update":"true","delete":"true"},"employeeattendance":{"create":"true","read":"true","update":"true","delete":"true"}}";s:10:"role_title";s:11:"Super Admin";}'),
('99468ba8d481f85fbf24b9657e96c313', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 1555150342, 'a:12:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"admin";s:15:"admin_full_name";s:15:"Talib Allauddin";s:8:"admin_id";s:1:"1";s:10:"last_login";s:19:"2019-04-13 08:18:09";s:7:"last_ip";s:3:"::1";s:12:"user_role_id";s:1:"1";s:9:"clinic_id";s:1:"0";s:10:"permission";s:2412:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"clinics":{"create":"true","read":"true","update":"true","delete":"true"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"appointments":{"create":"true","read":"true","update":"true","delete":"true"},"doctors":{"create":"true","read":"true","update":"true","delete":"true"},"patients":{"create":"true","read":"true","update":"true","delete":"true"},"procedures":{"create":"true","read":"true","update":"true","delete":"true"},"labtracking":{"create":"true","read":"true","update":"true","delete":"true"},"reports":{"create":"false","read":"true","update":"false","delete":"false"},"treatments":{"create":"true","read":"true","update":"true","delete":"true"},"invoices":{"create":"true","read":"true","update":"true","delete":"true"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"true"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"true"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"worktype":{"create":"true","read":"true","update":"true","delete":"true"},"work":{"create":"true","read":"true","update":"true","delete":"true"},"suppliers":{"create":"true","read":"true","update":"true","delete":"true"},"lab":{"create":"true","read":"true","update":"true","delete":"true"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"patientfiles":{"create":"true","read":"true","update":"true","delete":"true"},"expenses":{"create":"true","read":"true","update":"true","delete":"true"},"purchases":{"create":"true","read":"true","update":"true","delete":"true"},"items":{"create":"true","read":"true","update":"true","delete":"true","issue":"true"},"stocks":{"create":"true","read":"true","update":"true","delete":"true"},"accounts":{"create":"true","read":"true","update":"true","delete":"true"},"purchasedetails":{"create":"true","read":"true","update":"true","delete":"true"},"stockreleases":{"create":"true","read":"true","update":"true","delete":"true"},"employeepayroll":{"create":"true","read":"true","update":"true","delete":"true"},"employeeattendance":{"create":"true","read":"true","update":"true","delete":"true"},"tokens":{"create":"true","read":"true","update":"true","delete":"true"}}";s:10:"role_title";s:11:"Super Admin";}'),
('9a16a1192c0ec744ff472a5d99976eda', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 1555051042, 'a:12:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"admin";s:15:"admin_full_name";s:15:"Talib Allauddin";s:8:"admin_id";s:1:"1";s:10:"last_login";s:19:"2019-04-11 14:20:18";s:7:"last_ip";s:3:"::1";s:12:"user_role_id";s:1:"1";s:9:"clinic_id";s:1:"0";s:10:"permission";s:2412:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"clinics":{"create":"true","read":"true","update":"true","delete":"true"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"appointments":{"create":"true","read":"true","update":"true","delete":"true"},"doctors":{"create":"true","read":"true","update":"true","delete":"true"},"patients":{"create":"true","read":"true","update":"true","delete":"true"},"procedures":{"create":"true","read":"true","update":"true","delete":"true"},"labtracking":{"create":"true","read":"true","update":"true","delete":"true"},"reports":{"create":"false","read":"true","update":"false","delete":"false"},"treatments":{"create":"true","read":"true","update":"true","delete":"true"},"invoices":{"create":"true","read":"true","update":"true","delete":"true"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"true"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"true"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"worktype":{"create":"true","read":"true","update":"true","delete":"true"},"work":{"create":"true","read":"true","update":"true","delete":"true"},"suppliers":{"create":"true","read":"true","update":"true","delete":"true"},"lab":{"create":"true","read":"true","update":"true","delete":"true"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"patientfiles":{"create":"true","read":"true","update":"true","delete":"true"},"expenses":{"create":"true","read":"true","update":"true","delete":"true"},"purchases":{"create":"true","read":"true","update":"true","delete":"true"},"items":{"create":"true","read":"true","update":"true","delete":"true","issue":"true"},"stocks":{"create":"true","read":"true","update":"true","delete":"true"},"accounts":{"create":"true","read":"true","update":"true","delete":"true"},"purchasedetails":{"create":"true","read":"true","update":"true","delete":"true"},"stockreleases":{"create":"true","read":"true","update":"true","delete":"true"},"employeepayroll":{"create":"true","read":"true","update":"true","delete":"true"},"employeeattendance":{"create":"true","read":"true","update":"true","delete":"true"},"tokens":{"create":"true","read":"true","update":"true","delete":"true"}}";s:10:"role_title";s:11:"Super Admin";}');
INSERT INTO `ci_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('9c02cd9a893d8d0ff943c9dfb158b803', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', 1554623038, 'a:12:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"admin";s:15:"admin_full_name";s:15:"Talib Allauddin";s:8:"admin_id";s:1:"1";s:10:"last_login";s:19:"2019-04-07 10:26:00";s:7:"last_ip";s:3:"::1";s:12:"user_role_id";s:1:"1";s:9:"clinic_id";s:1:"0";s:10:"permission";s:2324:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"clinics":{"create":"true","read":"true","update":"true","delete":"true"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"appointments":{"create":"true","read":"true","update":"true","delete":"true"},"doctors":{"create":"true","read":"true","update":"true","delete":"true"},"patients":{"create":"true","read":"true","update":"true","delete":"true"},"procedures":{"create":"true","read":"true","update":"true","delete":"true"},"labtracking":{"create":"true","read":"true","update":"true","delete":"true"},"reports":{"create":"false","read":"true","update":"false","delete":"false"},"treatments":{"create":"true","read":"true","update":"true","delete":"true"},"invoices":{"create":"true","read":"true","update":"true","delete":"true"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"true"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"true"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"worktype":{"create":"true","read":"true","update":"true","delete":"true"},"work":{"create":"true","read":"true","update":"true","delete":"true"},"suppliers":{"create":"true","read":"true","update":"true","delete":"true"},"lab":{"create":"true","read":"true","update":"true","delete":"true"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"patientfiles":{"create":"true","read":"true","update":"true","delete":"true"},"expenses":{"create":"true","read":"true","update":"true","delete":"true"},"purchases":{"create":"true","read":"true","update":"true","delete":"true"},"items":{"create":"true","read":"true","update":"true","delete":"true"},"stocks":{"create":"true","read":"true","update":"true","delete":"true"},"accounts":{"create":"true","read":"true","update":"true","delete":"true"},"purchasedetails":{"create":"true","read":"true","update":"true","delete":"true"},"stockreleases":{"create":"true","read":"true","update":"true","delete":"true"},"employeepayroll":{"create":"true","read":"true","update":"true","delete":"true"},"employeeattendance":{"create":"true","read":"true","update":"true","delete":"true"}}";s:10:"role_title";s:11:"Super Admin";}'),
('9e69f66b0d04bd2579dcacc0e072af56', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 1555489465, ''),
('a202a0befb95ed4ef301e5ccb041b3e8', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 1556297894, 'a:13:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"rafay";s:15:"admin_full_name";s:11:"Abdur Rafay";s:8:"admin_id";s:1:"6";s:10:"last_login";s:19:"2019-04-25 17:46:19";s:7:"last_ip";s:3:"::1";s:12:"user_role_id";s:1:"2";s:9:"clinic_id";s:1:"1";s:10:"permission";s:3166:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"clinics":{"create":"false","read":"false","update":"false","delete":"false"},"rolemanagement":{"create":"false","read":"false","update":"false","delete":"false"},"appointments":{"create":"true","read":"true","update":"true","delete":"true"},"patients":{"create":"true","read":"true","update":"true","delete":"true"},"procedures":{"create":"true","read":"true","update":"true","delete":"true"},"labtracking":{"create":"true","read":"true","update":"true","delete":"true"},"reports":{"create":"false","read":"true","update":"false","delete":"false"},"treatments":{"create":"true","read":"true","update":"true","delete":"true"},"invoices":{"create":"true","read":"true","update":"true","delete":"true"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"true"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"true"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"worktype":{"create":"true","read":"true","update":"true","delete":"true"},"work":{"create":"true","read":"true","update":"true","delete":"true"},"suppliers":{"create":"true","read":"true","update":"true","delete":"true"},"lab":{"create":"true","read":"true","update":"true","delete":"true"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"patientfiles":{"create":"true","read":"true","update":"true","delete":"true"},"expenses":{"create":"true","read":"true","update":"true","delete":"true"},"purchases":{"create":"true","read":"true","update":"true","delete":"true"},"items":{"create":"true","read":"true","update":"true","delete":"true","issue":"true"},"stocks":{"create":"true","read":"true","update":"true","delete":"true"},"accounts":{"create":"true","read":"true","update":"true","delete":"true"},"purchasedetails":{"create":"true","read":"true","update":"true","delete":"true"},"stockreleases":{"create":"true","read":"true","update":"true","delete":"true"},"employeepayroll":{"create":"true","read":"true","update":"true","delete":"true"},"employeeattendance":{"create":"true","read":"true","update":"true","delete":"true"},"tokens":{"create":"true","read":"true","update":"true","delete":"true"},"backup":{"create":"false","read":"true","update":"false","delete":"false"},"reporttype":{"create":"true","read":"true","update":"true","delete":"true"},"":{"create":"false","read":"false","update":"false","delete":"false"},"roomtype":{"create":"true","read":"true","update":"true","delete":"true"},"room":{"create":"true","read":"true","update":"true","delete":"true"},"bedtype":{"create":"true","read":"true","update":"true","delete":"true"},"bed":{"create":"true","read":"true","update":"true","delete":"true"},"bedpatient":{"create":"true","read":"true","update":"true","delete":"true"},"roompatient":{"create":"true","read":"true","update":"true","delete":"true"},"doctordays":{"create":"true","read":"true","update":"true","delete":"true"},"doctors":{"create":"true","read":"true","update":"true","delete":"true"}}";s:14:"doctor_role_id";s:1:"4";s:10:"role_title";s:5:"Admin";}'),
('a29ee5f99619e6ee35140c4c6ab17cab', '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36', 1460332334, ''),
('a43c15f4da4863361d99943f86483a49', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 1555688973, 'a:13:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"rafay";s:15:"admin_full_name";s:11:"Abdur Rafay";s:8:"admin_id";s:1:"6";s:10:"last_login";s:19:"2019-04-19 10:35:44";s:7:"last_ip";s:3:"::1";s:12:"user_role_id";s:1:"2";s:9:"clinic_id";s:1:"1";s:10:"permission";s:2692:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"false"},"clinics":{"create":"false","read":"false","update":"false","delete":"false"},"rolemanagement":{"create":"false","read":"false","update":"false","delete":"false"},"appointments":{"create":"true","read":"true","update":"true","delete":"true"},"doctors":{"create":"true","read":"true","update":"true","delete":"true"},"patients":{"create":"true","read":"true","update":"true","delete":"true"},"procedures":{"create":"false","read":"true","update":"false","delete":"false"},"labtracking":{"create":"true","read":"true","update":"true","delete":"true"},"reports":{"create":"false","read":"true","update":"false","delete":"false"},"treatments":{"create":"true","read":"true","update":"true","delete":"true"},"invoices":{"create":"true","read":"true","update":"true","delete":"true"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"true"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"true"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"false","update":"false","delete":"false"},"worktype":{"create":"true","read":"true","update":"true","delete":"true"},"work":{"create":"true","read":"true","update":"true","delete":"true"},"suppliers":{"create":"true","read":"true","update":"true","delete":"true"},"lab":{"create":"true","read":"true","update":"true","delete":"true"},"site":{"create":"true","read":"true","update":"true","delete":"false"},"patientfiles":{"create":"false","read":"false","update":"false","delete":"false"},"expenses":{"create":"false","read":"false","update":"false","delete":"false"},"purchases":{"create":"false","read":"false","update":"false","delete":"false"},"items":{"create":"false","read":"false","update":"false","delete":"false","issue":"false"},"stocks":{"create":"false","read":"false","update":"false","delete":"false"},"accounts":{"create":"false","read":"false","update":"false","delete":"false"},"purchasedetails":{"create":"false","read":"false","update":"false","delete":"false"},"stockreleases":{"create":"false","read":"false","update":"false","delete":"false"},"employeepayroll":{"create":"false","read":"false","update":"false","delete":"false"},"employeeattendance":{"create":"true","read":"true","update":"true","delete":"true"},"tokens":{"create":"true","read":"true","update":"true","delete":"true"},"backup":{"create":"false","read":"false","update":"false","delete":"false"},"reporttype":{"create":"false","read":"false","update":"false","delete":"false"},"":{"create":"false","read":"false","update":"false","delete":"false"}}";s:14:"doctor_role_id";s:1:"4";s:10:"role_title";s:5:"Admin";}'),
('a4d9fe60fc1e554c065aa03275f8dbdc', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 1555583815, 'a:12:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"rafay";s:15:"admin_full_name";s:11:"Abdur Rafay";s:8:"admin_id";s:1:"6";s:10:"last_login";s:19:"2019-04-18 15:15:19";s:7:"last_ip";s:3:"::1";s:12:"user_role_id";s:1:"2";s:9:"clinic_id";s:1:"1";s:10:"permission";s:1559:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"false"},"clinics":{"create":"false","read":"false","update":"false","delete":"false"},"rolemanagement":{"create":"false","read":"false","update":"false","delete":"false"},"appointments":{"create":"true","read":"true","update":"true","delete":"true"},"doctors":{"create":"true","read":"true","update":"true","delete":"true"},"patients":{"create":"true","read":"true","update":"true","delete":"true"},"procedures":{"create":"false","read":"true","update":"false","delete":"false"},"labtracking":{"create":"true","read":"true","update":"true","delete":"true"},"reports":{"create":"false","read":"true","update":"false","delete":"false"},"treatments":{"create":"true","read":"true","update":"true","delete":"true"},"invoices":{"create":"true","read":"true","update":"true","delete":"true"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"true"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"true"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"false","update":"false","delete":"false"},"worktype":{"create":"true","read":"true","update":"true","delete":"true"},"work":{"create":"true","read":"true","update":"true","delete":"true"},"suppliers":{"create":"true","read":"true","update":"true","delete":"true"},"lab":{"create":"true","read":"true","update":"true","delete":"true"},"site":{"create":"true","read":"true","update":"true","delete":"false"}}";s:10:"role_title";s:5:"Admin";}'),
('a6644b32c443c77698d457ce413e129d', '192.168.0.102', 'Mozilla/5.0 (Linux; Android 6.0.1; SM-T800) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.90 Safari/537.36', 1555575310, 'a:12:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"admin";s:15:"admin_full_name";s:15:"Talib Allauddin";s:8:"admin_id";s:1:"1";s:10:"last_login";s:19:"2019-04-18 10:25:31";s:7:"last_ip";s:3:"::1";s:12:"user_role_id";s:1:"1";s:9:"clinic_id";s:1:"0";s:10:"permission";s:2565:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"clinics":{"create":"true","read":"true","update":"true","delete":"true"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"appointments":{"create":"true","read":"true","update":"true","delete":"true"},"doctors":{"create":"true","read":"true","update":"true","delete":"true"},"patients":{"create":"true","read":"true","update":"true","delete":"true"},"procedures":{"create":"true","read":"true","update":"true","delete":"true"},"labtracking":{"create":"true","read":"true","update":"true","delete":"true"},"reports":{"create":"false","read":"true","update":"false","delete":"false"},"treatments":{"create":"true","read":"true","update":"true","delete":"true"},"invoices":{"create":"true","read":"true","update":"true","delete":"true"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"true"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"true"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"worktype":{"create":"true","read":"true","update":"true","delete":"true"},"work":{"create":"true","read":"true","update":"true","delete":"true"},"suppliers":{"create":"true","read":"true","update":"true","delete":"true"},"lab":{"create":"true","read":"true","update":"true","delete":"true"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"patientfiles":{"create":"true","read":"true","update":"true","delete":"true"},"expenses":{"create":"true","read":"true","update":"true","delete":"true"},"purchases":{"create":"true","read":"true","update":"true","delete":"true"},"items":{"create":"true","read":"true","update":"true","delete":"true","issue":"true"},"stocks":{"create":"true","read":"true","update":"true","delete":"true"},"accounts":{"create":"true","read":"true","update":"true","delete":"true"},"purchasedetails":{"create":"true","read":"true","update":"true","delete":"true"},"stockreleases":{"create":"true","read":"true","update":"true","delete":"true"},"employeepayroll":{"create":"true","read":"true","update":"true","delete":"true"},"employeeattendance":{"create":"true","read":"true","update":"true","delete":"true"},"tokens":{"create":"true","read":"true","update":"true","delete":"true"},"backup":{"create":"false","read":"true","update":"false","delete":"false"},"reporttype":{"create":"true","read":"true","update":"true","delete":"true"}}";s:10:"role_title";s:11:"Super Admin";}'),
('a6cf62c94ae56ff23c2ba282ddfda010', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', 1553503739, 'a:12:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:6:"alhaaj";s:15:"admin_full_name";s:6:"alhaaj";s:8:"admin_id";s:1:"3";s:10:"last_login";s:19:"2019-03-22 17:08:42";s:7:"last_ip";s:3:"::1";s:12:"user_role_id";s:1:"2";s:9:"clinic_id";s:1:"1";s:10:"permission";s:1559:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"false"},"clinics":{"create":"false","read":"false","update":"false","delete":"false"},"rolemanagement":{"create":"false","read":"false","update":"false","delete":"false"},"appointments":{"create":"true","read":"true","update":"true","delete":"true"},"doctors":{"create":"true","read":"true","update":"true","delete":"true"},"patients":{"create":"true","read":"true","update":"true","delete":"true"},"procedures":{"create":"false","read":"true","update":"false","delete":"false"},"labtracking":{"create":"true","read":"true","update":"true","delete":"true"},"reports":{"create":"false","read":"true","update":"false","delete":"false"},"treatments":{"create":"true","read":"true","update":"true","delete":"true"},"invoices":{"create":"true","read":"true","update":"true","delete":"true"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"true"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"true"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"false","update":"false","delete":"false"},"worktype":{"create":"true","read":"true","update":"true","delete":"true"},"work":{"create":"true","read":"true","update":"true","delete":"true"},"suppliers":{"create":"true","read":"true","update":"true","delete":"true"},"lab":{"create":"true","read":"true","update":"true","delete":"true"},"site":{"create":"true","read":"true","update":"true","delete":"false"}}";s:10:"role_title";s:5:"Admin";}'),
('ab88440448605c5ba2aa93f21ca7b44d', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', 1553514987, 'a:12:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"admin";s:15:"admin_full_name";s:15:"Talib Allauddin";s:8:"admin_id";s:1:"1";s:10:"last_login";s:19:"2019-03-23 04:48:54";s:7:"last_ip";s:3:"::1";s:12:"user_role_id";s:1:"1";s:9:"clinic_id";s:1:"0";s:10:"permission";s:1463:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"clinics":{"create":"true","read":"true","update":"true","delete":"true"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"appointments":{"create":"true","read":"true","update":"true","delete":"true"},"doctors":{"create":"true","read":"true","update":"true","delete":"true"},"patients":{"create":"true","read":"true","update":"true","delete":"true"},"procedures":{"create":"true","read":"true","update":"true","delete":"true"},"labtracking":{"create":"true","read":"true","update":"true","delete":"true"},"reports":{"create":"true","read":"true","update":"true","delete":"true"},"treatments":{"create":"true","read":"true","update":"true","delete":"true"},"invoices":{"create":"true","read":"true","update":"true","delete":"true"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"true"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"true"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"true","read":"true","update":"true","delete":"true"},"worktype":{"create":"true","read":"true","update":"true","delete":"true"},"work":{"create":"true","read":"true","update":"true","delete":"true"},"lab":{"create":"true","read":"true","update":"true","delete":"true"},"site":{"create":"true","read":"true","update":"true","delete":"true"}}";s:10:"role_title";s:11:"Super Admin";}'),
('ac022d12c222ff1db39cb0e29d085bd7', '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36', 1458921920, ''),
('ac7a87f82cc7a05cd86476d431d54de0', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 1555088534, ''),
('b046994a04f81547e9d8ee4a87f3718c', '::1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36', 1461587229, 'a:9:{s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"admin";s:15:"admin_full_name";s:5:"5-STI";s:8:"admin_id";s:1:"1";s:10:"last_login";s:19:"2016-04-25 09:06:17";s:7:"last_ip";s:3:"::1";s:10:"permission";s:1714:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"customers":{"create":"true","read":"true","update":"true","delete":"true"},"orders":{"create":"true","read":"true","update":"true","delete":"true","history":"false","fullaccess":"false"},"payments":{"create":"false","read":"false","update":"false","delete":"false"},"invoice":{"create":"true","read":"true","update":"true","delete":"true"},"menu":{"create":"true","read":"true","update":"true","delete":"true"},"pages":{"create":"true","read":"true","update":"true","delete":"true"},"about":{"create":"true","read":"true","update":"true","delete":"true"},"services":{"create":"true","read":"true","update":"true","delete":"true"},"gallery":{"create":"true","read":"true","update":"true","delete":"true"},"testimonial":{"create":"true","read":"true","update":"true","delete":"true"},"sliders":{"create":"true","read":"true","update":"true","delete":"true"},"slider":{"create":"true","read":"true","update":"true","delete":"true"},"products":{"create":"true","read":"true","update":"true","delete":"true"},"productcategory":{"create":"true","read":"true","update":"true","delete":"true"},"sales":{"create":"false","read":"true","update":"false","delete":"true","history":"true","fullaccess":"true"},"contact":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"payment":{"create":"true","read":"true","update":"true","delete":"true","history":"true","fullaccess":"true"}}";s:10:"role_title";s:5:"Admin";}'),
('b068ce72eef6b3c337479d540520ac71', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', 1553610166, 'a:12:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:6:"alhaaj";s:15:"admin_full_name";s:6:"alhaaj";s:8:"admin_id";s:1:"3";s:10:"last_login";s:19:"2019-03-23 04:50:15";s:7:"last_ip";s:3:"::1";s:12:"user_role_id";s:1:"2";s:9:"clinic_id";s:1:"1";s:10:"permission";s:1559:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"false"},"clinics":{"create":"false","read":"false","update":"false","delete":"false"},"rolemanagement":{"create":"false","read":"false","update":"false","delete":"false"},"appointments":{"create":"true","read":"true","update":"true","delete":"true"},"doctors":{"create":"true","read":"true","update":"true","delete":"true"},"patients":{"create":"true","read":"true","update":"true","delete":"true"},"procedures":{"create":"false","read":"true","update":"false","delete":"false"},"labtracking":{"create":"true","read":"true","update":"true","delete":"true"},"reports":{"create":"false","read":"true","update":"false","delete":"false"},"treatments":{"create":"true","read":"true","update":"true","delete":"true"},"invoices":{"create":"true","read":"true","update":"true","delete":"true"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"true"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"true"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"false","update":"false","delete":"false"},"worktype":{"create":"true","read":"true","update":"true","delete":"true"},"work":{"create":"true","read":"true","update":"true","delete":"true"},"suppliers":{"create":"true","read":"true","update":"true","delete":"true"},"lab":{"create":"true","read":"true","update":"true","delete":"true"},"site":{"create":"true","read":"true","update":"true","delete":"false"}}";s:10:"role_title";s:5:"Admin";}'),
('b16bc2f30841bb760e097b853a658ed6', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', 1554886808, 'a:12:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"admin";s:15:"admin_full_name";s:15:"Talib Allauddin";s:8:"admin_id";s:1:"1";s:10:"last_login";s:19:"2019-04-09 14:41:46";s:7:"last_ip";s:3:"::1";s:12:"user_role_id";s:1:"1";s:9:"clinic_id";s:1:"0";s:10:"permission";s:2339:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"clinics":{"create":"true","read":"true","update":"true","delete":"true"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"appointments":{"create":"true","read":"true","update":"true","delete":"true"},"doctors":{"create":"true","read":"true","update":"true","delete":"true"},"patients":{"create":"true","read":"true","update":"true","delete":"true"},"procedures":{"create":"true","read":"true","update":"true","delete":"true"},"labtracking":{"create":"true","read":"true","update":"true","delete":"true"},"reports":{"create":"false","read":"true","update":"false","delete":"false"},"treatments":{"create":"true","read":"true","update":"true","delete":"true"},"invoices":{"create":"true","read":"true","update":"true","delete":"true"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"true"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"true"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"worktype":{"create":"true","read":"true","update":"true","delete":"true"},"work":{"create":"true","read":"true","update":"true","delete":"true"},"suppliers":{"create":"true","read":"true","update":"true","delete":"true"},"lab":{"create":"true","read":"true","update":"true","delete":"true"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"patientfiles":{"create":"true","read":"true","update":"true","delete":"true"},"expenses":{"create":"true","read":"true","update":"true","delete":"true"},"purchases":{"create":"true","read":"true","update":"true","delete":"true"},"items":{"create":"true","read":"true","update":"true","delete":"true","issue":"true"},"stocks":{"create":"true","read":"true","update":"true","delete":"true"},"accounts":{"create":"true","read":"true","update":"true","delete":"true"},"purchasedetails":{"create":"true","read":"true","update":"true","delete":"true"},"stockreleases":{"create":"true","read":"true","update":"true","delete":"true"},"employeepayroll":{"create":"true","read":"true","update":"true","delete":"true"},"employeeattendance":{"create":"true","read":"true","update":"true","delete":"true"}}";s:10:"role_title";s:11:"Super Admin";}'),
('b32cea9ae27cb79a331ca8f8c073fe11', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 1557096223, ''),
('b92ea3b9847468ef1ba36d26905da55d', '192.168.10.12', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36', 1460114999, ''),
('bd4e949488337c8783447d1c6e2bfa19', '192.168.0.102', 'Mozilla/5.0 (Linux; Android 6.0.1; SM-T800) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.90 Safari/537.36', 1555592268, 'a:12:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"jabar";s:15:"admin_full_name";s:6:"Jabar ";s:8:"admin_id";s:1:"9";s:10:"last_login";N;s:7:"last_ip";N;s:12:"user_role_id";s:1:"2";s:9:"clinic_id";s:1:"3";s:10:"permission";s:2692:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"false"},"clinics":{"create":"false","read":"false","update":"false","delete":"false"},"rolemanagement":{"create":"false","read":"false","update":"false","delete":"false"},"appointments":{"create":"true","read":"true","update":"true","delete":"true"},"doctors":{"create":"true","read":"true","update":"true","delete":"true"},"patients":{"create":"true","read":"true","update":"true","delete":"true"},"procedures":{"create":"false","read":"true","update":"false","delete":"false"},"labtracking":{"create":"true","read":"true","update":"true","delete":"true"},"reports":{"create":"false","read":"true","update":"false","delete":"false"},"treatments":{"create":"true","read":"true","update":"true","delete":"true"},"invoices":{"create":"true","read":"true","update":"true","delete":"true"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"true"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"true"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"false","update":"false","delete":"false"},"worktype":{"create":"true","read":"true","update":"true","delete":"true"},"work":{"create":"true","read":"true","update":"true","delete":"true"},"suppliers":{"create":"true","read":"true","update":"true","delete":"true"},"lab":{"create":"true","read":"true","update":"true","delete":"true"},"site":{"create":"true","read":"true","update":"true","delete":"false"},"patientfiles":{"create":"false","read":"false","update":"false","delete":"false"},"expenses":{"create":"false","read":"false","update":"false","delete":"false"},"purchases":{"create":"false","read":"false","update":"false","delete":"false"},"items":{"create":"false","read":"false","update":"false","delete":"false","issue":"false"},"stocks":{"create":"false","read":"false","update":"false","delete":"false"},"accounts":{"create":"false","read":"false","update":"false","delete":"false"},"purchasedetails":{"create":"false","read":"false","update":"false","delete":"false"},"stockreleases":{"create":"false","read":"false","update":"false","delete":"false"},"employeepayroll":{"create":"false","read":"false","update":"false","delete":"false"},"employeeattendance":{"create":"true","read":"true","update":"true","delete":"true"},"tokens":{"create":"true","read":"true","update":"true","delete":"true"},"backup":{"create":"false","read":"false","update":"false","delete":"false"},"reporttype":{"create":"false","read":"false","update":"false","delete":"false"},"":{"create":"false","read":"false","update":"false","delete":"false"}}";s:10:"role_title";s:5:"Admin";}'),
('bdb621306b32facf53c1f78ae0440ec0', '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36', 1460108787, ''),
('be7f642864fafc7ad22d98e5dc352b24', '::1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.75 Safari/537.36', 1460830282, 'a:9:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";s:11:"Super Admin";s:15:"admin_user_name";s:5:"admin";s:15:"admin_full_name";s:5:"5-STI";s:8:"admin_id";s:1:"1";s:10:"last_login";s:19:"2016-04-16 16:45:34";s:7:"last_ip";s:3:"::1";s:10:"permission";s:1032:"{"order":{"create":"true","read":"true","update":"true","delete":"true"},"digitizing":{"create":"true","read":"true","update":"true","delete":"true"},"content":{"create":"true","read":"true","update":"true","delete":"true"},"pages":{"create":"true","read":"true","update":"true","delete":"true"},"payment":{"create":"true","read":"true","update":"true","delete":"true"},"invoice":{"create":"true","read":"true","update":"true","delete":"true"},"user":{"create":"true","read":"true","update":"true","delete":"true"},"customer":{"create":"true","read":"true","update":"true","delete":"true"},"product":{"create":"true","read":"true","update":"true","delete":"true"},"sales":{"create":"true","read":"true","update":"true","delete":"true"},"gallery":{"create":"true","read":"true","update":"true","delete":"true"},"testimonial":{"create":"true","read":"true","update":"true","delete":"true"},"slider":{"create":"true","read":"true","update":"true","delete":"true"},"role":{"create":"true","read":"true","update":"true","delete":"true"}}";}'),
('c20058405c057ab210511b8758b9e99b', '::1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.75 Safari/537.36', 1460973577, 'a:9:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";s:11:"Super Admin";s:15:"admin_user_name";s:5:"admin";s:15:"admin_full_name";s:5:"5-STI";s:8:"admin_id";s:1:"1";s:10:"last_login";s:19:"2016-04-17 14:22:09";s:7:"last_ip";s:3:"::1";s:10:"permission";s:1722:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"customers":{"create":"true","read":"true","update":"true","delete":"true"},"orders":{"create":"true","read":"true","update":"true","delete":"true","history":"false","fullaccess":"false"},"payments":{"create":"false","read":"false","update":"false","delete":"false"},"invoice":{"create":"true","read":"true","update":"true","delete":"true"},"menu":{"create":"true","read":"true","update":"true","delete":"true"},"pages":{"create":"true","read":"true","update":"true","delete":"true"},"about":{"create":"false","read":"true","update":"false","delete":"false"},"services":{"create":"true","read":"true","update":"true","delete":"true"},"gallery":{"create":"true","read":"true","update":"true","delete":"true"},"testimonial":{"create":"true","read":"true","update":"true","delete":"true"},"sliders":{"create":"true","read":"true","update":"true","delete":"true"},"products":{"create":"false","read":"false","update":"false","delete":"false"},"productcategory":{"create":"true","read":"true","update":"true","delete":"true"},"sales":{"create":"false","read":"true","update":"false","delete":"true","history":"true","fullaccess":"true"},"contact":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"payment":{"create":"true","read":"true","update":"true","delete":"true","history":"true","fullaccess":"true"},"product":{"create":"true","read":"true","update":"true","delete":"true"}}";}'),
('c40566c90783a6637a0f54dc6c1b800d', '::1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36', 1460549037, 'a:8:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";s:11:"Super Admin";s:15:"admin_user_name";s:5:"admin";s:15:"admin_full_name";s:5:"5-STI";s:8:"admin_id";s:1:"1";s:10:"last_login";s:19:"2016-04-11 09:10:22";s:7:"last_ip";s:3:"::1";}'),
('c8b7fc9c69758cfe7d640ccbc512fce3', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 1556972568, 'a:13:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"rafay";s:15:"admin_full_name";s:11:"Abdur Rafay";s:8:"admin_id";s:1:"6";s:10:"last_login";s:19:"2019-05-03 13:19:44";s:7:"last_ip";s:13:"192.168.10.11";s:12:"user_role_id";s:1:"2";s:9:"clinic_id";s:1:"1";s:10:"permission";s:3263:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"clinics":{"create":"false","read":"false","update":"false","delete":"false"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"appointments":{"create":"true","read":"true","update":"true","delete":"true"},"patients":{"create":"true","read":"true","update":"true","delete":"true","profile":"true"},"procedures":{"create":"true","read":"true","update":"true","delete":"true"},"labtracking":{"create":"true","read":"true","update":"true","delete":"true"},"reports":{"create":"false","read":"true","update":"false","delete":"false"},"treatments":{"create":"true","read":"true","update":"true","delete":"true"},"invoices":{"create":"true","read":"true","update":"true","delete":"true"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"true"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"true"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"worktype":{"create":"true","read":"true","update":"true","delete":"true"},"work":{"create":"true","read":"true","update":"true","delete":"true"},"suppliers":{"create":"true","read":"true","update":"true","delete":"true"},"lab":{"create":"true","read":"true","update":"true","delete":"true"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"patientfiles":{"create":"true","read":"true","update":"true","delete":"true"},"expenses":{"create":"true","read":"true","update":"true","delete":"true"},"purchases":{"create":"true","read":"true","update":"true","delete":"true"},"items":{"create":"true","read":"true","update":"true","delete":"true","issue":"true"},"stocks":{"create":"true","read":"true","update":"true","delete":"true"},"accounts":{"create":"true","read":"true","update":"true","delete":"true"},"purchasedetails":{"create":"true","read":"true","update":"true","delete":"true"},"stockreleases":{"create":"true","read":"true","update":"true","delete":"true"},"employeepayroll":{"create":"true","read":"true","update":"true","delete":"true"},"employeeattendance":{"create":"true","read":"true","update":"true","delete":"true"},"tokens":{"create":"true","read":"true","update":"true","delete":"true"},"backup":{"create":"false","read":"true","update":"false","delete":"false"},"reporttype":{"create":"true","read":"true","update":"true","delete":"true"},"":{"create":"false","read":"false","update":"false","delete":"false"},"roomtype":{"create":"true","read":"true","update":"true","delete":"true"},"room":{"create":"true","read":"true","update":"true","delete":"true"},"bedtype":{"create":"true","read":"true","update":"true","delete":"true"},"bed":{"create":"true","read":"true","update":"true","delete":"true"},"bedpatient":{"create":"true","read":"true","update":"true","delete":"true"},"roompatient":{"create":"true","read":"true","update":"true","delete":"true"},"doctordays":{"create":"true","read":"true","update":"true","delete":"true"},"patientprocedures":{"create":"true","read":"true","update":"true","delete":"true"},"doctors":{"create":"true","read":"true","update":"true","delete":"true"}}";s:14:"doctor_role_id";s:1:"4";s:10:"role_title";s:5:"Admin";}'),
('cbd5663683d837c372203c56d51ef3fb', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', 1554039367, 'a:12:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"admin";s:15:"admin_full_name";s:15:"Talib Allauddin";s:8:"admin_id";s:1:"1";s:10:"last_login";s:19:"2019-03-29 01:10:59";s:7:"last_ip";s:3:"::1";s:12:"user_role_id";s:1:"1";s:9:"clinic_id";s:1:"0";s:10:"permission";s:1628:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"clinics":{"create":"true","read":"true","update":"true","delete":"true"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"appointments":{"create":"true","read":"true","update":"true","delete":"true"},"doctors":{"create":"true","read":"true","update":"true","delete":"true"},"patients":{"create":"true","read":"true","update":"true","delete":"true"},"procedures":{"create":"true","read":"true","update":"true","delete":"true"},"labtracking":{"create":"true","read":"true","update":"true","delete":"true"},"reports":{"create":"false","read":"true","update":"false","delete":"false"},"treatments":{"create":"true","read":"true","update":"true","delete":"true"},"invoices":{"create":"true","read":"true","update":"true","delete":"true"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"true"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"true"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"worktype":{"create":"true","read":"true","update":"true","delete":"true"},"work":{"create":"true","read":"true","update":"true","delete":"true"},"suppliers":{"create":"false","read":"false","update":"false","delete":"false"},"lab":{"create":"true","read":"true","update":"true","delete":"true"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"patientfiles":{"create":"true","read":"true","update":"true","delete":"true"}}";s:10:"role_title";s:11:"Super Admin";}'),
('ce576049f165e164a3e43dd0176fbe47', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', 1554716291, 'a:12:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:6:"alhaaj";s:15:"admin_full_name";s:6:"alhaaj";s:8:"admin_id";s:1:"4";s:10:"last_login";N;s:7:"last_ip";N;s:12:"user_role_id";s:1:"2";s:9:"clinic_id";s:1:"1";s:10:"permission";s:1559:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"false"},"clinics":{"create":"false","read":"false","update":"false","delete":"false"},"rolemanagement":{"create":"false","read":"false","update":"false","delete":"false"},"appointments":{"create":"true","read":"true","update":"true","delete":"true"},"doctors":{"create":"true","read":"true","update":"true","delete":"true"},"patients":{"create":"true","read":"true","update":"true","delete":"true"},"procedures":{"create":"false","read":"true","update":"false","delete":"false"},"labtracking":{"create":"true","read":"true","update":"true","delete":"true"},"reports":{"create":"false","read":"true","update":"false","delete":"false"},"treatments":{"create":"true","read":"true","update":"true","delete":"true"},"invoices":{"create":"true","read":"true","update":"true","delete":"true"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"true"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"true"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"false","update":"false","delete":"false"},"worktype":{"create":"true","read":"true","update":"true","delete":"true"},"work":{"create":"true","read":"true","update":"true","delete":"true"},"suppliers":{"create":"true","read":"true","update":"true","delete":"true"},"lab":{"create":"true","read":"true","update":"true","delete":"true"},"site":{"create":"true","read":"true","update":"true","delete":"false"}}";s:10:"role_title";s:5:"Admin";}'),
('ce5a0ac300c954a9acd0adb33126673b', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 1557963077, ''),
('ce660c939377965d78504165b1c42c58', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 1556007314, 'a:13:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"rafay";s:15:"admin_full_name";s:11:"Abdur Rafay";s:8:"admin_id";s:1:"6";s:10:"last_login";s:19:"2019-04-23 11:10:02";s:7:"last_ip";s:3:"::1";s:12:"user_role_id";s:1:"2";s:9:"clinic_id";s:1:"1";s:10:"permission";s:2644:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"clinics":{"create":"false","read":"false","update":"false","delete":"false"},"rolemanagement":{"create":"false","read":"false","update":"false","delete":"false"},"appointments":{"create":"true","read":"true","update":"true","delete":"true"},"patients":{"create":"true","read":"true","update":"true","delete":"true"},"procedures":{"create":"true","read":"true","update":"true","delete":"true"},"labtracking":{"create":"true","read":"true","update":"true","delete":"true"},"reports":{"create":"false","read":"true","update":"false","delete":"false"},"treatments":{"create":"true","read":"true","update":"true","delete":"true"},"invoices":{"create":"true","read":"true","update":"true","delete":"true"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"true"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"true"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"worktype":{"create":"true","read":"true","update":"true","delete":"true"},"work":{"create":"true","read":"true","update":"true","delete":"true"},"suppliers":{"create":"true","read":"true","update":"true","delete":"true"},"lab":{"create":"true","read":"true","update":"true","delete":"true"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"patientfiles":{"create":"true","read":"true","update":"true","delete":"true"},"expenses":{"create":"true","read":"true","update":"true","delete":"true"},"purchases":{"create":"true","read":"true","update":"true","delete":"true"},"items":{"create":"true","read":"true","update":"true","delete":"true","issue":"true"},"stocks":{"create":"true","read":"true","update":"true","delete":"true"},"accounts":{"create":"true","read":"true","update":"true","delete":"true"},"purchasedetails":{"create":"true","read":"true","update":"true","delete":"true"},"stockreleases":{"create":"true","read":"true","update":"true","delete":"true"},"employeepayroll":{"create":"true","read":"true","update":"true","delete":"true"},"employeeattendance":{"create":"true","read":"true","update":"true","delete":"true"},"tokens":{"create":"true","read":"true","update":"true","delete":"true"},"backup":{"create":"false","read":"true","update":"false","delete":"false"},"reporttype":{"create":"true","read":"true","update":"true","delete":"true"},"":{"create":"false","read":"false","update":"false","delete":"false"},"doctors":{"create":"true","read":"true","update":"true","delete":"true"}}";s:14:"doctor_role_id";s:1:"4";s:10:"role_title";s:5:"Admin";}'),
('d0715de2e864c460f1555d31c3f3502c', '::1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36', 1461569774, 'a:9:{s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"admin";s:15:"admin_full_name";s:5:"5-STI";s:8:"admin_id";s:1:"1";s:10:"last_login";s:19:"2016-04-25 08:33:52";s:7:"last_ip";s:3:"::1";s:10:"permission";s:1714:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"customers":{"create":"true","read":"true","update":"true","delete":"true"},"orders":{"create":"true","read":"true","update":"true","delete":"true","history":"false","fullaccess":"false"},"payments":{"create":"false","read":"false","update":"false","delete":"false"},"invoice":{"create":"true","read":"true","update":"true","delete":"true"},"menu":{"create":"true","read":"true","update":"true","delete":"true"},"pages":{"create":"true","read":"true","update":"true","delete":"true"},"about":{"create":"true","read":"true","update":"true","delete":"true"},"services":{"create":"true","read":"true","update":"true","delete":"true"},"gallery":{"create":"true","read":"true","update":"true","delete":"true"},"testimonial":{"create":"true","read":"true","update":"true","delete":"true"},"sliders":{"create":"true","read":"true","update":"true","delete":"true"},"slider":{"create":"true","read":"true","update":"true","delete":"true"},"products":{"create":"true","read":"true","update":"true","delete":"true"},"productcategory":{"create":"true","read":"true","update":"true","delete":"true"},"sales":{"create":"false","read":"true","update":"false","delete":"true","history":"true","fullaccess":"true"},"contact":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"payment":{"create":"true","read":"true","update":"true","delete":"true","history":"true","fullaccess":"true"}}";s:10:"role_title";s:5:"Admin";}'),
('d0b14c9e8692346864f899fc94623024', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 1555088534, '');
INSERT INTO `ci_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('d41c607a6fa40e0f65657e4f10dfa91e', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 1556033860, 'a:13:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"rafay";s:15:"admin_full_name";s:11:"Abdur Rafay";s:8:"admin_id";s:1:"6";s:10:"last_login";s:19:"2019-04-23 12:33:28";s:7:"last_ip";s:3:"::1";s:12:"user_role_id";s:1:"2";s:9:"clinic_id";s:1:"1";s:10:"permission";s:2644:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"clinics":{"create":"false","read":"false","update":"false","delete":"false"},"rolemanagement":{"create":"false","read":"false","update":"false","delete":"false"},"appointments":{"create":"true","read":"true","update":"true","delete":"true"},"patients":{"create":"true","read":"true","update":"true","delete":"true"},"procedures":{"create":"true","read":"true","update":"true","delete":"true"},"labtracking":{"create":"true","read":"true","update":"true","delete":"true"},"reports":{"create":"false","read":"true","update":"false","delete":"false"},"treatments":{"create":"true","read":"true","update":"true","delete":"true"},"invoices":{"create":"true","read":"true","update":"true","delete":"true"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"true"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"true"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"worktype":{"create":"true","read":"true","update":"true","delete":"true"},"work":{"create":"true","read":"true","update":"true","delete":"true"},"suppliers":{"create":"true","read":"true","update":"true","delete":"true"},"lab":{"create":"true","read":"true","update":"true","delete":"true"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"patientfiles":{"create":"true","read":"true","update":"true","delete":"true"},"expenses":{"create":"true","read":"true","update":"true","delete":"true"},"purchases":{"create":"true","read":"true","update":"true","delete":"true"},"items":{"create":"true","read":"true","update":"true","delete":"true","issue":"true"},"stocks":{"create":"true","read":"true","update":"true","delete":"true"},"accounts":{"create":"true","read":"true","update":"true","delete":"true"},"purchasedetails":{"create":"true","read":"true","update":"true","delete":"true"},"stockreleases":{"create":"true","read":"true","update":"true","delete":"true"},"employeepayroll":{"create":"true","read":"true","update":"true","delete":"true"},"employeeattendance":{"create":"true","read":"true","update":"true","delete":"true"},"tokens":{"create":"true","read":"true","update":"true","delete":"true"},"backup":{"create":"false","read":"true","update":"false","delete":"false"},"reporttype":{"create":"true","read":"true","update":"true","delete":"true"},"":{"create":"false","read":"false","update":"false","delete":"false"},"doctors":{"create":"true","read":"true","update":"true","delete":"true"}}";s:14:"doctor_role_id";s:1:"4";s:10:"role_title";s:5:"Admin";}'),
('d55f0ae15e799b5ef1e79bb605ab9912', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', 1553525928, 'a:12:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"admin";s:15:"admin_full_name";s:15:"Talib Allauddin";s:8:"admin_id";s:1:"1";s:10:"last_login";s:19:"2019-03-25 16:56:35";s:7:"last_ip";s:3:"::1";s:12:"user_role_id";s:1:"1";s:9:"clinic_id";s:1:"0";s:10:"permission";s:1463:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"clinics":{"create":"true","read":"true","update":"true","delete":"true"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"appointments":{"create":"true","read":"true","update":"true","delete":"true"},"doctors":{"create":"true","read":"true","update":"true","delete":"true"},"patients":{"create":"true","read":"true","update":"true","delete":"true"},"procedures":{"create":"true","read":"true","update":"true","delete":"true"},"labtracking":{"create":"true","read":"true","update":"true","delete":"true"},"reports":{"create":"true","read":"true","update":"true","delete":"true"},"treatments":{"create":"true","read":"true","update":"true","delete":"true"},"invoices":{"create":"true","read":"true","update":"true","delete":"true"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"true"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"true"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"true","read":"true","update":"true","delete":"true"},"worktype":{"create":"true","read":"true","update":"true","delete":"true"},"work":{"create":"true","read":"true","update":"true","delete":"true"},"lab":{"create":"true","read":"true","update":"true","delete":"true"},"site":{"create":"true","read":"true","update":"true","delete":"true"}}";s:10:"role_title";s:11:"Super Admin";}'),
('d5b83646948f335a432b3e98140b18df', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 1557850687, 'a:13:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"rafay";s:15:"admin_full_name";s:11:"Abdur Rafay";s:8:"admin_id";s:1:"6";s:10:"last_login";s:19:"2019-05-13 07:39:48";s:7:"last_ip";s:3:"::1";s:12:"user_role_id";s:1:"2";s:9:"clinic_id";s:1:"1";s:10:"permission";s:3263:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"clinics":{"create":"false","read":"false","update":"false","delete":"false"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"appointments":{"create":"true","read":"true","update":"true","delete":"true"},"patients":{"create":"true","read":"true","update":"true","delete":"true","profile":"true"},"procedures":{"create":"true","read":"true","update":"true","delete":"true"},"labtracking":{"create":"true","read":"true","update":"true","delete":"true"},"reports":{"create":"false","read":"true","update":"false","delete":"false"},"treatments":{"create":"true","read":"true","update":"true","delete":"true"},"invoices":{"create":"true","read":"true","update":"true","delete":"true"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"true"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"true"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"worktype":{"create":"true","read":"true","update":"true","delete":"true"},"work":{"create":"true","read":"true","update":"true","delete":"true"},"suppliers":{"create":"true","read":"true","update":"true","delete":"true"},"lab":{"create":"true","read":"true","update":"true","delete":"true"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"patientfiles":{"create":"true","read":"true","update":"true","delete":"true"},"expenses":{"create":"true","read":"true","update":"true","delete":"true"},"purchases":{"create":"true","read":"true","update":"true","delete":"true"},"items":{"create":"true","read":"true","update":"true","delete":"true","issue":"true"},"stocks":{"create":"true","read":"true","update":"true","delete":"true"},"accounts":{"create":"true","read":"true","update":"true","delete":"true"},"purchasedetails":{"create":"true","read":"true","update":"true","delete":"true"},"stockreleases":{"create":"true","read":"true","update":"true","delete":"true"},"employeepayroll":{"create":"true","read":"true","update":"true","delete":"true"},"employeeattendance":{"create":"true","read":"true","update":"true","delete":"true"},"tokens":{"create":"true","read":"true","update":"true","delete":"true"},"backup":{"create":"false","read":"true","update":"false","delete":"false"},"reporttype":{"create":"true","read":"true","update":"true","delete":"true"},"":{"create":"false","read":"false","update":"false","delete":"false"},"roomtype":{"create":"true","read":"true","update":"true","delete":"true"},"room":{"create":"true","read":"true","update":"true","delete":"true"},"bedtype":{"create":"true","read":"true","update":"true","delete":"true"},"bed":{"create":"true","read":"true","update":"true","delete":"true"},"bedpatient":{"create":"true","read":"true","update":"true","delete":"true"},"roompatient":{"create":"true","read":"true","update":"true","delete":"true"},"doctordays":{"create":"true","read":"true","update":"true","delete":"true"},"patientprocedures":{"create":"true","read":"true","update":"true","delete":"true"},"doctors":{"create":"true","read":"true","update":"true","delete":"true"}}";s:14:"doctor_role_id";s:1:"4";s:10:"role_title";s:5:"Admin";}'),
('d79e567a721f1695df7f88c07117bc4c', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', 1553298575, ''),
('d82f58e3fcf8434a5f0a432fa629fefb', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.157 Safari/537.36', 1558174547, 'a:13:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"rafay";s:15:"admin_full_name";s:11:"Abdur Rafay";s:8:"admin_id";s:1:"6";s:10:"last_login";s:19:"2019-05-18 13:16:43";s:7:"last_ip";s:3:"::1";s:12:"user_role_id";s:1:"2";s:9:"clinic_id";s:1:"1";s:10:"permission";s:3597:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"clinics":{"create":"false","read":"false","update":"false","delete":"false"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"appointments":{"create":"true","read":"true","update":"true","delete":"true"},"patients":{"create":"true","read":"true","update":"true","delete":"true","profile":"true"},"procedures":{"create":"true","read":"true","update":"true","delete":"true"},"labtracking":{"create":"true","read":"true","update":"true","delete":"true"},"reports":{"create":"false","read":"true","update":"false","delete":"false"},"treatments":{"create":"true","read":"true","update":"true","delete":"true"},"invoices":{"create":"true","read":"true","update":"true","delete":"true"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"true"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"true"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"worktype":{"create":"true","read":"true","update":"true","delete":"true"},"work":{"create":"true","read":"true","update":"true","delete":"true"},"suppliers":{"create":"true","read":"true","update":"true","delete":"true"},"lab":{"create":"true","read":"true","update":"true","delete":"true"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"patientfiles":{"create":"true","read":"true","update":"true","delete":"true"},"expenses":{"create":"true","read":"true","update":"true","delete":"true"},"purchases":{"create":"true","read":"true","update":"true","delete":"true"},"items":{"create":"true","read":"true","update":"true","delete":"true","issue":"true"},"stocks":{"create":"true","read":"true","update":"true","delete":"true"},"accounts":{"create":"true","read":"true","update":"true","delete":"true"},"purchasedetails":{"create":"true","read":"true","update":"true","delete":"true"},"stockreleases":{"create":"true","read":"true","update":"true","delete":"true"},"employeepayroll":{"create":"true","read":"true","update":"true","delete":"true"},"employeeattendance":{"create":"true","read":"true","update":"true","delete":"true"},"tokens":{"create":"true","read":"true","update":"true","delete":"true"},"backup":{"create":"false","read":"true","update":"false","delete":"false"},"reporttype":{"create":"true","read":"true","update":"true","delete":"true"},"":{"create":"false","read":"false","update":"false","delete":"false"},"roomtype":{"create":"true","read":"true","update":"true","delete":"true"},"room":{"create":"true","read":"true","update":"true","delete":"true"},"bedtype":{"create":"true","read":"true","update":"true","delete":"true"},"bed":{"create":"true","read":"true","update":"true","delete":"true"},"bedpatient":{"create":"true","read":"true","update":"true","delete":"true"},"roompatient":{"create":"true","read":"true","update":"true","delete":"true"},"doctordays":{"create":"true","read":"true","update":"true","delete":"true"},"patientprocedures":{"create":"true","read":"true","update":"true","delete":"true"},"facilities":{"create":"true","read":"true","update":"true","delete":"true"},"patientcase":{"create":"true","read":"true","update":"true","delete":"true","details":"true"},"relations":{"create":"true","read":"true","update":"true","delete":"true"},"patientfacility":{"create":"false","read":"false","update":"false","delete":"false"},"doctors":{"create":"true","read":"true","update":"true","delete":"true"}}";s:14:"doctor_role_id";s:1:"4";s:10:"role_title";s:5:"Admin";}'),
('d830919dad04bfc476ac9d68ee21f6d0', '::1', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1547825286, 'a:10:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"admin";s:15:"admin_full_name";s:15:"Talib Allauddin";s:8:"admin_id";s:1:"1";s:10:"last_login";s:19:"2019-01-18 15:53:20";s:7:"last_ip";s:3:"::1";s:10:"permission";s:1942:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"customers":{"create":"true","read":"true","update":"true","delete":"true"},"orders":{"create":"true","read":"true","update":"true","delete":"true","history":"false","fullaccess":"false"},"payments":{"create":"false","read":"false","update":"false","delete":"false"},"invoice":{"create":"true","read":"true","update":"true","delete":"true"},"menu":{"create":"true","read":"true","update":"true","delete":"true"},"pages":{"create":"true","read":"true","update":"true","delete":"true"},"faqs":{"create":"true","read":"true","update":"true","delete":"true"},"about":{"create":"true","read":"true","update":"true","delete":"true"},"homecontent":{"create":"false","read":"false","update":"false","delete":"false"},"services":{"create":"true","read":"true","update":"true","delete":"true"},"gallery":{"create":"true","read":"true","update":"true","delete":"true"},"testimonial":{"create":"true","read":"true","update":"true","delete":"true"},"sliders":{"create":"true","read":"true","update":"true","delete":"true"},"slider":{"create":"true","read":"true","update":"true","delete":"true"},"products":{"create":"true","read":"true","update":"true","delete":"true"},"productcategory":{"create":"true","read":"true","update":"true","delete":"true"},"sales":{"create":"false","read":"true","update":"false","delete":"true","history":"true","fullaccess":"true"},"contact":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"payment":{"create":"true","read":"true","update":"true","delete":"true","history":"true","fullaccess":"true"},"shipment":{"create":"true","read":"true","update":"true","delete":"true"}}";s:10:"role_title";s:5:"Admin";}'),
('dac7ce875ac870a26af3d4c8b7e28f6e', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 1556547416, 'a:13:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"rafay";s:15:"admin_full_name";s:11:"Abdur Rafay";s:8:"admin_id";s:1:"6";s:10:"last_login";s:19:"2019-04-28 18:09:20";s:7:"last_ip";s:3:"::1";s:12:"user_role_id";s:1:"2";s:9:"clinic_id";s:1:"1";s:10:"permission";s:3166:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"clinics":{"create":"false","read":"false","update":"false","delete":"false"},"rolemanagement":{"create":"false","read":"false","update":"false","delete":"false"},"appointments":{"create":"true","read":"true","update":"true","delete":"true"},"patients":{"create":"true","read":"true","update":"true","delete":"true"},"procedures":{"create":"true","read":"true","update":"true","delete":"true"},"labtracking":{"create":"true","read":"true","update":"true","delete":"true"},"reports":{"create":"false","read":"true","update":"false","delete":"false"},"treatments":{"create":"true","read":"true","update":"true","delete":"true"},"invoices":{"create":"true","read":"true","update":"true","delete":"true"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"true"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"true"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"worktype":{"create":"true","read":"true","update":"true","delete":"true"},"work":{"create":"true","read":"true","update":"true","delete":"true"},"suppliers":{"create":"true","read":"true","update":"true","delete":"true"},"lab":{"create":"true","read":"true","update":"true","delete":"true"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"patientfiles":{"create":"true","read":"true","update":"true","delete":"true"},"expenses":{"create":"true","read":"true","update":"true","delete":"true"},"purchases":{"create":"true","read":"true","update":"true","delete":"true"},"items":{"create":"true","read":"true","update":"true","delete":"true","issue":"true"},"stocks":{"create":"true","read":"true","update":"true","delete":"true"},"accounts":{"create":"true","read":"true","update":"true","delete":"true"},"purchasedetails":{"create":"true","read":"true","update":"true","delete":"true"},"stockreleases":{"create":"true","read":"true","update":"true","delete":"true"},"employeepayroll":{"create":"true","read":"true","update":"true","delete":"true"},"employeeattendance":{"create":"true","read":"true","update":"true","delete":"true"},"tokens":{"create":"true","read":"true","update":"true","delete":"true"},"backup":{"create":"false","read":"true","update":"false","delete":"false"},"reporttype":{"create":"true","read":"true","update":"true","delete":"true"},"":{"create":"false","read":"false","update":"false","delete":"false"},"roomtype":{"create":"true","read":"true","update":"true","delete":"true"},"room":{"create":"true","read":"true","update":"true","delete":"true"},"bedtype":{"create":"true","read":"true","update":"true","delete":"true"},"bed":{"create":"true","read":"true","update":"true","delete":"true"},"bedpatient":{"create":"true","read":"true","update":"true","delete":"true"},"roompatient":{"create":"true","read":"true","update":"true","delete":"true"},"doctordays":{"create":"true","read":"true","update":"true","delete":"true"},"doctors":{"create":"true","read":"true","update":"true","delete":"true"}}";s:14:"doctor_role_id";s:1:"4";s:10:"role_title";s:5:"Admin";}'),
('de01d5d2165b1c0e3f4ead53819e37e6', '192.168.10.6', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 1558026790, 'a:13:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"rafay";s:15:"admin_full_name";s:11:"Abdur Rafay";s:8:"admin_id";s:1:"6";s:10:"last_login";s:19:"2019-05-16 08:22:37";s:7:"last_ip";s:3:"::1";s:12:"user_role_id";s:1:"2";s:9:"clinic_id";s:1:"1";s:10:"permission";s:3494:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"clinics":{"create":"false","read":"false","update":"false","delete":"false"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"appointments":{"create":"true","read":"true","update":"true","delete":"true"},"patients":{"create":"true","read":"true","update":"true","delete":"true","profile":"true"},"procedures":{"create":"true","read":"true","update":"true","delete":"true"},"labtracking":{"create":"true","read":"true","update":"true","delete":"true"},"reports":{"create":"false","read":"true","update":"false","delete":"false"},"treatments":{"create":"true","read":"true","update":"true","delete":"true"},"invoices":{"create":"true","read":"true","update":"true","delete":"true"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"true"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"true"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"worktype":{"create":"true","read":"true","update":"true","delete":"true"},"work":{"create":"true","read":"true","update":"true","delete":"true"},"suppliers":{"create":"true","read":"true","update":"true","delete":"true"},"lab":{"create":"true","read":"true","update":"true","delete":"true"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"patientfiles":{"create":"true","read":"true","update":"true","delete":"true"},"expenses":{"create":"true","read":"true","update":"true","delete":"true"},"purchases":{"create":"true","read":"true","update":"true","delete":"true"},"items":{"create":"true","read":"true","update":"true","delete":"true","issue":"true"},"stocks":{"create":"true","read":"true","update":"true","delete":"true"},"accounts":{"create":"true","read":"true","update":"true","delete":"true"},"purchasedetails":{"create":"true","read":"true","update":"true","delete":"true"},"stockreleases":{"create":"true","read":"true","update":"true","delete":"true"},"employeepayroll":{"create":"true","read":"true","update":"true","delete":"true"},"employeeattendance":{"create":"true","read":"true","update":"true","delete":"true"},"tokens":{"create":"true","read":"true","update":"true","delete":"true"},"backup":{"create":"false","read":"true","update":"false","delete":"false"},"reporttype":{"create":"true","read":"true","update":"true","delete":"true"},"":{"create":"false","read":"false","update":"false","delete":"false"},"roomtype":{"create":"true","read":"true","update":"true","delete":"true"},"room":{"create":"true","read":"true","update":"true","delete":"true"},"bedtype":{"create":"true","read":"true","update":"true","delete":"true"},"bed":{"create":"true","read":"true","update":"true","delete":"true"},"bedpatient":{"create":"true","read":"true","update":"true","delete":"true"},"roompatient":{"create":"true","read":"true","update":"true","delete":"true"},"doctordays":{"create":"true","read":"true","update":"true","delete":"true"},"patientprocedures":{"create":"true","read":"true","update":"true","delete":"true"},"facilities":{"create":"true","read":"true","update":"true","delete":"true"},"patientcase":{"create":"true","read":"true","update":"true","delete":"true"},"relations":{"create":"true","read":"true","update":"true","delete":"true"},"doctors":{"create":"true","read":"true","update":"true","delete":"true"}}";s:14:"doctor_role_id";s:1:"4";s:10:"role_title";s:5:"Admin";}'),
('df85194973eb19d48daf810ec780de4c', '::1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.75 Safari/537.36', 1460993467, ''),
('dfb0b68feb8ce0eee64ff6076cb8e119', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', 1552052052, 'a:10:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"admin";s:15:"admin_full_name";s:15:"Talib Allauddin";s:8:"admin_id";s:1:"1";s:10:"last_login";s:19:"2019-03-08 00:09:59";s:7:"last_ip";s:3:"::1";s:10:"permission";s:1942:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"customers":{"create":"true","read":"true","update":"true","delete":"true"},"orders":{"create":"true","read":"true","update":"true","delete":"true","history":"false","fullaccess":"false"},"payments":{"create":"false","read":"false","update":"false","delete":"false"},"invoice":{"create":"true","read":"true","update":"true","delete":"true"},"menu":{"create":"true","read":"true","update":"true","delete":"true"},"pages":{"create":"true","read":"true","update":"true","delete":"true"},"faqs":{"create":"true","read":"true","update":"true","delete":"true"},"about":{"create":"true","read":"true","update":"true","delete":"true"},"homecontent":{"create":"false","read":"false","update":"false","delete":"false"},"services":{"create":"true","read":"true","update":"true","delete":"true"},"gallery":{"create":"true","read":"true","update":"true","delete":"true"},"testimonial":{"create":"true","read":"true","update":"true","delete":"true"},"sliders":{"create":"true","read":"true","update":"true","delete":"true"},"slider":{"create":"true","read":"true","update":"true","delete":"true"},"products":{"create":"true","read":"true","update":"true","delete":"true"},"productcategory":{"create":"true","read":"true","update":"true","delete":"true"},"sales":{"create":"false","read":"true","update":"false","delete":"true","history":"true","fullaccess":"true"},"contact":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"payment":{"create":"true","read":"true","update":"true","delete":"true","history":"true","fullaccess":"true"},"shipment":{"create":"true","read":"true","update":"true","delete":"true"}}";s:10:"role_title";s:5:"Admin";}'),
('e3fa360bb38f18cb1105b1f38ad98cdb', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', 1553067557, 'a:12:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:6:"alhaaj";s:15:"admin_full_name";s:6:"alhaaj";s:8:"admin_id";s:1:"3";s:10:"last_login";s:19:"2019-03-20 07:06:12";s:7:"last_ip";s:3:"::1";s:12:"user_role_id";s:1:"2";s:9:"clinic_id";s:1:"1";s:10:"permission";s:1267:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"false"},"clinics":{"create":"false","read":"false","update":"false","delete":"false"},"rolemanagement":{"create":"false","read":"false","update":"false","delete":"false"},"appointments":{"create":"true","read":"true","update":"true","delete":"true"},"doctors":{"create":"true","read":"true","update":"true","delete":"true"},"patients":{"create":"true","read":"true","update":"true","delete":"true"},"procedures":{"create":"false","read":"true","update":"false","delete":"false"},"labtracking":{"create":"true","read":"true","update":"true","delete":"true"},"reports":{"create":"false","read":"true","update":"false","delete":"false"},"treatments":{"create":"true","read":"true","update":"true","delete":"true"},"invoices":{"create":"true","read":"true","update":"true","delete":"true"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"true"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"true"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"false","update":"false","delete":"false"},"site":{"create":"true","read":"true","update":"true","delete":"false"}}";s:10:"role_title";s:5:"Admin";}'),
('e5ae3359b3bf75c46a54d4e2e4018a56', '::1', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1547726632, 'a:10:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"admin";s:15:"admin_full_name";s:5:"5-STI";s:8:"admin_id";s:1:"1";s:10:"last_login";s:19:"2016-04-26 17:18:13";s:7:"last_ip";s:3:"::1";s:10:"permission";s:1785:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"customers":{"create":"true","read":"true","update":"true","delete":"true"},"orders":{"create":"true","read":"true","update":"true","delete":"true","history":"false","fullaccess":"false"},"payments":{"create":"false","read":"false","update":"false","delete":"false"},"invoice":{"create":"true","read":"true","update":"true","delete":"true"},"menu":{"create":"true","read":"true","update":"true","delete":"true"},"pages":{"create":"true","read":"true","update":"true","delete":"true"},"faqs":{"create":"true","read":"true","update":"true","delete":"true"},"about":{"create":"true","read":"true","update":"true","delete":"true"},"services":{"create":"true","read":"true","update":"true","delete":"true"},"gallery":{"create":"true","read":"true","update":"true","delete":"true"},"testimonial":{"create":"true","read":"true","update":"true","delete":"true"},"sliders":{"create":"true","read":"true","update":"true","delete":"true"},"slider":{"create":"true","read":"true","update":"true","delete":"true"},"products":{"create":"true","read":"true","update":"true","delete":"true"},"productcategory":{"create":"true","read":"true","update":"true","delete":"true"},"sales":{"create":"false","read":"true","update":"false","delete":"true","history":"true","fullaccess":"true"},"contact":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"payment":{"create":"true","read":"true","update":"true","delete":"true","history":"true","fullaccess":"true"}}";s:10:"role_title";s:5:"Admin";}'),
('e93a2f1b99b5a2970348af5947e768fe', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 1556318061, 'a:13:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"rafay";s:15:"admin_full_name";s:11:"Abdur Rafay";s:8:"admin_id";s:1:"6";s:10:"last_login";s:19:"2019-04-26 21:13:23";s:7:"last_ip";s:3:"::1";s:12:"user_role_id";s:1:"2";s:9:"clinic_id";s:1:"1";s:10:"permission";s:3166:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"clinics":{"create":"false","read":"false","update":"false","delete":"false"},"rolemanagement":{"create":"false","read":"false","update":"false","delete":"false"},"appointments":{"create":"true","read":"true","update":"true","delete":"true"},"patients":{"create":"true","read":"true","update":"true","delete":"true"},"procedures":{"create":"true","read":"true","update":"true","delete":"true"},"labtracking":{"create":"true","read":"true","update":"true","delete":"true"},"reports":{"create":"false","read":"true","update":"false","delete":"false"},"treatments":{"create":"true","read":"true","update":"true","delete":"true"},"invoices":{"create":"true","read":"true","update":"true","delete":"true"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"true"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"true"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"worktype":{"create":"true","read":"true","update":"true","delete":"true"},"work":{"create":"true","read":"true","update":"true","delete":"true"},"suppliers":{"create":"true","read":"true","update":"true","delete":"true"},"lab":{"create":"true","read":"true","update":"true","delete":"true"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"patientfiles":{"create":"true","read":"true","update":"true","delete":"true"},"expenses":{"create":"true","read":"true","update":"true","delete":"true"},"purchases":{"create":"true","read":"true","update":"true","delete":"true"},"items":{"create":"true","read":"true","update":"true","delete":"true","issue":"true"},"stocks":{"create":"true","read":"true","update":"true","delete":"true"},"accounts":{"create":"true","read":"true","update":"true","delete":"true"},"purchasedetails":{"create":"true","read":"true","update":"true","delete":"true"},"stockreleases":{"create":"true","read":"true","update":"true","delete":"true"},"employeepayroll":{"create":"true","read":"true","update":"true","delete":"true"},"employeeattendance":{"create":"true","read":"true","update":"true","delete":"true"},"tokens":{"create":"true","read":"true","update":"true","delete":"true"},"backup":{"create":"false","read":"true","update":"false","delete":"false"},"reporttype":{"create":"true","read":"true","update":"true","delete":"true"},"":{"create":"false","read":"false","update":"false","delete":"false"},"roomtype":{"create":"true","read":"true","update":"true","delete":"true"},"room":{"create":"true","read":"true","update":"true","delete":"true"},"bedtype":{"create":"true","read":"true","update":"true","delete":"true"},"bed":{"create":"true","read":"true","update":"true","delete":"true"},"bedpatient":{"create":"true","read":"true","update":"true","delete":"true"},"roompatient":{"create":"true","read":"true","update":"true","delete":"true"},"doctordays":{"create":"true","read":"true","update":"true","delete":"true"},"doctors":{"create":"true","read":"true","update":"true","delete":"true"}}";s:14:"doctor_role_id";s:1:"4";s:10:"role_title";s:5:"Admin";}'),
('e940651f91a29b3e0d097dd7d44a9416', '::1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36', 1461138097, 'a:10:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"admin";s:15:"admin_full_name";s:5:"5-STI";s:8:"admin_id";s:1:"1";s:10:"last_login";s:19:"2016-04-20 09:09:37";s:7:"last_ip";s:3:"::1";s:10:"permission";s:1714:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"customers":{"create":"true","read":"true","update":"true","delete":"true"},"orders":{"create":"true","read":"true","update":"true","delete":"true","history":"false","fullaccess":"false"},"payments":{"create":"false","read":"false","update":"false","delete":"false"},"invoice":{"create":"true","read":"true","update":"true","delete":"true"},"menu":{"create":"true","read":"true","update":"true","delete":"true"},"pages":{"create":"true","read":"true","update":"true","delete":"true"},"about":{"create":"true","read":"true","update":"true","delete":"true"},"services":{"create":"true","read":"true","update":"true","delete":"true"},"gallery":{"create":"true","read":"true","update":"true","delete":"true"},"testimonial":{"create":"true","read":"true","update":"true","delete":"true"},"sliders":{"create":"true","read":"true","update":"true","delete":"true"},"slider":{"create":"true","read":"true","update":"true","delete":"true"},"products":{"create":"true","read":"true","update":"true","delete":"true"},"productcategory":{"create":"true","read":"true","update":"true","delete":"true"},"sales":{"create":"false","read":"true","update":"false","delete":"true","history":"true","fullaccess":"true"},"contact":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"payment":{"create":"true","read":"true","update":"true","delete":"true","history":"true","fullaccess":"true"}}";s:10:"role_title";s:5:"Admin";}'),
('ec131761e8cb7d66b829f3cfcc2e01a7', '::1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36', 1461567197, 'a:10:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"admin";s:15:"admin_full_name";s:5:"5-STI";s:8:"admin_id";s:1:"1";s:10:"last_login";s:19:"2016-04-20 11:59:08";s:7:"last_ip";s:3:"::1";s:10:"permission";s:1714:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"customers":{"create":"true","read":"true","update":"true","delete":"true"},"orders":{"create":"true","read":"true","update":"true","delete":"true","history":"false","fullaccess":"false"},"payments":{"create":"false","read":"false","update":"false","delete":"false"},"invoice":{"create":"true","read":"true","update":"true","delete":"true"},"menu":{"create":"true","read":"true","update":"true","delete":"true"},"pages":{"create":"true","read":"true","update":"true","delete":"true"},"about":{"create":"true","read":"true","update":"true","delete":"true"},"services":{"create":"true","read":"true","update":"true","delete":"true"},"gallery":{"create":"true","read":"true","update":"true","delete":"true"},"testimonial":{"create":"true","read":"true","update":"true","delete":"true"},"sliders":{"create":"true","read":"true","update":"true","delete":"true"},"slider":{"create":"true","read":"true","update":"true","delete":"true"},"products":{"create":"true","read":"true","update":"true","delete":"true"},"productcategory":{"create":"true","read":"true","update":"true","delete":"true"},"sales":{"create":"false","read":"true","update":"false","delete":"true","history":"true","fullaccess":"true"},"contact":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"payment":{"create":"true","read":"true","update":"true","delete":"true","history":"true","fullaccess":"true"}}";s:10:"role_title";s:5:"Admin";}'),
('ee662434029dbbff77418333a495b9ce', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', 1552228717, 'a:10:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"admin";s:15:"admin_full_name";s:15:"Talib Allauddin";s:8:"admin_id";s:1:"1";s:10:"last_login";s:19:"2019-03-08 14:34:32";s:7:"last_ip";s:3:"::1";s:10:"permission";s:1942:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"customers":{"create":"true","read":"true","update":"true","delete":"true"},"orders":{"create":"true","read":"true","update":"true","delete":"true","history":"false","fullaccess":"false"},"payments":{"create":"false","read":"false","update":"false","delete":"false"},"invoice":{"create":"true","read":"true","update":"true","delete":"true"},"menu":{"create":"true","read":"true","update":"true","delete":"true"},"pages":{"create":"true","read":"true","update":"true","delete":"true"},"faqs":{"create":"true","read":"true","update":"true","delete":"true"},"about":{"create":"true","read":"true","update":"true","delete":"true"},"homecontent":{"create":"false","read":"false","update":"false","delete":"false"},"services":{"create":"true","read":"true","update":"true","delete":"true"},"gallery":{"create":"true","read":"true","update":"true","delete":"true"},"testimonial":{"create":"true","read":"true","update":"true","delete":"true"},"sliders":{"create":"true","read":"true","update":"true","delete":"true"},"slider":{"create":"true","read":"true","update":"true","delete":"true"},"products":{"create":"true","read":"true","update":"true","delete":"true"},"productcategory":{"create":"true","read":"true","update":"true","delete":"true"},"sales":{"create":"false","read":"true","update":"false","delete":"true","history":"true","fullaccess":"true"},"contact":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"payment":{"create":"true","read":"true","update":"true","delete":"true","history":"true","fullaccess":"true"},"shipment":{"create":"true","read":"true","update":"true","delete":"true"}}";s:10:"role_title";s:5:"Admin";}'),
('efa5b3651201f605769bfead86217086', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36', 1460289535, 'a:8:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";s:11:"Super Admin";s:15:"admin_user_name";s:5:"admin";s:15:"admin_full_name";s:5:"5-STI";s:8:"admin_id";s:1:"1";s:10:"last_login";s:19:"2016-04-08 11:12:57";s:7:"last_ip";s:9:"127.0.0.1";}'),
('f31da7b81844ecbece9555ef53b2a3df', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 1556482976, 'a:13:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"rafay";s:15:"admin_full_name";s:11:"Abdur Rafay";s:8:"admin_id";s:1:"6";s:10:"last_login";s:19:"2019-04-27 11:16:19";s:7:"last_ip";s:3:"::1";s:12:"user_role_id";s:1:"2";s:9:"clinic_id";s:1:"1";s:10:"permission";s:3166:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"clinics":{"create":"false","read":"false","update":"false","delete":"false"},"rolemanagement":{"create":"false","read":"false","update":"false","delete":"false"},"appointments":{"create":"true","read":"true","update":"true","delete":"true"},"patients":{"create":"true","read":"true","update":"true","delete":"true"},"procedures":{"create":"true","read":"true","update":"true","delete":"true"},"labtracking":{"create":"true","read":"true","update":"true","delete":"true"},"reports":{"create":"false","read":"true","update":"false","delete":"false"},"treatments":{"create":"true","read":"true","update":"true","delete":"true"},"invoices":{"create":"true","read":"true","update":"true","delete":"true"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"true"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"true"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"worktype":{"create":"true","read":"true","update":"true","delete":"true"},"work":{"create":"true","read":"true","update":"true","delete":"true"},"suppliers":{"create":"true","read":"true","update":"true","delete":"true"},"lab":{"create":"true","read":"true","update":"true","delete":"true"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"patientfiles":{"create":"true","read":"true","update":"true","delete":"true"},"expenses":{"create":"true","read":"true","update":"true","delete":"true"},"purchases":{"create":"true","read":"true","update":"true","delete":"true"},"items":{"create":"true","read":"true","update":"true","delete":"true","issue":"true"},"stocks":{"create":"true","read":"true","update":"true","delete":"true"},"accounts":{"create":"true","read":"true","update":"true","delete":"true"},"purchasedetails":{"create":"true","read":"true","update":"true","delete":"true"},"stockreleases":{"create":"true","read":"true","update":"true","delete":"true"},"employeepayroll":{"create":"true","read":"true","update":"true","delete":"true"},"employeeattendance":{"create":"true","read":"true","update":"true","delete":"true"},"tokens":{"create":"true","read":"true","update":"true","delete":"true"},"backup":{"create":"false","read":"true","update":"false","delete":"false"},"reporttype":{"create":"true","read":"true","update":"true","delete":"true"},"":{"create":"false","read":"false","update":"false","delete":"false"},"roomtype":{"create":"true","read":"true","update":"true","delete":"true"},"room":{"create":"true","read":"true","update":"true","delete":"true"},"bedtype":{"create":"true","read":"true","update":"true","delete":"true"},"bed":{"create":"true","read":"true","update":"true","delete":"true"},"bedpatient":{"create":"true","read":"true","update":"true","delete":"true"},"roompatient":{"create":"true","read":"true","update":"true","delete":"true"},"doctordays":{"create":"true","read":"true","update":"true","delete":"true"},"doctors":{"create":"true","read":"true","update":"true","delete":"true"}}";s:14:"doctor_role_id";s:1:"4";s:10:"role_title";s:5:"Admin";}'),
('f3c213eaf51229ca541fb5307a13abb9', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 1556887802, 'a:13:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"admin";s:15:"admin_full_name";s:15:"Talib Allauddin";s:8:"admin_id";s:1:"1";s:10:"last_login";s:19:"2019-05-03 11:57:18";s:7:"last_ip";s:13:"192.168.10.11";s:12:"user_role_id";s:1:"1";s:9:"clinic_id";s:1:"0";s:10:"permission";s:3242:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"clinics":{"create":"true","read":"true","update":"true","delete":"true"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"appointments":{"create":"true","read":"true","update":"true","delete":"true"},"patients":{"create":"true","read":"true","update":"true","delete":"true"},"procedures":{"create":"true","read":"true","update":"true","delete":"true"},"labtracking":{"create":"true","read":"true","update":"true","delete":"true"},"reports":{"create":"false","read":"true","update":"false","delete":"false"},"treatments":{"create":"true","read":"true","update":"true","delete":"true"},"invoices":{"create":"true","read":"true","update":"true","delete":"true"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"true"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"true"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"worktype":{"create":"true","read":"true","update":"true","delete":"true"},"work":{"create":"true","read":"true","update":"true","delete":"true"},"suppliers":{"create":"true","read":"true","update":"true","delete":"true"},"lab":{"create":"true","read":"true","update":"true","delete":"true"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"patientfiles":{"create":"true","read":"true","update":"true","delete":"true"},"expenses":{"create":"true","read":"true","update":"true","delete":"true"},"purchases":{"create":"true","read":"true","update":"true","delete":"true"},"items":{"create":"true","read":"true","update":"true","delete":"true","issue":"true"},"stocks":{"create":"true","read":"true","update":"true","delete":"true"},"accounts":{"create":"true","read":"true","update":"true","delete":"true"},"purchasedetails":{"create":"true","read":"true","update":"true","delete":"true"},"stockreleases":{"create":"true","read":"true","update":"true","delete":"true"},"employeepayroll":{"create":"true","read":"true","update":"true","delete":"true"},"employeeattendance":{"create":"true","read":"true","update":"true","delete":"true"},"tokens":{"create":"true","read":"true","update":"true","delete":"true"},"backup":{"create":"false","read":"true","update":"false","delete":"false"},"reporttype":{"create":"true","read":"true","update":"true","delete":"true"},"":{"create":"false","read":"false","update":"false","delete":"false"},"roomtype":{"create":"true","read":"true","update":"true","delete":"true"},"room":{"create":"true","read":"true","update":"true","delete":"true"},"bedtype":{"create":"true","read":"true","update":"true","delete":"true"},"bed":{"create":"true","read":"true","update":"true","delete":"true"},"bedpatient":{"create":"true","read":"true","update":"true","delete":"true"},"roompatient":{"create":"true","read":"true","update":"true","delete":"true"},"doctordays":{"create":"true","read":"true","update":"true","delete":"true"},"patientprocedures":{"create":"true","read":"true","update":"true","delete":"true"},"doctors":{"create":"true","read":"true","update":"true","delete":"true"}}";s:14:"doctor_role_id";s:1:"4";s:10:"role_title";s:11:"Super Admin";}'),
('f3d10a6f159a072027ce476bd388557d', '::1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.75 Safari/537.36', 1460993401, '');
INSERT INTO `ci_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('f6912671015d28cf22494b06c2594ab1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 1557594116, 'a:13:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"admin";s:15:"admin_full_name";s:15:"Talib Allauddin";s:8:"admin_id";s:1:"1";s:10:"last_login";s:19:"2019-05-06 03:17:31";s:7:"last_ip";s:3:"::1";s:12:"user_role_id";s:1:"1";s:9:"clinic_id";s:1:"0";s:10:"permission";s:3242:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"clinics":{"create":"true","read":"true","update":"true","delete":"true"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"appointments":{"create":"true","read":"true","update":"true","delete":"true"},"patients":{"create":"true","read":"true","update":"true","delete":"true"},"procedures":{"create":"true","read":"true","update":"true","delete":"true"},"labtracking":{"create":"true","read":"true","update":"true","delete":"true"},"reports":{"create":"false","read":"true","update":"false","delete":"false"},"treatments":{"create":"true","read":"true","update":"true","delete":"true"},"invoices":{"create":"true","read":"true","update":"true","delete":"true"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"true"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"true"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"worktype":{"create":"true","read":"true","update":"true","delete":"true"},"work":{"create":"true","read":"true","update":"true","delete":"true"},"suppliers":{"create":"true","read":"true","update":"true","delete":"true"},"lab":{"create":"true","read":"true","update":"true","delete":"true"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"patientfiles":{"create":"true","read":"true","update":"true","delete":"true"},"expenses":{"create":"true","read":"true","update":"true","delete":"true"},"purchases":{"create":"true","read":"true","update":"true","delete":"true"},"items":{"create":"true","read":"true","update":"true","delete":"true","issue":"true"},"stocks":{"create":"true","read":"true","update":"true","delete":"true"},"accounts":{"create":"true","read":"true","update":"true","delete":"true"},"purchasedetails":{"create":"true","read":"true","update":"true","delete":"true"},"stockreleases":{"create":"true","read":"true","update":"true","delete":"true"},"employeepayroll":{"create":"true","read":"true","update":"true","delete":"true"},"employeeattendance":{"create":"true","read":"true","update":"true","delete":"true"},"tokens":{"create":"true","read":"true","update":"true","delete":"true"},"backup":{"create":"false","read":"true","update":"false","delete":"false"},"reporttype":{"create":"true","read":"true","update":"true","delete":"true"},"":{"create":"false","read":"false","update":"false","delete":"false"},"roomtype":{"create":"true","read":"true","update":"true","delete":"true"},"room":{"create":"true","read":"true","update":"true","delete":"true"},"bedtype":{"create":"true","read":"true","update":"true","delete":"true"},"bed":{"create":"true","read":"true","update":"true","delete":"true"},"bedpatient":{"create":"true","read":"true","update":"true","delete":"true"},"roompatient":{"create":"true","read":"true","update":"true","delete":"true"},"doctordays":{"create":"true","read":"true","update":"true","delete":"true"},"patientprocedures":{"create":"true","read":"true","update":"true","delete":"true"},"doctors":{"create":"true","read":"true","update":"true","delete":"true"}}";s:14:"doctor_role_id";s:1:"4";s:10:"role_title";s:11:"Super Admin";}'),
('f7799b6d66e9e6e44568c34b10a2a827', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 1556887369, ''),
('f8b4a5ca4458950cd0cdf682537cece1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 1556274561, 'a:13:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"rafay";s:15:"admin_full_name";s:11:"Abdur Rafay";s:8:"admin_id";s:1:"6";s:10:"last_login";s:19:"2019-04-25 16:09:51";s:7:"last_ip";s:3:"::1";s:12:"user_role_id";s:1:"2";s:9:"clinic_id";s:1:"1";s:10:"permission";s:3166:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"clinics":{"create":"false","read":"false","update":"false","delete":"false"},"rolemanagement":{"create":"false","read":"false","update":"false","delete":"false"},"appointments":{"create":"true","read":"true","update":"true","delete":"true"},"patients":{"create":"true","read":"true","update":"true","delete":"true"},"procedures":{"create":"true","read":"true","update":"true","delete":"true"},"labtracking":{"create":"true","read":"true","update":"true","delete":"true"},"reports":{"create":"false","read":"true","update":"false","delete":"false"},"treatments":{"create":"true","read":"true","update":"true","delete":"true"},"invoices":{"create":"true","read":"true","update":"true","delete":"true"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"true"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"true"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"worktype":{"create":"true","read":"true","update":"true","delete":"true"},"work":{"create":"true","read":"true","update":"true","delete":"true"},"suppliers":{"create":"true","read":"true","update":"true","delete":"true"},"lab":{"create":"true","read":"true","update":"true","delete":"true"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"patientfiles":{"create":"true","read":"true","update":"true","delete":"true"},"expenses":{"create":"true","read":"true","update":"true","delete":"true"},"purchases":{"create":"true","read":"true","update":"true","delete":"true"},"items":{"create":"true","read":"true","update":"true","delete":"true","issue":"true"},"stocks":{"create":"true","read":"true","update":"true","delete":"true"},"accounts":{"create":"true","read":"true","update":"true","delete":"true"},"purchasedetails":{"create":"true","read":"true","update":"true","delete":"true"},"stockreleases":{"create":"true","read":"true","update":"true","delete":"true"},"employeepayroll":{"create":"true","read":"true","update":"true","delete":"true"},"employeeattendance":{"create":"true","read":"true","update":"true","delete":"true"},"tokens":{"create":"true","read":"true","update":"true","delete":"true"},"backup":{"create":"false","read":"true","update":"false","delete":"false"},"reporttype":{"create":"true","read":"true","update":"true","delete":"true"},"":{"create":"false","read":"false","update":"false","delete":"false"},"roomtype":{"create":"true","read":"true","update":"true","delete":"true"},"room":{"create":"true","read":"true","update":"true","delete":"true"},"bedtype":{"create":"true","read":"true","update":"true","delete":"true"},"bed":{"create":"true","read":"true","update":"true","delete":"true"},"bedpatient":{"create":"true","read":"true","update":"true","delete":"true"},"roompatient":{"create":"true","read":"true","update":"true","delete":"true"},"doctordays":{"create":"true","read":"true","update":"true","delete":"true"},"doctors":{"create":"true","read":"true","update":"true","delete":"true"}}";s:14:"doctor_role_id";s:1:"4";s:10:"role_title";s:5:"Admin";}'),
('fa97f04112710f0d91f3329df1fb3d97', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 1556848617, 'a:13:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"admin";s:15:"admin_full_name";s:15:"Talib Allauddin";s:8:"admin_id";s:1:"1";s:10:"last_login";s:19:"2019-04-28 17:45:05";s:7:"last_ip";s:3:"::1";s:12:"user_role_id";s:1:"1";s:9:"clinic_id";s:1:"0";s:10:"permission";s:2565:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"clinics":{"create":"true","read":"true","update":"true","delete":"true"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"appointments":{"create":"true","read":"true","update":"true","delete":"true"},"doctors":{"create":"true","read":"true","update":"true","delete":"true"},"patients":{"create":"true","read":"true","update":"true","delete":"true"},"procedures":{"create":"true","read":"true","update":"true","delete":"true"},"labtracking":{"create":"true","read":"true","update":"true","delete":"true"},"reports":{"create":"false","read":"true","update":"false","delete":"false"},"treatments":{"create":"true","read":"true","update":"true","delete":"true"},"invoices":{"create":"true","read":"true","update":"true","delete":"true"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"true"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"true"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"worktype":{"create":"true","read":"true","update":"true","delete":"true"},"work":{"create":"true","read":"true","update":"true","delete":"true"},"suppliers":{"create":"true","read":"true","update":"true","delete":"true"},"lab":{"create":"true","read":"true","update":"true","delete":"true"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"patientfiles":{"create":"true","read":"true","update":"true","delete":"true"},"expenses":{"create":"true","read":"true","update":"true","delete":"true"},"purchases":{"create":"true","read":"true","update":"true","delete":"true"},"items":{"create":"true","read":"true","update":"true","delete":"true","issue":"true"},"stocks":{"create":"true","read":"true","update":"true","delete":"true"},"accounts":{"create":"true","read":"true","update":"true","delete":"true"},"purchasedetails":{"create":"true","read":"true","update":"true","delete":"true"},"stockreleases":{"create":"true","read":"true","update":"true","delete":"true"},"employeepayroll":{"create":"true","read":"true","update":"true","delete":"true"},"employeeattendance":{"create":"true","read":"true","update":"true","delete":"true"},"tokens":{"create":"true","read":"true","update":"true","delete":"true"},"backup":{"create":"false","read":"true","update":"false","delete":"false"},"reporttype":{"create":"true","read":"true","update":"true","delete":"true"}}";s:14:"doctor_role_id";s:1:"4";s:10:"role_title";s:11:"Super Admin";}'),
('fdbcad0bbdf725b96648006fa1bb2590', '::1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36', 1461074019, 'a:10:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"admin";s:15:"admin_full_name";s:5:"5-STI";s:8:"admin_id";s:1:"1";s:10:"last_login";s:19:"2016-04-18 16:57:11";s:7:"last_ip";s:3:"::1";s:10:"permission";s:1714:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"customers":{"create":"true","read":"true","update":"true","delete":"true"},"orders":{"create":"true","read":"true","update":"true","delete":"true","history":"false","fullaccess":"false"},"payments":{"create":"false","read":"false","update":"false","delete":"false"},"invoice":{"create":"true","read":"true","update":"true","delete":"true"},"menu":{"create":"true","read":"true","update":"true","delete":"true"},"pages":{"create":"true","read":"true","update":"true","delete":"true"},"about":{"create":"true","read":"true","update":"true","delete":"true"},"services":{"create":"true","read":"true","update":"true","delete":"true"},"gallery":{"create":"true","read":"true","update":"true","delete":"true"},"testimonial":{"create":"true","read":"true","update":"true","delete":"true"},"sliders":{"create":"true","read":"true","update":"true","delete":"true"},"slider":{"create":"true","read":"true","update":"true","delete":"true"},"products":{"create":"true","read":"true","update":"true","delete":"true"},"productcategory":{"create":"true","read":"true","update":"true","delete":"true"},"sales":{"create":"false","read":"true","update":"false","delete":"true","history":"true","fullaccess":"true"},"contact":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"payment":{"create":"true","read":"true","update":"true","delete":"true","history":"true","fullaccess":"true"}}";s:10:"role_title";s:5:"Admin";}'),
('fdee6e241cacabce0dca792bfa1928b8', '192.168.10.11', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 1556871579, 'a:13:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"rafay";s:15:"admin_full_name";s:11:"Abdur Rafay";s:8:"admin_id";s:1:"6";s:10:"last_login";s:19:"2019-05-03 12:37:10";s:7:"last_ip";s:13:"192.168.10.11";s:12:"user_role_id";s:1:"2";s:9:"clinic_id";s:1:"1";s:10:"permission";s:3314:"{"admins":{"create":"false","read":"true","update":"true","delete":"false","fullaccess":"false"},"clinics":{"create":"false","read":"true","update":"true","delete":"false"},"rolemanagement":{"create":"false","read":"true","update":"true","delete":"false"},"appointments":{"create":"false","read":"true","update":"true","delete":"false"},"patients":{"create":"false","read":"true","update":"true","delete":"false"},"procedures":{"create":"false","read":"true","update":"true","delete":"false"},"labtracking":{"create":"false","read":"true","update":"true","delete":"false"},"reports":{"create":"false","read":"false","update":"false","delete":"false"},"treatments":{"create":"false","read":"true","update":"true","delete":"false"},"invoices":{"create":"false","read":"true","update":"true","delete":"false"},"healthrecord":{"create":"false","read":"true","update":"true","delete":"false"},"dentalchart":{"create":"false","read":"true","update":"true","delete":"false"},"doctortiming":{"create":"false","read":"true","update":"true","delete":"false"},"useractivitylog":{"create":"false","read":"false","update":"false","delete":"false"},"worktype":{"create":"false","read":"false","update":"true","delete":"false"},"work":{"create":"false","read":"false","update":"true","delete":"false"},"suppliers":{"create":"false","read":"false","update":"true","delete":"false"},"lab":{"create":"false","read":"false","update":"true","delete":"false"},"site":{"create":"false","read":"false","update":"true","delete":"false"},"patientfiles":{"create":"false","read":"false","update":"true","delete":"false"},"expenses":{"create":"false","read":"false","update":"true","delete":"false"},"purchases":{"create":"false","read":"false","update":"true","delete":"false"},"items":{"create":"false","read":"false","update":"true","delete":"false","issue":"false"},"stocks":{"create":"false","read":"false","update":"true","delete":"false"},"accounts":{"create":"false","read":"false","update":"true","delete":"false"},"purchasedetails":{"create":"false","read":"false","update":"true","delete":"false"},"stockreleases":{"create":"false","read":"false","update":"true","delete":"false"},"employeepayroll":{"create":"false","read":"false","update":"true","delete":"false"},"employeeattendance":{"create":"false","read":"false","update":"true","delete":"false"},"tokens":{"create":"false","read":"false","update":"true","delete":"false"},"backup":{"create":"false","read":"false","update":"false","delete":"false"},"reporttype":{"create":"false","read":"false","update":"true","delete":"false"},"":{"create":"false","read":"false","update":"false","delete":"false"},"roomtype":{"create":"false","read":"false","update":"true","delete":"false"},"room":{"create":"false","read":"false","update":"true","delete":"false"},"bedtype":{"create":"false","read":"false","update":"true","delete":"false"},"bed":{"create":"false","read":"false","update":"true","delete":"false"},"bedpatient":{"create":"false","read":"false","update":"true","delete":"false"},"roompatient":{"create":"false","read":"false","update":"true","delete":"false"},"doctordays":{"create":"false","read":"false","update":"true","delete":"false"},"patientprocedures":{"create":"false","read":"false","update":"true","delete":"false"},"doctors":{"read":"true","update":"true"}}";s:14:"doctor_role_id";s:1:"4";s:10:"role_title";s:5:"Admin";}'),
('ff1d8a15b1652c13d0684e92c20bf083', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 1557946334, 'a:14:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"rafay";s:15:"admin_full_name";s:11:"Abdur Rafay";s:8:"admin_id";s:1:"6";s:10:"last_login";s:19:"2019-05-14 22:18:29";s:7:"last_ip";s:3:"::1";s:12:"user_role_id";s:1:"2";s:9:"clinic_id";s:1:"1";s:10:"permission";s:3263:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"clinics":{"create":"false","read":"false","update":"false","delete":"false"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"appointments":{"create":"true","read":"true","update":"true","delete":"true"},"patients":{"create":"true","read":"true","update":"true","delete":"true","profile":"true"},"procedures":{"create":"true","read":"true","update":"true","delete":"true"},"labtracking":{"create":"true","read":"true","update":"true","delete":"true"},"reports":{"create":"false","read":"true","update":"false","delete":"false"},"treatments":{"create":"true","read":"true","update":"true","delete":"true"},"invoices":{"create":"true","read":"true","update":"true","delete":"true"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"true"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"true"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"worktype":{"create":"true","read":"true","update":"true","delete":"true"},"work":{"create":"true","read":"true","update":"true","delete":"true"},"suppliers":{"create":"true","read":"true","update":"true","delete":"true"},"lab":{"create":"true","read":"true","update":"true","delete":"true"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"patientfiles":{"create":"true","read":"true","update":"true","delete":"true"},"expenses":{"create":"true","read":"true","update":"true","delete":"true"},"purchases":{"create":"true","read":"true","update":"true","delete":"true"},"items":{"create":"true","read":"true","update":"true","delete":"true","issue":"true"},"stocks":{"create":"true","read":"true","update":"true","delete":"true"},"accounts":{"create":"true","read":"true","update":"true","delete":"true"},"purchasedetails":{"create":"true","read":"true","update":"true","delete":"true"},"stockreleases":{"create":"true","read":"true","update":"true","delete":"true"},"employeepayroll":{"create":"true","read":"true","update":"true","delete":"true"},"employeeattendance":{"create":"true","read":"true","update":"true","delete":"true"},"tokens":{"create":"true","read":"true","update":"true","delete":"true"},"backup":{"create":"false","read":"true","update":"false","delete":"false"},"reporttype":{"create":"true","read":"true","update":"true","delete":"true"},"":{"create":"false","read":"false","update":"false","delete":"false"},"roomtype":{"create":"true","read":"true","update":"true","delete":"true"},"room":{"create":"true","read":"true","update":"true","delete":"true"},"bedtype":{"create":"true","read":"true","update":"true","delete":"true"},"bed":{"create":"true","read":"true","update":"true","delete":"true"},"bedpatient":{"create":"true","read":"true","update":"true","delete":"true"},"roompatient":{"create":"true","read":"true","update":"true","delete":"true"},"doctordays":{"create":"true","read":"true","update":"true","delete":"true"},"patientprocedures":{"create":"true","read":"true","update":"true","delete":"true"},"doctors":{"create":"true","read":"true","update":"true","delete":"true"}}";s:14:"doctor_role_id";s:1:"4";s:10:"role_title";s:5:"Admin";s:8:"per_page";s:3:"100";}'),
('ff3ab3bf5a0547200bb03133caba0e1f', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', 1554998126, 'a:12:{s:9:"user_data";s:0:"";s:10:"admin_auth";s:5:"allow";s:10:"admin_role";N;s:15:"admin_user_name";s:5:"admin";s:15:"admin_full_name";s:15:"Talib Allauddin";s:8:"admin_id";s:1:"1";s:10:"last_login";s:19:"2019-04-11 14:07:16";s:7:"last_ip";s:3:"::1";s:12:"user_role_id";s:1:"1";s:9:"clinic_id";s:1:"0";s:10:"permission";s:2412:"{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"clinics":{"create":"true","read":"true","update":"true","delete":"true"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"appointments":{"create":"true","read":"true","update":"true","delete":"true"},"doctors":{"create":"true","read":"true","update":"true","delete":"true"},"patients":{"create":"true","read":"true","update":"true","delete":"true"},"procedures":{"create":"true","read":"true","update":"true","delete":"true"},"labtracking":{"create":"true","read":"true","update":"true","delete":"true"},"reports":{"create":"false","read":"true","update":"false","delete":"false"},"treatments":{"create":"true","read":"true","update":"true","delete":"true"},"invoices":{"create":"true","read":"true","update":"true","delete":"true"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"true"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"true"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"worktype":{"create":"true","read":"true","update":"true","delete":"true"},"work":{"create":"true","read":"true","update":"true","delete":"true"},"suppliers":{"create":"true","read":"true","update":"true","delete":"true"},"lab":{"create":"true","read":"true","update":"true","delete":"true"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"patientfiles":{"create":"true","read":"true","update":"true","delete":"true"},"expenses":{"create":"true","read":"true","update":"true","delete":"true"},"purchases":{"create":"true","read":"true","update":"true","delete":"true"},"items":{"create":"true","read":"true","update":"true","delete":"true","issue":"true"},"stocks":{"create":"true","read":"true","update":"true","delete":"true"},"accounts":{"create":"true","read":"true","update":"true","delete":"true"},"purchasedetails":{"create":"true","read":"true","update":"true","delete":"true"},"stockreleases":{"create":"true","read":"true","update":"true","delete":"true"},"employeepayroll":{"create":"true","read":"true","update":"true","delete":"true"},"employeeattendance":{"create":"true","read":"true","update":"true","delete":"true"},"tokens":{"create":"true","read":"true","update":"true","delete":"true"}}";s:10:"role_title";s:11:"Super Admin";}');

-- --------------------------------------------------------

--
-- Table structure for table `clinics`
--

CREATE TABLE `clinics` (
  `clinic_id` int(11) NOT NULL,
  `clinic_name` varchar(150) NOT NULL,
  `clinic_owner` varchar(150) NOT NULL,
  `clinic_address` text NOT NULL,
  `clinic_phone1` varchar(20) NOT NULL,
  `clinic_phone2` varchar(20) NOT NULL,
  `clinic_email1` varchar(120) NOT NULL,
  `clinic_email2` varchar(120) NOT NULL,
  `clinic_mobile1` varchar(20) NOT NULL,
  `clinic_mobile2` varchar(20) NOT NULL,
  `clinic_purchase_date` date NOT NULL,
  `clinic_subs_plan_id` int(11) NOT NULL,
  `clinic_website` varchar(180) NOT NULL,
  `clinic_subs_token` varchar(255) NOT NULL,
  `clinic_refered_by` varchar(150) NOT NULL,
  `clinic_added` datetime NOT NULL,
  `clinic_added_by` int(11) NOT NULL,
  `clinic_updated` datetime NOT NULL,
  `clinic_updated_by` int(11) NOT NULL,
  `clinic_deleted` char(1) NOT NULL,
  `clinic_status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `clinics`
--

INSERT INTO `clinics` (`clinic_id`, `clinic_name`, `clinic_owner`, `clinic_address`, `clinic_phone1`, `clinic_phone2`, `clinic_email1`, `clinic_email2`, `clinic_mobile1`, `clinic_mobile2`, `clinic_purchase_date`, `clinic_subs_plan_id`, `clinic_website`, `clinic_subs_token`, `clinic_refered_by`, `clinic_added`, `clinic_added_by`, `clinic_updated`, `clinic_updated_by`, `clinic_deleted`, `clinic_status`) VALUES
(1, 'Rafay Clinic Center', 'Abdur Rafay', 'johor mor karachi', '03330200979', '', 'm.abdurrafay04@gmail.com', '', '03330505458', '', '2019-04-18', 1, 'www.clinic.com', 'one only', '', '2019-04-18 15:11:20', 1, '2019-04-18 15:11:20', 1, '0', 'Published'),
(2, 'My Clinic Center', 'Anwar Ul Qamar', '', '03215648932', '', 'Qamar@gmail.com', '', '03456782190', '', '2019-04-18', 1, 'www.my.com', 'No one', '', '2019-04-18 16:32:36', 1, '2019-04-18 16:32:36', 1, '0', 'Published'),
(3, 'Jabar hospital', 'Jabar khan', 'Fiaz colony', '03215648932', '', 'Jabar123@gmail.com', '', '03456782190', '', '2019-04-18', 1, 'www.my.com', 'Only me', '', '2019-04-18 17:33:49', 1, '2019-04-18 17:33:49', 1, '0', 'Published');

-- --------------------------------------------------------

--
-- Table structure for table `doctor_timing`
--

CREATE TABLE `doctor_timing` (
  `dt_id` int(11) NOT NULL,
  `dt_day_id` int(11) NOT NULL,
  `dt_doctor_id` int(11) NOT NULL,
  `dt_status` varchar(15) NOT NULL,
  `dt_created_by` int(11) NOT NULL,
  `dt_updated_by` int(11) NOT NULL,
  `dt_start_time` time NOT NULL,
  `dt_end_time` time NOT NULL,
  `dt_added` datetime NOT NULL,
  `dt_updated` datetime NOT NULL,
  `dt_is_deleted` char(1) NOT NULL,
  `dt_clinic_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `doctor_timing`
--

INSERT INTO `doctor_timing` (`dt_id`, `dt_day_id`, `dt_doctor_id`, `dt_status`, `dt_created_by`, `dt_updated_by`, `dt_start_time`, `dt_end_time`, `dt_added`, `dt_updated`, `dt_is_deleted`, `dt_clinic_id`) VALUES
(1, 1, 10, 'Available', 6, 6, '02:57:00', '02:57:00', '2019-05-03 06:58:07', '2019-05-03 06:58:07', '0', 1),
(2, 2, 10, 'Available', 6, 6, '03:10:00', '03:10:00', '2019-05-03 06:58:07', '2019-05-03 06:58:07', '0', 1),
(9, 3, 10, 'Not Available', 6, 6, '03:10:00', '03:10:00', '2019-05-03 14:42:21', '2019-05-03 14:42:21', '0', 1),
(10, 4, 10, 'Not Available', 6, 6, '03:10:00', '03:10:00', '2019-05-03 15:08:15', '2019-05-03 15:08:15', '0', 1),
(11, 5, 10, 'Not Available', 6, 6, '03:10:00', '03:10:00', '2019-05-03 15:08:15', '2019-05-03 15:08:15', '0', 1),
(12, 6, 10, 'Not Available', 6, 6, '03:10:00', '03:10:00', '2019-05-03 15:08:15', '2019-05-03 15:08:15', '0', 1),
(13, 7, 10, 'Not Available', 6, 6, '03:10:00', '03:10:00', '2019-05-03 15:08:15', '2019-05-03 15:08:15', '0', 1);

-- --------------------------------------------------------

--
-- Table structure for table `drug_units`
--

CREATE TABLE `drug_units` (
  `du_id` int(11) NOT NULL,
  `du_name` varchar(180) NOT NULL,
  `du_added` datetime NOT NULL,
  `du_updated` datetime NOT NULL,
  `du_created_by` int(11) NOT NULL,
  `du_updated_by` int(11) NOT NULL,
  `du_is_deleted` char(1) NOT NULL,
  `du_status` varchar(15) NOT NULL,
  `du_clinic_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `employee_attendance`
--

CREATE TABLE `employee_attendance` (
  `ea_id` int(11) NOT NULL,
  `ea_clinic_id` int(11) NOT NULL,
  `ea_user_id` int(11) NOT NULL COMMENT 'User',
  `ea_checkin` time NOT NULL COMMENT 'Check In',
  `ea_checkout` time NOT NULL COMMENT 'Check Out',
  `ea_attendance` char(1) NOT NULL COMMENT 'Attendance',
  `ea_absent_reason` text NOT NULL COMMENT 'Absent Reason',
  `ea_date` date NOT NULL COMMENT 'Date',
  `ea_added` datetime NOT NULL,
  `ea_updated` datetime NOT NULL,
  `ea_created_by` int(11) NOT NULL,
  `ea_updated_by` int(11) NOT NULL,
  `ea_is_deleted` char(1) NOT NULL,
  `ea_status` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_attendance`
--

INSERT INTO `employee_attendance` (`ea_id`, `ea_clinic_id`, `ea_user_id`, `ea_checkin`, `ea_checkout`, `ea_attendance`, `ea_absent_reason`, `ea_date`, `ea_added`, `ea_updated`, `ea_created_by`, `ea_updated_by`, `ea_is_deleted`, `ea_status`) VALUES
(1, 1, 0, '11:53:05', '00:00:00', '0', '0', '1970-01-01', '2019-04-23 11:53:05', '2019-04-23 11:53:05', 6, 6, '0', 'Enabled'),
(2, 1, 0, '11:53:07', '00:00:00', '0', '0', '1970-01-01', '2019-04-23 11:53:07', '2019-04-23 11:53:07', 6, 6, '0', 'Enabled'),
(3, 1, 0, '11:54:06', '00:00:00', '0', '0', '1970-01-01', '2019-04-23 11:54:06', '2019-04-23 11:54:06', 6, 6, '0', 'Enabled'),
(4, 1, 0, '06:06:36', '00:00:00', '0', '0', '1970-01-01', '2019-05-13 06:06:36', '2019-05-13 06:06:36', 6, 6, '0', 'Enabled'),
(5, 1, 0, '00:00:00', '05:00:00', 'P', '', '1970-01-01', '2019-05-13 06:08:31', '2019-05-13 06:08:31', 6, 6, '0', 'Enabled'),
(6, 1, 0, '06:08:49', '00:00:00', '0', '0', '1970-01-01', '2019-05-13 06:08:49', '2019-05-13 06:08:49', 6, 6, '0', 'Enabled'),
(7, 1, 0, '00:00:00', '05:00:00', 'P', '', '1970-01-01', '2019-05-13 06:08:58', '2019-05-13 06:08:58', 6, 6, '0', 'Enabled'),
(8, 1, 0, '06:11:33', '00:00:00', '0', '0', '1970-01-01', '2019-05-13 06:11:33', '2019-05-13 06:11:33', 6, 6, '0', 'Enabled'),
(9, 1, 0, '00:00:00', '05:00:00', 'P', '', '1970-01-01', '2019-05-13 06:11:44', '2019-05-13 06:11:44', 6, 6, '0', 'Enabled'),
(10, 1, 0, '21:50:06', '00:00:00', '0', '0', '1970-01-01', '2019-05-15 21:50:06', '2019-05-15 21:50:06', 6, 6, '0', 'Enabled'),
(11, 1, 0, '23:04:25', '00:00:00', '0', '0', '1970-01-01', '2019-05-15 23:04:25', '2019-05-15 23:04:25', 6, 6, '0', 'Enabled'),
(12, 1, 0, '00:00:00', '05:00:00', 'P', '', '1970-01-01', '2019-05-15 23:04:39', '2019-05-15 23:04:39', 6, 6, '0', 'Enabled'),
(13, 1, 0, '13:38:36', '00:00:00', '0', '0', '1970-01-01', '2019-05-18 13:38:36', '2019-05-18 13:38:36', 6, 6, '0', 'Enabled'),
(14, 1, 0, '00:00:00', '05:00:00', 'P', '', '1970-01-01', '2019-05-18 13:39:01', '2019-05-18 13:39:01', 6, 6, '0', 'Enabled');

-- --------------------------------------------------------

--
-- Table structure for table `employee_payroll`
--

CREATE TABLE `employee_payroll` (
  `ep_id` int(11) NOT NULL,
  `ep_clinic_id` int(11) NOT NULL,
  `ep_user_id` int(11) NOT NULL COMMENT 'Employee',
  `ep_total_salary` int(11) NOT NULL COMMENT 'Total Salary',
  `ep_net_salary` int(11) NOT NULL COMMENT 'Net Salary',
  `ep_deduction` int(11) NOT NULL COMMENT 'Deduction',
  `ep_commission` int(11) NOT NULL COMMENT 'Commission',
  `ep_month` date NOT NULL COMMENT 'Month',
  `ep_added` datetime NOT NULL,
  `ep_updated` datetime NOT NULL,
  `ep_created_by` int(11) NOT NULL,
  `ep_updated_by` int(11) NOT NULL,
  `ep_is_deleted` char(1) NOT NULL,
  `ep_status` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `expenses`
--

CREATE TABLE `expenses` (
  `e_id` int(11) NOT NULL,
  `e_clinic_id` int(11) NOT NULL,
  `e_account_id` int(11) NOT NULL COMMENT 'Account,foreign,accounts,a_,a_id',
  `e_description` text NOT NULL COMMENT 'Description',
  `e_amount` double NOT NULL COMMENT 'Amount',
  `e_added` datetime NOT NULL,
  `e_updated` datetime NOT NULL,
  `e_created_by` int(11) NOT NULL,
  `e_updated_by` int(11) NOT NULL,
  `e_is_deleted` char(1) NOT NULL,
  `e_status` varchar(15) NOT NULL,
  `e_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `expenses`
--

INSERT INTO `expenses` (`e_id`, `e_clinic_id`, `e_account_id`, `e_description`, `e_amount`, `e_added`, `e_updated`, `e_created_by`, `e_updated_by`, `e_is_deleted`, `e_status`, `e_date`) VALUES
(1, 1, 0, 'hi hello\r\n', 500, '2019-05-15 23:17:00', '2019-05-15 23:17:00', 6, 6, '0', 'Enabled', '2019-05-01'),
(2, 1, 0, '', 500, '2019-05-15 23:17:36', '2019-05-15 23:17:36', 6, 6, '0', 'Enabled', '2019-05-31'),
(3, 1, 0, '', 500, '2019-05-18 13:38:15', '2019-05-18 13:38:15', 6, 6, '0', 'Enabled', '2019-05-18');

-- --------------------------------------------------------

--
-- Table structure for table `facilities`
--

CREATE TABLE `facilities` (
  `facility_id` int(11) NOT NULL,
  `facility_name` varchar(120) NOT NULL,
  `facility_is_deleted` char(1) NOT NULL,
  `facility_status` varchar(8) NOT NULL,
  `facility_created` datetime NOT NULL,
  `facility_updated` datetime NOT NULL,
  `facility_updated_by` int(11) NOT NULL,
  `facility_created_by` int(11) NOT NULL,
  `facility_clinic_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `facilities`
--

INSERT INTO `facilities` (`facility_id`, `facility_name`, `facility_is_deleted`, `facility_status`, `facility_created`, `facility_updated`, `facility_updated_by`, `facility_created_by`, `facility_clinic_id`) VALUES
(1, 'Room', '0', 'Enable', '2019-05-16 07:15:20', '2019-05-16 07:28:01', 6, 6, 1),
(2, 'Medicine', '0', 'Enabled', '2019-05-16 07:27:56', '2019-05-16 07:27:56', 6, 6, 1),
(4, 'Bed', '0', 'Enabled', '2019-05-16 21:42:45', '2019-05-16 21:42:45', 6, 6, 1),
(5, 'a.c', '0', 'Enabled', '2019-05-18 13:33:30', '2019-05-18 13:33:30', 6, 6, 1);

-- --------------------------------------------------------

--
-- Table structure for table `frequencies`
--

CREATE TABLE `frequencies` (
  `f_id` int(11) NOT NULL,
  `f_name` varchar(120) NOT NULL,
  `f_added` datetime NOT NULL,
  `f_updated` datetime NOT NULL,
  `f_created_by` int(11) NOT NULL,
  `f_updated_by` int(11) NOT NULL,
  `f_is_deleted` char(1) NOT NULL,
  `f_status` varchar(15) NOT NULL,
  `f_clinic_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `i_id` int(11) NOT NULL,
  `i_clinic_id` int(11) NOT NULL,
  `i_name` varchar(160) NOT NULL COMMENT 'Name',
  `i_description` text NOT NULL COMMENT 'Description',
  `i_unit_price` double NOT NULL COMMENT 'Unit Price',
  `i_added` datetime NOT NULL,
  `i_updated` datetime NOT NULL,
  `i_created_by` int(11) NOT NULL,
  `i_updated_by` int(11) NOT NULL,
  `i_is_deleted` char(1) NOT NULL,
  `i_status` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`i_id`, `i_clinic_id`, `i_name`, `i_description`, `i_unit_price`, `i_added`, `i_updated`, `i_created_by`, `i_updated_by`, `i_is_deleted`, `i_status`) VALUES
(1, 1, 'gun', '', 500, '2019-05-15 23:45:43', '2019-05-15 23:45:43', 6, 6, '0', 'Enabled');

-- --------------------------------------------------------

--
-- Table structure for table `lab`
--

CREATE TABLE `lab` (
  `lab_id` int(11) NOT NULL,
  `lab_name` varchar(150) NOT NULL,
  `lab_contact_person` varchar(120) NOT NULL,
  `lab_phone` varchar(15) NOT NULL,
  `lab_email` varchar(150) NOT NULL,
  `lab_address` text NOT NULL,
  `lab_is_deleted` char(1) NOT NULL,
  `lab_added` datetime NOT NULL,
  `lab_updated` datetime NOT NULL,
  `lab_created_by` int(11) NOT NULL,
  `lab_updated_by` int(11) NOT NULL,
  `lab_status` varchar(15) NOT NULL,
  `lab_clinic_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lab`
--

INSERT INTO `lab` (`lab_id`, `lab_name`, `lab_contact_person`, `lab_phone`, `lab_email`, `lab_address`, `lab_is_deleted`, `lab_added`, `lab_updated`, `lab_created_by`, `lab_updated_by`, `lab_status`, `lab_clinic_id`) VALUES
(1, 'Agha Khan', 'A. Rafay', '123123123', 'rafay@live.com', '', '0', '2019-04-19 22:25:54', '2019-04-19 22:25:54', 6, 6, 'Enable', 1),
(2, 'dow lad', 'wahaj', '03333785647', 'wahaj@gmail.com', 'Karachi', '0', '2019-05-18 13:32:13', '2019-05-18 13:32:13', 6, 6, 'Enable', 1);

-- --------------------------------------------------------

--
-- Table structure for table `labtracking`
--

CREATE TABLE `labtracking` (
  `lt_id` int(11) NOT NULL,
  `lt_lab_id` int(11) NOT NULL,
  `lt_due_date` date NOT NULL,
  `lt_work_type_id` int(11) NOT NULL,
  `lt_work_id` int(11) NOT NULL,
  `lt_shade` varchar(150) NOT NULL,
  `lt_comment` text NOT NULL,
  `lt_clinic_id` int(11) NOT NULL,
  `lt_added` datetime NOT NULL,
  `lt_updated` datetime NOT NULL,
  `lt_created_by` int(11) NOT NULL,
  `lt_updated_by` int(11) NOT NULL,
  `lt_is_deleted` char(1) NOT NULL,
  `lt_status` varchar(15) NOT NULL,
  `lt_patient_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `labtracking`
--

INSERT INTO `labtracking` (`lt_id`, `lt_lab_id`, `lt_due_date`, `lt_work_type_id`, `lt_work_id`, `lt_shade`, `lt_comment`, `lt_clinic_id`, `lt_added`, `lt_updated`, `lt_created_by`, `lt_updated_by`, `lt_is_deleted`, `lt_status`, `lt_patient_id`) VALUES
(1, 1, '2019-04-19', 1, 1, '', '', 1, '2019-04-19 22:26:40', '2019-04-19 22:26:40', 6, 6, '0', 'Enable', 6),
(2, 1, '2019-04-22', 1, 1, '', '', 1, '2019-04-22 18:16:35', '2019-04-22 18:16:35', 6, 6, '0', 'Enable', 2),
(3, 1, '2019-04-22', 1, 1, '', '', 1, '2019-04-22 18:19:34', '2019-04-22 18:19:34', 6, 6, '0', 'Enable', 2),
(4, 2, '2019-05-18', 2, 2, 'Blue', '', 1, '2019-05-18 13:35:49', '2019-05-18 13:35:49', 6, 6, '0', 'Enable', 6);

-- --------------------------------------------------------

--
-- Table structure for table `medical_conditions`
--

CREATE TABLE `medical_conditions` (
  `mc_id` int(11) NOT NULL,
  `mc_name` varchar(120) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `medical_conditions`
--

INSERT INTO `medical_conditions` (`mc_id`, `mc_name`) VALUES
(1, 'Blood Pressure'),
(2, 'Asthma'),
(3, 'Jaundice'),
(4, 'Bleeding Problem'),
(5, 'Hepatitis'),
(6, 'Diabetics'),
(7, 'Kidney Disease'),
(8, 'Gastric/PUD'),
(9, 'Allergy_Penicllin'),
(10, 'Alergy to Sulphers'),
(11, 'Alergy_Anaesthetic'),
(12, 'Pregnency'),
(13, 'Oral Contraceptive');

-- --------------------------------------------------------

--
-- Table structure for table `medicines`
--

CREATE TABLE `medicines` (
  `m_id` int(11) NOT NULL,
  `m_name` varchar(180) NOT NULL,
  `m_strength` varchar(25) NOT NULL,
  `m_drug_unit_id` int(11) NOT NULL,
  `m_added` datetime NOT NULL,
  `m_updated` datetime NOT NULL,
  `m_created_by` int(11) NOT NULL,
  `m_updated_by` int(11) NOT NULL,
  `m_is_deleted` char(1) NOT NULL,
  `m_status` varchar(15) NOT NULL,
  `m_clinic_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `patients`
--

CREATE TABLE `patients` (
  `patient_id` int(11) NOT NULL,
  `patient_mr_id` varchar(15) NOT NULL COMMENT 'master record id',
  `patient_name` varchar(255) NOT NULL,
  `patient_email` text NOT NULL,
  `patient_phone1` varchar(25) NOT NULL,
  `patient_phone2` varchar(25) NOT NULL,
  `patient_phone3` varchar(25) NOT NULL,
  `patient_gender` varchar(15) NOT NULL,
  `patient_birth_date` date NOT NULL,
  `patient_age` int(11) NOT NULL,
  `patient_address` text NOT NULL,
  `patient_refered_by` varchar(255) NOT NULL,
  `patient_photo` text NOT NULL,
  `patient_added` datetime NOT NULL,
  `patient_created_by` int(11) NOT NULL,
  `patient_updated` datetime NOT NULL,
  `patient_modified_by` int(11) NOT NULL,
  `patient_status` varchar(15) NOT NULL,
  `patient_is_deleted` char(1) NOT NULL,
  `patient_clinic_id` int(11) NOT NULL,
  `patient_image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `patients`
--

INSERT INTO `patients` (`patient_id`, `patient_mr_id`, `patient_name`, `patient_email`, `patient_phone1`, `patient_phone2`, `patient_phone3`, `patient_gender`, `patient_birth_date`, `patient_age`, `patient_address`, `patient_refered_by`, `patient_photo`, `patient_added`, `patient_created_by`, `patient_updated`, `patient_modified_by`, `patient_status`, `patient_is_deleted`, `patient_clinic_id`, `patient_image`) VALUES
(1, '1804191', 'Talib Allauddin', 'talib@digitemb.com', '03451249755', '', '', 'Male', '0000-00-00', 22, 'B-52,Federal B Area Block 12 Gulberg Town', '', '', '2019-04-18 15:32:37', 6, '2019-04-18 15:32:37', 6, 'Enable', '0', 2, 'download.png'),
(2, '1804191', 'Muhammad Jazib Aleem', 'jazibaleem@gmail.com', '03451244456', '', '', 'Male', '0000-00-00', 25, '1St Floor Plot 1 Row 1 Block D Nazim Abad Number 5', '', '', '2019-04-18 15:36:47', 6, '2019-04-18 15:40:45', 6, 'Enable', '0', 1, 'images1.jpg'),
(3, '1804192', 'mahad khan', 'mahadkhan@gmail.com', '03459858511', '', '', 'Male', '0000-00-00', 14, 'noman avenew karachi', '', '', '2019-04-18 15:43:13', 6, '2019-04-18 15:43:13', 6, 'Enable', '0', 1, ''),
(4, '1804193', 'Rufi farhan', 'rufi123@gmail.com', '031-4349349', '', '', 'Male', '0000-00-00', 39, 'johor chorongi karachi', '', '', '2019-04-18 15:45:54', 6, '2019-04-18 15:45:54', 6, 'Enable', '0', 1, ''),
(5, '1804194', 'Zaid munwaar', 'Zaidmunwaar@gmail.com', '0331123456', '', '', 'Male', '0000-00-00', 69, 'Sindh baloch colony karachi', '', '', '2019-04-18 16:09:44', 6, '2019-04-18 16:09:44', 6, 'Enable', '0', 1, ''),
(6, '1804195', 'Anwar ul qamar', 'Anwarulqamar@gmail', '03311234568', '', '', 'Male', '0000-00-00', 26, 'Narialmor karachi', '', '', '2019-04-18 16:12:47', 6, '2019-04-18 16:12:47', 6, 'Enable', '0', 1, ''),
(7, '1804196', 'Raza mahraj', 'Raza@live.com', '03000233797', '', '', 'Male', '0000-00-00', 22, 'Johor chorangi', '', '', '2019-04-18 16:15:33', 6, '2019-04-18 16:15:33', 6, 'Enable', '0', 1, ''),
(8, '1804197', 'Saad muneer', 'Saadm@gmail.com', '03452121966', '', '', 'Male', '0000-00-00', 54, 'Kalapul karachi', '', '', '2019-04-18 16:18:25', 6, '2019-04-18 16:18:25', 6, 'Enable', '0', 1, ''),
(9, '1804198', 'Rafay', 'M.abdurrafay04@gmail.com', '03330200979', '', '', 'Male', '0000-00-00', 16, 'Johor chorangi', '', '', '2019-04-18 17:10:16', 7, '2019-04-18 17:10:16', 7, 'Enable', '0', 2, ''),
(10, '1804199', 'Zaid munwaar', 'Zaidmunwaar@gmail.com', '0331123456', '', '', 'Male', '0000-00-00', 55, 'Sindh baloch colony karachi', '', '', '2019-04-18 17:11:14', 7, '2019-04-18 17:11:14', 7, 'Enable', '0', 2, ''),
(11, '18041910', 'Anwar ul qamar', 'Anwarulqamar@gmail', '03000233797', '', '', 'Male', '0000-00-00', 24, 'Narialmor karachi', '', '', '2019-04-18 17:11:55', 7, '2019-04-18 17:11:55', 7, 'Enable', '0', 2, ''),
(12, '18041911', 'Raza mahraj', 'Raza@live.com', '03000233797', '', '', 'Male', '0000-00-00', 22, '', '', '', '2019-04-18 17:12:38', 7, '2019-04-18 17:12:38', 7, 'Enable', '0', 2, ''),
(13, '18041912', 'Rafay', 'M.abdurrafay04@gmail.com', '03000233797', '', '', 'Male', '0000-00-00', 12, 'Johor more karachi', '', '', '2019-04-18 17:58:19', 9, '2019-04-18 17:58:19', 9, 'Enabled', '0', 3, ''),
(14, '18041913', 'Rafay', 'M.abdurrafay04@gmail.com', '03000233797', '', '', 'Male', '0000-00-00', 12, 'Johor more karachi', '', '', '2019-04-18 17:58:36', 9, '2019-04-18 17:58:36', 9, 'Enabled', '1', 3, ''),
(15, '18041914', 'Rafay', 'M.abdurrafay04@gmail.com', '03000233797', '', '', 'Male', '0000-00-00', 12, 'Johor more karachi', '', '', '2019-04-18 17:58:40', 9, '2019-04-18 17:58:40', 9, 'Enabled', '1', 3, ''),
(16, '18041915', 'Zaid munwaar', 'Zaidmunwaar@gmail.com', '03311234568', '', '', 'Male', '0000-00-00', 44, 'Kalapul karachi', '', '', '2019-04-18 18:00:03', 9, '2019-04-18 18:00:03', 9, 'Enabled', '0', 3, ''),
(17, '18041916', 'Raza mahraj', 'Raza@live.com', '03311234568', '', '', 'Male', '0000-00-00', 55, 'Sindh baloch colony karachi', '', '', '2019-04-18 18:00:43', 9, '2019-04-18 18:00:43', 9, 'Enabled', '0', 3, ''),
(18, '1-9', 'wieoru', 'weoiru@Oiew.com', '0239480923', '', '', 'Male', '0000-00-00', 32, 'oiweur', '', '', '2019-04-19 12:06:20', 6, '2019-04-19 12:06:20', 6, 'Enabled', '1', 1, ''),
(19, '1-9', 'talha ansari', 'talhaansari@gmail.com', '03337866986', '031-4349349', '', 'Male', '0000-00-00', 80, 'gujrawala', '', '', '2019-04-19 13:10:20', 9, '2019-04-22 16:54:17', 6, 'Enable', '0', 1, ''),
(20, '1-10', 'Ramba Mamba', 'Ramba@live.com', '183289328192', '92834923849', '', 'Male', '0000-00-00', 0, '', '', '', '2019-04-29 01:55:10', 6, '2019-05-03 12:37:26', 6, 'Enable', '1', 1, ''),
(21, '1-11', 'Sana A', '', '03001234567', '', '', 'Female', '0000-00-00', 32, 'R-234, Gulberg Town, Karachi', '', '', '2019-05-14 12:52:06', 6, '2019-05-14 12:52:06', 6, 'Enabled', '1', 1, ''),
(22, '1-12', 'ALIYA', '', '033312345', '', '', 'Male', '0000-00-00', 32, 'R123', '', '', '2019-05-14 13:10:11', 6, '2019-05-14 13:10:11', 6, 'Enabled', '1', 1, ''),
(23, '1-13', 'ALIYA', '', '033312345', '', '', 'Female', '0000-00-00', 33, 'R123', '', '', '2019-05-14 13:10:39', 6, '2019-05-16 21:41:00', 6, 'Enable', '1', 1, ''),
(24, '1-14', 'saif abassi', 'saif@gmail.com', '03339878676', '', '', 'Male', '0000-00-00', 39, 'machar colony', '', '', '2019-05-16 22:10:06', 6, '2019-05-16 22:10:06', 6, 'Enable', '0', 1, 'IMG_20150328_141916.jpg'),
(25, '1-15', 'Tahir boloch', 'tahir@live.com', '03139876856', '', '', 'Male', '0000-00-00', 35, 'johor chorongi karachi', '', '', '2019-05-18 13:20:51', 6, '2019-05-18 13:20:51', 6, 'Enable', '0', 1, 'IMG_20190408_073914116.jpg'),
(26, '1-16', 'Abdur Rafay', 'a@live.com', '031-4349349', '', '', 'Female', '0000-00-00', 21, 'B-52,Federal B Area Block 12 Gulberg Town', '', '', '2019-05-18 17:22:36', 6, '2019-05-18 17:22:36', 6, 'Enabled', '0', 1, ''),
(27, '1-17', 'mahad biag ', 'talib@digitemb.com', '03451249755', '', '', 'Female', '0000-00-00', 21, 'nOMAN', 'Jazib', '', '2019-05-18 17:23:40', 6, '2019-05-18 17:23:40', 6, 'Enable', '0', 1, 'IMG_20190414_061936954_BURST000_COVER.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `patient_activity`
--

CREATE TABLE `patient_activity` (
  `pa_id` int(11) NOT NULL,
  `pa_message` text NOT NULL,
  `pa_added` datetime NOT NULL,
  `pa_clinic_id` int(11) NOT NULL,
  `pa_patient_id` int(11) NOT NULL,
  `pa_created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `patient_activity`
--

INSERT INTO `patient_activity` (`pa_id`, `pa_message`, `pa_added`, `pa_clinic_id`, `pa_patient_id`, `pa_created_by`) VALUES
(1, 'Patient created.', '2019-04-18 15:32:37', 1, 1, 6),
(2, 'Patient created.', '2019-04-18 15:36:47', 1, 2, 6),
(3, 'Information has been updated.', '2019-04-18 15:40:15', 1, 2, 6),
(4, 'Information has been updated.', '2019-04-18 15:40:45', 1, 2, 6),
(5, 'Patient created.', '2019-04-18 15:43:14', 1, 3, 6),
(6, 'Patient created.', '2019-04-18 15:45:54', 1, 4, 6),
(7, 'New appointment has been created for 18-Apr-2019 12:58 AM with Dr. Abdur Rafay!', '2019-04-18 15:58:48', 1, 1, 6),
(8, 'New appointment has been created for 18-Apr-2019 12:00 AM with Dr. mahad baig!', '2019-04-18 16:00:27', 1, 2, 6),
(9, 'New appointment has been created for 18-Apr-2019 12:00 AM with Dr. Talha Ali!', '2019-04-18 16:00:56', 1, 3, 6),
(10, 'New appointment has been created for 18-Apr-2019 12:01 AM with Dr. Aish Sawar!', '2019-04-18 16:01:37', 1, 4, 6),
(11, 'Patient created.', '2019-04-18 16:09:44', 1, 5, 6),
(12, 'Patient created.', '2019-04-18 16:12:47', 1, 6, 6),
(13, 'Patient created.', '2019-04-18 16:15:33', 1, 7, 6),
(14, 'Patient created.', '2019-04-18 16:18:26', 1, 8, 6),
(15, 'New appointment has been created for 18-Apr-2019 12:18 AM with Dr. Abdur Rafay!', '2019-04-18 16:19:03', 1, 5, 6),
(16, 'New appointment has been created for 18-Apr-2019 12:19 AM with Dr. Talha Ali!', '2019-04-18 16:19:31', 1, 8, 6),
(17, 'Patient created.', '2019-04-18 17:10:16', 2, 9, 7),
(18, 'Patient created.', '2019-04-18 17:11:14', 2, 10, 7),
(19, 'Patient created.', '2019-04-18 17:11:55', 2, 11, 7),
(20, 'Patient created.', '2019-04-18 17:12:38', 2, 12, 7),
(21, 'Patient created.', '2019-04-18 17:58:19', 3, 13, 9),
(22, 'Patient created.', '2019-04-18 17:58:36', 3, 14, 9),
(23, 'Patient created.', '2019-04-18 17:58:41', 3, 15, 9),
(24, 'Patient profile deleted!', '2019-04-18 17:59:21', 3, 14, 9),
(25, 'Patient profile deleted!', '2019-04-18 17:59:26', 3, 15, 9),
(26, 'Patient created.', '2019-04-18 18:00:03', 3, 16, 9),
(27, 'Patient created.', '2019-04-18 18:00:44', 3, 17, 9),
(28, 'New appointment has been created for 30-Apr-2019 12:01 AM with Dr. Gufran kamran!', '2019-04-18 18:01:51', 3, 5, 9),
(29, 'New appointment has been created for 30-Apr-2019 12:01 AM with Dr. Ayub khan!', '2019-04-18 18:02:19', 3, 17, 9),
(30, 'New appointment has been created for 26-Apr-2019 12:03 AM with Dr. Ayub khan!', '2019-04-18 18:04:11', 3, 13, 9),
(31, 'New appointment has been created for 15-May-2019 12:04 AM with Dr. Ayub khan!', '2019-04-18 18:04:51', 3, 5, 9),
(33, 'New appointment has been created for 18-Apr-2019 12:19 AM with Dr. Talha Ali!', '2019-04-19 09:07:39', 1, 5, 6),
(34, 'New appointment has been created for 19-Apr-2019 09:08 AM with Dr. Abdur Rafay!', '2019-04-19 09:08:42', 1, 1, 6),
(36, 'New appointment has been created for 19-Apr-2019 09:08 AM with Dr. Abdur Rafay!', '2019-04-19 09:09:18', 1, 2, 6),
(37, 'Appointment canceled with Dr. Abdur Rafay for 18-Apr-2019 12:58 PM', '2019-04-19 09:13:01', 1, 1, 6),
(38, 'New appointment has been created for 18-Apr-2019 12:58 AM with Dr. Abdur Rafay!', '2019-04-19 09:13:01', 1, 2, 6),
(39, 'Appointment canceled with Dr. Abdur Rafay for 19-Apr-2019 09:08 AM', '2019-04-19 09:24:38', 1, 2, 6),
(40, 'New appointment has been created for 20-Jul-2019 09:22 AM with Dr. Abdur Rafay!', '2019-04-19 09:24:38', 1, 1, 6),
(41, 'New appointment has been created for 19-Apr-2019 09:25 AM with Dr. Abdur Rafay!', '2019-04-19 09:25:04', 1, 1, 6),
(42, 'Appointment rescheduled with Dr. 19-Apr-2019 for 9:26 AM!', '2019-04-19 09:27:04', 1, 1, 6),
(43, 'Appointment rescheduled with Dr. Aish Sawar for 19-Apr-2019 09:31 AM!', '2019-04-19 09:31:33', 1, 1, 6),
(44, 'Appointment canceled with Dr. Aish Sawar for 19-Apr-2019 09:31 AM', '2019-04-19 09:41:24', 1, 1, 6),
(45, 'Patient created.', '2019-04-19 12:06:20', 1, 18, 6),
(46, 'New appointment has been created for 19-Apr-2019 12:33 AM with Dr. Jamal!', '2019-04-19 12:33:37', 1, 2, 9),
(47, 'New appointment has been created for 19-Apr-2019 12:35 AM with Dr. Jamal!', '2019-04-19 12:35:33', 1, 4, 9),
(48, 'New appointment has been created for 19-Apr-2019 12:08 AM with Dr. Jamal!', '2019-04-19 13:08:46', 1, 7, 9),
(49, 'Patient created.', '2019-04-19 13:10:20', 1, 19, 9),
(50, 'New report added with file name <a href="http://localhost/medical-information-management-system/assets/frontend/images/patientfiles/1.png">1.png</a>!', '2019-04-22 16:54:04', 1, 19, 6),
(51, 'Information has been updated.', '2019-04-22 16:54:17', 1, 19, 6),
(52, 'New appointment has been created for 22-Apr-2019 12:28 AM with Dr. Jamal!', '2019-04-22 17:28:28', 1, 2, 6),
(53, 'New appointment has been created for 22-Apr-2019 12:28 AM with Dr. Jamal!', '2019-04-22 17:28:38', 1, 2, 6),
(54, 'New appointment has been created for 22-Apr-2019 12:29 AM with Dr. Jamal!', '2019-04-22 17:29:15', 1, 2, 6),
(55, 'New appointment has been created for 22-Apr-2019 12:29 AM with Dr. Jamal!', '2019-04-22 18:00:37', 1, 2, 6),
(56, 'New lab work ordered with Agha Khan for Bridge in Stainless Steel, due by 01-Jan-1970', '2019-04-22 18:19:34', 1, 2, 6),
(57, 'Appointment canceled with Dr. Jamal for 22-Apr-2019 12:28 PM', '2019-04-22 19:36:21', 1, 2, 6),
(58, 'Appointment canceled with Dr. Jamal for 22-Apr-2019 12:28 PM', '2019-04-22 19:43:26', 1, 2, 6),
(59, 'Appointment canceled with Dr. Jamal for 22-Apr-2019 12:28 PM', '2019-04-22 19:43:30', 1, 2, 6),
(60, 'New appointment has been created for 22-Apr-2019 12:05 AM with Dr. Jamal!', '2019-04-22 20:05:52', 1, 2, 6),
(61, 'New appointment has been created for 22-Apr-2019 12:06 AM with Dr. Jamal!', '2019-04-22 20:06:42', 1, 5, 6),
(62, 'New appointment has been created for 22-Apr-2019 12:07 AM with Dr. Jamal!', '2019-04-22 20:07:16', 1, 2, 6),
(63, 'New appointment has been created for 22-Apr-2019 12:07 AM with Dr. Jamal!', '2019-04-22 20:07:36', 1, 2, 6),
(64, 'New appointment has been created for 22-Apr-2019 12:08 AM with Dr. Jamal!', '2019-04-22 20:08:43', 1, 2, 6),
(65, 'New appointment has been created for 22-Apr-2019 12:08 AM with Dr. Jamal!', '2019-04-22 20:11:09', 1, 2, 6),
(66, 'New appointment has been created for 22-Apr-2019 12:08 AM with Dr. Jamal!', '2019-04-22 20:11:49', 1, 2, 6),
(67, 'New appointment has been created for 22-Apr-2019 12:08 AM with Dr. Jamal!', '2019-04-22 20:12:01', 1, 2, 6),
(68, 'New appointment has been created for 22-Apr-2019 12:15 AM with Dr. Jamal!', '2019-04-22 20:15:44', 1, 6, 6),
(69, 'New appointment has been created for 22-Apr-2019 12:17 AM with Dr. Jamal!', '2019-04-22 20:17:42', 1, 2, 6),
(70, 'New appointment has been created for 23-Apr-2019 10:24 AM with Dr. Jamal!', '2019-04-23 10:24:50', 1, 2, 6),
(71, 'New appointment has been created for 23-Apr-2019 10:26 AM with Dr. Jamal!', '2019-04-23 10:26:22', 1, 2, 6),
(72, 'New appointment has been created for 23-Apr-2019 10:34 AM with Dr. Jamal!', '2019-04-23 10:34:34', 1, 5, 6),
(73, 'New appointment has been created for 23-Apr-2019 10:46 AM with Dr. Jamal!', '2019-04-23 10:46:37', 1, 7, 6),
(74, 'New appointment has been created for 23-Apr-2019 10:46 AM with Dr. Jamal!', '2019-04-23 10:46:48', 1, 7, 6),
(75, 'New appointment has been created for 23-Apr-2019 10:46 AM with Dr. Jamal!', '2019-04-23 10:47:08', 1, 7, 6),
(76, 'New appointment has been created for 23-Apr-2019 10:48 AM with Dr. Jamal!', '2019-04-23 10:48:38', 1, 19, 6),
(77, 'New appointment has been created for 23-Apr-2019 10:49 AM with Dr. Jamal!', '2019-04-23 10:49:50', 1, 4, 6),
(78, 'New appointment has been created for 23-Apr-2019 10:49 AM with Dr. Jamal!', '2019-04-23 10:50:43', 1, 4, 6),
(79, 'New appointment has been created for 23-Apr-2019 10:51 AM with Dr. Jamal!', '2019-04-23 10:51:30', 1, 5, 6),
(80, 'New appointment has been created for 23-Apr-2019 10:51 AM with Dr. Jamal!', '2019-04-23 10:52:11', 1, 5, 6),
(81, 'New appointment has been created for 24-Apr-2019 11:10 AM with Dr. Jamal!', '2019-04-23 11:10:40', 1, 4, 6),
(82, 'New appointment has been created for 25-Apr-2019 12:24 AM with Dr. Jamal!', '2019-04-25 14:24:27', 1, 5, 6),
(83, 'New report added with file name <a href="http://localhost/medical-information-management-system/assets/frontend/images/patientfiles/IMG-20190118-WA0080.jpeg">IMG-20190118-WA0080.jpeg</a>!', '2019-04-25 14:28:18', 1, 2, 6),
(84, 'New report added with file name <a href="http://localhost/medical-information-management-system/assets/frontend/images/patientfiles/luca-bravo-189272-unsplash.jpg">luca-bravo-189272-unsplash.jpg</a>!', '2019-04-25 14:29:09', 1, 2, 6),
(85, 'New appointment has been created for 26-Apr-2019 12:15 AM with Dr. Jamal!', '2019-04-26 21:15:16', 1, 2, 6),
(86, 'New appointment has been created for 28-Apr-2019 12:35 AM with Dr. Jamal!', '2019-04-28 17:35:55', 1, 2, 6),
(87, 'Patient created.', '2019-04-29 01:55:10', 1, 20, 6),
(88, 'New appointment has been created for 29-Apr-2019 02:55 AM with Dr. Jamal!', '2019-04-29 02:55:47', 1, 2, 6),
(89, 'New appointment has been created for 29-Apr-2019 12:39 AM with Dr. Jamal!', '2019-04-29 18:40:28', 1, 6, 6),
(90, 'New appointment has been created for 30-Apr-2019 12:28 AM with Dr. Jamal!', '2019-04-30 20:28:40', 1, 2, 6),
(91, 'New appointment has been created for 01-May-2019 02:03 AM with Dr. Jamal!', '2019-05-01 02:03:35', 1, 2, 6),
(92, 'New appointment has been created for 03-May-2019 11:58 AM with Dr. Jamal!', '2019-05-03 11:58:45', 1, 2, 6),
(93, 'New appointment has been created for 03-May-2019 12:15 AM with Dr. Jamal!', '2019-05-03 12:15:59', 1, 2, 6),
(94, 'New appointment has been created for 03-May-2019 12:16 AM with Dr. Jamal!', '2019-05-03 12:16:50', 1, 2, 6),
(95, 'Appointment canceled with Dr. Jamal for 03-May-2019 12:16 PM', '2019-05-03 12:17:18', 1, 2, 6),
(96, 'Appointment canceled with Dr. Jamal for 03-May-2019 12:15 PM', '2019-05-03 12:17:21', 1, 2, 6),
(97, 'Information has been updated.', '2019-05-03 12:37:26', 1, 20, 6),
(98, 'New appointment has been created for 04-May-2019 12:23 AM with Dr. Jamal!', '2019-05-04 17:23:21', 1, 2, 6),
(99, 'Patient created.', '2019-05-14 12:52:06', 1, 21, 6),
(100, 'New appointment has been created for 15-May-2019 12:53 AM with Dr. Najeeb!', '2019-05-14 12:55:34', 1, 8, 6),
(101, 'New appointment has been created for 14-May-2019 12:55 AM with Dr. Jamal!', '2019-05-14 12:56:09', 1, 2, 6),
(102, 'Patient created.', '2019-05-14 13:10:11', 1, 22, 6),
(103, 'Patient created.', '2019-05-14 13:10:39', 1, 23, 6),
(104, 'New appointment has been created for 08-May-2019 12:15 AM with Dr. Najeeb!', '2019-05-14 13:16:08', 1, 4, 6),
(105, 'New report added with file name <a href="http://localhost/medical-information-management-system/assets/frontend/images/patientfiles/download.jpg">download.jpg</a>!', '2019-05-14 13:19:08', 1, 6, 6),
(106, 'New appointment has been created for 14-May-2019 12:54 AM with Dr. Jamal!', '2019-05-14 20:54:37', 1, 2, 6),
(107, 'New appointment has been created for 14-May-2019 12:54 AM with Dr. Jamal!', '2019-05-14 20:54:42', 1, 7, 6),
(108, 'New appointment has been created for 14-May-2019 12:54 AM with Dr. Najeeb!', '2019-05-14 20:54:47', 1, 8, 6),
(109, 'New appointment has been created for 16-May-2019 04:41 AM with Dr. Jamal!', '2019-05-16 04:41:08', 1, 2, 6),
(110, 'New appointment has been created for 16-May-2019 04:41 AM with Dr. Jamal!', '2019-05-16 04:41:48', 1, 2, 6),
(111, 'New case has been created with Case # 85254', '2019-05-16 22:17:07', 1, 4, 6),
(112, 'Appointment rescheduled with Dr. Najeeb for 16-May-2019 04:41 AM!', '2019-05-16 23:33:28', 1, 2, 6),
(113, 'New appointment has been created for 16-May-2019 12:34 AM with Dr. Jamal!', '2019-05-16 23:34:15', 1, 18, 6),
(114, 'New report added with file name <a href="http://localhost/medical-information-management-system/assets/frontend/images/patientfiles/download.jpg">download.jpg</a>!', '2019-05-18 05:25:41', 1, 4, 6),
(115, 'New case has been created with <a href="http://localhost/medical-information-management-system/manage/patientcase/details/123">Case # 123</a>', '2019-05-18 05:28:29', 1, 3, 6),
(116, 'New case has been created with <a href="http://localhost/medical-information-management-system/manage/patientcase/details/145">Case # 145</a>', '2019-05-18 08:46:26', 1, 6, 6),
(117, 'New case has been created with <a href="http://localhost/medical-information-management-system/manage/patientcase/details/145">Case # 145</a>', '2019-05-18 08:51:16', 1, 3, 6),
(118, 'New case has been created with <a href="http://localhost/medical-information-management-system/manage/patientcase/details/146">Case # 146</a>', '2019-05-18 10:09:25', 1, 5, 6),
(119, 'Patient profile deleted!', '2019-05-18 13:12:00', 1, 22, 6),
(120, 'Patient profile deleted!', '2019-05-18 13:12:06', 1, 21, 6),
(121, 'Patient profile deleted!', '2019-05-18 13:12:14', 1, 18, 6),
(122, 'Patient profile deleted!', '2019-05-18 13:12:24', 1, 20, 6),
(123, 'Patient created.', '2019-05-18 13:20:51', 1, 25, 6),
(124, 'New appointment has been created for 18-May-2019 12:21 AM with Dr. Jamal!', '2019-05-18 13:22:24', 1, 25, 6),
(125, 'New lab work ordered with dow lad for Crown in Metal, due by 01-Jan-1970', '2019-05-18 13:35:49', 1, 6, 6),
(126, 'Patient created.', '2019-05-18 17:22:36', 1, 26, 6),
(127, 'Patient created.', '2019-05-18 17:23:40', 1, 27, 6);

-- --------------------------------------------------------

--
-- Table structure for table `patient_appointment_invoices`
--

CREATE TABLE `patient_appointment_invoices` (
  `pai_id` int(11) NOT NULL,
  `pai_pp_id` int(11) NOT NULL,
  `pai_appointment_id` int(11) NOT NULL,
  `pai_note` text NOT NULL,
  `pai_added` datetime NOT NULL,
  `pai_updated` datetime NOT NULL,
  `pai_created_by` int(11) NOT NULL,
  `pai_updated_by` int(11) NOT NULL,
  `pai_is_deleted` char(1) NOT NULL,
  `pai_status` varchar(15) NOT NULL,
  `pai_clinic_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `patient_appointment_medications`
--

CREATE TABLE `patient_appointment_medications` (
  `pam_id` int(11) NOT NULL,
  `pam_clinic_id` int(11) NOT NULL,
  `pam_patient_id` int(11) NOT NULL,
  `pam_appointment_id` int(11) NOT NULL,
  `pam_note` text NOT NULL,
  `pam_added` datetime NOT NULL,
  `pam_updated` datetime NOT NULL,
  `pam_created_by` int(11) NOT NULL,
  `pam_updated_by` int(11) NOT NULL,
  `pam_is_deleted` char(1) NOT NULL,
  `pam_status` varchar(15) NOT NULL,
  `pam_complaint` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `patient_case`
--

CREATE TABLE `patient_case` (
  `pc_id` int(11) NOT NULL,
  `pc_case_number` int(11) NOT NULL,
  `pc_patient_id` int(11) NOT NULL,
  `pc_dtr_id` int(11) NOT NULL,
  `pc_room_id` int(11) NOT NULL,
  `pc_charges` int(11) NOT NULL,
  `pc_is_deleted` char(1) NOT NULL,
  `pc_status` varchar(8) NOT NULL,
  `pc_created` datetime NOT NULL,
  `pc_created_by` int(11) NOT NULL,
  `pc_updated` datetime NOT NULL,
  `pc_updated_by` int(11) NOT NULL,
  `pc_relative_id` int(11) NOT NULL,
  `pc_relative_name` varchar(50) NOT NULL,
  `pc_clinic_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `patient_case`
--

INSERT INTO `patient_case` (`pc_id`, `pc_case_number`, `pc_patient_id`, `pc_dtr_id`, `pc_room_id`, `pc_charges`, `pc_is_deleted`, `pc_status`, `pc_created`, `pc_created_by`, `pc_updated`, `pc_updated_by`, `pc_relative_id`, `pc_relative_name`, `pc_clinic_id`) VALUES
(1, 1000, 3, 11, 0, 12500, '0', 'Enabled', '2019-05-16 19:00:38', 6, '2019-05-16 19:00:38', 6, 2, 'Ali', 1),
(2, 1000, 3, 11, 0, 12500, '0', 'Enabled', '2019-05-16 19:01:30', 6, '2019-05-16 19:01:30', 6, 2, 'Ali', 1),
(3, 1000, 3, 11, 0, 12500, '0', 'Enabled', '2019-05-16 19:02:00', 6, '2019-05-16 19:02:00', 6, 2, 'Ali', 1),
(4, 85254, 4, 10, 0, 51465, '0', 'Enabled', '2019-05-16 22:16:22', 6, '2019-05-16 22:16:22', 6, 3, 'Mushti', 1),
(5, 85254, 4, 10, 0, 51465, '0', 'Enabled', '2019-05-16 22:17:07', 6, '2019-05-16 22:17:07', 6, 3, 'Mushti', 1),
(6, 123, 3, 11, 0, 564, '0', 'Enabled', '2019-05-18 05:28:29', 6, '2019-05-18 05:28:29', 6, 2, 'dsf', 1),
(7, 145, 6, 11, 0, 12500, '0', 'Enabled', '2019-05-18 08:46:26', 6, '2019-05-18 08:46:26', 6, 2, 'Ali', 1),
(8, 145, 3, 10, 0, 12345, '0', 'Enabled', '2019-05-18 08:51:16', 6, '2019-05-18 08:51:16', 6, 2, 'Ali', 1),
(9, 146, 5, 10, 2, 12345, '0', 'Enabled', '2019-05-18 10:09:25', 6, '2019-05-18 10:09:25', 6, 2, 'Ali', 1);

-- --------------------------------------------------------

--
-- Table structure for table `patient_facilities`
--

CREATE TABLE `patient_facilities` (
  `pf_id` int(11) NOT NULL,
  `pf_patient_id` int(11) NOT NULL,
  `pf_facility_id` int(11) NOT NULL,
  `pf_pc_id` int(11) NOT NULL,
  `pf_clinic_id` int(11) NOT NULL,
  `pf_is_deleted` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `patient_facilities`
--

INSERT INTO `patient_facilities` (`pf_id`, `pf_patient_id`, `pf_facility_id`, `pf_pc_id`, `pf_clinic_id`, `pf_is_deleted`) VALUES
(1, 6, 1, 7, 1, '0'),
(2, 6, 2, 7, 1, '0'),
(3, 3, 1, 8, 1, '0'),
(4, 3, 2, 8, 1, '0'),
(5, 5, 1, 9, 1, '0'),
(6, 5, 2, 9, 1, '0');

-- --------------------------------------------------------

--
-- Table structure for table `patient_files`
--

CREATE TABLE `patient_files` (
  `pf_id` int(11) NOT NULL,
  `pf_patient_id` int(11) NOT NULL COMMENT 'Patient,foreign,patients,patient_,patient_id',
  `pf_file` text NOT NULL COMMENT 'File',
  `pf_added` datetime NOT NULL,
  `pf_updated` datetime NOT NULL,
  `pf_created_by` int(11) NOT NULL,
  `pf_updated_by` int(11) NOT NULL,
  `pf_is_deleted` char(1) NOT NULL,
  `pf_clinic_id` int(11) NOT NULL,
  `pf_status` varchar(8) NOT NULL COMMENT 'Status',
  `pf_reporttype_id` int(11) NOT NULL,
  `pf_description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `patient_files`
--

INSERT INTO `patient_files` (`pf_id`, `pf_patient_id`, `pf_file`, `pf_added`, `pf_updated`, `pf_created_by`, `pf_updated_by`, `pf_is_deleted`, `pf_clinic_id`, `pf_status`, `pf_reporttype_id`, `pf_description`) VALUES
(1, 19, '19_1555934044_1.png', '2019-04-22 16:54:04', '2019-04-22 16:54:04', 6, 6, '0', 1, '', 1, 'adsf'),
(2, 2, '2_1556184498_IMG-20190118-WA0080.jpeg', '2019-04-25 14:28:18', '2019-04-25 14:28:18', 6, 6, '0', 1, '', 1, 'sd'),
(3, 2, '2_1556184549_luca-bravo-189272-unsplash.jpg', '2019-04-25 14:29:09', '2019-04-25 14:29:09', 6, 6, '0', 1, '', 1, ''),
(4, 6, '6_1557821948_download.jpg', '2019-05-14 13:19:08', '2019-05-14 13:19:08', 6, 6, '0', 1, '', 1, ''),
(5, 4, '4_1558139141_download.jpg', '2019-05-18 05:25:41', '2019-05-18 05:25:41', 6, 6, '0', 1, '', 1, 'sd');

-- --------------------------------------------------------

--
-- Table structure for table `patient_invoices`
--

CREATE TABLE `patient_invoices` (
  `pi_id` int(11) NOT NULL,
  `pi_clinic_id` int(11) NOT NULL,
  `pi_patient_id` int(11) NOT NULL,
  `pi_pai_id` int(11) NOT NULL,
  `pi_payment_method` varchar(120) NOT NULL,
  `pi_amount` int(11) NOT NULL,
  `pi_added` datetime NOT NULL,
  `pi_updated` datetime NOT NULL,
  `pi_created_by` int(11) NOT NULL,
  `pi_updated_by` int(11) NOT NULL,
  `pi_is_deleted` char(1) NOT NULL,
  `pi_status` varchar(15) NOT NULL,
  `pi_remarks` text NOT NULL,
  `pi_date` date NOT NULL,
  `pi_discount_rs` int(11) NOT NULL,
  `pi_discount_percent` int(11) NOT NULL,
  `pi_type` varchar(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `patient_invoices`
--

INSERT INTO `patient_invoices` (`pi_id`, `pi_clinic_id`, `pi_patient_id`, `pi_pai_id`, `pi_payment_method`, `pi_amount`, `pi_added`, `pi_updated`, `pi_created_by`, `pi_updated_by`, `pi_is_deleted`, `pi_status`, `pi_remarks`, `pi_date`, `pi_discount_rs`, `pi_discount_percent`, `pi_type`) VALUES
(1, 1, 3, 0, 'Cash', 8865, '2019-05-01 00:05:35', '2019-05-01 00:05:35', 6, 6, '0', 'Enabled', 'test remark', '0000-00-00', 0, 10, 'OPD'),
(2, 1, 3, 0, 'Cash', 8865, '2019-05-01 00:09:51', '2019-05-01 00:09:51', 6, 6, '0', 'Enabled', 'test remark', '2019-04-30', 0, 10, 'OPD'),
(3, 1, 3, 0, 'Cash', 8865, '2019-05-01 00:18:32', '2019-05-01 00:18:32', 6, 6, '0', 'Enabled', 'test remark', '2019-04-30', 0, 10, 'OPD'),
(4, 1, 3, 0, 'Cash', 2000, '2019-05-01 00:45:25', '2019-05-01 00:45:25', 6, 6, '0', 'Enabled', 'adsf', '2019-05-01', 0, 0, 'OPD'),
(5, 1, 3, 0, 'Cash', 2000, '2019-05-01 00:46:37', '2019-05-01 00:46:37', 6, 6, '0', 'Enabled', 'adsf', '2019-05-14', 0, 0, 'OPD'),
(6, 1, 3, 0, 'Cash', 1980, '2019-05-01 01:27:52', '2019-05-01 01:27:52', 6, 6, '0', 'Enabled', 'asdf', '2019-05-01', 0, 10, 'OPD'),
(7, 1, 4, 0, 'Cash', 2000, '2019-05-01 01:30:55', '2019-05-01 01:30:55', 6, 6, '0', 'Enabled', 'asdf', '2019-05-01', 0, 0, 'OPD'),
(8, 1, 3, 0, 'Cash', 2000, '2019-05-01 01:37:15', '2019-05-01 01:37:15', 6, 6, '0', 'Enabled', 'adsfssss', '2019-05-01', 0, 0, 'OPD'),
(9, 1, 5, 0, 'Cash', 2000, '2019-05-01 01:38:32', '2019-05-01 01:38:32', 6, 6, '0', 'Enabled', 'adsfssss', '2019-05-01', 0, 0, 'OPD'),
(10, 1, 2, 0, 'Cash', 5947, '2019-05-01 02:04:48', '2019-05-01 02:04:48', 6, 6, '0', 'Enabled', 'adsf', '2019-05-01', 0, 5, 'OPD'),
(11, 1, 2, 0, 'Cash', 7619, '2019-05-01 02:21:12', '2019-05-01 02:21:12', 6, 6, '0', 'Enabled', 'adsf', '2019-05-01', 0, 5, 'OPD'),
(12, 1, 2, 0, 'Cash', 2000, '2019-05-04 04:02:38', '2019-05-04 04:02:38', 6, 6, '0', 'Enabled', 'asdf', '2019-05-01', 0, 0, 'OPD'),
(13, 1, 4, 0, 'Cash', 9000, '2019-05-04 17:24:20', '2019-05-04 17:24:20', 6, 6, '0', 'Enabled', 'asdf', '2019-05-01', 0, 0, 'OPD'),
(14, 1, 2, 0, 'Cash', 2000, '2019-05-10 06:22:30', '2019-05-10 06:22:30', 6, 6, '0', 'Enabled', 'adsf', '2019-05-13', 0, 0, 'OPD'),
(15, 1, 4, 0, 'Cash', 3935, '2019-05-10 18:28:32', '2019-05-10 18:28:32', 6, 6, '0', 'Enabled', 'test remark', '2019-05-08', 0, 2, 'OPD'),
(16, 1, 5, 0, 'Cash', 8500, '2019-05-10 18:32:50', '2019-05-10 18:32:50', 6, 6, '0', 'Enabled', 'adsf', '2019-05-08', 0, 0, 'OPD'),
(17, 1, 3, 0, 'Cash', 8800, '2019-05-10 18:48:19', '2019-05-10 18:48:19', 6, 6, '0', 'Enabled', 'test remark', '2019-05-10', 0, 0, 'OPD'),
(18, 1, 6, 0, 'Cash', 19017, '2019-05-10 18:56:05', '2019-05-10 18:56:05', 6, 6, '0', 'Enabled', 'adsf', '2019-05-01', 0, 10, 'OPD'),
(19, 1, 4, 0, 'Cash', 2000, '2019-05-13 21:34:53', '2019-05-13 21:34:53', 6, 6, '0', 'Enabled', '456', '2019-05-13', 0, 0, 'OPD'),
(20, 1, 2, 0, 'Cash', 2000, '2019-05-14 12:58:06', '2019-05-14 12:58:06', 6, 6, '0', 'Enabled', 'test remark', '2019-05-14', 0, 0, 'OPD'),
(21, 1, 3, 0, 'Cash', 100, '2019-05-14 13:04:10', '2019-05-14 13:04:10', 6, 6, '0', 'Enabled', '', '2019-05-14', 0, 0, 'OPD'),
(22, 1, 4, 0, 'Cash', 200, '2019-05-15 21:53:23', '2019-05-15 21:53:23', 6, 6, '0', 'Enabled', 'adsf', '2019-05-02', 0, 0, 'OPD'),
(23, 1, 2, 0, 'Cash', 200, '2019-05-16 21:28:38', '2019-05-16 21:28:38', 6, 6, '0', 'Enabled', 'qwrtyui', '2019-05-31', 0, 0, 'OPD'),
(24, 1, 2, 0, 'Cash', 200, '2019-05-16 21:28:48', '2019-05-16 21:28:48', 6, 6, '0', 'Enabled', 'qwrtyui', '2019-05-16', 0, 0, 'OPD'),
(25, 1, 2, 0, 'Cash', 200, '2019-05-16 21:28:49', '2019-05-16 21:28:49', 6, 6, '0', 'Enabled', 'qwrtyui', '2019-05-16', 0, 0, 'OPD'),
(26, 1, 2, 0, 'Cash', 200, '2019-05-16 21:28:49', '2019-05-16 21:28:49', 6, 6, '0', 'Enabled', 'qwrtyui', '2019-05-16', 0, 0, 'OPD'),
(27, 1, 2, 0, 'Cash', 200, '2019-05-16 21:28:49', '2019-05-16 21:28:49', 6, 6, '0', 'Enabled', 'qwrtyui', '2019-05-16', 0, 0, 'OPD'),
(28, 1, 2, 0, 'Cash', 200, '2019-05-16 21:28:50', '2019-05-16 21:28:50', 6, 6, '0', 'Enabled', 'qwrtyui', '2019-05-16', 0, 0, 'OPD'),
(29, 1, 2, 0, 'Cash', 200, '2019-05-16 21:28:50', '2019-05-16 21:28:50', 6, 6, '0', 'Enabled', 'qwrtyui', '2019-05-16', 0, 0, 'OPD'),
(30, 1, 2, 0, 'Cash', 200, '2019-05-16 21:28:50', '2019-05-16 21:28:50', 6, 6, '0', 'Enabled', 'qwrtyui', '2019-05-16', 0, 0, 'OPD'),
(31, 1, 25, 0, 'Cash', 600, '2019-05-18 13:36:56', '2019-05-18 13:36:56', 6, 6, '0', 'Enabled', 'good', '2019-05-18', 0, 0, 'OPD');

-- --------------------------------------------------------

--
-- Table structure for table `patient_medical_histories`
--

CREATE TABLE `patient_medical_histories` (
  `pmh_id` int(11) NOT NULL,
  `pmh_clinic_id` int(11) NOT NULL,
  `pmh_patient_id` int(11) NOT NULL,
  `pmh_medical_condition` varchar(220) NOT NULL,
  `pmh_added` datetime NOT NULL,
  `pmh_updated` datetime NOT NULL,
  `pmh_created_by` int(11) NOT NULL,
  `pmh_updated_by` int(11) NOT NULL,
  `pmh_is_deleted` char(1) NOT NULL,
  `pmh_status` varchar(15) NOT NULL,
  `pmh_appointment_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `patient_medication`
--

CREATE TABLE `patient_medication` (
  `pm_id` int(11) NOT NULL,
  `pm_clinic_id` int(11) NOT NULL,
  `pm_patient_id` int(11) NOT NULL,
  `pm_appointment_id` int(11) NOT NULL,
  `pm_drug_id` int(11) NOT NULL,
  `pm_frequency_id` int(11) NOT NULL,
  `pm_days` int(11) NOT NULL,
  `pm_quantity` varchar(50) NOT NULL,
  `pm_instruction` text NOT NULL,
  `pm_added` datetime NOT NULL,
  `pm_updated` datetime NOT NULL,
  `pm_created_by` int(11) NOT NULL,
  `pm_updated_by` int(11) NOT NULL,
  `pm_is_deleted` char(1) NOT NULL,
  `pm_status` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `patient_procedures`
--

CREATE TABLE `patient_procedures` (
  `pp_id` int(11) NOT NULL,
  `pp_clinic_id` int(11) NOT NULL,
  `pp_patient_id` int(11) NOT NULL,
  `pp_procedure_id` int(11) NOT NULL,
  `pp_tooth_code` int(11) NOT NULL,
  `pp_date` datetime NOT NULL,
  `pp_amount` int(11) NOT NULL,
  `pp_discount` int(11) NOT NULL,
  `pp_discount_type` varchar(15) NOT NULL,
  `pp_added` datetime NOT NULL,
  `pp_updated` datetime NOT NULL,
  `pp_created_by` int(11) NOT NULL,
  `pp_updated_by` int(11) NOT NULL,
  `pp_is_deleted` char(1) NOT NULL,
  `pp_status` varchar(15) NOT NULL,
  `pp_description` varchar(150) NOT NULL,
  `pp_appointment_id` int(11) NOT NULL,
  `pp_invoice_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `patient_procedures`
--

INSERT INTO `patient_procedures` (`pp_id`, `pp_clinic_id`, `pp_patient_id`, `pp_procedure_id`, `pp_tooth_code`, `pp_date`, `pp_amount`, `pp_discount`, `pp_discount_type`, `pp_added`, `pp_updated`, `pp_created_by`, `pp_updated_by`, `pp_is_deleted`, `pp_status`, `pp_description`, `pp_appointment_id`, `pp_invoice_id`) VALUES
(1, 1, 3, 1, 0, '2019-04-30 00:00:00', 4000, 10, '', '2019-05-01 00:05:35', '2019-05-01 00:05:35', 6, 6, '0', 'Enabled', 'test', 0, 0),
(2, 1, 3, 2, 0, '2019-04-30 00:00:00', 1000, 100, '', '2019-05-01 00:05:35', '2019-05-01 00:05:35', 6, 6, '0', 'Enabled', 'unplug', 0, 0),
(3, 1, 3, 1, 0, '2019-04-30 00:00:00', 4000, 10, '', '2019-05-01 00:09:51', '2019-05-01 00:09:51', 6, 6, '0', 'Enabled', 'test', 0, 0),
(4, 1, 3, 2, 0, '2019-04-30 00:00:00', 1000, 100, '', '2019-05-01 00:09:51', '2019-05-01 00:09:51', 6, 6, '0', 'Enabled', 'unplug', 0, 0),
(5, 1, 3, 1, 0, '2019-04-30 00:00:00', 4000, 10, '%', '2019-05-01 00:18:32', '2019-05-01 00:18:32', 6, 6, '0', 'Enabled', 'test', 0, 0),
(6, 1, 3, 2, 0, '2019-04-30 00:00:00', 1000, 100, 'Rs.', '2019-05-01 00:18:32', '2019-05-01 00:18:32', 6, 6, '0', 'Enabled', 'unplug', 0, 0),
(7, 1, 3, 1, 0, '2019-05-01 00:00:00', 2000, 0, 'Rs.', '2019-05-01 00:45:25', '2019-05-01 00:45:25', 6, 6, '0', 'Enabled', '', 0, 0),
(8, 1, 3, 1, 0, '2019-05-14 00:00:00', 2000, 0, 'Rs.', '2019-05-01 00:46:37', '2019-05-01 00:46:37', 6, 6, '0', 'Enabled', '', 0, 0),
(9, 1, 3, 1, 0, '2019-05-01 00:00:00', 2000, 0, 'Rs.', '2019-05-01 01:27:52', '2019-05-01 01:27:52', 6, 6, '0', 'Enabled', '', 0, 0),
(10, 1, 3, 2, 0, '2019-05-01 00:00:00', 200, 0, 'Rs.', '2019-05-01 01:27:52', '2019-05-01 01:27:52', 6, 6, '0', 'Enabled', '', 0, 0),
(11, 1, 4, 1, 0, '2019-05-01 00:00:00', 2000, 0, 'Rs.', '2019-05-01 01:30:55', '2019-05-01 01:30:55', 6, 6, '0', 'Enabled', '', 0, 0),
(12, 1, 3, 1, 0, '2019-05-01 00:00:00', 2000, 0, 'Rs.', '2019-05-01 01:37:15', '2019-05-01 01:37:15', 6, 6, '0', 'Enabled', '', 0, 9),
(13, 1, 5, 1, 0, '2019-05-01 00:00:00', 2000, 0, 'Rs.', '2019-05-01 01:38:32', '2019-05-01 01:38:32', 6, 6, '0', 'Enabled', '', 0, 9),
(14, 1, 2, 1, 0, '2019-05-01 00:00:00', 1900, 100, 'Rs.', '2019-05-01 02:04:48', '2019-05-01 02:04:48', 6, 6, '0', 'Enabled', 'adsf', 48, 10),
(15, 1, 2, 2, 0, '2019-05-01 00:00:00', 180, 20, 'Rs.', '2019-05-01 02:04:48', '2019-05-01 02:04:48', 6, 6, '0', 'Enabled', '', 48, 10),
(16, 1, 2, 1, 0, '2019-05-01 00:00:00', 1980, 20, 'Rs.', '2019-05-01 02:21:12', '2019-05-01 02:21:12', 6, 6, '0', 'Enabled', 'adsf', 48, 11),
(17, 1, 2, 2, 0, '2019-05-01 00:00:00', 190, 10, 'Rs.', '2019-05-01 02:21:12', '2019-05-01 02:21:12', 6, 6, '0', 'Enabled', '', 48, 11),
(18, 1, 2, 1, 0, '2019-05-01 00:00:00', 2000, 0, 'Rs.', '2019-05-04 04:02:38', '2019-05-04 04:02:38', 6, 6, '0', 'Enabled', '', 0, 12),
(19, 1, 4, 1, 0, '2019-05-01 00:00:00', 2000, 0, 'Rs.', '2019-05-04 17:24:20', '2019-05-04 17:24:20', 6, 6, '0', 'Enabled', '', 0, 13),
(20, 1, 2, 1, 0, '2019-05-13 00:00:00', 2000, 0, 'Rs.', '2019-05-10 06:22:30', '2019-05-10 06:22:30', 6, 6, '0', 'Enabled', '', 0, 14),
(21, 1, 4, 1, 0, '2019-05-08 00:00:00', 1995, 5, 'Rs.', '2019-05-10 18:28:32', '2019-05-10 18:28:32', 6, 6, '0', 'Enabled', '', 0, 15),
(22, 1, 4, 2, 0, '2019-05-08 00:00:00', 180, 10, '%', '2019-05-10 18:28:32', '2019-05-10 18:28:32', 6, 6, '0', 'Enabled', '', 0, 15),
(23, 1, 4, 1, 0, '2019-05-08 00:00:00', 1840, 8, '%', '2019-05-10 18:28:32', '2019-05-10 18:28:32', 6, 6, '0', 'Enabled', '', 0, 15),
(24, 1, 5, 1, 0, '2019-05-08 00:00:00', 2000, 0, 'Rs.', '2019-05-10 18:32:50', '2019-05-10 18:32:50', 6, 6, '0', 'Enabled', '', 0, 16),
(25, 1, 3, 1, 0, '2019-05-10 00:00:00', 2000, 0, 'Rs.', '2019-05-10 18:48:19', '2019-05-10 18:48:19', 6, 6, '0', 'Enabled', '', 0, 17),
(26, 1, 6, 2, 0, '2019-05-01 00:00:00', 180, 20, 'Rs.', '2019-05-10 18:56:05', '2019-05-10 18:56:05', 6, 6, '0', 'Enabled', 'adsf', 0, 18),
(27, 1, 4, 1, 0, '2019-05-13 00:00:00', 2000, 0, 'Rs.', '2019-05-13 21:34:53', '2019-05-13 21:34:53', 6, 6, '0', 'Enabled', '', 0, 19),
(28, 1, 2, 1, 0, '2019-05-14 00:00:00', 2000, 0, 'Rs.', '2019-05-14 12:58:06', '2019-05-14 12:58:06', 6, 6, '0', 'Enabled', '', 54, 20),
(29, 1, 3, 3, 0, '2019-05-14 00:00:00', 100, 50, 'Rs.', '2019-05-14 13:04:10', '2019-05-14 13:04:10', 6, 6, '0', 'Enabled', '', 0, 21),
(30, 1, 4, 2, 0, '2019-05-02 00:00:00', 200, 0, 'Rs.', '2019-05-15 21:53:23', '2019-05-15 21:53:23', 6, 6, '0', 'Enabled', '', 0, 22),
(31, 1, 2, 2, 0, '2019-05-31 00:00:00', 200, 0, 'Rs.', '2019-05-16 21:28:38', '2019-05-16 21:28:38', 6, 6, '0', 'Enabled', '', 0, 23),
(32, 1, 2, 2, 0, '2019-05-16 00:00:00', 200, 0, 'Rs.', '2019-05-16 21:28:48', '2019-05-16 21:28:48', 6, 6, '0', 'Enabled', '', 0, 24),
(33, 1, 2, 2, 0, '2019-05-16 00:00:00', 200, 0, 'Rs.', '2019-05-16 21:28:49', '2019-05-16 21:28:49', 6, 6, '0', 'Enabled', '', 0, 25),
(34, 1, 2, 2, 0, '2019-05-16 00:00:00', 200, 0, 'Rs.', '2019-05-16 21:28:49', '2019-05-16 21:28:49', 6, 6, '0', 'Enabled', '', 0, 26),
(35, 1, 2, 2, 0, '2019-05-16 00:00:00', 200, 0, 'Rs.', '2019-05-16 21:28:49', '2019-05-16 21:28:49', 6, 6, '0', 'Enabled', '', 0, 27),
(36, 1, 2, 2, 0, '2019-05-16 00:00:00', 200, 0, 'Rs.', '2019-05-16 21:28:50', '2019-05-16 21:28:50', 6, 6, '0', 'Enabled', '', 0, 28),
(37, 1, 2, 2, 0, '2019-05-16 00:00:00', 200, 0, 'Rs.', '2019-05-16 21:28:50', '2019-05-16 21:28:50', 6, 6, '0', 'Enabled', '', 0, 29),
(38, 1, 2, 2, 0, '2019-05-16 00:00:00', 200, 0, 'Rs.', '2019-05-16 21:28:50', '2019-05-16 21:28:50', 6, 6, '0', 'Enabled', '', 0, 30),
(39, 1, 25, 4, 0, '2019-05-18 00:00:00', 600, 0, 'Rs.', '2019-05-18 13:36:56', '2019-05-18 13:36:56', 6, 6, '0', 'Enabled', '', 0, 31);

-- --------------------------------------------------------

--
-- Table structure for table `procedures`
--

CREATE TABLE `procedures` (
  `procedure_id` int(11) NOT NULL,
  `procedure_name` varchar(50) NOT NULL,
  `procedure_price` varchar(15) NOT NULL,
  `procedure_added` datetime NOT NULL,
  `procedure_updated` datetime NOT NULL,
  `procedure_created_by` int(11) NOT NULL,
  `procedure_modified_by` int(11) NOT NULL,
  `procedure_status` varchar(15) NOT NULL,
  `procedure_is_deleted` char(1) DEFAULT NULL,
  `procedure_clinic_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `procedures`
--

INSERT INTO `procedures` (`procedure_id`, `procedure_name`, `procedure_price`, `procedure_added`, `procedure_updated`, `procedure_created_by`, `procedure_modified_by`, `procedure_status`, `procedure_is_deleted`, `procedure_clinic_id`) VALUES
(1, 'Root Canal', '2000', '2019-04-18 15:52:32', '2019-04-18 15:52:32', 6, 6, 'Enable', '0', 1),
(2, 'Unplug', '200', '2019-04-18 15:52:43', '2019-04-18 15:52:43', 6, 6, 'Enable', '0', 1),
(3, 'NICU Drip Charges', '150', '2019-05-14 13:03:41', '2019-05-14 13:03:41', 6, 6, 'Enable', '0', 1),
(4, 'head pain', '600', '2019-05-18 13:30:36', '2019-05-18 13:30:36', 6, 6, 'Enable', '0', 1);

-- --------------------------------------------------------

--
-- Table structure for table `purchases`
--

CREATE TABLE `purchases` (
  `p_id` int(11) NOT NULL,
  `p_clinic_id` int(11) NOT NULL,
  `p_account_id` int(11) NOT NULL COMMENT 'Account,foreign,accounts,a_,a_id',
  `p_supplier_id` int(11) NOT NULL COMMENT 'Supplier,foreign,suppliers,s_,s_id',
  `p_total` double NOT NULL COMMENT 'Total Amount',
  `p_remaining` double NOT NULL COMMENT 'Remaining',
  `p_paid` double NOT NULL COMMENT 'Paid',
  `p_added` datetime NOT NULL,
  `p_updated` datetime NOT NULL,
  `p_created_by` int(11) NOT NULL,
  `p_updated_by` int(11) NOT NULL,
  `p_is_deleted` char(1) NOT NULL,
  `p_status` varchar(15) NOT NULL,
  `p_remarks` varchar(255) NOT NULL,
  `p_date` date NOT NULL,
  `p_payable` int(11) NOT NULL,
  `p_total_discount_percent` int(11) NOT NULL,
  `p_total_discount_rs` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchases`
--

INSERT INTO `purchases` (`p_id`, `p_clinic_id`, `p_account_id`, `p_supplier_id`, `p_total`, `p_remaining`, `p_paid`, `p_added`, `p_updated`, `p_created_by`, `p_updated_by`, `p_is_deleted`, `p_status`, `p_remarks`, `p_date`, `p_payable`, `p_total_discount_percent`, `p_total_discount_rs`) VALUES
(1, 1, 0, 0, 499, 0, 0, '2019-05-15 23:52:23', '2019-05-15 23:52:23', 6, 6, '0', 'Enabled', '', '2019-05-15', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `purchase_details`
--

CREATE TABLE `purchase_details` (
  `pd_id` int(11) NOT NULL,
  `pd_clinic_id` int(11) NOT NULL,
  `pd_purchase_id` int(11) NOT NULL,
  `pd_item_id` int(11) NOT NULL,
  `pd_qty` int(11) NOT NULL,
  `pd_unit_price` double NOT NULL,
  `pd_added` datetime NOT NULL,
  `pd_updated` datetime NOT NULL,
  `pd_created_by` int(11) NOT NULL,
  `pd_updated_by` int(11) NOT NULL,
  `pd_is_deleted` char(1) NOT NULL,
  `pd_status` varchar(15) NOT NULL,
  `pd_discount_percent` int(11) NOT NULL,
  `pd_discount_rs` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase_details`
--

INSERT INTO `purchase_details` (`pd_id`, `pd_clinic_id`, `pd_purchase_id`, `pd_item_id`, `pd_qty`, `pd_unit_price`, `pd_added`, `pd_updated`, `pd_created_by`, `pd_updated_by`, `pd_is_deleted`, `pd_status`, `pd_discount_percent`, `pd_discount_rs`) VALUES
(1, 1, 1, 1, 1, 499, '2019-05-15 23:52:23', '2019-05-15 23:52:23', 6, 6, '0', 'Enabled', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `relations`
--

CREATE TABLE `relations` (
  `relative_id` int(11) NOT NULL,
  `relative_name` varchar(50) NOT NULL,
  `relative_created_by` int(11) NOT NULL,
  `relative_created` datetime NOT NULL,
  `relative_updated_by` int(11) NOT NULL,
  `relative_updated` datetime NOT NULL,
  `relative_status` varchar(8) NOT NULL,
  `relative_is_deleted` char(1) NOT NULL,
  `relative_clinic_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `relations`
--

INSERT INTO `relations` (`relative_id`, `relative_name`, `relative_created_by`, `relative_created`, `relative_updated_by`, `relative_updated`, `relative_status`, `relative_is_deleted`, `relative_clinic_id`) VALUES
(2, 'Husband', 6, '0000-00-00 00:00:00', 6, '2019-05-16 08:30:05', 'Enable', '0', 1),
(3, 'Father', 6, '0000-00-00 00:00:00', 6, '2019-05-16 21:10:34', 'Enable', '0', 1),
(4, 'mother', 6, '0000-00-00 00:00:00', 6, '2019-05-18 13:32:57', 'Enable', '0', 1);

-- --------------------------------------------------------

--
-- Table structure for table `report_type`
--

CREATE TABLE `report_type` (
  `r_id` int(11) NOT NULL,
  `r_name` varchar(120) NOT NULL,
  `r_status` varchar(15) NOT NULL,
  `r_created_by` int(11) NOT NULL,
  `r_updated_by` int(11) NOT NULL,
  `r_added` datetime NOT NULL,
  `r_updated` datetime NOT NULL,
  `r_is_deleted` char(1) NOT NULL,
  `r_clinic_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `report_type`
--

INSERT INTO `report_type` (`r_id`, `r_name`, `r_status`, `r_created_by`, `r_updated_by`, `r_added`, `r_updated`, `r_is_deleted`, `r_clinic_id`) VALUES
(1, 'X-Ray', '', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0', 1);

-- --------------------------------------------------------

--
-- Table structure for table `room`
--

CREATE TABLE `room` (
  `ro_id` int(11) NOT NULL,
  `ro_number` varchar(50) NOT NULL,
  `ro_room_type_id` int(11) NOT NULL,
  `ro_is_booked` char(1) NOT NULL,
  `ro_status` varchar(15) NOT NULL,
  `ro_created_by` int(11) NOT NULL,
  `ro_updated_by` int(11) NOT NULL,
  `ro_added` datetime NOT NULL,
  `ro_updated` datetime NOT NULL,
  `ro_is_deleted` char(1) NOT NULL,
  `ro_clinic_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `room`
--

INSERT INTO `room` (`ro_id`, `ro_number`, `ro_room_type_id`, `ro_is_booked`, `ro_status`, `ro_created_by`, `ro_updated_by`, `ro_added`, `ro_updated`, `ro_is_deleted`, `ro_clinic_id`) VALUES
(2, 'Room # 1', 2, '1', 'Enabled', 6, 6, '2019-04-25 17:47:55', '2019-04-25 17:47:55', '0', 1),
(3, 'Room # 5', 0, '0', 'Enabled', 6, 6, '2019-05-18 13:27:36', '2019-05-18 13:27:36', '0', 1),
(4, 'Room # 5', 4, '0', 'Enabled', 6, 6, '2019-05-18 13:29:32', '2019-05-18 13:29:32', '0', 1);

-- --------------------------------------------------------

--
-- Table structure for table `room_patient`
--

CREATE TABLE `room_patient` (
  `rp_id` int(11) NOT NULL,
  `rp_room_id` int(11) NOT NULL,
  `rp_patient_id` int(11) NOT NULL,
  `rp_alloted_by` int(11) NOT NULL,
  `rp_status` varchar(15) NOT NULL,
  `rp_created_by` int(11) NOT NULL,
  `rp_updated_by` int(11) NOT NULL,
  `rp_added` datetime NOT NULL,
  `rp_admission_date` datetime NOT NULL,
  `rp_discharge_date` datetime NOT NULL,
  `rp_updated` datetime NOT NULL,
  `rp_is_deleted` char(1) NOT NULL,
  `rp_clinic_id` int(11) NOT NULL,
  `rp_amount` int(11) NOT NULL,
  `rp_discount` int(11) NOT NULL,
  `rp_payment_status` varchar(15) NOT NULL,
  `rp_invoice_id` int(11) NOT NULL,
  `rp_discount_type` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `room_patient`
--

INSERT INTO `room_patient` (`rp_id`, `rp_room_id`, `rp_patient_id`, `rp_alloted_by`, `rp_status`, `rp_created_by`, `rp_updated_by`, `rp_added`, `rp_admission_date`, `rp_discharge_date`, `rp_updated`, `rp_is_deleted`, `rp_clinic_id`, `rp_amount`, `rp_discount`, `rp_payment_status`, `rp_invoice_id`, `rp_discount_type`) VALUES
(1, 2, 3, 10, 'Enabled', 6, 6, '2019-05-01 00:05:35', '2019-04-28 00:00:00', '2019-05-01 00:00:00', '2019-05-01 00:05:35', '0', 1, 4500, 450, 'paid', 1, ''),
(2, 2, 3, 10, 'Enabled', 6, 6, '2019-05-01 00:09:51', '2019-04-28 00:00:00', '2019-05-01 00:00:00', '2019-05-01 00:09:51', '0', 1, 4500, 450, 'paid', 2, ''),
(3, 2, 3, 10, 'Enabled', 6, 6, '2019-05-01 00:18:32', '2019-04-28 00:00:00', '2019-05-01 00:00:00', '2019-05-01 00:18:32', '0', 1, 4500, 450, 'paid', 3, ''),
(4, 2, 2, 4, 'Enabled', 6, 6, '2019-05-01 02:04:48', '2019-04-28 00:00:00', '2019-05-01 00:00:00', '2019-05-01 02:04:48', '0', 1, 4500, 20, 'paid', 10, ''),
(5, 2, 2, 3, 'Enabled', 6, 6, '2019-05-01 02:21:12', '2019-04-28 00:00:00', '2019-05-01 00:00:00', '2019-05-01 02:21:12', '0', 1, 4500, 50, 'paid', 11, ''),
(6, 2, 5, 0, 'Enabled', 6, 6, '2019-05-10 18:32:50', '2019-05-08 00:00:00', '2019-05-11 00:00:00', '2019-05-10 18:32:50', '0', 1, 4500, 0, 'paid', 16, ''),
(7, 2, 3, 0, 'Enabled', 6, 6, '2019-05-10 18:48:19', '2019-05-06 00:00:00', '2019-05-10 00:00:00', '2019-05-10 18:48:19', '0', 1, 6000, 0, 'paid', 17, 'Rs.'),
(8, 2, 6, 0, 'Enabled', 6, 6, '2019-05-10 18:56:05', '2019-05-02 00:00:00', '2019-05-16 00:00:00', '2019-05-10 18:56:05', '0', 1, 21000, 5000, 'paid', 18, 'Rs.'),
(9, 2, 6, 3, 'Enabled', 6, 6, '2019-05-18 08:46:26', '1970-01-01 05:00:00', '1970-01-01 05:00:00', '2019-05-18 08:46:26', '0', 1, 0, 0, 'unpaid', 0, ''),
(10, 2, 3, 4, 'Enabled', 6, 6, '2019-05-18 08:51:16', '2019-05-18 09:00:00', '2019-05-23 10:00:00', '2019-05-18 08:51:16', '0', 1, 0, 0, 'unpaid', 0, ''),
(11, 2, 5, 4, 'Enabled', 6, 6, '2019-05-18 10:09:25', '2019-05-25 10:15:00', '2019-05-27 10:15:00', '2019-05-18 10:09:25', '0', 1, 0, 0, 'unpaid', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `room_type`
--

CREATE TABLE `room_type` (
  `rt_id` int(11) NOT NULL,
  `rt_name` varchar(120) NOT NULL,
  `rt_per_day_charges` varchar(15) NOT NULL,
  `rt_billing_type` char(1) NOT NULL,
  `rt_description` text NOT NULL,
  `rt_status` varchar(15) NOT NULL,
  `rt_created_by` int(11) NOT NULL,
  `rt_updated_by` int(11) NOT NULL,
  `rt_added` datetime NOT NULL,
  `rt_updated` datetime NOT NULL,
  `rt_is_deleted` char(1) NOT NULL,
  `rt_clinic_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `room_type`
--

INSERT INTO `room_type` (`rt_id`, `rt_name`, `rt_per_day_charges`, `rt_billing_type`, `rt_description`, `rt_status`, `rt_created_by`, `rt_updated_by`, `rt_added`, `rt_updated`, `rt_is_deleted`, `rt_clinic_id`) VALUES
(2, 'VIP', '1500', '1', '&lt;p&gt;test&lt;/p&gt;\r\n', 'Enabled', 6, 6, '2019-04-25 17:44:46', '2019-04-25 17:44:46', '0', 1),
(3, 'General', '53', '1', '', 'Enabled', 6, 6, '2019-04-26 21:28:31', '2019-04-26 21:28:31', '0', 1),
(4, 'private', '1500', '1', '', 'Enabled', 6, 6, '2019-05-18 13:28:49', '2019-05-18 13:28:49', '0', 1);

-- --------------------------------------------------------

--
-- Table structure for table `site_settings`
--

CREATE TABLE `site_settings` (
  `id` int(11) UNSIGNED NOT NULL,
  `website_title` varchar(255) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `website_url` varchar(255) DEFAULT NULL,
  `clinic_id` int(11) NOT NULL,
  `tag_line` varchar(80) NOT NULL,
  `address` varchar(75) NOT NULL,
  `full_address` varchar(130) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `tel` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `site_settings`
--

INSERT INTO `site_settings` (`id`, `website_title`, `logo`, `website_url`, `clinic_id`, `tag_line`, `address`, `full_address`, `tel`) VALUES
(1, 'Frontier Courier Service', 'frontier-courier-service.png', 'http://www.frontiercourierservice.com', 0, '', '', '', ''),
(2, 'Muhammadi Hospital', 'MHG-Logo-Final.png', 'www.mohammadihospital.com', 1, 'Preventation is better than Cure!', 'Plot No C-53, Shah Jahan Ave, Block 17, Federal B Area, Karachi.', 'Plot No C-53, Shah Jahan Ave, Block 17, Federal B Area, Karachi.', '+92-302-5558485');

-- --------------------------------------------------------

--
-- Table structure for table `stocks`
--

CREATE TABLE `stocks` (
  `s_id` int(11) NOT NULL,
  `s_clinic_id` int(11) NOT NULL,
  `s_item_id` int(11) NOT NULL COMMENT 'Item,foreign,items,i_,i_id',
  `s_qty` int(11) NOT NULL COMMENT 'Quantity',
  `s_min_qty` int(11) NOT NULL COMMENT 'Min. Quantity',
  `s_added` datetime NOT NULL,
  `s_updated` datetime NOT NULL,
  `s_created_by` int(11) NOT NULL,
  `s_updated_by` int(11) NOT NULL,
  `s_is_deleted` char(1) NOT NULL,
  `s_status` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stocks`
--

INSERT INTO `stocks` (`s_id`, `s_clinic_id`, `s_item_id`, `s_qty`, `s_min_qty`, `s_added`, `s_updated`, `s_created_by`, `s_updated_by`, `s_is_deleted`, `s_status`) VALUES
(1, 1, 1, 1, 1, '2019-05-15 23:52:23', '2019-05-15 23:52:23', 6, 6, '0', 'Enabled');

-- --------------------------------------------------------

--
-- Table structure for table `stock_release`
--

CREATE TABLE `stock_release` (
  `sr_id` int(11) NOT NULL,
  `sr_clinic_id` int(11) NOT NULL,
  `sr_item_id` int(11) NOT NULL COMMENT 'Items,foreign,items,i_,i_id',
  `sr_qty` int(11) NOT NULL COMMENT 'Quantity',
  `sr_issued_to` int(11) NOT NULL COMMENT 'Issued To,foreign,admin_users,,id',
  `sr_description` text NOT NULL COMMENT 'Description',
  `sr_added` datetime NOT NULL,
  `sr_updated` datetime NOT NULL,
  `sr_created_by` int(11) NOT NULL,
  `sr_updated_by` int(11) NOT NULL,
  `sr_is_deleted` char(1) NOT NULL,
  `sr_status` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `subs_plan`
--

CREATE TABLE `subs_plan` (
  `subs_plan_id` int(11) NOT NULL,
  `subs_plan_name` varchar(25) NOT NULL,
  `subs_plan_price` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subs_plan`
--

INSERT INTO `subs_plan` (`subs_plan_id`, `subs_plan_name`, `subs_plan_price`) VALUES
(1, 'Infant', '20000');

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE `suppliers` (
  `s_id` int(11) NOT NULL,
  `s_name` varchar(150) NOT NULL,
  `s_address` text NOT NULL,
  `s_phone1` varchar(15) NOT NULL,
  `s_contact_person` varchar(120) NOT NULL,
  `s_email` varchar(120) NOT NULL,
  `s_phone2` varchar(15) NOT NULL,
  `s_is_deleted` char(1) NOT NULL,
  `s_added` datetime NOT NULL,
  `s_updated` datetime NOT NULL,
  `s_created_by` int(11) NOT NULL,
  `s_updated_by` int(11) NOT NULL,
  `s_status` varchar(15) NOT NULL,
  `s_clinic_id` int(11) NOT NULL,
  `s_payable` int(11) NOT NULL,
  `s_receivable` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suppliers`
--

INSERT INTO `suppliers` (`s_id`, `s_name`, `s_address`, `s_phone1`, `s_contact_person`, `s_email`, `s_phone2`, `s_is_deleted`, `s_added`, `s_updated`, `s_created_by`, `s_updated_by`, `s_status`, `s_clinic_id`, `s_payable`, `s_receivable`) VALUES
(1, 'danish mirza', 'machar colony', '03333288797', 'ali mirza', 'alidanish@gmail.com', '03132567487', '0', '2019-05-16 00:03:57', '2019-05-16 00:03:57', 6, 6, 'Enable', 1, 0, 0),
(2, 'New Supplier', 'ALKSDFJ', '039857397856', 'rafay', 'm.abdurrafay@gmail.com', '03579738587', '0', '2019-05-18 13:37:51', '2019-05-18 13:37:51', 6, 6, 'Enable', 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tokens`
--

CREATE TABLE `tokens` (
  `t_id` int(11) NOT NULL,
  `t_patient_id` int(11) NOT NULL,
  `t_doctor_id` int(11) NOT NULL,
  `t_number` int(11) NOT NULL,
  `t_date` date NOT NULL,
  `t_added` datetime NOT NULL,
  `t_updated` datetime NOT NULL,
  `t_created_by` int(11) NOT NULL,
  `t_updated_by` int(11) NOT NULL,
  `t_status` varchar(15) NOT NULL,
  `t_clinic_id` int(11) NOT NULL,
  `t_is_deleted` char(1) NOT NULL,
  `checked` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tokens`
--

INSERT INTO `tokens` (`t_id`, `t_patient_id`, `t_doctor_id`, `t_number`, `t_date`, `t_added`, `t_updated`, `t_created_by`, `t_updated_by`, `t_status`, `t_clinic_id`, `t_is_deleted`, `checked`) VALUES
(3, 18, 10, 3, '2019-04-19', '2019-04-19 12:40:20', '2019-04-19 12:40:20', 9, 9, 'Enabled', 1, '0', '0'),
(4, 6, 10, 4, '2019-04-19', '2019-04-19 12:42:09', '2019-04-19 12:42:09', 9, 9, 'Enabled', 1, '0', '0'),
(5, 5, 10, 5, '2019-04-19', '2019-04-19 12:55:07', '2019-04-19 12:55:07', 9, 9, 'Enabled', 1, '0', '0'),
(6, 4, 10, 6, '2019-04-19', '2019-04-19 12:55:46', '2019-04-19 12:55:46', 9, 9, 'Enabled', 1, '0', '0'),
(7, 3, 10, 7, '2019-04-19', '2019-04-19 13:07:22', '2019-04-19 13:07:22', 9, 9, 'Enabled', 1, '0', '0'),
(8, 2, 10, 8, '2019-04-19', '2019-04-19 13:12:02', '2019-04-19 13:12:02', 9, 9, 'Enabled', 1, '0', '0'),
(9, 8, 0, 1, '2019-04-22', '2019-04-22 14:02:06', '2019-04-22 19:25:53', 6, 6, 'Enabled', 1, '0', '1'),
(10, 7, 0, 2, '2019-04-22', '2019-04-22 14:02:27', '2019-04-22 19:26:58', 6, 6, 'Enabled', 1, '0', '1'),
(11, 6, 0, 3, '2019-04-22', '2019-04-22 14:02:46', '2019-04-22 19:29:21', 6, 6, 'Enabled', 1, '1', '0'),
(12, 5, 0, 4, '2019-04-22', '2019-04-22 14:03:00', '2019-04-22 19:43:34', 6, 6, 'Enabled', 1, '1', '0'),
(13, 4, 0, 5, '2019-04-22', '2019-04-22 14:03:12', '2019-04-22 19:43:40', 6, 6, 'Enabled', 1, '0', '1'),
(14, 3, 0, 6, '2019-04-22', '2019-04-22 14:03:52', '2019-04-22 19:43:44', 6, 6, 'Enabled', 1, '0', '1'),
(15, 2, 0, 7, '2019-04-22', '2019-04-22 14:04:21', '2019-04-22 19:43:47', 6, 6, 'Enabled', 1, '1', '0'),
(16, 8, 10, 7, '2019-04-22', '2019-04-22 19:44:20', '2019-04-22 19:44:20', 6, 6, 'Enabled', 1, '0', ''),
(17, 2, 10, 8, '2019-04-22', '2019-04-22 19:44:58', '2019-04-22 19:44:58', 6, 6, 'Enabled', 1, '0', ''),
(18, 3, 10, 9, '2019-04-22', '2019-04-22 19:47:16', '2019-04-22 19:47:16', 6, 6, 'Enabled', 1, '0', '0'),
(19, 3, 10, 1, '2019-04-23', '2019-04-23 10:33:36', '2019-04-23 10:33:36', 6, 6, 'Enabled', 1, '0', '0'),
(20, 4, 10, 2, '2019-04-23', '2019-04-23 10:33:56', '2019-04-23 10:33:56', 6, 6, 'Enabled', 1, '0', '0'),
(21, 6, 10, 3, '2019-04-23', '2019-04-23 10:34:11', '2019-04-23 10:34:11', 6, 6, 'Enabled', 1, '0', '0'),
(22, 8, 10, 4, '2019-04-23', '2019-04-23 10:34:23', '2019-04-23 10:34:23', 6, 6, 'Enabled', 1, '0', '0'),
(23, 3, 10, 5, '2019-04-23', '2019-04-23 11:48:05', '2019-04-23 11:48:05', 6, 6, 'Enabled', 1, '0', '0'),
(24, 7, 10, 6, '2019-04-23', '2019-04-23 11:51:22', '2019-04-23 11:51:22', 6, 6, 'Enabled', 1, '0', '0'),
(25, 7, 10, 1, '2019-04-25', '2019-04-25 14:24:11', '2019-04-25 14:26:21', 6, 6, 'Enabled', 1, '0', '1'),
(26, 7, 10, 2, '2019-04-25', '2019-04-25 14:26:34', '2019-04-25 14:26:44', 6, 6, 'Enabled', 1, '1', '0'),
(27, 7, 10, 2, '2019-04-25', '2019-04-25 14:26:57', '2019-04-25 14:26:57', 6, 6, 'Enabled', 1, '0', '0'),
(28, 6, 10, 3, '2019-04-25', '2019-04-25 22:48:48', '2019-04-25 22:48:48', 6, 6, 'Enabled', 1, '0', '0'),
(29, 6, 10, 1, '2019-04-26', '2019-04-26 21:15:06', '2019-04-26 21:15:06', 6, 6, 'Enabled', 1, '0', '0'),
(30, 2, 10, 1, '2019-04-28', '2019-04-28 17:35:34', '2019-04-28 17:35:34', 6, 6, 'Enabled', 1, '0', '0'),
(31, 3, 10, 2, '2019-04-28', '2019-04-28 17:35:42', '2019-04-28 17:35:42', 6, 6, 'Enabled', 1, '0', '0'),
(32, 5, 10, 3, '2019-04-28', '2019-04-28 17:35:50', '2019-04-28 17:35:50', 6, 6, 'Enabled', 1, '0', '0'),
(33, 7, 10, 1, '2019-04-29', '2019-04-29 18:41:43', '2019-04-29 18:41:43', 6, 6, 'Enabled', 1, '0', '0'),
(34, 7, 10, 1, '2019-05-03', '2019-05-03 11:59:27', '2019-05-03 11:59:27', 6, 6, 'Enabled', 1, '0', '0'),
(35, 21, 11, 1, '2019-05-14', '2019-05-14 12:53:18', '2019-05-14 12:56:41', 6, 6, 'Enabled', 1, '0', '1'),
(36, 23, 10, 2, '2019-05-14', '2019-05-14 13:11:23', '2019-05-14 13:11:23', 6, 6, 'Enabled', 1, '0', '0'),
(37, 7, 11, 3, '2019-05-14', '2019-05-14 13:12:17', '2019-05-14 13:12:17', 6, 6, 'Enabled', 1, '0', '0'),
(38, 7, 10, 1, '2019-05-15', '2019-05-15 21:50:24', '2019-05-15 23:57:21', 6, 6, 'Enabled', 1, '0', '1'),
(39, 8, 11, 2, '2019-05-15', '2019-05-15 21:50:40', '2019-05-15 21:50:40', 6, 6, 'Enabled', 1, '0', '0'),
(40, 7, 11, 1, '2019-05-16', '2019-05-16 04:42:21', '2019-05-16 06:03:58', 6, 6, 'Enabled', 1, '0', '1'),
(41, 5, 10, 2, '2019-05-16', '2019-05-16 05:42:49', '2019-05-16 06:11:13', 6, 6, 'Enabled', 1, '0', '1'),
(42, 7, 10, 3, '2019-05-16', '2019-05-16 05:54:40', '2019-05-16 05:54:40', 6, 6, 'Enabled', 1, '0', '0'),
(43, 3, 11, 2, '2019-05-16', '2019-05-16 05:55:01', '2019-05-16 21:12:07', 6, 6, 'Enabled', 1, '0', '1'),
(44, 4, 11, 3, '2019-05-16', '2019-05-16 05:55:20', '2019-05-16 21:12:12', 6, 6, 'Enabled', 1, '0', '1'),
(45, 7, 11, 4, '2019-05-16', '2019-05-16 05:55:36', '2019-05-16 06:10:59', 6, 6, 'Enabled', 1, '0', '1'),
(46, 7, 10, 4, '2019-05-16', '2019-05-16 05:55:55', '2019-05-16 05:55:55', 6, 6, 'Enabled', 1, '0', '0'),
(47, 8, 11, 5, '2019-05-16', '2019-05-16 21:21:47', '2019-05-16 21:26:01', 6, 6, 'Enabled', 1, '0', '1'),
(48, 7, 11, 6, '2019-05-16', '2019-05-16 21:22:56', '2019-05-16 21:26:08', 6, 6, 'Enabled', 1, '1', '0'),
(49, 7, 11, 1, '2019-05-18', '2019-05-18 13:07:06', '2019-05-18 13:07:18', 6, 6, 'Enabled', 1, '0', '1'),
(50, 7, 12, 1, '2019-05-18', '2019-05-18 13:23:02', '2019-05-18 13:23:02', 6, 6, 'Enabled', 1, '0', '0'),
(51, 8, 11, 2, '2019-05-18', '2019-05-18 13:23:36', '2019-05-18 13:23:36', 6, 6, 'Enabled', 1, '0', '0'),
(52, 6, 10, 1, '2019-05-18', '2019-05-18 13:24:13', '2019-05-18 13:24:13', 6, 6, 'Enabled', 1, '0', '0'),
(53, 5, 10, 2, '2019-05-18', '2019-05-18 13:24:47', '2019-05-18 13:24:47', 6, 6, 'Enabled', 1, '0', '0');

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE `user_role` (
  `page_id` int(11) NOT NULL,
  `role_title` varchar(150) NOT NULL,
  `permission` text NOT NULL,
  `page_status` enum('Yes','No') NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified_by` int(11) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_deleted` char(1) NOT NULL,
  `clinic_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`page_id`, `role_title`, `permission`, `page_status`, `created_by`, `modified_by`, `date_created`, `date_updated`, `is_deleted`, `clinic_id`) VALUES
(1, 'Super Admin', '{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"clinics":{"create":"true","read":"true","update":"true","delete":"true"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"appointments":{"create":"true","read":"true","update":"true","delete":"true"},"patients":{"create":"true","read":"true","update":"true","delete":"true"},"procedures":{"create":"true","read":"true","update":"true","delete":"true"},"labtracking":{"create":"true","read":"true","update":"true","delete":"true"},"reports":{"create":"false","read":"true","update":"false","delete":"false"},"treatments":{"create":"true","read":"true","update":"true","delete":"true"},"invoices":{"create":"true","read":"true","update":"true","delete":"true"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"true"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"true"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"worktype":{"create":"true","read":"true","update":"true","delete":"true"},"work":{"create":"true","read":"true","update":"true","delete":"true"},"suppliers":{"create":"true","read":"true","update":"true","delete":"true"},"lab":{"create":"true","read":"true","update":"true","delete":"true"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"patientfiles":{"create":"true","read":"true","update":"true","delete":"true"},"expenses":{"create":"true","read":"true","update":"true","delete":"true"},"purchases":{"create":"true","read":"true","update":"true","delete":"true"},"items":{"create":"true","read":"true","update":"true","delete":"true","issue":"true"},"stocks":{"create":"true","read":"true","update":"true","delete":"true"},"accounts":{"create":"true","read":"true","update":"true","delete":"true"},"purchasedetails":{"create":"true","read":"true","update":"true","delete":"true"},"stockreleases":{"create":"true","read":"true","update":"true","delete":"true"},"employeepayroll":{"create":"true","read":"true","update":"true","delete":"true"},"employeeattendance":{"create":"true","read":"true","update":"true","delete":"true"},"tokens":{"create":"true","read":"true","update":"true","delete":"true"},"backup":{"create":"false","read":"true","update":"false","delete":"false"},"reporttype":{"create":"true","read":"true","update":"true","delete":"true"},"":{"create":"false","read":"false","update":"false","delete":"false"},"roomtype":{"create":"true","read":"true","update":"true","delete":"true"},"room":{"create":"true","read":"true","update":"true","delete":"true"},"bedtype":{"create":"true","read":"true","update":"true","delete":"true"},"bed":{"create":"true","read":"true","update":"true","delete":"true"},"bedpatient":{"create":"true","read":"true","update":"true","delete":"true"},"roompatient":{"create":"true","read":"true","update":"true","delete":"true"},"doctordays":{"create":"true","read":"true","update":"true","delete":"true"},"patientprocedures":{"create":"true","read":"true","update":"true","delete":"true"},"facilities":{"create":"true","read":"true","update":"true","delete":"true"},"doctors":{"create":"true","read":"true","update":"true","delete":"true"}}', 'Yes', 1, 0, '2019-05-16 07:03:30', '2019-05-16 02:03:30', '0', 0),
(2, 'Admin', '{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"clinics":{"create":"false","read":"false","update":"false","delete":"false"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"appointments":{"create":"true","read":"true","update":"true","delete":"true"},"patients":{"create":"true","read":"true","update":"true","delete":"true","profile":"true"},"procedures":{"create":"true","read":"true","update":"true","delete":"true"},"labtracking":{"create":"true","read":"true","update":"true","delete":"true"},"reports":{"create":"false","read":"true","update":"false","delete":"false"},"treatments":{"create":"true","read":"true","update":"true","delete":"true"},"invoices":{"create":"true","read":"true","update":"true","delete":"true"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"true"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"true"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"worktype":{"create":"true","read":"true","update":"true","delete":"true"},"work":{"create":"true","read":"true","update":"true","delete":"true"},"suppliers":{"create":"true","read":"true","update":"true","delete":"true"},"lab":{"create":"true","read":"true","update":"true","delete":"true"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"patientfiles":{"create":"true","read":"true","update":"true","delete":"true"},"expenses":{"create":"true","read":"true","update":"true","delete":"true"},"purchases":{"create":"true","read":"true","update":"true","delete":"true"},"items":{"create":"true","read":"true","update":"true","delete":"true","issue":"true"},"stocks":{"create":"true","read":"true","update":"true","delete":"true"},"accounts":{"create":"true","read":"true","update":"true","delete":"true"},"purchasedetails":{"create":"true","read":"true","update":"true","delete":"true"},"stockreleases":{"create":"true","read":"true","update":"true","delete":"true"},"employeepayroll":{"create":"true","read":"true","update":"true","delete":"true"},"employeeattendance":{"create":"true","read":"true","update":"true","delete":"true"},"tokens":{"create":"true","read":"true","update":"true","delete":"true"},"backup":{"create":"false","read":"true","update":"false","delete":"false"},"reporttype":{"create":"true","read":"true","update":"true","delete":"true"},"":{"create":"false","read":"false","update":"false","delete":"false"},"roomtype":{"create":"true","read":"true","update":"true","delete":"true"},"room":{"create":"true","read":"true","update":"true","delete":"true"},"bedtype":{"create":"true","read":"true","update":"true","delete":"true"},"bed":{"create":"true","read":"true","update":"true","delete":"true"},"bedpatient":{"create":"true","read":"true","update":"true","delete":"true"},"roompatient":{"create":"true","read":"true","update":"true","delete":"true"},"doctordays":{"create":"true","read":"true","update":"true","delete":"true"},"patientprocedures":{"create":"true","read":"true","update":"true","delete":"true"},"facilities":{"create":"true","read":"true","update":"true","delete":"true"},"patientcase":{"create":"true","read":"true","update":"true","delete":"true","details":"true"},"relations":{"create":"true","read":"true","update":"true","delete":"true"},"patientfacility":{"create":"false","read":"false","update":"false","delete":"false"},"doctors":{"create":"true","read":"true","update":"true","delete":"true"}}', 'Yes', 1, 0, '2019-05-18 10:22:37', '2019-05-18 05:22:37', '0', 1),
(3, 'User', '{"admins":{"create":"false","read":"false","update":"false","delete":"false","fullaccess":"false"},"clinics":{"create":"false","read":"false","update":"false","delete":"false"},"rolemanagement":{"create":"false","read":"false","update":"false","delete":"false"},"appointments":{"create":"true","read":"true","update":"true","delete":"false"},"doctors":{"create":"false","read":"true","update":"false","delete":"false"},"patients":{"create":"true","read":"true","update":"true","delete":"false"},"procedures":{"create":"false","read":"true","update":"false","delete":"false"},"labtracking":{"create":"true","read":"true","update":"true","delete":"false"},"reports":{"create":"false","read":"false","update":"false","delete":"false"},"treatments":{"create":"true","read":"true","update":"true","delete":"false"},"invoices":{"create":"true","read":"true","update":"true","delete":"false"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"false"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"false"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"false"},"useractivitylog":{"create":"false","read":"false","update":"false","delete":"false"},"worktype":{"create":"false","read":"false","update":"false","delete":"false"},"work":{"create":"false","read":"false","update":"false","delete":"false"},"suppliers":{"create":"true","read":"true","update":"true","delete":"true"},"lab":{"create":"false","read":"false","update":"false","delete":"false"},"site":{"create":"false","read":"true","update":"true","delete":"false"}}', 'Yes', 1, 1, '2019-03-23 04:49:22', '2019-04-19 05:16:20', '0', 1),
(4, 'Doctor', '{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"clinics":{"create":"true","read":"true","update":"true","delete":"true"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"appointments":{"create":"true","read":"true","update":"true","delete":"true"},"patients":{"create":"true","read":"true","update":"true","delete":"true"},"procedures":{"create":"true","read":"true","update":"true","delete":"true"},"labtracking":{"create":"true","read":"true","update":"true","delete":"true"},"reports":{"create":"false","read":"true","update":"false","delete":"false"},"treatments":{"create":"true","read":"true","update":"true","delete":"true"},"invoices":{"create":"true","read":"true","update":"true","delete":"true"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"true"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"true"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"worktype":{"create":"true","read":"true","update":"true","delete":"true"},"work":{"create":"true","read":"true","update":"true","delete":"true"},"suppliers":{"create":"true","read":"true","update":"true","delete":"true"},"lab":{"create":"true","read":"true","update":"true","delete":"true"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"patientfiles":{"create":"true","read":"true","update":"true","delete":"true"},"expenses":{"create":"true","read":"true","update":"true","delete":"true"},"purchases":{"create":"true","read":"true","update":"true","delete":"true"},"items":{"create":"true","read":"true","update":"true","delete":"true","issue":"true"},"stocks":{"create":"true","read":"true","update":"true","delete":"true"},"accounts":{"create":"true","read":"true","update":"true","delete":"true"},"purchasedetails":{"create":"true","read":"true","update":"true","delete":"true"},"stockreleases":{"create":"true","read":"true","update":"true","delete":"true"},"employeepayroll":{"create":"true","read":"true","update":"true","delete":"true"},"employeeattendance":{"create":"true","read":"true","update":"true","delete":"true"},"tokens":{"create":"true","read":"true","update":"true","delete":"true"},"backup":{"create":"false","read":"true","update":"false","delete":"false"},"reporttype":{"create":"true","read":"true","update":"true","delete":"true"},"":{"create":"false","read":"false","update":"false","delete":"false"},"doctors":{"create":"true","read":"true","update":"true","delete":"true"}}', 'Yes', 1, 0, '2019-04-19 22:19:34', '2019-04-19 17:19:34', '0', 1),
(6, 'Paitent', '{"admins":{"create":"true","read":"true","update":"true","delete":"true","fullaccess":"true"},"clinics":{"create":"true","read":"true","update":"true","delete":"true"},"rolemanagement":{"create":"true","read":"true","update":"true","delete":"true"},"appointments":{"create":"true","read":"true","update":"true","delete":"true"},"patients":{"create":"true","read":"true","update":"true","delete":"true","profile":"true"},"procedures":{"create":"true","read":"true","update":"true","delete":"true"},"labtracking":{"create":"true","read":"true","update":"true","delete":"true"},"reports":{"create":"false","read":"true","update":"false","delete":"false"},"treatments":{"create":"true","read":"true","update":"true","delete":"true"},"invoices":{"create":"true","read":"true","update":"true","delete":"true"},"healthrecord":{"create":"true","read":"true","update":"true","delete":"true"},"dentalchart":{"create":"true","read":"true","update":"true","delete":"true"},"doctortiming":{"create":"true","read":"true","update":"true","delete":"true"},"useractivitylog":{"create":"false","read":"true","update":"false","delete":"false"},"worktype":{"create":"true","read":"true","update":"true","delete":"true"},"work":{"create":"true","read":"true","update":"true","delete":"true"},"suppliers":{"create":"true","read":"true","update":"true","delete":"true"},"lab":{"create":"true","read":"true","update":"true","delete":"true"},"site":{"create":"true","read":"true","update":"true","delete":"true"},"patientfiles":{"create":"true","read":"true","update":"true","delete":"true"},"expenses":{"create":"true","read":"true","update":"true","delete":"true"},"purchases":{"create":"true","read":"true","update":"true","delete":"true"},"items":{"create":"true","read":"true","update":"true","delete":"true","issue":"true"},"stocks":{"create":"true","read":"true","update":"true","delete":"true"},"accounts":{"create":"true","read":"true","update":"true","delete":"true"},"purchasedetails":{"create":"true","read":"true","update":"true","delete":"true"},"stockreleases":{"create":"true","read":"true","update":"true","delete":"true"},"employeepayroll":{"create":"true","read":"true","update":"true","delete":"true"},"employeeattendance":{"create":"true","read":"true","update":"true","delete":"true"},"tokens":{"create":"true","read":"true","update":"true","delete":"true"},"backup":{"create":"false","read":"true","update":"false","delete":"false"},"reporttype":{"create":"true","read":"true","update":"true","delete":"true"},"":{"create":"false","read":"false","update":"false","delete":"false"},"roomtype":{"create":"true","read":"true","update":"true","delete":"true"},"room":{"create":"true","read":"true","update":"true","delete":"true"},"bedtype":{"create":"true","read":"true","update":"true","delete":"true"},"bed":{"create":"true","read":"true","update":"true","delete":"true"},"bedpatient":{"create":"true","read":"true","update":"true","delete":"true"},"roompatient":{"create":"true","read":"true","update":"true","delete":"true"},"doctordays":{"create":"true","read":"true","update":"true","delete":"true"},"patientprocedures":{"create":"true","read":"true","update":"true","delete":"true"},"doctors":{"create":"true","read":"true","update":"true","delete":"true"}}', 'Yes', 0, 0, '2019-05-03 17:40:50', '2019-05-03 12:40:50', '0', 0),
(7, 'Patient', '{"admins":{"create":"false","read":"false","update":"false","delete":"false","fullaccess":"false"},"clinics":{"create":"false","read":"false","update":"false","delete":"false"},"rolemanagement":{"create":"false","read":"false","update":"false","delete":"false"},"appointments":{"create":"false","read":"true","update":"false","delete":"false"},"patients":{"create":"false","read":"true","update":"false","delete":"false","profile":"true"},"procedures":{"create":"false","read":"false","update":"false","delete":"false"},"labtracking":{"create":"false","read":"false","update":"false","delete":"false"},"reports":{"create":"false","read":"false","update":"false","delete":"false"},"treatments":{"create":"false","read":"true","update":"false","delete":"false"},"invoices":{"create":"false","read":"true","update":"false","delete":"false"},"healthrecord":{"create":"false","read":"true","update":"false","delete":"false"},"dentalchart":{"create":"false","read":"false","update":"false","delete":"false"},"doctortiming":{"create":"false","read":"true","update":"false","delete":"false"},"useractivitylog":{"create":"false","read":"false","update":"false","delete":"false"},"worktype":{"create":"false","read":"false","update":"false","delete":"false"},"work":{"create":"false","read":"false","update":"false","delete":"false"},"suppliers":{"create":"false","read":"false","update":"false","delete":"false"},"lab":{"create":"false","read":"false","update":"false","delete":"false"},"site":{"create":"false","read":"false","update":"false","delete":"false"},"patientfiles":{"create":"false","read":"false","update":"false","delete":"false"},"expenses":{"create":"false","read":"false","update":"false","delete":"false"},"purchases":{"create":"false","read":"false","update":"false","delete":"false"},"items":{"create":"false","read":"false","update":"false","delete":"false","issue":"false"},"stocks":{"create":"false","read":"false","update":"false","delete":"false"},"accounts":{"create":"false","read":"false","update":"false","delete":"false"},"purchasedetails":{"create":"false","read":"false","update":"false","delete":"false"},"stockreleases":{"create":"false","read":"false","update":"false","delete":"false"},"employeepayroll":{"create":"false","read":"false","update":"false","delete":"false"},"employeeattendance":{"create":"false","read":"false","update":"false","delete":"false"},"tokens":{"create":"false","read":"false","update":"false","delete":"false"},"backup":{"create":"false","read":"false","update":"false","delete":"false"},"reporttype":{"create":"false","read":"false","update":"false","delete":"false"},"":{"create":"false","read":"false","update":"false","delete":"false"},"roomtype":{"create":"false","read":"false","update":"false","delete":"false"},"room":{"create":"false","read":"false","update":"false","delete":"false"},"bedtype":{"create":"false","read":"false","update":"false","delete":"false"},"bed":{"create":"false","read":"false","update":"false","delete":"false"},"bedpatient":{"create":"false","read":"false","update":"false","delete":"false"},"roompatient":{"create":"false","read":"false","update":"false","delete":"false"},"doctordays":{"create":"false","read":"false","update":"false","delete":"false"},"patientprocedures":{"create":"false","read":"false","update":"false","delete":"false"}}', 'Yes', 0, 0, '2019-05-03 17:49:32', '2019-05-03 12:50:18', '0', 1);

-- --------------------------------------------------------

--
-- Table structure for table `work`
--

CREATE TABLE `work` (
  `w_id` int(11) NOT NULL,
  `w_name` varchar(120) NOT NULL,
  `w_status` varchar(15) NOT NULL,
  `w_created_by` int(11) NOT NULL,
  `w_updated_by` int(11) NOT NULL,
  `w_added` datetime NOT NULL,
  `w_updated` datetime NOT NULL,
  `w_is_deleted` char(1) NOT NULL,
  `w_clinic_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `work`
--

INSERT INTO `work` (`w_id`, `w_name`, `w_status`, `w_created_by`, `w_updated_by`, `w_added`, `w_updated`, `w_is_deleted`, `w_clinic_id`) VALUES
(1, 'Stainless Steel', 'Enable', 6, 6, '2019-04-19 22:26:13', '2019-04-19 22:26:13', '0', 1),
(2, 'Metal', 'Enable', 6, 6, '2019-05-18 13:31:12', '2019-05-18 13:31:12', '0', 1);

-- --------------------------------------------------------

--
-- Table structure for table `work_type`
--

CREATE TABLE `work_type` (
  `wt_id` int(11) NOT NULL,
  `wt_name` varchar(150) NOT NULL,
  `wt_added` datetime NOT NULL,
  `wt_updated` datetime NOT NULL,
  `wt_created_by` int(11) NOT NULL,
  `wt_updated_by` int(11) NOT NULL,
  `wt_status` varchar(15) NOT NULL,
  `wt_is_deleted` char(1) NOT NULL,
  `wt_clinic_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `work_type`
--

INSERT INTO `work_type` (`wt_id`, `wt_name`, `wt_added`, `wt_updated`, `wt_created_by`, `wt_updated_by`, `wt_status`, `wt_is_deleted`, `wt_clinic_id`) VALUES
(1, 'Bridge', '2019-04-19 22:26:05', '2019-04-19 22:26:05', 6, 6, 'Enable', '0', 1),
(2, 'Crown', '2019-05-18 13:30:53', '2019-05-18 13:30:53', 6, 6, 'Enable', '0', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`a_id`);

--
-- Indexes for table `admin_users`
--
ALTER TABLE `admin_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `appointments`
--
ALTER TABLE `appointments`
  ADD PRIMARY KEY (`appointment_id`);

--
-- Indexes for table `bed`
--
ALTER TABLE `bed`
  ADD PRIMARY KEY (`b_id`);

--
-- Indexes for table `bed_patient`
--
ALTER TABLE `bed_patient`
  ADD PRIMARY KEY (`bp_id`);

--
-- Indexes for table `bed_type`
--
ALTER TABLE `bed_type`
  ADD PRIMARY KEY (`bt_id`);

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD PRIMARY KEY (`session_id`),
  ADD KEY `last_activity_idx` (`last_activity`);

--
-- Indexes for table `clinics`
--
ALTER TABLE `clinics`
  ADD PRIMARY KEY (`clinic_id`);

--
-- Indexes for table `doctor_timing`
--
ALTER TABLE `doctor_timing`
  ADD PRIMARY KEY (`dt_id`);

--
-- Indexes for table `drug_units`
--
ALTER TABLE `drug_units`
  ADD PRIMARY KEY (`du_id`);

--
-- Indexes for table `employee_attendance`
--
ALTER TABLE `employee_attendance`
  ADD PRIMARY KEY (`ea_id`);

--
-- Indexes for table `employee_payroll`
--
ALTER TABLE `employee_payroll`
  ADD PRIMARY KEY (`ep_id`);

--
-- Indexes for table `expenses`
--
ALTER TABLE `expenses`
  ADD PRIMARY KEY (`e_id`);

--
-- Indexes for table `facilities`
--
ALTER TABLE `facilities`
  ADD PRIMARY KEY (`facility_id`);

--
-- Indexes for table `frequencies`
--
ALTER TABLE `frequencies`
  ADD PRIMARY KEY (`f_id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`i_id`);

--
-- Indexes for table `lab`
--
ALTER TABLE `lab`
  ADD PRIMARY KEY (`lab_id`);

--
-- Indexes for table `labtracking`
--
ALTER TABLE `labtracking`
  ADD PRIMARY KEY (`lt_id`);

--
-- Indexes for table `medical_conditions`
--
ALTER TABLE `medical_conditions`
  ADD PRIMARY KEY (`mc_id`);

--
-- Indexes for table `medicines`
--
ALTER TABLE `medicines`
  ADD PRIMARY KEY (`m_id`);

--
-- Indexes for table `patients`
--
ALTER TABLE `patients`
  ADD PRIMARY KEY (`patient_id`);

--
-- Indexes for table `patient_activity`
--
ALTER TABLE `patient_activity`
  ADD PRIMARY KEY (`pa_id`);

--
-- Indexes for table `patient_appointment_invoices`
--
ALTER TABLE `patient_appointment_invoices`
  ADD PRIMARY KEY (`pai_id`);

--
-- Indexes for table `patient_appointment_medications`
--
ALTER TABLE `patient_appointment_medications`
  ADD PRIMARY KEY (`pam_id`);

--
-- Indexes for table `patient_case`
--
ALTER TABLE `patient_case`
  ADD PRIMARY KEY (`pc_id`);

--
-- Indexes for table `patient_facilities`
--
ALTER TABLE `patient_facilities`
  ADD PRIMARY KEY (`pf_id`);

--
-- Indexes for table `patient_files`
--
ALTER TABLE `patient_files`
  ADD PRIMARY KEY (`pf_id`);

--
-- Indexes for table `patient_invoices`
--
ALTER TABLE `patient_invoices`
  ADD PRIMARY KEY (`pi_id`);

--
-- Indexes for table `patient_medical_histories`
--
ALTER TABLE `patient_medical_histories`
  ADD PRIMARY KEY (`pmh_id`);

--
-- Indexes for table `patient_medication`
--
ALTER TABLE `patient_medication`
  ADD PRIMARY KEY (`pm_id`);

--
-- Indexes for table `patient_procedures`
--
ALTER TABLE `patient_procedures`
  ADD PRIMARY KEY (`pp_id`);

--
-- Indexes for table `procedures`
--
ALTER TABLE `procedures`
  ADD PRIMARY KEY (`procedure_id`);

--
-- Indexes for table `purchases`
--
ALTER TABLE `purchases`
  ADD PRIMARY KEY (`p_id`);

--
-- Indexes for table `purchase_details`
--
ALTER TABLE `purchase_details`
  ADD PRIMARY KEY (`pd_id`);

--
-- Indexes for table `relations`
--
ALTER TABLE `relations`
  ADD PRIMARY KEY (`relative_id`);

--
-- Indexes for table `report_type`
--
ALTER TABLE `report_type`
  ADD PRIMARY KEY (`r_id`);

--
-- Indexes for table `room`
--
ALTER TABLE `room`
  ADD PRIMARY KEY (`ro_id`);

--
-- Indexes for table `room_patient`
--
ALTER TABLE `room_patient`
  ADD PRIMARY KEY (`rp_id`);

--
-- Indexes for table `room_type`
--
ALTER TABLE `room_type`
  ADD PRIMARY KEY (`rt_id`);

--
-- Indexes for table `site_settings`
--
ALTER TABLE `site_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stocks`
--
ALTER TABLE `stocks`
  ADD PRIMARY KEY (`s_id`);

--
-- Indexes for table `stock_release`
--
ALTER TABLE `stock_release`
  ADD PRIMARY KEY (`sr_id`);

--
-- Indexes for table `subs_plan`
--
ALTER TABLE `subs_plan`
  ADD PRIMARY KEY (`subs_plan_id`);

--
-- Indexes for table `suppliers`
--
ALTER TABLE `suppliers`
  ADD PRIMARY KEY (`s_id`);

--
-- Indexes for table `tokens`
--
ALTER TABLE `tokens`
  ADD PRIMARY KEY (`t_id`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `work`
--
ALTER TABLE `work`
  ADD PRIMARY KEY (`w_id`);

--
-- Indexes for table `work_type`
--
ALTER TABLE `work_type`
  ADD PRIMARY KEY (`wt_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `a_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `admin_users`
--
ALTER TABLE `admin_users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `appointments`
--
ALTER TABLE `appointments`
  MODIFY `appointment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;
--
-- AUTO_INCREMENT for table `bed`
--
ALTER TABLE `bed`
  MODIFY `b_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `bed_patient`
--
ALTER TABLE `bed_patient`
  MODIFY `bp_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `bed_type`
--
ALTER TABLE `bed_type`
  MODIFY `bt_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `clinics`
--
ALTER TABLE `clinics`
  MODIFY `clinic_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `doctor_timing`
--
ALTER TABLE `doctor_timing`
  MODIFY `dt_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `drug_units`
--
ALTER TABLE `drug_units`
  MODIFY `du_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `employee_attendance`
--
ALTER TABLE `employee_attendance`
  MODIFY `ea_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `employee_payroll`
--
ALTER TABLE `employee_payroll`
  MODIFY `ep_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `expenses`
--
ALTER TABLE `expenses`
  MODIFY `e_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `facilities`
--
ALTER TABLE `facilities`
  MODIFY `facility_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `frequencies`
--
ALTER TABLE `frequencies`
  MODIFY `f_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `i_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `lab`
--
ALTER TABLE `lab`
  MODIFY `lab_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `labtracking`
--
ALTER TABLE `labtracking`
  MODIFY `lt_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `medical_conditions`
--
ALTER TABLE `medical_conditions`
  MODIFY `mc_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `medicines`
--
ALTER TABLE `medicines`
  MODIFY `m_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `patients`
--
ALTER TABLE `patients`
  MODIFY `patient_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `patient_activity`
--
ALTER TABLE `patient_activity`
  MODIFY `pa_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=128;
--
-- AUTO_INCREMENT for table `patient_appointment_invoices`
--
ALTER TABLE `patient_appointment_invoices`
  MODIFY `pai_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `patient_appointment_medications`
--
ALTER TABLE `patient_appointment_medications`
  MODIFY `pam_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `patient_case`
--
ALTER TABLE `patient_case`
  MODIFY `pc_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `patient_facilities`
--
ALTER TABLE `patient_facilities`
  MODIFY `pf_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `patient_files`
--
ALTER TABLE `patient_files`
  MODIFY `pf_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `patient_invoices`
--
ALTER TABLE `patient_invoices`
  MODIFY `pi_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `patient_medical_histories`
--
ALTER TABLE `patient_medical_histories`
  MODIFY `pmh_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `patient_medication`
--
ALTER TABLE `patient_medication`
  MODIFY `pm_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `patient_procedures`
--
ALTER TABLE `patient_procedures`
  MODIFY `pp_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `procedures`
--
ALTER TABLE `procedures`
  MODIFY `procedure_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `purchases`
--
ALTER TABLE `purchases`
  MODIFY `p_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `purchase_details`
--
ALTER TABLE `purchase_details`
  MODIFY `pd_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `relations`
--
ALTER TABLE `relations`
  MODIFY `relative_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `report_type`
--
ALTER TABLE `report_type`
  MODIFY `r_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `room`
--
ALTER TABLE `room`
  MODIFY `ro_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `room_patient`
--
ALTER TABLE `room_patient`
  MODIFY `rp_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `room_type`
--
ALTER TABLE `room_type`
  MODIFY `rt_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `site_settings`
--
ALTER TABLE `site_settings`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `stocks`
--
ALTER TABLE `stocks`
  MODIFY `s_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `stock_release`
--
ALTER TABLE `stock_release`
  MODIFY `sr_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `subs_plan`
--
ALTER TABLE `subs_plan`
  MODIFY `subs_plan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `suppliers`
--
ALTER TABLE `suppliers`
  MODIFY `s_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tokens`
--
ALTER TABLE `tokens`
  MODIFY `t_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;
--
-- AUTO_INCREMENT for table `user_role`
--
ALTER TABLE `user_role`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `work`
--
ALTER TABLE `work`
  MODIFY `w_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `work_type`
--
ALTER TABLE `work_type`
  MODIFY `wt_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
