'use strict';

module.exports = function(grunt) {

    // Time how long tasks take. Can help when optimizing build times
    require('time-grunt')(grunt);

    var activeProjectFiles = [
        '**',
        '.htaccess',
        '!**/idea/**',
        '!**/.vscode/**',
        '!**/Build/**',
        '!**/db/**',
        '!**/node_modules/**',
        '!**/docs/**',
        '!**/screenshots/**',
        '!**/test/**',
        '!**/vendor/**',
        '!**/*.gitignore',
        '!**/artisan.php',
        '!**/bitbucket-pipelines.yml',
        '!**/changelog.md',
        '!**/composer.json',
        '!**/composer.lock',
        '!**/config.json',
        '!**/erd.gliffy',
        '!**/GruntFile.js',
        '!**/package-lock.json',
        '!**/package.json',
        '!**/readme.md',
        '!**/npm-debug.log',
    ];

    // var   testing_database = 'digitemb_testing',
    //     mysql_user = 'tahseent_digitem', // password is blank :(
    //     mysql_password = 'LuyepBCPLJuTPzFe';

    //Project Configuration
    grunt.initConfig({
        //Metadata
        pkg : grunt.file.readJSON('package.json'),
        conf: grunt.file.readJSON('config.json'),

        //Sets as banner on minified JS file
        banner: '/*! <%= conf.title || conf.name %> - v<%= conf.version %> - ' +
            '<%= grunt.template.today("yyyy-mm-dd") %>\n' +
            '<%= conf.homepage ? "* " + conf.homepage + "\\n" : "" %>' +
            '* Copyright (c) <%= grunt.template.today("yyyy") %> <%= conf.author.name %>;' +
            'Licensed <%= _.pluck(conf.licenses, "type").join(", ") %> */\n',

        //Makes sure directory is clean before re-build
        clean: {
            build: {
                src: ['Build']
            },
            screenshots: {
                src: ['screenshots']
            },
            post_build: {
                src: ["Build/javascripts/plugins"]
            }
        },

        //removes console logging statements
        removelogging: {
            dist: {
                src: "Build/assets/**/*.js"
            }
        },

        //used during development to make sure
        //all files are newly copied
        copy: {
            build: {
                cwd: '',
                src: activeProjectFiles,
                dest: 'Build',
                expand: true
            },
        },

        //Concatinates JS files
        concat: {
            options: {
                separator: ';',
                stripBanners: {
                    block: true
                },

            },
            dist: {
                files: {
                    'Build/assets/admin/js/jquery.min.js' : ['Build/assets/admin/js/jquery-1.10.2.min.js'],
                    'Build/assets/admin/js/footer.min.js' : ['Build/assets/admin/js/gsap/main-gsap.min.js',
                        'Build/assets/admin/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js',
                        'Build/assets/admin/js/bootstrap.min.js',
                        'Build/assets/admin/js/moment/moment.min.js',
                        'Build/assets/admin/js/joinable.min.js',
                        'Build/assets/admin/js/resizeable.min.js',
                        'Build/assets/admin/js/neon-api.min.js',
                        'Build/assets/admin/js/select2/select2.min.js',
                        'Build/assets/admin/js/jquery.sparkline.min.js',
                        'Build/assets/admin/js/raphael.min.js',
                        'Build/assets/admin/js/morris.min.js',
                        'Build/assets/admin/js/jquery.validate.min.js',
                        'Build/assets/admin/js/additional-methods.min.js',
                        'Build/assets/admin/js/neon-login.min.js',
                        'Build/assets/admin/js/neon-custom.min.js',
                        'Build/assets/admin/js/jquery.magnific-popup.min.js',
                        'Build/assets/admin/js/custom.min.js',
                        'Build/assets/admin/js/custom/casedetail.min.js',
                        'Build/assets/admin/js/custom/profile.min.js',
                        'Build/assets/admin/js/custom/reports.min.js',
                        'Build/assets/admin/js/custom/show_appointments.min.js',
                    ],
                    'Build/javascripts/photos.min.js' : ['Build/assets/admin/js/jquery.knob.min.js',
                        'Build/assets/admin/js/jquery.ui.widget.min.js',
                        'Build/assets/admin/js/jquery.iframe-transport.min.js',
                        'Build/assets/admin/js/jquery.fileupload.min.js',
                        'Build/assets/admin/js/script.min.js'
                    ],
                    'Build/javascripts/datetimepicker.min.js' : [
                        'Build/assets/admin/js/bootstrap-datepicker.min.js',
                        'Build/assets/admin/js/bootstrap-timepicker.min.js'
                    ],
                    'Build/assets/admin/js/ckeditor/ckeditor.min.js' : [
                        'Build/assets/admin/js/bootstrap-datepicker.min.js',
                        'Build/assets/admin/js/bootstrap-timepicker.min.js'
                    ],
                },
            },
        },

        // Minifies JS file and place banner on top
        uglify: {
            main: {
                options: {
                },
                files: {
                    'Build/javascripts/footer_scripts.min.js': ['Build/javascripts/footer_scripts.js'],
                }
            }
        },

        //minifies CSS files and merge them into single file
        cssmin: {
            options: {
                shorthandCompacting: false,
                roundingPrecision: -1,
                keepSpecialComments: 0
            },
            target: {
                files: {
                    'Build/stylesheets/css/header_styles.min.css': ['Build/javascripts/plugins/font-awesome/css/font-awesome.min.css'],
                }
            }
        },

        //Process html files at build time to modify them
        //depending on the release environment
        processhtml: {
            options: {

            },
            dist: {
                files: {
                    'Build/base_layouts/partials/common/_header.html': 'base_layouts/partials/common/_header.html'
                }
            }
        },

        //Executes shell commands
        // exec: {
        //     change_permissions: 'chmod -R 777 Build',
        //     remove_extras: 'rm -R Build/javascripts/plugins',
        //     check_testing_database: {
        //         cmd: 'mysqlshow -u ' + mysql_user + ' -p ' + mysql_password + ' ' + testing_database,
        //         exitCodes: [0, 1],
        //         callback: function(error, stdout, stderr) {
        //             if(error) {
        //                 console.error('No database exists? Creating one '+ stderr);
        //                 grunt.task.run('exec:create_testing_database');
        //                 return;
        //             }

        //             console.log('Database already present, moving on...');
        //         }
        //     },
        //     create_testing_database: 'mysqladmin -u '  + mysql_user + ' create ' + testing_database
        // },

        jsObfuscate: {
            test: {
                options: {
                    concurrency: 2,
                    keepLinefeeds: false,
                    keepIndentations: false,
                    encodeStrings: true,
                    encodeNumbers: true,
                    moveStrings: true,
                    replaceNames: true,
                    variableExclusions: [ '^_get_', '^_set_', '^_mtd_' ]
                },
                files: {
                    'Build/javascripts/header_scripts.min.js': ['Build/javascripts/header_scripts.min.js']
                }
            }
        },


        protractor: {
            options: {
                keepAlive: true, // If false, the grunt process stops when the test fails.
                noColor: false, // If true, protractor will not use colors in its output.
            },
            test: {   // Grunt requires at least one target to run so you can simply put 'all: {}' here too.
                options: {
                    configFile: "test/config/conf.js", // Target-specific config file
                }
            },
        },


        htmlmin: {
            dist: {
                options: {
                    collapseWhitespace: true,
                    conservativeCollapse: true,
                    collapseBooleanAttributes: true,
                    removeCommentsFromCDATA: true,
                    minifyJS: true,
                    minifyCSS: true,
                    useShortDoctype: true,
                    removeRedundantAttributes: true,
                    removeAttributeQuotes: true,
                    removeComments: true,
                    minifyURLs: true
                },
                files: {
                    'Build/backend/views/pages/callagentDashboard.html' : 'Build/backend/views/pages/callagentDashboard.html'
                }
            }
        },

        // used once to rewrite the include labels.This is no longer needed in the build process
        'string-replace': {
            // Date Created: September 16, 2015
            // A hack to remove closing div's added by htmlmin
            build: {
                files: {
                    'Build/base_layouts/partials/common/_sidebar.html': 'Build/base_layouts/partials/common/_sidebar.html'
                },
                options: {
                    replacements: [{
                        pattern: / <\/div><\/div>/g,
                        replacement: " "
                    }]
                }
            },

            // Last Modified: September 16, 2015
            // changes databse name for testing
            // testing: { // replace the database name with testing db
            //     files: {
            //         'Build/backend/config/constants.php': 'Build/backend/config/constants.php'
            //     },
            //     options: {
            //         replacements:[{
            //             pattern: /define\('DATABASE','digitemb'\);/g,
            //             replacement: "define\('DATABASE','" + testing_database + "'\);"
            //         }]
            //     }
            // },
            // Date Created: September 16, 2015
            // Removes Dummy source from options
            // Used for testing
            remove_source: {
                files: {
                    'Build/base_layouts/partials/modals/customer.html': 'Build/base_layouts/partials/modals/customer.html',
                    'Build/base_layouts/partials/common/countries.html': 'Build/base_layouts/partials/common/countries.html'
                },
                options: {
                    replacements: [{
                        pattern: /<option value=(Dummy|DM)>Dummy<\/option>/g,
                        replacement: " "
                    }]
                }
            }
        }
    });

    //Loads tasks here
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-remove-logging');
    grunt.loadNpmTasks('grunt-processhtml');
    grunt.loadNpmTasks('grunt-exec');
    grunt.loadNpmTasks('js-obfuscator');
    grunt.loadNpmTasks('grunt-contrib-htmlmin');
    grunt.loadNpmTasks('grunt-string-replace');
    grunt.loadNpmTasks('grunt-protractor-runner');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('mkdirp');
    grunt.loadNpmTasks('mysql');
    grunt.loadNpmTasks('protractor-screenshot-reporter');

    //grunt.loadNpmTasks('grunt-filerev');
    //grunt.loadNpmTasks('grunt-usemin');
    //grunt.loadNpmTasks('grunt-contrib-watch');
    //grunt.loadNpmTasks('grunt-contrib-connect');
    //grunt.loadNpmTasks('grunt-prompt');
    //grunt.loadNpmTasks('grunt-ftp-deploy');

    // Date Created: September 16, 2015
    // This would be used while deploying on production
    grunt.registerTask('deploy', ['removelogging', 'clean:build', 'copy:build', 'processhtml', 'cssmin', 'concat', 'uglify', 'htmlmin', 'exec:change_permissions', 'jsObfuscate', 'string-replace:build', 'string-replace:remove_source', 'clean:post_build']);

    // Last Modified: September 16, 2015
    // This would be used during development
    // , 'processhtml', 'cssmin', 'concat', 'uglify', 'htmlmin', 'exec:change_permissions', 'jsObfuscate', 'string-replace:build', 'clean:post_build'
    grunt.registerTask('build', ['removelogging', 'clean:build', 'copy:build']);

    // Last Modified: September 16, 2015
    // This would be used when testing the app
    grunt.registerTask('test', ['string-replace:testing', 'clean:screenshots', 'protractor']);
};