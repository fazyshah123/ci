<?php

class PatientFacilitiesModel extends CI_Model
{
    public $tblName = 'patient_facilities';
    public $controller = 'patientfacility';
    public $colPrefix = 'pf_';
    public function __construct() {
	    // Call the Model constructor
	   	parent::__construct();
    }

    public function addRoomPatient() {
    	if($this->SqlModel->checkPermissions($this->controller, 'create')!==true) {
            $this->session->set_flashdata('alert','permerror');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        }
        if($this->input->post($this->colPrefix.'room_id')=="") {
            $this->session->set_flashdata('alert','error');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
            exit();
        }

        $data = array(
            $this->colPrefix.'id' => $this->input->post($this->colPrefix.'id'),
            $this->colPrefix.'patient_id' => $this->input->post($this->colPrefix.'patient_id'),
            $this->colPrefix.'facility_id' => $this->input->post($this->colPrefix.'facility_id'),
            $this->colPrefix.'pc_id' => $this->input->post($this->colPrefix.'pc_id'),
            $this->colPrefix.'clinic_id' => $this->session->userdata('clinic_id'),
            $this->colPrefix.'is_deleted' => '0'
        );        

        $q = $this->SqlModel->insertRecord($this->tblName, $data);
        $this->session->unset_userdata($this->controller.'_data');
        if($q!="") {
            $this->session->set_flashdata('alert','success');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        } else {
            $this->session->set_flashdata('alert','error');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        }
    }

    public function save($data = []) {
        $q = $this->SqlModel->insertRecord($this->tblName, $data);
        if($q!="") {
            return $q;
        } else {
            return false;
        }
    }
}