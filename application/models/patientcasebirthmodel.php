<?php

class PatientCaseBirthModel extends CI_Model
{
    public $tblName = 'patient_case_birth';
    public $controller = 'patientcasebirth';
    public $colPrefix = 'pcb_';
    public function __construct() {
	    // Call the Model constructor
	   	parent::__construct();
    }

    public function save($data=[]) {
        $q = $this->SqlModel->insertRecord($this->tblName, $data);
        $this->session->unset_userdata($this->controller.'_data');
        if($q!="") {
            $logData = [
                'message' => 'New child added.',
                'patient_id' => $data['pcb_patient_id']
            ];
            $this->PatientLogs->CreateLog($logData);
            return $q;
        } else {
            return false;
        }
    }
}