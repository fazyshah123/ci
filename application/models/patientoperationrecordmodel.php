<?php

class PatientOperationRecordModel extends CI_Model
{
    public $tblName = 'patient_operation_record';
    public $controller = 'patientoperationrecord';
    public $colPrefix = 'por_';
    public function __construct() {
	    // Call the Model constructor
	   	parent::__construct();
    }

    public function save($data=[]) {
        $q = $this->SqlModel->insertRecord($this->tblName, $data);
        $this->session->unset_userdata($this->controller.'_data');
        if($q!="") {
            $logData = [
                'message' => 'Operation record created.',
                'patient_id' => $data[$this->colPrefix.'patient_id']
            ];
            $this->PatientLogs->CreateLog($logData);
            return $q;
        } else {
            return false;
        }
    }
}