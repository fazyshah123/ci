<?php

class SqlModel extends CI_Model
{
	private $table;

    public function __construct() {
	    // Call the Model constructor
	   	parent::__construct();
    }


	/******** Start General Functions *******/

	//insert function
	function insertRecord($table , $colums) {
		if($this->db->insert($table , $colums))
			return $this->db->insert_id();
		else
			return false;
	}


	//update function
	function updateRecord($table , $colums , $condition) {
		if($this->db->update($table , $colums , $condition))
			return true;
		else
			return false;
	}


	function batchInsert($table , $data) {
		if($this->db->insert_batch($table,$data))
			return true;
		else
			return false;
	}


	function batchUpdate($table , $data , $whereKey) {
		if($this->db->update_batch($table,$data,$whereKey))
			return true;
		else
			return false;
	}


	// delete function
	function deleteRecord($table , $condition) {
		if($this->db->delete($table , $condition))
		{
			return true;
		}
		else
		{
			return false;
		}
	}


	function runQuery($sql, $flag='') {
		$this->result = $this->db->query($sql);
		if($flag)
		{
			return $this->result->row_array();
	    } else
	    {
			return $this->result->result_array();
	    }
	}


	function runInsert($sql) {
		$this->result = $this->db->query($sql);
		return True;
	}

	public function getSingleRecord($table, $where) {
		
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where($where);
		$count = $this->db->count_all_results();
		if($count=="1")
		{
			$query = $this->db->get_where($table, $where);
			$data=$query->row_array();
			return $data;
		}
		else
		{
			return null;
		}
	}


	public function getSingleRecordWithJoin($table, $tblid, $where, $join, $joinid) {
		
		$this->db->select('*');
		$this->db->from($table);
		$a = $join.'.'.$joinid;
		$b = $table.'.'.$tblid;
		$this->db->join($join, "$a = $b");
		$this->db->where($where);
		$query = $this->db->get();
		$data=$query->row_array();
		return $data;
	}


	public function getSingleRecordWithThreeJoin($fields, $table, $tbl1, $tbl1id, $tbl1joinid, $tbl2, $tbl2id, $tbl2joinid, $tbl3, $tbl3id, $tbl3joinid, $where) {
		
		$this->db->select($fields);
		$this->db->from($table);
		$a = $tbl1.'.'.$tbl1id;
		$b = $table.'.'.$tbl1joinid;
		
		$this->db->join($tbl1, "$a = $b");

		$a = $tbl2.'.'.$tbl2id;
		$b = $table.'.'.$tbl2joinid;
		$this->db->join($tbl2, "$a = $b");

		$a = $tbl3.'.'.$tbl3id;
		$b = $table.'.'.$tbl3joinid;
		$this->db->join($tbl3, "$a = $b");
		$this->db->where($where);
		$query = $this->db->get();
		$data=$query->row_array();
		return $data;
	}


	public function getAllRecordWithJoin($table, $tblid, $where, $join, $joinid) {
		
		$this->db->select('*');
		$this->db->from($table);
		$a = $join.'.'.$joinid;
		$b = $table.'.'.$tblid;
		$this->db->join($join, "$a = $b");
		$this->db->where($where);
		$query = $this->db->get();
		$data=$query->result_array();
		//$data=$query->row_array();
		return $data;
	}


	public function getUserPermission($table, $where) {
		
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where($where);
		$count = $this->db->count_all_results();
		if($count=="1")
		{
			$this->db->join('user_role','user_role.page_id = admin_users.user_role_id','LEFT');
			$query = $this->db->get_where($table, $where);
			$data=$query->row_array();
			return $data;
		}
		else
		{
			return null;
		}
	}


	public function getSingleField($col, $table, $where) {
		$this->db->select($col);
		$this->db->from($table);
		$this->db->where($where);
		$query = $this->db->get();
		$data = $query->row_array();
		if(isset($data[$col]))
		{
			return $data[$col];
		}
		else
		{
			return null;
		}
	}


	public function getRecords(
		$fields,
		$table,
		$sortby="",
		$order="",
		$where="",
		$search=array(),
		$limit="0",
		$start="0",
		$addqoutes=TRUE
	) {
		$this->db->select($fields,$addqoutes);
		$this->db->from($table);
		if(!empty($where))
		{
			$this->db->where($where);
		}

		if(
			!empty($search) &&
			isset($search['cols']) &&
			isset($search['value']) &&
			$search['cols'] !="" &&
			$search['value']!=""
		)
		{
			$like = "(";
			$colArray = explode(",",$search['cols']);
			foreach($colArray as $c)
			{
				$like .= " ".trim($c)." LIKE '%".$this->db->escape_like_str($search['value'])."%' OR ";
			}

			$vs = explode(" ",$search['value']);
			if(count($vs)>1)
			{
				foreach($vs as $v)
				{
					foreach($colArray as $c)
					{
						$like .= " ".trim($c)." LIKE '%".$this->db->escape_like_str($v)."%' OR ";
					}
				}
			}

			$like = rtrim($like,"OR ");
			$like .= ")";
			$this->db->where($like);
		}

		if(
			$sortby!="" &&
			$order!=""
		)
		{
			$this->db->order_by($sortby,$order);
		}

		if($limit != "0")
		{
			$this->db->limit($limit, $start);

		}
		$query = $this->db->get();
		$data=$query->result_array();
		return $data;
	}


	public function getExpensesRecords(
		$fields,
		$table,
		$a_join,
		$a_joinid,
		$a_tblid,
		$sortby="",
		$order="",
		$where="",
		$search=array(),
		$limit="0",
		$start="0",
		$addqoutes=TRUE
	) {
		$this->db->select($fields,$addqoutes);
		$this->db->from($table);
		$a = $a_join.'.'.$a_joinid;
		$b = $table.'.'.$a_tblid;
		$this->db->join($a_join, "$a = $b");

		if(!empty($where))
		{
			$this->db->where($where);
		}

		if(
			!empty($search) &&
			isset($search['cols']) &&
			isset($search['value']) &&
			$search['cols'] !="" &&
			$search['value']!=""
		)
		{
			$like = "(";
			$colArray = explode(",",$search['cols']);
			foreach($colArray as $c)
			{
				$like .= " ".trim($c)." LIKE '%".$this->db->escape_like_str($search['value'])."%' OR ";
			}

			$vs = explode(" ",$search['value']);
			if(count($vs)>1)
			{
				foreach($vs as $v)
				{
					foreach($colArray as $c)
					{
						$like .= " ".trim($c)." LIKE '%".$this->db->escape_like_str($v)."%' OR ";
					}
				}
			}

			$like = rtrim($like,"OR ");
			$like .= ")";
			$this->db->where($like);
		}

		if(
			$sortby!="" &&
			$order!=""
		)
		{
			$this->db->order_by($sortby,$order);
		}

		if($limit != "0")
		{
			$this->db->limit($limit, $start);

		}
		$query = $this->db->get();
		$data=$query->result_array();
		return $data;
	}

	public function getStockReleaseRecords(
		$fields,
		$table,
		$i_join,
		$i_joinid,
		$i_tblid,
		$it_join,
		$it_joinid,
		$it_tblid,
		$sortby="",
		$order="",
		$where="",
		$search=array(),
		$limit="0",
		$start="0",
		$addqoutes=TRUE
	) {
		$this->db->select($fields,$addqoutes);
		$this->db->from($table);

		$a = $i_join.'.'.$i_joinid;
		$b = $table.'.'.$i_tblid;
		$this->db->join($i_join, "$a = $b");

		$a = $it_join.'.'.$it_joinid;
		$b = $table.'.'.$it_tblid;
		$this->db->join($it_join, "$a = $b");

		if(!empty($where))
		{
			$this->db->where($where);
		}

		if(
			!empty($search) &&
			isset($search['cols']) &&
			isset($search['value']) &&
			$search['cols'] !="" &&
			$search['value']!=""
		)
		{
			$like = "(";
			$colArray = explode(",",$search['cols']);
			foreach($colArray as $c)
			{
				$like .= " ".trim($c)." LIKE '%".$this->db->escape_like_str($search['value'])."%' OR ";
			}

			$vs = explode(" ",$search['value']);
			if(count($vs)>1)
			{
				foreach($vs as $v)
				{
					foreach($colArray as $c)
					{
						$like .= " ".trim($c)." LIKE '%".$this->db->escape_like_str($v)."%' OR ";
					}
				}
			}

			$like = rtrim($like,"OR ");
			$like .= ")";
			$this->db->where($like);
		}

		if(
			$sortby!="" &&
			$order!=""
		)
		{
			$this->db->order_by($sortby,$order);
		}

		if($limit != "0")
		{
			$this->db->limit($limit, $start);

		}
		$query = $this->db->get();
		$data=$query->result_array();
		return $data;
	}


	public function getRecordsWithSingleJoin(
		$fields,
		$table,
		$tbl_join,
		$tbl_joinid,
		$tbl_tblid,
		$sortby="",
		$order="",
		$where="",
		$search=array(),
		$limit="0",
		$start="0",
		$addqoutes=TRUE
	) {
		$this->db->select($fields,$addqoutes);
		$this->db->from($table);

		$a = $tbl_join.'.'.$tbl_joinid;
		$b = $table.'.'.$tbl_tblid;
		$this->db->join($tbl_join, "$a = $b");

		if(!empty($where))
		{
			$this->db->where($where);
		}

		if(
			!empty($search) &&
			isset($search['cols']) &&
			isset($search['value']) &&
			$search['cols'] !="" &&
			$search['value']!=""
		)
		{
			$like = "(";
			$colArray = explode(",",$search['cols']);
			foreach($colArray as $c)
			{
				$like .= " ".trim($c)." LIKE '%".$this->db->escape_like_str($search['value'])."%' OR ";
			}

			$vs = explode(" ",$search['value']);
			if(count($vs)>1)
			{
				foreach($vs as $v)
				{
					foreach($colArray as $c)
					{
						$like .= " ".trim($c)." LIKE '%".$this->db->escape_like_str($v)."%' OR ";
					}
				}
			}

			$like = rtrim($like,"OR ");
			$like .= ")";
			$this->db->where($like);
		}

		if(
			$sortby!="" &&
			$order!=""
		)
		{
			$this->db->order_by($sortby,$order);
		}

		if($limit != "0")
		{
			$this->db->limit($limit, $start);

		}
		$query = $this->db->get();
		$data=$query->result_array();
		return $data;
	}

	public function getRecordsWithTwoJoinSameTable(
		$fields,
		$table,
		$tbl1_join,
		$tbl1_joinid,
		$tbl1_tblid,
		$tbl1_alias,
		$tbl2_join,
		$tbl2_joinid,
		$tbl2_tblid,
		$tbl2_alias,
		$sortby="",
		$order="",
		$where="",
		$search=array(),
		$limit="0",
		$start="0",
		$addqoutes=TRUE
	) {
		$this->db->select($fields,$addqoutes);
		$this->db->from($table);
		$a = $tbl1_alias.'.'.$tbl1_joinid;
		$b = $table.'.'.$tbl1_tblid;
		$this->db->join($tbl1_join, "$a = $b");

		$a = $tbl2_alias.'.'.$tbl2_joinid;
		$b = $table.'.'.$tbl2_tblid;
		$this->db->join($tbl2_join, "$a = $b");

		if(!empty($where))
		{
			$this->db->where($where);
		}

		if(
			!empty($search) &&
			isset($search['cols']) &&
			isset($search['value']) &&
			$search['cols'] !="" &&
			$search['value']!=""
		)
		{
			$like = "(";
			$colArray = explode(",",$search['cols']);
			foreach($colArray as $c)
			{
				$like .= " ".trim($c)." LIKE '%".$this->db->escape_like_str($search['value'])."%' OR ";
			}

			$vs = explode(" ",$search['value']);
			if(count($vs)>1)
			{
				foreach($vs as $v)
				{
					foreach($colArray as $c)
					{
						$like .= " ".trim($c)." LIKE '%".$this->db->escape_like_str($v)."%' OR ";
					}
				}
			}

			$like = rtrim($like,"OR ");
			$like .= ")";
			$this->db->where($like);
		}

		if(
			$sortby!="" &&
			$order!=""
		)
		{
			$this->db->order_by($sortby,$order);
		}

		if($limit != "0")
		{
			$this->db->limit($limit, $start);

		}
		$query = $this->db->get();
		$data=$query->result_array();
		return $data;
	}

	public function getRecordsWithTwoJoin(
		$fields,
		$table,
		$tbl1_join,
		$tbl1_joinid,
		$tbl1_tblid,
		$tbl2_join,
		$tbl2_joinid,
		$tbl2_tblid,
		$sortby="",
		$order="",
		$where="",
		$search=array(),
		$limit="0",
		$start="0",
		$addqoutes=TRUE
	) {
		$this->db->select($fields,$addqoutes);
		$this->db->from($table);
		$a = $tbl1_join.'.'.$tbl1_joinid;
		$b = $table.'.'.$tbl1_tblid;
		$this->db->join($tbl1_join, "$a = $b");

		$a = $tbl2_join.'.'.$tbl2_joinid;
		$b = $table.'.'.$tbl2_tblid;
		$this->db->join($tbl2_join, "$a = $b");

		if(!empty($where))
		{
			$this->db->where($where);
		}

		if(
			!empty($search) &&
			isset($search['cols']) &&
			isset($search['value']) &&
			$search['cols'] !="" &&
			$search['value']!=""
		)
		{
			$like = "(";
			$colArray = explode(",",$search['cols']);
			foreach($colArray as $c)
			{
				$like .= " ".trim($c)." LIKE '%".$this->db->escape_like_str($search['value'])."%' OR ";
			}

			$vs = explode(" ",$search['value']);
			if(count($vs)>1)
			{
				foreach($vs as $v)
				{
					foreach($colArray as $c)
					{
						$like .= " ".trim($c)." LIKE '%".$this->db->escape_like_str($v)."%' OR ";
					}
				}
			}

			$like = rtrim($like,"OR ");
			$like .= ")";
			$this->db->where($like);
		}

		if(
			$sortby!="" &&
			$order!=""
		)
		{
			$this->db->order_by($sortby,$order);
		}

		if($limit != "0")
		{
			$this->db->limit($limit, $start);

		}
		$query = $this->db->get();
		$data=$query->result_array();
		return $data;
	}

	public function getRecordsWithThreeJoin(
		$fields,
		$table,
		$tbl1_join,
		$tbl1_joinid,
		$tbl1_tblid,
		$tbl2_join,
		$tbl2_joinid,
		$tbl2_tblid,
		$tbl3_join,
		$tbl3_joinid,
		$tbl3_tblid,
		$sortby="",
		$order="",
		$where="",
		$search=array(),
		$limit="0",
		$start="0",
		$addqoutes=TRUE
	) {
		$this->db->select($fields,$addqoutes);
		$this->db->from($table);
		$a = $tbl1_join.'.'.$tbl1_joinid;
		$b = $table.'.'.$tbl1_tblid;
		$this->db->join($tbl1_join, "$a = $b");

		$a = $tbl2_join.'.'.$tbl2_joinid;
		$b = $table.'.'.$tbl2_tblid;
		$this->db->join($tbl2_join, "$a = $b");

		$a = $tbl3_join.'.'.$tbl3_joinid;
		$b = $table.'.'.$tbl3_tblid;
		$this->db->join($tbl3_join, "$a = $b");

		if(!empty($where))
		{
			$this->db->where($where);
		}

		if(
			!empty($search) &&
			isset($search['cols']) &&
			isset($search['value']) &&
			$search['cols'] !="" &&
			$search['value']!=""
		)
		{
			$like = "(";
			$colArray = explode(",",$search['cols']);
			foreach($colArray as $c)
			{
				$like .= " ".trim($c)." LIKE '%".$this->db->escape_like_str($search['value'])."%' OR ";
			}

			$vs = explode(" ",$search['value']);
			if(count($vs)>1)
			{
				foreach($vs as $v)
				{
					foreach($colArray as $c)
					{
						$like .= " ".trim($c)." LIKE '%".$this->db->escape_like_str($v)."%' OR ";
					}
				}
			}

			$like = rtrim($like,"OR ");
			$like .= ")";
			$this->db->where($like);
		}

		if(
			$sortby!="" &&
			$order!=""
		)
		{
			$this->db->order_by($sortby,$order);
		}

		if($limit != "0")
		{
			$this->db->limit($limit, $start);

		}
		$query = $this->db->get();
		$data=$query->result_array();
		return $data;
	}

	public function getRecordsWithFourJoin(
		$fields,
		$table,
		$tbl1_join,
		$tbl1_joinid,
		$tbl1_tblid,
		$tbl2_join,
		$tbl2_joinid,
		$tbl2_tblid,
		$tbl3_join,
		$tbl3_joinid,
		$tbl3_tblid,
		$tbl4_join,
		$tbl4_joinid,
		$tbl4_tblid,
		$sortby="",
		$order="",
		$where="",
		$search=array(),
		$limit="0",
		$start="0",
		$addqoutes=TRUE
	) {
		$this->db->select($fields,$addqoutes);
		$this->db->from($table);
		$a = $tbl1_join.'.'.$tbl1_joinid;
		$b = $table.'.'.$tbl1_tblid;
		$this->db->join($tbl1_join, "$a = $b");

		$a = $tbl2_join.'.'.$tbl2_joinid;
		$b = $table.'.'.$tbl2_tblid;
		$this->db->join($tbl2_join, "$a = $b");

		$a = $tbl3_join.'.'.$tbl3_joinid;
		$b = $table.'.'.$tbl3_tblid;
		$this->db->join($tbl3_join, "$a = $b");

		$a = $tbl4_join.'.'.$tbl4_joinid;
		$b = $table.'.'.$tbl4_tblid;
		$this->db->join($tbl4_join, "$a = $b");

		if(!empty($where))
		{
			$this->db->where($where);
		}

		if(
			!empty($search) &&
			isset($search['cols']) &&
			isset($search['value']) &&
			$search['cols'] !="" &&
			$search['value']!=""
		)
		{
			$like = "(";
			$colArray = explode(",",$search['cols']);
			foreach($colArray as $c)
			{
				$like .= " ".trim($c)." LIKE '%".$this->db->escape_like_str($search['value'])."%' OR ";
			}

			$vs = explode(" ",$search['value']);
			if(count($vs)>1)
			{
				foreach($vs as $v)
				{
					foreach($colArray as $c)
					{
						$like .= " ".trim($c)." LIKE '%".$this->db->escape_like_str($v)."%' OR ";
					}
				}
			}

			$like = rtrim($like,"OR ");
			$like .= ")";
			$this->db->where($like);
		}

		if(
			$sortby!="" &&
			$order!=""
		)
		{
			$this->db->order_by($sortby,$order);
		}

		if($limit != "0")
		{
			$this->db->limit($limit, $start);

		}
		$query = $this->db->get();
		$data=$query->result_array();
		return $data;
	}

	public function getPurchasesRecords(
		$fields,
		$table,
		$s_join,
		$s_joinid,
		$s_tblid,
		$a_join,
		$a_joinid,
		$a_tblid,
		$sortby="",
		$order="",
		$where="",
		$search=array(),
		$limit="0",
		$start="0",
		$addqoutes=TRUE
	) {
		$this->db->select($fields,$addqoutes);
		$this->db->from($table);
		$a = $s_join.'.'.$s_joinid;
		$b = $table.'.'.$s_tblid;
		$this->db->join($s_join, "$a = $b");

		$a = $a_join.'.'.$a_joinid;
		$b = $table.'.'.$a_tblid;
		$this->db->join($a_join, "$a = $b");

		if(!empty($where))
		{
			$this->db->where($where);
		}

		if(
			!empty($search) &&
			isset($search['cols']) &&
			isset($search['value']) &&
			$search['cols'] !="" &&
			$search['value']!=""
		)
		{
			$like = "(";
			$colArray = explode(",",$search['cols']);
			foreach($colArray as $c)
			{
				$like .= " ".trim($c)." LIKE '%".$this->db->escape_like_str($search['value'])."%' OR ";
			}

			$vs = explode(" ",$search['value']);
			if(count($vs)>1)
			{
				foreach($vs as $v)
				{
					foreach($colArray as $c)
					{
						$like .= " ".trim($c)." LIKE '%".$this->db->escape_like_str($v)."%' OR ";
					}
				}
			}

			$like = rtrim($like,"OR ");
			$like .= ")";
			$this->db->where($like);
		}

		if(
			$sortby!="" &&
			$order!=""
		)
		{
			$this->db->order_by($sortby,$order);
		}

		if($limit != "0")
		{
			$this->db->limit($limit, $start);

		}
		$query = $this->db->get();
		$data=$query->result_array();
		return $data;
	}


	public function getLabtrackingRecords(
		$fields,
		$table,
		$pa_join,
		$pa_joinid,
		$pa_tblid,
		$dtr_join,
		$dtr_joinid,
		$dtr_tblid,
		$proc_join,
		$proc_joinid,
		$proc_tblid,
		$lab_join,
		$lab_joinid,
		$lab_tblid,
		$sortby="",
		$order="",
		$where="",
		$search=array(),
		$limit="0",
		$start="0",
		$addqoutes=TRUE
	) {
		$this->db->select($fields,$addqoutes);
		$this->db->from($table);
		$a = $pa_join.'.'.$pa_joinid;
		$b = $table.'.'.$pa_tblid;
		$this->db->join($pa_join, "$a = $b");

		$a = $dtr_join.'.'.$dtr_joinid;
		$b = $table.'.'.$dtr_tblid;
		$this->db->join($dtr_join, "$a = $b");

		$a = $proc_join.'.'.$proc_joinid;
		$b = $table.'.'.$proc_tblid;
		$this->db->join($proc_join, "$a = $b");

		$a = $lab_join.'.'.$lab_joinid;
		$b = $table.'.'.$lab_tblid;
		$this->db->join($lab_join, "$a = $b");
		if(!empty($where))
		{
			$this->db->where($where);
		}

		if(
			!empty($search) &&
			isset($search['cols']) &&
			isset($search['value']) &&
			$search['cols'] !="" &&
			$search['value']!=""
		)
		{
			$like = "(";
			$colArray = explode(",",$search['cols']);
			foreach($colArray as $c)
			{
				$like .= " ".trim($c)." LIKE '%".$this->db->escape_like_str($search['value'])."%' OR ";
			}

			$vs = explode(" ",$search['value']);
			if(count($vs)>1)
			{
				foreach($vs as $v)
				{
					foreach($colArray as $c)
					{
						$like .= " ".trim($c)." LIKE '%".$this->db->escape_like_str($v)."%' OR ";
					}
				}
			}

			$like = rtrim($like,"OR ");
			$like .= ")";
			$this->db->where($like);
		}

		if(
			$sortby!="" &&
			$order!=""
		)
		{
			$this->db->order_by($sortby,$order);
		}

		if($limit != "0")
		{
			$this->db->limit($limit, $start);

		}
		$query = $this->db->get();
		$data=$query->result_array();
		return $data;
	}


	public function countRecords($table, $where=array(),$search=array()) {
		$this->db->select('*');
		$this->db->from($table);
		if(!empty($where))
		{
			$this->db->where($where);
		}
		if(
			!empty($search) &&
			isset($search['cols']) &&
			isset($search['value']) &&
			$search['cols'] !="" &&
			$search['value']!=""
		)
		{
			$like = "(";
			$colArray = explode(",",$search['cols']);
			foreach($colArray as $c)
			{
				$like .= " ".trim($c)." LIKE '%".$this->db->escape_like_str($search['value'])."%' OR ";
			}

			$vs = explode(" ",$search['value']);
			if(count($vs)>1)
			{
				foreach($vs as $v)
				{
					foreach($colArray as $c)
					{
						$like .= " ".trim($c)." LIKE '%".$this->db->escape_like_str($v)."%' OR ";
					}
				}
			}

			$like = rtrim($like,"OR ");
			$like .= ")";
			$this->db->where($like);
		}
		$records = $this->db->count_all_results();
		return $records;
	}


	public function checkCookie() {
		if(
			isset($_COOKIE['dstacklog']) &&
			$_COOKIE['dstacklog']!=""
		)
		{
			$id = $this->encrypt->decode($_COOKIE['dstacklog']);
			$user_data = $this->getUserDataById($id);
			if(!empty($user_data))
			{
				$this->session->set_userdata('user_id',$user_data['id']);
				$this->session->set_userdata('fb_uid',$user_data['fb_uid']);
				$this->session->set_userdata('email',$user_data['email']);
				$this->session->set_userdata('full_name',$user_data['full_name']);
				$this->session->set_userdata('role', $user_data['user_role']);
				$this->session->set_userdata('auth', '1');
				redirect(base_url().'home','location');
			}
			else
			{
				return;
			}
		}
		else
		{
			return;
		}
	}


	public function truncate($table="") {
		if($table=="")
		{
			return;
		}
		if($this->db->truncate($table))
		{
			return true;
		}
		else
		{
			return false;
		}
	}


	public function seostring($rstring="") {
		$string = preg_replace('/\%/',' percentage',$rstring);
		$string = preg_replace('/\@/',' at ',$string);
		$string = preg_replace('/\&/',' AND ',$string);
		$string = preg_replace('/\s[\s]+/','-',$string);    // Strip off multiple spaces
		$string = preg_replace('/[\s\W]+/','-',$string);    // Strip off spaces AND non-alpha-numeric
		$string = preg_replace('/^[\-]+/','',$string); // Strip off the starting hyphens
		$string = preg_replace('/[\-]+$/','',$string); // // Strip off the ending hyphens
		$string = strtolower($string);
		$string = str_replace(" ","-",$string).'.html';
		return $string;
	}


	public function authAdmin($authKey="",$adminID="") {
		if(
			$authKey == "allow" &&
			$adminID != ""
		)
		{
			$adminData = $this->getSingleRecord('admin_users' , array('id'=>$adminID,'status'=>'Enable'));
			if(empty($adminID))
			{
				return false;
			}
			else
			{
				$this->setTitle();
				return $adminData;
			}
		}
		else
		{
			return false;
		}
	}


	public function setTitle() {
		$clinic_id = $this->session->userdata('clinic_id');
		$webTitle = $this->getSingleField('website_title', 'site_settings', ['clinic_id'=>$clinic_id]);
		if (!defined('PROJECT_TITLE'))
			define('PROJECT_TITLE',$webTitle );
	}


	public function checkAccess($type="",$uD=array()) {
		if(
			$type=="" ||
			empty($uD)
		)
		{
			redirect(ADMIN_URL);
			exit;
		}
		if($uD['user_role']=="Admin"&&$uD[$type]=="No")
		{
			return false;
		}
		else
		{
			return true;
		}
	}


	public function publish() {
		$page_pub = "UPDATE pages p SET page_status='Published', page_pdate='0000-00-00 00:00:00' WHERE p.`page_pdate`<='".date('Y-m-d H:i:s')."' AND p.`page_pdate`!='0000-00-00 00:00:00'";
		@$this->db->query($page_pub);

		$page_pub = "UPDATE pages p SET page_status='Un-Published', page_update='0000-00-00 00:00:00' WHERE p.`page_update`<='".date('Y-m-d H:i:s')."' AND p.`page_update`!='0000-00-00 00:00:00'";
		@$this->db->query($page_pub);

		$blog_pub = "UPDATE blogs p SET blog_status='Published', blog_pdate='0000-00-00 00:00:00' WHERE p.`blog_pdate`<='".date('Y-m-d H:i:s')."' AND p.`blog_pdate`!='0000-00-00 00:00:00'";
		@$this->db->query($page_pub);

		$blog_pub = "UPDATE blogs p SET blog_status='Un-Published', blog_update='0000-00-00 00:00:00' WHERE p.`blog_update`<='".date('Y-m-d H:i:s')."' AND p.`blog_update`!='0000-00-00 00:00:00'";
		@$this->db->query($page_pub);
	}


	public function getAllRoles($role = '') {
		$clinic_id = $this->session->userdata('clinic_id');
		if ($role != '') {
			$qr = ' AND admin_users.user_role_id = "'.$role.'" ';
		} else {
		$qr = '';
		}
		if ($this->session->userdata('user_role_id') != '1') {
			$query = "SELECT admin_users.*, user_role.role_title  FROM admin_users LEFT JOIN user_role ON user_role.page_id = admin_users.user_role_id WHERE admin_users.is_deleted = '0' AND user_role.is_deleted = '0' AND admin_users.clinic_id = '$clinic_id' ".$qr." order by admin_users.id";
		} else if ($this->session->userdata('user_role_id') == '1') {
 			$query = "SELECT admin_users.*, user_role.role_title  FROM admin_users LEFT JOIN user_role ON user_role.page_id = admin_users.user_role_id WHERE admin_users.is_deleted = '0' AND user_role.is_deleted = '0' ".$qr." order by admin_users.id";
		}
		return $this->SqlModel->runQuery($query);
	}


	/* Get Patients */
	public function getPatients() {
		return $this->runQuery("SELECT patient_id, patient_name FROM patients WHERE patient_is_deleted = '0' AND patient_clinic_id = '".$this->session->userdata('clinic_id')."'",true);
	}


	public function checkPermissions($controller, $action) {
		$permission = json_decode($this->session->userdata('permission'));

		if(isset($permission->$controller->$action) && $permission->$controller->$action == 'true') {
			return true;
		}
		return false;
	}


	public function checkUserRole($role='') {
		if ($role === $this->session->userdata('role_title')) {
			return true;
		} else {
			return false;
		}
	}


	// Returns current User ID
	public function getUserID() {
		return $this->session->userdata['admin_id'];
	}


	public function getMedicalConditions() {
		$result = [];
	
		$sql = "SELECT mc_id, mc_name FROM medical_conditions";
		
		$result = $this->runQuery($sql);
	
		return $result;
	}


	public function getTodayAppointments() {
		$query = "SELECT appointments.appointment_id, appointments.appointment_start_time, patients.patient_name, admin_users.full_name, patients.patient_id FROM appointments LEFT JOIN patients ON patients.patient_id = appointments.appointment_patient_id LEFT JOIN admin_users ON admin_users.id = appointments.appointment_dtr_id WHERE appointment_is_deleted = '0' AND appointment_date LIKE '".date('Y-m-d', strtotime('now'))."' AND appointment_clinic_id = '".$this->session->userdata('clinic_id')."' AND appointment_is_canceled = '0' AND appointment_checked='0' order by appointment_start_time limit 50";
		return $this->SqlModel->runQuery($query);
	}


	public function getTodayTokens() {
		$query = "SELECT tokens.*, patients.patient_name, admin_users.full_name FROM tokens LEFT JOIN patients ON patients.patient_id = tokens.t_patient_id LEFT JOIN admin_users ON admin_users.id = tokens.t_doctor_id WHERE t_is_deleted = '0' AND t_date LIKE '".date('Y-m-d', strtotime('now'))."' AND t_clinic_id = '".$this->session->userdata('clinic_id')."' AND tokens.checked='0' order by t_number limit 50";
		return $this->SqlModel->runQuery($query);
	}


	public function getTodaysAppointmentCount() {
		$query = "SELECT COUNT(*) AS count FROM appointments WHERE appointment_is_deleted = '0' AND appointment_date = '".date('Y-m-d', strtotime('now'))."' AND appointment_clinic_id = '".$this->session->userdata('clinic_id')."' AND appointment_is_canceled = '0'";
		return $this->SqlModel->runQuery($query);
	}


	public function getTotalDoctors() {
		$query = "SELECT COUNT(*) AS count FROM admin_users WHERE is_deleted = '0' AND clinic_id = '".$this->session->userdata('clinic_id')."' AND user_role_id = '".$this->session->userdata('doctor_role_id')."'";
		return $this->SqlModel->runQuery($query);
	}


	public function getTotalPatients() {
		$query = "SELECT COUNT(*) AS count FROM patients WHERE patient_is_deleted = '0' AND patient_clinic_id = '".$this->session->userdata('clinic_id')."'";
		return $this->SqlModel->runQuery($query);
	}


	public function getTotalDues() {
		$query = "SELECT COUNT(*) AS count FROM invoices WHERE invoice_is_deleted = '0' AND invoice_clinic_id = '".$this->session->userdata('clinic_id')."'";
		return $this->SqlModel->runQuery($query);
	}


	public function getAppointmentsRange($start_date, $end_date) {
		$query = "SELECT concat(appointments.appointment_date,'T',appointment_start_time) start, concat(appointments.appointment_date,'T',appointment_end_time) end, concat(patients.patient_name, ' with Dr.',admin_users.full_name) title,concat(patients.patient_name, ' with Dr.',admin_users.full_name) description FROM appointments LEFT JOIN patients ON patients.patient_id = appointments.appointment_patient_id LEFT JOIN admin_users ON admin_users.id = appointments.appointment_dtr_id WHERE appointment_is_deleted = '0' AND appointment_date BETWEEN '".date('Y-m-d', strtotime($start_date))."' AND '".date('Y-m-d', strtotime($end_date))."' AND appointment_clinic_id = '".$this->session->userdata('clinic_id')."' AND appointment_is_canceled = '0' order by appointment_start_time limit 50";
		return $this->runQuery($query);
	}

	public function getPatientReports($patient_id) {
		$query = "SELECT pf.*, rt.r_name FROM patient_files pf LEFT JOIN report_type rt ON rt.r_id = pf.pf_reporttype_id WHERE pf.pf_is_deleted = '0' AND pf.pf_clinic_id = '".$this->session->userdata('clinic_id')."' AND pf.pf_patient_id = '$patient_id'";
		return $this->runQuery($query);
	}
	

	/* Get Patients */
	public function getPatientsDropDown() {
		return $this->runQuery("SELECT patient_id, patient_name, patient_mr_id FROM patients WHERE patient_is_deleted = '0' AND patient_clinic_id = '".$this->session->userdata('clinic_id')."'");
	}
	

	/* Get Accounts */
	public function getAccountsDropDown() {
		return $this->runQuery("SELECT a_id, a_name FROM accounts WHERE a_is_deleted = '0' AND a_clinic_id = '".$this->session->userdata('clinic_id')."'");
	}
	

	/* Get Suppliers */
	public function getSuppliersDropDown() {
		return $this->runQuery("SELECT s_id, s_name, s_payable FROM suppliers WHERE s_is_deleted = '0' AND s_clinic_id = '".$this->session->userdata('clinic_id')."'");
	}
	

	/* Get Items */
	public function getItemsDropDown() {
		return $this->runQuery("SELECT i_id, i_name, i_unit_price FROM items WHERE i_is_deleted = '0' AND i_clinic_id = '".$this->session->userdata('clinic_id')."'");
	}
	

	/* Get Doctors */
	public function getDoctorsDropDown() {
		return $this->runQuery("SELECT id, full_name FROM admin_users WHERE is_deleted = '0' AND clinic_id = '".$this->session->userdata('clinic_id')."' AND `user_role_id` = '".$this->session->userdata('doctor_role_id')."'");
	}
	

	/* Get AdminUsers */
	public function getAdminUsersDropDown() {
		return $this->runQuery("SELECT id, full_name FROM admin_users WHERE is_deleted = '0' AND clinic_id = '".$this->session->userdata('clinic_id')."'");
	}
	

	/* Get ROles */
	public function getRolesDropDown() {
		return $this->runQuery("SELECT page_id, role_title FROM user_role WHERE is_deleted = '0' AND clinic_id = '".$this->session->userdata('clinic_id')."'");
	}

	/* Get ReportType */
	public function getReportTypeDropDown() {
		return $this->runQuery("SELECT r_id, r_name FROM report_type WHERE r_is_deleted = '0' AND r_clinic_id = '".$this->session->userdata('clinic_id')."'");
	}

	/* Get Dropdown data */
	public function getBedTypeDropDown() {
		return $this->runQuery("SELECT bt_id, bt_name FROM bed_type WHERE bt_is_deleted = '0' AND bt_clinic_id = '".$this->session->userdata('clinic_id')."'");
	}

	public function getRelationDropDown() {
		return $this->runQuery("SELECT relative_id, relative_name FROM relations WHERE relative_is_deleted = '0' AND relative_clinic_id = '".$this->session->userdata('clinic_id')."'");
	}

	/* Get Dropdown data */
	public function getRoomTypeDropDown() {
		return $this->runQuery("SELECT rt_id, rt_name FROM room_type WHERE rt_is_deleted = '0' AND rt_clinic_id = '".$this->session->userdata('clinic_id')."'");
	}

	/* Get Dropdown data */
	public function getBedDropDown() {
		return $this->runQuery("SELECT b.b_id, b.b_number, bt.bt_name, bt.bt_per_day_charges, bt.bt_billing_type FROM bed b LEFT JOIN bed_type bt ON bt.bt_id = b.b_bed_type_id WHERE b_is_deleted = '0' AND b_clinic_id = '".$this->session->userdata('clinic_id')."'");
	}

	/* Get Dropdown data */
	public function getRoomDropDown() {
		return $this->runQuery("SELECT r.ro_id, r.ro_number, rt.rt_name, rt.rt_per_day_charges, rt.rt_billing_type, r.ro_is_booked FROM room r LEFT JOIN room_type rt ON rt.rt_id = r.ro_room_type_id WHERE ro_is_deleted = '0' AND ro_clinic_id = '".$this->session->userdata('clinic_id')."'");
	}

	public function getProcedureDropDown() {
		return $this->runQuery("SELECT procedure_id, procedure_name, procedure_price FROM procedures WHERE procedure_is_deleted = '0' AND procedure_clinic_id = '".$this->session->userdata('clinic_id')."'");
	}
	// {{DROPDOWN_METHODS}}

	public function getEmployeeAttendance($user_id, $month) {
		return $this->runQuery("SELECT ea.*, au.full_name FROM employee_attendance ea LEFT JOIN admin_users au ON au.id = ea.ea_user_id WHERE MONTH(ea_date) = '$month' AND ea_is_deleted = '0' AND ea_clinic_id = '".$this->session->userdata('clinic_id')."'");
	}

	public function getLastID() {
		return $this->runQuery("SELECT patient_id FROM patients ORDER BY patient_id DESC LIMIT 1", true);
	}

	public function getLastToken() {
		return $this->runQuery("SELECT t_number FROM tokens WHERE t_date = '".date('Y-m-d', strtotime('now'))."' AND t_is_deleted = '0' AND t_clinic_id = '".$this->session->userdata('clinic_id')."' AND t_doctor_id = '".$this->input->post('dtr_id')."' ORDER BY t_id DESC LIMIT 1");
	}

	// Reports Methods
	public function getPatientCount($start_date, $end_date) {
		$result = [];
		$clinic_id = $this->session->userdata('clinic_id');
	
		$sql = "SELECT COUNT(patient_id) FROM patients WHERE p_is_deleted = '0' AND `patient_added` = '$start_date' AND `patient_added` = '$end_date' AND `patient_clinic_id` = '$clinic_id'";
		
		return $this->runQuery($sql)[0];
	}


	public function getTotalEarnings($start_date, $end_date) {
		$result = [];
		$clinic_id = $this->session->userdata('clinic_id');
	
		$sql = "SELECT SUM(pi_amount) earnings FROM patient_invoices WHERE pi_is_deleted = '0' AND `pi_added` BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59' AND `pi_clinic_id` = '$clinic_id'";
		
		return $this->runQuery($sql)[0];
	}

	public function getAppointments($start_date, $end_date) {
		$result = [];
		$clinic_id = $this->session->userdata('clinic_id');
		$sql = "SELECT COUNT(appointment_id) total_appointments FROM appointments WHERE `appointment_date` BETWEEN '$start_date' AND '$end_date' AND `appointment_is_deleted` = '0' AND `appointment_clinic_id` = '$clinic_id' AND appointment_is_canceled = '0'";
		
		return $this->runQuery($sql)[0];
	}

	public function getExpenses($start_date, $end_date) {
		$result = [];
		$clinic_id = $this->session->userdata('clinic_id');
		$sql = "SELECT SUM(e_amount) total_expenses FROM expenses WHERE `e_is_deleted` = '0' AND `e_date` BETWEEN '$start_date' AND '$end_date' AND e_clinic_id = '$clinic_id'";
		
		return $this->runQuery($sql)[0];
	}

	public function getIssuedItems($start_date, $end_date) {
		$result = [];
		$clinic_id = $this->session->userdata('clinic_id');
		$sql = "SELECT SUM(sr_qty) total_items_issued FROM stock_release WHERE sr_is_deleted = '0' AND sr_clinic_id = '$clinic_id' AND sr_added BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59'";
		
		return $this->runQuery($sql)[0];
	}

	public function getSupplierPayable() {
		$result = [];
		$clinic_id = $this->session->userdata('clinic_id');
		$sql = "SELECT SUM(s_payable) total_payable FROM suppliers WHERE `s_is_deleted` = '0' AND `s_clinic_id` = '$clinic_id'";
		
		return $this->runQuery($sql)[0];
	}

	public function getNewPatients($start_date, $end_date) {
		$result = [];
		$clinic_id = $this->session->userdata('clinic_id');
	
		$sql = "SELECT COUNT(patient_id) new_patients FROM patients WHERE patient_is_deleted = '0' AND patient_clinic_id = '$clinic_id' AND patient_added BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59'";
		
		return $this->runQuery($sql)[0];
	}

	public function getPurchases($start_date, $end_date) {
		$result = [];
		$clinic_id = $this->session->userdata('clinic_id');
	
		$sql = "SELECT SUM(p_total) total_purchase FROM purchases WHERE p_is_deleted = '0' AND `p_clinic_id` = '$clinic_id' AND `p_date` BETWEEN '$start_date' AND '$end_date'";
		
		return $this->runQuery($sql)[0];
	}

	public function getStaffAttendance($start_date, $end_date) {
		$result = [];
		$clinic_id = $this->session->userdata('clinic_id');

		$sql = "SELECT au.full_name, ea.ea_attendance, ea.ea_checkin, ea.ea_checkout, ea.ea_absent_reason, ea.ea_date FROM employee_attendance ea  LEFT JOIN admin_users au on au.id = ea.ea_user_id WHERE ea_clinic_id = '$clinic_id' AND ea_date BETWEEN '$start_date' AND '$end_date' AND ea_is_deleted = '0'";
		
		$result = $this->runQuery($sql);
	
		return $result;
	}

	public function getExpenseDetails($start_date, $end_date) {
		$result = [];
		$clinic_id = $this->session->userdata('clinic_id');
	
		$sql = "SELECT e.e_description, e.e_amount, e.e_added, au.full_name, a.a_name  FROM expenses e LEFT JOIN admin_users au ON au.id = e.e_created_by LEFT JOIN accounts a ON a.a_id = e.e_account_id WHERE e.e_is_deleted = '0' AND e.e_clinic_id = '$clinic_id' AND e_date BETWEEN '$start_date' AND '$end_date' ";
		
		$result = $this->runQuery($sql);
	
		return $result;
	}

	public function getPurchaseDetails($start_date, $end_date) {
		$result = [];
		$clinic_id = $this->session->userdata('clinic_id');
	
		$sql = "SELECT p.p_remarks, p.p_total, p.p_added, au.full_name, a.a_name  FROM purchases p LEFT JOIN admin_users au ON au.id = p.p_created_by LEFT JOIN accounts a ON a.a_id = p.p_account_id WHERE p.p_is_deleted = '0' AND p.p_clinic_id = '$clinic_id' AND p_date BETWEEN '$start_date' AND '$end_date' ";
		
		$result = $this->runQuery($sql);
	
		return $result;
	}

	public function getIssuedItemDetails($start_date, $end_date) {
		$result = [];
		$clinic_id = $this->session->userdata('clinic_id');
	
		$sql = "SELECT sr.sr_description, sr.sr_qty, sr.sr_added, au.full_name, i.i_name  FROM stock_release sr LEFT JOIN admin_users au ON au.id = sr.sr_created_by LEFT JOIN items i ON i.i_id = sr.sr_item_id WHERE sr.sr_is_deleted = '0' AND sr.sr_clinic_id = '$clinic_id' AND sr_added BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59' ";
		
		$result = $this->runQuery($sql);
	
		return $result;
	}

	public function getPatientTimelineItems($activity_id='0', $patient_id) {
		$clinic_id = $this->session->userdata('clinic_id');
		if ($activity_id == '0') {
			$activity_query = '';
		} else {
			$activity_query = " AND pa.pa_added < '$activity_id' ";
		}
		$sql = "SELECT pa.*, au.full_name FROM patient_activity pa LEFT JOIN admin_users au ON au.id = pa.pa_created_by WHERE pa_clinic_id = '$clinic_id' AND pa_patient_id = '$patient_id' " . $activity_query . " ORDER BY pa_added DESC limit 10";
		return $this->runQuery($sql);
	}

	public function getPatientTimelineItemsCount($patient_id) {
		$clinic_id = $this->session->userdata('clinic_id');
		$sql = "SELECT COUNT(pa.pa_id) item_count FROM patient_activity pa LEFT JOIN admin_users au ON au.id = pa.pa_created_by WHERE pa_clinic_id = '$clinic_id' AND pa_patient_id = '$patient_id' ORDER BY pa_added DESC";
		return $this->runQuery($sql);
	}

	public function getPatientTimelineItemsRemaining($activity_id, $patient_id) {
		$clinic_id = $this->session->userdata('clinic_id');
		$sql = "SELECT COUNT(pa.pa_id) remaining_item_count FROM patient_activity pa LEFT JOIN admin_users au ON au.id = pa.pa_created_by WHERE pa_clinic_id = '$clinic_id' AND pa_patient_id = '$patient_id' AND pa.pa_added < '$activity_id' ORDER BY pa_added DESC";
		return $this->runQuery($sql);
	}

	public function getUpcomingTokens() {
		$result = [];
		$today = date('Y-m-d', strtotime('now'));
		$clinic_id = $this->session->userdata('clinic_id');
		$sql = "SELECT t.*, p.patient_name, au.full_name FROM tokens t LEFT JOIN patients p ON p.patient_id = t.t_patient_id LEFT JOIN admin_users au ON au.id = t.t_doctor_id WHERE t.t_clinic_id = '$clinic_id' AND t.checked = '0' AND t.t_is_deleted = '0' AND t.`t_status` = 'Enabled' AND t.`t_date` = '$today'";
		
		$result = $this->runQuery($sql);
	
		return $result;
	}

	public function getPatientCountByClinic() {
		$result = [];
	
		$sql = "SELECT COUNT(patient_id) patient_count FROM patients WHERE patient_clinic_id = '".$this->session->userdata('clinic_id')."'";
		
		$result = $this->runQuery($sql);
	
		return $result;
	}

	public function getDoctorsList() {
		$sql = "SELECT full_name, id FROM admin_users WHERE is_deleted='0' AND clinic_id='".$this->session->userdata('clinic_id')."' AND user_role_id = '".$this->session->userdata('doctor_role_id')."'";
		return $this->runQuery($sql);
	}

	public function getFacilities() {
		$sql = "SELECT facility_name, facility_id FROM facilities WHERE facility_is_deleted='0' AND facility_clinic_id='".$this->session->userdata('clinic_id')."'";
		return $this->runQuery($sql);
	}

	public function getTreatmentByName($name='') {
		$sql = "SELECT t_id FROM treatments WHERE t_name = '$name' AND t_is_deleted = '0' AND t_clinic_id = '".$this->session->userdata('clinic_id')."'";
		return $this->runQuery($sql);
	}

	public function getPatientCases($patient_id='') {
		$sql = "SELECT pc_case_number, pc_created, pc_id FROM patient_case WHERE pc_patient_id = '$patient_id' AND pc_is_deleted = '0' AND pc_clinic_id = '".$this->session->userdata('clinic_id')."'";
		return $this->runQuery($sql);
	}
}