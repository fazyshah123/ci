<?php

class PatientTreatmentModel extends CI_Model
{
    public $tblName = 'patient_treatment';
    public $controller = 'patienttreatment';
    public $colPrefix = 'pt_';
    public function __construct() {
	    // Call the Model constructor
	   	parent::__construct();
    }

    public function save($data=[]) {
        $q = $this->SqlModel->insertRecord($this->tblName, $data);
        $this->session->unset_userdata($this->controller.'_data');
        if($q!="") {
            $dtr = $this->SqlModel->getSingleField('full_name', 'admin_users', ['id'=>$data[$this->colPrefix.'dtr_id']]);
            $logData = [
                'message' => $dtr . ' provided oxygen inhalation treatment from '. date("d-m-Y h:i A", strtotime($data[$this->colPrefix.'start_datetime'])) . ' to '. date("d-m-Y h:i A", strtotime($data[$this->colPrefix.'end_datetime'])),
                'patient_id' => $data[$this->colPrefix.'patient_id']
            ];
            if ($data[$this->colPrefix.'diagnosis'] != '') {
                $logData['message'] .= '   <blockquote>'.$data[$this->colPrefix.'diagnosis'].'</blockquote>';
            }
            $this->PatientLogs->CreateLog($logData);
            return $q;
        } else {
            return false;
        }
    }
}