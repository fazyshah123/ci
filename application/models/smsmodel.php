<?php

class SMSModel extends CI_Model {
	public $sms;

    private $api_key = "f9e1ac00831084fbe93c196dc3381e4191f4d673";
    public $params = array(
        'login'     => 'BitCloud',
        'phone'     => '923430505458',
        'sender'    => 'BitCloud',
        'text'      => '',
        'timestamp' => ''
    );

	public function __construct() {
        parent::__construct();
    }

    public function Send($message, $phone = '923430505458') {
        $this->params['phone'] = $phone;
        $this->params['text'] = $message;
        $this->params['timestamp'] = $this->getTimeStamp();

        $Signature = $this->Signature($this->params, $this->api_key);

        $link = "http://sms.vasway.net/external/get/send.php?login=BitCloud&signature=$Signature&phone=".$this->params['phone']."&text=".urlencode($this->params['text'])."&sender=".$this->params['sender']."&timestamp=".$this->params['timestamp'];

        $d = curl_init($link);
        curl_setopt($d, CURLOPT_RETURNTRANSFER, true);
        $ts2 = curl_exec($d);

        if (curl_error($d))
            die(curl_error($d));
        curl_getinfo($d, CURLINFO_HTTP_CODE);
        curl_close($d);
    }

    public function Signature( $params, $api_key )
    {
        ksort( $params );
        reset( $params );
     
        return md5( implode( $params ) . $api_key );
    }

    public function getTimeStamp() {
        $c = curl_init("http://sms.vasway.net/external/get/timestamp.php");
        curl_setopt($c, CURLOPT_RETURNTRANSFER, true);

        $ts = curl_exec($c);

        if (curl_error($c))
            die(curl_error($c));

        // Get the status code
        $status = curl_getinfo($c, CURLINFO_HTTP_CODE);

        curl_close($c);
        return $ts;
    }
}