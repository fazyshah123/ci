<?php

class PatientAppointmentInvoicesModel extends CI_Model
{
    public function __construct() {
	    // Call the Model constructor
	   	parent::__construct();
    }

    public function save($tblName='', $colPrefix='', $data=[]) {
        $q = $this->SqlModel->insertRecord($tblName, $data);
        if($q!="") {
            return $q;
        } else {
            return false;
        }
    }
}