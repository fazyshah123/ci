<?php

class BedModel extends CI_Model
{
    public $tblName = 'bed';
    public $controller = 'bed';
    public $colPrefix = 'b_';
    public function __construct() {
	    // Call the Model constructor
	   	parent::__construct();
    } 

    public function addBed($controller='', $tblName='', $colPrefix='') {
    	if($this->SqlModel->checkPermissions('bed', 'create')!==true) {
            $this->session->set_flashdata('alert','permerror');
            redirect(base_url().'manage/'.$controller.'/index','location');
        }
        if($this->input->post($colPrefix.'number')=="") {
            $this->session->set_flashdata('alert','error');
            redirect(base_url().'manage/'.$controller.'/index','location');
            exit();
        }

        $data = array(
            $this->colPrefix.'number' => addslashes($this->input->post($this->colPrefix.'number')),
            $this->colPrefix.'bed_type_id' => addslashes($this->input->post($this->colPrefix.'bed_type_id')),
            $this->colPrefix.'is_booked' => '0',
            $this->colPrefix.'status' => 'Enabled',
            $this->colPrefix.'created_by' => $this->session->userdata('admin_id'),
            $this->colPrefix.'updated_by' => $this->session->userdata('admin_id'),
            $this->colPrefix.'added' => date('Y-m-d H:i:s', strtotime('now')),
            $this->colPrefix.'updated' => date('Y-m-d H:i:s', strtotime('now')),
            $this->colPrefix.'is_deleted' => '0',
            $this->colPrefix.'clinic_id' => $this->session->userdata('clinic_id')
        );

        $q = $this->SqlModel->insertRecord($tblName, $data);
        $this->session->unset_userdata($controller.'_data');
        if($q!="") {;
            $this->session->set_flashdata('alert','success');
            redirect(base_url().'manage/'.$controller.'/index','location');
        } else {
            $this->session->set_flashdata('alert','error');
            redirect(base_url().'manage/'.$controller.'/index','location');
        }
    } 

    public function save($controller='', $tblName='', $colPrefix='', $data=[]) {
        $q = $this->SqlModel->insertRecord($tblName, $data);
        $this->session->unset_userdata($controller.'_data');
        if($q!="") {
            return true;
        } else {
            return false;
        }
    }

    public function setBedBooked($bed_id) {
        $d = ['b_is_booked' => '1'];
        $this->SqlModel->updateRecord($this->tblName, $d, array($this->colPrefix.'id'=>$bed_id));
    }

    public function setBedUnbooked($bed_id) {
        $d = ['b_is_booked' => '0'];
        $this->SqlModel->updateRecord($this->tblName, $d, array($this->colPrefix.'id'=>$bed_id));
    }
}