<?php

class PatientCaseModel extends CI_Model
{
    public $tblName = 'patient_case';
    public $controller = 'patientcase';
    public $colPrefix = 'pc_';
    public function __construct() {
	    // Call the Model constructor
	   	parent::__construct();
    } 

    public function addPatientCase() {
    	if($this->SqlModel->checkPermissions('patientcase', 'create')!==true) {
            $this->session->set_flashdata('alert','permerror');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        }
        if($this->input->post($colPrefix.'patient_id')=="" && $this->input->post($colPrefix.'case_number')=="") {
            $this->session->set_flashdata('alert','error');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
            exit();
        }

        $data = array(
            $colPrefix.'case_number' => $this->input->post($colPrefix.'case_number'),
            $colPrefix.'patient_id' => $this->input->post($colPrefix.'patient_id'),
            $colPrefix.'dtr_id' => $this->input->post($colPrefix.'dtr_id'),
            $colPrefix.'charges' => $this->input->post($colPrefix.'charges'),
            $colPrefix.'relative_id' => $this->input->post($colPrefix.'relative_id'),
            $colPrefix.'relative_name' => $this->input->post($colPrefix.'relative_name'),
            $colPrefix.'status' => 'Enabled',
            $colPrefix.'created_by' => $this->session->userdata('admin_id'),
            $colPrefix.'updated_by' => $this->session->userdata('admin_id'),
            $colPrefix.'added' => date('Y-m-d H:i:s', strtotime('now')),
            $colPrefix.'updated' => date('Y-m-d H:i:s', strtotime('now')),
            $colPrefix.'is_deleted' => '0',
            $colPrefix.'clinic_id' => $this->session->userdata('clinic_id')
        );

        $q = $this->SqlModel->insertRecord($this->tblName, $data);
        $this->session->unset_userdata($this->controller.'_data');
        if($q!="") {
            $this->session->set_flashdata('alert','success');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        } else {
            $this->session->set_flashdata('alert','error');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        }
    } 

    public function save($data=[]) {
        $q = $this->SqlModel->insertRecord($this->tblName, $data);
        $this->session->unset_userdata($this->controller.'_data');
        if($q!="") {
            $logData = [
                'message' => 'New case has been created with <a href="'.ADMIN_URL.'patientcase/details/'.$data['pc_case_number'].'">Case # ' . $data['pc_case_number'] . '</a>',
                'patient_id' => $data['pc_patient_id']
            ];
            $this->PatientLogs->CreateLog($logData);
            return $q;
        } else {
            return false;
        }
    }

    public function getBedByInvoiceId($invoiceId='') {
        $where = ['bp_invoice_id'=>$invoiceId];
        return $this->SqlModel->getRecords('*', $this->tblName, 'bp_id', 'ASC',  $where);
    }
}