<?php

class InvoicesModel extends CI_Model
{
    public $tblName = 'patient_invoices';
    public $colPrefix = 'pi_';
    public $controller = 'invoices';

    public function __construct() {
	    // Call the Model constructor
	   	parent::__construct();
    }

    public function getInvoiceById($invoiceId='') {
        return $this->SqlModel->getSingleRecord($this->tblName, [$this->colPrefix.'id'=>$invoiceId]);
    }
}