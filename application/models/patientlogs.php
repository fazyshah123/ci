<?php

class PatientLogs extends CI_Model
{
	private $table = 'patient_activity';

    public function __construct() {
	    // Call the Model constructor
	   	parent::__construct();
    }

    public function CreateLog($data=[]) {
        $data = array(
            'pa_clinic_id' => $this->session->userdata('clinic_id'),
            'pa_message' => $data['message'],
            'pa_patient_id' => $data['patient_id'],
            'pa_created_by' => $this->session->userdata('admin_id'),
            'pa_added' => date('Y-m-d H:i:s', strtotime('now')),
            'pa_created_by' => $this->session->userdata('admin_id')
            
        );
        return $this->SqlModel->insertRecord($this->table, $data);
    }

    public function CreatePatientAddedLog($patient_id) {
    	$patient = $this->SqlModel->getSingleRecord('patients', ['patient_id' => $patient_id]);
    	$data = array(
            'pa_clinic_id' => $this->session->userdata('clinic_id'),
            'pa_message' => 'Patient created.',
            'pa_patient_id' => $patient_id,
            'pa_created_by' => $this->session->userdata('admin_id'),
            'pa_added' => date('Y-m-d H:i:s', strtotime('now')),
            'pa_created_by' => $this->session->userdata('admin_id')
            
        );
        return $this->SqlModel->insertRecord($this->table, $data);
    }

    public function CreatePatientUpdatedLog($patient_id) {
    	$patient = $this->SqlModel->getSingleRecord('patients', ['patient_id' => $patient_id]);
    	$data = array(
            'pa_clinic_id' => $this->session->userdata('clinic_id'),
            'pa_message' => 'Information has been updated.',
            'pa_patient_id' => $patient_id,
            'pa_created_by' => $this->session->userdata('admin_id'),
            'pa_added' => date('Y-m-d H:i:s', strtotime('now')),
            'pa_created_by' => $this->session->userdata('admin_id')
            
        );
        return $this->SqlModel->insertRecord($this->table, $data);
    }

    public function CreatePatientDeletedLog($patient_id) {
    	$patient = $this->SqlModel->getSingleRecord('patients', ['patient_id' => $patient_id]);
    	$data = array(
            'pa_clinic_id' => $this->session->userdata('clinic_id'),
            'pa_message' => 'Patient profile deleted!',
            'pa_patient_id' => $patient_id,
            'pa_created_by' => $this->session->userdata('admin_id'),
            'pa_added' => date('Y-m-d H:i:s', strtotime('now')),
            'pa_created_by' => $this->session->userdata('admin_id')
            
        );
        return $this->SqlModel->insertRecord($this->table, $data);
    }

    public function CreatePatientAppointmentLog($patient_id, $appointment_time, $appointment_date, $dtr_name) {
    	$patient = $this->SqlModel->getSingleRecord('patients', ['patient_id' => $patient_id]);
    	$data = array(
            'pa_clinic_id' => $this->session->userdata('clinic_id'),
            'pa_message' => 'New appointment has been created for '.date('d-M-Y h:i A', strtotime($appointment_date . ' ' . $appointment_time)) . ' with Dr. ' . $dtr_name . '!',
            'pa_patient_id' => $patient_id,
            'pa_created_by' => $this->session->userdata('admin_id'),
            'pa_added' => date('Y-m-d H:i:s', strtotime('now')),
            'pa_created_by' => $this->session->userdata('admin_id')
            
        );
        return $this->SqlModel->insertRecord($this->table, $data);
    }

    public function CreatePatientFileAddedLog($patient_id, $file_name) {
        $patient = $this->SqlModel->getSingleRecord('patients', ['patient_id' => $patient_id]);
        $data = array(
            'pa_clinic_id' => $this->session->userdata('clinic_id'),
            'pa_message' => 'New report added with file name <a href="' . FRONTEND_ASSETS .'images/patientfiles/'.$file_name . '">'.$file_name.'</a>!',
            'pa_patient_id' => $patient_id,
            'pa_created_by' => $this->session->userdata('admin_id'),
            'pa_added' => date('Y-m-d H:i:s', strtotime('now')),
            'pa_created_by' => $this->session->userdata('admin_id')
            
        );
        return $this->SqlModel->insertRecord($this->table, $data);
    }

    public function CreatePatientAppointmentRescheduledLog($patient_id, $appointment_time, $dtr_name) {
        $patient = $this->SqlModel->getSingleRecord('patients', ['patient_id' => $patient_id]);
        $data = array(
            'pa_clinic_id' => $this->session->userdata('clinic_id'),
            'pa_message' => 'Appointment rescheduled with Dr. '. $dtr_name.' for '.$appointment_time .'!',
            'pa_patient_id' => $patient_id,
            'pa_created_by' => $this->session->userdata('admin_id'),
            'pa_added' => date('Y-m-d H:i:s', strtotime('now')),
            'pa_created_by' => $this->session->userdata('admin_id')
            
        );
        return $this->SqlModel->insertRecord($this->table, $data);
    }

    public function CreatePatientAppointmentCanceledLog($patient_id, $dtr_name, $adt) {
        $patient = $this->SqlModel->getSingleRecord('patients', ['patient_id' => $patient_id]);
        $data = array(
            'pa_clinic_id' => $this->session->userdata('clinic_id'),
            'pa_message' => 'Appointment canceled with Dr. '.$dtr_name .' for '. $adt,
            'pa_patient_id' => $patient_id,
            'pa_created_by' => $this->session->userdata('admin_id'),
            'pa_added' => date('Y-m-d H:i:s', strtotime('now')),
            'pa_created_by' => $this->session->userdata('admin_id')
            
        );
        return $this->SqlModel->insertRecord($this->table, $data);
    }

    public function CreatePatientLabTrackingLog($patient_id, $lab, $worktype, $work, $due) {
        $data = array(
            'pa_clinic_id' => $this->session->userdata('clinic_id'),
            'pa_message' => 'New lab work ordered with '.$lab.' for '.$worktype.' in '.$work.', due by '.$due,
            'pa_patient_id' => $patient_id,
            'pa_created_by' => $this->session->userdata('admin_id'),
            'pa_added' => date('Y-m-d H:i:s', strtotime('now')),
            'pa_created_by' => $this->session->userdata('admin_id')
            
        );
        return $this->SqlModel->insertRecord($this->table, $data);
    }
}