<?php

class PatientModel extends CI_Model
{
    public $tblName = 'patients';
    public $colPrefix = 'patient_';
    public $controller = 'patients';

    public function __construct() {
	    // Call the Model constructor
	   	parent::__construct();
    }

    public function addPatient() {
    	if($this->SqlModel->checkPermissions('patients', 'create')!==true) {
            $this->session->set_flashdata('alert','permerror');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        }
        if($this->input->post($this->colPrefix.'name')=="") {
            $this->session->set_flashdata('alert','error');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
            exit();
        }

        $data = array(
            $this->colPrefix.'name' => $this->input->post($this->colPrefix.'name'),
            $this->colPrefix.'mr_id' => $this->generateNextID(),
            $this->colPrefix.'status' => $this->input->post($this->colPrefix.'status'),
            $this->colPrefix.'email' => $this->input->post($this->colPrefix.'email'),
            $this->colPrefix.'phone1' => $this->input->post($this->colPrefix.'phone1'),
            $this->colPrefix.'phone2' => $this->input->post($this->colPrefix.'phone2'),
            $this->colPrefix.'phone3' => $this->input->post($this->colPrefix.'phone3'),
            $this->colPrefix.'gender' => $this->input->post($this->colPrefix.'gender'),
            $this->colPrefix.'industry_id' => $this->input->post($this->colPrefix.'industry'),
            $this->colPrefix.'profession_id' => $this->input->post($this->colPrefix.'profession'),
            $this->colPrefix.'job_level_id' => $this->input->post($this->colPrefix.'job_level_id'),
            $this->colPrefix.'subindustry_id' => $this->input->post($this->colPrefix.'subindustry'),
            $this->colPrefix.'jobfunction_id' => $this->input->post($this->colPrefix.'jobfunction'),
            $this->colPrefix.'birth_date' => date('Y-m-d H:i:s', strtotime($this->input->post($this->colPrefix.'birth_date'))),
            $this->colPrefix.'age' => $this->input->post($this->colPrefix.'age'),
            $this->colPrefix.'address' => $this->input->post($this->colPrefix.'address'),
            $this->colPrefix.'refered_by' => $this->input->post($this->colPrefix.'refered_by'),
            $this->colPrefix.'added' => date('Y-m-d H:i:s', strtotime('now')),
			$this->colPrefix.'updated' => date('Y-m-d H:i:s', strtotime('now')),
            $this->colPrefix.'created_by' => $this->session->userdata('admin_id'),
            $this->colPrefix.'modified_by' => $this->session->userdata('admin_id'),
            $this->colPrefix.'is_deleted' => '0',
            $this->colPrefix.'clinic_id' => $this->session->userdata('clinic_id'),
        );

        $config['upload_path'] = './assets/frontend/images/patients/';
        $config['allowed_types'] = 'jpg|png|jpeg|ico|x-icon|icon';
        $config['max_size'] = '102400';
        $config['remove_spaces'] = true;
        $this->load->library('upload', $config);
        $this->load->library('image_lib');

        if(isset($_FILES['patient_image'])) {
            if ($this->upload->do_upload('patient_image')) {
                $filename_ephoto = $this->upload->data('patient_image');
                $data['patient_image'] = $filename_ephoto['file_name'];
            }
        }

        $q = $this->SqlModel->insertRecord($this->tblName, $data);
        $this->session->unset_userdata($this->controller.'_data');
        if($q!="") {
            $this->PatientLogs->CreatePatientAddedLog($q);
            $this->session->set_flashdata('alert','success');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        }
        else {
            $this->session->set_flashdata('alert','error');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        }
    }

    public function save($data) {
        $q = $this->SqlModel->insertRecord($this->tblName, $data);
        $this->session->unset_userdata($this->controller.'_data');
        if($q!="") {
            $this->PatientLogs->CreatePatientAddedLog($q);
            return $q;
        }
        else {
            return false;
        }
    }

    public function generateNextID() {
        if (empty($this->SqlModel->getLastID())) {
            return $this->session->userdata('clinic_id').'1';    
        } else {
            $patientCount = $this->SqlModel->getPatientCountByClinic();
            
            return $this->session->userdata('clinic_id').'-'.(++$patientCount[0]['patient_count']);
        }
    }

    public function getPatientById($patientId='') {
        return $this->SqlModel->getSingleRecord($this->tblName, [$this->colPrefix.'id'=>$patientId]);
    }
}