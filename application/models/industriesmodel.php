<?php

class IndustriesModel extends CI_Model
{
    public $tblName = 'industries';
    public $colPrefix = 'i_';
    public $controller = 'industries';

    public function __construct() {
	    // Call the Model constructor
	   	parent::__construct();
    }

    public function getIndustriesDropDown($parent_id=0) {
        if ($parent_id == 0) {
            return $this->SqlModel->runQuery("SELECT i_name, i_id FROM industries WHERE i_is_deleted = '0' AND i_status = 'Enabled' AND i_parent_id = '0'");
        } else {
            return $this->SqlModel->runQuery("SELECT i_name, i_id FROM industries WHERE i_is_deleted = '0' AND i_status = 'Enabled' and i_parent_id = '$parent_id'");
        }
    }
}