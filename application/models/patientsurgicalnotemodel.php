<?php

class PatientSurgicalNoteModel extends CI_Model
{
    public $tblName = 'patient_surgical_note';
    public $controller = 'patientsurgicalnote';
    public $colPrefix = 'psn_';
    public function __construct() {
	    // Call the Model constructor
	   	parent::__construct();
    }

    public function save($data=[]) {
        $q = $this->SqlModel->insertRecord($this->tblName, $data);
        if($q!="") {
            $logData = [
                'message' => 'Surgical note has been created.',
                'patient_id' => $data['psn_patient_id']
            ];
            $this->PatientLogs->CreateLog($logData);
            return $q;
        } else {
            return false;
        }
    }
}