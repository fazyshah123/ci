<?php

class RemindersModel extends CI_Model
{
    public $tblName = 'reminders';
    public $colPrefix = 'r_';
    public $controller = 'reminders';

    public function __construct() {
	    // Call the Model constructor
	   	parent::__construct();
    }

    public function save($data) {
        $q = $this->SqlModel->insertRecord($this->tblName, $data);
        $this->session->unset_userdata($this->controller.'_data');
        if($q!="") {
            $this->PatientLogs->CreatePatientAddedLog($q);
            return $q;
        }
        else {
            return false;
        }
    }

    public function getRemindersWidget() {
        $where = '';
        $where .= $this->colPrefix.'is_deleted = "0" AND (' . $this->colPrefix.'created_by = ' . $this->session->userdata('admin_id') . ' OR ' . $this->colPrefix.'report_to = ' . $this->session->userdata('admin_id') . ' OR ' . $this->colPrefix.'assigned_to = ' . $this->session->userdata('admin_id') . ' OR ' . $this->colPrefix.'type = "Public") AND ' . $this->colPrefix.'clinic_id = ' . $this->session->userdata('clinic_id') . ' AND ' . $this->colPrefix.'completed = 0';
        return $this->SqlModel->getRecordsWithTwoJoinSameTable('r_remind_on, r_id, r_text, r_completed, r_priority, r_assigned_to, r_report_to, au.full_name assigned_to, au2.full_name report_to', 'reminders', 'admin_users au', 'id', 'r_assigned_to', 'au', 'admin_users au2', 'id', 'r_report_to', 'au2', 'r_remind_on', 'DESC',  $where, [], 10);
    }

    public function getRemindersCount() {
        $where = '';
        $where .= $this->colPrefix.'is_deleted = "0" AND (' . $this->colPrefix.'created_by = ' . $this->session->userdata('admin_id') . ' OR ' . $this->colPrefix.'report_to = ' . $this->session->userdata('admin_id') . ' OR ' . $this->colPrefix.'assigned_to = ' . $this->session->userdata('admin_id') . ' OR ' . $this->colPrefix.'type = "Public") AND ' . $this->colPrefix.'clinic_id = ' . $this->session->userdata('clinic_id') . ' AND ' . $this->colPrefix.'completed = 0';
        return $this->SqlModel->getRecordsWithTwoJoinSameTable('Count(r_remind_on) total', 'reminders', 'admin_users au', 'id', 'r_assigned_to', 'au', 'admin_users au2', 'id', 'r_report_to', 'au2', 'r_remind_on', 'DESC',  $where);
    }
}