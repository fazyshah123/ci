<?php

class RoomPatientModel extends CI_Model
{
    public $tblName = 'room_patient';
    public $controller = 'roompatient';
    public $colPrefix = 'rp_';
    public function __construct() {
	    // Call the Model constructor
	   	parent::__construct();
    }

    public function addRoomPatient() {
    	if($this->SqlModel->checkPermissions('roompatient', 'create')!==true) {
            $this->session->set_flashdata('alert','permerror');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        }
        if($this->input->post($this->colPrefix.'room_id')=="") {
            $this->session->set_flashdata('alert','error');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
            exit();
        }

        $data = array(
            $this->colPrefix.'room_id' => $this->input->post($this->colPrefix.'room_id'),
            $this->colPrefix.'patient_id' => $this->input->post($this->colPrefix.'patient_id'),
            $this->colPrefix.'alloted_by' => $this->input->post($this->colPrefix.'alloted_by'),
            $this->colPrefix.'status' => 'Enabled',
            $this->colPrefix.'created_by' => $this->session->userdata('admin_id'),
            $this->colPrefix.'updated_by' => $this->session->userdata('admin_id'),
            $this->colPrefix.'added' => date('Y-m-d H:i:s', strtotime('now')),
            $this->colPrefix.'admission_date' => date('Y-m-d H:i:s', strtotime($this->input->post($this->colPrefix.'admission_date'))),
            $this->colPrefix.'discharge_date' => date('Y-m-d H:i:s', strtotime($this->input->post($this->colPrefix.'discharge_date'))),
            $this->colPrefix.'updated' => date('Y-m-d H:i:s', strtotime('now')),
            $this->colPrefix.'is_deleted' => '0',
            $this->colPrefix.'clinic_id' => $this->session->userdata('clinic_id'),
            $this->colPrefix.'payment_status' => 'unpaid'
        );

        

        $q = $this->SqlModel->insertRecord($this->tblName, $data);
        $this->session->unset_userdata($this->controller.'_data');
        if($q!="") {
            $this->session->set_flashdata('alert','success');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        } else {
            $this->session->set_flashdata('alert','error');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        }
    }

    public function save($data = []) {
        $q = $this->SqlModel->insertRecord($this->tblName, $data);
        if($q!="") {
            return $q;
        } else {
            return false;
        }
    }

    public function getRoomByInvoiceId($invoiceId='') {
        $where = ['rp_invoice_id'=>$invoiceId];
        return $this->SqlModel->getRecords('*', $this->tblName, 'rp_id', 'ASC',  $where);
    }
}