<?php

class PatientMedicalConditionModel extends CI_Model
{
    public $tblName = 'patient_medical_condition';
    public $controller = 'patientmedicalcondition';
    public $colPrefix = 'pmc_';
    public function __construct() {
	    // Call the Model constructor
	   	parent::__construct();
    }

    public function save($data=[]) {
        $q = $this->SqlModel->insertRecord($this->tblName, $data);
        $this->session->unset_userdata($this->controller.'_data');
        if($q!="") {
            // $logData = [
            //     'message' => 'New case examination has been created.',
            //     'patient_id' => $data['pce_patient_id']
            // ];
            // $this->PatientLogs->CreateLog($logData);
            return $q;
        } else {
            return false;
        }
    }
}