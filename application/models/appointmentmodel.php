<?php

class AppointmentModel extends CI_Model
{
    public function __construct() {
	    // Call the Model constructor
	   	parent::__construct();
    }

    public function addAppointment($controller='', $tblName='', $colPrefix='') {
    	if($this->input->post($colPrefix.'patient_id')=="") {
            $this->session->set_flashdata('alert','error');
            redirect(base_url().'manage/'.$controller.'/index','location');
            exit();
        }

        $data = array(
            $colPrefix.'patient_id' => $this->input->post($colPrefix.'patient_id'),
            $colPrefix.'status' => $this->input->post($colPrefix.'status'),
            $colPrefix.'is_canceled' => '0',
            $colPrefix.'dtr_id' => $this->input->post($colPrefix.'dtr_id'),
            $colPrefix.'checked' =>'0',
            $colPrefix.'start_time' => $this->input->post($colPrefix.'start_time'),
            $colPrefix.'end_time' => $this->input->post($colPrefix.'end_time'),
            $colPrefix.'date' => date('Y-m-d', strtotime($this->input->post($colPrefix.'date'))),
            $colPrefix.'procedure_id' => $this->input->post($colPrefix.'procedure_id'),
            $colPrefix.'comment' => $this->input->post($colPrefix.'comment'),
            $colPrefix.'send_confirmation' => $this->input->post($colPrefix.'send_confirmation'),
            $colPrefix.'is_deleted' => '0',
            $colPrefix.'added' => date('Y-m-d H:i:s', strtotime('now')),
            $colPrefix.'updated' => date('Y-m-d H:i:s', strtotime('now')),
            $colPrefix.'created_by' => $this->session->userdata('admin_id'),
            $colPrefix.'modified_by' => $this->session->userdata('admin_id'),
            $colPrefix.'clinic_id' => $this->session->userdata('clinic_id')
        );
        $dtr_name = $this->SqlModel->getSingleField('full_name', 'admin_users', ['id'=>$this->input->post($colPrefix.'dtr_id')]);
        
        $q = $this->SqlModel->insertRecord($tblName, $data);
        $this->session->unset_userdata($controller.'_data');
        if($q!="") {
            $this->PatientLogs->CreatePatientAppointmentLog($this->input->post($colPrefix.'patient_id'), $this->input->post($colPrefix.'start_time'), date('Y-m-d', strtotime($this->input->post($colPrefix.'date'))), $dtr_name);
            
            $patient_name = $this->SqlModel->getSingleField('patient_name', 'patients', ['patient_id' => $this->input->post($colPrefix.'patient_id')]);
            // $this->SMSModel->Send("Dear $patient_name, Your new appointment has been scheduled with Dr. $dtr_name on ".(date('d-M-Y', strtotime($this->input->post($colPrefix.'date').' '.$this->input->post($colPrefix.'start_time')))));

            $this->session->set_flashdata('alert','success');
            redirect(base_url().'manage/'.$controller.'/index','location');
        }
        else {
            $this->session->set_flashdata('alert','error');
            redirect(base_url().'manage/'.$controller.'/index','location');
        }
    }
}