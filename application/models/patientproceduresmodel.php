<?php

class PatientProceduresModel extends CI_Model
{
    public $tblName = 'patient_procedures';
    public $controller = 'patientprocedures';
    public $colPrefix = 'pp_';
    public function __construct() {
	    // Call the Model constructor
	   	parent::__construct();
    }

    public function addPP() {
    	if($this->SqlModel->checkPermissions('patientprocedures', 'create')!==true) {
            $this->session->set_flashdata('alert','permerror');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        }
        if($this->input->post($this->colPrefix.'name')=="") {
            $this->session->set_flashdata('alert','error');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
            exit();
        }

        $data = array(
            $this->colPrefix.'clinic_id' => $this->session->userdata('clinic_id'),
            $this->colPrefix.'patient_id' => $this->input->post($this->colPrefix.'patient_id'),
            $this->colPrefix.'procedure_id' => $this->input->post($this->colPrefix.'procedure_id'),
            $this->colPrefix.'tooth_code' => $this->input->post($this->colPrefix.'tooth_code'),
            $this->colPrefix.'date' => date('Y-m-d H:i:s', strtotime('now')),
            $this->colPrefix.'amount' => $this->input->post($this->colPrefix.'amount'),
            $this->colPrefix.'quantity' => addslashes($this->input->post($this->colPrefix.'quantity')),
            $this->colPrefix.'discount' => $this->input->post($this->colPrefix.'discount'),
            $this->colPrefix.'added' => date('Y-m-d H:i:s', strtotime('now')),
            $this->colPrefix.'updated' => date('Y-m-d H:i:s', strtotime('now')),
            $this->colPrefix.'created_by' => $this->session->userdata('admin_id'),
            $this->colPrefix.'updated_by' => $this->session->userdata('admin_id'),
            $this->colPrefix.'is_deleted' => '0',
            $this->colPrefix.'status' => 'Enabled',
            $this->colPrefix.'description' => addslashes($this->input->post($this->colPrefix.'description'))
        );

        $q = $this->SqlModel->insertRecord($this->tblName, $data);
        $this->session->unset_userdata($this->controller.'_data');
        if($q!="") {
            $this->session->set_flashdata('alert','success');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        } else {
            $this->session->set_flashdata('alert','error');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        }
    }

    public function save($data=[]) {
        $q = $this->SqlModel->insertRecord($this->tblName, $data);
        $this->session->unset_userdata($this->controller.'_data');
        if($q!="") {
            return $q;
        } else {
            return false;
        }
    }

    public function getProceduresByInvoiceId($invoiceId='') {
        $where = ['pp_invoice_id'=>$invoiceId];
        return $this->SqlModel->getRecordsWithSingleJoin('patient_procedures.*, procedures.procedure_name', $this->tblName, 'procedures', 'procedure_id', 'pp_procedure_id', 'pp_id', 'ASC',  $where);
    }
}