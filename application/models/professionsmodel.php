<?php

class ProfessionsModel extends CI_Model
{
    public $tblName = 'professions';
    public $colPrefix = 'p_';
    public $controller = 'professions';

    public function __construct() {
	    // Call the Model constructor
	   	parent::__construct();
    }

    public function getProfessionsDropDown($parent_id=0) {
        if ($parent_id == 0) {
            return $this->SqlModel->runQuery("SELECT p_name, p_id FROM professions WHERE p_is_deleted = '0' AND p_status = 'Enabled' AND p_parent_id = '0'");
        } else {
            return $this->SqlModel->runQuery("SELECT p_name, p_id FROM professions WHERE p_is_deleted = '0' AND p_status = 'Enabled' and p_parent_id = '$parent_id'");
        }
    }
}