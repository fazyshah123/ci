<?php

class DoctorTimingModel extends CI_Model
{
    public $tblName = 'doctor_timing';
    public $colPrefix = 'dt_';
    public $controller = 'doctortiming';

    public function __construct() {
	    // Call the Model constructor
	   	parent::__construct();
    }

    public function addDoctorTiming() {
    	if($this->SqlModel->checkPermissions('patients', 'create')!==true) {
            $this->session->set_flashdata('alert','permerror');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        }
        if($this->input->post($this->colPrefix.'name')=="") {
            $this->session->set_flashdata('alert','error');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
            exit();
        }

        $data = array(
            $this->colPrefix.'day_id' => $this->input->post($this->colPrefix.'day_id'),
            $this->colPrefix.'doctor_id' => $this->input->post($this->colPrefix.'doctor_id'),
            $this->colPrefix.'status' => 'Enabled',
            $this->colPrefix.'created_by' => $this->session->userdata('admin_id'),
            $this->colPrefix.'updated_by' => $this->session->userdata('admin_id'),
            $this->colPrefix.'start_time' => $this->input->post($this->colPrefix.'start_time'),
            $this->colPrefix.'end_time' => $this->input->post($this->colPrefix.'end_time'),
            $this->colPrefix.'added' => date('Y-m-d H:i:s', strtotime('now')),
            $this->colPrefix.'updated' => date('Y-m-d H:i:s', strtotime('now')),
            $this->colPrefix.'is_deleted' => '0',
            $this->colPrefix.'clinic_id' => $this->session->userdata('clinic_id')
        );

        $q = $this->SqlModel->insertRecord($this->tblName, $data);
        $this->session->unset_userdata($this->controller.'_data');
        if($q!="") {
            $this->PatientLogs->CreatePatientAddedLog($q);
            $this->session->set_flashdata('alert','success');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        }
        else {
            $this->session->set_flashdata('alert','error');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        }
    }

    public function save($data) {
        $q = $this->SqlModel->insertRecord($this->tblName, $data);
        $this->session->unset_userdata($this->controller.'_data');
        if($q!="") {
            //$this->PatientLogs->CreatePatientAddedLog($q);
            return $q;
        }
        else {
            return false;
        }
    }

    public function update($data, $where) {
        $q = $this->SqlModel->updateRecord($this->tblName, $data, $where);
        $this->session->unset_userdata($this->controller.'_data');
        if($q!="") {
            //$this->PatientLogs->CreatePatientAddedLog($q);
            return $q;
        }
        else {
            return false;
        }
    }
}