<?php

class PatientVisitLogModel extends CI_Model
{
    public $tblName = 'patient_visit_log';
    public $controller = 'patientvisitlog';
    public $colPrefix = 'pvl_';
    public function __construct() {
	    // Call the Model constructor
	   	parent::__construct();
    }

    public function save($data=[]) {
        $q = $this->SqlModel->insertRecord($this->tblName, $data);
        $this->session->unset_userdata($this->controller.'_data');
        if($q!="") {
            $dtr = $this->SqlModel->getSingleField('full_name', 'admin_users', ['id'=>$data['pvl_dtr_id']]);
            $logData = [
                'message' => $dtr . ' visited patient at '. date("d-m-Y h:i A", strtotime($data[$this->colPrefix.'datetime'])),
                'patient_id' => $data['pvl_patient_id']
            ];
            if ($data[$this->colPrefix.'note'] != '') {
                $logData['message'] .= '   <blockquote>'.$data[$this->colPrefix.'note'].'</blockquote>';
            }
            $this->PatientLogs->CreateLog($logData);
            return $q;
        } else {
            return false;
        }
    }

    public function getPatientVisitLog($last_visit_log_id='0', $patient_id='', $case_id='') {
        $sql = "SELECT pvl_datetime, pvl_id FROM patient_visit_log WHERE pvl_patient_id = '$patient_id' AND pvl_is_deleted = '0' AND pvl_case_id = '$case_id' AND pvl_clinic_id = '".$this->session->userdata('clinic_id')."' order by pvl_datetime desc limit 10";

        return $this->SqlModel->runQuery($sql);
    }

    public function getPatientTotalVisits() {
        $sql = "SELECT count(pvl_id) total_visits FROM patient_visit_log WHERE pvl_patient_id = '$patient_id' AND pvl_is_deleted = '0' AND pvl_case_id = '$case_id' AND pvl_clinic_id = '".$this->session->userdata('clinic_id')."'";

        return $this->SqlModel->runQuery($sql);
    }
}