<?php

class JobLevelModel extends CI_Model
{
    public $tblName = 'job_level';
    public $colPrefix = 'jl_';
    public $controller = 'job_level';

    public function __construct() {
	    // Call the Model constructor
	   	parent::__construct();
    }

    public function getJobLevelDropDown() {
        return $this->SqlModel->runQuery("SELECT jl_name, jl_id FROM job_level WHERE jl_is_deleted = '0' AND jl_status = 'Enabled'");
    }
}