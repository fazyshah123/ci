<?php

class EmailModel extends CI_Model {
	public $mail;
	public function __construct() {
        parent::__construct();

		$this->load->library('PHPMailer/phpmailer');
		$this->emailHost = EMAIL_HOST;
		$this->sendFrom = EMAIL_SENDER_NAME;
		$this->sendFromEmail = EMAIL_ADDRESS;
    }


    public function init() {
        $this->mail = new PHPMailer();
        $this->mail->IsSMTP();
        $this->mail->Host = 'smtp.gmail.com';
        $this->mail->SMTPSecure = "ssl";
        $this->mail->Port = 465;
        $this->mail->SMTPAuth = true;
    }


    public function infoEmail($params = []) {
        $this->init();
        $this->mail->Username = "taliballauddin@gmail.com";
        $this->mail->Password = "??T@lZopallibIM09??";
        $this->mail->FromName = "USA Digital Art Internationl";
        $this->ingredients($params);
        if ($this->mail->Send()) {
            return true;
        } else {
            return 'Error sending: ' . $this->mail->ErrorInfo;
        }
    }


    public function ingredients($params = []) {
        $this->mail->SetFrom($params['from_email'], $params['from_name']);
        $this->mail->AddAddress($params['to_email']);

        if (isset($params['cc'])) {
            foreach ($params['cc'] as $key => $value) {
                $this->mail->AddCC($value);
            }
        }

        if (isset($params['reply_to'])) {
            $this->mail->AddReplyTo($params['reply_to']);
        }

        if (isset($params['bcc'])) {
            foreach ($params['bcc'] as $key => $value) {
                $this->mail->AddBCC($value);
            }
        }

        $this->mail->IsHTML($params['isHTML']);
        $this->mail->Subject = $params['subject'];
        $this->mail->Body = $params['body'];
        $this->mail->AltBody = $params['body'];
        $this->mail->WordWrap = 100;
        if (isset($params['files'])) {
            foreach ($params['files'] as $key => $value) {
                $this->mail->AddAttachment($value);
            }
        }
    }

    /*
        For Sender vs From : http://stackoverflow.com/questions/4728393/
        should-i-use-the-reply-to-header-when-sending-emails-as-a-service-to-others
        Add custom reply to as well.
    */
    private function getHeader($email_from) {
        return 'From: '. $email_from . "\r\n" .
                            'Reply-To: support@digitemb.com'.
                            'X-Mailer: PHP/' . phpversion() . "\r\n" .
                            'MIME-Version: 1.0' . "\r\n".
                            'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    }


	public function sendEmail($name, $to, $subject, $body, $attach="") {
		$mail             = new PHPMailer();
		$mail->SMTPDebug  = 0;  // enables SMTP debug information (for testing)
								// 1 = errors and messages
								// 2 = messages only

		$mail->Host       = $this->emailHost; // sets GMAIL as the SMTP server
		$mail->SetFrom($this->sendFromEmail, $this->sendFrom);
		$mail->AddReplyTo($this->sendFromEmail, $this->sendFrom);
		$mail->Subject    = $subject;
		$mail->MsgHTML($body);
		$mail->AddAddress($to, $name);

		if($attach!="") {
			$mail->AddAttachment($attach);      // attachment
		}

		if(!$mail->Send()) {
			return "Error";
		}
		else {
			return "Sent";
		}
	}


    /*public function sendEmailLocal($name, $to, $subject, $body, $attach=[], $cc='') {
 		$mail             = new PHPMailer();
   		$mail->IsSMTP(); // telling the class to use SMTP
		$mail->SMTPDebug  = 1; // enables SMTP debug information (for testing)
							   // 1 = errors and messages
							   // 2 = messages only

		$mail->SMTPAuth   = true;            // enable SMTP authentication
		$mail->SMTPSecure = "ssl";           // sets the prefix to the servier
		$mail->Host       = "smtp.gmail.com";// sets GMAIL as the SMTP server
		$mail->Port       = 465;             // set the SMTP port for the GMAIL server
		$mail->Username   = "taliballauddin@gmail.com";  // GMAIL username
		$mail->Password   = "yjswalapass09??";            // GMAIL password
   		$mail->SetFrom("taliballauddin@gmail.com", "Talib Mail");
		$mail->AddReplyTo("taliballauddin@gmail.com", "Talib Mail");
		$mail->AddReplyTo("taliballauddin@gmail.com", "Talib Mail");
		$mail->Subject    = $subject;
		$mail->MsgHTML($body);
		$mail->AddAddress($to, $name);

		if(!empty($attach)) {
			foreach ($attach as $key => $value) {
				$mail->AddAttachment($value);
			}
		}

		if(!$mail->Send()) {
			return false;
		}
		else {
			return true;
		}
	}*/
}