<?php
$crumb2 = "";
if(isset($tbl_data['e_id'])&&$tbl_data['e_id']!=""){
	$e_id = $tbl_data['e_id'];
	$e_clinic_id = $tbl_data['e_clinic_id'];
	$e_account_id = $tbl_data['e_account_id'];
	$e_date = $tbl_data['e_date'];
	$e_description = $tbl_data['e_description'];
	$e_amount = $tbl_data['e_amount'];
	$e_added = $tbl_data['e_added'];
	$e_updated = $tbl_data['e_updated'];
	$e_created_by = $tbl_data['e_created_by'];
	$e_updated_by = $tbl_data['e_updated_by'];
	$e_is_deleted = $tbl_data['e_is_deleted'];
	$e_status = $tbl_data['e_status'];
    $crumb = "Edit";
    $action = "editRecord/".$tbl_data['e_id'];
}
else{
	$e_id = '';
	$e_clinic_id = '';
	$e_account_id = '';
	$e_date = '';
	$e_description = '';
	$e_amount = '';
	$e_added = '';
	$e_updated = '';
	$e_created_by = '';
	$e_updated_by = '';
	$e_is_deleted = '';
	$e_status = '';
    $on_home 		= "No";
    $crumb 			= "Add";
    $action 		= "addRecord";
}
?>
<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo ADMIN_URL;?>">
            <i class="entypo-home"></i>Home
        </a>
    </li>
    <li>
        <a href="<?php echo ADMIN_URL.$this->controller;?>">
            <i></i><?php echo $this->moduleName;?>
        </a>
    </li>
    <li class="active">
        <strong><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></strong>
    </li>
</ol>

<h2><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></h2>
<br />
<div class="panel panel-primary">
    <div class="panel-body">
        <form  id="page_form"
               name="page_form"
               method="post"
               action="<?php echo base_url('manage/'.$this->controller.'/'.$action);?>"
               enctype="multipart/form-data"
               class="validate"
        >

            <div class="form-group">
                <label class="control-label">Account <span class="req">*</span> :</label>
                <select required class="form-control"  name="e_account_id" id="e_account_id">
                    <option value="select1">Select</option>
                <?php
                $accounts = $this->SqlModel->getAccountsDropDown();
                foreach ($accounts as $key => $value) {
                    echo '<option value="'.$value['a_id'].'" '.($value['a_id'] == $e_account_id ? 'selected' : '').'>'.$value["a_name"].'</option>';
                }
                ?>
                </select>
            </div>

            <div class="form-group">
                <label class="control-label">Amount <span class="req">*</span> :</label>
                <input type="number" name="e_amount" id="e_amount" value="<?php echo $e_amount;?>" class="form-control " placeholder="Amount" data-validate="required,maxlength[250]"/>
            </div>

            <div class="form-group" style="height: 53px;">
                <label class="control-label">Date <span class="req">*</span> :</label>
                <div class="date-and-time">
                    <input style="width: 100%;float: left;" required type="text" name="e_date" id="e_date" value="<?php echo $e_date;?>" autocomplete="off" class="form-control datepicker"  data-format="dd MM yyyy" placeholder="Date"/>
                </div>
            </div>


            <div class="form-group" >
                <label class="control-label">Description :</label>
				<?php echo $this->ckeditor->editor("e_description", html_entity_decode($e_description)); ?>
			</div>

            <div class="form-group hidden">
                <label class="control-label">Status :</label>
                <select class="form-control"  name="e_status" id="e_status">
                    <option value="Enable" <?php if($e_status=="Enable"){ echo ' selected="selected"';} ?>>Enable</option>
                    <option value="Disable" <?php if($e_status=="Disable"){ echo ' selected="selected"';} ?>>Disable</option>
                  </select>
            </div>



            <div class="form-group">
                <button type="button" class="btn btn-danger" onclick="window.location='<?php echo ADMIN_URL;?>expenses'">Cancel</button>
                <button type="submit" id="expsubmit"  class="btn btn-success">Submit</button>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    document.addEventListener("DOMContentLoaded", function(event) {
        $("#expsubmit").on('click', function(e) {
            e.preventDefault();
            var e_account = $("#e_account_id").val();

            if (e_account == 'select1') {
                alert('Please select an Account');
                return;
            }
            $("#page_form").submit();

        });
    });
</script>