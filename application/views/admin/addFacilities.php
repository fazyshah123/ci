<?php
$crumb2 = "";
if(isset($tbl_data['facility_id'])&&$tbl_data['facility_id']!=""){
	$facility_id = $tbl_data['facility_id'];
	$facility_name = $tbl_data['facility_name'];
	$facility_is_deleted = $tbl_data['facility_is_deleted'];
	$facility_status = $tbl_data['facility_status'];
	$facility_created = $tbl_data['facility_created'];
	$facility_updated = $tbl_data['facility_updated'];
	$facility_updated_by = $tbl_data['facility_updated_by'];
	$facility_created_by = $tbl_data['facility_created_by'];
    $crumb = "Edit";
    $action = "editRecord/".$tbl_data['facility_id'];
}
else{
	$facility_id = '';
	$facility_name = '';
	$facility_is_deleted = '';
	$facility_status = '';
	$facility_created = '';
	$facility_updated = '';
	$facility_updated_by = '';
	$facility_created_by = '';
    $on_home 		= "No";
    $crumb 			= "Add";
    $action 		= "addRecord";
}
?>
<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo ADMIN_URL;?>">
            <i class="entypo-home"></i>Home
        </a>
    </li>
    <li>
        <a href="<?php echo ADMIN_URL.$this->controller;?>">
            <i></i><?php echo $this->moduleName;?>
        </a>
    </li>
    <li class="active">
        <strong><?php echo $crumb;?> Facility</strong>
    </li>
</ol>

<h2><?php echo $crumb;?> Facility</h2>
<br />
<div class="panel panel-primary">
    <div class="panel-body">
        <form  id="page_form"
               name="page_form"
               method="post"
               action="<?php echo base_url('manage/'.$this->controller.'/'.$action);?>"
               enctype="multipart/form-data"
               class="validate"
        >

            <div class="form-group">
                <label class="control-label">Name :<span style="color: red"> *</span> </label>
                <input type="text" name="facility_name" id="facility_name" value="<?php echo $facility_name;?>" class="form-control " placeholder="Name" data-validate="required,maxlength[250]"/>
            </div>
            
            <div class="form-group">
                <label class="control-label">Status :</label>
                
                  <select class="form-control"  name="facility_status" id="facility_status">
                    <option selected value="Enable" <?php if($facility_status=="Enable"){ echo ' selected="selected"';} ?>>Enable</option>
                    <option value="Disable" <?php if($facility_status=="Disable"){ echo ' selected="selected"';} ?>>Disable</option>
                  </select>
            </div>

            <div class="form-group">
                <button type="button" class="btn btn-danger" onclick="window.location='<?php echo ADMIN_URL;?>facilities'">Cancel</button>
                <button id="facilsubmit" type="submit"  class="btn btn-success">Submit</button>
            </div>
        </form>
    </div>
</div>