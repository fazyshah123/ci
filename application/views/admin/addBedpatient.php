<?php
$crumb2 = "";
if(isset($tbl_data['bp_id'])&&$tbl_data['bp_id']!=""){
	$bp_id = $tbl_data['bp_id'];
	$bp_bed_id = $tbl_data['bp_bed_id'];
	$bp_patient_id = $tbl_data['bp_patient_id'];
	$bp_alloted_by = $tbl_data['bp_alloted_by'];
	$bp_status = $tbl_data['bp_status'];
	$bp_created_by = $tbl_data['bp_created_by'];
	$bp_updated_by = $tbl_data['bp_updated_by'];
	$bp_added = $tbl_data['bp_added'];
	$bp_admission_date = $tbl_data['bp_admission_date'];
	$bp_discharge_date = $tbl_data['bp_discharge_date'];
	$bp_updated = $tbl_data['bp_updated'];
	$bp_is_deleted = $tbl_data['bp_is_deleted'];
	$bp_clinic_id = $tbl_data['bp_clinic_id'];
    $bp_payment_status = $tbl_data['bp_payment_status'];
    $crumb = "Edit";
    $action = "editRecord/".$tbl_data['bp_id'];
}
else{
	$bp_id = '';
	$bp_bed_id = '';
	$bp_patient_id = $this->input->get('patient_id');
	$bp_alloted_by = '';
	$bp_status = '';
	$bp_created_by = '';
	$bp_updated_by = '';
	$bp_added = '';
    $bp_payment_status = ''; 
	$bp_admission_date = date('d F Y', strtotime('now'));
	$bp_discharge_date = '';
	$bp_updated = '';
	$bp_is_deleted = '';
	$bp_clinic_id = '';
    $on_home 		= "No";
    $crumb 			= "Add";
    $action 		= "addRecord";
}
?>
<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo ADMIN_URL;?>">
            <i class="entypo-home"></i>Home
        </a>
    </li>
    <li>
        <a href="<?php echo ADMIN_URL.$this->controller;?>">
            <i></i><?php echo $this->moduleName;?>
        </a>
    </li>
    <li class="active">
        <strong><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></strong>
    </li>
</ol>

<h2><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></h2>
<br />
<div class="panel panel-primary">
    <div class="panel-body">
        <form  id="page_form"
               name="page_form"
               method="post"
               action="<?php echo base_url('manage/'.$this->controller.'/'.$action);?>"
               enctype="multipart/form-data"
               class="validate"
        >
            <div class="form-group">
                <label class="control-label">Patient :<span class="req"> *</span></label>
                <select class="form-control patientsDropdown" name="bp_patient_id" id="bp_patient_id">
                    <option value="Select">Select</option>
                <?php
                $patients = $this->SqlModel->getPatientsDropDown();
                foreach ($patients as $key => $value) {
                    if($value['patient_id']==$bp_patient_id) { 
                        $selected = ' selected="selected" ';
                    } else {
                        $selected = '';
                    }
                    echo '<option '.$selected.' value="'.$value['patient_id'].'">'.$value["patient_name"].'</option>';
                }
                ?>
                </select>
            </div>

            <div class="form-group">
                <label class="control-label">Bed :<span class="req"> *</span></label>
                <?php
                    if ($crumb == 'Edit') {
                        $disabled = 'disabled';
                    } else {
                        $disabled = "";
                    }
                ?>
                <select class="form-control bedsDropdown" name="bp_bed_id" <?=$disabled;?> id="bp_bed_id">
                    <option value="Select">Select</option>
                <?php
                $patients = $this->SqlModel->getBedDropDown();
                foreach ($patients as $key => $value) {
                    $selected = '';
                    if($value['b_id']==$bp_bed_id) { 
                        $selected = 'selected';
                    }
                    echo '<option '.$selected.' value="'.$value['b_id'].'">'.$value["b_number"].'</option>';
                }
                ?>
                </select>
            </div>

            <div class="form-group">
                <label class="control-label">Alloted By :<span class="req"> *</span></label>
                <select class="form-control check_in_user"  name="bp_alloted_by" id="bp_alloted_by">
                    <option value="Select">Select</option>
                <?php
                $users = $this->SqlModel->getAdminUsersDropDown();
                foreach ($users as $key => $value) {
                    if($value['id']==$bp_alloted_by) { 
                        $selected = ' selected="selected"';
                    } else {
                        $selected = '';
                    }
                    echo '<option '.$selected.' value="'.$value['id'].'">'.$value["full_name"].'</option>';
                }
                ?>
                </select>
            </div>

            <div class="form-group hidden">
                <label class="control-label">Status :</label>
                <select class="form-control" name="bp_status" id="bp_status">
                    <option value="Enable" <?php if($bp_status=="Enable"){ echo ' selected="selected"';} ?>>Enable</option>
                    <option value="Disable" <?php if($bp_status=="Disable"){ echo ' selected="selected"';} ?>>Disable</option>
                </select>
            </div>

            <div class="form-group" style="height: 53px;">
                <label class="control-label">Admission Date :<span class="req"> *</span></label>
                <div class="date-and-time">
                    <input style="width: 100%;float: left;" required type="text" name="bp_admission_date" id="bp_admission_date" value="<?php echo $bp_admission_date;?>" autocomplete="off" class="form-control datepicker"  data-format="dd MM yyyy" placeholder="Admission Date"/>
                </div>
            </div>

            <div class="form-group" style="height: 53px;">
                <label class="control-label">Discharge Date :</label>
                <div class="date-and-time">
                    <input style="width: 100%;float: left;" type="text" name="bp_discharge_date" id="bp_discharge_date" value="<?php echo $bp_discharge_date;?>" autocomplete="off" class="form-control datepicker"  data-format="dd MM yyyy" placeholder="Discharge Date"/>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label">Payment Status :</label>
                <select class="form-control" name="bp_payment_status" id="bp_payment_status">
                    <option value="Unpaid" <?php if($bp_payment_status=="Unpaid"){ echo ' selected="selected"';} ?>>Unpaid</option>
                    <option value="Paid" <?php if($bp_payment_status=="Paid"){ echo ' selected="selected"';} ?>>Paid</option>
                </select>
            </div>

            <div class="form-group">
                <button type="button" class="btn btn-danger" onclick="window.location='<?php echo ADMIN_URL;?>bedpatient'">Cancel</button>
                <button type="submit" id="bedpasubmit" class="btn btn-success">Submit</button>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    var bp_patient_id = '<?=$bp_patient_id;?>';
    document.addEventListener("DOMContentLoaded", function(event) {
        if (bp_patient_id != '') {
            $("#bp_patient_id").prop('disabled', true);
        }

        $("#bedpasubmit").on('click', function(e) {
            e.preventDefault();

            var bp_patientid = $("#bp_patient_id").val();
            
            if (bp_patientid == 'Select') {
                alert('Please select a Patient');
                return;
            }
            var bp_bed_id = $("#bp_bed_id").val();
            if (bp_bed_id == 'Select') {
                alert('Please enter select a Bed');
                return;
            }
            var bp_allotedby = $("#bp_alloted_by").val();
            if (bp_allotedby == 'Select') {
                alert('Please select a Alloted By');
                return;
            }
            $("#page_form").submit();

        });
    });
</script>