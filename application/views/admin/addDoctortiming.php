<?php
$crumb2 = "";
if(isset($tbl_data['dt_id'])&&$tbl_data['dt_id']!=""){
	$dt_id = $tbl_data['dt_id'];
	$dt_day_id = $tbl_data['dt_day_id'];
	$dt_doctor_id = $tbl_data['dt_doctor_id'];
	$dt_status = $tbl_data['dt_status'];
	$dt_created_by = $tbl_data['dt_created_by'];
	$dt_updated_by = $tbl_data['dt_updated_by'];
    $day_status = $tbl_data['day_status'];
	$dt_start_time = $tbl_data['dt_start_time'];
	$dt_end_time = $tbl_data['dt_end_time'];
	$dt_added = $tbl_data['dt_added'];
	$dt_updated = $tbl_data['dt_updated'];
	$dt_is_deleted = $tbl_data['dt_is_deleted'];
	$dt_clinic_id = $tbl_data['dt_clinic_id'];
    $crumb = "Edit";
    $action = "editRecord/".$tbl_data['dt_id'];
}
else{
	$dt_id = '';
	$dt_day_id = '';
	$dt_doctor_id = $this->input->get('uid');
	$dt_status = '';
	$dt_created_by = '';
	$dt_updated_by = '';
    $day_status = '';
	$dt_start_time = '';
	$dt_end_time = '';
	$dt_added = '';
	$dt_updated = '';
	$dt_is_deleted = '';
	$dt_clinic_id = '';
    $on_home 		= "No";
    $crumb 			= "Add";
    $action 		= "addRecord";
}
?>
<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo ADMIN_URL;?>">
            <i class="entypo-home"></i>Home
        </a>
    </li>
    <li>
        <a href="<?php echo ADMIN_URL.$this->controller;?>">
            <i></i><?php echo $this->moduleName;?>
        </a>
    </li>
    <li class="active">
        <strong><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></strong>
    </li>
</ol>

<h2><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></h2>
<br />
<div class="panel panel-primary">
    <div class="panel-body">
        <form  id="page_form"
               name="page_form"
               method="post"
               action="<?php echo base_url('manage/doctortiming/addDoctorTiming');?>"
               enctype="multipart/form-data"
               class="validate"
        >
            <div class="form-group">
                <label class="control-label">Doctor :</label>
                <select class="form-control doctorsDropdown"  name="dt_doctor_id" id="dt_doctor_id">
                    <option>Select</option>
                <?php
                $doctors = $this->SqlModel->getDoctorsDropDown();
                $uid = $this->input->get('uid');
                foreach ($doctors as $key => $value) {
                    if($value['id']==$dt_doctor_id) { 
                        $selected = ' selected="selected"';
                    } else {
                        $selected = '';
                    }
                    echo '<option '.$selected.' value="'.$value['id'].'">'.$value["full_name"].'</option>';
                }
                ?>
                </select>
            </div>

            <div class="row">
                <div class="col-md-2" style="padding: 15px;"><h4><strong>Monday</strong></h4></div>
                <div class="col-md-2">
                    <div class="form-group" style="padding-top: 15px;">
                        <select class="dayStatus form-control"  name="monday_status" id="monday_status" data-day="monday">
                            <option value="Not Available">Not Available</option>
                            <option <?=($tbl_data[0]['dt_day_id'] == '1') ? 'selected' : '';?> value="Available">Available</option>
                        </select>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="mondayTimings <?=($tbl_data[0]['dt_day_id'] == '1') ? '' : 'hidden';?>">
                    <div class="row" style="padding: 0px 15px;">
                        <div class="form-group col-md-5 col-xs-12 col-lg-4">
                            <label class="control-label">Start Time :</label>
                            <div class="date-and-time">
                                <input style="width: 100%;float: left;" id="mondayStartTime" data-default-time="<?=$tbl_data[0]['dt_start_time'];?>" name="mondayStartTime" type="text" autocomplete="off" class="form-control timepicker" data-template="dropdown"" data-show-meridian="true" data-minute-step="1" data-second-step="1" placeholder="Start Time"/>
                            </div>
                        </div>

                        <div class="form-group col-md-5 col-xs-12 col-lg-4">
                            <label class="control-label">End Time :</label>
                            <div class="date-and-time">
                                <input style="width: 100%;float: left;" id="mondayEndTime" data-default-time="<?=$tbl_data[0]['dt_end_time'];?>" name="mondayEndTime" type="text" autocomplete="off" class="form-control timepicker" data-template="dropdown" data-show-meridian="true" data-minute-step="1" data-second-step="1" placeholder="End Time"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <?php 
                    if (count($tbl_data) > 1) {
                        $day2 = ($tbl_data[0]['dt_day_id'] == '2' && $tbl_data[0]['dt_status'] == 'Available') || ($tbl_data[1]['dt_day_id'] == '2' && $tbl_data[1]['dt_status'] == 'Available');
                    } else {
                        $day2 = '';
                    }
                ?>
                <div class="col-md-2" style="padding: 15px;"><h4><strong>Tuesday</strong></h4></div>
                <div class="col-md-2">
                    <div class="form-group" style="padding-top: 15px;">
                        <select class="dayStatus form-control"  name="tuesday_status" id="tuesday_status" data-day="tuesday">
                            <option value="Not Available">Not Available</option>
                            <option <?=($day2) ? 'selected' : '';?> value="Available">Available</option>
                        </select>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="tuesdayTimings <?=($day2) ? '' : 'hidden';?>">
                    <div class="row" style="padding: 0px 15px;">
                        <div class="form-group col-md-5 col-xs-12 col-lg-4">
                            <label class="control-label">Start Time :</label>
                            <div class="date-and-time">
                                <input style="width: 100%;float: left;" id="tuesdayStartTime" name="tuesdayStartTime" type="text" autocomplete="off" class="form-control timepicker" data-template="dropdown"" data-show-meridian="true" data-minute-step="1" data-second-step="1" placeholder="Start Time"/>
                            </div>
                        </div>

                        <div class="form-group col-md-5 col-xs-12 col-lg-4">
                            <label class="control-label">End Time :</label>
                            <div class="date-and-time">
                                <input style="width: 100%;float: left;" id="tuesdayEndTime" name="tuesdayEndTime" type="text" autocomplete="off" class="form-control timepicker" data-template="dropdown" data-show-meridian="true" data-minute-step="1" data-second-step="1" placeholder="End Time"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <?php 
                    if (count($tbl_data) > 2) {
                        $day3 = ($tbl_data[0]['dt_day_id'] == '3' && $tbl_data[0]['dt_status'] == 'Available') || ($tbl_data[1]['dt_day_id'] == '3' && $tbl_data[1]['dt_status'] == 'Available') || ($tbl_data[2]['dt_day_id'] == '3' && $tbl_data[2]['dt_status'] == 'Available');
                    } else {
                        $day3 = '';
                    }
                ?>
                <div class="col-md-2" style="padding: 15px;"><h4><strong>Wednesday</strong></h4></div>
                <div class="col-md-2">
                    <div class="form-group" style="padding-top: 15px;">
                        <select class="dayStatus form-control"  name="wednesday_status" id="wednesday_status" data-day="wednesday">
                            <option value="Not Available">Not Available</option>
                            <option <?=($day3) ? 'selected' : '';?> value="Available">Available</option>
                        </select>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="wednesdayTimings <?=($day3) ? '' : 'hidden';?>"">
                    <div class="row" style="padding: 0px 15px;">
                        <div class="form-group col-md-5 col-xs-12 col-lg-4">
                            <label class="control-label">Start Time :</label>
                            <div class="date-and-time">
                                <input style="width: 100%;float: left;" id="wednesdayStartTime" name="wednesdayStartTime" type="text" autocomplete="off" class="form-control timepicker" data-template="dropdown"" data-show-meridian="true" data-minute-step="1" data-second-step="1" placeholder="Start Time"/>
                            </div>
                        </div>

                        <div class="form-group col-md-5 col-xs-12 col-lg-4">
                            <label class="control-label">End Time :</label>
                            <div class="date-and-time">
                                <input style="width: 100%;float: left;" id="wednesdayEndTime" name="wednesdayEndTime" type="text" autocomplete="off" class="form-control timepicker" data-template="dropdown" data-show-meridian="true" data-minute-step="1" data-second-step="1" placeholder="End Time"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <?php 
                    if (count($tbl_data) > 3) {
                        $day4 = ($tbl_data[0]['dt_day_id'] == '4' && $tbl_data[0]['dt_status'] == 'Available') || ($tbl_data[1]['dt_day_id'] == '4' && $tbl_data[1]['dt_status'] == 'Available') || ($tbl_data[2]['dt_day_id'] == '4' && $tbl_data[2]['dt_status'] == 'Available') || ($tbl_data[3]['dt_day_id'] == '4' && $tbl_data[3]['dt_status'] == 'Available');
                    } else {
                        $day4 = '';
                    }
                ?>
                <div class="col-md-2" style="padding: 15px;"><h4><strong>Thursday</strong></h4></div>
                <div class="col-md-2">
                    <div class="form-group" style="padding-top: 15px;">
                        <select class="dayStatus form-control"  name="thursday_status" id="thursday_status" data-day="thursday">
                            <option value="Not Available">Not Available</option>
                            <option <?=($day4) ? 'selected' : '';?> value="Available">Available</option>
                        </select>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="thursdayTimings <?=($day4) ? '' : 'hidden';?>"">
                    <div class="row" style="padding: 0px 15px;">
                        <div class="form-group col-md-5 col-xs-12 col-lg-4">
                            <label class="control-label">Start Time :</label>
                            <div class="date-and-time">
                                <input style="width: 100%;float: left;" id="thursdayStartTime" name="thursdayStartTime" type="text" autocomplete="off" class="form-control timepicker" data-template="dropdown"" data-show-meridian="true" data-minute-step="1" data-second-step="1" placeholder="Start Time"/>
                            </div>
                        </div>

                        <div class="form-group col-md-5 col-xs-12 col-lg-4">
                            <label class="control-label">End Time :</label>
                            <div class="date-and-time">
                                <input style="width: 100%;float: left;" id="thursdayEndTime" name="thursdayEndTime" type="text" autocomplete="off" class="form-control timepicker" data-template="dropdown" data-show-meridian="true" data-minute-step="1" data-second-step="1" placeholder="End Time"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <?php 
                    if (count($tbl_data) > 4) {
                        $day5 = ($tbl_data[0]['dt_day_id'] == '5' && $tbl_data[0]['dt_status'] == 'Available') || ($tbl_data[1]['dt_day_id'] == '5' && $tbl_data[1]['dt_status'] == 'Available') || ($tbl_data[2]['dt_day_id'] == '5' && $tbl_data[2]['dt_status'] == 'Available') || ($tbl_data[3]['dt_day_id'] == '5' && $tbl_data[3]['dt_status'] == 'Available') || ($tbl_data[4]['dt_day_id'] == '5' && $tbl_data[4]['dt_status'] == 'Available');
                    } else {
                        $day5 = '';
                    }
                ?>
                <div class="col-md-2" style="padding: 15px;"><h4><strong>Friday</strong></h4></div>
                <div class="col-md-2">
                    <div class="form-group" style="padding-top: 15px;">
                        <select class="dayStatus form-control"  name="friday_status" id="friday_status" data-day="friday">
                            <option value="Not Available">Not Available</option>
                            <option <?=($day5) ? 'selected' : '';?> value="Available">Available</option>
                        </select>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="fridayTimings <?=($day5) ? '' : 'hidden';?>"">
                    <div class="row" style="padding: 0px 15px;">
                        <div class="form-group col-md-5 col-xs-12 col-lg-4">
                            <label class="control-label">Start Time :</label>
                            <div class="date-and-time">
                                <input style="width: 100%;float: left;" id="fridayStartTime" name="fridayStartTime" type="text" autocomplete="off" class="form-control timepicker" data-template="dropdown"" data-show-meridian="true" data-minute-step="1" data-second-step="1" placeholder="Start Time"/>
                            </div>
                        </div>

                        <div class="form-group col-md-5 col-xs-12 col-lg-4">
                            <label class="control-label">End Time :</label>
                            <div class="date-and-time">
                                <input style="width: 100%;float: left;" id="fridayEndTime" name="fridayEndTime" type="text" autocomplete="off" class="form-control timepicker" data-template="dropdown" data-show-meridian="true" data-minute-step="1" data-second-step="1" placeholder="End Time"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <?php 
                    if (count($tbl_data) > 5) {
                        $day6 = ($tbl_data[0]['dt_day_id'] == '6' && $tbl_data[0]['dt_status'] == 'Available') || ($tbl_data[1]['dt_day_id'] == '6' && $tbl_data[1]['dt_status'] == 'Available') || ($tbl_data[2]['dt_day_id'] == '6' && $tbl_data[2]['dt_status'] == 'Available') || ($tbl_data[3]['dt_day_id'] == '6' && $tbl_data[3]['dt_status'] == 'Available') || ($tbl_data[4]['dt_day_id'] == '6' && $tbl_data[4]['dt_status'] == 'Available') || ($tbl_data[5]['dt_day_id'] == '6' && $tbl_data[5]['dt_status'] == 'Available');
                    } else {
                        $day6 = '';
                    }
                ?>
                <div class="col-md-2" style="padding: 15px;"><h4><strong>Saturday</strong></h4></div>
                <div class="col-md-2">
                    <div class="form-group" style="padding-top: 15px;">
                        <select class="dayStatus form-control"  name="saturday_status" id="saturday_status" data-day="saturday">
                            <option value="Not Available">Not Available</option>
                            <option <?=($day6) ? 'selected' : '';?> value="Available">Available</option>
                        </select>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="saturdayTimings <?=($day6) ? '' : 'hidden';?>"">
                    <div class="row" style="padding: 0px 15px;">
                        <div class="form-group col-md-5 col-xs-12 col-lg-4">
                            <label class="control-label">Start Time :</label>
                            <div class="date-and-time">
                                <input style="width: 100%;float: left;" id="saturdayStartTime" name="saturdayStartTime" type="text" autocomplete="off" class="form-control timepicker" data-template="dropdown"" data-show-meridian="true" data-minute-step="1" data-second-step="1" placeholder="Start Time"/>
                            </div>
                        </div>

                        <div class="form-group col-md-5 col-xs-12 col-lg-4">
                            <label class="control-label">End Time :</label>
                            <div class="date-and-time">
                                <input style="width: 100%;float: left;" id="saturdayEndTime" name="saturdayEndTime" type="text" autocomplete="off" class="form-control timepicker" data-template="dropdown" data-show-meridian="true" data-minute-step="1" data-second-step="1" placeholder="End Time"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <?php 
                    if (count($tbl_data) > 6) {
                        $day7 = ($tbl_data[4]['dt_day_id'] == '7' && $tbl_data[0]['dt_status'] == 'Available') || ($tbl_data[1]['dt_day_id'] == '7' && $tbl_data[1]['dt_status'] == 'Available') || ($tbl_data[2]['dt_day_id'] == '7' && $tbl_data[2]['dt_status'] == 'Available') || ($tbl_data[3]['dt_day_id'] == '7' && $tbl_data[3]['dt_status'] == 'Available') || ($tbl_data[4]['dt_day_id'] == '7' && $tbl_data[4]['dt_status'] == 'Available') || ($tbl_data[5]['dt_day_id'] == '7' && $tbl_data[5]['dt_status'] == 'Available') || ($tbl_data[6]['dt_day_id'] == '7' && $tbl_data[6]['dt_status'] == 'Available');
                    } else {
                        $day7 = '';
                    }
                ?>
                <div class="col-md-2" style="padding: 15px;"><h4><strong>Sunday</strong></h4></div>
                <div class="col-md-2">
                    <div class="form-group" style="padding-top: 15px;">
                        <select class="dayStatus form-control"  name="sunday_status" id="sunday_status" data-day="sunday">
                            <option value="Not Available">Not Available</option>
                            <option <?=($day7) ? 'selected' : '';?> value="Available">Available</option>
                        </select>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="sundayTimings <?=($day7) ? '' : 'hidden';?>"">
                    <div class="row" style="padding: 0px 15px;">
                        <div class="form-group col-md-5 col-xs-12 col-lg-4">
                            <label class="control-label">Start Time :</label>
                            <div class="date-and-time">
                                <input style="width: 100%;float: left;" id="sundayStartTime" name="sundayStartTime" type="text" autocomplete="off" class="form-control timepicker" data-template="dropdown"" data-show-meridian="true" data-minute-step="1" data-second-step="1" placeholder="Start Time"/>
                            </div>
                        </div>

                        <div class="form-group col-md-5 col-xs-12 col-lg-4">
                            <label class="control-label">End Time :</label>
                            <div class="date-and-time">
                                <input style="width: 100%;float: left;" id="sundayEndTime" name="sundayEndTime" type="text" autocomplete="off" class="form-control timepicker" data-template="dropdown" data-show-meridian="true" data-minute-step="1" data-second-step="1" placeholder="End Time"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <button type="button" class="btn btn-danger" onclick="window.location='<?php echo ADMIN_URL;?>pages'">Cancel</button>
                <button type="submit"  class="btn btn-success">Submit</button>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    document.addEventListener("DOMContentLoaded", function(event) {
        <?php if ($dt_doctor_id != '') {?>
            $("#dt_doctor_id").prop('readonly', true);
        <?php } ?>
        $(".dayStatus").unbind("change").bind("change", function(e) {
            e.preventDefault();
            var day = $(this).data('day');
            if ($("."+day+"Timings").hasClass("hidden")) {
                $("."+day+"Timings").removeClass("hidden");
            } else {
                $("."+day+"Timings").addClass("hidden");
            }
        });
    });
</script>