<?php
$crumb2 = "";
if(isset($tbl_data['b_id'])&&$tbl_data['b_id']!=""){
	$b_id = $tbl_data['b_id'];
	$b_number = $tbl_data['b_number'];
	$b_bed_type_id = $tbl_data['b_bed_type_id'];
	$b_is_booked = $tbl_data['b_is_booked'];
	$b_status = $tbl_data['b_status'];
	$b_created_by = $tbl_data['b_created_by'];
	$b_updated_by = $tbl_data['b_updated_by'];
	$b_added = $tbl_data['b_added'];
	$b_updated = $tbl_data['b_updated'];
	$b_is_deleted = $tbl_data['b_is_deleted'];
	$b_clinic_id = $tbl_data['b_clinic_id'];
    $crumb = "Edit";
    $action = "editRecord/".$tbl_data['b_id'];
}
else{
	$b_id = '';
	$b_number = '';
	$b_bed_type_id = '';
	$b_is_booked = '';
	$b_status = '';
	$b_created_by = '';
	$b_updated_by = '';
	$b_added = '';
	$b_updated = '';
	$b_is_deleted = '';
	$b_clinic_id = '';
    $on_home 		= "No";
    $crumb 			= "Add";
    $action 		= "addRecord";
}
?>
<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo ADMIN_URL;?>">
            <i class="entypo-home"></i>Home
        </a>
    </li>
    <li>
        <a href="<?php echo ADMIN_URL.$this->controller;?>">
            <i></i><?php echo $this->moduleName;?>
        </a>
    </li>
    <li class="active">
        <strong><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></strong>
    </li>
</ol>

<h2><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></h2>
<br />
<div class="panel panel-primary">
    <div class="panel-body">
        <form  id="page_form"
               name="page_form"
               method="post"
               action="<?php echo base_url('manage/'.$this->controller.'/'.$action);?>"
               enctype="multipart/form-data"
               class="validate"
    >
                        <div class="form-group">
                <label class="control-label">Number :<span style="color: red"> *</span></label>
                <input required type="text" name="b_number" id="b_number" value="<?php echo $b_number;?>" class="form-control " placeholder="Bed Number" data-validate="required,maxlength[250]"/>
            </div>

            <div class="form-group">
                <label class="control-label">Bed Type :<span style="color: red"> *</span></label>
                <select class="form-control bed_type_dropdown" name="b_bed_type_id" id="b_bed_type_id">
                    <option value="Select">Select</option>
                <?php
                $bedtype = $this->SqlModel->getBedTypeDropDown();
                foreach ($bedtype as $key => $value) {
                    if($value['bt_id']==$b_bed_type_id) { 
                        $selected = ' selected="selected"';
                    } else {
                        $selected = '';
                    }
                    echo '<option '.$selected.' value="'.$value['bt_id'].'">'.$value["bt_name"].'</option>';
                }
                ?>
                </select>
            </div>

            <div class="form-group col-md-6 col-xs-12 col-lg-6 hidden">
                <label class="control-label">Status :</label>
                <select class="form-control" name="b_status" id="b_status">
                    <option value="Enable" <?php if($b_status=="Enable"){ echo ' selected="selected"';} ?>>Enable</option>
                    <option value="Disable" <?php if($b_status=="Disable"){ echo ' selected="selected"';} ?>>Disable</option>
                </select>
            </div>

            <div class="form-group">
                <button type="button" class="btn btn-danger" onclick="window.location='<?php echo ADMIN_URL;?>bed'">Cancel</button>
                <button type="submit" id="bedsubmit" class="btn btn-success">Submit</button>
            </div>
        </form>
    </div>
</div>