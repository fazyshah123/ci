<div class="row" style="padding: 0px 15px;">
	<h3><strong class="reportDate"><?=date("d-M-Y");?></strong></h3>
</div>
<div class="row">
  	<div class="row alertrow hidden">
		<div class="col-md-12">
	    <button class="close alertBox" data-dismiss="alert">x</button>
			<div class="alert responseType"><strong class="responseText"></strong> <span class="responseMessage"></span></div>
		</div>
	 </div>
</div>
<div class="row" style="margin-bottom: 15px;">
	<div class="col-md-7 pull-right">
		<div class="btn-group pull-right">
		  <button type="button" data-key="today" class="btn btn-default reportBtn active todayReport">Today</button>
		  <button type="button" data-key="yesterday" class="btn btn-default reportBtn yesterdayReport">Yesterday</button>
		  <button type="button" data-key="week" class="btn btn-default reportBtn weekReport">Week</button>
		  <button type="button" data-key="month" class="btn btn-default reportBtn monthReport">Month</button>
		  <button type="button" data-key="range" class="btn btn-default reportBtn rangeReport">Range</button>
		</div>
        <div class="form-group col-md-5 pull-right hidden rangeReportFields">
            <div class="date-and-time col-md-4 pull-right" style="padding: 0px;">
                <input style="border-radius: 3px;width: 109%;float: right;" required type="text" name="end_date" id="end_date" autocomplete="off" class="form-control datepicker"  data-format="dd-M-yyyy" placeholder="To"/>
            </div>
            <div class="date-and-time col-md-4 pull-right" style="padding: 0px;margin-right: 10px;">
                <input style="border-radius: 3px;width: 109%;float: right;" required type="text" name="start_date" id="start_date" autocomplete="off" class="form-control datepicker"  data-format="dd-M-yyyy" placeholder="From"/>
            </div>
        </div>
	</div>
</div>
<div class="row">
	<div class="col-xs-6 col-sm-3 adminTileHeight150">
		<div class="tile-stats tile-orange-light">
			<div class="icon"><i class="fa fa-user-md"></i></div>
			<div class="num appointmentCount" data-start="0" data-end="0" data-postfix="" data-duration="1500" data-delay="0">0</div>
			<h3>Appointments</h3>
		</div>
	</div>
	<div class="col-xs-6 col-sm-3 adminTileHeight150">
		<div class="tile-stats tile-mag">
			<div class="icon"><i class="fa fa-wheelchair"></i></div>
			<div class="num newPatientCount" data-start="0" data-end="0" data-postfix="" data-duration="1500" data-delay="0">0</div>
			<h3>New Patients</h3>
		</div>
	</div>
	<div class="col-xs-6 col-sm-3 adminTileHeight150">
		<div class="tile-stats tile-aqua">
			<div class="icon"><i class="fa fa-dollar"></i></div>
			<span style="color: white;font-size: 20px !important;">Rs. </span>
			<div class="num expenseCount" data-start="0" data-end="0" data-postfix="" data-duration="1500" data-delay="0">0</div>
			<h3>Expenses</h3>
		</div>
	</div>
	<div class="col-xs-6 col-sm-3 adminTileHeight150">
		<div class="tile-stats tile-new-greene">
			<div class="icon"><i class="fa fa-mail-reply"></i></div>
			<span style="color: white;font-size: 20px !important;">Rs. </span>
			<div class="num purchasesCount" data-start="0" data-end="0" data-postfix="" data-duration="1500" data-delay="0">0</div>
			<h3>Purchases</h3>
		</div>
	</div>
	<div class="col-xs-6 col-sm-3 adminTileHeight150">
		<div class="tile-stats tile-purple">
			<div class="icon"><i class="fa fa-money"></i></div>
			<span style="color: white;font-size: 20px !important;">Rs. </span>
			<div class="num incomeCount" data-start="0" data-end="0" data-postfix="" data-duration="1500" data-delay="0">0</div>
			<h3>Income</h3>
		</div>
	</div>
	<div class="col-xs-6 col-sm-3 adminTileHeight150">
		<div class="tile-stats tile-gulabi">
			<div class="icon"><i class="fa fa-archive"></i></div>
			<div class="num itemsIssuedCount" data-start="0" data-end="0" data-postfix="" data-duration="1500" data-delay="0">0</div>
			<h3>Items Issued</h3>
		</div>
	</div>
	<div class="col-xs-6 col-sm-3 adminTileHeight150">
		<div class="tile-stats tile-blue">
			<div class="icon"><i class="fa fa-truck"></i></div>
			<span style="color: white;font-size: 20px !important;">Rs. </span>
			<div class="num supplierPayable" data-start="0" data-end="0" data-postfix="" data-duration="1500" data-delay="0">0</div>
			<h3>Supp. Payable</h3>
		</div>
	</div>
	<div class="col-xs-6 col-sm-3 adminTileHeight150">
		<div class="tile-stats tile-steel">
			<div class="icon"><i class="fa fa-envelope"></i></div>
			<div class="num dueInvoices" data-start="0" data-end="0" data-postfix="" data-duration="1500" data-delay="0">0</div>
			<h3>Due Invoices</h3>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-6" style="height: 350px !important;overflow-y: scroll;">
		<h4><strong>Staff Attendance</strong></h4>
		<table class="table table-hover table-striped table-condensed">
			<thead>
				<tr>
					<th>Name</th>
					<th>Status</th>
					<th>Date</th>
					<th>Check-In</th>
					<th>Check-Out</th>
				</tr>
			</thead>
			<tbody class="attendanceTBody"></tbody>
		</table>
	</div>
	<div class="col-md-6" style="height: 350px !important;overflow-y: scroll;">
		<h4><strong>Expenses</strong></h4>
		<table class="table table-hover table-striped table-condensed">
			<thead>
				<tr>
					<th>Account</th>
					<th>Description</th>
					<th>Amount</th>
					<th>Created By</th>
					<th>Time</th>
				</tr>
			</thead>
			<tbody class="expenseTBody"></tbody>
		</table>
	</div>
</div>
<div class="row">
	<div class="col-md-6" style="height: 350px !important;overflow-y: scroll;">
		<h4><strong>Purchases</strong></h4>
		<table class="table table-hover table-striped table-condensed">
			<thead>
				<tr>
					<th>Account</th>
					<th>Description</th>
					<th>Amount</th>
					<th>Created By</th>
					<th>Time</th>
				</tr>
			</thead>
			<tbody class="purchaseTBody"></tbody>
		</table>
	</div><div class="col-md-6" style="height: 350px !important;overflow-y: scroll;">
		<h4><strong>Items Issued - 14/Apr/2019</strong></h4>
		<table class="table table-hover table-striped table-condensed">
			<thead>
				<tr>
					<th>Item</th>
					<th>Description</th>
					<th>Qty</th>
					<th>Issued To</th>
					<th>Time</th>
				</tr>
			</thead>
			<tbody class="itemsTBody"></tbody>
		</table>
	</div>
</div>
<style type="text/css">
	th {
		font-weight: bolder;
	}
	.tile-stats .num {
	    visibility: visible;
	}
	.table-hover > tbody > tr:hover > td, .table-hover > tbody > tr:hover > th {
		background-color: #b6b6cc;
		color: #000;
	}
</style>