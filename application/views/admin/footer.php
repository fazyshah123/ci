<?php if(!isset($loginSection)) {?>
	<footer class="main">
		<span><strong>Powered By Machotics</strong></span>
	</footer>
<?php } ?>
	</div>
</div>
 <!-- Modal -->
<div class="modal fade" id="dupModal">
	<div class="modal-dialog">
		<div class="modal-content">
			
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Duplicate Record</h4>
			</div>
			
			<div class="modal-body">
				Are you sure you want to duplicate the <strong id="recname"></strong> record.
			</div>
			
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button type="button" id="duplicateLink" class="btn btn-primary">Confirm</button>
			</div>
		</div>
	</div>
</div>
<!-- Model -->   
<!-- Modal -->
<div class="modal fade" id="statusModal">
	<div class="modal-dialog">
		<div class="modal-content">
			
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Change Status</h4>
			</div>
			
			<div class="modal-body">
				Are you sure you want to change the status to <span id="modStatus"></span>?
			</div>
			
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button type="button" id="statusLink" class="btn btn-success">Confirm</button>
			</div>
		</div>
	</div>
</div>
<!-- Model -->
<!-- Modal -->
<div class="modal fade" id="cancelModal">
	<div class="modal-dialog">
		<div class="modal-content">
			
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Cancel Appointment</h4>
			</div>
			
			<div class="modal-body">
				Are you sure you want to cancel the appointment?
				<input type="hidden" id="cancelAppointmentId">
			</div>
			
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button type="button" id="cancelLink" class="btn btn-danger">Confirm</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="cancelTokenModal">
	<div class="modal-dialog">
		<div class="modal-content">
			
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Cancel Token</h4>
			</div>
			
			<div class="modal-body">
				Are you sure you want to cancel this token?
				<input type="hidden" id="cancelTokenId">
			</div>
			
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button type="button" id="cancelTokenLink" class="btn btn-danger">Confirm</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="checkedTokenModal">
	<div class="modal-dialog">
		<div class="modal-content">
			
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Checked Token</h4>
			</div>
			
			<div class="modal-body">
				Are you sure you want to mark token as checked?
				<input type="hidden" id="checkedTokenId">
			</div>
			
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button type="button" id="markCheckedLink" class="btn btn-danger">Confirm</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="checkedAppointmentModal">
	<div class="modal-dialog">
		<div class="modal-content">
			
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Checked Appointment</h4>
			</div>
			
			<div class="modal-body">
				Are you sure you want to mark appointment as checked?
				<input type="hidden" id="checkedAppointmentId">
			</div>
			
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button type="button" id="markAppointmentCheckedLink" class="btn btn-danger">Confirm</button>
			</div>
		</div>
	</div>
</div>
<!-- Model --><!-- Modal -->
<div class="modal fade" id="deleteModal">
	<div class="modal-dialog">
		<div class="modal-content">
			
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Delete Record</h4>
			</div>
			
			<div class="modal-body">
				Are you sure you want to delete the record?
			</div>
			
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button type="button" id="deleteLink" class="btn btn-danger">Confirm</button>
			</div>
		</div>
	</div>
</div>
<!-- Model -->

<!-- Modal -->
<div class="modal fade" id="delAllModal">
	<div class="modal-dialog">
		<div class="modal-content">
			
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Delete Selected Records</h4>
			</div>
			
			<div class="modal-body">
				Are you sure you want to delete the selected records?
			</div>
			
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-danger" id="delAllSubmit">Confirm</button>
			</div>
		</div>
	</div>
</div>
<!-- Model -->
<!-- Blog Category Modal -->

<!-- Shortcut Modal -->
<div id="testm" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" id="checkoutForm" data-dismiss="modal">&times;</button>
				<h4 class="modal-title hel">Keyboard Shortcut's</h4>
			</div>
			<div class="modal-body hlist">
				<div class="l1">
					<ul class="help-list hl-1">

          <li class="help-key-unit">
            <kbd class="help-key"><span>F1</span></kbd>
            <span class="help-key-def">Add Patients</span>
          </li>
          <li class="help-key-unit">
            <kbd class="help-key"><span>F2</span></kbd>
            <span class="help-key-def">Add Appointments</span>
          </li>
          <li class="help-key-unit">
            <kbd class="help-key"><span>F3</span></kbd>
            <span class="help-key-def">Add Invoice</span>
          </li>
          <li class="help-key-unit">
            <kbd class="help-key"><span>F4</span></kbd>
            <span class="help-key-def">Issue Token</span>
          </li>
          <li class="help-key-unit">
            <kbd class="help-key"><span>F5</span></kbd>
            <span class="help-key-def">Add Patients</span>
          </li>
          <li class="help-key-unit">
            <kbd class="help-key"><span>F6</span></kbd>
            <span class="help-key-def">Issue Items</span>
          </li>
          <li class="help-key-unit">
            <kbd class="help-key"><span>F7</span></kbd>
            <span class="help-key-def">Patient Case</span>
          </li>
          <li class="help-key-unit">
            <kbd class="help-key"><span>F8</span></kbd>
            <span class="help-key-def">Add Patient Case</span>
          </li>
          <li class="help-key-unit">
            <kbd class="help-key"><span>F9</span></kbd>
            <span class="help-key-def">Reports</span></li>
          <li class="help-key-unit">
            <kbd class="help-key"><span>F10</span></kbd>
            <span class="help-key-def">Web Settings</span>
          </li>
          <li class="help-key-unit">
            <kbd class="help-key"><span>F11</span></kbd>
            <span class="help-key-def">Add Purchase</span>
          </li>
          <li class="help-key-unit">
            <kbd class="help-key"><span>F12</span></kbd>
            <span class="help-key-def">Add Expense</span>
          </li>
        </ul><!-- .help-list -->

        <ul class="help-list hl-2">

          
          <li class="help-key-unit">
            <kbd class="help-key"><span>i</span></kbd>
            <span class="help-key-def">Check-In</span>
          </li>
         
          <li class="help-key-unit">
            <kbd class="help-key"><span>o</span></kbd>
            <span class="help-key-def">Check-Out</span>
          </li>
          <li class="help-key-unit">
            <kbd class="help-key"><span>p</span></kbd>
            <span class="help-key-def">Employee Payroll</span></li>
          <li class="help-key-unit">
            <kbd class="help-key"><span>s</span></kbd>
            <span class="help-key-def">Suppliers</span>
          </li>

        </ul><!-- .help-list --></div>
				<div class="l2">
					        <ul class="help-list hl-3">

          <li class="help-key-unit">
            <kbd class="help-key"><span>a</span></kbd>
            <span class="help-key-def">Accounts</span>
          </li>
          <li class="help-key-unit">
            <kbd class="help-key"><span>u</span></kbd>
            <span class="help-key-def">User Management</span>
          </li>
          <li class="help-key-unit">
            <kbd class="help-key"><span>r</span></kbd>
            <span class="help-key-def">Role Management</span>
          </li>
          <li class="help-key-unit">
            <kbd class="help-key"><span>l</span></kbd>
            <span class="help-key-def">Add Lab Tracking</span>
          </li>
          <li class="help-key-unit">
            <kbd class="help-key"><span>b</span></kbd>
            <span class="help-key-def">Add Bed</span>
          </li>
          <li class="help-key-unit">
            <kbd class="help-key"><span>x</span></kbd>
            <span class="help-key-def">Delete</span></li>
        </ul><!-- .help-list -->

        <ul class="help-list hl-4">
          <li class="help-key-unit">
            <kbd class="help-key"><span>Ctrl + s</span></kbd>
            <span class="help-key-def">Stocks</span>
          </li>
          <li class="help-key-unit">
            <kbd class="help-key"><span>Ctrl + a</span></kbd>
            <span class="help-key-def">Employee Attendance</span>
          </li>
          <li class="help-key-unit">
            <kbd class="help-key"><span>Ctrl + r</span></kbd>
            <span class="help-key-def">Add Room</span>
          </li>
          <li class="help-key-unit">
            <kbd class="help-key"><span>&</span></kbd>
            <span class="help-key-def">Merge</span>
          </li>
          <li class="help-key-unit">
            <kbd class="help-key"><span>Alt-w</span></kbd>
            <span class="help-key-def">Wipe</span>
          </li>
          <li class="help-key-unit">
            <kbd class="help-key"><span>$</span></kbd>
            <span class="help-key-def">Command</span>
          </li>
        
          <li class="help-key-unit">
            <kbd class="help-key"><span>*</span></kbd>
            <span class="help-key-def">Go wild</span>
          </li>
          <li class="help-key-unit">
            <kbd class="help-key"><span>[</span></kbd>
            <span class="help-key-def">Nudge left</span></li>
          <li class="help-key-unit">
            <kbd class="help-key"><span>]</span></kbd>
            <span class="help-key-def">Nudge right</span>
          </li>

        </ul><!-- .help-list -->
				</div>
	

			</div>
		</div>
	</div>
</div>
<style>

#calendars {
  max-width: 900px;
  margin: 20px auto;
  padding: 15px !important;
}
.form-control.select2-container a {
	height: 31px !important;
    line-height: 31px !important;
    border: 1px solid #aaa !important;
    border-right: none !important;
    border-left: none !important;
}
.form-control, .form-control.select2-container.select2-dropdown-open {
	border: 1px solid #aaa !important;
}

</style>
<!-- Model -->
<script>
	var ADMIN_URL = '<?=ADMIN_URL;?>',
		ENTER_KEYCODE = 13;
	var base_url = "<?php echo base_url();?>";
	var controller = "<?php echo base_url('manage/'.$this->controller).'/';?>";
	var contName = "<?php echo $this->controller;?>";
</script>
    <!-- <script src='<?php //echo ADMIN_ASSETS;?>/fullcalendar/core/main.js'></script>
    <script src='<?php //echo ADMIN_ASSETS;?>/fullcalendar/daygrid/main.js'></script>
    <script src='<?php //echo ADMIN_ASSETS;?>/fullcalendar/interaction/main.js'></script>
    <script src='<?php //echo ADMIN_ASSETS;?>/fullcalendar/timeGrid/main.js'></script> -->

	<script src="<?php echo ADMIN_ASSETS;?>js/jquery-1.10.2.min.js"></script>
	<script src="<?php echo ADMIN_ASSETS;?>js/gsap/main-gsap.js"></script>
	<script src="<?php echo ADMIN_ASSETS;?>js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
	<script src="<?php echo ADMIN_ASSETS;?>js/bootstrap.min.js"></script>
	<script src="<?php echo ADMIN_ASSETS;?>js/moment/moment.min.js"></script>
	<script src="<?php echo ADMIN_ASSETS;?>js/joinable.js"></script>
	<!-- CKEDITOR library -->
	<script src="https://cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>
	<script src="<?php echo ADMIN_ASSETS;?>js/resizeable.js"></script>
	<script src="<?php echo ADMIN_ASSETS;?>js/neon-api.js"></script>
    <script src="<?php echo ADMIN_ASSETS;?>js/select2/select2.min.js"></script>
  	<!-- <script src="<?php echo ADMIN_ASSETS;?>js/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script src="<?php echo ADMIN_ASSETS;?>js/jvectormap/jquery-jvectormap-europe-merc-en.js"></script>-->
	<script src="<?php echo ADMIN_ASSETS;?>js/jquery.sparkline.min.js"></script>
	<!--<script src="<?php echo ADMIN_ASSETS;?>js/rickshaw/vendor/d3.v3.js"></script>-->
	<!--<script src="<?php echo ADMIN_ASSETS;?>js/rickshaw/rickshaw.min.js"></script>-->
	<script src="<?php echo ADMIN_ASSETS;?>js/raphael-min.js"></script>
	<?php if(isset($dashBoard)){?>
	<script src="<?php echo ADMIN_ASSETS;?>js/morris.min.js"></script>
	<?php } if(isset($datePicker)){?>
    <script src="<?php echo ADMIN_ASSETS;?>js/bootstrap-datepicker.js"></script>
	<script src="<?php echo ADMIN_ASSETS;?>js/bootstrap-timepicker.min.js"></script>
    <?php } if(isset($uploadscript))
	{?>
	<!-- JavaScript Includes -->
	<script src="<?php echo ADMIN_ASSETS;?>js/jquery.knob.js"></script>
	<!-- jQuery File Upload Dependencies -->
	<script src="<?php echo ADMIN_ASSETS;?>js/jquery.ui.widget.js"></script>
	<script src="<?php echo ADMIN_ASSETS;?>js/jquery.iframe-transport.js"></script>
	<script src="<?php echo ADMIN_ASSETS;?>js/jquery.fileupload.js"></script>
	<!-- Our main JS file -->
	<script src="<?php echo ADMIN_ASSETS;?>js/script.js"></script>
	<?php } if(isset($suploadscript))
	{?>
	<!-- JavaScript Includes -->
	<script src="<?php echo ADMIN_ASSETS;?>js/jquery.knob.js"></script>
	<!-- jQuery File Upload Dependencies -->
	<script src="<?php echo ADMIN_ASSETS;?>js/jquery.ui.widget.js"></script>
	<script src="<?php echo ADMIN_ASSETS;?>js/jquery.iframe-transport.js"></script>
	<script src="<?php echo ADMIN_ASSETS;?>js/jquery.sliderfileupload.js"></script>
	<!-- Our main JS file -->
	<script src="<?php echo ADMIN_ASSETS;?>js/script.js"></script>
    <?php } if(isset($photosScript))
	{?>
	<!-- JavaScript Includes -->
	<script src="<?php echo ADMIN_ASSETS;?>js/jquery.knob.js"></script>
	<!-- jQuery File Upload Dependencies -->
	<script src="<?php echo ADMIN_ASSETS;?>js/jquery.ui.widget.js"></script>
	<script src="<?php echo ADMIN_ASSETS;?>js/jquery.iframe-transport.js"></script>
	<script src="<?php echo ADMIN_ASSETS;?>js/jquery.photofileupload.js"></script>

	<!-- Our main JS file -->
	<script src="<?php echo ADMIN_ASSETS;?>js/script.js"></script>
	<?php }?>

	<script src="<?php echo ADMIN_ASSETS;?>js/jquery.validate.js"></script>
    <script src="<?php echo ADMIN_ASSETS;?>js/additional-methods.js"></script>
    <script src="<?php echo ADMIN_ASSETS;?>js/hotkeys.min.js"></script>
	<script src="<?php echo ADMIN_ASSETS;?>js/neon-login.js"></script>
	<script src="<?php echo ADMIN_ASSETS;?>js/neon-custom.js"></script>
	<script src="<?php echo ADMIN_ASSETS;?>js/jquery.magnific-popup.min.js"></script>
    <script src="<?php echo ADMIN_ASSETS;?>js/custom.js"></script>
    <script src="<?php echo ADMIN_ASSETS;?>js/custom/casedetail.js"></script>
    <script src="<?php echo ADMIN_ASSETS;?>js/custom/profile.js"></script>
    <script src="<?php echo ADMIN_ASSETS;?>js/custom/reports.js"></script>
    <script src="<?php echo ADMIN_ASSETS;?>js/custom/show_appointments.js"></script>
<script>

// document.addEventListener('DOMContentLoaded', function() {  	
// 	  updateCalendarView();
// });
// function updateCalendarView(title = '<?=date("Y-m-d", strtotime("now"));?>') {
// 	var start_date = title;
// 	var end_date = title;
	
// 	$.get( "<?php //echo base_url().'manage/'.$this->controller.'/getAppointments/';?>", {
// 		'start_date': start_date,
// 		'end_date': end_date
// 	}, function( data ) {
// 		debugger;
// 		if (window.calendar)
// 			window.calendar.destroy();
// 		var calendarEl = document.getElementById('calendars');
// 		if (data != '') {
// 			data = JSON.parse(data);
// 		} else {
// 			data = '{}';
// 		}
// 		window.calendar = new FullCalendar.Calendar(calendarEl, {
// 		plugins: [ 'interaction', 'dayGrid', 'timeGrid' ],
// 			defaultView: 'dayGridDay',
// 			defaultDate: title,
// 			header: {
// 			left: 'prev,next today',
// 			center: 'title',
// 			right: 'dayGridMonth,timeGridWeek,timeGridDay'
// 			},
// 			events: JSON.parse(data)
// 		});
// 		window.calendar.render();
		
// 		$(".fc-prev-button").unbind('click').bind("click",function(e) {
// 			var title = $(".fc-center h2").text();
// 			console.log(title);
// 			updateCalendarView(title);
// 		});

// 		$(".fc-next-button").unbind('click').bind("click",function(e) {
// 			var title = $(".fc-center h2").text();
// 			updateCalendarView(title);
// 		});

//         $('.collapse').collapse({
// 		  toggle: true
// 		});
// 	});
// }
document.addEventListener("DOMContentLoaded", function(event) {
    $("#cancelLink").on("click", function(e) {
		var id = $("#cancelAppointmentId").val();
		var url = '<?=base_url()."manage/appointments/cancelAppointment/";?>'+id;
		window.location = url;
	});
	$("#cancelTokenLink").on("click", function(e) {
		var id = $("#cancelTokenId").val();
		var url = '<?=base_url()."manage/tokens/markCanceled/";?>'+id;
		window.location = url;
	});
	$("#markCheckedLink").on("click", function(e) {
		var id = $("#checkedTokenId").val();
		var url = '<?=base_url()."manage/tokens/markChecked/";?>'+id;
		window.location = url;
	});
	$("#markAppointmentCheckedLink").on("click", function(e) {
		var id = $("#checkedAppointmentId").val();
		var url = '<?=base_url()."manage/appointments/markChecked/";?>'+id;
		window.location = url;
	});
	$(".users_dropdown, .relationDropdown, .check_in_user, .checkout_user, .accounts_dropdown, .items_dropdown, #p_account_id, #p_supplier_id, .item_name, .patientsDropdown, .doctorsDropdown, #por_procedure_id, #r_type, #r_priority").select2();
	$("#sendBackup").on("click", function(e) {
		e.preventDefault();
		alert("Backup will be sent to admin's email aaddress, Thankyou!");
	});
});
</script>

<!-- shortcuts Modal script -->

<script type="text/javascript">
hotkeys('`,F1,F2,F3,F4,F5,F6,F7,F8,F9,F10,F11,F12,Home,i,o,p,ctrl+a,ctrl+s,a,s,u,r,l,b,ctrl+r', function (event, handler){
event.preventDefault();
  switch (handler.key) {
    case '`': $("#testm").modal('toggle');
      break;
    case 'F1': $("#addPatientModal").modal('toggle');
      break;
    case 'F2': window.location.href = '<?=ADMIN_URL;?>appointments/control/';
      break;
    case 'F3': window.location.href = '<?=ADMIN_URL;?>invoices/control/';
      break;
    case 'F4': $("#issueTokenModal").modal('toggle');
      break;
    case 'F5': window.location.href = '<?=ADMIN_URL;?>patients/control/';
      break;
    case 'F6': $("#issueItemModal").modal('toggle');
      break;
    case 'F7': window.location.href = '<?=ADMIN_URL;?>patientcase/';
      break;
    case 'F8': window.location.href = '<?=ADMIN_URL;?>patientcase/control';
      break;
    case 'Home': window.location.href = '<?=ADMIN_URL;?>';
      break;
    case 'F9': window.location.href = '<?=ADMIN_URL;?>reports';
      break;
    case 'F10': window.location.href = '<?=ADMIN_URL;?>home/websettings';
      break;
    case 'F11': $("#addExpenseModal").modal('toggle');
      break;
    case 'F12': window.location.href = '<?=ADMIN_URL;?>purchases/addpurchaseorder';
      break;
    case 'i': $("#markCheckInModal").modal('toggle');
      break;
    case 'o': $("#markCheckOutModal").modal('toggle');
      break;
    case 'p': window.location.href = '<?=ADMIN_URL;?>employeepayroll/control';
      break;
    case 'ctrl+a': window.location.href = '<?=ADMIN_URL;?>employeeattendance';
      break;
    case 'a': window.location.href = '<?=ADMIN_URL;?>accounts/control';
      break;
    case 'ctrl+s': window.location.href = '<?=ADMIN_URL;?>stocks/control';
      break;
    case 'r': window.location.href = '<?=ADMIN_URL;?>rolemanagement/control';
      break;
    case 'u': window.location.href = '<?=ADMIN_URL;?>admins/control';
      break;
    case 'l': window.location.href = '<?=ADMIN_URL;?>labtracking/control';
      break;
    case 'b': window.location.href = '<?=ADMIN_URL;?>bed/control';
      break;
    case 'ctrl+r': window.location.href = '<?=ADMIN_URL;?>room/control';
      break;
    default: alert(event);
  }
});
</script>
</body>
</html>