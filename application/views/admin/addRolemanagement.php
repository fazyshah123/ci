<?php
$crumb2 = "";
if(isset($tbl_data['page_id'])&&$tbl_data['page_id']!=""){
	$page_id = $tbl_data['page_id'];
	$role_title = $tbl_data['role_title'];
	$permission = $tbl_data['permission'];
	$page_status = $tbl_data['page_status'];
	$created_by = $tbl_data['created_by'];
	$modified_by = $tbl_data['modified_by'];
	$date_created = $tbl_data['date_created'];
	$date_updated = $tbl_data['date_updated'];
    $crumb = "Edit";
    $action = "editRecord/".$tbl_data['page_id'];?>
<script type="text/javascript">
var permission = JSON.parse('<?=$permission;?>');
$(document).ready(function(){ 
    $.each(permission, function(index,value) {
        $.each(value, function(ind, val) {
            name = index+'_'+ind;
            if (val == 'true') {
                $('input[name="permission['+name+']"]').attr('checked', 'checked');
            }
        });
    });
});
</script>    
<?php
} else{
	$page_id = '';
	$role_title = '';
	$permission = '';
	$page_status = '';
	$created_by = '';
	$modified_by = '';
	$date_created = '';
	$date_updated = '';
    $on_home 		= "No";
    $crumb 			= "Add";
    $action 		= "addRecord";
}
?>
<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo ADMIN_URL;?>">
            <i class="entypo-home"></i>Home
        </a>
    </li>
    <li>
        <a href="<?php echo ADMIN_URL.$this->controller;?>">
            <i></i><?php echo $this->moduleName;?>
        </a>
    </li>
    <li class="active">
        <strong><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></strong>
    </li>
</ol>

<h2><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></h2>
<br />
<div class="panel panel-primary">
    <div class="panel-body">
        <form  id="page_form"
               name="page_form"
               method="post"
               action="<?php echo base_url('manage/'.$this->controller.'/'.$action);?>"
               enctype="multipart/form-data"
               class="validate"
        >
            <div class="form-group">
                <label class="control-label">Role Title :</label>
                <input type="text" name="role_title" id="role_title" value="<?php echo $role_title;?>" class="form-control " placeholder="Role Title" data-validate="required,maxlength[250]"/>
            </div>
            <div class="form-group form-md-line-input">
                <p><label><input type="checkbox" id="checkAll"/> Check all</label></p>
                <br>
            </div>
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                      <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                          User Management
                        </a>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                            <div class="form-group form-md-line-input">
                                <label for="form_control_1" class="col-md-4 h4"><strong>User Management </strong></label>
                            
                                <input type="checkbox" class="checkbox-style form-control admins checkbox" id="admins_create" name="permission[admins_create]" value="create">
                                <label>Create </label>

                                <input type="checkbox" class="checkbox-style form-control admins" id="admins_read" name="permission[admins_read]" value="read">
                                <label>Read </label>

                                <input type="checkbox" class="checkbox-style form-control admins" id="admins_update" name="permission[admins_update]" value="update">
                                <label>Update </label>

                                <input type="checkbox" class="checkbox-style form-control admins" id="admins_delete" name="permission[admins_delete]" value="delete">
                                <label>Delete </label>

                                <input type="checkbox" class="checkbox-style form-control admins" id="admins_fullaccess" name="permission[admins_fullaccess]" value="fullaccess">
                                <label>Full Access </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwo">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                          User Role Management
                        </a>
                      </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                        <div class="panel-body">
                            <div class="form-group form-md-line-input">
                                <label for="form_control_1" class="col-md-4 h4"><strong>User Role Management </strong></label>

                                <input type="checkbox" class="checkbox-style form-control rolemanagement" id="rolemanagement_create" name="permission[rolemanagement_create]" value="create">
                                <label>Create </label>

                                <input type="checkbox" class="checkbox-style form-control rolemanagement" id="rolemanagement_read" name="permission[rolemanagement_read]" value="read">
                                <label>Read </label>

                                <input type="checkbox" class="checkbox-style form-control rolemanagement" id="rolemanagement_update" name="permission[rolemanagement_update]" value="update">
                                <label>Update </label>

                                <input type="checkbox" class="checkbox-style form-control rolemanagement" id="rolemanagement_delete" name="permission[rolemanagement_delete]" value="delete">
                                <label>Delete </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThree">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                          Clinic Management
                        </a>
                      </h4>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                        <div class="panel-body">
                            <div class="form-group form-md-line-input">
                                <label for="form_control_1" class="col-md-4 h4"><strong>Clinics Management </strong></label>

                                <input type="checkbox" class="checkbox-style form-control clinics" id="clinics_create" name="permission[clinics_create]" value="create">
                                <label>Create </label>

                                <input type="checkbox" class="checkbox-style form-control clinics" id="clinics_read" name="permission[clinics_read]" value="read">
                                <label>Read </label>

                                <input type="checkbox" class="checkbox-style form-control clinics" id="clinics_update" name="permission[clinics_update]" value="update">
                                <label>Update </label>

                                <input type="checkbox" class="checkbox-style form-control clinics" id="clinics_delete" name="permission[clinics_delete]" value="delete">
                                <label>Delete </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingFour">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                          Doctors Management
                        </a>
                      </h4>
                    </div>
                    <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                        <div class="panel-body">
                            <div class="form-group form-md-line-input">
                                <label for="form_control_1" class="col-md-4 h4"><strong>Doctors Management </strong></label>

                                <input type="checkbox" class="checkbox-style form-control doctors" id="doctors_create" name="permission[doctors_create]" value="create">
                                <label>Create </label>

                                <input type="checkbox" class="checkbox-style form-control doctors" id="doctors_read" name="permission[doctors_read]" value="read">
                                <label>Read </label>

                                <input type="checkbox" class="checkbox-style form-control doctors" id="doctors_update" name="permission[doctors_update]" value="update">
                                <label>Update </label>

                                <input type="checkbox" class="checkbox-style form-control doctors" id="doctors_delete" name="permission[doctors_delete]" value="delete">
                                <label>Delete </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingFive">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                          Patients Management
                        </a>
                      </h4>
                    </div>
                    <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                        <div class="panel-body">
                            <div class="form-group form-md-line-input">
                                <label for="form_control_1" class="col-md-4 h4"><strong>Patients Management </strong></label>

                                <input type="checkbox" class="checkbox-style form-control patients" id="patients_create" name="permission[patients_create]" value="create">
                                <label>Create </label>

                                <input type="checkbox" class="checkbox-style form-control patients" id="patients_read" name="permission[patients_read]" value="read">
                                <label>Read </label>

                                <input type="checkbox" class="checkbox-style form-control patients" id="patients_update" name="permission[patients_update]" value="update">
                                <label>Update </label>

                                <input type="checkbox" class="checkbox-style form-control patients" id="patients_delete" name="permission[patients_delete]" value="delete">
                                <label>Delete </label>

                                <input type="checkbox" class="checkbox-style form-control patients" id="patients_profile" name="permission[patients_profile]" value="profile">
                                <label>Profile </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingSix">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                          Appointments Management
                        </a>
                      </h4>
                    </div>
                    <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                        <div class="panel-body">
                            <div class="form-group form-md-line-input">
                                <label for="form_control_1" class="col-md-4 h4"><strong>Appointments Management </strong></label>

                                <input type="checkbox" class="checkbox-style form-control appointments" id="appointments_create" name="permission[appointments_create]" value="create">
                                <label>Create </label>

                                <input type="checkbox" class="checkbox-style form-control appointments" id="appointments_read" name="permission[appointments_read]" value="read">
                                <label>Read </label>

                                <input type="checkbox" class="checkbox-style form-control appointments" id="appointments_update" name="permission[appointments_update]" value="update">
                                <label>Update </label>

                                <input type="checkbox" class="checkbox-style form-control appointments" id="appointments_delete" name="permission[appointments_delete]" value="delete">
                                <label>Delete </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingSeven">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                          Procedures Management
                        </a>
                      </h4>
                    </div>
                    <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
                        <div class="panel-body">
                            <div class="form-group form-md-line-input">
                                <label for="form_control_1" class="col-md-4 h4"><strong>Procedures Management </strong></label>

                                <input type="checkbox" class="checkbox-style form-control procedures" id="procedures_create" name="permission[procedures_create]" value="create">
                                <label>Create </label>

                                <input type="checkbox" class="checkbox-style form-control procedures" id="procedures_read" name="permission[procedures_read]" value="read">
                                <label>Read </label>

                                <input type="checkbox" class="checkbox-style form-control procedures" id="procedures_update" name="permission[procedures_update]" value="update">
                                <label>Update </label>

                                <input type="checkbox" class="checkbox-style form-control procedures" id="procedures_delete" name="permission[procedures_delete]" value="delete">
                                <label>Delete </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwelve">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwelve" aria-expanded="false" aria-controls="collapseTwelve">
                          Invoices Management
                        </a>
                      </h4>
                    </div>
                    <div id="collapseTwelve" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwelve">
                        <div class="panel-body">
                            <div class="form-group form-md-line-input">
                                <label for="form_control_1" class="col-md-4 h4"><strong>Invoices Management </strong></label>

                                <input type="checkbox" class="checkbox-style form-control invoices" id="invoices_create" name="permission[invoices_create]" value="create">
                                <label>Create </label>

                                <input type="checkbox" class="checkbox-style form-control invoices" id="invoices_read" name="permission[invoices_read]" value="read">
                                <label>Read </label>

                                <input type="checkbox" class="checkbox-style form-control invoices" id="invoices_update" name="permission[invoices_update]" value="update">
                                <label>Update </label>

                                <input type="checkbox" class="checkbox-style form-control invoices" id="invoices_delete" name="permission[invoices_delete]" value="delete">
                                <label>Delete </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingEight">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                          Health Records Management
                        </a>
                      </h4>
                    </div>
                    <div id="collapseEight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEight">
                        <div class="panel-body">
                            <div class="form-group form-md-line-input">
                                <label for="form_control_1" class="col-md-4 h4"><strong>Health Records Management </strong></label>

                                <input type="checkbox" class="checkbox-style form-control healthrecord" id="healthrecord_create" name="permission[healthrecord_create]" value="create">
                                <label>Create </label>

                                <input type="checkbox" class="checkbox-style form-control healthrecord" id="healthrecord_read" name="permission[healthrecord_read]" value="read">
                                <label>Read </label>

                                <input type="checkbox" class="checkbox-style form-control healthrecord" id="healthrecord_update" name="permission[healthrecord_update]" value="update">
                                <label>Update </label>

                                <input type="checkbox" class="checkbox-style form-control healthrecord" id="healthrecord_delete" name="permission[healthrecord_delete]" value="delete">
                                <label>Delete </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingEleven1">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEleven1" aria-expanded="false" aria-controls="collapseEleven">
                          Dental Chart Management
                        </a>
                      </h4>
                    </div>
                    <div id="collapseEleven1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEleven1">
                        <div class="panel-body">
                            <div class="form-group form-md-line-input">
                                <label for="form_control_1" class="col-md-4 h4"><strong>Dental Chart Management </strong></label>

                                <input type="checkbox" class="checkbox-style form-control dentalchart" id="dentalchart_create" name="permission[dentalchart_create]" value="create">
                                <label>Create </label>

                                <input type="checkbox" class="checkbox-style form-control dentalchart" id="dentalchart_read" name="permission[dentalchart_read]" value="read">
                                <label>Read </label>

                                <input type="checkbox" class="checkbox-style form-control dentalchart" id="dentalchart_update" name="permission[dentalchart_update]" value="update">
                                <label>Update </label>

                                <input type="checkbox" class="checkbox-style form-control dentalchart" id="dentalchart_delete" name="permission[dentalchart_delete]" value="delete">
                                <label>Delete </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingNine">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                          Doctor Timing Management
                        </a>
                      </h4>
                    </div>
                    <div id="collapseNine" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingNine">
                        <div class="panel-body">
                            <div class="form-group form-md-line-input">
                                <label for="form_control_1" class="col-md-4 h4"><strong>Doctor Timing Management </strong></label>

                                <input type="checkbox" class="checkbox-style form-control doctortiming" id="doctortiming_manage" name="permission[doctortiming_manage]" value="manage">
                                <label>Manage </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTen">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                          Lab Tracking Management
                        </a>
                      </h4>
                    </div>
                    <div id="collapseTen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTen">
                        <div class="panel-body">
                            <div class="form-group form-md-line-input">
                                <label for="form_control_1" class="col-md-4 h4"><strong>Lab Tracking </strong></label>

                                <input type="checkbox" class="checkbox-style form-control labtracking" id="labtracking_create" name="permission[labtracking_create]" value="create">
                                <label>Create </label>

                                <input type="checkbox" class="checkbox-style form-control labtracking" id="labtracking_read" name="permission[labtracking_read]" value="read">
                                <label>Read </label>

                                <input type="checkbox" class="checkbox-style form-control labtracking" id="labtracking_update" name="permission[labtracking_update]" value="update">
                                <label>Update </label>

                                <input type="checkbox" class="checkbox-style form-control labtracking" id="labtracking_delete" name="permission[labtracking_delete]" value="delete">
                                <label>Delete </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingEleven">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEleven" aria-expanded="false" aria-controls="collapseEleven">
                          Reports Management
                        </a>
                      </h4>
                    </div>
                    <div id="collapseEleven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEleven">
                        <div class="panel-body">
                            <div class="form-group form-md-line-input">
                                <label for="form_control_1" class="col-md-4 h4"><strong>Reports </strong></label>

                                <input type="checkbox" class="checkbox-style form-control reports" id="reports_read" name="permission[reports_read]" value="read">
                                <label>Read </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThirteen">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThirteen" aria-expanded="false" aria-controls="collapseThirteen">
                          Work Type Management
                        </a>
                      </h4>
                    </div>
                    <div id="collapseThirteen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThirteen">
                        <div class="panel-body">
                            <div class="form-group form-md-line-input">
                                <label for="form_control_1" class="col-md-4 h4"><strong>Work Type </strong></label>

                                <input type="checkbox" class="checkbox-style form-control worktype" id="worktype_create" name="permission[worktype_create]" value="create">
                                <label>Create </label>

                                <input type="checkbox" class="checkbox-style form-control worktype" id="worktype_read" name="permission[worktype_read]" value="read">
                                <label>Read </label>

                                <input type="checkbox" class="checkbox-style form-control worktype" id="worktype_update" name="permission[worktype_update]" value="update">
                                <label>Update </label>

                                <input type="checkbox" class="checkbox-style form-control worktype" id="worktype_delete" name="permission[worktype_delete]" value="delete">
                                <label>Delete </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingFourteen">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFourteen" aria-expanded="false" aria-controls="collapseFourteen">
                          Work Management
                        </a>
                      </h4>
                    </div>
                    <div id="collapseFourteen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFourteen">
                        <div class="panel-body">
                            <div class="form-group form-md-line-input">
                                <label for="form_control_1" class="col-md-4 h4"><strong>Work </strong></label>

                                <input type="checkbox" class="checkbox-style form-control work" id="work_create" name="permission[work_create]" value="create">
                                <label>Create </label>

                                <input type="checkbox" class="checkbox-style form-control work" id="work_read" name="permission[work_read]" value="read">
                                <label>Read </label>

                                <input type="checkbox" class="checkbox-style form-control work" id="work_update" name="permission[work_update]" value="update">
                                <label>Update </label>

                                <input type="checkbox" class="checkbox-style form-control work" id="work_delete" name="permission[work_delete]" value="delete">
                                <label>Delete </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingFifteen">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFifteen" aria-expanded="false" aria-controls="collapseFifteen">
                          Suppliers Management
                        </a>
                      </h4>
                    </div>
                    <div id="collapseFifteen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFifteen">
                        <div class="panel-body">
                            <div class="form-group form-md-line-input">
                                <label for="form_control_1" class="col-md-4 h4"><strong>Suppliers</strong></label>

                                <input type="checkbox" class="checkbox-style form-control suppliers" id="suppliers_create" name="permission[suppliers_create]" value="create">
                                <label>Create </label>

                                <input type="checkbox" class="checkbox-style form-control suppliers" id="suppliers_read" name="permission[suppliers_read]" value="read">
                                <label>Read </label>

                                <input type="checkbox" class="checkbox-style form-control suppliers" id="suppliers_update" name="permission[suppliers_update]" value="update">
                                <label>Update </label>

                                <input type="checkbox" class="checkbox-style form-control suppliers" id="suppliers_delete" name="permission[suppliers_delete]" value="delete">
                                <label>Delete </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingSixteen">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSixteen" aria-expanded="false" aria-controls="collapseSixteen">
                          Lab Management
                        </a>
                      </h4>
                    </div>
                    <div id="collapseSixteen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSixteen">
                        <div class="panel-body">
                            <div class="form-group form-md-line-input">
                                <label for="form_control_1" class="col-md-4 h4"><strong>Lab </strong></label>

                                <input type="checkbox" class="checkbox-style form-control lab" id="lab_create" name="permission[lab_create]" value="create">
                                <label>Create </label>

                                <input type="checkbox" class="checkbox-style form-control lab" id="lab_read" name="permission[lab_read]" value="read">
                                <label>Read </label>

                                <input type="checkbox" class="checkbox-style form-control lab" id="lab_update" name="permission[lab_update]" value="update">
                                <label>Update </label>

                                <input type="checkbox" class="checkbox-style form-control lab" id="lab_delete" name="permission[lab_delete]" value="delete">
                                <label>Delete </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingSeventeen">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeventeen" aria-expanded="false" aria-controls="collapseSeventeen">
                          Site Settings
                        </a>
                      </h4>
                    </div>
                    <div id="collapseSeventeen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeventeen">
                        <div class="panel-body">
                            <div class="form-group form-md-line-input">
                                <label for="form_control_1" class="col-md-4 h4"><strong>Site Settings </strong></label>

                                <input type="checkbox" class="checkbox-style form-control site" id="site_create" name="permission[site_create]" value="create">
                                <label>Create </label>

                                <input type="checkbox" class="checkbox-style form-control site" id="site_read" name="permission[site_read]" value="read">
                                <label>Read </label>

                                <input type="checkbox" class="checkbox-style form-control site" id="site_update" name="permission[site_update]" value="update">
                                <label>Update </label>

                                <input type="checkbox" class="checkbox-style form-control site" id="site_delete" name="permission[site_delete]" value="delete">
                                <label>Delete </label>
                            </div>
                        </div>
                    </div>
                </div>
                            
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingEighteen">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEighteen" aria-expanded="false" aria-controls="collapseEighteen">
                          Paitent Files Management
                        </a>
                      </h4>
                    </div>
                    <div id="collapseEighteen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEighteen">
                        <div class="panel-body">
                            <div class="form-group form-md-line-input">
                                <label for="form_control_1" class="col-md-4 h4"><strong>Paitent Files </strong></label>

                                <input type="checkbox" class="checkbox-style form-control patientfiles" id="patientfiles_create" name="permission[patientfiles_create]" value="create">
                                <label>Create </label>

                                <input type="checkbox" class="checkbox-style form-control patientfiles" id="patientfiles_read" name="permission[patientfiles_read]" value="read">
                                <label>Read </label>

                                <input type="checkbox" class="checkbox-style form-control patientfiles" id="patientfiles_update" name="permission[patientfiles_update]" value="update">
                                <label>Update </label>

                                <input type="checkbox" class="checkbox-style form-control patientfiles" id="patientfiles_delete" name="permission[patientfiles_delete]" value="delete">
                                <label>Delete </label>
                            </div>
                        </div>
                    </div>
                </div>
                            
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingNineteen">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseNineteen" aria-expanded="false" aria-controls="collapseNineteen">
                          Expenses Management
                        </a>
                      </h4>
                    </div>
                    <div id="collapseNineteen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingNineteen">
                        <div class="panel-body">
                            <div class="form-group form-md-line-input">
                                <label for="form_control_1" class="col-md-4 h4"><strong>Expenses </strong></label>

                                <input type="checkbox" class="checkbox-style form-control expenses" id="expenses_create" name="permission[expenses_create]" value="create">
                                <label>Create </label>

                                <input type="checkbox" class="checkbox-style form-control expenses" id="expenses_read" name="permission[expenses_read]" value="read">
                                <label>Read </label>

                                <input type="checkbox" class="checkbox-style form-control expenses" id="expenses_update" name="permission[expenses_update]" value="update">
                                <label>Update </label>

                                <input type="checkbox" class="checkbox-style form-control expenses" id="expenses_delete" name="permission[expenses_delete]" value="delete">
                                <label>Delete </label>
                            </div>
                        </div>
                    </div>
                </div>
                            
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwenty">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwenty" aria-expanded="false" aria-controls="collapseTwenty">
                          Purchases Management
                        </a>
                      </h4>
                    </div>
                    <div id="collapseTwenty" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwenty">
                        <div class="panel-body">
                            <div class="form-group form-md-line-input">
                                <label for="form_control_1" class="col-md-4 h4"><strong>Purchases </strong></label>

                                <input type="checkbox" class="checkbox-style form-control purchases" id="purchases_create" name="permission[purchases_create]" value="create">
                                <label>Create </label>

                                <input type="checkbox" class="checkbox-style form-control purchases" id="purchases_read" name="permission[purchases_read]" value="read">
                                <label>Read </label>

                                <input type="checkbox" class="checkbox-style form-control purchases" id="purchases_update" name="permission[purchases_update]" value="update">
                                <label>Update </label>

                                <input type="checkbox" class="checkbox-style form-control purchases" id="purchases_delete" name="permission[purchases_delete]" value="delete">
                                <label>Delete </label>
                            </div>
                        </div>
                    </div>
                </div>
                            
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwentyOne">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwentyOne" aria-expanded="false" aria-controls="collapseTwentyOne">
                          Items Management
                        </a>
                      </h4>
                    </div>
                    <div id="collapseTwentyOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwentyOne">
                        <div class="panel-body">
                            <div class="form-group form-md-line-input">
                                <label for="form_control_1" class="col-md-4 h4"><strong>Items </strong></label>

                                <input type="checkbox" class="checkbox-style form-control items" id="items_create" name="permission[items_create]" value="create">
                                <label>Create </label>

                                <input type="checkbox" class="checkbox-style form-control items" id="items_read" name="permission[items_read]" value="read">
                                <label>Read </label>

                                <input type="checkbox" class="checkbox-style form-control items" id="items_update" name="permission[items_update]" value="update">
                                <label>Update </label>

                                <input type="checkbox" class="checkbox-style form-control items" id="items_delete" name="permission[items_delete]" value="delete">
                                <label>Delete </label>

                                <input type="checkbox" class="checkbox-style form-control items" id="items_issue" name="permission[items_issue]" value="issue">
                                <label>Issue </label>
                            </div>
                        </div>
                    </div>
                </div>
                            
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwentyTwo">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwentyTwo" aria-expanded="false" aria-controls="collapseTwentyTwo">
                          Stocks Management
                        </a>
                      </h4>
                    </div>
                    <div id="collapseTwentyTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwentyTwo">
                        <div class="panel-body">
                            <div class="form-group form-md-line-input">
                                <label for="form_control_1" class="col-md-4 h4"><strong>Stocks </strong></label>

                                <input type="checkbox" class="checkbox-style form-control stocks" id="stocks_create" name="permission[stocks_create]" value="create">
                                <label>Create </label>

                                <input type="checkbox" class="checkbox-style form-control stocks" id="stocks_read" name="permission[stocks_read]" value="read">
                                <label>Read </label>

                                <input type="checkbox" class="checkbox-style form-control stocks" id="stocks_update" name="permission[stocks_update]" value="update">
                                <label>Update </label>

                                <input type="checkbox" class="checkbox-style form-control stocks" id="stocks_delete" name="permission[stocks_delete]" value="delete">
                                <label>Delete </label>
                            </div>
                        </div>
                    </div>
                </div>
                            
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwentyThree">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwentyThree" aria-expanded="false" aria-controls="collapseTwentyThree">
                          Accounts Management
                        </a>
                      </h4>
                    </div>
                    <div id="collapseTwentyThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwentyThree">
                        <div class="panel-body">
                            <div class="form-group form-md-line-input">
                                <label for="form_control_1" class="col-md-4 h4"><strong>Accounts </strong></label>

                                <input type="checkbox" class="checkbox-style form-control accounts" id="accounts_create" name="permission[accounts_create]" value="create">
                                <label>Create </label>

                                <input type="checkbox" class="checkbox-style form-control accounts" id="accounts_read" name="permission[accounts_read]" value="read">
                                <label>Read </label>

                                <input type="checkbox" class="checkbox-style form-control accounts" id="accounts_update" name="permission[accounts_update]" value="update">
                                <label>Update </label>

                                <input type="checkbox" class="checkbox-style form-control accounts" id="accounts_delete" name="permission[accounts_delete]" value="delete">
                                <label>Delete </label>
                            </div>
                        </div>
                    </div>
                </div>
                           
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwentyFour">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwentyFour" aria-expanded="false" aria-controls="collapseTwentyFour">
                          Stock Release Management
                        </a>
                      </h4>
                    </div>
                    <div id="collapseTwentyFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwentyFour">
                        <div class="panel-body">
                            <div class="form-group form-md-line-input">
                                <label for="form_control_1" class="col-md-4 h4"><strong>Stock Release </strong></label>

                                <input type="checkbox" class="checkbox-style form-control stockreleases" id="stockreleases_create" name="permission[stockreleases_create]" value="create">
                                <label>Create </label>

                                <input type="checkbox" class="checkbox-style form-control stockreleases" id="stockreleases_read" name="permission[stockreleases_read]" value="read">
                                <label>Read </label>

                                <input type="checkbox" class="checkbox-style form-control stockreleases" id="stockreleases_update" name="permission[stockreleases_update]" value="update">
                                <label>Update </label>

                                <input type="checkbox" class="checkbox-style form-control stockreleases" id="stockreleases_delete" name="permission[stockreleases_delete]" value="delete">
                                <label>Delete </label>
                            </div>
                        </div>
                    </div>
                </div>
                            
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwentyFive">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwentyFive" aria-expanded="false" aria-controls="collapseTwentyFive">
                          Employee Payroll Management
                        </a>
                      </h4>
                    </div>
                    <div id="collapseTwentyFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwentyFive">
                        <div class="panel-body">
                            <div class="form-group form-md-line-input">
                                <label for="form_control_1" class="col-md-4 h4"><strong>Employee Payroll </strong></label>

                                <input type="checkbox" class="checkbox-style form-control employeepayroll" id="employeepayroll_create" name="permission[employeepayroll_create]" value="create">
                                <label>Create </label>

                                <input type="checkbox" class="checkbox-style form-control employeepayroll" id="employeepayroll_read" name="permission[employeepayroll_read]" value="read">
                                <label>Read </label>

                                <input type="checkbox" class="checkbox-style form-control employeepayroll" id="employeepayroll_update" name="permission[employeepayroll_update]" value="update">
                                <label>Update </label>

                                <input type="checkbox" class="checkbox-style form-control employeepayroll" id="employeepayroll_delete" name="permission[employeepayroll_delete]" value="delete">
                                <label>Delete </label>
                            </div>
                        </div>
                    </div>
                </div>
                            
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwentyFive1">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwentyFive1" aria-expanded="false" aria-controls="collapseTwentyFive">
                          Employee Attendance Management
                        </a>
                      </h4>
                    </div>
                    <div id="collapseTwentyFive1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwentyFive1">
                        <div class="panel-body">
                            <div class="form-group form-md-line-input">
                                <label for="form_control_1" class="col-md-4 h4"><strong>Employee Attendance </strong></label>

                                <input type="checkbox" class="checkbox-style form-control employeeattendance" id="employeeattendance_create" name="permission[employeeattendance_create]" value="create">
                                <label>Create </label>

                                <input type="checkbox" class="checkbox-style form-control employeeattendance" id="employeeattendance_read" name="permission[employeeattendance_read]" value="read">
                                <label>Read </label>

                                <input type="checkbox" class="checkbox-style form-control employeeattendance" id="employeeattendance_update" name="permission[employeeattendance_update]" value="update">
                                <label>Update </label>

                                <input type="checkbox" class="checkbox-style form-control employeeattendance" id="employeeattendance_delete" name="permission[employeeattendance_delete]" value="delete">
                                <label>Delete </label>
                            </div>
                        </div>
                    </div>
                </div>
                            
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwentySix">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwentySix" aria-expanded="false" aria-controls="collapseTwentySix">
                          Tokens Management
                        </a>
                      </h4>
                    </div>
                    <div id="collapseTwentySix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwentySix">
                        <div class="panel-body">
                            <div class="form-group form-md-line-input">
                                <label for="form_control_1" class="col-md-4 h4"><strong>Tokens </strong></label>

                                <input type="checkbox" class="checkbox-style form-control tokens" id="tokens_create" name="permission[tokens_create]" value="create">
                                <label>Create </label>

                                <input type="checkbox" class="checkbox-style form-control tokens" id="tokens_read" name="permission[tokens_read]" value="read">
                                <label>Read </label>

                                <input type="checkbox" class="checkbox-style form-control tokens" id="tokens_update" name="permission[tokens_update]" value="update">
                                <label>Update </label>

                                <input type="checkbox" class="checkbox-style form-control tokens" id="tokens_delete" name="permission[tokens_delete]" value="delete">
                                <label>Delete </label>
                            </div>
                        </div>
                    </div>
                </div>
                            
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwentySeven">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwentySeven" aria-expanded="false" aria-controls="collapseTwentySeven">
                          Report Types Management
                        </a>
                      </h4>
                    </div>
                    <div id="collapseTwentySeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwentySeven">
                        <div class="panel-body">
                            <div class="form-group form-md-line-input">
                                <label for="form_control_1" class="col-md-4 h4"><strong>Report Type </strong></label>

                                <input type="checkbox" class="checkbox-style form-control reporttype" id="reporttype_create" name="permission[reporttype_create]" value="create">
                                <label>Create </label>

                                <input type="checkbox" class="checkbox-style form-control reporttype" id="reporttype_read" name="permission[reporttype_read]" value="read">
                                <label>Read </label>

                                <input type="checkbox" class="checkbox-style form-control reporttype" id="reporttype_update" name="permission[reporttype_update]" value="update">
                                <label>Update </label>

                                <input type="checkbox" class="checkbox-style form-control reporttype" id="reporttype_delete" name="permission[reporttype_delete]" value="delete">
                                <label>Delete </label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwentyEight">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwentyEight" aria-expanded="false" aria-controls="collapseTwentyEight">
                          Room Type Management
                        </a>
                      </h4>
                    </div>
                    <div id="collapseTwentyEight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwentyEight">
                        <div class="panel-body">
                            <div class="form-group form-md-line-input">
                                <label for="form_control_1" class="col-md-4 h4"><strong>Room Type </strong></label>

                                <input type="checkbox" class="checkbox-style form-control roomtype" id="roomtype_create" name="permission[roomtype_create]" value="create">
                                <label>Create </label>

                                <input type="checkbox" class="checkbox-style form-control roomtype" id="roomtype_read" name="permission[roomtype_read]" value="read">
                                <label>Read </label>

                                <input type="checkbox" class="checkbox-style form-control roomtype" id="roomtype_update" name="permission[roomtype_update]" value="update">
                                <label>Update </label>

                                <input type="checkbox" class="checkbox-style form-control roomtype" id="roomtype_delete" name="permission[roomtype_delete]" value="delete">
                                <label>Delete </label>
                            </div>
                        </div>
                    </div>
                </div>
                            
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwentyNine">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwentyNine" aria-expanded="false" aria-controls="collapseTwentyNine">
                          Room Management
                        </a>
                      </h4>
                    </div>
                    <div id="collapseTwentyNine" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwentyNine">
                        <div class="panel-body">
                            <div class="form-group form-md-line-input">
                                <label for="form_control_1" class="col-md-4 h4"><strong>Room </strong></label>

                                <input type="checkbox" class="checkbox-style form-control room" id="room_create" name="permission[room_create]" value="create">
                                <label>Create </label>

                                <input type="checkbox" class="checkbox-style form-control room" id="room_read" name="permission[room_read]" value="read">
                                <label>Read </label>

                                <input type="checkbox" class="checkbox-style form-control room" id="room_update" name="permission[room_update]" value="update">
                                <label>Update </label>

                                <input type="checkbox" class="checkbox-style form-control room" id="room_delete" name="permission[room_delete]" value="delete">
                                <label>Delete </label>
                            </div>
                        </div>
                    </div>
                </div>
                            
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThirty">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThirty" aria-expanded="false" aria-controls="collapseThirty">
                          Bed Type Management
                        </a>
                      </h4>
                    </div>
                    <div id="collapseThirty" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThirty">
                        <div class="panel-body">
                            <div class="form-group form-md-line-input">
                                <label for="form_control_1" class="col-md-4 h4"><strong>Bed Type </strong></label>

                                <input type="checkbox" class="checkbox-style form-control bedtype" id="bedtype_create" name="permission[bedtype_create]" value="create">
                                <label>Create </label>

                                <input type="checkbox" class="checkbox-style form-control bedtype" id="bedtype_read" name="permission[bedtype_read]" value="read">
                                <label>Read </label>

                                <input type="checkbox" class="checkbox-style form-control bedtype" id="bedtype_update" name="permission[bedtype_update]" value="update">
                                <label>Update </label>

                                <input type="checkbox" class="checkbox-style form-control bedtype" id="bedtype_delete" name="permission[bedtype_delete]" value="delete">
                                <label>Delete </label>
                            </div>
                        </div>
                    </div>
                </div>
                            
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThiryOne">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThiryOne" aria-expanded="false" aria-controls="collapseThiryOne">
                          Bed Management
                        </a>
                      </h4>
                    </div>
                    <div id="collapseThiryOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThiryOne">
                        <div class="panel-body">
                            <div class="form-group form-md-line-input">
                                <label for="form_control_1" class="col-md-4 h4"><strong>Bed </strong></label>

                                <input type="checkbox" class="checkbox-style form-control bed" id="bed_create" name="permission[bed_create]" value="create">
                                <label>Create </label>

                                <input type="checkbox" class="checkbox-style form-control bed" id="bed_read" name="permission[bed_read]" value="read">
                                <label>Read </label>

                                <input type="checkbox" class="checkbox-style form-control bed" id="bed_update" name="permission[bed_update]" value="update">
                                <label>Update </label>

                                <input type="checkbox" class="checkbox-style form-control bed" id="bed_delete" name="permission[bed_delete]" value="delete">
                                <label>Delete </label>
                            </div>
                        </div>
                    </div>
                </div>
                            
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThiryTwo">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThiryTwo" aria-expanded="false" aria-controls="collapseThiryTwo">
                          Bed Patient Management
                        </a>
                      </h4>
                    </div>
                    <div id="collapseThiryTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThiryTwo">
                        <div class="panel-body">
                            <div class="form-group form-md-line-input">
                                <label for="form_control_1" class="col-md-4 h4"><strong>Bed Patient </strong></label>

                                <input type="checkbox" class="checkbox-style form-control bedpatient" id="bedpatient_create" name="permission[bedpatient_create]" value="create">
                                <label>Create </label>

                                <input type="checkbox" class="checkbox-style form-control bedpatient" id="bedpatient_read" name="permission[bedpatient_read]" value="read">
                                <label>Read </label>

                                <input type="checkbox" class="checkbox-style form-control bedpatient" id="bedpatient_update" name="permission[bedpatient_update]" value="update">
                                <label>Update </label>

                                <input type="checkbox" class="checkbox-style form-control bedpatient" id="bedpatient_delete" name="permission[bedpatient_delete]" value="delete">
                                <label>Delete </label>
                            </div>
                        </div>
                    </div>
                </div>
                            
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThiryThree">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThiryThree" aria-expanded="false" aria-controls="collapseThiryThree">
                          Room Patient
                        </a>
                      </h4>
                    </div>
                    <div id="collapseThiryThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThiryThree">
                        <div class="panel-body">
                            <div class="form-group form-md-line-input">
                                <label for="form_control_1" class="col-md-4 h4"><strong>Room Patient </strong></label>

                                <input type="checkbox" class="checkbox-style form-control roompatient" id="roompatient_create" name="permission[roompatient_create]" value="create">
                                <label>Create </label>

                                <input type="checkbox" class="checkbox-style form-control roompatient" id="roompatient_read" name="permission[roompatient_read]" value="read">
                                <label>Read </label>

                                <input type="checkbox" class="checkbox-style form-control roompatient" id="roompatient_update" name="permission[roompatient_update]" value="update">
                                <label>Update </label>

                                <input type="checkbox" class="checkbox-style form-control roompatient" id="roompatient_delete" name="permission[roompatient_delete]" value="delete">
                                <label>Delete </label>
                            </div>
                        </div>
                    </div>
                </div>
                            
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThiryFive">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThiryFive" aria-expanded="false" aria-controls="collapseThiryFive">
                          Patient Procedures Management
                        </a>
                      </h4>
                    </div>
                    <div id="collapseThiryFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThiryFive">
                        <div class="panel-body">
                            <div class="form-group form-md-line-input">
                                <label for="form_control_1" class="col-md-4 h4"><strong>Patient Procedures </strong></label>

                                <input type="checkbox" class="checkbox-style form-control patientprocedures" id="patientprocedures_create" name="permission[patientprocedures_create]" value="create">
                                <label>Create </label>

                                <input type="checkbox" class="checkbox-style form-control patientprocedures" id="patientprocedures_read" name="permission[patientprocedures_read]" value="read">
                                <label>Read </label>

                                <input type="checkbox" class="checkbox-style form-control patientprocedures" id="patientprocedures_update" name="permission[patientprocedures_update]" value="update">
                                <label>Update </label>

                                <input type="checkbox" class="checkbox-style form-control patientprocedures" id="patientprocedures_delete" name="permission[patientprocedures_delete]" value="delete">
                                <label>Delete </label>
                            </div>
                        </div>
                    </div>
                </div>
                            
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThirySix">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThirySix" aria-expanded="false" aria-controls="collapseThirySix">
                          Facilities Management
                        </a>
                      </h4>
                    </div>
                    <div id="collapseThirySix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThirySix">
                        <div class="panel-body">
                            <div class="form-group form-md-line-input">
                                <label for="form_control_1" class="col-md-4 h4"><strong>Facilities </strong></label>

                                <input type="checkbox" class="checkbox-style form-control facilities" id="facilities_create" name="permission[facilities_create]" value="create">
                                <label>Create </label>

                                <input type="checkbox" class="checkbox-style form-control facilities" id="facilities_read" name="permission[facilities_read]" value="read">
                                <label>Read </label>

                                <input type="checkbox" class="checkbox-style form-control facilities" id="facilities_update" name="permission[facilities_update]" value="update">
                                <label>Update </label>

                                <input type="checkbox" class="checkbox-style form-control facilities" id="facilities_delete" name="permission[facilities_delete]" value="delete">
                                <label>Delete </label>
                            </div>
                        </div>
                    </div>
                </div>
                            
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThirySeven">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThirySeven" aria-expanded="false" aria-controls="collapseThirySeven">
                          Patient Case Management
                        </a>
                      </h4>
                    </div>
                    <div id="collapseThirySeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThirySeven">
                        <div class="panel-body">
                            <div class="form-group form-md-line-input">
                                <label for="form_control_1" class="col-md-4 h4"><strong>Patient Case </strong></label>

                                <input type="checkbox" class="checkbox-style form-control patientcase" id="patientcase_create" name="permission[patientcase_create]" value="create">
                                <label>Create </label>

                                <input type="checkbox" class="checkbox-style form-control patientcase" id="patientcase_read" name="permission[patientcase_read]" value="read">
                                <label>Read </label>

                                <input type="checkbox" class="checkbox-style form-control patientcase" id="patientcase_update" name="permission[patientcase_update]" value="update">
                                <label>Update </label>

                                <input type="checkbox" class="checkbox-style form-control patientcase" id="patientcase_delete" name="permission[patientcase_delete]" value="delete">
                                <label>Delete </label>

                                <input type="checkbox" class="checkbox-style form-control patientcase" id="patientcase_details" name="permission[patientcase_details]" value="details">
                                <label>Details </label>
                            </div>
                        </div>
                    </div>
                </div>
                            
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThiryEight">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThiryEight" aria-expanded="false" aria-controls="collapseThiryEight">
                          Relations Management
                        </a>
                      </h4>
                    </div>
                    <div id="collapseThiryEight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThiryEight">
                        <div class="panel-body">
                            <div class="form-group form-md-line-input">
                                <label for="form_control_1" class="col-md-4 h4"><strong>Relations </strong></label>

                                <input type="checkbox" class="checkbox-style form-control relations" id="relations_create" name="permission[relations_create]" value="create">
                                <label>Create </label>

                                <input type="checkbox" class="checkbox-style form-control relations" id="relations_read" name="permission[relations_read]" value="read">
                                <label>Read </label>

                                <input type="checkbox" class="checkbox-style form-control relations" id="relations_update" name="permission[relations_update]" value="update">
                                <label>Update </label>

                                <input type="checkbox" class="checkbox-style form-control relations" id="relations_delete" name="permission[relations_delete]" value="delete">
                                <label>Delete </label>
                            </div>
                        </div>
                    </div>
                </div>
                            
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThiryNine">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThiryNine" aria-expanded="false" aria-controls="collapseThiryNine">
                          Patient Facilities Management
                        </a>
                      </h4>
                    </div>
                    <div id="collapseThiryNine" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThiryNine">
                        <div class="panel-body">
                            <div class="form-group form-md-line-input">
                                <label for="form_control_1" class="col-md-4 h4"><strong>Patient Facility </strong></label>

                                <input type="checkbox" class="checkbox-style form-control patientfacility" id="patientfacility_create" name="permission[patientfacility_create]" value="create">
                                <label>Create </label>

                                <input type="checkbox" class="checkbox-style form-control patientfacility" id="patientfacility_read" name="permission[patientfacility_read]" value="read">
                                <label>Read </label>

                                <input type="checkbox" class="checkbox-style form-control patientfacility" id="patientfacility_update" name="permission[patientfacility_update]" value="update">
                                <label>Update </label>

                                <input type="checkbox" class="checkbox-style form-control patientfacility" id="patientfacility_delete" name="permission[patientfacility_delete]" value="delete">
                                <label>Delete </label>
                            </div>
                        </div>
                    </div>
                </div>
                            
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingFourty">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFourty" aria-expanded="false" aria-controls="collapseFourty">
                          Treatments Management
                        </a>
                      </h4>
                    </div>
                    <div id="collapseFourty" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFourty">
                        <div class="panel-body">
                            <div class="form-group form-md-line-input">
                                <label for="form_control_1" class="col-md-4 h4"><strong>Treatments </strong></label>

                                <input type="checkbox" class="checkbox-style form-control treatments" id="treatments_create" name="permission[treatments_create]" value="create">
                                <label>Create </label>

                                <input type="checkbox" class="checkbox-style form-control treatments" id="treatments_read" name="permission[treatments_read]" value="read">
                                <label>Read </label>

                                <input type="checkbox" class="checkbox-style form-control treatments" id="treatments_update" name="permission[treatments_update]" value="update">
                                <label>Update </label>

                                <input type="checkbox" class="checkbox-style form-control treatments" id="treatments_delete" name="permission[treatments_delete]" value="delete">
                                <label>Delete </label>
                            </div>
                        </div>
                    </div>
                </div>
                            
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingFourtyOne">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFourtyOne" aria-expanded="false" aria-controls="collapseFourtyOne">
                          Patient Treatment Management
                        </a>
                      </h4>
                    </div>
                    <div id="collapseFourtyOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFourtyOne">
                        <div class="panel-body">
                            <div class="form-group form-md-line-input">
                                <label for="form_control_1" class="col-md-4 h4"><strong>Patient Treatment </strong></label>

                                <input type="checkbox" class="checkbox-style form-control patienttreatment" id="patienttreatment_create" name="permission[patienttreatment_create]" value="create">
                                <label>Create </label>

                                <input type="checkbox" class="checkbox-style form-control patienttreatment" id="patienttreatment_read" name="permission[patienttreatment_read]" value="read">
                                <label>Read </label>

                                <input type="checkbox" class="checkbox-style form-control patienttreatment" id="patienttreatment_update" name="permission[patienttreatment_update]" value="update">
                                <label>Update </label>

                                <input type="checkbox" class="checkbox-style form-control patienttreatment" id="patienttreatment_delete" name="permission[patienttreatment_delete]" value="delete">
                                <label>Delete </label>
                            </div>
                        </div>
                    </div>
                </div>
                            
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingFourtyTwo">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFourtyTwo" aria-expanded="false" aria-controls="collapseFourtyTwo">
                          Patient Visit Log Management
                        </a>
                      </h4>
                    </div>
                    <div id="collapseFourtyTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFourtyTwo">
                        <div class="panel-body">
                            <div class="form-group form-md-line-input">
                                <label for="form_control_1" class="col-md-4 h4"><strong>Patient Visit Log </strong></label>

                                <input type="checkbox" class="checkbox-style form-control patientvisitlog" id="patientvisitlog_create" name="permission[patientvisitlog_create]" value="create">
                                <label>Create </label>

                                <input type="checkbox" class="checkbox-style form-control patientvisitlog" id="patientvisitlog_read" name="permission[patientvisitlog_read]" value="read">
                                <label>Read </label>

                                <input type="checkbox" class="checkbox-style form-control patientvisitlog" id="patientvisitlog_update" name="permission[patientvisitlog_update]" value="update">
                                <label>Update </label>

                                <input type="checkbox" class="checkbox-style form-control patientvisitlog" id="patientvisitlog_delete" name="permission[patientvisitlog_delete]" value="delete">
                                <label>Delete </label>
                            </div>
                        </div>
                    </div>
                </div>
                            
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingFourtyThree">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFourtyThree" aria-expanded="false" aria-controls="collapseFourtyThree">
                          Patient Surgical Note Management
                        </a>
                      </h4>
                    </div>
                    <div id="collapseFourtyThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFourtyThree">
                        <div class="panel-body">
                            <div class="form-group form-md-line-input">
                                <label for="form_control_1" class="col-md-4 h4"><strong>Patient Surgical Note </strong></label>

                                <input type="checkbox" class="checkbox-style form-control patientsurgicalnote" id="patientsurgicalnote_create" name="permission[patientsurgicalnote_create]" value="create">
                                <label>Create </label>

                                <input type="checkbox" class="checkbox-style form-control patientsurgicalnote" id="patientsurgicalnote_read" name="permission[patientsurgicalnote_read]" value="read">
                                <label>Read </label>

                                <input type="checkbox" class="checkbox-style form-control patientsurgicalnote" id="patientsurgicalnote_update" name="permission[patientsurgicalnote_update]" value="update">
                                <label>Update </label>

                                <input type="checkbox" class="checkbox-style form-control patientsurgicalnote" id="patientsurgicalnote_delete" name="permission[patientsurgicalnote_delete]" value="delete">
                                <label>Delete </label>
                            </div>
                        </div>
                    </div>
                </div>
                            
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingFourtyFive">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFourtyFive" aria-expanded="false" aria-controls="collapseFourtyFive">
                          Patient Case Examination Management
                        </a>
                      </h4>
                    </div>
                    <div id="collapseFourtyFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFourtyFive">
                        <div class="panel-body">
                            <div class="form-group form-md-line-input">
                                <label for="form_control_1" class="col-md-4 h4"><strong>Patient Case Examinations </strong></label>

                                <input type="checkbox" class="checkbox-style form-control patientcaseexaminations" id="patientcaseexaminations_create" name="permission[patientcaseexaminations_create]" value="create">
                                <label>Create </label>

                                <input type="checkbox" class="checkbox-style form-control patientcaseexaminations" id="patientcaseexaminations_read" name="permission[patientcaseexaminations_read]" value="read">
                                <label>Read </label>

                                <input type="checkbox" class="checkbox-style form-control patientcaseexaminations" id="patientcaseexaminations_update" name="permission[patientcaseexaminations_update]" value="update">
                                <label>Update </label>

                                <input type="checkbox" class="checkbox-style form-control patientcaseexaminations" id="patientcaseexaminations_delete" name="permission[patientcaseexaminations_delete]" value="delete">
                                <label>Delete </label>
                            </div>
                        </div>
                    </div>
                </div>
                            
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingFourtySix">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFourtySix" aria-expanded="false" aria-controls="collapseFourtySix">
                          Patient Operation Record Management
                        </a>
                      </h4>
                    </div>
                    <div id="collapseFourtySix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFourtySix">
                        <div class="panel-body">
                            <div class="form-group form-md-line-input">
                                <label for="form_control_1" class="col-md-4 h4"><strong>Patient Operation Record </strong></label>

                                <input type="checkbox" class="checkbox-style form-control patientoperationrecord" id="patientoperationrecord_create" name="permission[patientoperationrecord_create]" value="create">
                                <label>Create </label>

                                <input type="checkbox" class="checkbox-style form-control patientoperationrecord" id="patientoperationrecord_read" name="permission[patientoperationrecord_read]" value="read">
                                <label>Read </label>

                                <input type="checkbox" class="checkbox-style form-control patientoperationrecord" id="patientoperationrecord_update" name="permission[patientoperationrecord_update]" value="update">
                                <label>Update </label>

                                <input type="checkbox" class="checkbox-style form-control patientoperationrecord" id="patientoperationrecord_delete" name="permission[patientoperationrecord_delete]" value="delete">
                                <label>Delete </label>
                            </div>
                        </div>
                    </div>
                </div>
                            
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingFourtySeven">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFourtySeven" aria-expanded="false" aria-controls="collapseFourtySeven">
                          Patient Case Birth Management
                        </a>
                      </h4>
                    </div>
                    <div id="collapseFourtySeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFourtySeven">
                        <div class="panel-body">
                            <div class="form-group form-md-line-input">
                                <label for="form_control_1" class="col-md-4 h4"><strong>Patient Case Birth </strong></label>

                                <input type="checkbox" class="checkbox-style form-control patientcasebirth" id="patientcasebirth_create" name="permission[patientcasebirth_create]" value="create">
                                <label>Create </label>

                                <input type="checkbox" class="checkbox-style form-control patientcasebirth" id="patientcasebirth_read" name="permission[patientcasebirth_read]" value="read">
                                <label>Read </label>

                                <input type="checkbox" class="checkbox-style form-control patientcasebirth" id="patientcasebirth_update" name="permission[patientcasebirth_update]" value="update">
                                <label>Update </label>

                                <input type="checkbox" class="checkbox-style form-control patientcasebirth" id="patientcasebirth_delete" name="permission[patientcasebirth_delete]" value="delete">
                                <label>Delete </label>
                            </div>
                        </div>
                    </div>
                </div>
                            
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingFourtyEight">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFourtyEight" aria-expanded="false" aria-controls="collapseFourtyEight">
                          Patient Medical Histories Management
                        </a>
                      </h4>
                    </div>
                    <div id="collapseFourtyEight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFourtyEight">
                        <div class="panel-body">
                            <div class="form-group form-md-line-input">
                                <label for="form_control_1" class="col-md-4 h4"><strong>Patient Medical Histories </strong></label>

                                <input type="checkbox" class="checkbox-style form-control patientmedicalhistories" id="patientmedicalhistories_create" name="permission[patientmedicalhistories_create]" value="create">
                                <label>Create </label>

                                <input type="checkbox" class="checkbox-style form-control patientmedicalhistories" id="patientmedicalhistories_read" name="permission[patientmedicalhistories_read]" value="read">
                                <label>Read </label>

                                <input type="checkbox" class="checkbox-style form-control patientmedicalhistories" id="patientmedicalhistories_update" name="permission[patientmedicalhistories_update]" value="update">
                                <label>Update </label>

                                <input type="checkbox" class="checkbox-style form-control patientmedicalhistories" id="patientmedicalhistories_delete" name="permission[patientmedicalhistories_delete]" value="delete">
                                <label>Delete </label>
                            </div>
                        </div>
                    </div>
                </div>
                            
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingFourtyNine">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFourtyNine" aria-expanded="false" aria-controls="collapseFourtyNine">
                          Patient Medical Condition Management
                        </a>
                      </h4>
                    </div>
                    <div id="collapseFourtyNine" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFourtyNine">
                        <div class="panel-body">
                            <div class="form-group form-md-line-input">
                                <label for="form_control_1" class="col-md-4 h4"><strong>Patient Medical Condition </strong></label>

                                <input type="checkbox" class="checkbox-style form-control patientmedicalcondition" id="patientmedicalcondition_create" name="permission[patientmedicalcondition_create]" value="create">
                                <label>Create </label>

                                <input type="checkbox" class="checkbox-style form-control patientmedicalcondition" id="patientmedicalcondition_read" name="permission[patientmedicalcondition_read]" value="read">
                                <label>Read </label>

                                <input type="checkbox" class="checkbox-style form-control patientmedicalcondition" id="patientmedicalcondition_update" name="permission[patientmedicalcondition_update]" value="update">
                                <label>Update </label>

                                <input type="checkbox" class="checkbox-style form-control patientmedicalcondition" id="patientmedicalcondition_delete" name="permission[patientmedicalcondition_delete]" value="delete">
                                <label>Delete </label>
                            </div>
                        </div>
                    </div>
                </div>
                            
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingFifty">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFifty" aria-expanded="false" aria-controls="collapseFifty">
                          Notes Management
                        </a>
                      </h4>
                    </div>
                    <div id="collapseFifty" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFifty">
                        <div class="panel-body">
                            <div class="form-group form-md-line-input">
                                <label for="form_control_1" class="col-md-4 h4"><strong>Notes </strong></label>

                                <input type="checkbox" class="checkbox-style form-control notes" id="notes_create" name="permission[notes_create]" value="create">
                                <label>Create </label>

                                <input type="checkbox" class="checkbox-style form-control notes" id="notes_read" name="permission[notes_read]" value="read">
                                <label>Read </label>

                                <input type="checkbox" class="checkbox-style form-control notes" id="notes_update" name="permission[notes_update]" value="update">
                                <label>Update </label>

                                <input type="checkbox" class="checkbox-style form-control notes" id="notes_delete" name="permission[notes_delete]" value="delete">
                                <label>Delete </label>
                            </div>
                        </div>
                    </div>
                </div>
                            
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingFiftyOne">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFiftyOne" aria-expanded="false" aria-controls="collapseFiftyOne">
                          Reminders Management
                        </a>
                      </h4>
                    </div>
                    <div id="collapseFiftyOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFiftyOne">
                        <div class="panel-body">
                            <div class="form-group form-md-line-input">
                                <label for="form_control_1" class="col-md-4 h4"><strong>Reminders </strong></label>

                                <input type="checkbox" class="checkbox-style form-control reminders" id="reminders_create" name="permission[reminders_create]" value="create">
                                <label>Create </label>

                                <input type="checkbox" class="checkbox-style form-control reminders" id="reminders_read" name="permission[reminders_read]" value="read">
                                <label>Read </label>

                                <input type="checkbox" class="checkbox-style form-control reminders" id="reminders_update" name="permission[reminders_update]" value="update">
                                <label>Update </label>

                                <input type="checkbox" class="checkbox-style form-control reminders" id="reminders_delete" name="permission[reminders_delete]" value="delete">
                                <label>Delete </label>

                                <input type="checkbox" class="checkbox-style form-control reminders" id="reminders_updatestatus" name="permission[reminders_updatestatus]" value="delete">
                                <label>Update Status </label>
                            </div>
                        </div>
                    </div>
                </div>
                            
            <div class="form-group form-md-line-input">
                <label for="form_control_1" class="col-md-4 h4"><strong>Test </strong></label>

                <input type="checkbox" class="checkbox-style form-control test" id="test_create" name="permission[test_create]" value="create">
                <label>Create </label>

                <input type="checkbox" class="checkbox-style form-control test" id="test_read" name="permission[test_read]" value="read">
                <label>Read </label>

                <input type="checkbox" class="checkbox-style form-control test" id="test_update" name="permission[test_update]" value="update">
                <label>Update </label>

                <input type="checkbox" class="checkbox-style form-control test" id="test_delete" name="permission[test_delete]" value="delete">
                <label>Delete </label>
            </div>
            <!-- {{ROLEMANAGEMENT_CHECKBOX}} -->
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingFiftyTwo">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFiftyTwo" aria-expanded="false" aria-controls="collapseFiftyTwo">
                          User Activity Log Management
                        </a>
                      </h4>
                    </div>
                    <div id="collapseFiftyTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFiftyTwo">
                        <div class="panel-body">
                            <div class="form-group form-md-line-input">
                                <label for="form_control_1" class="col-md-4 h4"><strong>User Activity Logs </strong></label>

                                <input type="checkbox" class="checkbox-style form-control useractivitylog" id="useractivitylog_read" name="permission[useractivitylog_read]" value="read">
                                <label>Read </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingFiftyThree">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFiftyThree" aria-expanded="false" aria-controls="collapseFiftyThree">
                          Backup/Restore Management
                        </a>
                      </h4>
                    </div>
                    <div id="collapseFiftyThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFiftyThree">
                        <div class="panel-body">
                            <div class="form-group form-md-line-input">
                                <label for="form_control_1" class="col-md-4 h4"><strong>Backup/Restore </strong></label>

                                <input type="checkbox" class="checkbox-style form-control backup" id="backup_read" name="permission[backup_read]" value="read">
                                <label>Read </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
				<label class="control-label">Page Status :</label>
                <select autocomplete="off" class="form-control"  name="page_status" id="page_status">
                    <option value="Yes" <?php if($page_status=="Yes"){ echo ' selected="selected"';} ?>>Yes</option>
                    <option value="No" <?php if($page_status=="No"){ echo ' selected="selected"';} ?>>No</option>
                </select>
			</div>
            <div class="form-group">
                <button type="button" class="btn btn-danger" onclick="window.location='<?php echo ADMIN_URL;?>rolemanagement'">Cancel</button>
                <button type="submit"  class="btn btn-success">Submit</button>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    document.addEventListener("DOMContentLoaded", function(event) {
        $("#checkAll").change(function () {
            $("input:checkbox").prop('checked', $(this).prop("checked"));
        });
    });
</script>