    <?php
$crumb2 = "";
if(isset($tbl_data['rt_id'])&&$tbl_data['rt_id']!=""){
	$rt_id = $tbl_data['rt_id'];
	$rt_name = $tbl_data['rt_name'];
	$rt_per_day_charges = $tbl_data['rt_per_day_charges'];
	$rt_billing_type = $tbl_data['rt_billing_type'];
	$rt_description = $tbl_data['rt_description'];
	$rt_status = $tbl_data['rt_status'];
	$rt_created_by = $tbl_data['rt_created_by'];
	$rt_updated_by = $tbl_data['rt_updated_by'];
	$rt_added = $tbl_data['rt_added'];
	$rt_updated = $tbl_data['rt_updated'];
	$rt_is_deleted = $tbl_data['rt_is_deleted'];
	$rt_clinic_id = $tbl_data['rt_clinic_id'];
    $crumb = "Edit";
    $action = "editRecord/".$tbl_data['rt_id'];
}
else{
	$rt_id = ''; 
	$rt_name = '';
	$rt_per_day_charges = '';
	$rt_billing_type = '';
	$rt_description = '';
	$rt_status = '';
	$rt_created_by = '';
	$rt_updated_by = '';
	$rt_added = '';
	$rt_updated = '';
	$rt_is_deleted = '';
	$rt_clinic_id = '';
    $on_home 		= "No";
    $crumb 			= "Add";
    $action 		= "addRecord";
}
?>
<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo ADMIN_URL;?>">
            <i class="entypo-home"></i>Home
        </a>
    </li>
    <li>
        <a href="<?php echo ADMIN_URL.$this->controller;?>">
            <i></i><?php echo $this->moduleName;?>
        </a>
    </li>
    <li class="active">
        <strong><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></strong>
    </li>
</ol>

<h2><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></h2>
<br />
<div class="panel panel-primary">
    <div class="panel-body">
        <form  id="page_form"
               name="page_form"
               method="post"
               action="<?php echo base_url('manage/'.$this->controller.'/'.$action);?>"
               enctype="multipart/form-data"
               class="validate"
        >
                        <div class="form-group">
                <label class="control-label">Name :<span class="req"> *</span></label>
                <input type="text" name="rt_name" id="rt_name" value="<?php echo $rt_name;?>" class="form-control " placeholder="Name" data-validate="required,maxlength[250]"/>
            </div>

            <div class="form-group">
                <label class="control-label">Charges :<span class="req"> *</span></label>
                <input type="number" name="rt_per_day_charges" id="rt_per_day_charges" value="<?php echo $rt_per_day_charges;?>" class="form-control " placeholder="Charges" data-validate="required,maxlength[250]"/>
            </div>

            <div class="form-group col-md-6 col-xs-12 col-lg-6">
                <label class="control-label" style="margin-right: 15px;">Billing Type :</label>
                <div class="clearfix"></div>
                <input checked="checked" type="radio" name="rt_billing_type" id="rt_billing_type" value="<?php echo ($rt_billing_type == '') ? '1' : $rt_billing_type;?>" />
                <label class="control-label">Daily</label>
                <div class="clearfix"></div>
                <input type="radio" name="rt_billing_type" id="rt_billing_type" value="<?php echo ($rt_billing_type == '') ? '0' : $rt_billing_type;?>" />
                <label class="control-label">Per Admission</label>
            </div>
            <div class="clearfix"></div>


            <div class="form-group" >
                <label class="control-label">Description</label>
				<?php echo $this->ckeditor->editor("rt_description", $rt_description); ?>
			</div>

           

            <div class="form-group">
                <button type="button" class="btn btn-danger" onclick="window.location='<?php echo ADMIN_URL;?>roomtype'">Cancel</button>
                <button id="roomt_submit" type="submit"  class="btn btn-success">Submit</button>
            </div>
        </form>
    </div>
</div>