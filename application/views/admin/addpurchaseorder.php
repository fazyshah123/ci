<?php
$crumb2 = "";
if(isset($tbl_data['a_id'])&&$tbl_data['a_id']!=""){
	$a_id = $tbl_data['a_id'];
	$a_clinic_id = $tbl_data['a_clinic_id'];
	$a_name = $tbl_data['a_name'];
	$a_description = $tbl_data['a_description'];
	$a_added = $tbl_data['a_added'];
	$a_updated = $tbl_data['a_updated'];
	$a_created_by = $tbl_data['a_created_by'];
	$a_updated_by = $tbl_data['a_updated_by'];
	$a_is_deleted = $tbl_data['a_is_deleted'];
	$a_status = $tbl_data['a_status'];
    $p_date = $tbl_data['p_date'];
    $p_remarks = $tbl_data['p_remarks'];
    $crumb = "Edit";
    $action = "editRecord/".$tbl_data['a_id'];
}
else{
	$a_id = '';
	$a_clinic_id = '';
	$a_name = '';
	$a_description = '';
	$a_added = '';
	$a_updated = '';
	$a_created_by = '';
	$a_updated_by = '';
	$a_is_deleted = '';
	$a_status = '';
    $p_date = date('d-M-Y', strtotime('now'));
    $p_remarks = '';
    $on_home 		= "No";
    $crumb 			= "Add";
    $action 		= "addRecord";
}
?>
<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo ADMIN_URL;?>">
            <i class="entypo-home"></i>Home
        </a>
    </li>
    <li>
        <a href="<?php echo ADMIN_URL.$this->controller;?>">
            <i></i><?php echo $this->moduleName;?>
        </a>
    </li>
    <li class="active">
        <strong><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></strong>
    </li>
</ol>

<h2><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></h2>
<br />
<div class="panel panel-primary">
    <div class="panel-body">
        <form  id="page_form"
               name="page_form"
               method="post"
               action="<?php echo base_url('manage/'.$this->controller.'/'.$action);?>"
               enctype="multipart/form-data"
               class="validate"
        >
            <div class="container row">
                <div class="form-group col-md-4">
                    <label class="control-label">Account : <span class="req">*</span></label>
                    <select class="form-control"  name="p_account_id" id="p_account_id">
                        <option value="Select">Select</option>
                    <?php
                    $accounts = $this->SqlModel->getAccountsDropDown();
                    foreach ($accounts as $key => $value) {
                        $selected = '';
                        if ($value['a_name'] == $a_name) {
                        $selected = 'selected';
                    }
                        echo '<option value="'.$value['a_id'].'"'.$selected.'>'.$value["a_name"].'</option>';
                    }
                    ?>
                    </select>
                </div>
                <div class="form-group col-md-4">
                    <label class="control-label">Remarks :</label>
                    <input type="text" name="p_remarks" id="p_remarks" value="<?php echo $p_remarks;?>" class="form-control " placeholder="Remarks" data-validate="maxlength[250]"/>
                </div>
                <div class="form-group col-md-4">
                    <label class="control-label">Date :<span class="req"> *</span></label>
                    <div class="date-and-time">
                        <input required style="width: 100%;float: left;" id="p_date" name="p_date" value="<?=$p_date;?>" type="text" autocomplete="off" class="form-control datepicker" data-template="dropdown" data-default-time="" data-show-meridian="true" data-minute-step="1" data-second-step="1" placeholder="Date"/>
                        <input type="hidden" name="total_items" id="total_items" value="1">
                    </div>
                </div>
    
            </div>

            <div class="container row">
                <div class="form-group col-md-4">
                    <label class="control-label">Suppliers :</label>
                    <select class="form-control"  name="p_supplier_id" id="p_supplier_id">
                        <option value="Select">Select</option>
                    <?php
                    $suppliers = $this->SqlModel->getSuppliersDropDown();
                    foreach ($suppliers as $key => $value) {
                        echo '<option value="'.$value['s_id'].'" data-payable="'.$value['s_payable'].'">'.$value["s_name"].'</option>';
                    }
                    ?>
                    </select>
                </div>
            </div>

            <div class="">
                <h4><strong>Purchased Items</strong></h4>
            </div>
            
            <?php
            $items = $this->SqlModel->getItemsDropDown();
            $itemsList = '';
            foreach ($items as $key => $value) {
                $itemsList .= '<option value="'.$value['i_id'].'" data-price="'.$value['i_unit_price'].'">'.$value["i_name"].'</option>';
            }
            ?>
            <div class="items">
                <div class="container row pd_items">
                    <div class="form-group col-md-3">
                        <label class="control-label">Item :<span class="req">*</span></label>
                        <select class="form-control item_name"  name="pd_item_id-1" id="pd_item_id-1">
                            <option value="Select">Select</option>
                            <?=$itemsList;?>
                        </select>
                    </div>
                    <div class="form-group col-md-2">
                        <label class="control-label">Qty <span class="req">*</span> :</label>
                        <input type="number" name="pd_qty-1" data-item="1" id="pd_qty-1" class="form-control item_qty" placeholder="Qty" data-validate="required,maxlength[250]"/>
                    </div>
                    <div class="form-group col-md-2">
                        <label class="control-label">Buy Price <span class="req">*</span> :</label>
                        <input type="number" step="any" name="pd_unit_price-1" data-item="1" id="pd_unit_price-1" class="form-control item_unit_price" placeholder="Price" data-validate="required,maxlength[250]"/>
                    </div>
                    <div class="form-group col-md-2">
                        <label class="control-label">Discount in Rs. :</label>
                        <input type="number" step="any" name="pd_discount_rs-1" data-item="1" id="pd_discount_rs-1" class="form-control item_discount_rs" placeholder="Discount in Rs."/>
                    </div>
                    <div class="form-group col-md-2">
                        <label class="control-label">Discount in % :</label>
                        <input type="number" step="any" name="pd_discount_percent-1" data-item="1" id="pd_discount_percent-1" class="form-control item_discount_percent" placeholder="Discount in %"/>
                    </div>
                </div>
            </div>

            <div class="container row">
                <a class="btn btn-success pull-right" id="add_item_row" href="javascript:;">Add Item</a>
            </div>

            <div class="form-group hidden">
                <label class="control-label">Status :</label>
                <select class="form-control"  name="a_status" id="a_status">
                    <option value="Enable" <?php if($a_status=="Enable"){ echo ' selected="selected"';} ?>>Enable</option>
                    <option value="Disable" <?php if($a_status=="Disable"){ echo ' selected="selected"';} ?>>Disable</option>
                  </select>
            </div>

            <div class="container row">
                <div class="form-group col-md-4">
                    <label class="control-label">Supplier Prev. Bal. :</label>
                    <input type="number" readonly name="supplier_prev_bal" id="supplier_prev_bal" class="form-control" placeholder="Supplier Prev. Bal."/>
                </div>
                <div class="form-group col-md-2">
                    <label class="control-label">Total Bill :</label>
                    <div class="clearfix"></div>
                    <label class="control-label" style="font-size: 18px;">Rs. <span class="total_bill">0</span></label>
                </div>
                <div class="form-group col-md-2">
                    <label class="control-label">Payable :</label>
                    <input type="number" readonly name="p_total" id="p_total" class="form-control" placeholder="Payable"/>
                    <input type="hidden" name="p_total_amount" id="p_total_amount"/>
                </div>
                <div class="form-group col-md-2">
                    <label class="control-label">Balance :</label>
                    <input type="number" readonly name="p_balance" id="p_balance" class="form-control" placeholder="Balance"/>
                </div>
                <div class="form-group col-md-2">
                    <label class="control-label">Credit Change :</label>
                    <div class="clearfix"></div>
                    <center><input type="checkbox" disabled value="add" name="p_credit_change" id="p_credit_change" class=""/></center>
                </div>
            </div>

            <div class="container row">
                <div class="form-group col-md-4">
                </div>
                <div class="form-group col-md-2">
                    <label class="control-label">Discount in Rs. :</label>
                    <input type="number" name="p_total_discount_rs" id="p_total_discount_rs" class="form-control" placeholder="Discount in Rs."/>
                </div>
                <div class="form-group col-md-2">
                    <label class="control-label">Discount in % :</label>
                    <input type="number" name="p_total_discount_percent" id="p_total_discount_percent" class="form-control" placeholder="Discount in %"/>
                </div>
                <div class="form-group col-md-2">
                    <label class="control-label">Cash Paid :<span class="req">*</span></label>
                    <input required type="number" name="p_paid" id="p_paid" class="form-control" placeholder="Cash Paid"/>
                </div>
                <div class="form-group col-md-2">
                    <label class="control-label">Change :</label>
                    <input type="number" name="p_change_to_return" id="p_change_to_return" class="form-control" placeholder="Change"/>
                </div>
            </div>

            <div class="container row">
                <div class="form-group col-md-4">
                </div>
                <div class="form-group col-md-4">
                </div>
            </div>

            <div class="form-group">
                <button type="button" class="btn btn-danger" onclick="window.location='<?php echo ADMIN_URL;?>purchases'">Cancel</button>
                <button id="purchcesubmit" type="submit"  class="btn btn-success">Submit</button>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    document.addEventListener("DOMContentLoaded", function(event) {
        $("#pd_item_id-1").on("change", function (e) {
            $("#pd_unit_price-1").val($(this).find('option:selected').data('price'));
            calculateSupplierBalance();
            calculatePrice();
            updateCreditChange();
        });
        $("#p_supplier_id").on("change", function (e) {
            $("#supplier_prev_bal").val($(this).find('option:selected').data('payable'));
            calculateSupplierBalance();
            calculatePrice();
            updateCreditChange();
        });
        $("#add_item_row").on("click", function(e) {
            e.preventDefault();
            var item_rows_count = $(".pd_items").length;
            var new_row_id = ++item_rows_count;
            var itemsList = '<?=$itemsList;?>';
            var new_row = '\
                    <div class="container row pd_items">\
                        <div class="form-group col-md-3">\
                            <label class="control-label">Item <span class="req">*</span> :</label>\
                            <select class="form-control item_name"  name="pd_item_id-'+new_row_id+'"" data-item="'+new_row_id+'" id="pd_item_id-'+new_row_id+'">\
                                <option value="Select">Select</option>'+ itemsList +'</select>\
                        </div>\
                        <div class="form-group col-md-2">\
                            <label class="control-label">Qty  <span class="req">*</span>:</label>\
                            <input required type="number" step="any" name="pd_qty-'+new_row_id+'" data-item="'+new_row_id+'" id="pd_qty-'+new_row_id+'" class="form-control item_qty" placeholder="Qty" data-validate="required,maxlength[250]"/>\
                        </div>\
                        <div class="form-group col-md-2">\
                            <label class="control-label">Buy Price <span class="req">*</span> :</label>\
                            <input type="number" step="any" name="pd_unit_price-'+new_row_id+'" data-item="'+new_row_id+'" id="pd_unit_price-'+new_row_id+'" class="form-control item_unit_price" placeholder="Price" data-validate="required,maxlength[250]"/>\
                        </div>\
                        <div class="form-group col-md-2">\
                            <label class="control-label">Discount in Rs. :</label>\
                            <input type="number" step="any" name="pd_discount_rs-'+new_row_id+'" data-item="'+new_row_id+'" id="pd_discount_rs-'+new_row_id+'" class="form-control item_discount_rs" placeholder="Discount in Rs."/>\
                        </div>\
                        <div class="form-group col-md-2">\
                            <label class="control-label">Discount in % :</label>\
                            <input type="number" step="any" name="pd_discount_percent-'+new_row_id+'" data-item="'+new_row_id+'" id="pd_discount_percent-'+new_row_id+'" class="form-control item_discount_percent" placeholder="Discount in %"/>\
                        </div>\
                        <div class="form-group col-md-1">\
                            <div class="clearfix" style="margin-top: 28px;"></div>\
                            <a style="vertical-align: center;" data-item="'+new_row_id+'" id="pd_remove-'+new_row_id+'" class="remove_item" href="javascript:;"><i class="fa fa-times"></i></a>\
                        </div>\
                    </div>';

            $(".items").append(new_row);
            $("#total_items").val(new_row_id);

            $(".remove_item").unbind("click").bind("click", function(e) {
                e.preventDefault();
                var item = $(this).data('item');
                var total_items = $(".pd_items").length;

                $(this).closest('.pd_items').remove();

                for (var i = item+1; i <= total_items; i++) {
                    
                    $("#pd_item_id-"+i).attr('name', 'pd_item_id-'+(i-1)).attr('data-item', (i-1)).attr('id', 'pd_item_id-'+(i-1));
                    $("#pd_remove-"+i).attr('name', 'pd_remove-'+(i-1)).attr('data-item', (i-1)).attr('id', 'pd_remove-'+(i-1));
                    $("#pd_qty-"+i).attr('name', 'pd_qty-'+(i-1)).attr('data-item', (i-1)).attr('id', 'pd_qty-'+(i-1));
                    $("#pd_unit_price-"+i).attr('name', 'pd_unit_price-'+(i-1)).attr('data-item', (i-1)).attr('id', 'pd_unit_price-'+(i-1));
                    $("#pd_discount_rs-"+i).attr('name', 'pd_discount_rs-'+(i-1)).attr('data-item', (i-1)).attr('id', 'pd_discount_rs-'+(i-1));
                    $("#pd_discount_percent-"+i).attr('name', 'pd_discount_percent-'+(i-1)).attr('data-item', (i-1)).attr('id', 'pd_discount_percent-'+(i-1));
                }
                var minus_item = $("#total_items").val();
                $("#total_items").val(--new_row_id);
                calculatePrice();
            });
            $("#pd_item_id-"+new_row_id).select2();
            $("#pd_item_id-"+new_row_id).unbind('change').bind("change", function (e) {
                $("#pd_unit_price-"+new_row_id).val($(this).find('option:selected').data('price'));
            });
            $(".item_qty").unbind('change').bind('change', function(e) {
                alert($(this).val());
            });
            calculatePriceEvent();
        });
        calculatePriceEvent();
        $("#p_paid").unbind("keyup").bind("keyup", function(argument) {
            var payable = $("#p_total").val();
            var cashPaid = $(this).val();
            var total = cashPaid - payable;
            if (total < 0) {
                $("#p_balance").val(total.toString().replace('-', ''));
                $("#p_change_to_return").val('');
            } else {
                $("#p_change_to_return").val(total);
                $("#p_balance").val('');
            }
            calculatePrice();
            updateCreditChange();
            calculateSupplierBalance();
        });
        //  purchase validations
        $("#purchcesubmit").on('click', function(e) {
            e.preventDefault();
            var p_account_i = $("#p_account_id").val();
            var p_supplier_i = $("#p_supplier_id").val();
            var pd_item_i = $("#pd_item_id-1").val();
            
            if (p_account_i == 'Select') {
                alert('Please select an Account');
                return;
            }
            if (p_supplier_i == 'Select') {
                alert('Please select a Supplier');
                return;
            }
            if (pd_item_i == 'Select') {
                alert('Please select an Item');
                return;
            }
            $("#page_form").submit();
        });
    });
    function calculatePriceEvent() {
        $(".item_qty, .item_unit_price, .item_discount_percent, .item_discount_rs, #p_total_discount_rs, #p_total_discount_percent").unbind('keyup').bind('keyup', function(e) {
            var item = $(this).data('item');
            var i = $("#pd_item_id-"+item).val();
            var qty = $("#pd_qty-"+item).val();
            var price = $("#pd_unit_price-"+item).val();
            if (price == '' || price == 0 || i == 'Select' || qty == '' || qty == '0') {
                return;
            } else {
                calculatePrice();
                updateCreditChange();
                calculateSupplierBalance();
            }
        });
    }
    function calculatePrice() {
        var total_bill = 0;
        var item_rows_count = $(".pd_items").length;
        for (var i = 1; i <= item_rows_count; i++) {
            total = 0;
            if (!($("#pd_qty-"+i).length)) {
                i++;
            }
            q = $("#pd_qty-"+i).val();
            p = $("#pd_unit_price-"+i).val();
            dr = $("#pd_discount_rs-"+i).val();
            dp = $("#pd_discount_percent-"+i).val();
            if (q == undefined) {
            } else {
                total = (q*p);
                if (dr != '') {
                    total -= dr;
                }
                if (dp != '' && dp != '0') {
                    total -= (total * (dp/100));
                }
                total_bill += total;
            }
        }

        var tdr = $("#p_total_discount_rs").val();
        var tdp = $("#p_total_discount_percent").val();
        var suppliers_prev_bal = $("#supplier_prev_bal").val();

        if (tdr != '') {
            total_bill -= tdr;
        }

        if (tdp) {
            total_bill -= (total_bill * (tdp/100));
        }
        
        $("#p_total").val(parseInt(suppliers_prev_bal) + parseInt(total_bill));
        $(".total_bill").text(total_bill);
        $("#p_total_amount").val(total_bill);
        updateCreditChange();
        calculateSupplierBalance();
    }
    function calculateSupplierBalance() {
        var suppliers_prev_bal = $("#supplier_prev_bal").val();
        var total_bill = $(".total_bill").text();
        $("#p_total").val(parseInt(suppliers_prev_bal) + parseInt(total_bill));
        updateCreditChange();
    }
    function updateCreditChange() {
        var change = $("#p_change_to_return").val();
        
        if (change > 0) {
            $("#p_credit_change").removeAttr('disabled');
        } else {
            $("#p_credit_change").addAttr('disabled');
        }
    }
</script>