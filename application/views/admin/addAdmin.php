 <?php
 if($alert=="error"||$alert=="edit"){
	$full_name = $tbl_data['full_name'];
	$user_name = $tbl_data['user_name'];
	$email = $tbl_data['email'];
	$pwd = $tbl_data['pwd'];
	$pwd2 = $tbl_data['pwd'];
	$status = $tbl_data['status']; 
	$user_role_id = $tbl_data['user_role_id']; 
	$clinic_id = $tbl_data['clinic_id']; 
	$salary = $tbl_data['salary'];
	$clinic_owner = $tbl_data['clinic_owner']; 
 }
 else{
	$full_name="";
	$user_name = "";
	$email = "";
	$pwd = "";
	$pwd2 = "";
	$user_role_id = "";
	$clinic_id = "";
	$salary = "";
	$clinic_owner = "";
	$status = "Enable";
 }
if($alert=="error")
{
$pwd = "";
$pwd2 = "";	
}

 
 if($alert=="edit")
 {
	$crumb = "Edit";
	$action = "editRecord/".$tbl_data['id']; 
 }
 else{
	$crumb = "Add";
	$action = "addRecord"; 
 }
 ?>
<ol class="breadcrumb bc-3">
<li><a href="<?php echo ADMIN_URL;?>"><i class="entypo-home"></i>Home</a></li>
<li><a href="<?php echo ADMIN_URL.$this->controller;?>"><i></i><?php echo $this->moduleName;?></a></li>
<li class="active"><strong><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></strong></li>
</ol>
     
     
     
     
<?php if($alert=="success") { ?>
<div class="row alertrow">
	<div class="col-md-12">
    <button class="close alertBox" data-dismiss="alert">x</button>
		<div class="alert alert-success"><strong>Success!</strong> Settings saved sucessfully.</div>
	</div>
</div>
<?php } if($alert=="error"||$editerror=="editerror") { ?>
<div class="row alertrow">
	<div class="col-md-12">
     <button class="close alertBox" data-dismiss="alert">x</button>
		<div class="alert alert-danger"><strong>Error!!</strong> The user name or email you specified is already exist, please use different user name or email.</div>
	</div>
</div>
<?php } if($alert=="perror") { ?>
<div class="row alertrow">
	<div class="col-md-12">
     <button class="close alertBox" data-dismiss="alert">x</button>
		<div class="alert alert-danger"><strong>Error!!</strong> Unable to change the password, please provide the correct current password.</div>
	</div>
</div>

<?php } ?>     
     
     

<h2><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></h2>
<br />


<div class="panel panel-primary">

	
	
	<div class="panel-body">
	
		<form role="form" id="admin_form"   name="admin_form" method="post" action="<?php echo ADMIN_URL.$this->controller.'/'.$action;?>" >
			
			<div class="form-group">
				<label class="control-label">Name <span style="color: red">*</span> :</label>
				<input type="text" class="form-control" name="full_name" id="full_name" value="<?php echo $full_name;?>" placeholder="Example: John Smith" />
			</div>

            <div class="form-group">
				<label type="text" class="control-label">Role <span style="color: red">*</span> :</label>
				 <select autocomplete="off" class="form-control"  name="role_id" id="role_id">
	                  <?php
	                  	$where = [];
	                  	if ($this->session->userdata('user_role_id') != '1') {
	                  		$where['clinic_id'] = $this->session->userdata('clinic_id');
	                  	}
	                  	$where['is_deleted'] = '0';
	                  	$user_role = $this->SqlModel->getRecords('page_id,role_title', 'user_role', 'role_title', 'ASC', $where);
	                  	foreach ($user_role as $key => $value): ?>
	                  		<?php if ($value['page_id'] != '1') {
	                  			?>
		                    	<option value="<?= $value['page_id']; ?>" <?php if($value['page_id']==$user_role_id){ echo ' selected="selected"';} ?>><?= $value['role_title']; ?></option>
	                  <?php } endforeach ?>
                  </select>
			</div>

			<?php 
        if($this->SqlModel->checkPermissions('clinics','create')==true)
        {?>
			<div class="clearfix"></div>
			<div class="form-group">
				<label class="control-label">User Name :<span style="color: red"> *</span></label>
				<input type="text" class="form-control uname"   name="user_name" id="user_name" value="<?php echo $user_name;?>" placeholder="john.smith" />
			</div>
			
			<div class="clearfix"></div>

 			<div class="form-group">
				<label class="control-label" for="clinic_owner">Clinic Owner :</label>     
                <input type="checkbox" class="form-control" name="clinic_owner" id="clinic_owner" value="1" <?php ($clinic_owner == '1') ? ' checked' : '';?> />
			</div>
			
			<div class="clearfix"></div>
            <div class="form-group">
				<label class="control-label">Clinic :</label>
				 <select autocomplete="off" class="form-control"  name="clinic_id" id="clinic_id">
	                  <?php 
	                  	foreach ($tbl_data['clinics'] as $key => $value): ?>
	                    	<option value="<?= $value['clinic_id']; ?>" <?php if($value['clinic_id']==$clinic_id){ echo ' selected="selected"';} ?>><?= $value['clinic_name']; ?></option>
	                <?php endforeach ?>
                </select>
 			</div>
			
		<?php } ?>
			
			<div class="form-group">
				<label class="control-label">Email :<span style="color: red"> *</span></label>
                
                <input type="email" class="form-control" name="email" id="email" value="<?php echo $email;?>" placeholder="john_smith@dummyemail.com" />
			</div>
			
			<div class="form-group">
				<label class="control-label">Salary :<span style="color: red"> *</span></label>
                
                <input required type="text" class="form-control salary"   name="salary" id="salary" value="<?php echo $salary;?>" placeholder="Salary" />
			</div>
			
			<div class="form-group">
				<label class="control-label">User Name :<span style="color: red"> *</span></label>
                
                <input required type="text" class="form-control uname"   name="user_name" id="user_name" value="<?php echo $user_name;?>" placeholder="john.smith" />
			</div>
			
			<div class="form-group">
				<label class="control-label">Password <span style="color: red">*</span> :</label>
               
                <input type="password" class="form-control"   name="pwd" id="pwd" value="<?php echo $pwd;?>" placeholder="Password " />
			</div>
			
			<div class="form-group">
				<label class="control-label">Confirm Password <span style="color: red">*</span> :</label>
               
                <input type="password" class="form-control"   name="pwd2" id="pwd2" value="<?php echo $pwd2;?>" placeholder="Password " />
			</div>
			
			<div class="form-group <?php echo (isset($tbl_data['id'])&&(int)$tbl_data['id']===(int)1) ? 'hidden' : '';?> " >
				<label class="control-label">Status :</label>
                
                  <select class="form-control"  name="status" id="status">
                    <option value="Enable" <?php if($status=="Enable"){ echo ' selected="selected"';} ?>>Enable</option>
                    <option value="Disable" <?php if($status=="Disable"){ echo ' selected="selected"';} ?>>Disable</option>
                  </select>
			</div>
            
            
           
			<div class="form-group">
				
				<button type="button" class="btn btn-danger" onclick="window.location='<?php echo ADMIN_URL.$this->controller;?>'">Cancel</button>
                <button type="submit" class="btn btn-success">Submit</button>
			</div>
		
		</form>
	
 
</div>
  
  </div>


