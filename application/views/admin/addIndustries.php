<?php
$crumb2 = "";
if(isset($tbl_data['i_id'])&&$tbl_data['i_id']!=""){
	$i_id = $tbl_data['i_id'];
	$i_name = $tbl_data['i_name'];
	$i_parent_id = $tbl_data['i_parent_id'];
	$i_is_deleted = $tbl_data['i_is_deleted'];
	$i_status = $tbl_data['i_status'];
    $crumb = "Edit";
    $action = "editRecord/".$tbl_data['i_id'];
}
else{
	$i_id = '';
	$i_name = '';
	$i_parent_id = '';
	$i_is_deleted = '';
	$i_status = '';
    $on_home 		= "No";
    $crumb 			= "Add";
    $action 		= "addRecord";
}
?>
<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo ADMIN_URL;?>">
            <i class="entypo-home"></i>Home
        </a>
    </li>
    <li>
        <a href="<?php echo ADMIN_URL.$this->controller;?>">
            <i></i><?php echo $this->moduleName;?>
        </a>
    </li>
    <li class="active">
        <strong><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></strong>
    </li>
</ol>

<h2><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></h2>
<br />
<div class="panel panel-primary">
    <div class="panel-body">
        <form  id="page_form"
               name="page_form"
               method="post"
               action="<?php echo base_url('manage/'.$this->controller.'/'.$action);?>"
               enctype="multipart/form-data"
               class="validate"
        >
                        <div class="form-group">
                <label class="control-label">i_name :</label>
                <input type="text" name="i_name" id="i_name" value="<?php echo $i_name;?>" class="form-control " placeholder="i_name" data-validate="required,maxlength[250]"/>
            </div>

            <div class="form-group">
                <label class="control-label">i_parent_id :</label>
                <input type="text" name="i_parent_id" id="i_parent_id" value="<?php echo $i_parent_id;?>" class="form-control " placeholder="i_parent_id" data-validate="required,maxlength[250]"/>
            </div>

            <div class="form-group">
                <label class="control-label">i_status :</label>
                <input type="text" name="i_status" id="i_status" value="<?php echo $i_status;?>" class="form-control " placeholder="i_status" data-validate="required,maxlength[250]"/>
            </div>



            <div class="form-group">
                <button type="button" class="btn btn-danger" onclick="window.location='<?php echo ADMIN_URL;?>pages'">Cancel</button>
                <button type="submit"  class="btn btn-success">Submit</button>
            </div>
        </form>
    </div>
</div>