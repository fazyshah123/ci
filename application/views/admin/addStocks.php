<?php
$crumb2 = "";
if(isset($tbl_data['s_id'])&&$tbl_data['s_id']!=""){
	$s_id = $tbl_data['s_id'];
	$s_clinic_id = $tbl_data['s_clinic_id'];
	$s_item_id = $tbl_data['s_item_id'];
	$s_qty = $tbl_data['s_qty'];
	$s_min_qty = $tbl_data['s_min_qty'];
	$s_added = $tbl_data['s_added'];
	$s_updated = $tbl_data['s_updated'];
	$s_created_by = $tbl_data['s_created_by'];
	$s_updated_by = $tbl_data['s_updated_by'];
	$s_is_deleted = $tbl_data['s_is_deleted'];
	$s_status = $tbl_data['s_status'];
    $crumb = "Edit";
    $action = "editRecord/".$tbl_data['s_id'];
}
else{
	$s_id = '';
	$s_clinic_id = '';
	$s_item_id = '';
	$s_qty = '';
	$s_min_qty = '';
	$s_added = '';
	$s_updated = '';
	$s_created_by = '';
	$s_updated_by = '';
	$s_is_deleted = '';
	$s_status = '';
    $on_home 		= "No";
    $crumb 			= "Add";
    $action 		= "addRecord";
}
?>
<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo ADMIN_URL;?>">  
            <i class="entypo-home"></i>Home
        </a>
    </li>
    <li>
        <a href="<?php echo ADMIN_URL.$this->controller;?>">
            <i></i><?php echo $this->moduleName;?>
        </a>
    </li>
    <li class="active">
        <strong><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></strong>
    </li>
</ol>

<h2><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></h2>
<br />
<div class="panel panel-primary">
    <div class="panel-body">
        <form  id="page_form"
               name="page_form"
               method="post"
               action="<?php echo base_url('manage/'.$this->controller.'/'.$action);?>"
               enctype="multipart/form-data"
               class="validate"
        >

            <div class="form-group">
                <label class="control-label">Item <span style="color: red">*</span> :</label>
                <?php
                    if ($crumb == 'Edit') {
                        $disabled = 'disabled';
                    }
                ?>
                <select class="form-control" required name="s_item_id" <?=$disabled;?> id="s_item_id">
                    <option value="select1">Select</option>
                <?php
                $items = $this->SqlModel->getItemsDropDown();
                foreach ($items as $key => $value) {
                    echo '<option value="'.$value['i_id'].'" '.($value['i_id'] == $s_item_id ? 'selected' : '').'>'.$value["i_name"].'</option>';
                }
                ?>
                </select>
            </div>

            <div class="form-group">
                <label class="control-label">Quantity :<span style="color: red"> *</span></label>
                <input type="number" name="s_qty" id="s_qty" value="<?php echo $s_qty;?>" class="form-control " placeholder="Quantity" data-validate="required,maxlength[250]"/>
            </div>

            <div class="form-group">
                <label class="control-label">Min. Quantity : <span style="color: red"> *</span></label>
                <input required type="number" name="s_min_qty" id="s_min_qty" value="<?php echo $s_min_qty;?>" class="form-control " placeholder="Min. Quantity" data-validate="maxlength[250]"/>
            </div>

            <div class="form-group">
                <button type="button" class="btn btn-danger" onclick="window.location='<?php echo ADMIN_URL;?>stocks'">Cancel</button>
                <button type="submit" id="stocksubmit" class="btn btn-success">Submit</button>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    document.addEventListener("DOMContentLoaded", function(event) {
        $("#stocksubmit").on('click', function(e){
            e.preventDefault();
            var s_item_i = $("#s_item_id").val();
            
            if (s_item_i == 'select1') {
                alert('Please select an Item');
                return;
            }
            $("#page_form").submit();
        });
    });
</script>