<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
	<style>
		body {
			font-family: arial
		}
		.hospital-name {
			font-size: 20px;
			padding: 3px;
			font-family: arial
		}
		.address, .tagline {
			font-family: arial;
			font-size: 13px;
		}
		.tagline {
			padding-top: 4px;
		}
		.format {
			font-size: 15px;
		}
		tr {
			height: 21px;
		}
		table {
			height: 332px;
			width: 730px;
			margin-left: 20px;
		}
		th {
			font-size: 15px;
		}
		table, td, th {
			border: 1px solid #000;
			border-collapse: collapse;
		}
		.number {
			text-align: right;
			padding-right: 10px;
		}
	</style>
	<div class="recipt" style="width: 796px;">
		<div class="row" style="height: 65px;">
			<div style="width: 95px;float:left">
				<?php 
				$logo = './assets/frontend/images/logo/'.$this->SqlModel->getSingleField('logo','site_settings',array('clinic_id'=>$this->session->userdata('clinic_id')));
				if(file_exists($logo)) {
				?>
					<img style="padding: 3px;" src="<?php echo $this->imagethumb->image($logo,95, 0);?>" alt="Admin Logo" />
	            <?php } ?>
			</div>
			<div class="col-md-8">
				<center><div class="hospital-name"><i><strong>Mohammadi Hospital</strong></i></div></center>
				<div class="address"><center>Plot No. C-53, Shah Jahan Ave, Block 17, Fedral B. Area, Karachi. Tel: +92-31234567</center></div>
				<div class="tagline"><strong><center>Preventation is better than Cure</center></strong></div>
			</div>
		</div>
		<hr>
		<div class="row" style="padding-left: 20px;">
			<div class="format" style="width: 200px;float:left;"><strong>MR NO. <span style="padding-left: 20px;">1-1002</span></strong></div>
			<div class="format" style="width: 130px; float:left;"><strong><span style="padding-left: 20px;">Shafi Uddin</span></strong></div>
			<div class="format" style="width: 130px; float:right;padding-right: 20px;"><strong><span>Male/65 Years</span></strong></div>
		</div>
		<div style="clear:both;padding-top: 10px;"></div>
		<div class="row" style="padding-left: 20px;">
			<div class="format" style="width: 200px;float:left;"><strong>Date: <span style="padding-left: 20px;">4-May-19</span></strong></div>
			<div class="format" style="width: 300px; float:left;"><strong><span style="padding-left: 20px;">Consultant: Dr. Sarwari</span></strong></div>
		</div>
		<div style="clear:both;padding-top: 10px;"></div>
		<table>
			<thead>
				<tr>
					<th>S NO.</th>
					<th>Description</th>
					<th>Amount</th>
					<th>Discount</th>
					<th>Total</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="number">1</td>
					<td>Operation</td>
					<td>20,000 Rs.</td>
					<td>2,000 Rs.</td>
					<td>18,000 Rs.</td>
				</tr>
				<tr>
					<td class="number">2</td>
					<td>Room Charges</td>
					<td>5,000 Rs.</td>
					<td>1,000 Rs.</td>
					<td>4,000 Rs.</td>
				</tr>
				<tr>
					<td class="number">3</td>
					<td>Consultant Fee</td>
					<td>500 Rs.</td>
					<td>0 Rs.</td>
					<td>500 Rs.</td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td><strong>Grand Total</strong></td>
					<td>22,500 Rs.</td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td><strong>Discount</strong></td>
					<td>500 Rs.</td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td><strong>Total</strong></td>
					<td>22,000 Rs.</td>
				</tr>
			</tbody>
		</table>
		<hr>
		<div class="row" style="">
			<div class="col-md-8">
				<div class="address" style="margin-bottom: 5px;"><center>Plot No. C-53, Shah Jahan Ave, Block 17, Fedral B. Area, Karachi. Tel: +92-31234567</center></div>
			<div class="format" style="width: 200px;float:left;font-size:13px;"><strong>Developed By BitCloudGlobal</strong></div>
			<div class="format" style="width: 130px; float:right;padding-right: 20px;font-size:13px;"><strong>www.bitcloudglobal.com</strong></div>
			</div>
		</div>
	</div>
</body>
</html>