<?php
$crumb2 = "";
if(isset($tbl_data['p_id'])&&$tbl_data['p_id']!=""){
	$p_id = $tbl_data['p_id'];
	$p_name = $tbl_data['p_name'];
	$p_parent_id = $tbl_data['p_parent_id'];
	$p_is_deleted = $tbl_data['p_is_deleted'];
	$p_status = $tbl_data['p_status'];
    $crumb = "Edit";
    $action = "editRecord/".$tbl_data['p_id'];
}
else{
	$p_id = '';
	$p_name = '';
	$p_parent_id = '';
	$p_is_deleted = '';
	$p_status = '';
    $on_home 		= "No";
    $crumb 			= "Add";
    $action 		= "addRecord";
}
?>
<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo ADMIN_URL;?>">
            <i class="entypo-home"></i>Home
        </a>
    </li>
    <li>
        <a href="<?php echo ADMIN_URL.$this->controller;?>">
            <i></i><?php echo $this->moduleName;?>
        </a>
    </li>
    <li class="active">
        <strong><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></strong>
    </li>
</ol>

<h2><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></h2>
<br />
<div class="panel panel-primary">
    <div class="panel-body">
        <form  id="page_form"
               name="page_form"
               method="post"
               action="<?php echo base_url('manage/'.$this->controller.'/'.$action);?>"
               enctype="multipart/form-data"
               class="validate"
        >
                        <div class="form-group">
                <label class="control-label">p_name :</label>
                <input type="text" name="p_name" id="p_name" value="<?php echo $p_name;?>" class="form-control " placeholder="p_name" data-validate="required,maxlength[250]"/>
            </div>

            <div class="form-group">
                <label class="control-label">p_parent_id :</label>
                <input type="text" name="p_parent_id" id="p_parent_id" value="<?php echo $p_parent_id;?>" class="form-control " placeholder="p_parent_id" data-validate="required,maxlength[250]"/>
            </div>

            <div class="form-group">
                <label class="control-label">p_status :</label>
                <input type="text" name="p_status" id="p_status" value="<?php echo $p_status;?>" class="form-control " placeholder="p_status" data-validate="required,maxlength[250]"/>
            </div>



            <div class="form-group">
                <button type="button" class="btn btn-danger" onclick="window.location='<?php echo ADMIN_URL;?>pages'">Cancel</button>
                <button type="submit"  class="btn btn-success">Submit</button>
            </div>
        </form>
    </div>
</div>