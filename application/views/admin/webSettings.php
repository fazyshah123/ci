<ol class="breadcrumb bc-3">
	<li><a href="<?php echo ADMIN_URL;?>"><i class="entypo-home"></i>Home</a></li>
	<li class="active"><strong>Website Settings</strong></li>
</ol>
<?php if($alert=="success") { ?>
<div class="row alertrow">
	<div class="col-md-12">
    <button class="close alertBox" data-dismiss="alert">x</button>
		<div class="alert alert-success"><strong>Success!</strong> Website Settings saved sucessfully.</div>
	</div>
</div>
<?php } if($alert=="error") { ?>
<div class="row alertrow">
	<div class="col-md-12">
     <button class="close alertBox" data-dismiss="alert">x</button>
		<div class="alert alert-danger"><strong>Error!!</strong>  Error occurred while saving the record, please try again.</div>
	</div>
</div>
<?php }if($alert=="permerror") { ?>
  <div class="row alertrow">
    <div class="col-md-12"> 
     <button class="close alertBox" data-dismiss="alert">x</button>
        <div class="alert alert-danger"><strong>Error!!</strong> Seems like you don't have permission, please try again.</div>
    </div>
</div>
<?php }?>
<h2>Website Settings</h2>
<br />
<div class="panel panel-primary">
	<div class="panel-body">
		 <form  id="slider_form" name="slider_form" method="post" action="<?php echo base_url();?>manage/home/savewebsettings" enctype="multipart/form-data" class="validate" >
			<div class="form-group">
				<label class="control-label">Name :<span class="req"> *</span> </label>
                <input required type="text" data-validate="required" name="website_title" id="website_title" value="<?php echo $web['website_title'];?>" class="form-control" placeholder="Name" />
			</div>
			<div class="form-group">
				<label class="control-label">Tag Line :<span class="req"> *</span></label>
                <input required type="text" data-validate="required" name="tag_line" id="tag_line" value="<?php echo $web['tag_line'];?>" class="form-control" placeholder="Tag Line" />
			</div>
			<div class="form-group">
				<label class="control-label">Short Address :</label>
                <input type="text" data-validate="required" name="address" id="address" value="<?php echo $web['address'];?>" class="form-control" placeholder="Short Address" />
			</div>
			<div class="form-group">
				<label class="control-label">Full Address :</label>
                <input type="text" data-validate="required" name="full_address" id="full_address" value="<?php echo $web['full_address'];?>" class="form-control" placeholder="Full Address" />
			</div>
			<div class="form-group">
				<label class="control-label">Contact No :<span class="req"> *</span></label>
                <input type="number" data-validate="required" name="tel" id="tel" value="<?php echo $web['tel'];?>" class="form-control" placeholder="Contact No" />
			</div>
            <div class="form-group">
				<label class="control-label">Website URL :</label>
                <input type="text"  name="website_url" id="website_url" value="<?php echo $web['website_url'];?>" class="form-control" placeholder="www.mywebsite.com" />
			</div>
            <div class="form-group">
				<label class="control-label">Logo :</label>
                <input type="file" name="uploadfile" id="uploadfile" />
                <br/> <span>Image Type:</strong> JPEG / PNG</span><br/>
                <br/>
                <a href="<?php echo ($web['logo']!="" && file_exists('./assets/frontend/images/logo/'.$web['logo'])) ? FRONTEND_ASSETS.'images/logo/'.$web['logo'] : FRONTEND_ASSETS.'images/no_image.jpg';?>" class="fancybox">  
                	<img width="180"  src="<?php echo ($web['logo']!="" && file_exists('./assets/frontend/images/logo/'.$web['logo'])) ? $this->imagethumb->image('./assets/frontend/images/logo/'.$web['logo'],180,0) : FRONTEND_ASSETS.'images/no_image.jpg';?>" />
               	</a>
			</div>
			<div class="form-group">
				<button type="button" class="btn btn-danger" onclick="window.location='<?php echo ADMIN_URL;?>'">Cancel</button>
                <button id="websitesetting_submit" type="submit" class="btn btn-success">Submit</button>
			</div>
		</form>
  	</div>
</div>