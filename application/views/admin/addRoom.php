<?php
$crumb2 = "";
if(isset($tbl_data['ro_id'])&&$tbl_data['ro_id']!=""){
	$ro_id = $tbl_data['ro_id'];
	$ro_number = $tbl_data['ro_number'];
	$ro_room_type_id = $tbl_data['ro_room_type_id'];
	$ro_is_booked = $tbl_data['ro_is_booked'];
	$ro_status = $tbl_data['ro_status'];
	$ro_created_by = $tbl_data['ro_created_by'];
	$ro_updated_by = $tbl_data['ro_updated_by'];
	$ro_added = $tbl_data['ro_added'];
	$ro_updated = $tbl_data['ro_updated'];
	$ro_is_deleted = $tbl_data['ro_is_deleted'];
	$ro_clinic_id = $tbl_data['ro_clinic_id'];
    $crumb = "Edit";
    $action = "editRecord/".$tbl_data['ro_id'];
}
else{
	$ro_id = '';
	$ro_number = '';
	$ro_room_type_id = '';
	$ro_is_booked = '';
	$ro_status = '';
	$ro_created_by = '';
	$ro_updated_by = '';
	$ro_added = '';
	$ro_updated = ''; 
	$ro_is_deleted = '';
	$ro_clinic_id = '';
    $on_home 		= "No";
    $crumb 			= "Add";
    $action 		= "addRecord";
}
?>
<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo ADMIN_URL;?>">
            <i class="entypo-home"></i>Home
        </a>
    </li>
    <li>
        <a href="<?php echo ADMIN_URL.$this->controller;?>">
            <i></i><?php echo $this->moduleName;?>
        </a>
    </li>
    <li class="active">
        <strong><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></strong>
    </li>
</ol>

<h2><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></h2>
<br />
<div class="panel panel-primary">
    <div class="panel-body">
        <form  id="page_form"
               name="page_form"
               method="post"
               action="<?php echo base_url('manage/'.$this->controller.'/'.$action);?>"
               enctype="multipart/form-data"
               class="validate"
        >
                        <div class="form-group">
                <label class="control-label">Number :<span style="color: red"> *</span></label>
                <input type="text" name="ro_number" id="ro_number" value="<?php echo $ro_number;?>" class="form-control " placeholder="Room Number" data-validate="required,maxlength[250]"/>
            </div>

            <div class="form-group">
                <label class="control-label">Room Type :<span style="color: red"> *</span></label>
                <select class="form-control room_type_dropdown" name="ro_room_type_id" id="ro_room_type_id">
                    <option value="Select">Select</option>
                <?php
                $roomtype = $this->SqlModel->getRoomTypeDropDown();
                foreach ($roomtype as $key => $value) {
                    if($value['rt_id']==$ro_room_type_id) { 
                        $selected = ' selected="selected"';
                    } else {
                        $selected = '';
                    }
                    echo '<option '.$selected.' value="'.$value['rt_id'].'">'.$value["rt_name"].'</option>';
                }
                ?>
                </select>
            </div>

            <div class="form-group col-md-6 col-xs-12 col-lg-6 hidden">
                <label class="control-label">Status :</label>
                <select class="form-control" name="ro_status" id="ro_status">
                    <option value="Enable" <?php if($ro_status=="Enable"){ echo ' selected="selected"';} ?>>Enable</option>
                    <option value="Disable" <?php if($ro_status=="Disable"){ echo ' selected="selected"';} ?>>Disable</option>
                </select>
            </div>



            <div class="form-group">
                <button type="button" class="btn btn-danger" onclick="window.location='<?php echo ADMIN_URL;?>room'">Cancel</button>
                <button type="submit" id="roomsubmit" class="btn btn-success">Submit</button>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    document.addEventListener("DOMContentLoaded", function(event) {
        $("#roomsubmit").on('click', function(e){
            e.preventDefault();
            var ro_room_typeid = $("#ro_room_type_id").val();
            if ( ro_room_typeid == 'Select') {
                alert('Please select a Room');
                return;
            }
            $("#page_form").submit();
        });
    });
</script>