<?php
$crumb2 = "";
if(isset($tbl_data['ea_id'])&&$tbl_data['ea_id']!=""){
	$ea_id = $tbl_data['ea_id'];
	$ea_clinic_id = $tbl_data['ea_clinic_id'];
	$ea_user_id = $tbl_data['ea_user_id'];
	$ea_checkin = $tbl_data['ea_checkin'];
	$ea_checkout = $tbl_data['ea_checkout'];
	$ea_attendance = $tbl_data['ea_attendance'];
	$ea_absent_reason = $tbl_data['ea_absent_reason'];
	$ea_date = $tbl_data['ea_date'];
	$ea_added = $tbl_data['ea_added'];
	$ea_updated = $tbl_data['ea_updated'];
	$ea_created_by = $tbl_data['ea_created_by'];
	$ea_updated_by = $tbl_data['ea_updated_by'];
	$ea_is_deleted = $tbl_data['ea_is_deleted'];
	$ea_status = $tbl_data['ea_status'];
    $crumb = "Edit";
    $action = "editRecord/".$tbl_data['ea_id'];
}
else{
	$ea_id = '';
	$ea_clinic_id = '';
	$ea_user_id = '';
	$ea_checkin = '';
	$ea_checkout = '';
	$ea_attendance = '';
	$ea_absent_reason = '';
	$ea_date = '';
	$ea_added = '';
	$ea_updated = '';
	$ea_created_by = '';
	$ea_updated_by = '';
	$ea_is_deleted = '';
	$ea_status = '';
    $on_home 		= "No";
    $crumb 			= "Add";
    $action 		= "addRecord";
}
?>
<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo ADMIN_URL;?>">
            <i class="entypo-home"></i>Home
        </a>
    </li>
    <li>
        <a href="<?php echo ADMIN_URL.$this->controller;?>">
            <i></i><?php echo $this->moduleName;?>
        </a>
    </li>
    <li class="active">
        <strong><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></strong>
    </li>
</ol>

<h2><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></h2>
<br />
<div class="panel panel-primary">
    <div class="panel-body">
        <form  id="page_form"
               name="page_form"
               method="post"
               action="<?php echo base_url('manage/'.$this->controller.'/'.$action);?>"
               enctype="multipart/form-data"
               class="validate"
        >

            <div class="form-group" style="margin-bottom: 10px;">
                <label class="control-label">User <span class="req">*</span> :</label>
                <select required class="form-control checkout_user"  name="ea_user_id" id="ea_user_id">
                    <option value="usrsele">Select</option>
                <?php
                $users = $this->SqlModel->getAdminUsersDropDown();
                foreach ($users as $key => $value) {
                    $selected = '';
                    if ($value['id'] == $ea_user_id) {
                        $selected = 'selected';
                    }
                    echo '<option value="'.$value['id'].'" '.$selected.'>'.$value["full_name"].'</option>';
                }
                ?>
                </select>
            </div>

            <div class="form-group">
                <label class="control-label">Date : <span class="req"> *</span></label>
                <div class="date-and-time">
                    <input required style="width: 100%;margin-bottom: 10px;" id="ea_date" name="ea_date" type="text" autocomplete="off" class="form-control datepicker" data-template="dropdown" value="<?=date('m/d/Y');?>" data-default-time="" data-show-meridian="true" data-minute-step="1" data-second-step="1" placeholder="Date"/>
                </div>
            </div>
            <?php if ($crumb != "Edit") { ?>
            <div class="form-group col-md-6 col-xs-12 col-lg-6">
                <label class="control-label" style="margin-right: 15px;">Attendance :</label>
                <div class="clearfix"></div>
                <input type="radio" checked name="attendance_type" class="attendance_type" id="attendance_type_in" value="in" />
                <label class="control-label">Check-In</label>
                <div class="clearfix"></div>
                <input type="radio" name="attendance_type" class="attendance_type" id="attendance_type_out" value="out" />
                <label class="control-label">Check-Out</label>
            </div>
            <?php } ?>
            <div class="clearfix"></div>

            <div class="form-group">
                <label class="control-label">Check-In : <span class="req"> *</span></label>

                <div class="date-and-time">
                    <input required style="width: 100%;margin-bottom: 10px;" value="<?=($crumb!="Add"?$ea_checkin:date('h:i A'));?>" id="ea_checkin" name="ea_checkin" type="text" autocomplete="off" class="form-control timepicker" data-template="dropdown" data-default-time="" data-show-meridian="true" data-minute-step="1" data-second-step="1" placeholder="Check-In"/>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label">Check-Out :</label>
                <div class="date-and-time">
                    <input style="width: 100%;margin-bottom: 10px;" <?=($crumb!="Edit"?"disabled":"");?> value="<?=($crumb!="Add"?$ea_checkout:'');?>" id="ea_checkout" name="ea_checkout" type="text" autocomplete="off" class="form-control timepicker" data-template="dropdown" data-default-time="" data-show-meridian="true" data-minute-step="1" data-second-step="1" placeholder="Check-Out"/>
                </div>
            </div>
            <br/>
            <div class="form-group">
                <label class="control-label">Attendance <span class="req">*</span> :</label>
                <select required class="form-control attendance_status" name="ea_attendance" id="ea_attendance">
                    <option <?=($ea_attendance=="P"?"selected":"");?> value="P">P</option>
                    <option <?=($ea_attendance=="A"?"selected":"");?> value="A">A</option>
                </select>
            </div>


            <div class="form-group" >
                <label class="control-label">Absent Reason :</label>
				<?php echo $this->ckeditor->editor("ea_absent_reason", html_entity_decode($ea_absent_reason)); ?>
			</div>

            <div class="form-group">
                <button type="button" class="btn btn-danger" onclick="window.location='<?php echo ADMIN_URL;?>employeeattendance'">Cancel</button>
                <button type="submit" id="empattsubmit" class="btn btn-success">Submit</button>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    document.addEventListener("DOMContentLoaded", function(event) {
        $(".attendance_type").on('change', function(e) {
            e.preventDefault();
            var type = $(this).val();
            if (type == 'out') {
                $("#ea_checkout").removeAttr('disabled').val(moment().format('HH:mm A'));
                $("#ea_checkin").prop('disabled','disabled').val('');

            } else {
                $("#ea_checkin").removeAttr('disabled').val(moment().format('HH:mm A'));
                $("#ea_checkout").prop('disabled','disabled').val('');
            }
        });
        $("#empattsubmit").on('click', function(e) {
            e.preventDefault();
            var usrid = $("#ea_user_id").val();
       
            
            if (usrid == 'usrsele') {
                alert('Please select a User');
                return;
            }
           
            $("#page_form").submit();

        });
    });
</script>