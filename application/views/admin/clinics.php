<ol class="breadcrumb bc-3">
    <li><a href="<?= ADMIN_URL;?>"><i class="entypo-home"></i>Home</a></li>
    <li class="active"><strong><?= $this->moduleName;?></strong></li>
</ol>

<?php if($alert=="success") { ?>
 <div class="row alertrow">
	<div class="col-md-12">
    <button class="close alertBox" data-dismiss="alert">x</button>
		<div class="alert alert-success"><strong>Success!</strong> <?= rtrim($this->moduleName,'s');?> added sucessfully.</div>
	</div>
 </div>
   <?php } if($alert=="deletesuccess") { ?>
 <div class="row alertrow">
	<div class="col-md-12">
    <button class="close alertBox" data-dismiss="alert">x</button>
		<div class="alert alert-success"><strong>Success!</strong> <?= rtrim($this->moduleName,'s');?> deleted sucessfully.</div>
	</div>
 </div>
   <?php } if($alert=="deleteerror") { ?>
  <div class="row alertrow">
	<div class="col-md-12">
     <button class="close alertBox" data-dismiss="alert">x</button>
		<div class="alert alert-danger"><strong>Error!!</strong> Error occurred while deleting the record, please try again.</div>
	</div>
 </div>
<?php } if($alert=="editsuccess") { ?>
 <div class="row alertrow">
	<div class="col-md-12">
    <button class="close alertBox" data-dismiss="alert">x</button>
		<div class="alert alert-success"><strong>Success!</strong> <?= rtrim($this->moduleName,'s');?> updated sucessfully.</div>
	</div>
 </div>
<?php } if($alert=="error") { ?>
 <div class="row alertrow">
	  <div class="col-md-12">
        <button class="close alertBox" data-dismiss="alert">x</button>
		<div class="alert alert-danger"><strong>Error!!</strong> Error occurred while saving the record, please try again.</div>
	  </div>
 </div>
<?php }if($alert=="permerror") { ?>
  <div class="row alertrow">
    <div class="col-md-12">
     <button class="close alertBox" data-dismiss="alert">x</button>
        <div class="alert alert-danger"><strong>Error!!</strong> Seems like you don't have permission, please try again.</div>
    </div>
</div>
<?php } ?>

<h2><?= $this->moduleName;?></h2>
<hr />

<div class="row" style="min-height:400px;">
	<div class="col-md-12">
		
        <?php 
        if($this->SqlModel->checkPermissions('clinics','delete')==true)
        {?>
        <button  id="deleteAllRecords" class="btn btn-default btn-icon icon-left" type="button">
            Delete Selected
            <i class="entypo-trash"></i>
        </button>
        <?php }
        if($this->SqlModel->checkPermissions('clinics','create')==true)
        {?>
        <button  class="btn btn-default btn-icon icon-left"
                 type="button"
                 onclick="javascript:window.location='<?= base_url('manage/'.$this->controller.'/control/');?>'"
        >
            Add <?= rtrim($this->moduleName,'s');?>
            <i class="entypo-plus-circled"></i>
        </button>
        <?php }?>

        <span class="selectPerPage pull-right"><br/><span style="padding-top: 20px;">Records Per Page:</span>
            <select class="noselect input-small form-control" style="float: right; width: 70px;margin-left: 8px;margin-top: -8px;" name="per_page" id="per_page">
                <option value="0" <?= ((int)$this->per_page===0) ? ' selected="selected" ' : '';?>>All</option>
                    <?php
			            for($i=10;$i<=100;$i+=10) {
                            echo '<option value="'.$i.'"';
                            echo ((int)$this->per_page===$i) ? ' selected="selected" ' : '';
                            echo '>'.$i.'</option>';
			            }
			        ?>
            </select>
        </span>
		<br clear="all" />
        <hr />
        <form class="form-inline pull-right" style="width: 300px;">
            <input class="input-medium form-control span11 aplha"
                    style="width: 65%;margin-right: 10px;float:left;"
                    type="text"
                    value="<?= ($keywords!="-") ? $keywords : "" ;?>"
                    name="search_keywords"
                    id="search_keywords"
                    placeholder="Search keywords"
                    class=" span11 aplha"
            >
            <select autocomplete="off" class="form-control noselect input-medium" name="search_status" id="search_status" style="width: 30%">
                <option value="-">Select Status</option>
                <option selected value="Enabled"
                    <?php if(isset($status)&&$status=="Enabled") {
                            echo ' selected="selected" ';
                          } ?>
                >
                    Enabled
                </option>
                <option value="Disabled"
                    <?php if(isset($status)&&$status=="Disabled") {
                            echo ' selected="selected" ';
                          } ?>
                >
                    Disabled
                </option>
            </select>
        </form>
		&nbsp;
        <hr />
		<form action="<?= ADMIN_URL.$this->controller.'/deleteall';?>" method="post" name="multiDel" id="multiDel">
		<table id="table-<?= $this->controller;?>" class="table table-hover table-striped">
			<thead>
				<tr>
					<th>
					    <input type="checkbox" id="all-checkbox" name="all-checkbox" autocomplete="off">
					</th>
                    <th><a href="<?= base_url('manage/'.$this->controller.'/rent/clinic_id/'.$order.'/'.$status.'/'.$keywords.'/'.$page_numb);?>"
    >Clinic Id</a></th>
<th><a href="<?= base_url('manage/'.$this->controller.'/rent/clinic_owner/'.$order.'/'.$status.'/'.$keywords.'/'.$page_numb);?>"
    >Owner Name</a></th>
<th><a href="<?= base_url('manage/'.$this->controller.'/rent/clinic_name/'.$order.'/'.$status.'/'.$keywords.'/'.$page_numb);?>"
    >Clinic Name</a></th>
<th><a href="<?= base_url('manage/'.$this->controller.'/rent/clinic_mobile1/'.$order.'/'.$status.'/'.$keywords.'/'.$page_numb);?>"
    >Contact</a></th>
<th><a href="<?= base_url('manage/'.$this->controller.'/rent/clinic_address/'.$order.'/'.$status.'/'.$keywords.'/'.$page_numb);?>"
    >Address</a></th>
<th><a href="<?= base_url('manage/'.$this->controller.'/rent/clinic_purchase_date/'.$order.'/'.$status.'/'.$keywords.'/'.$page_numb);?>"
    >Purchase Date</a></th>
<th><a href="<?= base_url('manage/'.$this->controller.'/rent/clinic_added/'.$order.'/'.$status.'/'.$keywords.'/'.$page_numb);?>"
    >Date Added</a></th>

                    <th>Actions</th>
				</tr>
			</thead>
		    <tbody class="records_list">
            <?php
			  if(count($listing)>0) {
				foreach($listing as $c) { ?>
                <tr id="<?= $this->controller.'-'.$c['clinic_id'];?>">
                    <td>
                        <input name="records[]"
                                autocomplete="off"
                                class="cselect"
                                value="<?= $c['clinic_id'];?>"
                                type="checkbox" />
                    </td>
                    
<td><a href="<?= base_url('manage/'.$this->controller.'/control/edit/'.$c['clinic_id']);?>"><?= $c['clinic_id'];?></a></td>

<td><a href="<?= base_url('manage/'.$this->controller.'/control/edit/'.$c['clinic_id']);?>"><?= $c['clinic_owner'];?></a></td>
<td><a href="<?= base_url('manage/'.$this->controller.'/control/edit/'.$c['clinic_id']);?>"><?= $c['clinic_name'];?></a></td>
<td><a href="<?= base_url('manage/'.$this->controller.'/control/edit/'.$c['clinic_id']);?>"><?= 'M:'.$c['clinic_mobile1'].',<br/>P:'.$c['clinic_phone1'];?></a></td>
<td><a href="<?= base_url('manage/'.$this->controller.'/control/edit/'.$c['clinic_id']);?>"><?= $c['clinic_address'];?></a></td>
<td><a href="<?= base_url('manage/'.$this->controller.'/control/edit/'.$c['clinic_id']);?>"><?= $c['clinic_purchase_date'];?></a></td>
<td><a href="<?= base_url('manage/'.$this->controller.'/control/edit/'.$c['clinic_id']);?>"><?= date('d-F-Y h:i:s A', strtotime($c['clinic_added']));?></a></td>

                    <td>
                        <?php 
                        if($this->SqlModel->checkPermissions('clinics','update')==true)
                        {?>
                        <a class="icon-left font16"
                            href="<?= base_url('manage/'.$this->controller.'/control/edit/'.$c['clinic_id']);?>"
                        >
                            <i class="entypo-pencil"></i>
                        </a>

                        <?php }
                        if($this->SqlModel->checkPermissions('clinics','delete')==true)
                        {?>
                        <a class="icon-left font16 delitem"
                            href="javascript:void(0);"
                            data-controller="<?= $this->controller; ?>"
                            id="recordID<?= $c['clinic_id'];?>"
                        >
                            <i class="entypo-cancel"></i>
                        </a>
                        <?php } ?>
                    </td>
                </tr>
			<?php
			  }
		    } else{ ?>
				<tr><td colspan="9">Sorry! No Records.</td></tr>
            <?php } ?>
			</tbody>
		</table>
        </form>
<?php
 $total_pages = ((int) $per_page === 0 )  ? '1' : ceil($total_rows/$per_page);
 $current_page = ((int) $per_page === 0 )  ? '1' : ceil($page_numb/$per_page)+1;
 if($total_pages=="1") {
	$showing_from = 1;
	$showing_to = $total_rows;
 } else if($total_pages==$current_page) {
	$showing_from = ($per_page*($current_page-1))+1;
	$showing_to = $total_rows;
 } else{
	$showing_from = ($per_page*($current_page-1))+1;
	$showing_to = $per_page*$current_page;
 }
 if($total_rows>0) {
?>
Showing <?= $showing_from;?> to <?= $showing_to;?> of <?= $total_rows;?> Record<?= ((int)$total_rows>1)? 's' : '';?> (Total <?= $total_pages;?> Page<?= ((int)$total_pages>1)? 's' : '';?>) <?php } echo (isset($paginate)&&$paginate!="") ? $paginate : '';?>
	</div>
</div>

<script type="text/javascript">
    document.addEventListener("DOMContentLoaded", function(event) {
        window.addEventListener("keypress", dealWithKeyboard, false);     
        function dealWithKeyboard(e) {
            if ((e.keyCode == "65" || e.keyCode == "97") && !$("#search_keywords").is(':focus')) {
                window.location = "control";
            }
        }
        var controller = '<?=$this->controller;?>';
        var sortby = '<?=$sortby;?>';
        var order = '<?=(($order=="ASC") ? 'DESC' : 'ASC');?>';
    });
</script>