<?php
$crumb2 = "";
if(isset($tbl_data['bt_id'])&&$tbl_data['bt_id']!=""){
	$bt_id = $tbl_data['bt_id'];
	$bt_name = $tbl_data['bt_name'];
	$bt_per_day_charges = $tbl_data['bt_per_day_charges']; 
	$bt_billing_type = $tbl_data['bt_billing_type'];
	$bt_description = html_entity_decode ($tbl_data['bt_description']);
	$bt_status = $tbl_data['bt_status'];
	$bt_created_by = $tbl_data['bt_created_by'];
	$bt_updated_by = $tbl_data['bt_updated_by'];
	$bt_added = $tbl_data['bt_added'];
    $bt_updated = $tbl_data['bt_updated'];
	$bt_is_deleted = $tbl_data['bt_is_deleted'];
	$bt_clinic_id = $tbl_data['bt_clinic_id'];
    $crumb = "Edit";
    $action = "editRecord/".$tbl_data['bt_id'];
}
else{
	$bt_id = '';
	$bt_name = '';
	$bt_per_day_charges = '';
	$bt_billing_type = '';
	$bt_description = '';
	$bt_status = '';
	$bt_created_by = '';
	$bt_updated_by = '';
	$bt_added = '';
	$bt_updated = '';
	$bt_is_deleted = '';
	$bt_clinic_id = '';
    $on_home 		= "No";
    $crumb 			= "Add";
    $action 		= "addRecord";
}
?>
<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo ADMIN_URL;?>">
            <i class="entypo-home"></i>Home
        </a>
    </li>
    <li>
        <a href="<?php echo ADMIN_URL.$this->controller;?>">
            <i></i><?php echo $this->moduleName;?>
        </a>
    </li>
    <li class="active">
        <strong><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></strong>
    </li>
</ol>

<h2><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></h2>
<br />
<div class="panel panel-primary">
    <div class="panel-body">
        <form  id="page_form"
               name="page_form"
               method="post"
               action="<?php echo base_url('manage/'.$this->controller.'/'.$action);?>"
               enctype="multipart/form-data"
               class="validate"
        >
                        <div class="form-group">
                <label class="control-label">Name :<span class="req"> *</span></label>
                <input type="text" name="bt_name" id="bt_name" value="<?php echo $bt_name;?>" class="form-control " placeholder="Name" data-validate="required,maxlength[250]"/>
            </div>

            <div class="form-group">
                <label class="control-label">Charges :<span class="req"> *</span></label>
                <input type="number" name="bt_per_day_charges" id="bt_per_day_charges" value="<?php echo $bt_per_day_charges;?>" class="form-control " placeholder="Charges" data-validate="required,maxlength[250]"/>
            </div>

            <div class="form-group col-md-6 col-xs-12 col-lg-6">
                <label class="control-label" style="margin-right: 15px;">Billing Type :</label>
                <div class="clearfix"></div>
                <input checked="checked" type="radio" name="bt_billing_type" id="bt_billing_type1" value="1" <?php echo ($bt_billing_type == '1') ? 'checked' : '';?> />
                <label  class="control-label" for="bt_billing_type1">Daily</label>
                <div class="clearfix"></div>
                <input type="radio" name="bt_billing_type" id="bt_billing_type2" <?php echo ($bt_billing_type == '0') ? 'checked' : '';?> value="0" />
                <label class="control-label" for="bt_billing_type2">Per Admission</label>
            </div>
            <div class="clearfix"></div>


            <div class="form-group" >
                <label class="control-label">Description</label>
				<?php echo $this->ckeditor->editor("bt_description", $bt_description); ?>
			</div>

            <div class="form-group col-md-6 col-xs-12 col-lg-6 hidden">
                <label class="control-label">Status :</label>
                <select class="form-control" name="bt_status" id="bt_status">
                    <option value="Enable" <?php if($bt_status=="Enable"){ echo ' selected="selected"';} ?>>Enable</option>
                    <option value="Disable" <?php if($bt_status=="Disable"){ echo ' selected="selected"';} ?>>Disable</option>
                </select>
            </div>

            <div class="form-group">
                <button type="button" class="btn btn-danger" onclick="window.location='<?php echo ADMIN_URL;?>bedtype'">Cancel</button>
                <button id="bedtype_submit" type="submit"  class="btn btn-success">Submit</button>
            </div>
        </form>
    </div>
</div>