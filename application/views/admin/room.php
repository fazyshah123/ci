<ol class="breadcrumb bc-3">
    <li><a href="<?php echo ADMIN_URL;?>"><i class="entypo-home"></i>Home</a></li>
    <li class="active"><strong><?php echo $this->moduleName;?></strong></li>
</ol>

<?php if($alert=="success") { ?>
 <div class="row alertrow">
	<div class="col-md-12">
    <button class="close alertBox" data-dismiss="alert">x</button>
		<div class="alert alert-success"><strong>Success!</strong> <?php echo rtrim($this->moduleName,'s');?> added sucessfully.</div>
	</div>
 </div>
   <?php } if($alert=="deletesuccess") { ?>
 <div class="row alertrow">
	<div class="col-md-12">
    <button class="close alertBox" data-dismiss="alert">x</button>
		<div class="alert alert-success"><strong>Success!</strong> <?php echo rtrim($this->moduleName,'s');?> deleted sucessfully.</div>
	</div>
 </div>
   <?php } if($alert=="deleteerror") { ?>
  <div class="row alertrow">
	<div class="col-md-12">
     <button class="close alertBox" data-dismiss="alert">x</button>
		<div class="alert alert-danger"><strong>Error!!</strong> Error occurred while deleting the record, please try again.</div>
	</div>
 </div>
<?php } if($alert=="editsuccess") { ?>
 <div class="row alertrow">
	<div class="col-md-12"> 
    <button class="close alertBox" data-dismiss="alert">x</button>
		<div class="alert alert-success"><strong>Success!</strong> <?php echo rtrim($this->moduleName,'s');?> updated sucessfully.</div>
	</div>
 </div>
<?php } if($alert=="error") { ?>
 <div class="row alertrow">
	  <div class="col-md-12">
        <button class="close alertBox" data-dismiss="alert">x</button>
		<div class="alert alert-danger"><strong>Error!!</strong> Error occurred while saving the record, please try again.</div>
	  </div>
 </div>
<?php } if($alert=="permerror") { ?>
  <div class="row alertrow">
    <div class="col-md-12">
     <button class="close alertBox" data-dismiss="alert">x</button>
        <div class="alert alert-danger"><strong>Error!!</strong> Seems like you don't have permission, please try again.</div>
    </div>
+</div>
   <?php } ?>

<h2><?php echo $this->moduleName;?></h2>
<hr />

<div class="row" style="min-height:400px;">
	<div class="col-md-12">
        <?php if($this->SqlModel->checkPermissions('room', 'delete')==true) {?>
		<button  id="deleteAllRecords" class="btn btn-default btn-icon icon-left" type="button">
            Delete Selected
            <i class="entypo-trash"></i>
        </button>
        <?php }
        if($this->SqlModel->checkPermissions('room', 'create')==true) {?>
        <button id="add_room" class="btn btn-default btn-icon icon-left"
                 type="button"
                 onclick="javascript:window.location='<?php echo base_url('manage/'.$this->controller.'/control/');?>'"
        >
            Add <?php echo rtrim($this->moduleName,'s');?>
            <i class="entypo-plus-circled"></i>
        </button>
        <?php }?>
        <span class="selectPerPage pull-right"><br/>Records Per Page:
            <select class="noselect input-small" name="per_page" id="per_page">
                <option value="0" <?php echo ((int)$this->per_page===0) ? ' selected="selected" ' : '';?>>All</option>
                    <?php
			            for($i=10;$i<=100;$i+=10) {
                            echo '<option value="'.$i.'"';
                            echo ((int)$this->per_page===$i) ? ' selected="selected" ' : '';
                            echo '>'.$i.'</option>';
			            }
			        ?>
            </select>
        </span>
		<br clear="all" />
        <hr />
        <form class="form-inline pull-right">
            <input class="input-medium"
                    type="text"
                    value="<?php echo ($keywords!="-") ? $keywords : "" ;?>"
                    name="search_keywords"
                    id="search_keywords"
                    placeholder="Search keywords"
                    class=" span11 aplha"
            >
            <select autocomplete="off" class=" noselect input-medium" name="search_status" id="search_status">
                <option value="-">Select Status</option>
                <option value="Enabled"
                    <?php if(isset($status)&&$status=="Enabled") {
                            echo ' selected="selected" ';
                          } ?>
                >
                    Enabled
                </option>
                <option value="Disabled"
                    <?php if(isset($status)&&$status=="Disabled") {
                            echo ' selected="selected" ';
                          } ?>
                >
                    Disabled
                </option>
            </select>
        </form>
		&nbsp;
        <hr />
		<form action="<?php echo ADMIN_URL.$this->controller.'/deleteall';?>" method="post" name="multiDel" id="multiDel">
		<table id="table-<?php echo $this->controller;?>" class="table table-hover table-striped">
			<thead>
				<tr>
					<th>
					    <input type="checkbox" id="all-checkbox" name="all-checkbox" autocomplete="off">
					</th>
                    <th><a href="<?php echo base_url('manage/'.$this->controller.'/index/ro_id/'.$order.'/'.$status.'/'.$keywords.'/'.$page_numb);?>"
    >Id</a></th>
<th><a href="<?php echo base_url('manage/'.$this->controller.'/index/ro_number/'.$order.'/'.$status.'/'.$keywords.'/'.$page_numb);?>"
    >Number</a></th>
<th><a href="<?php echo base_url('manage/'.$this->controller.'/index/ro_room_type_id/'.$order.'/'.$status.'/'.$keywords.'/'.$page_numb);?>"
    >Room Type Id</a></th>
<th><a href="<?php echo base_url('manage/'.$this->controller.'/index/ro_is_booked/'.$order.'/'.$status.'/'.$keywords.'/'.$page_numb);?>"
    >Booked</a></th>
<th><a href="<?php echo base_url('manage/'.$this->controller.'/index/ro_status/'.$order.'/'.$status.'/'.$keywords.'/'.$page_numb);?>"
    >Status</a></th>

                    <th>Actions</th>
				</tr>
			</thead>
		    <tbody>
            <?php
			  if(count($listing)>0) {
				foreach($listing as $c) { ?>
                <tr id="<?php echo $this->controller.'-'.$c['ro_id'];?>">
                    <td>
                        <input name="records[]"
                                autocomplete="off"
                                class="cselect"
                                value="<?php echo $c['ro_id'];?>"
                                type="checkbox" />
                    </td>
                    
<td><a href="<?php echo base_url('manage/'.$this->controller.'/control/edit/'.$c['ro_id']);?>"><?php echo $c['ro_id'];?></a></td>

<td><a href="<?php echo base_url('manage/'.$this->controller.'/control/edit/'.$c['ro_id']);?>"><?php echo $c['ro_number'];?></a></td>

<td><a href="<?php echo base_url('manage/'.$this->controller.'/control/edit/'.$c['ro_id']);?>"><?php echo $c['rt_name'];?></a></td>

<td><a href="<?php echo base_url('manage/'.$this->controller.'/control/edit/'.$c['ro_id']);?>"><?php echo ($c['ro_is_booked'] =='0' ? 'No' : 'Yes');?></a></td>

<td><a href="<?php echo base_url('manage/'.$this->controller.'/control/edit/'.$c['ro_id']);?>"><?php echo $c['ro_status'];?></a></td>

                    <td>
                <?php if($this->SqlModel->checkPermissions('room', 'update')==true) {?>
                        <a class="icon-left font16"
                            href="<?php echo base_url('manage/'.$this->controller.'/control/edit/'.$c['ro_id']);?>"
                        >
					        <i class="entypo-pencil"></i>
					    </a>
                    <?php }
                    if($this->SqlModel->checkPermissions('room', 'delete')==true) {?>
					    <a class="icon-left font16 delitem"
					        href="javascript:void(0);"
					        data-controller="<?php echo $this->controller; ?>"
					        id="recordID<?php echo $c['ro_id'];?>"
					    >
					        <i class="entypo-cancel"></i>
					    </a>
                    <?php }?>
                    </td>
                </tr>
			<?php
			  }
		    } else{ ?>
				<tr><td colspan="9">Sorry! No Records.</td></tr>
            <?php } ?>
			</tbody>
		</table>
        </form>
<?php
 $total_pages = ((int) $per_page === 0 )  ? '1' : ceil($total_rows/$per_page);
 $current_page = ((int) $per_page === 0 )  ? '1' : ceil($page_numb/$per_page)+1;
 if($total_pages=="1") {
	$showing_from = 1;
	$showing_to = $total_rows;
 } else if($total_pages==$current_page) {
	$showing_from = ($per_page*($current_page-1))+1;
	$showing_to = $total_rows;
 } else{
	$showing_from = ($per_page*($current_page-1))+1;
	$showing_to = $per_page*$current_page;
 }
 if($total_rows>0) {
?>
Showing <?php echo $showing_from;?> to <?php echo $showing_to;?> of <?php echo $total_rows;?> Record<?php echo ((int)$total_rows>1)? 's' : '';?> (Total <?php echo $total_pages;?> Page<?php echo ((int)$total_pages>1)? 's' : '';?>) <?php } echo (isset($paginate)&&$paginate!="") ? $paginate : '';?>
	</div>
</div>

<script type="text/javascript">
    var controller = '<?=$this->controller;?>';
    var sortby = '<?=$sortby;?>';
    var order = '<?=(($order=="ASC") ? 'DESC' : 'ASC');?>';
</script>