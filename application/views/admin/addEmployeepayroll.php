<?php
$crumb2 = "";
if(isset($tbl_data['ep_id'])&&$tbl_data['ep_id']!=""){
	$ep_id = $tbl_data['ep_id'];
	$ep_clinic_id = $tbl_data['ep_clinic_id'];
	$ep_user_id = $tbl_data['ep_user_id'];
	$ep_total_salary = $tbl_data['ep_total_salary'];
	$ep_net_salary = $tbl_data['ep_net_salary'];
	$ep_deduction = $tbl_data['ep_deduction'];
	$ep_commission = $tbl_data['ep_commission'];
	$ep_month = $tbl_data['ep_month'];
	$ep_added = $tbl_data['ep_added'];
	$ep_updated = $tbl_data['ep_updated'];
	$ep_created_by = $tbl_data['ep_created_by'];
	$ep_updated_by = $tbl_data['ep_updated_by'];
	$ep_is_deleted = $tbl_data['ep_is_deleted'];
	$ep_status = $tbl_data['ep_status'];
    $crumb = "Edit";
    $action = "editRecord/".$tbl_data['ep_id'];
}
else{
	$ep_id = '';
	$ep_clinic_id = '';
	$ep_user_id = '';
	$ep_total_salary = '';
	$ep_net_salary = '';
	$ep_deduction = '';
	$ep_commission = '';
	$ep_month = '';
	$ep_added = '';
	$ep_updated = '';
	$ep_created_by = '';
	$ep_updated_by = '';
	$ep_is_deleted = '';
	$ep_status = '';
    $on_home 		= "No";
    $crumb 			= "Add";
    $action 		= "addRecord";
}
?>
<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo ADMIN_URL;?>">
            <i class="entypo-home"></i>Home
        </a>
    </li>
    <li>
        <a href="<?php echo ADMIN_URL.$this->controller;?>">
            <i></i><?php echo $this->moduleName;?>
        </a>
    </li>
    <li class="active">
        <strong><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></strong>
    </li>
</ol>

<h2><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></h2>
<br />
<div class="panel panel-primary">
    <div class="panel-body">
        <form  id="page_form"
               name="page_form"
               method="post"
               action="<?php echo base_url('manage/'.$this->controller.'/'.$action);?>"
               enctype="multipart/form-data"
               class="validate"
        >

            <div class="form-group">
                <label class="control-label">User :<span class="req"> *</span></label>
                <select class="form-control checkout_user"  name="ea_user_id" id="ea_user_id">
                    <option value="select1">Select</option>
                <?php
                $users = $this->SqlModel->getAdminUsersDropDown();
                foreach ($users as $key => $value) {
                    echo '<option value="'.$value['id'].'">'.$value["full_name"].'</option>';
                }
                ?>
                </select>
            </div>

            <div class="form-group">
                <label class="control-label">Total Salary :<span class="req"> *</span></label>
                <input type="number" name="ep_total_salary" id="ep_total_salary" value="<?php echo $ep_total_salary;?>" class="form-control " placeholder="Total Salary" data-validate="required,maxlength[250]"/>
            </div>

            <div class="form-group">
                <label class="control-label">Net Salary :<span class="req"> *</span></label>
                <input type="number" name="ep_net_salary" id="ep_net_salary" value="<?php echo $ep_net_salary;?>" class="form-control " placeholder="Net Salary" data-validate="required,maxlength[250]"/>
            </div>

            <div class="form-group">
                <label class="control-label">Deduction :<span class="req"> *</span></label>
                <input type="number" name="ep_deduction" id="ep_deduction" value="<?php echo $ep_deduction;?>" class="form-control " placeholder="Deduction" data-validate="required,maxlength[250]"/>
            </div>

            <div class="form-group">
                <label class="control-label">Commission :<span class="req"> *</span></label>
                <input type="number" name="ep_commission" id="ep_commission" value="<?php echo $ep_commission;?>" class="form-control " placeholder="Commission" data-validate="required,maxlength[250]"/>
            </div>

            
            <div class="form-group">
                <button type="button" class="btn btn-danger" onclick="window.location='<?php echo ADMIN_URL;?>employeepayroll'">Cancel</button>
                <button type="submit" id="payrollsubmit"  class="btn btn-success">Submit</button>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    document.addEventListener("DOMContentLoaded", function(event) {
        $("#payrollsubmit").on('click', function(e) {
            e.preventDefault();
            var ea_use = $("#ea_user_id").val();
            
            if (ea_use == 'select1') {
                alert('Please select a User');
                return;
            }

            $("#page_form").submit();

        });
    });
</script>