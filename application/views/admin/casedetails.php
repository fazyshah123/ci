<?php
$doctors = $this->SqlModel->getDoctorsDropDown();
?>
<link rel="stylesheet" href="<?php echo ADMIN_ASSETS;?>css/profile.css">
<div id="content">
	<div id="content-header">
		<?php if($alert=="filesuccess") { ?>
      	<div class="row alertrow">
			<div class="col-md-12">
		    <button class="close alertBox" data-dismiss="alert">x</button>
				<div class="alert alert-success"><strong class="responseText">Success </strong>File(s) added successfully!</div>
			</div>
		</div>
	    <?php } if($alert=="fileerror" && $alertmsg=="") { ?>
      	<div class="row alertrow">
			<div class="col-md-12">
		    <button class="close alertBox" data-dismiss="alert">x</button>
				<div class="alert alert-danger"><strong class="responseText">Error </strong>Unable to upload file due to some error!</div>
			</div>
		</div>
	   <?php } if($alertmsg!="" && $alert=="fileerror") { ?>
      	<div class="row alertrow">
			<div class="col-md-12">
		    <button class="close alertBox" data-dismiss="alert">x</button>
				<div class="alert alert-danger"><strong class="responseText">Error </strong>$alertmsg!</div>
			</div>
		</div>
	   <?php } if($alert=="alertmsg") { ?>
      	<div class="row alertrow">
			<div class="col-md-12">
		    <button class="close alertBox" data-dismiss="alert">x</button>
				<div class="alert alert-danger"><strong class="responseText">Error </strong>Unable to upload file due to some error!</div>
			</div>
		</div>
	   <?php } ?>
		<h1>Patient Case Details</h1>
	</div> <!-- #content-header -->	
	<div id="content-container">

		<div class="row">
			<div class="col-md-9">

				<div class="row">

					<div class="col-md-4 col-sm-5">

						<div class="thumbnail">
							<?php
							$image_url = ($patient_info['patient_image'] == '' ? 'profile-placeholder.png' : 'patients/'.$patient_info['patient_image']);
							?>
							<img src="<?=ADMIN_URL;?>../assets/frontend/images/<?=$image_url;?>" alt="Profile Picture">
						</div>

						<br>

						<div class="list-group">  

        					<?php if($this->SqlModel->checkPermissions('patientvisitlog', 'create')==true) {?>
							<a data-toggle="modal" data-target="#openVisitLogModal" href="javascript:;" id="openVisitLogDialouge" class="list-group-item">
								<i class="fa fa-pencil"></i> &nbsp;&nbsp;Visit Log
							</a> 

        					<?php } if($this->SqlModel->checkPermissions('patienttreatment', 'create')==true) {?>
							<a data-toggle="modal" data-target="#openOxygenModal" href="javascript:;" id="openOxygenDialouge" class="list-group-item">
								<i class="fa fa-user"></i> &nbsp;&nbsp;Oxygen Inhalation
							</a>

        					<?php } if($this->SqlModel->checkPermissions('patientcaseexaminations', 'create')==true) {?>
							<a data-toggle="modal" data-target="#openMedicalExamModal" href="javascript:;" id="openMedicalExamDialouge" class="list-group-item">
								<i class="fa fa-flask"></i> &nbsp;&nbsp;Add Medical Examination
							</a>

        					<?php } if($this->SqlModel->checkPermissions('patientoperationrecord', 'create')==true) {?>
							<a data-toggle="modal" data-target="#openOperationRecordModal" href="javascript:;" id="openOperationRecordDialouge" class="list-group-item">
								<i class="fa fa-folder-open"></i> &nbsp;&nbsp;Operation Record
							</a>

        					<?php } if($this->SqlModel->checkPermissions('patientsurgicalnote', 'create')==true) {?>
							<a data-toggle="modal" data-target="#openSurgicalNoteModal" href="javascript:;" id="openSurgicalNoteDialouge" class="list-group-item">
								<i class="fa fa-file"></i> &nbsp;&nbsp;Surgical Note
							</a>

        					<?php } if($this->SqlModel->checkPermissions('patientcasebirth', 'create')==true) {?>
							<a data-toggle="modal" data-target="#openBirthRecordModal" href="javascript:;" id="openBirthRecordDialouge" class="list-group-item">
								<i class="fa fa-credit-card"></i> &nbsp;&nbsp;Birth Record
							</a>
							<?php } ?>
						</div> <!-- /.list-group -->

					</div> <!-- /.col -->


					<div class="col-md-8 col-sm-7">

						<h3><?=$patient_info['patient_name'];?> (MR # <?=$patient_info['patient_mr_id'];?>)</h3>
						<h4>Case # <strong><?=$case['pc_case_number'];?></strong></h4>
						<hr>

						<div class="row">
							<div class="col-md-6">
								<ul class="icons-list">
									<?php
									if (isset($patient_info['patient_phone1']) && !empty($patient_info['patient_phone1'])) { ?>
										<li><i class="icon-li fa fa-phone"></i><?=$patient_info['patient_phone1'];?></li>
									<?php }
										if (isset($patient_info['patient_phone2']) && !empty($patient_info['patient_phone2'])) { ?>
										<li><i class="icon-li fa fa-phone"></i><?=$patient_info['patient_phone2'];?></li>
									<?php }
										if (isset($patient_info['patient_phone3']) && !empty($patient_info['patient_phone3'])) { ?>
										<li><i class="icon-li fa fa-phone"></i><?=$patient_info['patient_phone3'];?></li>
									<?php } ?>
								</ul>									
							</div>
							<div class="col-md-6">
								<ul class="icons-list">
									<?php
									if (isset($patient_info['patient_email']) && !empty($patient_info['patient_email'])) { ?>
									<li><i class="icon-li fa fa-envelope"></i><?=$patient_info['patient_email'];?></li>
									<?php }
									if (isset($patient_info['patient_address']) && !empty($patient_info['patient_address'])) { ?>
									<li><i class="icon-li fa fa-map-marker"></i><?=$patient_info['patient_address'];?></li>
									<?php }
									if (isset($patient_info['patient_age']) && !empty($patient_info['patient_age'])) { ?>
										<li><i class="icon-li fa fa-calendar"></i><?=$patient_info['patient_age'];?> years old</li>
									<?php } ?>
								</ul>									
							</div>
						</div>

						<hr>
						<h3 class="heading">History</h3>
						<div class="timeline_items">
						</div>
						<div class="timeline_show_more_link">
							<center>
								<a class="btn btn-xs btn-success" href="javascript:;" id="show_more_items">Show More</a>
								<input type="hidden" id="input_id">
							</center>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 col-sidebar-right">
				<!-- <div class="well">
					<h4>Medical History</h4>
					<ul class="icons-list text-md">
						<li>
							<i class="icon-li fa fa-location-arrow"></i>
							<strong><a href="#">ABC.png</a></strong> 
						</li>

						<li>
							<i class="icon-li fa fa-location-arrow"></i>

							<strong>Rod</strong> followed the research interest: <a href="javascript:;">Open Access Books in Linguistics</a>. 
							<br>
							<small>about 23 hours ago</small>
						</li>

						<li>
							<i class="icon-li fa fa-location-arrow"></i>

							<strong>Rod</strong> added 51 papers. 
							<br>
							<small>2 days ago</small>
						</li>
					</ul>

				</div> -->
				<?php 
				$visit_logs = $this->PatientVisitLogModel->getPatientVisitLog($patient_info['patient_id'], $case['pc_case_number']);
				$tv = $this->PatientVisitLogModel->getPatientTotalVisits($patient_info['patient_id'], $case['pc_case_number']);
				if (count($visit_logs) != 0) :?>
				<div class="well">
					<h4><strong>Visit Logs (<?=$tv[0]['total_visits'];?>)</strong></h4>
					<div class="icons-list text-md">
						<ol>
						<?php 
						foreach ($visit_logs as $key => $value): ?>
							<li><a><?='('.date('d-M-Y h:i A', strtotime($value['pvl_datetime'])).') ';?></a></li>
						<?php endforeach ?>
						</ol>
					</div>
					<div class="visit_log_show_more_link" style="padding-top:10px;">
						<center>
							<a class="btn btn-xs btn-danger" href="javascript:;" id="show_more_visit_log">Show More</a>
							<input type="hidden" id="last_visit_log_id" value="<?=$visit_logs[count($visit_logs) - 1]['pvl_id'];?>">
						</center>
					</div>
				</div>
				<?php endif; ?>
		</div>


		</div> <!-- /.row -->
	</div> <!-- /#content-container -->
</div>
<style>
	.cursor {
		cursor: pointer;
	}
	.modal-backdrop {
	  z-index: -1;
	}
</style>
<input type="hidden" id="case_id" value="<?=$case['pc_case_number'];?>" />
<input type="hidden" id="patient_id" value="<?=$patient_info['patient_id'];?>"/>

<?php if($this->SqlModel->checkPermissions('patientvisitlog', 'create')==true) {?>
<div class="modal fade" id="openVisitLogModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<form  id="page_form"
		               name="page_form"	
		               method="post"
		               action="<?php echo base_url('manage/patientcase/addVisitLog/'.$case['pc_case_number']);?>"
		               enctype="multipart/form-data"
		               class="validate"
		               onsubmit="return validateForm()"
		        >
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Add Visit Log</h4>
				</div>
				
				<div class="modal-body">
			      	<div class="row alertrow hidden">
						<div class="col-md-12">
					    <button class="close alertBox" data-dismiss="alert">x</button>
							<div class="alert responseType"><strong class="responseText"></strong> <span class="responseMessage"></span></div>
						</div>
					</div>
	            	<div class="form-group">
		                <div class="form-group">
			                <label class="control-label">Surgeon/Physician: <span class="must">*</span></label>
			                <select class="form-control doctorsDropdown"  name="pvl_dtr_id" id="pvl_dtr_id">
			                    <option value="Select">Select</option>
			                <?php
			                foreach ($doctors as $key => $value) {
			                    echo '<option value="'.$value['id'].'">'.$value["full_name"].'</option>';
			                }
			                ?>
			                </select>
			            </div>
		                <div class="clearfix"></div>
		                <div class="form-group col-md-6" style="padding-left: 0px;height: 60px;">
			                <label class="control-label">Visit Date: <span class="must">*</span></label>
			                <div class="date-and-time">
			                    <input style="width: 100%;float: left;" required type="text" value="<?=date('d F Y', strtotime('now'));?>" name="pvl_visit_date" id="pvl_visit_date" autocomplete="off" class="form-control datepicker"  data-format="dd MM yyyy" placeholder="Visit Date"/>
			                </div>
			            </div>
		                <div class="form-group col-md-6" style="padding-right: 0px;height: 60px;">
			                <label class="control-label">Visit Time: <span class="must">*</span></label>
			                <div class="date-and-time">
			                    <input style="width: 100%;float: left;" required type="text" name="pvl_visit_time" id="pvl_visit_time" autocomplete="off" class="form-control timepicker"  data-format="dd MM yyyy" placeholder="Visit Time"/>
			                </div>
			            </div>
		                <div class="row" style="height: 120px;">
	            			<label style="padding: 0px 15px;" for="pvl_note" class="control-label">Note:</label>
	            			<div class="clearfix"></div>
			                <div class="col-md-12">
			                	<textarea name="pvl_note" id="pvl_note" class="form-control" rows="2"></textarea>
			                </div>
		                </div>
	            	</div>
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					<button type="submit" id="submitVisitLogForm" class="btn btn-success">Submit</button>
				</div>
	        </form>
		</div>
	</div>
</div>
<?php } if($this->SqlModel->checkPermissions('patienttreatment', 'create')==true) {?>
<div class="modal fade" id="openOxygenModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<form  id="page_form"
		               name="page_form"	
		               method="post"
		               action="<?php echo base_url('manage/patientcase/addoxygenrecord/'.$case['pc_case_number']);?>"
		               enctype="multipart/form-data"
		               class="validate"
		        >
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Oxygen Inhalation</h4>
				</div>
				
				<div class="modal-body">
			      	<div class="row alertrow hidden">
						<div class="col-md-12">
					    <button class="close alertBox" data-dismiss="alert">x</button>
							<div class="alert responseType"><strong class="responseText"></strong> <span class="responseMessage"></span></div>
						</div>
					</div>
					<div class="items">
			            <div class="item">
			            	<div class="form-group">
				                <div class="form-group">
					                <label class="control-label">Surgeon/Physician: <span class="must">*</span></label>
					                <select class="form-control doctorsDropdown"  name="oi_dtr_id" id="oi_dtr_id">
					                    <option 'Select'>Select</option>
					                <?php
					                foreach ($doctors as $key => $value) {
					                    echo '<option value="'.$value['id'].'">'.$value["full_name"].'</option>';
					                }
					                ?>
					                </select>
					            </div>
				                <div class="clearfix"></div>
				                <div class="form-group col-md-4" style="padding-left: 0px;height: 60px;">
					                <label class="control-label">Date: <span class="must">*</span></label>
					                <div class="date-and-time">
					                    <input style="width: 100%;float: left;" required type="text" value="<?=date('m/d/Y');?>" name="oi_date" id="oi_date" autocomplete="off" class="form-control datepicker"  data-format="dd MM yyyy" placeholder="Date"/>
					                </div>
					            </div>
					            <div class="form-group col-md-4" style="height: 60px;">
					                <label class="control-label">Start Time: <span class="must">*</span></label>
					                <div class="date-and-time">
					                    <input style="width: 100%;float: left;" required type="text" name="oi_start_time" id="oi_start_time" autocomplete="off" class="form-control timepicker"  data-format="dd MM yyyy" placeholder="Start Time"/>
					                </div>
					            </div>
				                <div class="form-group col-md-4" style="height: 60px;padding-right: 0px;">
					                <label class="control-label">End Time: <span class="must">*</span></label>
					                <div class="date-and-time">
					                    <input style="width: 100%;float: left;" required type="text" name="oi_end_time" id="oi_end_time" autocomplete="off" class="form-control timepicker"  data-format="dd MM yyyy" placeholder="End Time"/>
					                </div>
					            </div>
				                <div class="row" style="height: 120px;">
			            			<label style="padding: 0px 15px;" for="oi_note" class="control-label">Diagnosis:</label>
			            			<div class="clearfix"></div>
					                <div class="col-md-12">
					                	<textarea name="oi_note" id="oi_note" class="form-control" rows="2"></textarea>
					                </div>
				                </div>
			            	</div>
			            </div>
					</div>
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					<button type="submit" name="openOxygenSubmit" id="openOxygenSubmit" class="btn btn-success">Submit</button>
				</div>
	        </form>
		</div>
	</div>
</div>
<?php } if($this->SqlModel->checkPermissions('patientcaseexaminations', 'create')==true) {?>
<div class="modal fade" id="openMedicalExamModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<form  id="medical_form"
		               name="medical_form"	
		               method="post"
		               action="<?php echo base_url('manage/patients/addPatientFiles/'.$patient_info['patient_id']);?>"
		               enctype="multipart/form-data"
		               class="validate medicalform"
		        >
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Add History & Physical Examination</h4>
				</div>
				
				<div class="modal-body">
			      	<div class="row alertrow hidden">
						<div class="col-md-12">
					    <button class="close alertBox" data-dismiss="alert">x</button>
							<div class="alert responseType"><strong class="responseText"></strong> <span class="responseMessage"></span></div>
						</div>
					</div>
	            	<div class="form-group">
		                <div class="form-group">
			                <label class="control-label">Surgeon/Physician: <span class="must">*</span></label>
			                <select class="form-control doctorsDropdown"  id="pce_dtr_id">
			                    <option value="Select">Select</option>
			                <?php
			                foreach ($doctors as $key => $value) {
			                    echo '<option value="'.$value['id'].'">'.$value["full_name"].'</option>';
			                }
			                ?>
			                </select>
			            </div>
						<div class="form-group">
						    <label class="control-label">Medical History:</label>
						    <div class="clearfix"></div>
						    <?php
			                $mc = $this->SqlModel->getMedicalConditions();
			                $style="margin-right: 5px;";
			                foreach ($mc as $key => $value) {
			                	$id = $value['mc_name'];
			                    echo '<div class="col-md-4">
			                    		<input type="checkbox" class="cursor" 
			                    			value="'.$id.'" 
			                    			style="'.$style.'" 
			                    			data-id="'.$value['mc_id'].'"
			                    			name="'.$id.'" 
			                    			id="'.$id.'">
			                    		<label 
			                    			class="control-label cursor" 
			                    			for="'.$id.'">'.$id.'
			                    		</label>
			                    	</div>';
			                }
			                ?>
						</div>
		                <div class="clearfix"></div>
		                <div class="form-group col-md-6" style="padding-left: 0px;height: 60px;">
			                <label class="control-label">Visit Date: <span class="must">*</span></label>
			                <div class="date-and-time">
			                    <input style="width: 100%;float: left;" required type="text" value="<?=date('d F Y', strtotime('now'));?>" name="pce_visit_date" id="pce_visit_date" autocomplete="off" class="form-control datepicker"  data-format="dd MM yyyy" placeholder="Visit Date"/>
			                </div>
			            </div>
		                <div class="form-group col-md-6" style="padding-right: 0px;height: 60px;">
			                <label class="control-label">Visit Time: <span class="must">*</span></label>
			                <div class="date-and-time">
			                    <input style="width: 100%;float: left;" required type="text" name="pce_visit_time" id="pce_visit_time" autocomplete="off" class="form-control timepicker"  data-format="dd MM yyyy" placeholder="Visit Time"/>
			                </div>
			            </div>
						<div class="clearfix"></div>
		                <div class="row" style="height: 60px;">
	            			<label style="padding: 0px 15px;" for="pce_examination" class="control-label">Physical Examination:</label>
	            			<div class="clearfix"></div>
			                <div class="col-md-12">
			                	<textarea name="pce_examination" id="pce_examination" class="form-control" rows="2"></textarea>
			                </div>
		                </div>
	            	</div>
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					<button type="submit" id="medicalSubmit" class="btn btn-success">Confirm</button>
				</div>
	        </form>
		</div>
	</div>
</div>
<?php } if($this->SqlModel->checkPermissions('patientoperationrecord', 'create')==true) {?>
<div class="modal fade" id="openOperationRecordModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<form  id="page_form"
		               name="page_form"	
		               method="post"
		               action="<?php echo base_url('manage/patients/addPatientFiles/'.$patient_info['patient_id']);?>"
		               enctype="multipart/form-data"
		               class="validate"
		        >
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Add Operation Record</h4>
				</div>
				
				<div class="modal-body" style="padding-bottom: 0px !important;">
			      	<div class="row alertrow hidden">
						<div class="col-md-12">
					    <button class="close alertBox" data-dismiss="alert">x</button>
							<div class="alert responseType"><strong class="responseText"></strong> <span class="responseMessage"></span></div>
						</div>
					</div>
	            	<div class="form-group">
		                <div class="row" style="height: 60px;">
	            			<label style="padding: 0px 15px;" for="or_provisional_diagnosis" class="control-label">Provisional Diagnosis:<span class="must"> *</span></label>
	            			<div class="clearfix"></div>
			                <div class="col-md-12">
			                	<input type="text" required name="por_provisional_diagnosis" id="por_provisional_diagnosis" class="form-control"/>
			                </div>
		                </div>
		                <div class="form-group col-md-6" style="padding-left:0px;">
			                <label class="control-label">Surgeon/Physician: <span class="must">*</span></label>
			                <select class="form-control doctorsDropdown"  name="pvl_surgeon_id" id="por_surgeon_id">
			                    <option>Select</option>
			                <?php
			                foreach ($doctors as $key => $value) {
			                    echo '<option value="'.$value['id'].'">'.$value["full_name"].'</option>';
			                }
			                ?>
			                </select>
			            </div>
		                <div class="form-group col-md-6" style="padding-right:0px;">
			                <label class="control-label">Assistant: </label>
			                <select class="form-control doctorsDropdown"  name="por_assistant_id" id="por_assistant_id">
			                    <option>Select</option>
			                <?php
			                foreach ($doctors as $key => $value) {
			                    echo '<option value="'.$value['id'].'">'.$value["full_name"].'</option>';
			                }
			                ?>
			                </select>
			            </div>
		                <div class="form-group col-md-6" style="padding-left:0px;">
			                <label class="control-label">Anaesthetist:</label>
			                <select class="form-control doctorsDropdown"  name="por_anaesthetist_id" id="por_anaesthetist_id">
			                    <option value="Select">Select</option>
			                <?php
			                foreach ($doctors as $key => $value) {
			                    echo '<option value="'.$value['id'].'">'.$value["full_name"].'</option>';
			                }
			                ?>
			                </select>
			            </div>
			            <div class="form-group col-md-6" style="padding-right:0px;">
			                <label class="control-label">Surgery: <span class="must">*</span></label>
			                <select class="form-control" name="por_procedure_id" id="por_procedure_id">
			                	<option value="Select">Select</option>
			                <?php
    						$procedures = $this->SqlModel->getProcedureDropDown();
			                foreach ($procedures as $key => $value) {
			                    echo '<option value="'.$value["procedure_id"].'">'.$value["procedure_name"].'</option>';
			                }
			                ?>
			                </select>
			            </div>
			            <div class="clearfix"></div>
		                <div class="row" style="height: 60px;">
	            			<label style="padding: 0px 15px;" for="por_indication_of_surgery" class="control-label">Indication of Surgery:</label>
	            			<div class="clearfix"></div>
			                <div class="col-md-12">
			                	<input type="text" name="por_indication_of_surgery" id="por_indication_of_surgery" class="form-control"/>
			                </div>
		                </div>
		                <div class="clearfix"></div>
		                <div class="form-group col-md-6" style="padding-left: 0px;height: 60px;">
			                <label class="control-label">Date: <span class="must">*</span></label>
			                <div class="date-and-time">
			                    <input style="width: 100%;float: left;" required type="text" value="<?=date('d F Y', strtotime('now'));?>" name="por_visit_date" id="por_visit_date" autocomplete="off" class="form-control datepicker"  data-format="dd MM yyyy" placeholder="Date"/>
			                </div>
			            </div>
		                <div class="form-group col-md-6" style="padding-right: 0px;height: 60px;">
			                <label class="control-label">Time: <span class="must">*</span></label>
			                <div class="date-and-time">
			                    <input style="width: 100%;float: left;" required type="text" name="por_visit_time" id="por_visit_time" autocomplete="off" class="form-control timepicker"  data-format="dd MM yyyy" placeholder="Time"/>
			                </div>
			            </div>
		                <div class="clearfix"></div>
	            	</div>
				</div>
				
				<div class="modal-footer" style="margin-top: 0px !important;">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					<button type="submit" id="operationRecordSubmit" class="btn btn-success">Confirm</button>
				</div>
	        </form>
		</div>
	</div>
</div>
<?php } if($this->SqlModel->checkPermissions('patientsurgicalnote', 'create')==true) {?>
<div class="modal fade" id="openSurgicalNoteModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<form  id="page_form"
		               name="page_form"	
		               method="post"
		               action="<?php echo base_url('manage/patients/addPatientFiles/'.$patient_info['patient_id']);?>"
		               enctype="multipart/form-data"
		               class="validate"
		        >
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Add Diagnosis Report</h4>
				</div>
				
				<div class="modal-body" style="padding-bottom: 0px;">
			      	<div class="row alertrow hidden">
						<div class="col-md-12">
					    <button class="close alertBox" data-dismiss="alert">x</button>
							<div class="alert responseType"><strong class="responseText"></strong> <span class="responseMessage"></span></div>
						</div>
					</div>
	            	<div class="form-group">
		                <div class="row" style="height: 60px;">
	            			<label style="padding: 0px 15px;" for="psn_blood_loss" class="control-label">Blood Loss During Surgery:</label>
	            			<div class="clearfix"></div>
			                <div class="col-md-12">
			                	<input type="text" name="psn_blood_loss" id="psn_blood_loss" class="form-control"/>
			                </div>
		                </div>
		                <div class="row" style="height: 85px;">
	            			<label style="padding: 0px 15px;" for="psn_surgical_notes" class="control-label">Surgical Notes:</label>
	            			<div class="clearfix"></div>
			                <div class="col-md-12">
			                	<textarea name="psn_surgical_notes" id="psn_surgical_notes" class="form-control" placeholder="OPERATION PRCEDURES AND NOTES" rows="2"></textarea>
			                </div>
		                </div>
		                <div class="clearfix"></div>
		                <div class="form-group col-md-6" style="padding-left: 0px;height: 60px;">
			                <label class="control-label">Date: <span class="must">*</span></label>
			                <div class="date-and-time">
			                    <input style="width: 100%;float: left;" required type="text" value="<?=date('d F Y', strtotime('now'));?>" name="psn_date" id="psn_date" autocomplete="off" class="form-control datepicker"  data-format="dd MM yyyy" placeholder="Date"/>
			                </div>
			            </div>
		                <div class="form-group col-md-6" style="padding-right: 0px;height: 60px;">
			                <label class="control-label">Time: <span class="must">*</span></label>
			                <div class="date-and-time">
			                    <input style="width: 100%;float: left;" required type="text" value="<?=date('h:i A');?>" name="psn_time" id="psn_time" autocomplete="off" class="form-control timepicker"  data-format="dd MM yyyy" placeholder="Time"/>
			                </div>
			            </div>
		                <div class="clearfix"></div>
			            <div class="form-group col-md-6" style="padding-left: 0px;height: 60px;">
			                <label class="control-label">Start Time: <span class="must">*</span></label>
			                <div class="date-and-time">
			                    <input style="width: 100%;float: left;" required type="text" value="<?=date('h:i A');?>" name="psn_start_time" id="psn_start_time" autocomplete="off" class="form-control timepicker"  data-format="dd MM yyyy" placeholder="Start Time"/>
			                </div>
			            </div>
		                <div class="form-group col-md-6" style="padding-right: 0px;height: 60px;">
			                <label class="control-label">Finished Time: <span class="must">*</span></label>
			                <div class="date-and-time">
			                    <input style="width: 100%;float: left;" required type="text" value="<?=date('h:i A');?>" name="psn_end_time" id="psn_end_time" autocomplete="off" class="form-control timepicker"  data-format="dd MM yyyy" placeholder="Finished Time"/>
			                </div>
			            </div>
			            <div class="clearfix"></div>
	            	</div>
				</div>
				
				<div class="modal-footer" style="margin-top: 0px;">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					<button type="submit" id="sugricalNoteSubmit" class="btn btn-success">Confirm</button>
				</div>
	        </form>
		</div>
	</div>
</div>
<?php } if($this->SqlModel->checkPermissions('patientcasebirth', 'create')==true) {?>
<div class="modal fade" id="openBirthRecordModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<form  id="page_form"
		               name="page_form"	
		               method="post"
		               action="<?php echo base_url('manage/patients/addPatientFiles/'.$patient_info['patient_id']);?>"
		               enctype="multipart/form-data"
		               class="validate"
		        >
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Add Birth Record</h4>
				</div>
				
				<div class="modal-body" style="padding-bottom: 0px;">
			      	<div class="row alertrow hidden">
						<div class="col-md-12">
					    <button class="close alertBox" data-dismiss="alert">x</button>
							<div class="alert responseType"><strong class="responseText"></strong> <span class="responseMessage"></span></div>
						</div>
					</div>
	                <div class="form-group col-md-4" style="padding-left: 0px;height: 55px;margin-bottom: 0px;">
            			<label for="pcb_weight" class="control-label">Weight: <span class="must">*</span></label>
            			<div class="clearfix"></div>
	                	<input required type="number" name="pcb_weight" id="pcb_weight" class="form-control"/>
	                </div>
	                <div class="form-group col-md-4" style="padding-left:0px;height: 55px;margin-bottom: 0px;">
            			<label for="pcb_sex" class="control-label">Sex: <span class="must">*</span></label>
            			<div class="clearfix"></div>
            			<select required class="form-control" name="pcb_sex" id="pcb_sex">
            				<option value="Male">Male</option>
            				<option value="Female">Female</option>
            			</select>
	                </div>
	                <div class="form-group col-md-4" style="padding:0px;height: 55px;margin-bottom: 3px;">
            			<label for="pcb_obs_duration" class="control-label">OBS Duration:</label>
            			<div class="clearfix"></div>
	                	<input type="text" name="pcb_obs_duration" id="pcb_obs_duration" class="form-control"/>
	                </div>
	                <div class="clearfix"></div>
	                <div class="form-group col-md-4" style="padding-left: 0px;height: 60px;margin-bottom: 0px;">
		                <label class="control-label">Birth Date: <span class="must">*</span></label>
		                <div class="date-and-time">
		                    <input style="width: 100%;float: left;" required type="text" value="<?=date('m/d/Y');?>" name="pcb_birth_date" id="pcb_birth_date" autocomplete="off" class="form-control datepicker"  data-format="dd MM yyyy" placeholder="Birth Date"/>
		                </div>
		            </div>
	                <div class="form-group col-md-4" style="padding-left: 0px;height: 60px;margin-bottom: 0px;">
		                <label class="control-label">Birth Time: <span class="must">*</span></label>
		                <div class="date-and-time">
		                    <input style="width: 100%;float: left;" required type="text" value="<?=date('h:i A');?>" name="pcb_birth_time" id="pcb_birth_time" autocomplete="off" class="form-control timepicker"  data-format="dd MM yyyy" placeholder="Birth Time"/>
		                </div>
		            </div>
	                <div class="form-group col-md-4" style="padding: 0px;height: 60px;margin-bottom: 0px;">
            			<label for="pcb_rbs" class="control-label">RBS:</label>
            			<div class="clearfix"></div>
	                	<input type="text" name="pcb_rbs" id="pcb_rbs" class="form-control"/>
	                </div>
	                <div class="form-group col-md-3" style="padding-left: 0px;height: 60px;margin-bottom: 0px;">
            			<label for="pcb_respiration_rate" class="control-label">Respiration Rate:</label>
            			<div class="clearfix"></div>
	                	<input type="text" name="pcb_respiration_rate" id="pcb_respiration_rate" class="form-control"/>
	                </div>
	                <div class="form-group col-md-3" style="padding-left: 0px;height: 60px;margin-bottom: 0px;">
            			<label for="pcb_heart_rate" class="control-label">Heart Rate:</label>
            			<div class="clearfix"></div>
	                	<input type="text" name="pcb_heart_rate" id="pcb_heart_rate" class="form-control"/>
	                </div>
	                <div class="form-group col-md-3" style="padding-left: 0px;height: 60px;margin-bottom: 0px;">
            			<label for="pcb_2saturation" class="control-label">O<sub>2</sub> Saturation %:</label>
            			<div class="clearfix"></div>
	                	<input type="text" name="pcb_2saturation" id="pcb_2saturation" class="form-control"/>
	                </div>
	                <div class="form-group col-md-3" style="padding: 0px;height: 60px;margin-bottom: 0px;">
            			<label for="pcb_temperature" class="control-label">Temperature:</label>
            			<div class="clearfix"></div>
	                	<input type="text" name="pcb_temperature" id="pcb_temperature" class="form-control"/>
	                </div>
	                <div class="clearfix"></div>
	                <div class="form-group col-md-4" style="padding-left: 0px;height: 60px;margin-bottom: 0px;">
            			<label for="pcb_nebulization" class="control-label">Nebulization:</label>
            			<div class="clearfix"></div>
            			<select class="form-control" name="pcb_nebulization" id="pcb_nebulization">
            				<option value="Yes">Yes</option>
            				<option value="No">No</option>
            			</select>
	                </div>
	                <div class="form-group col-md-4" style="padding-left: 0px;height: 60px;margin-bottom: 0px;">
            			<label for="pcb_section" class="control-label">Section:</label>
            			<div class="clearfix"></div>
            			<select class="form-control" name="pcb_section" id="pcb_section">
            				<option value="Yes">Yes</option>
            				<option value="No">No</option>
            			</select>
	                </div>
	                <div class="form-group col-md-4" style="padding: 0px;height: 60px;margin-bottom: 0px;">
            			<label for="pcb_stomach_wash" class="control-label">Stomach Wash:</label>
            			<div class="clearfix"></div>
            			<select class="form-control" name="pcb_stomach_wash" id="pcb_stomach_wash">
            				<option value="Yes">Yes</option>
            				<option value="No">No</option>
            			</select>
	                </div>
	                <div class="clearfix"></div>
	                <div class="form-group row" style="height: 60px; padding: 0 15px">
            			<label for="pcb_other_details" class="control-label">Other Details:</label>
            			<div class="clearfix"></div>
		                <textarea name="pcb_other_details" id="pcb_other_details" class="form-control" rows="2"></textarea>
	                </div>
	                <div class="clearfix"></div>

	                <div class="form-group col-md-4" style="padding-right: 0px;height: 60px;margin-bottom: 0px;padding-left: 0px;">
            			<label for="pcb_receiver_name" class="control-label">Receiver Name: <span class="must">*</span></label>
            			<div class="clearfix"></div>
	                	<input required type="text" name="pcb_receiver_name" id="pcb_receiver_name" class="form-control"/>
	                </div>

	                <div class="form-group col-md-4" style="padding-right: 0px;height: 60px;margin-bottom: 0px;">
            			<label for="pcb_receiver_phone" class="control-label">Receiver Phone: <span class="must">*</span></label>
            			<div class="clearfix"></div>
	                	<input required type="number" name="pcb_receiver_phone" id="pcb_receiver_phone" class="form-control"/>
	                </div>

	                <div class="form-group col-md-4" style="padding-right: 0px;height: 60px;margin-bottom: 0px;">
            			<label for="pcb_receiver_cnic" class="control-label">Receiver CNIC: <span class="must">*</span></label>
            			<div class="clearfix"></div>
	                	<input required type="number" name="pcb_receiver_cnic" id="pcb_receiver_cnic" class="form-control"/>
	                </div>
	                <div class="clearfix"></div>
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					<button type="submit" id="birthRecordSubmit" class="btn btn-success">Confirm</button>
				</div>
	        </form>
		</div>
	</div>
</div>
<?php } ?>
<script>
	var patient_id = '<?=$patient_info['patient_id'];?>';
	var case_number = '<?=$case['pc_case_number'];?>';
</script>