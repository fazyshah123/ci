<?php $todayAppointments = $this->SqlModel->getTodayAppointments();?>
<?php $todayTokens = $this->SqlModel->getTodayTokens();?>
<?php $reminders = $this->RemindersModel->getRemindersWidget();?>
<?php $remindersCount = $this->RemindersModel->getRemindersCount();?>
<?php if($this->SqlModel->checkPermissions('appointments', 'read')==true) {?>
<div class="col-md-7 dashboard_table" style="height: 350px !important;">
	<?php 
	if (isset($alert)) 
	{
		if($alert=="cancelsuccess") { ?>
			 <div class="row alertrow">
				<div class="col-md-12">
			    <button class="close alertBox" data-dismiss="alert">x</button>
					<div class="alert alert-success"><strong>Success!</strong> Appointment canceled sucessfully.</div>
				</div>
			 </div>
   <?php }
   	if($alert=="cancelerror") { ?>
		 <div class="row alertrow">
			<div class="col-md-12">
		    <button class="close alertBox" data-dismiss="alert">x</button>
				<div class="alert alert-danger"><strong>Error!</strong> Unable to cancel appointment, please try again.</div>
			</div>
		 </div>
	<?php }}?>
	<h4 class="dashboard-widget-header"><strong>Appointments</strong></h4>
	<table class="table table-hover table-striped table-condensed">
		<thead>
			<tr>
				<th>Patient Name</th>
				<th>Appoint. Time</th>
				<th>Doctor Name</th>
				<th>Actions</th>
			</tr>
		</thead>
		<tbody>
			<?php
			if (count($todayAppointments) == 0) {
			?>
				<tr>
					<td colspan="4"><center>No Appointments for today!</center></td>
				</tr>
			<?php
			}
			foreach ($todayAppointments as $key => $value) {
				echo '
				<tr>
					<td>'.$value['patient_name'].'</td>
					<td>'.date('h:i A', strtotime($value['appointment_start_time'])).'</td>
					<td>Dr. '.$value['full_name'].'</td>
					<td width="200">
						<a data-toggle="tooltip" title="Patient Checked" class="btn btn-xs tile-stats tile-cyan pull-left appointmentChecked"  data-id="'.$value["appointment_id"].'" style="margin-right: 8px;min-height: 15px !important;padding: 3px !important;min-width: 15px !important;width: 30px;color: white;" href="javascript:;"><i class="fa fa-check"></i></a>
						<a data-toggle="tooltip" title="Invoice" class="btn btn-xs tile-stats tile-pink pull-left"  data-id="'.$value["appointment_id"].'" style="margin-right: 8px;min-height: 15px !important;padding: 3px !important;min-width: 15px !important;width: 30px;color: white;" href="'.ADMIN_URL.'invoices/control/?patient_id='.$value['patient_id'].'&appointment_id='.$value['appointment_id'].'"><i class="fa fa-dollar"></i></a>
						<a data-toggle="tooltip" title="Prescription" class="btn btn-xs tile-stats tile-new-greene pull-left addPrescription"  data-id="'.$value["appointment_id"].'" style="margin-right: 8px;min-height: 15px !important;padding: 3px !important;min-width: 15px !important;width: 30px;color: white;" href="javascript:;"><i class="fa fa-list"></i></a>
						<a data-toggle="tooltip" title="Reschedule Appointment" class="btn btn-xs tile-stats tile-purple pull-left rescheduleAppointment"  data-id="'.$value["appointment_id"].'" style="margin-right: 8px;min-height: 15px !important;padding: 3px !important;min-width: 15px !important;width: 30px;color: white;" href="javascript:;"><i class="fa fa-calendar"></i></a>
						<a data-toggle="tooltip" title="Cancel Appointment" class="btn btn-xs tile-stats tile-red cancel_appointment_modal" data-id="'.$value["appointment_id"].'" style="min-height: 15px !important;padding: 3px !important;min-width: 15px !important;width: 30px;color: white;" href="javascript:;"><i class="fa fa-times"></i></a>
					</td>
				</tr>';
			}
			?>
			</tbody>
	</table>
</div>
<?php }
if($this->SqlModel->checkPermissions('tokens', 'read')==true) {?>
	<!-- <?php 
	if (isset($alert)) 
	{
		if($alert=="cancelsuccess") { ?>
			 <div class="row alertrow">
				<div class="col-md-12">
			    <button class="close alertBox" data-dismiss="alert">x</button>
					<div class="alert alert-success"><strong>Success!</strong> Appointment canceled sucessfully.</div>
				</div>
			 </div>
    <?php }
   	if($alert=="cancelerror") { ?>
		 <div class="row alertrow">
			<div class="col-md-12">
		    <button class="close alertBox" data-dismiss="alert">x</button>
				<div class="alert alert-danger"><strong>Error!</strong> Unable to cancel appointment, please try again.</div>
			</div>
		 </div>
	<?php }
	}?> -->
<div class="tokenTables"></div>
<?php }?>
<div class="clearfix"></div>
<?php if($this->SqlModel->checkPermissions('appointments', 'read')==true) {?>
<div class="col-md-11 dashboard_table">
	<div class="row alertrow hidden">
		<div class="col-md-12">
	    <button class="close alertBox" data-dismiss="alert">x</button>
			<div class="alert reminderResponseType"><strong class="reminderResponseText"></strong> <span class="reminderResponseMessage"></span></div>
		</div>
	</div>
	
	<h4 class=" dashboard-widget-header" >
		<strong>
			Reminders
		</strong>
		<?php if($this->SqlModel->checkPermissions('reminders', 'create')==true) {?>
			<a style="padding: 0px 10px;height: 25px;" class="btn btn-success" href="<?=ADMIN_URL.'reminders/control';?>"><strong style="font-size: 18px;">+</strong></a>
		<?php } ?>
	</h4>
	
	<?php if ($remindersCount[0]['total'] > 10) { ?>
		<a style="padding: 4px 8px;" class="btn btn-success" href="<?=ADMIN_URL.'reminders/';?>">View All</a>
	<?php } ?>
	<div class="clearfix"></div>
	<div style="height: 320px !important;">
		<table class="table table-hover table-striped table-condensed">
			<thead>
				<tr>
					<th width="40%">Description</th>
					<th>Priority</th>
					<th>Remind On</th>
					<th>Assigned To</th>
					<th>Report To</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
				<?php
				if (count($reminders) == 0) {
				?>
					<tr>
						<td colspan="8"><center>No Reminders!</center></td>
					</tr>
				<?php
				}
				foreach ($reminders as $key => $value) {
					echo '
					<tr id="r-'.$value["r_id"].'"">
						<td>'.htmlspecialchars_decode($value['r_text']).'</td>
						<td><img style="width: 16px;" src="'.FRONTEND_ASSETS."images/priority-flags/".$value['r_priority'].'.png" /></td>
						<td>'.date('d F Y h:i A', strtotime($value['r_remind_on'])).'</td>
						<td>'.$value['assigned_to'].'</td>
						<td>'.$value['report_to'].'</td>
						<td width="15%">';
						if($this->SqlModel->checkPermissions('reminders', 'updatestatus')==true) {
						echo '
							<a data-toggle="tooltip" title="Mark Completed" class="btn btn-xs tile-stats tile-cyan pull-left markCompleted"  data-id="'.$value["r_id"].'" style="margin-right: 8px;min-height: 15px !important;padding: 3px !important;min-width: 15px !important;width: 30px;color: white;" href="javascript:;"><i class="fa fa-check"></i></a>
							<a data-toggle="tooltip" title="Mark Completed" class="btn btn-xs tile-stats tile-new-greene pull-left markCompleted"  data-id="'.$value["r_id"].'" style="margin-right: 8px;min-height: 15px !important;padding: 3px !important;min-width: 15px !important;width: 30px;color: white;" href="javascript:;"><i class="fa fa-pencil"></i></a>
							';
						}
						echo '
						</td>
					</tr>';
				}
				?>
				</tbody>
		</table>
	</div>
</div>
<?php }
?>
<script>
	var doctorsList = <?=json_encode($this->SqlModel->getDoctorsList());?>;
</script>