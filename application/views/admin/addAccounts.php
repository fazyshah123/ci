<?php
$crumb2 = "";
if(isset($tbl_data['a_id'])&&$tbl_data['a_id']!=""){
	$a_id = $tbl_data['a_id'];
	$a_clinic_id = $tbl_data['a_clinic_id'];
	$a_name = $tbl_data['a_name'];
	$a_description = html_entity_decode ($tbl_data['a_description']);
	$a_added = $tbl_data['a_added'];
	$a_updated = $tbl_data['a_updated'];
	$a_created_by = $tbl_data['a_created_by'];
	$a_updated_by = $tbl_data['a_updated_by'];
	$a_is_deleted = $tbl_data['a_is_deleted'];
	$a_status = $tbl_data['a_status'];
    $crumb = "Edit";
    $action = "editRecord/".$tbl_data['a_id'];
}
else{
	$a_id = '';
	$a_clinic_id = '';
	$a_name = '';
	$a_description = '';
	$a_added = '';
	$a_updated = '';
	$a_created_by = '';
	$a_updated_by = '';
	$a_is_deleted = '';
	$a_status = '';
    $on_home 		= "No";
    $crumb 			= "Add";
    $action 		= "addRecord";
}
?>
<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo ADMIN_URL;?>">
            <i class="entypo-home"></i>Home
        </a>
    </li>
    <li>
        <a href="<?php echo ADMIN_URL.$this->controller;?>">
            <i></i><?php echo $this->moduleName;?>
        </a>
    </li>
    <li class="active">
        <strong><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></strong>
    </li>
</ol>

<h2><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></h2>
<br />
<div class="panel panel-primary">
    <div class="panel-body">
        <form  id="page_form"
               name="page_form"
               method="post"
               action="<?php echo base_url('manage/'.$this->controller.'/'.$action);?>"
               enctype="multipart/form-data"
               class="validate"
        >
            <div class="form-group">
                <label class="control-label">Name <span style="color: red">*</span> :</label>
                <input type="text" name="a_name" id="a_name" value="<?php echo $a_name;?>" class="form-control " placeholder="Name" data-validate="required,maxlength[250]"/>
            </div>


            <div class="form-group" >
                <label class="control-label">Description :</label>
				<?php echo $this->ckeditor->editor("a_description", $a_description); ?>
			</div>

            <div class="form-group hidden">
                <label class="control-label">Status :</label>
                <select class="form-control"  name="a_status" id="a_status">
                    <option value="Enable" <?php if($a_status=="Enable"){ echo ' selected="selected"';} ?>>Enable</option>
                    <option value="Disable" <?php if($a_status=="Disable"){ echo ' selected="selected"';} ?>>Disable</option>
                  </select>
            </div>



            <div class="form-group">
                <button type="button" class="btn btn-danger" onclick="window.location='<?php echo ADMIN_URL;?>accounts'">Cancel</button>
                <button id="account_submit" type="submit"  class="btn btn-success">Submit</button>
            </div>
        </form>
    </div>
</div>