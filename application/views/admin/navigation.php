<body class="page-body">
<div class="page-container sidebar-collapsed">
	<div class="sidebar-menu">
		<header class="logo-env">
			<!-- logo -->
			<div class="logo">
				<a href="<?php echo ADMIN_URL;?>">
               		<?php 
					$logo = './assets/frontend/images/logo/'.$this->SqlModel->getSingleField('logo','site_settings',array('clinic_id'=>$this->session->userdata('clinic_id')));
					if(file_exists($logo)) {
					?>
						<img src="<?php echo $this->imagethumb->image($logo,150,0);?>" alt="Admin Logo" />
                    <?php } ?>
				</a>
			</div>
			<!-- logo collapse icon -->
			<div class="sidebar-collapse">
				<a href="#" class="sidebar-collapse-icon with-animation"><!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
					<i class="entypo-menu"></i>
				</a>
			</div>
			<!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
			<div class="sidebar-mobile-menu visible-xs">
				<a href="#" class="with-animation"><!-- add class "with-animation" to support animation -->
					<i class="entypo-menu"></i>
				</a>
			</div>
		</header>
		<ul id="main-menu" class="">
			<li <?php echo (isset($dashBoard))? 'class="active"' : '';?>>
				<a id="dashboard" href="<?php echo ADMIN_URL;?>">
					<i class="entypo-gauge"></i>
					<span>Dashboard</span>
				</a>
			</li>
        <?php
		if($this->SqlModel->checkPermissions('patients', 'read')==true) {?>
        	<li <?php echo (isset($wsettingActive))? 'class="active"' : '';?>>
				<a href="<?php echo ADMIN_URL;?>patients">
					<i class="fa fa-wheelchair"></i>
					<span>Patients</span>
				</a>
			</li>
    	<?php }
		if($this->SqlModel->checkPermissions('appointments', 'read')==true) {?>
        	<li <?php echo (isset($wsettingActive))? 'class="active"' : '';?>>
				<a id="appointment" href="<?php echo ADMIN_URL;?>appointments">
					<i class="fa fa-book"></i>
					<span>Appointments</span>
				</a>
			</li>
    	<?php }
		if($this->SqlModel->checkPermissions('patientcase', 'read')==true) {?>
        	<li <?php echo (isset($labtrackingActive))? 'class="active"' : '';?>>
				<a id="patientcase" href="<?php echo ADMIN_URL;?>patientcase">
					<i class="fa fa-briefcase"></i>
					<span>Patient Cases</span>
				</a>
			</li>
    	<?php }
		if($this->SqlModel->checkPermissions('labtracking', 'read')==true) {?>
        	<li <?php echo (isset($labtrackingActive))? 'class="active"' : '';?>>
				<a id="labtracking" href="<?php echo ADMIN_URL;?>labtracking">
					<i class="medical-icon-i-laboratory"></i>
					<span>Lab Tracking</span>
				</a>
			</li>
    	<?php }
		if($this->SqlModel->checkPermissions('invoices', 'read')==true) {?>
        	<li <?php echo (isset($wsettingActive))? 'class="active"' : '';?>>
				<a id="invoices" href="<?php echo ADMIN_URL;?>invoices">
					<i class="fa fa-money"></i>
					<span>Invoices</span>
				</a>
			</li>
    	<?php }
    	if($this->SqlModel->checkPermissions('reports', 'read')==true) {?>
        	<li <?php echo (isset($reportsActive))? 'class="active"' : '';?>>
				<a href="<?php echo ADMIN_URL;?>reports">
					<i class="fa fa-bar-chart-o"></i>
					<span>Reports</span>
				</a>
			</li>
    	<?php }
    	if($this->SqlModel->checkPermissions('tokens', 'read')==true) {?>
        	<li <?php echo (isset($tokensActive))? 'class="active"' : '';?>>
				<a id="Token" href="<?php echo ADMIN_URL;?>tokens">
					<i class="fa fa-credit-card"></i>
					<span>Tokens</span>
				</a>
			</li>
    	<?php }
    	if($this->SqlModel->checkPermissions('reminders', 'read')==true) {?>
        	<li <?php echo (isset($remindersActive))? 'class="active"' : '';?>>
				<a id="Reminders" href="<?php echo ADMIN_URL;?>reminders">
					<i class="fa fa-bell"></i>
					<span>Reminders</span>
				</a>
			</li>
    	<?php } 
			if(
				$this->SqlModel->checkPermissions('employeeattendance', 'read')==true ||
				$this->SqlModel->checkPermissions('employeepayroll', 'read')==true
			) {
				?>
    		<li class="dropdown">
				<a href="javascript:;">
					<i class="fa fa-users"></i>
					<span>Employee Management</span>
				</a>	

				<ul class="sub-nav">
					<?php
			    	if($this->SqlModel->checkPermissions('employeeattendance', 'read')==true) {?>
			        	<li <?php echo (isset($employeeattendanceActive))? 'class="active"' : '';?>>
							<a href="<?php echo ADMIN_URL;?>employeeattendance">
								<i class="fa fa-clock-o"></i>
								<span>Employee Attendance</span>
							</a>
						</li>
			    	<?php }
			    	if($this->SqlModel->checkPermissions('employeepayroll', 'read')==true) {?>
			        	<li <?php echo (isset($employeepayrollActive))? 'class="active"' : '';?>>
							<a href="<?php echo ADMIN_URL;?>employeepayroll">
								<i class="fa fa-money"></i>
								<span>Employee Payroll</span>
							</a>
						</li>
			    	<?php } ?>
				</ul>
			</li>
			<?php }?>
    		<li class="dropdown">
				<a href="javascript:;">
					<i class="fa fa-book"></i>
					<span>Finance</span>
				</a>	

				<ul class="sub-nav">
					<?php
                    if($this->SqlModel->checkPermissions('accounts', 'read')==true) {?>
                        <li <?php echo (isset($expensesActive))? 'class="active"' : '';?>>
                            <a href="<?php echo ADMIN_URL;?>accounts">
                                <i class="fa fa-arrow-circle-right"></i>
                                <span>Accounts</span>
                            </a>
                        </li>
                    <?php }
					if($this->SqlModel->checkPermissions('suppliers', 'read')==true) {?>
			        	<li <?php echo (isset($suppliersActive))? 'class="active"' : '';?>>
							<a href="<?php echo ADMIN_URL;?>suppliers">
								<i class="fa fa-truck"></i>
								<span>Suppliers</span>
							</a>
						</li>
			    	<?php }
			    	if($this->SqlModel->checkPermissions('expenses', 'read')==true) {?>
			        	<li <?php echo (isset($expensesActive))? 'class="active"' : '';?>>
							<a href="<?php echo ADMIN_URL;?>expenses">
								<i class="fa fa-arrow-circle-right"></i>
								<span>Expenses</span>
							</a>
						</li>
			    	<?php }
			    	if($this->SqlModel->checkPermissions('purchases', 'read')==true) {?>
			        	<li <?php echo (isset($purchasesActive))? 'class="active"' : '';?>>
							<a href="<?php echo ADMIN_URL;?>purchases">
								<i class="fa fa-arrow-circle-left"></i>
								<span>Purchases</span>
							</a>
						</li>
			    	<?php }
			    	if($this->SqlModel->checkPermissions('items', 'read')==true) {?>
			        	<li <?php echo (isset($itemsActive))? 'class="active"' : '';?>>
							<a href="<?php echo ADMIN_URL;?>items">
								<i class="fa fa-dropbox"></i>
								<span>Items</span>
							</a>
						</li>
			    	<?php }
			    	if($this->SqlModel->checkPermissions('stockreleases', 'read')==true) {?>
			        	<li <?php echo (isset($stockreleasesActive))? 'class="active"' : '';?>>
							<a href="<?php echo ADMIN_URL;?>stockreleases">
								<i class="fa fa-download"></i>
								<span>Issued Items</span>
							</a>
						</li>
			    	<?php }
			    	if($this->SqlModel->checkPermissions('stocks', 'read')==true) {?>
			        	<li <?php echo (isset($stocksActive))? 'class="active"' : '';?>>
							<a href="<?php echo ADMIN_URL;?>stocks">
								<i class="fa fa-bitbucket"></i>
								<span>Stocks</span>
							</a>
						</li>
			    	<?php } ?>
				</ul>
			</li>
    		<li class="dropdown">
				<a href="javascript:;">
					<i class="fa fa-cog"></i>
					<span>Settings</span>
				</a>	

				<ul class="sub-nav">
					<?php
					if($this->session->userdata('role_title') == 'Super Admin' && $this->SqlModel->checkPermissions('clinics', 'read')==true) {?>
			        	<li <?php echo (isset($wsettingActive))? 'class="active"' : '';?>>
							<a href="<?php echo ADMIN_URL;?>clinics">
								<i class="medical-icon-i-health-services"></i>
								<span>Clinics</span>
							</a>
						</li>
			    	<?php }
			    	if($this->SqlModel->checkPermissions('bedpatient', 'read')==true) {?>
			        	<li <?php echo (isset($wsettingActive))? 'class="active"' : '';?>>
							<a href="<?php echo ADMIN_URL;?>bedpatient">
								<i class="medical-icon-i-health-services"></i>
								<span>Bed Allotment</span>
							</a>
						</li>
			    	<?php }
			    	if($this->SqlModel->checkPermissions('roompatient', 'read')==true) {?>
			        	<li <?php echo (isset($wsettingActive))? 'class="active"' : '';?>>
							<a href="<?php echo ADMIN_URL;?>roompatient">
								<i class="medical-icon-i-health-services"></i>
								<span>Room Allotment</span>
							</a>
						</li>
			    	<?php }
			    	if($this->SqlModel->checkPermissions('bed', 'read')==true) {?>
			        	<li <?php echo (isset($wsettingActive))? 'class="active"' : '';?>>
							<a href="<?php echo ADMIN_URL;?>bed">
								<i class="medical-icon-i-health-services"></i>
								<span>Bed</span>
							</a>
						</li>
			    	<?php }
			    	if($this->SqlModel->checkPermissions('bedtype', 'read')==true) {?>
			        	<li <?php echo (isset($wsettingActive))? 'class="active"' : '';?>>
							<a href="<?php echo ADMIN_URL;?>bedtype">
								<i class="medical-icon-i-health-services"></i>
								<span>Bed Type</span>
							</a>
						</li>
			    	<?php }
					if($this->SqlModel->checkPermissions('room', 'read')==true) {?>
			        	<li <?php echo (isset($wsettingActive))? 'class="active"' : '';?>>
							<a href="<?php echo ADMIN_URL;?>room">
								<i class="medical-icon-i-health-services"></i>
								<span>Room</span>
							</a>
						</li>
			    	<?php }
			    	if($this->SqlModel->checkPermissions('roomtype', 'read')==true) {?>
			        	<li <?php echo (isset($wsettingActive))? 'class="active"' : '';?>>
							<a href="<?php echo ADMIN_URL;?>roomtype">
								<i class="medical-icon-i-health-services"></i>
								<span>Room Type</span>
							</a>
						</li>
			    	<?php }
					if($this->SqlModel->checkPermissions('admins', 'read')==true) {?>
			        	<li <?php echo (isset($wsettingActive))? 'class="active"' : '';?>>
							<a href="<?php echo ADMIN_URL;?>admins">
								<i class="entypo-users"></i>
								<span>Users</span>
							</a>
						</li>
			    	<?php }
					if($this->SqlModel->checkPermissions('rolemanagement', 'read')==true) {?>
			        	<li <?php echo (isset($wsettingActive))? 'class="active"' : '';?>>
							<a href="<?php echo ADMIN_URL;?>rolemanagement">
								<i class="entypo-flow-branch"></i>
								<span>Role Management</span>
							</a>
						</li>
			    	<?php }
					if($this->SqlModel->checkPermissions('procedures', 'read')==true) {?>
			        	<li <?php echo (isset($wsettingActive))? 'class="active"' : '';?>>
							<a href="<?php echo ADMIN_URL;?>procedures">
								<i class="fa fa-list"></i>
								<span>Procedures</span>
							</a>
						</li>
					<?php }
					if($this->SqlModel->checkPermissions('worktype', 'read')==true) {?>
			        	<li <?php echo (isset($worktypeActive))? 'class="active"' : '';?>>
							<a href="<?php echo ADMIN_URL;?>worktype">
								<i class="medical-icon-i-dental"></i>
								<span>Work Type</span>
							</a>
						</li>
			    	<?php }
					if($this->SqlModel->checkPermissions('work', 'read')==true) {?>
			        	<li <?php echo (isset($workActive))? 'class="active"' : '';?>>
							<a href="<?php echo ADMIN_URL;?>work">
								<i class="fa fa-magic"></i>
								<span>Work</span>
							</a>
						</li>
			    	<?php }
					if($this->SqlModel->checkPermissions('lab', 'read')==true) {?>
			        	<li <?php echo (isset($labActive))? 'class="active"' : '';?>>
							<a href="<?php echo ADMIN_URL;?>lab">
								<i class="fa fa-medkit"></i>
								<span>Labs</span>
							</a>
						</li>
					<?php }
			    	if($this->SqlModel->checkPermissions('reporttype', 'read')==true) {?>
			        	<li <?php echo (isset($reporttypeActive))? 'class="active"' : '';?>>
							<a href="<?php echo ADMIN_URL;?>reporttype">
								<i class="fa fa-list-ol"></i>
								<span>Report Types</span>
							</a>
						</li>
			    	<?php }
			    	if($this->SqlModel->checkPermissions('relations', 'read')==true) {?>
			        	<li <?php echo (isset($relationsActive))? 'class="active"' : '';?>>
							<a href="<?php echo ADMIN_URL;?>relations">
								<i class="fa fa-users"></i>
								<span>Relations</span>
							</a>
						</li>
			    	<?php }
			    	if($this->SqlModel->checkPermissions('facilities', 'read')==true) {?>
			        	<li <?php echo (isset($facilitiesActive))? 'class="active"' : '';?>>
							<a href="<?php echo ADMIN_URL;?>facilities">
								<i class="fa fa-filter"></i>
								<span>Facilities</span>
							</a>
						</li>
			    	<?php }
			    	if($this->SqlModel->checkPermissions('site', 'read')==true) {?>
			        	<li <?php echo (isset($wsettingActive))? 'class="active"' : '';?>>
							<a href="<?php echo ADMIN_URL;?>home/websettings">
								<i class="entypo-cog"></i>
								<span>Website Settings</span>
							</a>
						</li>
			    	<?php }
			    	if($this->SqlModel->checkPermissions('backup', 'read')==true) {?>
			        	<li <?php echo (isset($wsettingActive))? 'class="active"' : '';?>>
							<a href="javascript:;" id="sendBackup">
								<i class="entypo-cog"></i>
								<span>Backup/Restore</span>
							</a>
						</li>
			    	<?php } ?>
				</ul>
			</li>
        	<li>
				<a href="<?php echo ADMIN_URL;?>home/logout">
					<i class="entypo-logout"></i>
					<span>Logout</span>
				</a>
			</li>
		</ul>
	</div>
<?php ################# END of Navigation ##################### ?>
<div class="main-content">
	<div class="row topnav">
		<!-- Profile Info and Notifications -->
		<div class="col-md-6 col-sm-8 clearfix">
			<ul class="user-info pull-left pull-none-xsm">
				<!-- Profile Info -->
				<li class="profile-info"><!-- add class "pull-right" if you want to place this from right -->
						<div id="welcome-user"><strong>Welcome!</strong> <?php echo $userdata['full_name'];?></div> <br/><strong>Last login:</strong> <?php echo date('M d, Y h:ia',strtotime($this->session->userdata('last_login')));?> | <strong>IP Address:</strong> <?php echo $this->session->userdata('last_ip');?>
				</li>
			</ul>
		</div>
		<!-- Raw Links -->
		<div class="col-md-6 col-sm-4 clearfix hidden-xs">
			<ul class="list-inline links-list pull-right">
				<li>
					<a href="<?php echo ADMIN_URL.'home/settings';?>">
						Account Settings <i class="entypo-tools right"></i>
					</a>
				</li>
				<li class="sep"></li>
				<li>
					<a href="<?php echo ADMIN_URL.'home/logout';?>">
						Log Out <i class="entypo-logout right"></i>
					</a>
				</li>
			</ul>
		</div>
	</div>
	<?php
	if ($this->uri->rsegments[1] == 'home' && $this->uri->rsegments[2] == 'index') {
		$pageClass = "";
	} else {
		$pageClass = "content-section";
	}
	?>
	<div class="<?=$pageClass;?>">
<script type="text/javascript">
function getRandomInt(min, max) 
{
	return Math.floor(Math.random() * (max - min + 1)) + min;
}
</script>