<?php
$crumb2 = "";
if(isset($tbl_data['relative_id'])&&$tbl_data['relative_id']!=""){
	$relative_id = $tbl_data['relative_id'];
	$relative_name = $tbl_data['relative_name'];
	$relative_status = $tbl_data['relative_status'];
    $crumb = "Edit";
    $action = "editRecord/".$tbl_data['relative_id'];
}
else{
	$relative_id = '';
	$relative_name = '';
	$relative_status = '';
    $on_home 		= "No";
    $crumb 			= "Add";
    $action 		= "addRecord";
}
?>
<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo ADMIN_URL;?>"> 
            <i class="entypo-home"></i>Home
        </a>
    </li>
    <li>
        <a href="<?php echo ADMIN_URL.$this->controller;?>">
            <i></i><?php echo $this->moduleName;?>
        </a>
    </li>
    <li class="active">
        <strong><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></strong>
    </li>
</ol>

<h2><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></h2>
<br />
<div class="panel panel-primary">
    <div class="panel-body">
        <form  id="page_form"
               name="page_form"
               method="post"
               action="<?php echo base_url('manage/'.$this->controller.'/'.$action);?>"
               enctype="multipart/form-data"
               class="validate"
        >
            <div class="form-group">
                <label class="control-label">Name :<span class="req"> *</span> </label>
                <input type="text" name="relative_name" id="relative_name" value="<?php echo $relative_name;?>" class="form-control " placeholder="Name" data-validate="required,maxlength[250]"/>
            </div>

            <div class="form-group <?php echo (isset($tbl_data['relative_id'])&&(int)$tbl_data['relative_id']===(int)1) ? 'hidden' : '';?> " >
                <label class="control-label">Status :</label>
                
                  <select class="form-control"  name="relative_status" id="relative_status">
                    <option value="Enable" <?php if($relative_status=="Enable"){ echo ' selected="selected"';} ?>>Enable</option>
                    <option value="Disable" <?php if($relative_status=="Disable"){ echo ' selected="selected"';} ?>>Disable</option>
                  </select>
            </div>

            <div class="form-group">
                <button type="button" class="btn btn-danger" onclick="window.location='<?php echo ADMIN_URL;?>relations'">Cancel</button>
                <button id="relation_submit" type="submit"  class="btn btn-success">Submit</button>
            </div>
        </form>
    </div>
</div>