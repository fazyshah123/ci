<?php
if($this->SqlModel->checkPermissions('employeeattendance','create')==true)
{?>
<!-- Modal -->
<div id="markCheckInModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
	<form method="post">
	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" id="checkinForm" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Mark Check-In</h4>
	      </div>
	      <div class="modal-body">
	      	<div class="row alertrow hidden">
				<div class="col-md-12">
			    <button id="back" class="close alertBox" data-dismiss="alert">x</button>
					<div class="alert responseType"><strong class="responseText"></strong> <span class="responseMessage"></span></div>
				</div>
			</div>
			<div class="form-group">
			    <label class="control-label">Name :<span class="req"> *</span></label></label>
			    <select class="form-control check_in_user"  name="ea_check_in_user_id" id="ea_check_in_user_id">
                    <option value="Select">Select</option>
                <?php
                $users = $this->SqlModel->getAdminUsersDropDown();
                foreach ($users as $key => $value) {
                    echo '<option value="'.$value['id'].'">'.$value["full_name"].'</option>';
                }
                ?>
                </select>
			</div>
			<div class="form-group" style="height: 53px;">
			    <label class="control-label">Check-In :<span class="req"> *</span></label></label>
                <div class="date-and-time">
                    <input style="width: 100%;float: left;" value="<?=date('h:i A');?>" id="ea_checkin" name="ea_checkin" type="text" autocomplete="off" class="form-control timepicker" data-template="dropdown" data-default-time="" data-show-meridian="true" data-minute-step="1" data-second-step="1" placeholder="Check-In"/>
                </div>
            </div>
			<div class="form-group" style="height: 53px;">
			    <label class="control-label">Date :</label>
                <div class="date-and-time">
                    <input style="width: 100%;float: left;" id="ea_checkin_date" name="ea_checkin_date" type="text" autocomplete="off" class="form-control datepicker" data-template="dropdown" value="<?=date('m/d/Y');?>" data-default-time="" data-show-meridian="true" data-minute-step="1" data-second-step="1" placeholder="Date"/>
                </div>
            </div>
			<div class="form-group">
			    <label class="control-label">Attendance :<span class="req"> *</span></label></label>
			    <select class="form-control attendance_status" name="ea_checkin_attendance" id="ea_checkin_attendance">
                    <option value="P">P</option>
                    <option value="A">A</option>
                </select>
			</div>
			<div class="form-group absent_reason_description hidden">
			    <label class="control-label">Attendance Reason :</label>
			    <textarea class="form-control" name="ea_absent_reason" id="ea_absent_reason"></textarea>
			</div>
			
	      </div>
	      <div class="modal-footer">
	        <input type="button" class="btn btn-default closeCheckIn" data-dismiss="modal" value="Cancel" />
	        <input id="Check-In" type="submit" class="btn btn-default submitCheckIn" value="Submit" />
	      </div>
	    </div>
	</form>
  </div>
</div>

<!-- Modal -->
<div id="markCheckOutModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
	<form method="post" action="<?=ADMIN_URL;?>employeeattendance/qcheckout">
	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" id="checkoutForm" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Mark Check-Out</h4>
	      </div>
	      <div class="modal-body">
	      	<div class="row alertrow hidden">
				<div class="col-md-12">
			    <button id="back" class="close alertBox" data-dismiss="alert">x</button>
					<div class="alert responseType"><strong class="responseText"></strong> <span class="responseMessage"></span></div>
				</div>
			 </div>
			<div class="form-group">
			    <label class="control-label">Name :<span class="req"> *</span></label>
			    <select class="form-control checkout_user"  name="ea_check_out_user_id" id="ea_check_out_user_id">
                    <option value="Select">Select</option>
                <?php
                $users = $this->SqlModel->getAdminUsersDropDown();
                foreach ($users as $key => $value) {
                    echo '<option value="'.$value['id'].'">'.$value["full_name"].'</option>';
                }
                ?>
                </select>
			</div>
			<div class="form-group" style="height: 53px;">
			    <label class="control-label">Check-Out :<span class="req"> *</span></label></label>
                <div class="date-and-time">
                    <input style="width: 100%;float: left;" value="<?=date('h:i A');?>" id="ea_checkout" name="ea_checkout" type="text" autocomplete="off" class="form-control timepicker" data-template="dropdown" data-default-time="" data-show-meridian="true" data-minute-step="1" data-second-step="1" placeholder="Check-Out"/>
                </div>
            </div>
			<div class="form-group">
			    <label class="control-label">Date :</label>
                <div class="date-and-time">
                    <input style="width: 100%;float: left;" id="ea_checkout_date" name="ea_checkout_date" type="text" autocomplete="off" class="form-control datepicker" data-template="dropdown" value="<?=date('m/d/Y');?>" data-default-time="" data-show-meridian="true" data-minute-step="1" data-second-step="1" placeholder="Date"/>
                </div>
            </div>
	      </div>
	      <div class="modal-footer">
	        <input type="button" class="btn btn-default closeCheckOut" data-dismiss="modal" value="Cancel" />
	        <input id="Check-out" type="submit" class="btn btn-default submitCheckOut" />
	      </div>
	    </div>
	</form>
  </div>
</div>
<?php }
if($this->SqlModel->checkPermissions('expenses','create')==true)
{?>
<!-- Modal -->
<div id="addExpenseModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
	<form method="post" onsubmit="return  name="exp" id="AddExpenseForm" action="<?=ADMIN_URL;?>expenses/addRecord">
	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Add Expense</h4>
	      </div>
	      <div class="modal-body">
			<div class="form-group">
			    <label class="control-label">Account : <span class="req"> *</span></label>
			    <select class="form-control accounts_dropdown"  name="e_account_id" id="e_account_id">
                    <option>Select</option>
                <?php
                $accounts = $this->SqlModel->getAccountsDropDown();
                foreach ($accounts as $key => $value) {
                    echo '<option value="'.$value['a_id'].'">'.$value["a_name"].'</option>';
                }
                ?>
                </select>
			</div>
			<div class="form-group" style="height: 53px;">
			    <label class="control-label">Date :<span class="req"> *</span></label>
                <div class="date-and-time">
                    <input required style="width: 100%;float: left;" id="e_date" name="e_date" type="text" autocomplete="off" class="form-control datepicker" data-show-meridian="true" data-minute-step="1" data-second-step="1" data-format="dd MM yyyy" value="<?=date('d F Y', strtotime('now'));?>" placeholder="Date"/>
                </div>
            </div>
			<div class="form-group">
			    <label class="control-label">Amount :<span class="req"> *</span></label>
			    <input  type="number" name="e_amount" id="e_amount" class="form-control " placeholder="Amount" data-validate="required,maxlength[250]" required/>
			</div>
			<div class="form-group">
			    <label class="control-label">Description :</label>
			    <textarea id="e_description" name="e_description" rows="2"></textarea>
			</div>
	      </div>
	      <div class="modal-footer">
	        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel" />
	        <input id="Expensesubmit" type="submit" class="btn btn-default" />
	      </div>
	    </div>
	</form>
  </div>
</div>
<?php }
if($this->SqlModel->checkPermissions('tokens','create')==true)
{?>
<!-- Modal -->
<div id="addPrescriptionModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
	<form method="post" action="<?=ADMIN_URL;?>expenses/addRecord">
	    <!-- Modal content-->
	    <div class="modal-content" style="width: 786px;left: -13.5%;">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Prescription</h4>
	      </div>
	      <div class="modal-body">
	      	<ul class="nav nav-tabs" style="margin-top: 0px;">
				<li class="active"><a data-toggle="tab" href="#treatment"><strong>Treatment</strong></a></li>
				<li><a data-toggle="tab" href="#medication"><strong>Medication</strong></a></li>
			</ul>

			<div class="tab-content">
				<div id="treatment" class="tab-pane fade in active">
					<div class="form-group">
					    <label class="control-label">Medical History :</label>
					    <div class="clearfix"></div>
					    <?php
		                $mc = $this->SqlModel->getMedicalConditions();
		                foreach ($mc as $key => $value) {
		                    echo '<div class="col-md-4"><input type="checkbox" value="'.$value['mc_id'].'" style="margin-right: 5px;" name="'.str_replace('/', '', str_replace(' ', '', $value['mc_name'])).'"><label class="control-label">'.$value["mc_name"].'</label></div>';
		                }
		                ?>
		                <input type="hidden" name="mc_patient_id" id="mc_patient_id">
		                <input type="hidden" name="mc_appointment_id" id="mc_appointment_id">
					</div>
					<div class="clearfix"></div>
					<div class="form-group" style="height: 60px;">
					    <label class="control-label">Complaint :</label>
		                <textarea id="mh_complaint" name="mh_complaint" class="form-control" placeholder="Complaint"/></textarea>
		            </div>
					<div class="form-group">
					    <label class="control-label">Clinical Note :</label>
		                <textarea id="mh_clinical_note" name="mh_clinical_note" class="form-control" placeholder="Clinical Note"/></textarea>
					</div>
				</div>
				<div id="medication" class="tab-pane fade">
					<?php
		            $items = $this->SqlModel->getItemsDropDown();
		            $itemsList = '';
		            foreach ($items as $key => $value) {
		                $itemsList .= '<option value="'.$value['i_id'].'" data-price="'.$value['i_unit_price'].'">'.$value["i_name"].'</option>';
		            }
		            ?>
		            <div class="items">
		                <div class="row col-md-12 pd_items">
		                    <div class="form-group col-md-4">
		                        <label class="control-label">Item <span class="req">*</span> :</label>
		                        <select class="form-control item_name"  name="pd_item_id-1" id="pd_item_id-1">
		                            <option>Select</option>
		                            <?=$$itemsList;?>
		                        </select>
		                    </div>
		                    <div class="form-group col-md-2">
		                        <label class="control-label">Duration <span class="req">*</span> :</label>
		                        <input required type="number" name="pd_qty-1" data-item="1" id="pd_qty-1" class="form-control item_qty" placeholder="Qty" data-validate="required,maxlength[250]"/>
		                        <select name="drug[][d_type]" id="drug__d_type" style="border: 0px !important;padding: 0px;height: 18px;" class="form-control container" value="day(s)">
		                        	<option selected="selected" value="day(s)">Day(s)</option>
									<option value="week(s)">Week(s)</option>
									<option value="month(s)">Month(s)</option>
								</select>
		                    </div>
		                    <div class="form-group col-md-2">
		                        <label class="control-label">Qty. <span class="req">*</span> :</label>
		                        <input type="number" step="any" name="pd_unit_price-1" data-item="1" id="pd_unit_price-1" class="form-control item_unit_price" placeholder="Price" data-validate="required,maxlength[250]"/>
		                        <select  style="border: 0px !important;padding: 0px;height: 18px;" name="drug[][medication_unit]" id="drug__medication_unit" class="form-control">
		                        	<option value="">Select</option>
		                        	<option value="capsule(s)">Capsule(s)</option>
									<option value="tablet(s)">Tablet(s)</option>
									<option value="ml">ml</option>
									<option value="mg">mg</option>
									<option value="iu">IU</option>
									<option value="drop">Drop</option>
									<option value="tablespoon">Tablespoon</option>
									<option value="teaspoon">Teaspoon</option>
									<option value="unit(s)">Unit(s)</option>
									<option value="puff(s)">Puff(s)</option>
								</select>
		                    </div>
		                    <div class="form-group col-md-2">
		                        <label class="control-label">Frequency :</label>
		                        <input type="number" step="any" name="pd_discount_rs-1" data-item="1" id="pd_discount_rs-1" class="form-control item_discount_rs" placeholder="Discount in Rs."/>
		                    </div>
		                    <div class="form-group col-md-2">
		                        <label class="control-label">Instruction :</label>
		                        <input type="number" step="any" name="pd_discount_percent-1" data-item="1" id="pd_discount_percent-1" class="form-control item_discount_percent" placeholder="Discount in %"/>
		                    </div>
		                </div>
		            </div>
				</div>
			</div>
	      </div>
	      <div class="modal-footer">
	        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel" />
	        <input  type="submit" class="btn btn-default" />
	      </div>
	    </div>
	</form>
  </div>
</div>
<?php }
if($this->SqlModel->checkPermissions('items','issue')==true)
{?>
<!-- Modal -->
<div id="issueItemModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
	<form method="post" id="issueform" name="itemform" action="<?=ADMIN_URL;?>stockreleases/qadd">
	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Issue Item</h4>
	      </div>
	      <div class="modal-body">
			<div class="form-group" style="height: 53px;">
			    <label class="control-label">Date :<span class="req"> *</span></label></label>
                <div class="date-and-time">
                    <input required style="width: 100%;float: left;"  type="text" name="sr_date" id="sr_date" autocomplete="off" class="form-control datepicker" value="<?=date('d-M-Y', strtotime('now'));?>"  data-format="dd MM yyyy" placeholder="Date"/>
                    <input type="hidden" name="itemCount" id="issuedItemCount" value="1" />
                </div>
			</div>
			<div class="row items" style="padding: 0px 15px 0px 15px;">
				<div class="row item item-1">
					<div class="form-group col-md-6">
					    <label class="control-label">Item :<span class="req"> *</span></label>
					    <select class="form-control items_dropdown" name="sr_item_id-1" id="sr_item_id-1">
		                    <option>Select</option>
		                <?php
		                $items = $this->SqlModel->getItemsDropDown();
		                foreach ($items as $key => $value) {
		                    echo '<option value="'.$value['i_id'].'">'.$value["i_name"].'</option>';
		                }
		                ?>
		                </select>
					</div>
					<div class="form-group col-md-6">
					    <label class="control-label">Quantity :<span class="req"> *</span></label></label>
					    <input required type="number" name="sr_qty-1" id="sr_qty-1" class="form-control " placeholder="Quantity" data-validate="required"/>
					</div>
				</div>
			</div>
			<div class="row" style="padding: 0px 15px 0px 15px;">
				<div class="form-group col-md-12">
				    <button class="pull-right btn btn-success addItemRow">Add Item</button>
				</div>
			</div>
			
			<div class="form-group">
			    <label class="control-label">Issues To :<span class="req"> *</span></label></label>
			    <select required class="form-control users_dropdown" name="sr_issued_to" id="sr_issued_to">
                    <option value="Select">Select</option>
                <?php
                $users = $this->SqlModel->getAdminUsersDropDown();
                foreach ($users as $key => $value) {
                    echo '<option value="'.$value['id'].'">'.$value["full_name"].'</option>';
                }
                ?>
                </select>
			</div>
			<div class="form-group">
			    <label class="control-label">Description :</label>
			    <textarea id="sr_description" name="sr_description" rows="2"></textarea>
			</div>
	      </div>
	      <div class="modal-footer">
	        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel" />
	        <input 
 type="submit" id="issueitem" class="btn btn-default" />
	      </div>
	    </div>
	</form>
  </div>
</div>

<?php }
if($this->SqlModel->checkPermissions('tokens','create')==true)
{?>
<!-- Modal -->
<div id="issueTokenModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
	<form method="post" action="#">
	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Issue Token</h4>
	      </div>
	      <div class="modal-body">
	      	<div class="row alertrow hidden">
				<div class="col-md-12">
			    <button id="back" class="close alertBox" data-dismiss="alert">x</button>
					<div class="alert responseType"><strong class="responseText"></strong> <span class="responseMessage"></span></div>
				</div>
			 </div>
			<div class="form-group">
			    <label class="control-label">Doctor :<span class="req"> *</span></label></label>
			    <select class="form-control doctorsDropdown"  name="t_dtr_id" id="t_dtr_id">
                    <option>Select</option>
                <?php
                $doctors = $this->SqlModel->getDoctorsDropDown();
                foreach ($doctors as $key => $value) {
                    echo '<option value="'.$value['id'].'">'.$value["full_name"].'</option>';
                }
                ?>
                </select>
			</div>
			<div class="form-group">
			    <label class="control-label">Token No. :</label>
			    <input type="text" name="t_number" id="t_number" readonly class="form-control " placeholder="Token No." data-validate="required,maxlength[250]"/>
			</div>
			<div class="form-group">
			    <label class="control-label">Patient MR # :<span class="req"> *</span></label></label>
			    <div class="clearfix"></div>
			    <input type="text" style="width: 80%;float: left;" name="patient_mr_id" id="patient_mr_id" class="form-control " placeholder="Patient MR #" data-validate="required,maxlength[250]"/>
			    <button id="search_patient" class="btn btn-default">Search</button>
			</div>
			<div class="form-group">
			    <label class="control-label">Patient Name :</label>
			    <input type="text" readonly name="t_patient_name" id="t_patient_name" class="form-control " placeholder="Patient Name" data-validate="required,maxlength[250]"/>
			    <input type="hidden" name="t_patient_id" id="t_patient_id"/>
			</div>
	      </div>
	      <div class="modal-footer">
	        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel" />
	        <input type="submit" name="submitIssueToken" class="btn btn-default" id="submitIssueToken" />
	      </div>
	    </div>
	</form>
  </div>
</div>

<?php }
if($this->SqlModel->checkPermissions('patients','create')==true)
{?>
<!-- Modal -->
<div id="addPatientModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
	<form method="post" action="<?=ADMIN_URL;?>patients/addRecord">
	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Add Patient</h4>
	      </div>
	      <div class="modal-body">
	      	<div class="row alertrow hidden">
				<div class="col-md-12">
			    <button id="back" class="close alertBox" data-dismiss="alert">x</button>
					<div class="alert responseType"><strong class="responseText"></strong> <span class="responseMessage"></span></div>
				</div>
			 </div>
			<div class="form-group">
			    <label class="control-label">Name :<span class="req"> *</span></label></label>
			    <input required type="text" name="patient_name" id="patient_name" class="form-control " placeholder="Name" data-validate="required,maxlength[250]"/>
			</div>
			<div class="form-group">
			    <label class="control-label">Mobile :<span class="req"> *</span></label></label>
			    <input required type="tel" name="patient_phone1" id="patient_phone1" class="form-control " placeholder="Mobile" data-validate="required,maxlength[250]"/>
			</div>
			<div class="form-group">
			    <label class="control-label">Age :<span class="req"> *</span></label></label>
			    <input required type="number" name="patient_age" id="patient_age" class="form-control " placeholder="Age" data-validate="required,maxlength[250]"/>
			</div>
			<div class="form-group">
			    <label class="control-label">Address :</label>
			    <input type="text" name="patient_address" id="patient_address" class="form-control " placeholder="Address" data-validate="required,maxlength[250]"/>
			</div>
			<div class="form-group">
			    <label class="control-label">Gender :<span class="req"> *</span></label></label>
			    <select class="form-control" name="patient_gender" id="patient_gender">
                    <option value="Male">Male</option>
                    <option value="Female">Female</option>
                    <option value="Other">Other</option>
                </select>
			</div>
			<div class="form-group">
			    <label class="control-label">Email :</label>
			    <input required type="email" name="patient_email" id="patient_email" class="form-control " placeholder="Email" data-validate="required,maxlength[250]"/>
			</div>
	      </div>
	      <div class="modal-footer">
	        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel" />
	        <input id="paitentsubmit" type="submit" class="btn btn-default addPatientSubmit" />
	      </div>
	    </div>
	</form>
  </div>
</div>
<?php }
if($this->SqlModel->checkPermissions('appointments','update')==true)
{?>
<!-- Modal -->
<div id="rescheduleAppointmentModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
	<form method="post" action="<?=ADMIN_URL;?>appointments/rescheduleAppointment">
	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Reschedule Appointment</h4>
	      </div>
	      <div class="modal-body">
	      	<div class="row alertrow hidden">
				<div class="col-md-12">
			    <button id="back" class="close alertBox" data-dismiss="alert">x</button>
					<div class="alert responseType"><strong class="responseText"></strong> <span class="responseMessage"></span></div>
				</div>
			 </div>
			<div class="form-group">
			    <label class="control-label">Patient <span class="req">*</span> :</label>
			    <select class="form-control patientsDropdown" name="appointment_patient_id" id="appointment_patient_id">
                <?php
                $items = $this->SqlModel->getPatientsDropDown();
                foreach ($items as $key => $value) {
                    echo '<option value="'.$value['patient_id'].'">'.$value["patient_name"].'</option>';
                }
                ?>
                </select>
			    <input type="hidden" name="appointment_id" id="appointment_id">
			</div>
			<div class="form-group">
			    <label class="control-label">Doctor :<span class="req">*</span></label>
			    <select class="form-control doctorsDropdown" name="appointment_dtr_id" id="appointment_patient_id">
                <?php
                $items = $this->SqlModel->getDoctorsDropDown();
                foreach ($items as $key => $value) {
                    echo '<option value="'.$value['id'].'">'.$value["full_name"].'</option>';
                }
                ?>
                </select>
			</div>
			<div class="form-group" style="height: 60px;">
			    <label class="control-label">Date :<span class="req">*</span></label>
                <div class="date-and-time">
                    <input required style="width: 100%;float: left;" value="<?=date('d-M-Y');?>" id="appointment_date" name="appointment_date" type="text" autocomplete="off" class="form-control datepicker" data-template="dropdown" data-default-time="" data-show-meridian="true" data-minute-step="1" data-second-step="1" placeholder="Date"/>
                </div>
			</div>
			<div class="form-group" style="height: 60px;">
			    <label class="control-label">Start Time :<span class="req">*</span></label>
                <div class="date-and-time">
                    <input required style="width: 100%;float: left;" value="<?=date('h:i A');?>" id="appointment_start_time" name="appointment_start_time" type="text" autocomplete="off" class="form-control timepicker" data-template="dropdown" data-default-time="" data-show-meridian="true" data-minute-step="1" data-second-step="1" placeholder="Start Time"/>
                </div>
			</div>
			<div class="form-group">
			    <label class="control-label">End Time :</label>
                <div class="date-and-time">
                    <input style="width: 100%;float: left;" value="<?=date('h:i A');?>" id="appointment_end_time" name="appointment_end_time" type="text" autocomplete="off" class="form-control timepicker" data-template="dropdown" data-default-time="" data-show-meridian="true" data-minute-step="1" data-second-step="1" placeholder="End Time"/>
                </div>
			</div>
	      </div>
	      <div class="modal-footer">
	        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel" />
	        <input type="submit" class="btn btn-default rescheduleAppointmentSubmit" />
	      </div>
	    </div>
	</form>
  </div>
</div>
<?php }?>
<script type="text/javascript">
	function clearCheckInForm() {
		$('#ea_check_in_user_id').val('Select');
		$('#ea_check_in_user_id').trigger('change');
		$("#ea_checkin").val('<?=date('h:i A');?>');
		$("#ea_checkin_date").val('<?=date('m/d/Y');?>');
		$('#ea_checkin_attendance').val('P');
		$('#ea_checkin_attendance').trigger('change');
		$("#ea_absent_reason").val('');
	}
	function clearCheckOutForm() {
		$('#ea_check_out_user_id').val('Select');
		$('#ea_check_out_user_id').trigger('change');
		$("#ea_checkout").val('<?=date('h:i A');?>');
		$("#ea_checkout_date").val('<?=date('m/d/Y');?>');
	}
	function postCheckIn() {
		var checkInUrl = '<?=ADMIN_URL;?>employeeattendance/qcheckin';
		var user = $("#ea_check_in_user_id").val();
		var checkin = $("#ea_checkin").val();
		var date = $("#ea_checkin_date").val();
		var attendance = $("#ea_checkin_attendance").val();
		var absent_reason = $("#ea_absent_reason").val();

		if ( user == 'Select') {
			alert('Please select Name');
			return;
		}
		$.post(checkInUrl, {
			ea_user_id: user,
			ea_checkin: checkin,	
			ea_date: date,
			ea_attendance: attendance,
			ea_absent_reason: absent_reason
		}, function(data) {
			showAlertNotification(data);
			clearCheckInForm();
		}, "json");
	}
	function postCheckOut() {
		var checkOutUrl = '<?=ADMIN_URL;?>employeeattendance/qcheckout';
		var user = $("#ea_check_out_user_id").val();
		var checkout = $("#ea_checkout").val();
		var date = $("#ea_checkout_date").val();

		if (user == 'Select') {
			alert('Please select Name');
			return;
		}
		$.post(checkOutUrl, {
			user: user,
			checkout: checkout,	
			date: date
		}, function(data) {
			showAlertNotification(data);
			clearCheckOutForm();
		}, "json");
	}
	function getTokenMarquee() {
		$.post('<?=ADMIN_URL;?>tokens/marquee', {
			patient_email: patient_email,
			patient_phone1: patient_phone1,
			patient_age: patient_age,
			patient_name: patient_name,
			patient_gender: patient_gender,
			patient_address: patient_address
		}, function(data) {
			showAlertNotification(data);
		}, "json");
	}
	function clearAlertNotification() {
		$(".responseText").text('');
		$(".responseType").removeClass('alert-danger alert-success');
		$(".responseMessage").text('');
		$(".alertrow").addClass('hidden');
	}
	function showAlertNotification(data) {
		$(".responseText").text(data.alert);
		$(".responseType").addClass(data.responseType);
		$(".responseMessage").text(data.message);
		$(".alertrow").removeClass('hidden');
	}
	function getLatestToken() {
		var t_id = $("#t_number").val();
		var dtr_id = $("#t_dtr_id").val();
		if (t_id == '' && t_id != 'Select') {
			$.post('<?=ADMIN_URL;?>tokens/getlatesttoken', {
				t_id: t_id,
				dtr_id: dtr_id
			}, function(data) {
				$("#t_number").val(data.token_number);
				if (data.message) {
					showAlertNotification(data);	
				}
			}, "json");
		}
	}
	function validateTokenForm() {
		var dtr_id = $("#t_dtr_id").val();
		var patient_mr_id = $("#patient_mr_id").val();
		var t_number = $("#t_number").val();
		var t_patient_id = $("#t_patient_id").val();
		if (dtr_id == '' || patient_mr_id == '' || t_number == '' || t_patient_id == '') {
			alert('One of the required field is missing, please try again!');
			return false;
		}
		return true;
	}
	function submitToken() {
		var dtr_id = $("#t_dtr_id").val();
		var patient_mr_id = $("#patient_mr_id").val();
		var t_number = $("#t_number").val();
		var t_patient_name = $("#t_patient_name").val();
		var t_patient_id = $("#t_patient_id").val();
		$.post('<?=ADMIN_URL;?>tokens/generateToken', {
			t_dtr_id: dtr_id,
			patient_mr_id: patient_mr_id,
			t_number: t_number,
			t_patient_name: t_patient_name,
			t_patient_id: t_patient_id,
		}, function(data) {
			if (data.message) {
				showAlertNotification(data);
				clearTokenModal();	
				window.location.reload();
			}
		}, "json");
	}
	function clearTokenModal() {
		$("#t_dtr_id").val('');
		$("#patient_mr_id").val('');
		$("#t_number").val('');
		$("#t_patient_name").val('');
		$("#t_patient_id").val('');
		$("#t_dtr_id").val('Select').select2().trigger('change')
	}
	function tokenTableEvents() {
		$(".addPrescription").on("click",function(e) {
			$('#addPrescriptionModal').appendTo("body").modal('show');
		});
		$(".tokenChecked").on("click", function(e) {
			e.preventDefault();
			$("#checkedTokenId").val($(this).data('id'));
			$('#checkedTokenModal').appendTo("body").modal('show');
		});
		$(".cancel_token_modal").on('click', function(e) {
			e.preventDefault();
			$("#cancelTokenId").val($(this).data('id'));
			$('#cancelTokenModal').appendTo("body").modal('show');
		});
	}
	function loadTokens() {
		$.post('<?=ADMIN_URL.'tokens/getUpcomingTokens';?>', function(data) {
			if (data.tokens) {
				if (data.tokens.length > 0) {
					var tokens = '';
					$.each(data.tokens, function(k, v) {
						tokens += ' | (TKN #' + v.t_number + '--' + v.patient_name + '-- Dr.' + v.full_name + ')'
					});
					$("#upcomingTokens").text(tokens);
				}
			}
		}, "json");
	}
	document.addEventListener("DOMContentLoaded", function(event) {
	    loadTokens();
		$(".submitCheckIn").on("click", function(e) {
			e.preventDefault();
			postCheckIn();
		});
		$(".submitCheckOut").on("click", function(e) {
			e.preventDefault();
			postCheckOut();
		});
		$(".addPatientSubmit").on("click", function(e) {
			e.preventDefault();
			var patient_email = $("#patient_email").val();
			var patient_gender = $("#patient_gender").val();
			var patient_address = $("#patient_address").val();
			var patient_name = $("#patient_name").val();
			var patient_age = $("#patient_age").val();
			var patient_phone1 = $("#patient_phone1").val();
			$.post('<?=ADMIN_URL;?>patients/qadd', {
				patient_email: patient_email,
				patient_phone1: patient_phone1,
				patient_age: patient_age,
				patient_name: patient_name,
				patient_gender: patient_gender,
				patient_address: patient_address
			}, function(data) {
				showAlertNotification(data);
			}, "json");
		});
		$("#submitIssueToken").on("click", function(e) {
			e.preventDefault();
			if (validateTokenForm()) {
				submitToken();
			}
		});
		$("#Expensesubmit").on('click', function(e) {
			e.preventDefault();
			var account_id = $("#e_account_id").val();
			var amount = $("#e_amount").val();
			if (account_id == 'Select') {
				alert('Please select an account to submit');
				return;
			}
			if (amount == '' || amount <= 0) {
				alert('Please enter an Amount');
				return;
			}

			$("#AddExpenseForm").submit();
		});
		$('#e_amount').keyup(function () { 
	    	this.value = this.value.replace(/[^0-9\.]/g,'');
		});
		$("#search_patient").on("click", function(e) {
			var mr_id = $("#patient_mr_id").val();
			if (mr_id == '') {
				alert('Please enter patient\'s master record number!');
				return;
			} else {
				$.post('<?=ADMIN_URL;?>patients/get_patient_by_mr_id', {
					mr_id: mr_id,
				}, function(data) {
					$("#t_patient_name").val(data.patient_name);
					$("#t_patient_id").val(data.patient_id);
					if (data.message) {
						showAlertNotification(data);	
					} else {
						clearAlertNotification();
					}
				}, "json");
			}
		});
		$("#issueitem").on('click', function(e) {
			e.preventDefault();
			var sr_item_ids = $("#sr_item_id-1").val();
			var sr_issued = $("#sr_issued_to").val();

			if (sr_item_ids == 'Select') {
				alert('Please select an Item to submit');
				return;
			}
			if (sr_issued == 'Select') {
				alert('Please select Issued To');
				return;
			}

			$("#issueform").submit();
		});
		$("#checkin_modal").on("click",function(e) {
			clearAlertNotification();
			$('#markCheckInModal').appendTo("body").modal('show');
		});
		$("#checkout_modal").on("click",function(e) {
			clearAlertNotification();
			$('#markCheckOutModal').appendTo("body").modal('show');
		});
		$("#add_patient_modal").on("click",function(e) {
			clearAlertNotification();
			$('#addPatientModal').appendTo("body").modal('show');
		});
		$("#issue_item_modal").on("click",function(e) {
			$('#issueItemModal').appendTo("body").modal('show');
		});
		$("#issue_token_modal").on("click", function(e) {
			clearAlertNotification();
			$('#issueTokenModal').appendTo("body").modal('show');
		});
		$("#t_dtr_id").on("change",function(e) {
			getLatestToken();
		});
		$(".cancel_appointment_modal").on("click",function(e) {
			e.preventDefault();
			$("#cancelAppointmentId").val($(this).data('id'));
			$('#cancelModal').appendTo("body").modal('show');
		});	
		$(".rescheduleAppointment").on("click",function(e) {
			e.preventDefault();
			$("#appointment_id").val($(this).data('id'));
			$('#rescheduleAppointmentModal').appendTo("body").modal('show');
		});	
		$("#add_expense_modal").on("click",function(e) {
			$('#addExpenseModal').appendTo("body").modal('show');
		});
		$("#search_patient").on("click", function(e) {
			e.preventDefault();
		});
		$(".attendance_status").on("change", function(e) {
			if ($(this).val() == 'A') {
				$(".absent_reason_description").removeClass('hidden');
			} else {
				$(".absent_reason_description").addClass('hidden');
			}
		});
		$(".appointmentChecked").on("click", function(e) {
			e.preventDefault();
			$("#checkedAppointmentId").val($(this).data('id'));
			$('#checkedAppointmentModal').appendTo("body").modal('show');
		});
		$(".addItemRow").on('click', function(e) {
			e.preventDefault();
			
			var newItemNumber = $(".item").length;
			var new_row_id = ++newItemNumber;
			<?php
	            $items = $this->SqlModel->getItemsDropDown();
	            $itemsList = '';
	            foreach ($items as $key => $value) {
	                $itemsList .= '<option value="'.$value['i_id'].'">'.$value["i_name"].'</option>';
	            }
	        ?>        
			var itemsList = '<?=$itemsList;?>';
			var newItem = '<div class="row item item-'+newItemNumber+'">\
							<div class="form-group col-md-5">\
							    <label class="control-label">Item :<span class="req"> *</span></label>\
							    <select class="form-control items_dropdown" name="sr_item_ids-'+newItemNumber+'" id="sr_item_ids-'+newItemNumber+'">\
				                    <option>Select</option>'+itemsList+'</select>\
							</div>\
							<div class="form-group col-md-6">\
							    <label class="control-label">Quantity :<span class="req"> *</span></label>\
							    <input required type="number" name="sr_qty-'+newItemNumber+'" id="sr_qty" class="form-control " placeholder="Quantity" value="1" data-validate="required"/>\
							</div>\
							<div class="form-group col-md-1" style="padding-left: 7px;padding-right: 7px;">\
	                            <div class="clearfix" style="margin-top: 28px;"></div>\
	                            <label class="control-label" style="visibility: hidden;">.</label>\
	                            <a href="javascript:;" data-item="'+new_row_id+'" id="remove_item-'+new_row_id+'" class="remove_item"><i class="fa fa-times"></i></a>\
	                        </div>\
						</div>';
			$(".items").append(newItem);
			$("#issuedItemCount").val(newItemNumber);
			$("#sr_item_ids-"+new_row_id).select2();
			$(".remove_Item").on('click', function(e) {
				$(this).closest('.item').remove();
			});
		});
	});
</script>
<style type="text/css">
	.bootstrap-timepicker-widget {
		z-index: 1050;
	}
</style>