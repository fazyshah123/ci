<?php
$crumb2 = "";
if(isset($tbl_data['sr_id'])&&$tbl_data['sr_id']!=""){
	$sr_id = $tbl_data['sr_id'];
	$sr_clinic_id = $tbl_data['sr_clinic_id'];
    $sr_date = $tbl_data['sr_date'];
	$sr_item_id = $tbl_data['sr_item_id'];
	$sr_qty = $tbl_data['sr_qty'];
	$sr_issued_to = $tbl_data['sr_issued_to'];
	$sr_description = $tbl_data['sr_description'];
	$sr_added = $tbl_data['sr_added'];
	$sr_updated = $tbl_data['sr_updated'];
	$sr_created_by = $tbl_data['sr_created_by'];
	$sr_updated_by = $tbl_data['sr_updated_by'];
	$sr_is_deleted = $tbl_data['sr_is_deleted'];
	$sr_status = $tbl_data['sr_status'];
    $crumb = "Edit";
    $action = "editRecord/".$tbl_data['sr_id'];
}
else{
	$sr_id = '';
	$sr_clinic_id = '';
    $sr_date = '';
	$sr_item_id = '';
	$sr_qty = '';
	$sr_issued_to = '';
	$sr_description = '';
	$sr_added = '';
	$sr_updated = '';
	$sr_created_by = '';
	$sr_updated_by = '';
	$sr_is_deleted = '';
	$sr_status = '';
    $on_home 		= "No";
    $crumb 			= "Add";
    $action 		= "addRecord";
}
?>
<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo ADMIN_URL;?>">
            <i class="entypo-home"></i>Home
        </a>
    </li>
    <li>
        <a href="<?php echo ADMIN_URL.$this->controller;?>">
            <i></i><?php echo $this->moduleName;?>
        </a>
    </li>
    <li class="active">
        <strong><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></strong>
    </li>
</ol>

<h2><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></h2>
<br />
<div class="panel panel-primary">
    <div class="panel-body">
        <form  id="page_form"
               name="page_form"
               method="post"
               action="<?php echo base_url('manage/'.$this->controller.'/'.$action);?>"
               enctype="multipart/form-data"
               class="validate"
        >
            <div class="form-group">
                <label class="control-label">Item <span style="color: red">*</span> :</label>
                <select required class="form-control items_dropdown" name="sr_item_id" id="sr_item_id">
                    <option value="select1">Select</option>
                <?php
                $items = $this->SqlModel->getItemsDropDown();
                foreach ($items as $key => $value) {
                    $selected = '';
                    if ($value['i_id'] == $sr_item_id) {
                        $selected = 'selected';
                    }
                    echo '<option value="'.$value['i_id'].'" '.$selected.'>'.$value["i_name"].'</option>';
                }
                ?>
                </select>
            </div>

            <div class="form-group">
                <label class="control-label">Quantity <span style="color: red">*</span> :</label>
                <input type="number" name="sr_qty" id="sr_qty" value="<?php echo $sr_qty;?>" class="form-control " placeholder="Quantity" data-validate="required,maxlength[250]"/>
            </div>

            <div class="form-group">
                <label class="control-label" for="sr_issued_to">Issued To <span style="color: red">*</span> :</label>
                <select required id="sr_issued_to"  class="form-control users_dropdown" name="sr_issued_to" id="sr_issued_to"
                data-validate="required,maxlength[250]">
                    <option value="select1">Select</option>
                <?php
                $users = $this->SqlModel->getAdminUsersDropDown();
                foreach ($users as $key => $value) {
                    $selected = '';
                    if ($value['id'] == $sr_issued_to) {
                        $selected = 'selected';
                    }
                    echo '<option value="'.$value['id'].'" '.$selected.'>'.$value["full_name"].'</option>';
                }
                ?>
                </select>
                
            </div>
             <div class="form-group" style="height: 53px;">
                <label class="control-label">Date <span class="req">*</span> :</label>
                <div class="date-and-time">
                    <input style="width: 100%;float: left;" required type="text" name="sr_date" id="sr_date" value="<?=date('d-M-Y', strtotime('now'));?>" autocomplete="off" class="form-control datepicker"  data-format="dd MM yyyy" placeholder="Date"/>
                </div>
            </div>
            
            <div class="form-group" >
                <label class="control-label">Description :</label>
				<?php echo $this->ckeditor->editor("sr_description", $sr_description); ?>
			</div>

            <div class="form-group">
                <button type="button" class="btn btn-danger" onclick="window.location='<?php echo ADMIN_URL;?>stockreleases'">Cancel</button>
                <button type="submit" id="stockrelsubmit"  class="btn btn-success">Submit</button>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    document.addEventListener("DOMContentLoaded", function(event) {
        $("#stockrelsubmit").on('click', function(e){
            e.preventDefault();
            var sr_items = $("#sr_item_id").val();
            var sr_issuedto = $("#sr_issued_to").val();
            
            if (sr_items == 'select1') {
                alert('Please select an Item');
                return;
            }
            if (sr_issuedto == 'select1') {
                alert('Please select Issued To');
                return;
            }
        
            $("#page_form").submit();
        });
    });
</script>
