<div></div><?php
$crumb2 = "";
if(isset($tbl_data['t_id'])&&$tbl_data['t_id']!=""){
	$t_id = $tbl_data['t_id'];
	$t_patient_id = $tbl_data['t_patient_id'];
	$t_doctor_id = $tbl_data['t_doctor_id'];
	$t_number = $tbl_data['t_number'];
	$t_date = $tbl_data['t_date'];
	$t_added = $tbl_data['t_added'];
	$t_updated = $tbl_data['t_updated'];
	$t_created_by = $tbl_data['t_created_by'];
	$t_updated_by = $tbl_data['t_updated_by'];
	$t_status = $tbl_data['t_status'];
	$t_clinic_id = $tbl_data['t_clinic_id'];
    $crumb = "Edit";
    $action = "editRecord/".$tbl_data['t_id'];
}
else{
	$t_id = '';
	$t_patient_id = '';
	$t_doctor_id = '';
	$t_number = '';
	$t_date = '';
	$t_added = '';
	$t_updated = '';
	$t_created_by = '';
	$t_updated_by = '';
	$t_status = '';
	$t_clinic_id = '';
    $on_home 		= "No";
    $crumb 			= "Add";
    $action 		= "addRecord";
}
?>
<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo ADMIN_URL;?>">
            <i class="entypo-home"></i>Home
        </a>
    </li>
    <li>
        <a href="<?php echo ADMIN_URL.$this->controller;?>">
            <i></i><?php echo $this->moduleName;?>
        </a>
    </li>
    <li class="active">
        <strong><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></strong>
    </li>
</ol>

<h2><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></h2>
<br />
<div class="panel panel-primary">
    <div class="panel-body">
        <form  id="page_form"
               name="page_form"
               method="post"
               action="<?php echo base_url('manage/'.$this->controller.'/'.$action);?>"
               enctype="multipart/form-data"
               class="validate"
        >
            <div class="form-group">
                <label class="control-label">Patient :<span class="req"> *</span></label>
                <?php
                    if ($crumb == 'Edit') {
                        $disabled = 'disabled';
                    }
                ?>
                <select class="form-control" name="t_patient_id" id="t_patient_id" <?=$disabled;?> id="t_patient_id">
                <option value="Select">Select</option>
                <?php
                foreach ($tbl_data['patients'] as $key => $value) {
                    ?>
                        <option value="<?=$value['patient_id'];?>" <?php if($value['patient_id']==$t_patient_id){ echo ' selected="selected"';} ?>><?=$value['patient_name'];?></option>
                    <?php  
                }
                ?>
                </select>
            </div>

            <div class="form-group">
                <label class="control-label">Doctor :<span class="req"> *</span></label>
                <select class="form-control"  name="t_doctor_id" id="t_doctor_id">
                    <option value="Select">Select</option>
                <?php
                $doctors = $this->SqlModel->getDoctorsDropDown();
                foreach ($doctors as $key => $value) {
                    $selected = '';
                    if ($value['id'] == $t_doctor_id) {
                        $selected = 'selected';
                    }
                    echo '<option value="'.$value['id'].'" '.$selected.'>Dr. '.$value["full_name"].'</option>';
                }
                ?>
                </select>
            </div>

            <div class="form-group">
                <label class="control-label">Number :<span class="req"> *</span></label>
                <input required type="text" name="t_number" id="t_number" value="<?php echo $t_number;?>" class="form-control " placeholder="Number" data-validate="required,maxlength[250]"/>
            </div>


            <div class="form-group">
                <button type="button" class="btn btn-danger" onclick="window.location='<?php echo ADMIN_URL;?>tokens'">Cancel</button>
                <button id="token_submit" type="submit" class="btn btn-success">Submit</button>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    document.addEventListener("DOMContentLoaded", function(event) {
        $("#token_submit").on('click', function(e) {
            e.preventDefault();
            var t_patient_i = $("#t_patient_id").val();
            if (t_patient_i == 'Select') {
                alert('Please select a Patient');
                return;
            }
            var pc_ro = $("#t_doctor_id").val();

            
            if (pc_ro == 'Select') {
                alert('Please select a Doctor');
                return;
            }

            $("#page_form").submit();

        });
    });
</script>