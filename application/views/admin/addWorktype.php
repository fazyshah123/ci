<?php
$crumb2 = "";
if(isset($tbl_data['wt_id'])&&$tbl_data['wt_id']!=""){
	$wt_id = $tbl_data['wt_id'];
    $wt_name = $tbl_data['wt_name'];
    $wt_status = $tbl_data['wt_status'];
    $crumb = "Edit";
    $action = "editRecord/".$tbl_data['wt_id'];
}
else{
	$wt_id = '';
    $wt_name = '';
    $wt_status = '';
    $on_home 		= "No";
    $crumb 			= "Add";
    $action 		= "addRecord";
}
?>
<style type="text/css">
    input, select {
        border: 1px solid #a5a4a4 !important;
    }
</style>
<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo ADMIN_URL;?>">
            <i class="entypo-home"></i>Home
        </a>
    </li>
    <li>
        <a href="<?php echo ADMIN_URL.$this->controller;?>">
            <i></i><?php echo $this->moduleName;?>
        </a>
    </li>
    <li class="active">
        <strong><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></strong>
    </li>
</ol>

<h2><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></h2>
<br />
<div class="panel panel-primary">
    <div class="panel-body">
        <form  id="page_form"
               name="page_form"
               method="post"
               action="<?php echo base_url('manage/'.$this->controller.'/'.$action);?>"
               enctype="multipart/form-data"
               class="validate"
        >

            <div class="form-group col-md-12 col-xs-12 col-lg-12">
                <label class="control-label">Work Type Name <span style="color: red">*</span> :</label>
                <input required type="text" name="wt_name" id="wt_name" value="<?php echo $wt_name;?>" class="form-control " placeholder="Work Type Name" data-validate="maxlength[250]"/>
            </div>

            <div class="clearfix"></div>

            <div class="form-group col-md-12 col-xs-12 col-lg-12 hidden">
                <label class="control-label">Status :</label>
                <select class="form-control" name="wt_status" id="wt_status">
                    <option value="Enable" <?php if($wt_status=="Enable"){ echo ' selected="selected"';} ?>>Enable</option>
                    <option value="Disable" <?php if($wt_status=="Disable"){ echo ' selected="selected"';} ?>>Disable</option>
                </select>
            </div>
            
            <div class="clearfix"></div>
            
            <div class="form-group col-md-12 col-xs-12 col-lg-12">
                <button type="button" class="btn btn-danger" onclick="window.location='<?php echo ADMIN_URL;?>worktype'">Cancel</button>                         
                <button id="workt_status" type="submit"  class="btn btn-success">Submit</button>                                                      
            </div>    
        </form>
    </div>
</div>