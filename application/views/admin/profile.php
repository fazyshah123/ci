<?php
	$reporttypelist = '';
	$reporttypes = $this->SqlModel->getReportTypeDropDown();
	foreach ($reporttypes as $key => $value) {
		$reporttypelist .= '<option value="'.$value['r_id'].'">'.$value['r_name'].'</option>';
	}
?>
<link rel="stylesheet" href="<?php echo ADMIN_ASSETS;?>css/profile.css">
<div id="content">
	<div id="content-header">
		<?php if($alert=="filesuccess") { ?>
      	<div class="row alertrow">
			<div class="col-md-12">
		    <button class="close alertBox" data-dismiss="alert">x</button>
				<div class="alert alert-success"><strong class="responseText">Success </strong>File(s) added successfully!</div>
			</div>
		</div>
	    <?php } if($alert=="fileerror" && $alertmsg=="") { ?>
      	<div class="row alertrow">
			<div class="col-md-12">
		    <button class="close alertBox" data-dismiss="alert">x</button>
				<div class="alert alert-danger"><strong class="responseText">Error </strong>Unable to upload file due to some error!</div>
			</div>
		</div>
	   <?php } if($alertmsg!="" && $alert=="fileerror") { ?>
      	<div class="row alertrow">
			<div class="col-md-12">
		    <button class="close alertBox" data-dismiss="alert">x</button>
				<div class="alert alert-danger"><strong class="responseText">Error </strong>$alertmsg!</div>
			</div>
		</div>
	   <?php } if($alert=="alertmsg") { ?>
      	<div class="row alertrow">
			<div class="col-md-12">
		    <button class="close alertBox" data-dismiss="alert">x</button>
				<div class="alert alert-danger"><strong class="responseText">Error </strong>Unable to upload file due to some error!</div>
			</div>
		</div>
	   <?php } ?>
		<h1>Patient Profile</h1>
	</div> <!-- #content-header -->	
	<div id="content-container">


		<div class="row">

			<div class="col-md-9">

				<div class="row">

					<div class="col-md-4 col-sm-5">

						<div class="thumbnail">
							<?php
							$image_url = ($patient_info['patient_image'] == '' ? 'profile-placeholder.png' : 'patients/'.$patient_info['patient_image']);
							?>
							<img src="<?=ADMIN_URL;?>../assets/frontend/images/<?=$image_url;?>" alt="Profile Picture">
						</div>

						<br>

						<div class="list-group">  

							<a href="<?php echo base_url('manage/patients/control/edit/'.$patient_info['patient_id']);?>" class="list-group-item">
								<i class="fa fa-pencil"></i> &nbsp;&nbsp;Edit Profile
							</a> 

							<a href="javascript:window.location='<?php echo base_url('manage/appointments/control/new/'.$patient_info['patient_id']);?>'" class="list-group-item">
								<i class="fa fa-user"></i> &nbsp;&nbsp;Add Appointment
							</a> 

							<a href="javascript:" class="list-group-item">
								<i class="fa fa-folder-open"></i> &nbsp;&nbsp;Add Health Record
							</a> 

							<a href="javascript:;" class="list-group-item">
								<i class="fa fa-flask"></i> &nbsp;&nbsp;Add Medical History
							</a> 

							<a href="javascript:;" id="openPatientFileUploadDialouge" class="list-group-item">
								<i class="fa fa-file"></i> &nbsp;&nbsp;Add Diagnosis Report
							</a> 

							<a href="<?=ADMIN_URL.'invoices/control/?patient_id='.$patient_info['patient_id'];?>" class="list-group-item">
								<i class="fa fa-credit-card"></i> &nbsp;&nbsp;Add Invoice
							</a> 

							<a href="<?=ADMIN_URL.'roompatient/control/?patient_id='.$patient_info['patient_id'];?>" class="list-group-item">
								<i class="fa fa-credit-card"></i> &nbsp;&nbsp;Allot Room
							</a> 

							<a href="<?=ADMIN_URL.'bedpatient/control/?patient_id='.$patient_info['patient_id'];?>" class="list-group-item">
								<i class="fa fa-credit-card"></i> &nbsp;&nbsp;Allot Bed
							</a> 

							<a href="javascript:;" class="list-group-item hidden">
								<i class="fa fa-clock-o"></i> &nbsp;&nbsp;Invoice History
							</a> 

							<a href="javascript:;" class="list-group-item hidden">
								<i class="medical-icon-i-dental"></i> &nbsp;&nbsp;Dental Chart
							</a> 

							<a href="javascript:;" class="list-group-item hidden">
								<i class="fa fa-location-arrow"></i> &nbsp;&nbsp;Treatment Plan
							</a> 

							<a href="javascript:;" class="list-group-item hidden">
								<i class="fa fa-envelope"></i> &nbsp;&nbsp;Send Message
							</a> 
						</div> <!-- /.list-group -->

					</div> <!-- /.col -->


					<div class="col-md-8 col-sm-7">

						<h3><?=$patient_info['patient_name'];?> (MR # <?=$patient_info['patient_mr_id'];?>)</h3>
						<hr>

						<div class="row">
							<div class="col-md-6">
								<ul class="icons-list">
									<?php
									if (isset($patient_info['patient_phone1']) && !empty($patient_info['patient_phone1'])) { ?>
										<li><i class="icon-li fa fa-phone"></i><?=$patient_info['patient_phone1'];?></li>
									<?php }
										if (isset($patient_info['patient_phone2']) && !empty($patient_info['patient_phone2'])) { ?>
										<li><i class="icon-li fa fa-phone"></i><?=$patient_info['patient_phone2'];?></li>
									<?php }
										if (isset($patient_info['patient_phone3']) && !empty($patient_info['patient_phone3'])) { ?>
										<li><i class="icon-li fa fa-phone"></i><?=$patient_info['patient_phone3'];?></li>
									<?php } ?>
								</ul>									
							</div>
							<div class="col-md-6">
								<ul class="icons-list">
									<?php
									if (isset($patient_info['patient_email']) && !empty($patient_info['patient_email'])) { ?>
									<li><i class="icon-li fa fa-envelope"></i><?=$patient_info['patient_email'];?></li>
									<?php }
									if (isset($patient_info['patient_address']) && !empty($patient_info['patient_address'])) { ?>
									<li><i class="icon-li fa fa-map-marker"></i><?=$patient_info['patient_address'];?></li>
									<?php }
									if (isset($patient_info['patient_age']) && !empty($patient_info['patient_age'])) { ?>
										<li><i class="icon-li fa fa-calendar"></i><?=$patient_info['patient_age'];?> years old</li>
									<?php } ?>
								</ul>									
							</div>
						</div>


						<hr>

						<h3 class="heading">History</h3>
						<div class="timeline_items">
						</div>
						<div class="timeline_show_more_link">
							<center>
								<a class="btn btn-xs btn-success" href="javascript:;" id="show_more_items">Show More</a>
								<input type="hidden" id="input_id">
							</center>
						</div>

						<?php /*<div class="timeline">
							<div class="feed-item feed-item-idea">

								<div class="feed-icon">
									<i class="fa fa-lightbulb-o"></i>
								</div> <!-- /.feed-icon -->

								<div class="feed-subject">
									<p><a href="javascript:;">Rod Howard</a> shared an idea: <a href="javascript:;">Create an Awesome Idea</a></p>
								</div> <!-- /.feed-subject -->

								<div class="feed-content">
									<ul class="icons-list">
										<li>
											<i class="icon-li fa fa-quote-left"></i>
											Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes.
										</li>
									</ul>
								</div> <!-- /.feed-content -->

								<div class="feed-actions">
									<a href="javascript:;" class="pull-right"><i class="fa fa-clock-o"></i> 2 days ago</a>
								</div> <!-- /.feed-actions -->

							</div> <!-- /.feed-item -->

							<div class="feed-item feed-item-image">

								<div class="feed-icon">
									<i class="fa fa-picture-o"></i>
								</div> <!-- /.feed-icon -->

								<div class="feed-subject">
									<p><a href="javascript:;">John Doe</a> posted the <strong>4 files</strong>: <a href="javascript:;">Annual Reports</a></p>
								</div> <!-- /.feed-subject -->

								<div class="feed-content">
									<div class="thumbnail" style="width: 375px">
										<div class="thumbnail-view">
			                        		<a class="thumbnail-view-hover ui-lightbox" href="<?=ADMIN_URL;?>../assets/frontend/images/mockup.png">
			                        		</a>

								            <img src="<?=ADMIN_URL;?>../assets/frontend/images/mockup.png" style="width: 100%;" alt="Gallery Image">
								        </div>

									</div> <!-- /.thumbnail -->
								</div> <!-- /.feed-content -->

								<div class="feed-actions">
									<a href="javascript:;" class="pull-right"><i class="fa fa-clock-o"></i> 2 days ago</a>
								</div> <!-- /.feed-actions -->

							</div> <!-- /.feed-item -->

							<div class="feed-item feed-item-file">

								<div class="feed-icon">
									<i class="fa fa-cloud-upload"></i>
								</div> <!-- /.feed-icon -->

								<div class="feed-subject">
									<p><a href="javascript:;">John Doe</a> posted the <strong>4 files</strong>: <a href="javascript:;">Annual Reports</a></p>
								</div> <!-- /.feed-subject -->

								<div class="feed-content">
									<ul class="icons-list">
										<li>
											<i class="icon-li fa fa-file-text-o"></i>
											<a href="javascript:;">Annual Report 2007</a> - annual_report_2007.pdf
										</li>
										<li>
											<i class="icon-li fa fa-file-text-o"></i>
											<a href="javascript:;">Annual Report 2008</a> - annual_report_2007.pdf
										</li>
										<li>
											<i class="icon-li fa fa-file-text-o"></i>
											<a href="javascript:;">Annual Report 2009</a> - annual_report_2007.pdf
										</li>
										<li>
											<i class="icon-li fa fa-file-text-o"></i>
											<a href="javascript:;">Annual Report 2010</a> - annual_report_2007.pdf
										</li>
									</ul>
								</div> <!-- /.feed-content -->

								<div class="feed-actions">
									<a href="javascript:;" class="pull-right"><i class="fa fa-clock-o"></i> 2 days ago</a>
								</div> <!-- /.feed-actions -->

							</div> <!-- /.feed-item -->

							<div class="feed-item feed-item-bookmark">

								<div class="feed-icon">
									<i class="fa fa-bookmark"></i>
								</div> <!-- /.feed-icon -->

								<div class="feed-subject">
									<p><a href="javascript:;">Rod Howard</a> bookmarked a page on Delicious: <a href="javascript:;">Jumpstart Themes</a></p>
								</div> <!-- /.feed-subject -->

								<div class="feed-content">
							
								</div> <!-- /.feed-content -->

								<div class="feed-actions">
									<a href="javascript:;" class="pull-right"><i class="fa fa-clock-o"></i> 2 days ago</a>
								</div> <!-- /.feed-actions -->

							</div> <!-- /.feed-item -->

							<div class="feed-item feed-item-question">

								<div class="feed-icon">
									<i class="fa fa-question"></i>
								</div> <!-- /.feed-icon -->

								<div class="feed-subject">
									<p><a href="javascript:;">Rod Howard</a> posted the question: <a href="javascript:;">Who can I call to get a new parking pass for the Rowan Building?</a></p>
								</div> <!-- /.feed-subject -->

								<div class="feed-content">
									<ul class="icons-list">
										<li>
											<i class="icon-li fa fa-quote-left"></i>
											Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes.
										</li>
									</ul>
								</div> <!-- /.feed-content -->

								<div class="feed-actions">
									<a href="javascript:;" class="pull-right"><i class="fa fa-clock-o"></i> 2 days ago</a>
								</div> <!-- /.feed-actions -->

							</div> <!-- /.feed-item -->

						</div>*/?>
					</div>

				</div>

			</div>


			<div class="col-md-3 col-sm-6 col-sidebar-right">
				<!-- <div class="well">
					<h4>Medical History</h4>
					<ul class="icons-list text-md">
						<li>
							<i class="icon-li fa fa-location-arrow"></i>
							<strong><a href="#">ABC.png</a></strong> 
						</li>

						<li>
							<i class="icon-li fa fa-location-arrow"></i>

							<strong>Rod</strong> followed the research interest: <a href="javascript:;">Open Access Books in Linguistics</a>. 
							<br>
							<small>about 23 hours ago</small>
						</li>

						<li>
							<i class="icon-li fa fa-location-arrow"></i>

							<strong>Rod</strong> added 51 papers. 
							<br>
							<small>2 days ago</small>
						</li>
					</ul>

				</div> -->
				<?php 
				$reports = $this->SqlModel->getPatientReports($patient_info['patient_id']);
				if (count($reports) != 0) :?>
				<div class="well">
					<h4><strong>Reports</strong></h4>
					<div class="icons-list text-md">
						<?php 
						foreach ($reports as $key => $value): ?>
							<li><a target="_blank" href="<?=FRONTEND_ASSETS.'images/patientfiles/'.$value['pf_file'];?>"><?=' ('.$value['r_name'].') ('.date('d-M-Y h:i A', strtotime($value['pf_added'])).') ';?></a></li>
						<?php endforeach ?>
					</div>
				</div>
				<?php endif; 

				// Patient Cases List Start
				$cases = $this->SqlModel->getPatientCases($patient_info['patient_id']);
				if (count($cases) != 0) :?>
				<div class="well">
					<h4><strong>Cases</strong></h4>
					<div class="icons-list text-md">
						<?php 
						foreach ($cases as $key => $value): ?>
							<li><a href="<?=ADMIN_URL.'patientcase/details/'.$value['pc_case_number'];?>"><?=' (Case # '.$value['pc_case_number'].') ('.date('d-M-Y h:i A', strtotime($value['pc_created'])).') ';?></a></li>
						<?php endforeach ?>
					</div>
				</div>
				<?php 
				endif; 
				// Patient Cases List End
				?>
		</div>


		</div> <!-- /.row -->
	</div> <!-- /#content-container -->
</div>
<!-- Modal -->
<div class="modal fade" id="uploadPatientFile">
	<div class="modal-dialog">
		<div class="modal-content">
			<form  id="page_form"
		               name="page_form"	
		               method="post"
		               action="<?php echo base_url('manage/patients/addPatientFiles/'.$patient_info['patient_id']);?>"
		               enctype="multipart/form-data"
		               class="validate"
		        >
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Add Diagnosis Report</h4>
				</div>
				
				<div class="modal-body">
					<div class="items">
			            <div class="item">
			            	<div class="form-group">
				                <div class="row" style="height: 60px;">
			            			<label style="padding: 0px 15px;" for="pf_file-1" class="control-label">Document 1:<span class="req"> *</span></label>
			            			<div class="clearfix"></div>
					                <div class="col-md-10">
					                	<input required type="file" name="pf_file-1" id="pf_file-1" class="form-control"/>
					                	<input type="hidden" name="patient_id" id="patient_id" class="form-control"/>
					                	<input type="hidden" name="total_items" id="total_items" value="1" class="form-control"/>
					                </div>
					                <div class="col-md-2">
					                	
					                </div>
				                </div>
				                <div class="clearfix"></div>
				                <div class="row" style="height: 60px;">
			            			<label style="padding: 0px 15px;" for="pf_file-1" class="control-label">Report Type 1:<span class="req"> *</span></label>
			            			<div class="clearfix"></div>
					                <div class="col-md-10">
					                	<select name="pf_reporttype-1" id="pf_reporttype-1" class="form-control">
					                		<option value="Select">Select</option>
					                		<?=$reporttypelist;?>
					                	</select>
					                </div>
				                </div>
				                <div class="row" style="height: 60px;">
			            			<label style="padding: 0px 15px;" for="pf_description-1" class="control-label">Description 1:</label>
			            			<div class="clearfix"></div>
					                <div class="col-md-10">
					                	<textarea name="pf_description-1" id="pf_description-1" class="form-control" rows="2"></textarea>
					                </div>
				                </div>
			            	</div>
			            </div>
					</div>

		            <div class="">
		                <a class="btn btn-success pull-right" id="add_item_row" href="javascript:;">Add Item</a>
		            </div>
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					<button type="submit" id="deleteLink" class="btn btn-success">Confirm</button>
				</div>
	        </form>
		</div>
	</div>
</div>
<!-- Model -->
<script>
	var patient_id = '<?=$patient_info['patient_id'];?>';
	var reportTypeList = '<?=$reporttypelist;?>';
</script>