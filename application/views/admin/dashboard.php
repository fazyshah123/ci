<?php 
$totalAppointments = $this->SqlModel->getTodaysAppointmentCount();
$totalDoctors = $this->SqlModel->getTotalDoctors();
$totalPatients = $this->SqlModel->getTotalPatients();
//$totalDues = $this->SqlModel->getTotalDues();
?>
<div class="row" id="upcomingTokensContainer">
	<marquee onMouseOver="document.getElementById('scroll_news').stop();" onMouseOut="document.getElementById('scroll_news').start();" id="scroll_news" style="font-size: 14px;" behavior="scroll" loop="infinite" direction="left"><strong>UPCOMING TOKENS: <span id="upcomingTokens"></span></strong></marquee>
</div>
<div class="row" id="quickActions" style="margin-bottom: 10px;">
	<div class="col-md-12">
		<!-- <h4 style="text-align: center;color: red;"><strong>Press<span style="font-size: 25px;">`</span>  Button For Shortcut's</strong> </h4> -->
		<h4 style="margin-top: 0px"><strong>Quick Actions</strong></h4>
		<style type="text/css">
			.quick-action-btns {
				margin-bottom: 3px;
			}
		</style>

		<?php
		if($this->SqlModel->checkPermissions('employeeattendance','create')==true)
		{?>
		<button id="checkin_modal" style="margin-left: 0px" class="btn btn-success quick-action-btns"><i class="fa fa-list"></i> Mark Check-In</button>
		<button id="checkout_modal" class="btn btn-success quick-action-btns"><i class="fa fa-list"></i> Mark Check-Out</button>
		<?php }
		if($this->SqlModel->checkPermissions('purchases','create')==true)
		{?>
		<a id="addPurchaseOrder" href="<?=ADMIN_URL;?>purchases/addpurchaseorder" class="btn btn-success quick-action-btns"><i class="fa fa-dropbox"></i> Purchase Order</a>
		<?php }
		if($this->SqlModel->checkPermissions('tokens','create')==true)
		{?>
		<button id="issue_token_modal" class="btn btn-success quick-action-btns"><i class="fa fa-tag"></i> Issue Token</button>
		<?php }
		if($this->SqlModel->checkPermissions('expenses','create')==true)
		{?>
		<button id="add_expense_modal" class="btn btn-success quick-action-btns"><i class="fa fa-money"></i> Add Expense</button>
		<?php }
		if($this->SqlModel->checkPermissions('items','issue')==true)
		{?>
		<button id="issue_item_modal" class="btn btn-success quick-action-btns"><i class="fa fa-external-link"></i> Issue Item</button>
		<?php }
		if($this->SqlModel->checkPermissions('patients','create')==true)
		{?>
		<button id="add_patient_modal" class="btn btn-success quick-action-btns"><i class="fa fa-wheelchair"></i> Add Patient</button>

		<?php }
		if($this->SqlModel->checkPermissions('appointments','create')==true)
		{ ?>
		<a id="makeappointment" href="<?=ADMIN_URL;?>appointments/control" class="btn btn-success quick-action-btns"><i class="fa fa-calendar"></i> Make Appointment</a>
		
		<?php }
		if($this->SqlModel->checkPermissions('appointments','create')==true)
		{ ?>
		<a id="AppointmentCheck" href="<?=ADMIN_URL;?>appointments" class="btn btn-success quick-action-btns"><i class="fa fa-clock-o"></i> Appointment Check</a>

		
		<?php }
		if($this->SqlModel->checkPermissions('invoices','create')==true)
		{ ?>
		<a id="Invoice" href="<?=ADMIN_URL;?>invoices/control" class="btn btn-success quick-action-btns"><i class="fa fa-pencil"></i> Invoice</a>
		<?php } ?>
	</div>
</div>
<div class="row" id="dashboardCards">
	<?php
	if($this->SqlModel->checkPermissions('admins','read')==true)
	{?>
	<div class="col-xs-6 col-sm-3 adminTileHeight150" onclick="javascript:window.location='<?php echo base_url('manage/admins/');?>'">
		<div class="tile-stats tile-cyan">
			<div class="icon"><i class="fa fa-user-md"></i></div>
			<div class="num" data-start="0" data-end="0" data-postfix="" data-duration="1500" data-delay="0"><?=$totalDoctors[0]['count'];?></div>
			<h3>Doctors</h3>
		</div>
	</div>
	<?php }
	if($this->SqlModel->checkPermissions('patients','read')==true)
	{?>
	<div id="patientCount" class="col-xs-6 col-sm-3 adminTileHeight150" onclick="javascript:window.location='<?php echo base_url('manage/patients/');?>'">
		<div class="tile-stats tile-pink">
			<div class="icon"><i class="fa fa-wheelchair"></i></div>
			<div class="num" data-start="0" data-end="0" data-postfix="" data-duration="1500" data-delay="0"><?=$totalPatients[0]['count'];?></div>
			<h3>Patients</h3>
		</div>
	</div>
    <?php }
	if($this->SqlModel->checkPermissions('appointments','read')==true)
	{?>
	<div id="appointmentcount" class="col-xs-6 col-sm-3 adminTileHeight150" onclick="javascript:window.location='<?php echo base_url('manage/appointments/');?>'">
		<div class="tile-stats tile-purple">
			<div class="icon"><i class="fa fa-book"></i></div>
			<div class="num" data-start="0" data-end="0" data-postfix="" data-duration="1500" data-delay="0"><?=$totalAppointments[0]['count'];?></div>
			<h3>Appointments</h3>
		</div>
	</div>
    <?php }
	if($this->SqlModel->checkPermissions('invoices','read')==true)
	{?>
	<div class="col-xs-6 col-sm-3 adminTileHeight150" onclick="javascript:window.location='<?php echo base_url('manage/invoices/');?>'">
		<div class="tile-stats tile-orange">
			<div class="icon"><i class="fa fa-money"></i></div>
			<div class="num" data-start="0" data-end="0" data-postfix="" data-duration="1500" data-delay="0"><?='0';//$totalDues[0]['count'];?></div>
			<h3>Dues</h3>
		</div>
	</div>
    <?php } ?>
</div>
<div class="row" id="tables">	
	<?php include_once('show_appointments.php');?>
</div>
<?php include_once('dashboard_modal.php');?>