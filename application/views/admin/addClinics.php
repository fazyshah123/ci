<?php
$crumb2 = "";
if(isset($tbl_data['clinic_id'])&&$tbl_data['clinic_id']!=""){
	$clinic_id = $tbl_data['clinic_id'];
    $clinic_name = $tbl_data['clinic_name'];
    $clinic_address = $tbl_data['clinic_address'];
    $clinic_status = $tbl_data['clinic_status'];
    $clinic_email1 = $tbl_data['clinic_email1'];
    $clinic_email2 = $tbl_data['clinic_email2'];
    $clinic_phone1 = $tbl_data['clinic_phone1'];
    $clinic_phone2 = $tbl_data['clinic_phone2'];
    $clinic_mobile1 = $tbl_data['clinic_mobile1'];
    $clinic_mobile2 = $tbl_data['clinic_mobile2'];
    $clinic_owner = $tbl_data['clinic_owner'];
    $clinic_refered_by = $tbl_data['clinic_refered_by'];
    $clinic_purchase_date = date('d F Y', strtotime($tbl_data['clinic_purchase_date']));
    $clinic_subs_token = $tbl_data['clinic_subs_token'];
    $clinic_added = $tbl_data['clinic_added'];
    $clinic_updated = $tbl_data['clinic_updated'];
    $clinic_added_by = $tbl_data['clinic_added_by'];
    $clinic_updated_by = $tbl_data['clinic_updated_by'];
    $clinic_website = $tbl_data['clinic_website'];
    $clinic_deleted = $tbl_data['clinic_deleted'];
    $clinic_subs_plan_id = $tbl_data['clinic_subs_plan_id'];
	$crumb = "Edit";
    $action = "editRecord/".$tbl_data['clinic_id'];
}
else{
	$clinic_id = '';
    $clinic_owner = '';
    $clinic_name = '';
    $clinic_address = '';
    $clinic_status = '';
    $clinic_email1 = '';
    $clinic_email2 = '';
    $clinic_phone1 = '';
    $clinic_phone2 = '';
    $clinic_mobile1 = '';
    $clinic_mobile2 = '';
    $clinic_refered_by = '';
    $clinic_purchase_date = date('d F Y', strtotime('now'));
    $clinic_subs_token = '';
    $clinic_added = '';
    $clinic_updated = '';
    $clinic_added_by = '';
    $clinic_updated_by = '';
    $clinic_website = '';
    $clinic_deleted = '';
    $clinic_subs_plan_id = '';
    $on_home 		= "No";
    $crumb 			= "Add";
    $action 		= "addRecord";
}
?>
<style type="text/css">
    input {
        border: 1px solid #a5a4a4 !important;
    }
</style>
<ol class="breadcrumb bc-3">
<li><a href="<?php echo ADMIN_URL;?>"><i class="entypo-home"></i>Home</a></li>
<li><a href="<?php echo ADMIN_URL.$this->controller;?>"><i></i><?php echo $this->moduleName;?></a></li>
<li class="active"><strong><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></strong></li>
</ol>

<h2><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></h2>
<br />
<style type="text/css">
    .form-group {
        padding-left: 0px !important;
        padding-right: 0px !important;
    }
    .label-padding-right {
        padding-right: 15px;
    }
</style>
<div class="panel panel-primary">
    <div class="panel-body">
        <form  id="page_form"
               name="page_form"
               method="post"
               action="<?php echo base_url('manage/'.$this->controller.'/'.$action);?>"
               enctype="multipart/form-data"
               class="validate"
        >
            <div class="row">

                <div class="col-md-6 col-xs-12 col-lg-6">
                    
                    <div class="form-group col-md-12 col-xs-12 col-lg-12">
                        <label class="control-label">Clinic Name <span style="color:red;">* </span>:</label>
                        <input required type="text" name="clinic_name" id="clinic_name" value="<?php echo $clinic_name;?>" class="form-control " placeholder="Clinic Name" data-validate="maxlength[250]"/>
                        <input type="hidden" name="clinic_id" <?=$clinic_id;?>>
                    </div>

                    <div class="form-group col-md-12 col-xs-12 col-lg-12">
                        <label class="control-label">Owner Name <span style="color:red;">* </span>:</label>
                        <input required type="text" name="clinic_owner" id="clinic_owner" value="<?php echo $clinic_owner;?>" class="form-control " placeholder="Owner Name" data-validate="maxlength[250]"/>
                    </div>

                    <div class="form-group col-md-12 col-xs-12 col-lg-12">
                        <label class="control-label">Phone 1 <span style="color:red;">* </span>:</label>
                        <input required type="text" name="clinic_phone1" id="clinic_phone1" value="<?php echo $clinic_phone1;?>" class="form-control " placeholder="Phone 1" data-validate="maxlength[250]"/>
                    </div>

                    <div class="form-group col-md-12 col-xs-12 col-lg-12">
                        <label class="control-label">Phone 2 :</label>
                        <input type="text" name="clinic_phone2" id="clinic_phone2" value="<?php echo $clinic_phone2;?>" class="form-control " placeholder="Phone 2" data-validate="maxlength[250]"/>
                    </div>

                    <div class="form-group col-md-12 col-xs-12 col-lg-12">
                        <label class="control-label">Mobile 1 <span style="color:red;">* </span>:</label>
                        <input required type="text" name="clinic_mobile1" id="clinic_mobile1" value="<?php echo $clinic_mobile1;?>" class="form-control " placeholder="Mobile 1" data-validate="maxlength[250]"/>
                    </div>

                    <div class="form-group col-md-12 col-xs-12 col-lg-12">
                        <label class="control-label">Mobile 2 :</label>
                        <input type="text" name="clinic_mobile2" id="clinic_mobile2" value="<?php echo $clinic_mobile2;?>" class="form-control " placeholder="Mobile 2" data-validate="maxlength[250]"/>
                    </div>

                    <div class="form-group col-md-12 col-xs-12 col-lg-12">
                        <label class="control-label">Subs. Token :</label>
                        <input type="text" name="clinic_subs_token" id="clinic_subs_token" value="<?php echo $clinic_subs_token;?>" class="form-control " placeholder="Subs. Token" data-validate="maxlength[250]"/>
                    </div>
                            
                </div>

                <div class="col-md-6 col-xs-12 col-lg-6">
        

                    <div class="form-group col-md-12 col-xs-12 col-lg-12">
                        <label class="control-label">Email 1 :</label>
                        <input type="text" name="clinic_email1" id="clinic_email1" value="<?php echo $clinic_email1;?>" class="form-control " placeholder="Email 1" data-validate="maxlength[250]"/>
                    </div>

                    <div class="form-group col-md-12 col-xs-12 col-lg-12">
                        <label class="control-label">Email 2 :</label>
                        <input type="text" name="clinic_email2" id="clinic_email2" value="<?php echo $clinic_email2;?>" class="form-control " placeholder="Email 2" data-validate="maxlength[250]"/>
                    </div>

                    <div class="form-group col-md-12 col-xs-12 col-lg-12">
                        <label class="control-label">Address :</label>
                        <input type="text" name="clinic_address" id="clinic_address" value="<?php echo $clinic_address;?>" class="form-control " placeholder="Address" data-validate="maxlength[250]"/>
                    </div>

                    <div class="form-group col-md-12 col-xs-12 col-lg-12" style="height: 53px">
                        <label class="control-label">Purchase Date <span style="color:red;">* </span>:</label>
                        <div class="date-and-time">
                            <input style="width: 100%;height: 31px;" required type="text" name="clinic_purchase_date" id="clinic_purchase_date" value="<?php echo $clinic_purchase_date;?>" autocomplete="off" class="form-control datepicker"  data-format="dd MM yyyy" placeholder="Purchase Date"/>
                        </div>
                    </div>

                    <div class="form-group col-md-12 col-xs-12 col-lg-12">
                        <label class="control-label">Website :</label>
                        <input type="text" name="clinic_website" id="clinic_website" value="<?php echo $clinic_website;?>" class="form-control " placeholder="Website" data-validate="maxlength[250]"/>
                    </div>

                    <div class="form-group col-md-12 col-xs-12 col-lg-12">
                        <label class="control-label">Property Status :</label>
                        <select class="form-control" name="clinic_status" id="clinic_status">
                            <option selected value="Enabled" <?php if('Enabled'==$clinic_status){ echo ' selected="selected"';} ?>>Enabled</option>
                            <option value="Disabled" <?php if('Disabled'==$clinic_status){ echo ' selected="selected"';} ?>>Disabled</option>
                        </select>
                    </div>

                    <div class="form-group col-md-12 col-xs-12 col-lg-12">
                        <label class="control-label">Subscription Plan :</label>
                        <select class="form-control" name="clinic_subs_plan_id" id="clinic_subs_plan_id">
                            <option value="1" <?php if('1'==$clinic_subs_plan_id){ echo ' selected="selected"';} ?>>Infant (Rs. 5000 PKR)</option>
                        </select>
                    </div>
            
                </div>
            </div>

            <div class="form-group col-md-12 col-xs-12 col-lg-12">
                <label class="control-label">Refered By :</label>
                <input type="text" name="clinic_refered_by" id="clinic_refered_by" value="<?php echo $clinic_refered_by;?>" class="form-control " placeholder="Refered By"/>
            </div>

            
            <div class="clearfix"></div>
            <div class="row">
                <div class="form-group col-md-12 col-xs-12 col-lg-12">
                    <button type="button" class="btn btn-danger" onclick="window.location='<?php echo ADMIN_URL;?>clinic'">Cancel</button>
                    <button type="submit" class="btn btn-success submitForm">Submit</button>
                </div>    
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    document.addEventListener("DOMContentLoaded", function(event) {
        $(document).keydown(function(e) {
            // If Control or Command key is pressed and the S key is pressed
            // run save function. 83 is the key code for S.
            if((e.ctrlKey || e.metaKey) && (e.which == 83 || e.which == 115)) {
                // Save Function
                e.preventDefault();
                //alert('Form about to submit');
                $(".submitForm").submit();
                return false;
            }
        });
    });
</script>