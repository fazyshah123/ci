<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title><?php echo (isset($page_title)) ? $page_title : PROJECT_TITLE;?></title>
	<!--<link rel="stylesheet" href="<?php echo ADMIN_ASSETS;?>css/no-theme/jquery-ui-1.10.3.custom.min.css">-->
	<link rel="stylesheet" href="<?php echo ADMIN_ASSETS;?>css/font-icons/entypo/css/entypo.css">
	<link rel="stylesheet" href="<?php echo ADMIN_ASSETS;?>css/font-icons/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?php echo ADMIN_ASSETS;?>css/font-icons/entypo/css/animation.css">
	<link rel="stylesheet" href="<?php echo ADMIN_ASSETS;?>css/question.mark.css">
	
	<!--<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">-->
	<link rel="stylesheet" href="<?php echo ADMIN_ASSETS;?>css/neon.css">
    <link rel="stylesheet" type="text/css" href="<?php echo ADMIN_ASSETS;?>js/fancy/jquery.fancybox.css?v=2.1.5" media="screen" />
	<link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
	<link rel="Shortcut Icon" href="<?php echo ADMIN_ASSETS;?>images/logo/favicon.png">
	<!-- <link rel="icon" href="<?php echo FRONTEND_ASSETS;?>images/logo/favicon.ico" type="image/x-icon"> -->
	<link rel="stylesheet" href="<?php echo ADMIN_ASSETS;?>css/custom.css">
	<link rel="stylesheet" href="<?php echo ADMIN_ASSETS;?>css/wfmi-style.css">
	<link rel="stylesheet" href="<?php echo ADMIN_ASSETS;?>css/ajaxupload.css" />
	<link rel="stylesheet" href="<?php echo ADMIN_ASSETS;?>css/magnific-popup.css" />
    <?php if(isset($menuActive)){ ?>
    <link rel="stylesheet" href="<?php echo ADMIN_ASSETS;?>css/menu.css" />
    <?php } ?>
	<link href='<?php echo ADMIN_ASSETS;?>/fullcalendar/core/main.css' rel='stylesheet' />
    <link href='<?php echo ADMIN_ASSETS;?>/fullcalendar/daygrid/main.css' rel='stylesheet' />
    <!-- <link href='<?php echo ADMIN_ASSETS;?>/fullcalendar/interaction/main.css' rel='stylesheet' /> -->
    <link href='<?php echo ADMIN_ASSETS;?>/fullcalendar/timeGrid/main.css' rel='stylesheet' />	
    <link rel="stylesheet" href="<?php echo ADMIN_ASSETS;?>js/jvectormap/jquery-jvectormap-1.2.2.css">
	<!--<link rel="stylesheet" href="<?php //echo ADMIN_ASSETS;?>js/rickshaw/rickshaw.min.css">-->
    <link rel="stylesheet" href="<?php echo ADMIN_ASSETS;?>js/select2/select2-bootstrap.css">
	<link rel="stylesheet" href="<?php echo ADMIN_ASSETS;?>js/select2/select2.css">
	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->
	<script>
		var patient_id = 0;
		var case_number = 0;
		var doctorsList = [];
	</script>
</head>