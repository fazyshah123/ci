<?php
$crumb2 = "";
if(isset($tbl_data['patient_id'])&&$tbl_data['patient_id']!=""){
	$patient_id = $tbl_data['patient_id'];
    $patient_mr_id = $tbl_data['patient_mr_id'];
    $patient_name = $tbl_data['patient_name'];
    $patient_email = $tbl_data['patient_email'];
    $patient_phone1 = $tbl_data['patient_phone1'];
    $patient_phone2 = $tbl_data['patient_phone2'];
    $patient_status = $tbl_data['patient_status'];
    $patient_phone3 = $tbl_data['patient_phone3'];
    $patient_gender = $tbl_data['patient_gender'];
    $patient_industry_id = $tbl_data['patient_industry_id'];
    $patient_profession_id = $tbl_data['patient_profession_id'];
    $patient_subindustry_id = $this->IndustriesModel->getIndustriesDropDown($tbl_data['patient_industry_id']);
    $patient_jobfunction_id = $this->ProfessionsModel->getProfessionsDropDown($tbl_data['patient_profession_id']);
    $patient_job_level_id = $tbl_data['patient_job_level_id'];
    $patient_birth_date = $tbl_data['patient_birth_date'];
    $patient_birth_date = date('d F Y', strtotime($tbl_data['patient_birth_date']));
    $patient_age = $tbl_data['patient_age'];
    $patient_address = $tbl_data['patient_address'];
    $patient_refered_by = $tbl_data['patient_refered_by'];
	$crumb = "Edit";
    $action = "editRecord/".$tbl_data['patient_id'];
}
else{
	$patient_id = '';
    $patient_mr_id = $nextID;
    $patient_name = '';
    $patient_email = '';
    $patient_phone1 = '';
    $patient_phone2 = '';
    $patient_status = '';
    $patient_phone3 = '';
    $patient_gender = '';
    $patient_industry_id = '';
    $patient_subindustry_id = '';
    $patient_profession_id = '';
    $patient_jobfunction_id = '';
    $patient_job_level_id = '';
    $patient_birth_date = '';
    $patient_age = '';
    $patient_address = '';
    $patient_refered_by = '';
    $on_home 		= "No";
    $crumb 			= "Add";
    $action 		= "addRecord";
}
?>
<style type="text/css">
    input, select {
        border: 1px solid #a5a4a4 !important;
    }
</style>
<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo ADMIN_URL;?>">
            <i class="entypo-home"></i>Home
        </a>
    </li>
    <li>
        <a href="<?php echo ADMIN_URL.$this->controller;?>">
            <i></i><?php echo $this->moduleName;?>
        </a>
    </li>
    <li class="active">
        <strong><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></strong>
    </li>
</ol>

<h2><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></h2>
<br />
<div class="panel panel-primary">
    <div class="panel-body">
        <form  id="page_form"
               name="page_form"
               method="post"
               action="<?php echo base_url('manage/'.$this->controller.'/'.$action);?>"
               enctype="multipart/form-data"
               class="validate"
        >
            <div class="form-group col-md-6 col-xs-12 col-lg-6">
                <label class="control-label">MR # :</label>
                <input readonly type="text" name="patient_mr_id" id="patient_mr_id" value="<?php echo $patient_mr_id;?>" class="form-control " placeholder="MR #" data-validate="maxlength[250]"/>
            </div>

            <div class="form-group col-md-6 col-xs-12 col-lg-6">
                <label class="control-label">Name <span class="req">*</span> :</label>
                <input required type="text" name="patient_name" id="patient_name" value="<?php echo $patient_name;?>" class="form-control" tabindex="1" placeholder="Name" data-validate="maxlength[250]"/>
            </div>

            <div class="form-group col-md-6 col-xs-12 col-lg-6">
                <label class="control-label">Email :</label>
                <input type="email" name="patient_email" id="patient_email" value="<?php echo $patient_email;?>" class="form-control " tabindex="2" placeholder="Email" data-validate="maxlength[250]"/>
            </div>

            <div class="form-group col-md-6 col-xs-12 col-lg-6">
                <label class="control-label">Phone 1 / Login <span class="req">*</span> :</label>
                <input required type="number" name="patient_phone1" id="patient_phone1" value="<?php echo $patient_phone1;?>" class="form-control " tabindex="3" placeholder="Phone1" data-validate="maxlength[250]"/>
            </div>

            <div class="form-group col-md-6 col-xs-12 col-lg-6">
                <label class="control-label">Phone 2 :</label>
                <input type="number" name="patient_phone2"id="patient_phone2"  value="<?php echo $patient_phone2;?>" class="form-control " tabindex="4" placeholder="Phone 2" data-validate="maxlength[250]"/>
            </div>

            <div class="form-group col-md-6 col-xs-12 col-lg-6">
                <label class="control-label">Phone 3 :</label>
                <input type="number" name="patient_phone3" id="patient_phone3" value="<?php echo $patient_phone3;?>" class="form-control " tabindex="5" placeholder="Phone 3" data-validate="maxlength[250]"/>
            </div>

            <div class="form-group col-md-6 col-xs-12 col-lg-6">
                <label class="control-label">Gender :<span class="req"> *</span></label></label>

                <select required class="form-control"  name="patient_gender" tabindex="6" id="patient_gender">
                    <option value="Male" <?php if($patient_gender=="Male"){ echo ' selected="selected"';} ?>>Male</option>
                    <option value="Female" <?php if($patient_gender=="Female"){ echo ' selected="selected"';} ?>>Female</option>
                </select>
            </div>

           <div class="form-group col-md-6 col-xs-12 col-lg-6">
                <label class="control-label">Birth Date :<span class="req"> *</span></label></label>
                <div class="date-and-time">
                    <input required style="width: 100%;float: left;" tabindex="7" name="patient_birth_date" id="patient_birth_date" type="text" autocomplete="off" class="form-control datepicker" value="<?php echo $patient_birth_date;?>" data-format="dd MM yyyy" placeholder="Birth Date">
                </div>
            </div>

            <div class="form-group col-md-6 col-xs-12 col-lg-6">
                <label class="control-label">Age :<span class="req"> *</span></label></label>
                <input required type="number" name="patient_age" id="patient_age" tabindex="8" value="<?php echo $patient_age;?>" class="form-control " placeholder="Age" data-validate="maxlength[250]"/>
            </div>

            <div class="form-group col-md-6 col-xs-12 col-lg-6">
                <label class="control-label">Address :</label>
                <input type="text" name="patient_address" id="patient_address" tabindex="9" value="<?php echo $patient_address;?>" class="form-control " placeholder="Address" data-validate="maxlength[250]"/>
            </div>

            <div class="form-group col-md-6 col-xs-12 col-lg-6">
                <label class="control-label">Main Industry :</label>
                <select class="form-control" name="patient_industry" tabindex="10" id="patient_industry">
                <?php
                $industries = $this->IndustriesModel->getIndustriesDropDown();
                foreach ($industries as $key => $value) {
                    ?>
                        <option value="<?=$value['i_id'];?>" <?php if($value['i_id']==$patient_industry_id){ echo ' selected="selected"';} ?>><?=$value['i_name'];?> </option>
                    <?php  
                }
                ?>
                </select>
            </div>

            <div class="form-group col-md-6 col-xs-12 col-lg-6">
                <label class="control-label">Job Area :</label>
                <select class="form-control" name="patient_profession" tabindex="12" id="patient_profession">
                <?php
                $professions = $this->ProfessionsModel->getProfessionsDropDown();
                foreach ($professions as $key => $value) {
                    ?>
                        <option value="<?=$value['p_id'];?>" <?php if($value['p_id']==$patient_profession_id){ echo ' selected="selected"';} ?>><?=$value['p_name'];?></option>
                    <?php  
                }
                ?>
                </select>
            </div>

            <div class="form-group col-md-6 col-xs-12 col-lg-6">
                <label class="control-label">Sub-Industry :</label>
                <select class="form-control" name="patient_subindustry" tabindex="11" id="patient_subindustry">
                <?php
                foreach ($patient_subindustry_id as $key => $value) {
                    ?>
                        <option value="<?=$value['i_id'];?>" <?php if($value['i_id']==$tbl_data['patient_subindustry_id']){ echo ' selected="selected"';} ?>><?=$value['i_name'];?></option>
                    <?php  
                }
                ?>
                </select>
            </div>

            <div class="form-group col-md-6 col-xs-12 col-lg-6">
                <label class="control-label">Job Function :</label>
                <select class="form-control" name="patient_jobfunction" tabindex="13" id="patient_jobfunction">
                <?php
                foreach ($patient_jobfunction_id as $key => $value) {
                    ?>
                        <option value="<?=$value['p_id'];?>" <?php if($value['p_id']==$tbl_data['patient_jobfunction_id']){ echo ' selected="selected"';} ?>><?=$value['p_name'];?></option>
                    <?php  
                }
                ?>
                </select>
            </div>

            <div class="form-group col-md-6 col-xs-12 col-lg-6">
                <label class="control-label">Job Level :</label>
                <select class="form-control" name="patient_job_level_id" tabindex="14" id="patient_job_level_id">
                <?php
                $joblevel = $this->JobLevelModel->getJobLevelDropDown();
                foreach ($joblevel as $key => $value) {
                    ?>
                        <option value="<?=$value['jl_id'];?>" <?php if($value['jl_id']==$patient_job_level_id){ echo ' selected="selected"';} ?>><?=$value['jl_name'];?></option>
                    <?php  
                }
                ?>
                </select>
            </div>

            <div class="form-group col-md-6 col-xs-12 col-lg-6">
                <label class="control-label">Refered By :</label>
                <input type="text" name="patient_refered_by" tabindex="15" id="patient_refered_by" value="<?php echo $patient_refered_by;?>" class="form-control " placeholder="Refered By" data-validate="maxlength[250]"/>
            </div>

            <div class="form-group col-md-6 col-xs-12 col-lg-6 hidden">
                <label class="control-label">Status :</label>

                <select class="form-control"  name="patient_status" id="patient_status">
                    <option value="Enable" <?php if($patient_status=="Enable"){ echo ' selected="selected"';} ?>>Enable</option>
                    <option value="Disable" <?php if($patient_status=="Disable"){ echo ' selected="selected"';} ?>>Disable</option>
                </select>
            </div>

            <div class="form-group col-md-6 col-xs-12 col-lg-6">
                <label class="control-label">Patient Image :</label>

                <input type="file" name="patient_image" tabindex="16" id="patient_image" accept="image/gif, image/jpeg, image/png" class="form-control">
            </div>
            
            <div class="clearfix"></div>

            <div class="row">
                <div class="form-group col-md-12 col-xs-12 col-lg-12">
                    <button type="button" class="btn btn-danger" onclick="window.location='<?php echo ADMIN_URL;?>Patients'">Cancel</button>
                    <button id="submit" name="submit" type="submit " class="btn btn-success">Submit </button>
                </div>    
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    document.addEventListener("DOMContentLoaded", function(event) {
        $("#patient_industry").unbind('change').bind('change', function(e) {
            e.preventDefault();
            var industry_id = $(this).val();
            $.get('<?=ADMIN_URL."industries/getIndustriesDropDownById";?>/?industry_id='+industry_id, function(data) {
                if(data.data.length>0) {
                    var output = '';
                    $.each(data.data, function(k, v) {
                        output += '<option value="'+v.i_id+'">'+v.i_name+'</option>';
                    });
                    $("#patient_subindustry").html(output);
                }
            }, "json");
        });
        $("#patient_profession").unbind('change').bind('change', function(e) {
            e.preventDefault();
            var profession_id = $(this).val();
            $.post('<?=ADMIN_URL."professions/getProfessionsDropDownById";?>', {
                profession_id: profession_id
            }, function(data) {
                if(data.data.length>0) {
                    var output = '';
                    $.each(data.data, function(k, v) {
                        output += '<option value="'+v.p_id+'">'+v.p_name+'</option>';
                    });
                    $("#patient_jobfunction").html(output);
                }
            }, "json");
        });
    });

hotkeys('ctrl+b', function (event, handler){
event.preventDefault();
  switch (handler.key) {
    case 'ctrl+b': $("#submit").onclick();
      break;
    default: alert(event);
  }
});

</script>