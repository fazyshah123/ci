<?php
$crumb2 = "";
if(isset($tbl_data['lt_id'])&&$tbl_data['lt_id']!=""){
    $lt_id = $tbl_data['lt_id'];
    $lt_patient_id = $tbl_data['lt_patient_id'];
    $lt_lab_id = $tbl_data['lt_lab_id'];
    $lt_due_date = $tbl_data['lt_due_date'];
    $lt_shade = $tbl_data['lt_shade'];
    $lt_work_type_id = $tbl_data['lt_work_type_id'];
    $lt_status = $tbl_data['lt_status'];
    $lt_comment = $tbl_data['lt_comment'];
    $lt_work_id = $tbl_data['lt_work_id'];
    $crumb = "Edit";
    $action = "editRecord/".$tbl_data['lt_id'];
}
else{
    $lt_id = '';
    $lt_patient_id = '';
    $lt_lab_id = '';
    $lt_due_date = date('d F Y', strtotime('now'));
    $lt_shade = '';
    $lt_work_type_id = '';
    $lt_status = '';
    $lt_comment = '';
    $lt_work_id = '';
    $on_home        = "No"; 
    $crumb          = "Add";
    $action         = "addRecord";
}
?>
<style type="text/css">
    input, select {
        border: 1px solid #a5a4a4 !important;
    }
</style>
<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo ADMIN_URL;?>">
            <i class="entypo-home"></i>Home
        </a>
    </li>
    <li>
        <a href="<?php echo ADMIN_URL.$this->controller;?>">
            <i></i><?php echo $this->moduleName;?>
        </a>
    </li>
    <li class="active">
        <strong><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></strong>
    </li>
</ol>

<h2><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></h2>
<br />
<div class="panel panel-primary">
    <div class="panel-body">
        <form  id="page_form"
               name="page_form"
               method="post"
               action="<?php echo base_url('manage/'.$this->controller.'/'.$action);?>"
               enctype="multipart/form-data"
               class="validate"
        >

            <div class="form-group col-md-6 col-xs-12 col-lg-6">
                <label class="control-label">Patient :<span class="req"> *</span></label>
                <select class="form-control" name="lt_patient_id" id="lt_patient_id">
                <?php
                foreach ($tbl_data['patients'] as $key => $value) {
                    ?>
                        <option value="<?=$value['patient_id'];?>" <?php if($value['patient_id']==$lt_patient_id){ echo ' selected="selected"';} ?>><?=$value['patient_name'];?></option>
                    <?php  
                }
                ?>
                </select>
            </div>

            <div class="form-group col-md-6 col-xs-12 col-lg-6">
                <label class="control-label">Lab :<span class="req"> *</span></label>
                <select class="form-control" name="lt_lab_id" id="lt_lab_id">
                <?php
                foreach ($tbl_data['lab'] as $key => $value) {
                    ?>
                        <option value="<?=$value['lab_id'];?>" <?php if($value['lab_id']==$lt_lab_id){ echo ' selected="selected"';} ?>><?=$value['lab_name'];?></option>
                    <?php  
                }
                ?>
                </select>
            </div>

            <div class="form-group col-md-6 col-xs-12 col-lg-6">
                <label class="control-label">Due Date :<span class="req"> *</span></label>
                <div class="date-and-time">
                    <input required style="width: 100%;float: left;"  type="text" name="lt_due_date" id="lt_due_date" value="<?php echo $lt_due_date;?>" autocomplete="off" class="form-control datepicker"  data-format="dd MM yyyy" placeholder="Date"/>
                </div>
            </div>

            <div class="form-group col-md-6 col-xs-12 col-lg-6">
                <label class="control-label">Shade :</label>
                <div>
                    <input style="width: 100%;float: left;" type="text" name="lt_shade" id="lt_shade" value="<?php echo $lt_shade;?>" class="form-control"  placeholder="Shade"/>
                </div>
            </div>

            <div class="form-group col-md-6 col-xs-12 col-lg-6">
                <label class="control-label">Work :</label>
                <select class="form-control" name="lt_work_id" id="lt_work_id">
                <?php
                foreach ($tbl_data['work'] as $key => $value) {
                    ?>
                        <option value="<?=$value['w_id'];?>" <?php if($value['w_id']==$lt_work_id){ echo ' selected="selected"';} ?>><?=$value['w_name'];?></option>
                    <?php  
                }
                ?>
                </select>
            </div>

            <div class="form-group col-md-6 col-xs-12 col-lg-6">
                <label class="control-label">Work Type :</label>
                <select class="form-control" name="lt_work_type_id" id="lt_work_type_id">
                <?php
                foreach ($tbl_data['work_type'] as $key => $value) {
                    ?>
                        <option value="<?=$value['wt_id'];?>" <?php if($value['wt_id']==$lt_work_type_id){ echo ' selected="selected"';} ?>><?=$value['wt_name'];?></option>
                    <?php  
                }
                ?>
                </select>
            </div>

            <div class="clearfix"></div>

            <div class="form-group col-md-12 col-xs-12 col-lg-12">
                <label class="control-label">Comments :</label>
                <?php echo $this->ckeditor->editor("lt_comment", $lt_comment); ?>
            </div>

            <div class="clearfix"></div>

            <div class="form-group col-md-6 col-xs-12 col-lg-6 hidden">
                <label class="control-label">Status :</label>
                <select class="form-control" name="lt_status" id="lt_status">
                    <option value="Enable" <?php if($lt_status=="Enable"){ echo ' selected="selected"';} ?>>Enable</option>
                    <option value="Disable" <?php if($lt_status=="Disable"){ echo ' selected="selected"';} ?>>Disable</option>
                </select>
            </div>
            
            <div class="clearfix"></div>
            
            <div class="row">
                <div class="form-group col-md-12 col-xs-12 col-lg-12">
                    <button type="button" id="" class="btn btn-danger lt_cancel" onclick="window.location='<?php echo ADMIN_URL;?>labtracking'">Cancel</button>
                    <button id="labtracking_submit" type="submit"  class="btn btn-success">Submit</button>
                </div>    
            </div>
        </form>
    </div>
</div>