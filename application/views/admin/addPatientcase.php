<?php
$crumb2 = "";
if(isset($tbl_data['pc_id'])&&$tbl_data['pc_id']!=""){
    print_r($tbl_data);exit;
    $pc_id = $tbl_data['pc_id'];
	$pc_case_number = $tbl_data['pc_case_number'];
	$pc_patient_id = $tbl_data['pc_patient_id'];
	$pc_dtr_id = $tbl_data['pc_dtr_id'];
	$pc_charges = $tbl_data['pc_charges'];
	$pc_status = $tbl_data['pc_status'];
    $pc_relative_name = $tbl_data['pc_relative_name'];
    $pc_relative_cnic = $tbl_data['pc_relative_cnic'];
    $pc_relative_phone = $tbl_data['pc_relative_phone'];
    $pc_admission_date = $tbl_data['pc_admission_date']; 
    $pc_admission_time = $tbl_data['pc_admission_time'];
    $pc_discharge_date = $tbl_data['pc_discharge_date'];
    $pc_discharge_time = $tbl_data['pc_discharge_time'];
    $crumb = "Edit";
    $action = "editRecord/".$tbl_data[''];
}
else{
	$pc_id = '';
	$pc_case_number = '';
	$pc_patient_id = '';
	$pc_dtr_id = '';
	$pc_charges = '';
    $pc_admission_date = '';
    $pc_relative_phone = '';
    $pc_relative_cnic = '';
    $pc_discharge_time = '';
    $pc_admission_time = '';
    $pc_discharge_date = '';
    $pc_relative_name = '';
	$pc_status = '';
    $on_home 		= "No";
    $crumb 			= "Add";
    $action 		= "addRecord";
}
?>
<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo ADMIN_URL;?>">
            <i class="entypo-home"></i>Home
        </a>
    </li>
    <li>
        <a href="<?php echo ADMIN_URL.$this->controller;?>">
            <i></i><?php echo $this->moduleName;?>
        </a>
    </li>
    <li class="active">
        <strong><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></strong>
    </li>
</ol>

<h2><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></h2>
<br />
<div class="col-md-12">
    <?php if($alert=="caseexist") { ?>
      <div class="row alertrow">
        <div class="col-md-12">
         <button class="close alertBox" data-dismiss="alert">x</button>
            <div class="alert alert-danger"><strong>Error!!</strong> Case already exist.</div>
        </div>
     </div>
    <?php } ?>
</div>
<div class="panel panel-primary">
    <div class="panel-body">
        <form  id="page_form"
               name="page_form"
               method="post"
               action="<?php echo base_url('manage/'.$this->controller.'/'.$action);?>"
               enctype="multipart/form-data"
               class="validate"
        >
            <div class="form-group col-md-4">
                <label class="control-label">Case Number :<span class="req"> *</span></label></label>
                <input required type="text" name="pc_case_number" id="pc_case_number" value="<?php echo $pc_case_number;?>" class="form-control " placeholder="Case Number" data-validate="maxlength[250]"/>
            </div>

            <div class="form-group col-md-4">
                <label class="control-label">Charges :<span class="req"> *</span></label></label>
                <input required type="text" name="pc_charges" id="pc_charges" value="<?php echo $pc_charges;?>" class="form-control " placeholder="Charges" data-validate="required,maxlength[250]"/>
            </div>
            <div class="clearfix"></div>

            <div class="form-group col-md-4">
                <label class="control-label">Admission Type :<span class="req"> *</span></label></label>
                <select class="form-control admission_type" name="admission_type" id="admission_type">
                    <option selected value="room">Room</option>
                    <option value="bed">Bed</option>
                </select>
            </div>

            <div class="form-group roomOption col-md-4">
                <label class="control-label">Room :<span class="req"> *</span></label></label>
                <select class="form-control patientsDropdown" data-item="1" name="pc_room_number" id="pc_room_number"><option value="select1">Select</option>
                <?php
                $rooms = $this->SqlModel->getRoomDropDown();
                foreach ($rooms as $key => $value) {
                    $rt = ($value['rt_billing_type'] == '1') ? 'Daily' : 'Per Day';
                    echo '<option data-price="'.$value['rt_per_day_charges'].'" data-type="'.$value['rt_billing_type'].'" value="'.$value['ro_id'].'">'.$value["ro_number"].' ('.$value['rt_name'].' | '.$rt.' | Rs.'.$value['rt_per_day_charges'].')</option>';
                }
                ?>
                </select>
            </div>

            <div class="form-group bedOption col-md-4 hidden">
                <label class="control-label">Bed :<span class="req"> *</span></label></label>
                <select class="form-control patientsDropdown" data-item="1" name="pc_bed_number" id="pc_bed_number"><option value="select1">Select</option>
                <?php
                $bed = $this->SqlModel->getBedDropDown();
                foreach ($bed as $key => $value) {
                    $bt = ($value['bt_billing_type'] == '1') ? 'Daily' : 'Per Day';
                    echo '<option data-price="'.$value['bt_per_day_charges'].'" data-type="'.$value['bt_billing_type'].'" value="'.$value['b_id'].'">'.$value["b_number"].' ('.$value['bt_name'].' | '.$bt.' | Rs.'.$value['bt_per_day_charges'].')</option>';
                }
                ?>
                </select>
            </div>
            <div class="clearfix">  </div>
            <div class="form-group col-md-12">
                <div class="dd">
                    <label class="control-label">Patient<span class="req"> *</span> <a id="makeappointment" href="<?=ADMIN_URL;?>patients/control"><span style="color: #55a;cursor: pointer;" id="Add_Patient">(Add New Patient)</span></label></a>:</label>
                    <select class="form-control patientsDropdown" name="pc_patient_id" id="pc_patient_id">
                        <option value="patientsel">Select</option>
                    <?php
                    $patients = $this->SqlModel->getPatientsDropDown();
                    foreach ($patients as $key => $value) {
                        if($value['patient_id']==$pi_patient_id) { 
                            $selected = ' selected="selected"';
                        } else {
                            $selected = '';
                        }
                        echo '<option '.$selected.' value="'.$value['patient_id'].'">'.$value["patient_name"] . '( MR # '.$value['patient_mr_id'].' )' .'</option>';
                    }
                    ?>
                    </select>
                </div>
                <div class="patientForm hidden">
                    <span class="patientFormBack" style="color: #55a;cursor: pointer;font-weight: bold;"><i class="fa fa-arrow-left"></i> Back</span>
                    <div class="clearfix"></div>
                    <div class="form-group col-md-6" style="padding-left: 0px;padding-right: 8px;">
                        <label class="control-label">Name :</label>
                        <input type="text" name="patient_name" id="patient_name" class="form-control " placeholder="Name" data-validate="required,maxlength[250]"/>
                    </div>
                    <div class="form-group col-md-6" style="padding-left: 0px;padding-right: 0px;">
                        <label class="control-label">Mobile :</label>
                        <input type="text" name="patient_phone1" id="patient_phone1" class="form-control " placeholder="Mobile" data-validate="required,maxlength[250]"/>
                    </div>
                    <div class="form-group col-md-6" style="padding-left: 0px;padding-right: 8px;">
                        <label class="control-label">Age :</label>
                        <input type="text" name="patient_age" id="patient_age" class="form-control " placeholder="Age" data-validate="required,maxlength[250]"/>
                    </div>
                    <div class="form-group col-md-6" style="padding-left: 0px;padding-right: 0px;">
                        <label class="control-label">Gender :</label>
                        <select class="form-control" name="patient_gender" id="patient_gender">
                            <option value="Male">Male</option>
                            <option value="Female">Female</option>
                            <option value="Other">Other</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Address :</label>
                        <input type="text" name="patient_address" id="patient_address" class="form-control " placeholder="Address" data-validate="required,maxlength[250]"/>
                    </div>
                </div>
            </div>

            <div class="form-group col-md-3">
                <label class="control-label">Relation :</label>
                <select class="form-control relationDropdown"  name="pc_relative_id" id="pc_relative_id">
                    <option>Select</option>
                <?php
                $relations = $this->SqlModel->getRelationDropDown();
                foreach ($relations as $key => $value) {
                    echo '<option value="'.$value['relative_id'].'">'.$value["relative_name"].'</option>';
                }
                ?>
                </select>
            </div>

            <div class="form-group col-md-3">
                <label class="control-label">Relative Name :<span class="req"> *</span></label></label>
                <input type="text" name="pc_relative_name" id="pc_relative_name" value="<?php echo $pc_relative_name;?>" class="form-control " placeholder="Relative Name" data-validate="required,maxlength[250]"/>
            </div>

            <div class="form-group col-md-3">
                <label class="control-label">Relative CNIC :<span class="req"> *</span></label></label>
                <input type="number" name="pc_relative_cnic" id="pc_relative_cnic" value="<?php echo $pc_relative_cnic;?>" class="form-control " placeholder="Relative CNIC" data-validate="required,maxlength[250]"/>
            </div>

            <div class="form-group col-md-3">
                <label class="control-label">Relative Phone :<span class="req"> *</span></label></label>
                <input type="number" name="pc_relative_phone" id="pc_relative_phone" value="<?php echo $pc_relative_phone;?>" class="form-control " placeholder="Relative Phone" data-validate="required,maxlength[250]"/>
            </div>

            <div class="clearfix"></div>

            <div class="form-group col-md-3" style="height: 53px;">
                <label class="control-label">Admission Date :<span class="req"> *</span></label></label>
                <div class="date-and-time">
                    <input style="width: 100%;float: left;" required type="text" name="pc_admission_date" id="pc_admission_date" value="<?php echo $pc_admission_date;?>" autocomplete="off" class="form-control datepicker"  data-format="dd MM yyyy" placeholder="Admission Date"/>
                </div>
            </div>

            <div class="form-group col-md-3" style="height: 53px;">
                <label class="control-label">Admission Time :<span class="req"> *</span></label></label>
                <div class="date-and-time">
                    <input required style="width: 100%;float: left;" required type="text" name="pc_admission_time" id="pc_admission_time" value="<?php echo $pc_admission_time;?>" autocomplete="off" class="form-control timepicker"  data-format="dd MM yyyy" placeholder="Admission Time"/>
                </div>
            </div>

            <div class="form-group col-md-6">
                <label class="control-label">Surgeon/Physician :<span class="req"> *</span></label></label>
                <select class="form-control doctorsDropdown"  name="pc_dtr_id" id="pc_dtr_id">
                    <option value="pc_dtr">Select</option>
                <?php
                $doctors = $this->SqlModel->getDoctorsDropDown();
                foreach ($doctors as $key => $value) {
                    echo '<option value="'.$value['id'].'">'.$value["full_name"].'</option>';
                }
                ?>
                </select>
            </div>

            <div class="clearfix"></div>

            <div class="form-group col-md-3" style="height: 53px;">
                <label class="control-label">Discharge Date :</label>
                <div class="date-and-time">
                    <input style="width: 100%;float: left;" type="text" name="pc_discharge_date" id="pc_discharge_date" value="<?php echo $pc_discharge_date;?>" autocomplete="off" class="form-control datepicker"  data-format="dd MM yyyy" placeholder="Discharge Date"/>
                </div>
            </div>

            <div class="form-group col-md-3" style="height: 53px;">
                <label class="control-label">Discharge Time :</label>
                <div class="date-and-time">
                    <input style="width: 100%;float: left;" type="text" name="pc_discharge_time" id="pc_discharge_time" value="<?php echo $pc_discharge_time;?>" autocomplete="off" class="form-control timepicker"  data-format="dd MM yyyy" placeholder="Discharge Time"/>
                </div>
            </div>

            <div class="form-group col-md-6">
                <label class="control-label">Alloted By :</label>
                <select class="form-control check_in_user"  name="pc_alloted_by" id="pc_alloted_by">
                    <option>Select</option>
                <?php
                $users = $this->SqlModel->getAdminUsersDropDown();
                foreach ($users as $key => $value) {
                    if($value['id']==$pc_alloted_by) { 
                        $selected = ' selected="selected"';
                    } else {
                        $selected = '';
                    }
                    echo '<option '.$selected.' value="'.$value['id'].'">'.$value["full_name"].'</option>';
                }
                ?>
                </select>
            </div>
            <div class="clearfix"></div>

            <div class="form-group col-md-6">
                <label class="control-label">Facilities :</label>
                <br>
                <?php $facilities = $this->SqlModel->getFacilities();
                    foreach ($facilities as $key => $value) {
                        echo '<input type="checkbox" name="facility-'.$value['facility_id'].'" id="facility- "'.$value['facility_id'].'"> <strong>'.$value['facility_name'] . '</strong><br/>';
                    }
                ?>
            </div>

            <div class="form-group hidden">
                <label class="control-label">pc_status :</label>
                <input type="text" name="pc_status" id="pc_status" value="<?php echo $pc_status;?>" class="form-control " placeholder="pc_status" data-validate="required,maxlength[250]"/>
            </div>
            <div class="clearfix"></div>

            <div class="form-group">
                <button type="button" class="btn btn-danger" onclick="window.location='<?php echo ADMIN_URL;?>patientcase'">Cancel</button>
                <button type="submit" id="patientcase_submit" class="btn btn-success">Submit</button>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    document.addEventListener("DOMContentLoaded", function(event) {
        $(".patientFormBack").unbind("click").bind("click", function(e) {
            e.preventDefault();
            $(".patientForm").addClass('hidden');
            $(".dd").removeClass('hidden');
        });
        $("#AddPatient").unbind("click").bind("click", function(e) {
            e.preventDefault();
            $(".patientForm").removeClass('hidden');
            $(".dd").addClass('hidden');
        });
        $("#admission_type").on("change", function(e) {
            var admission_type = $(this).val();
            if (admission_type == 'room') {
                    $(".bedOption").addClass('hidden');
                    $(".roomOption").removeClass('hidden');
            } else {
                    $(".roomOption").addClass('hidden');
                    $(".bedOption").removeClass('hidden');

            }
        });
        $("#pc_submit").on('click', function(e) {
            e.preventDefault();
            var pc_room = $("#pc_room_number").val();
            var patient_id = $("#pc_patient_id").val();
            var pc_dt = $("#pc_dtr_id").val();
            
            if (pc_room == 'select1') {
                alert('Please select a room');
                return;
            }
            if (patient_id == 'patientsel') {
                alert('Please enter select a patient');
                return;
            }
            if (pc_dt == 'pc_dtr') {
                alert('Please select a Surgeon/Physician');
                return;
            }
            $("#page_form").submit();
        });
    });
</script>