<?php
$crumb2 = "";
if(isset($tbl_data['pi_id'])&&$tbl_data['pi_id']!=""){
	$pi_id = $tbl_data['pi_id'];
	$pi_clinic_id = $tbl_data['pi_clinic_id'];
	$pi_patient_id = $tbl_data['pi_patient_id'];
	$pi_pai_id = $tbl_data['pi_pai_id'];
	$pi_payment_method = $tbl_data['pi_payment_method'];
	$pi_amount = $tbl_data['pi_amount'];
	$pi_added = $tbl_data['pi_added'];
	$pi_updated = $tbl_data['pi_updated'];
    $pi_remarks = $tbl_data['pi_remarks'];
	$pi_created_by = $tbl_data['pi_created_by'];
    $pi_date = $tbl_data['pi_date'];
	$pi_updated_by = $tbl_data['pi_updated_by'];
	$pi_is_deleted = $tbl_data['pi_is_deleted'];
	$pi_status = $tbl_data['pi_status'];
    $crumb = "Edit";
    $action = "editRecord/".$tbl_data['pi_id'];
}
else{
	$pi_id = '';
	$pi_clinic_id = '';
	$pi_patient_id = $this->input->get('patient_id');
	$pi_pai_id = '';
	$pi_payment_method = '';
	$pi_amount = '';
	$pi_added = '';
	$pi_updated = '';
    $pi_remarks = '';
	$pi_created_by = '';
    $pi_date = '';
	$pi_updated_by = '';
	$pi_is_deleted = '';
	$pi_status = '';
    $on_home 		= "No";
    $crumb 			= "Add";
    $action 		= "addRecord";
}
?>
<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo ADMIN_URL;?>">
            <i class="entypo-home"></i>Home
        </a>
    </li>
    <li>
        <a href="<?php echo ADMIN_URL.$this->controller;?>">
            <i></i><?php echo $this->moduleName;?>
        </a>
    </li>
    <li class="active">
        <strong><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></strong>
    </li>
</ol>
        <form  id="invoice_form"
               name="invoice_form"
               method="post"
               action="<?php echo base_url('manage/'.$this->controller.'/'.$action);?>"
               enctype="multipart/form-data"
        >
<div class="row">
    <div class="col-md-4">
        <h2 style="margin: 0px !important;"><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></h2>
    </div>
    <div class="col-md-8 printButton hidden">
        <div class="btn btn-group pull-right" role="group" aria-label="Basic example">
          <button href="<?=ADMIN_URL.'invoices/printInvoice/';?>" id="redirectToPrint" type="button" class="btn btn-secondary" style="border-right: 1px solid #eee;
          "><i class="fa fa-print"></i> Print</button>
        </div>
    </div>
</div>
<br />
<div class="panel panel-primary">
    <div class="panel-body">
            <div class="row alertrow hidden">
                <div class="col-md-12">
                <button class="close alertBox" data-dismiss="alert">x</button>
                    <div class="alert responseType"><strong class="responseText"></strong> <span class="responseMessage"></span></div>
                </div>
            </div>
            <?php
            $procedures = $this->SqlModel->getProcedureDropDown();
            $user = $this->SqlModel->getAdminUsersDropDown();
            $procedure_list = '';
            foreach ($procedures as $key => $value) {
                $procedure_list .= '<option data-price="'.$value['procedure_price'].'" value="'.$value['procedure_id'].'">'.$value["procedure_name"].' ('.$value['procedure_price'].')</option>';
            }
            ?>
            <div class="row">
                <div class="form-group col-md-6 col-xs-12 col-lg-6">
                    <label class="control-label" style="margin-right: 15px;">Invoice Type :</label>
                    <div class="clearfix"></div>
                    <input type="radio" checked name="pi_type" value="OPD" id="bt_opd_billing_type" />
                    <label class="control-label" for="bt_opd_billing_type" style="margin-right: 15px;">OPD</label>
                    <input type="radio" name="pi_type" value="IPD" id="bt_ipd_billing_type"/>
                    <label class="control-label" for="bt_ipd_billing_type">IPD</label>
                </div>
                <div class="form-group col-md-6 col-xs-12 col-lg-6 hidden">
                    <div class="con pull-right">
                        <input type="hidden" name="appointment_id" id="appointment_id">
                        <label class="control-label" style="margin-right: 15px;">Appointment :</label>
                        <div class="clearfix"></div>
                        <input type="checkbox" name="make_appointment" value="1" id="make_appointment" />
                        <label class="control-label" for="make_appointment" style="margin-right: 15px;">Make Appointment?</label>
                    </div>
                </div>
            </div>
            <div class="row" style="padding: 0px 0px;">
                <div class="form-group col-md-4">
                    <div class="dd">
                        <input type="hidden" name="appointment_id" id="appointment_id" value="<?=$this->input->get('appointment_id');?>">
                        <label class="control-label">Patient <span style="color: #55a;cursor: pointer;" id="AddPatient">(Add New Patient)</span>:</label>
                        <select class="form-control patientsDropdown" name="pi_patient_id" id="pi_patient_id">
                            <option value="">Select</option>
                        <?php
                        $patients = $this->SqlModel->getPatientsDropDown();
                        foreach ($patients as $key => $value) {
                            if($value['patient_id']==$pi_patient_id) { 
                                $selected = ' selected="selected"';
                            } else {
                                $selected = '';
                            }
                            echo '<option '.$selected.' value="'.$value['patient_id'].'">'.$value["patient_name"] . '( MR # '.$value['patient_mr_id'].' )' .'</option>';
                        }
                        ?>
                        </select>
                    </div>
                    <div class="patientForm hidden">
                        <span class="patientFormBack" style="color: #55a;cursor: pointer;font-weight: bold;"><i class="fa fa-arrow-left"></i> Back</span>
                        <div class="clearfix"></div>
                        <div class="form-group col-md-6" style="padding-left: 0px;padding-right: 8px;">
                            <label class="control-label">Name :</label>
                            <input type="text" name="patient_name" id="patient_name" class="form-control " placeholder="Name" data-validate="required,maxlength[250]"/>
                        </div>
                        <div class="form-group col-md-6" style="padding-left: 0px;padding-right: 0px;">
                            <label class="control-label">Mobile :</label>
                            <input type="text" name="patient_phone1" id="patient_phone1" class="form-control " placeholder="Mobile" data-validate="required,maxlength[250]"/>
                        </div>
                        <div class="form-group col-md-6" style="padding-left: 0px;padding-right: 8px;">
                            <label class="control-label">Age :</label>
                            <input type="text" name="patient_age" id="patient_age" class="form-control " placeholder="Age" data-validate="required,maxlength[250]"/>
                        </div>
                        <div class="form-group col-md-6" style="padding-left: 0px;padding-right: 0px;">
                            <label class="control-label">Gender :</label>
                            <select class="form-control" name="patient_gender" id="patient_gender">
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                                <option value="Other">Other</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Address :</label>
                            <input type="text" name="patient_address" id="patient_address" class="form-control " placeholder="Address" data-validate="required,maxlength[250]"/>
                        </div>
                    </div>
                </div>
                <div class="form-group col-md-4">
                    <label class="control-label">Remarks :</label>
                    <input type="text" name="pi_remarks" id="pi_remarks" value="<?php echo $pi_remarks;?>" class="form-control " placeholder="Remarks" />
                </div>
                <div class="form-group col-md-2">
                    <label class="control-label">Date :</label>
                    <div class="date-and-time">
                        <input style="width: 100%;float: left;" type="text" name="pi_date" id="pi_date" value="<?php echo $pi_date;?>" autocomplete="off" class="form-control datepicker"  data-format="dd MM yyyy" placeholder="Date"/>
                        <input type="hidden" id="total_items" value="1" name="total_items">
                    </div>
                </div>
                
                <div class="form-group col-md-2">
                    <label class="control-label">Payment Method :</label>
                    <select class="form-control" name="pi_payment_method" id="  ">
                        <option value="Cash" <?php if($pi_payment_method=="Cash"){ echo ' selected="selected"';} ?>>Cash</option>
                        <option value="Credit Card" <?php if($pi_payment_method=="Credit Card"){ echo ' selected="selected"';} ?>>Credit Card</option>
                        <option value="Cheque" <?php if($pi_payment_method=="Cheque"){ echo ' selected="selected"';} ?>>Cheque</option>
                        <option value="Online Payment" <?php if($pi_payment_method=="Online Payment"){ echo ' selected="selected"';} ?>>Online Payment</option>
                    </select>
                </div>
            </div>

            <div class="row" style="margin-bottom: 15px;">
                <div class="col-md-6 pull-right">
                    <button class="btn btn-success pull-right" id="add_procedure_row" data-toggle="tooltip" title="Add Procedure"><i class="fa fa-plus"></i> Procedure</button>
                    <button class="btn btn-success pull-right" id="add_bed_row" data-toggle="tooltip" title="Add Bed"><i class="fa fa-plus"></i> Bed</button>
                    <button class="btn btn-success pull-right" id="add_room_row" data-toggle="tooltip" title="Add Room"><i class="fa fa-plus"></i> Room</button>
                </div>
            </div>
            <div class="items">
                <div class="pitem">
                    <div class="row" style="padding: 0px 0px;">
                        <div class="form-group col-md-3">
                            <label class="control-label">Procedure :</label>
                            <select class="form-control patientsDropdown" data-item="1" name="pp_procedure_id-1" id="pp_procedure_id-1"><option>Select</option>
                            <?php
                            echo $procedure_list;
                            ?>
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                            <label class="control-label">Desc. :</label>
                            <input type="text" data-item="1" name="pp_description-1" id="pp_description-1" class="form-control " placeholder="Description"/>
                        </div>
                        <div class="form-group col-md-1" style="padding-left: 7px;padding-right: 7px;">
                            <label class="control-label">Rate :</label>
                            <input type="text" data-item="1" value="0" name="pp_rate-1" id="pp_rate-1" class="form-control item_rate" placeholder="Rate" data-validate="maxlength[250]"/>
                        </div>
                        <div class="form-group col-md-1" style="padding-left: 7px;padding-right: 7px;">
                            <label class="control-label">Discount :</label>
                            <input type="text" data-item="1" name="pp_discount-1" id="pp_discount-1" class="form-control item_discount" value="0" placeholder="Discount" data-validate="maxlength[250]"/>
                        </div>
                        <div class="form-group col-md-1" style="padding-left: 7px;padding-right: 7px;">
                            <label class="control-label" style="visibility: hidden;">.</label>
                            <select class="form-control item_discount_type" data-item="1" name="pp_discount_type-1" id="pp_discount_type-1" style="padding: 0px;">
                                <option value="Rs.">Rs.</option>
                                <option value="%">%</option>
                            </select>
                        </div>
                        <div class="form-group col-md-1" style="padding-left: 7px;padding-right: 7px;">
                            <label class="control-label">Amount :</label>
                            <input type="text" data-item="1" value="0" readonly name="pp_amount-1" id="pp_amount-1" class="form-control " placeholder="Amount" data-validate="maxlength[250]"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bedRow hidden">
                <hr>
                <h4><strong>Bed</strong></h4>
                <div class="row" style="padding: 0px 0px;">
                    <div class="form-group col-md-3">
                        <label class="control-label">Bed :</label>
                        <select class="form-control patientsDropdown" data-item="1" name="pp_bed_number" id="pp_bed_number"><option>Select</option>
                        <?php
                        $beds = $this->SqlModel->getBedDropDown();
                        foreach ($beds as $key => $value) {
                            $bt = ($value['bt_billing_type'] == '1') ? 'Daily' : 'Per Day';
                            echo '<option data-price="'.$value['bt_per_day_charges'].'" data-type="'.$value['bt_billing_type'].'" value="'.$value['b_id'].'">'.$value["b_number"].' ('.$value['bt_name'].' | '.$bt.' | Rs.'.$value['bt_per_day_charges'].')</option>';
                        }
                        ?>
                        </select>
                    </div>
                    <div class="form-group col-md-3">
                        <label class="control-label">Desc. :</label>
                        <input type="text" data-item="1" name="pp_description-1" id="pp_description-1" class="form-control " placeholder="Desc." data-validate="required,maxlength[250]"/>
                    </div>
                    <div class="form-group col-md-2" style="padding-left: 7px;padding-right: 7px;">
                        <label class="control-label">From :</label>
                        <div class="date-and-time">
                            <input style="width: 100%;float: left;" type="text" name="bed_from" id="bed_from" autocomplete="off" class="form-control datepicker"  data-format="dd MM yyyy" placeholder="Date"/>
                        </div>
                    </div>
                    <div class="form-group col-md-2" style="padding-left: 7px;padding-right: 7px;">
                        <label class="control-label">To. :</label>
                        <div class="date-and-time">
                            <input style="width: 100%;float: left;" type="text" name="bed_to" id="bed_to" autocomplete="off" class="form-control datepicker"  data-format="dd MM yyyy" placeholder="Date"/>
                        </div>
                    </div>
                    <div class="form-group col-md-2" style="padding-left: 7px;padding-right: 7px;margin-top: 20px;">
                        <label class="control-label hidden">To. :</label>
                        <button class="btn btn-danger tile-stat removeBed"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="row" style="padding: 0px;">
                    <div class="form-group col-md-3">
                        <label class="control-label">Alloted By :</label>
                        <select class="form-control patientsDropdown" data-item="1" name="pp_bed_alloted_by" id="pp_bed_alloted_by" data-validate="required"><option>Select</option>
                        <?php
                        foreach ($user as $key => $value) {
                            echo '<option value="'.$value['id'].'">'.$value["full_name"].'</option>';
                        }
                        ?>
                        </select>
                    </div>
                    <div class="form-group col-md-2" style="padding-right: 7px;">
                        <label class="control-label">Rate :</label>
                        <input type="text" data-item="1" value="0" name="pp_bed_rate" id="pp_bed_rate" class="form-control " placeholder="Amount" data-validate="required,maxlength[250]"/>
                    </div>
                    <div class="form-group col-md-2" style="padding-left: 7px;padding-right: 7px;">
                        <label class="control-label">Days :</label>
                        <input type="text" data-item="1" value="0" readonly name="pp_bed_total_days" id="pp_bed_total_days" class="form-control " placeholder="Amount" data-validate="required,maxlength[250]"/>
                    </div>
                    <div class="form-group col-md-1" style="padding-left: 7px;padding-right: 7px;">
                        <label class="control-label">Discount :</label>
                        <input type="text" data-item="1" name="pp_bed_discount" id="pp_bed_discount" class="form-control item_discount" value="0" placeholder="Discount" data-validate="required,maxlength[250]"/>
                    </div>
                    <div class="form-group col-md-1" style="padding-left: 7px;padding-right: 7px;">
                        <label class="control-label" style="visibility: hidden;">.</label>
                        <select class="form-control item_discount_type" data-item="1" name="pp_bed_discount_type" id="pp_bed_discount_type" style="padding: 0px;">
                            <option value="Rs.">Rs.</option>
                            <option value="%">%</option>
                        </select>
                    </div>
                    <div class="form-group col-md-2" style="padding-left: 7px;padding-right: 7px;">
                        <label class="control-label">Amount :</label>
                        <input type="text" data-item="1" readonly name="pp_bed_amount" id="pp_bed_amount" class="form-control item_discount" value="0" placeholder="Discount" data-validate="required,maxlength[250]"/>
                    </div>
                </div>
            </div>
            <div class="roomRow hidden">
                <hr>
                <h4><strong>Room</strong></h4>
                <div class="row" style="padding: 0px;">
                    <div class="form-group col-md-3">
                        <label class="control-label">Room :</label>
                        <select class="form-control patientsDropdown" data-item="1" name="pp_room_number" id="pp_room_number" data-validate="required"><option>Select</option>
                        <?php
                        $rooms = $this->SqlModel->getRoomDropDown();
                        foreach ($rooms as $key => $value) {
                            $rt = ($value['rt_billing_type'] == '1') ? 'Daily' : 'Per Day';
                            echo '<option data-price="'.$value['rt_per_day_charges'].'" data-type="'.$value['rt_billing_type'].'" value="'.$value['ro_id'].'">'.$value["ro_number"].' ('.$value['rt_name'].' | '.$rt.' | Rs.'.$value['rt_per_day_charges'].')</option>';
                        }
                        ?>
                        </select>
                    </div>
                    <div class="form-group col-md-3">
                        <label class="control-label">Desc. :</label>
                        <input type="text" data-item="1" name="pp_description-1" id="pp_description-1" class="form-control " placeholder="Desc." data-validate="required,maxlength[250]"/>
                    </div>
                    <div class="form-group col-md-2" style="padding-left: 7px;padding-right: 7px;">
                        <label class="control-label">From :</label>
                        <div class="date-and-time">
                            <input style="width: 100%;float: left;" type="text" name="room_from" id="room_from" autocomplete="off" class="form-control datepicker"  data-format="dd MM yyyy" placeholder="Date"/>
                        </div>
                    </div>
                    <div class="form-group col-md-2" style="padding-left: 7px;padding-right: 7px;">
                        <label class="control-label">To. :</label>
                        <div class="date-and-time">
                            <input style="width: 100%;float: left;" type="text" name="room_to" id="room_to" autocomplete="off" class="form-control datepicker"  data-format="dd MM yyyy" placeholder="Date"/>
                        </div>
                    </div>
                    <div class="form-group col-md-2" style="padding-left: 7px;padding-right: 7px;margin-top: 20px;">
                        <label class="control-label hidden">To. :</label>
                        <button class="btn btn-danger tile-stat removeRoom"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="row" style="padding: 0px 00px;">
                    <div class="form-group col-md-3">
                        <label class="control-label">Alloted By :</label>
                        <select class="form-control patientsDropdown" data-item="1" name="pp_room_alloted_by" id="pp_room_alloted_by" data-validate="required"><option>Select</option>
                        <?php
                        foreach ($user as $key => $value) {
                            echo '<option value="'.$value['id'].'">'.$value["full_name"].'</option>';
                        }
                        ?>
                        </select>
                    </div>
                    <div class="form-group col-md-2" style="padding-right: 7px;">
                        <label class="control-label">Rate :</label>
                        <input type="text" data-item="1" value="0" name="pp_room_rate" id="pp_room_rate" class="form-control " placeholder="Amount" data-validate="required,maxlength[250]"/>
                    </div>
                    <div class="form-group col-md-2" style="padding-left: 7px;padding-right: 7px;">
                        <label class="control-label">Days :</label>
                        <input type="text" data-item="1" value="0" readonly name="pp_room_total_days" id="pp_room_total_days" class="form-control " placeholder="Amount" data-validate="required,maxlength[250]"/>
                    </div>
                    <div class="form-group col-md-1" style="padding-left: 7px;padding-right: 7px;">
                        <label class="control-label">Discount :</label>
                        <input type="text" data-item="1" name="pp_room_discount" id="pp_room_discount" class="form-control item_discount" value="0" placeholder="Discount" data-validate="required,maxlength[250]"/>
                    </div>
                    <div class="form-group col-md-1" style="padding-left: 7px;padding-right: 7px;">
                        <label class="control-label" style="visibility: hidden;">.</label>
                        <select class="form-control item_discount_type" data-item="1" name="pp_room_discount_type" id="pp_room_discount_type" style="padding: 0px;">
                            <option value="Rs.">Rs.</option>
                            <option value="%">%</option>
                        </select>
                    </div>
                    <div class="form-group col-md-2" style="padding-left: 7px;padding-right: 7px;">
                        <label class="control-label">Amount :</label>
                        <input type="text" data-item="1" readonly name="pp_room_amount" id="pp_room_amount" class="form-control item_discount" value="0" placeholder="Discount" data-validate="required,maxlength[250]"/>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-2">
                    <label class="control-label">Discount in Rs. :</label>
                    <input type="text" data-item="1" name="p_total_discount_rs" id="p_total_discount_rs" class="form-control items_discount" value="0" placeholder="Discount in Rs." data-validate="maxlength[250]"/>
                </div>
                <div class="col-md-2">
                    <label class="control-label">Discount in %. :</label>
                    <input type="text" data-item="1" name="p_total_discount_percent" id="p_total_discount_percent" class="form-control item_discount" value="0" placeholder="Discount in %" data-validate="maxlength[250]"/>
                </div>
                <div class="col-md-2">
                    <label class="control-label">Paid :</label>
                    <input type="text" data-item="1" name="pp_paid" id="pp_paid" class="form-control" placeholder="Paid" data-validate="maxlength[250]"/>
                </div>
                <div class="col-md-2 pull-right">
                    <label>Rs. <span id="total"> 0</span></label>
                    <div class="clearfix"></div>
                    <label>Rs. <span id="totalDiscount"> 0</span></label>
                    <div class="clearfix"></div>
                    <label>Rs. <span id="grandTotal"> 0</span></label>
                    <input type="hidden" name="grand_total" id="grand_total">
                    <div class="clearfix"></div>
                    <label>Rs. <span id="due"> 0</span></label>
                    <div class="clearfix"></div>
                    <label>Rs. <span id="advance"> 0</span></label>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-2 pull-right">
                    <label>Total: <span id="total"></span></label>
                    <div class="clearfix"></div>
                    <label>Discount: <span id="due"></span></label>
                    <div class="clearfix"></div>
                    <label>Grand Total: <span id="grandTotal"></span></label>
                    <div class="clearfix"></div>
                    <label>Due: <span id="due"></span></label>
                    <div class="clearfix"></div>
                    <label>Advance: <span id="advance"></span></label>
                    <div class="clearfix"></div>
                </div>
                <input type="hidden" id="invoiceId" name="invoiceId"/>
            </div>

            <div class="form-group hidden">
                <label class="control-label">Status :</label>
                <select class="form-control" name="pi_status" id="pi_status">
                    <option value="Enable" <?php if($pi_status=="Enable"){ echo ' selected="selected"';} ?>>Enable</option>
                    <option value="Disable" <?php if($pi_status=="Disable"){ echo ' selected="selected"';} ?>>Disable</option>
                </select>
            </div>

            <div class="form-group">
                <button type="button" class="btn btn-danger" onclick="window.location='<?php echo ADMIN_URL;?>invoices'">Cancel</button>
                <button type="submit"  class="btn btn-success" id="submitInvoice">Submit</button>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    document.addEventListener("DOMContentLoaded", function(event) {
        $("#submitInvoice").unbind("click").bind("click", function(e) {
            e.preventDefault();
            var pm = $("#pi_payment_method").val();
            var date = $("#pi_date").val();
            var pidd = $("#pi_patient_id").val();
            var pn = $("#patient_name").val();
            var pp = $("#patient_phone1").val();
            var paid = $("#pp_paid").val();
            var bed = $("#pp_bed_number").val();
            var room = $("#pp_room_number").val();
            var procedure = $("#pp_procedure_id-1").val();
            if (pm == '') {
                alert('Please select a payment method');
                return false;
            }
            if (date == '') {
                alert('Please select an invoice date');
                return false;
            }
            if (pidd == '' && (pn == '' && pp == '')) {
                alert('Please select a Patient');
                return false;
            }
            if (bed == 'Select' && room == 'Select' && procedure == 'Select') {
                alert('No Procedure, Room or Bed is selected');
                return false;
            }
            if (paid == '') {
                alert('No payment amount has been entered');
                return false;
            }
            var str = $("#invoice_form").serialize();

            $.post($("#invoice_form").attr('action'), {
                data: str
            }, function(data) {
               showAlertNotification(data);
            }, "json");
        });
        <?php if ($pi_patient_id != '') {?>
            $("#pi_patient_id").prop('readonly', true);
        <?php } ?>
        $(".patientFormBack").unbind("click").bind("click", function(e) {
            e.preventDefault();
            $(".patientForm").addClass('hidden');
            $(".dd").removeClass('hidden');
        });
        $("#AddPatient").unbind("click").bind("click", function(e) {
            e.preventDefault();
            $(".patientForm").removeClass('hidden');
            $(".dd").addClass('hidden');
        });
        $("#add_bed_row").unbind("click").bind("click", function(e) {
            e.preventDefault();
            $(".bedRow").removeClass('hidden removed').addClass('shown');
            calculatePrice();
        });
        $(".removeBed").unbind("click").bind("click", function(e) {
            e.preventDefault();
            $(".bedRow").addClass('hidden removed').removeClass('shown');
            calculatePrice();
        });
        $("#add_room_row").unbind("click").bind("click", function(e) {
            e.preventDefault();
            $(".roomRow").removeClass('hidden removed').addClass('shown');
            calculatePrice();
        });
        $(".removeRoom").unbind("click").bind("click", function(e) {
            e.preventDefault();
            $(".roomRow").addClass('hidden removed').removeClass('shown');
            calculatePrice();
        });
        $("#redirectToPrint").unbind("click").bind("click", function(e) {
            var link = $(this).attr('href');
            var id = $("#invoiceId").val();
            var patient_id = $("#pi_patient_id").val();
            window.location = link + "?invoiceId="+id+"&patientId="+patient_id;
        });
        $("#pp_room_discount, #pp_bed_discount, #p_total_discount_rs, #p_total_discount_percent").unbind('keyup').bind('keyup', function(e) {
            e.preventDefault();
            calculatePrice();
        });
        $("#pp_room_discount_type, #pp_bed_discount_type").unbind('change').bind('change', function(e) {
            e.preventDefault();
            calculatePrice();
        });        
        $("#pp_room_total_days, #pp_room_rate, #pp_room_total_days, #pp_room_rate").unbind("change").bind("change", function(e) {
            e.preventDefault();
            calculateAmount("pp_room_total_days", "pp_room_rate", "pp_room_number", "pp_room_amount");
        });
        $("#pp_bed_total_days, #pp_bed_rate, #pp_bed_total_days, #pp_bed_rate").unbind("change").bind("change", function(e) {
            e.preventDefault();
            calculateAmount("pp_bed_total_days", "pp_bed_rate", "pp_bed_number", "pp_bed_amount");
        });
        $("#pp_paid").unbind('keyup').bind('keyup', function(e) {
            e.preventDefault();
            calculateChange();
        });
        $("#bed_from, #bed_to").unbind("change").bind("change", function(e) {
            calculateDays("bed_from", "bed_to", "pp_bed_total_days");
            calculateAmount("pp_bed_total_days", "pp_bed_rate", "pp_bed_number", "pp_bed_amount");
            calculatePrice();
        });
        $("#room_from, #room_to").unbind("change").bind("change", function(e) {
            calculateDays("room_from", "room_to", "pp_room_total_days");
            calculateAmount("pp_room_total_days", "pp_room_rate", "pp_room_number", "pp_room_amount");
            calculatePrice();
        });
        $("#pp_procedure_id-1").unbind("change").bind("change", function (e) {
            $("#pp_rate-1").val($(this).find('option:selected').data('price'));
            calculatePrice();
        });
        $("#pp_discount_type-1").unbind("change").bind("change", function (e) {
            $("#pp_rate-1").val($('#pp_procedure_id-1').find('option:selected').data('price'));
            calculatePrice();
        });
        $("#pp_bed_number").unbind("change").bind("change", function (e) {
            $("#pp_bed_rate").val($(this).find('option:selected').data('price'));
            calculateDays("bed_from", "bed_to", "pp_bed_total_days");
            calculateAmount("pp_bed_total_days", "pp_bed_rate", "pp_bed_number", "pp_bed_amount");
            calculatePrice();
        });
        $("#pp_room_number").unbind("change").bind("change", function (e) {
            $("#pp_room_rate").val($(this).find('option:selected').data('price'));
            calculateDays("room_from", "room_to", "pp_room_total_days");
            calculateAmount("pp_room_total_days", "pp_room_rate", "pp_room_number", "pp_room_amount");
            calculatePrice();
        });
        $("#add_procedure_row").on("click", function(e) {
            e.preventDefault();
            var item_rows_count = $(".pitem").length;
            var new_row_id = ++item_rows_count;
            var procedureList = '<?=$procedure_list;?>';
            var new_row = '\
                <div class="pitem">\
                    <div class="row" style="padding: 0px 0px;">\
                        <div class="form-group col-md-3">\
                            <label class="control-label">Procedure :</label>\
                            <select class="form-control patientsDropdown" data-item="'+new_row_id+'" name="pp_procedure_id-'+new_row_id+'" id="pp_procedure_id-'+new_row_id+'"><option>Select</option>'+procedureList+'\
                            </select>\
                        </div>\
                        <div class="form-group col-md-3">\
                            <label class="control-label">Desc. :</label>\
                            <input type="text" data-item="'+new_row_id+'" name="pp_description-'+new_row_id+'" id="pp_description-'+new_row_id+'" class="form-control " placeholder="Desc." data-validate="required,maxlength[250]"/>\
                        </div>\
                        <div class="form-group col-md-1" style="padding-left: 7px;padding-right: 7px;">\
                            <label class="control-label">Rate :</label>\
                            <input type="text" data-item="'+new_row_id+'" value="0" name="pp_rate-'+new_row_id+'" id="pp_rate-'+new_row_id+'" class="form-control item_rate" placeholder="Rate" data-validate="required,maxlength[250]"/>\
                        </div>\
                        <div class="form-group col-md-1" style="padding-left: 7px;padding-right: 7px;">\
                            <label class="control-label">Discount :</label>\
                            <input type="text" data-item="'+new_row_id+'" name="pp_discount-'+new_row_id+'" id="pp_discount-'+new_row_id+'" value="0" class="form-control item_discount" placeholder="Discount" data-validate="required,maxlength[250]"/>\
                        </div>\
                        <div class="form-group col-md-1" style="padding-left: 7px;padding-right: 7px;">\
                            <label class="control-label" style="visibility: hidden;">.</label>\
                            <select class="form-control item_discount_type" data-item="'+new_row_id+'" name="pp_discount_type-'+new_row_id+'" id="pp_discount_type-'+new_row_id+'" style="padding: 0px;">\
                                <option value="Rs.">Rs.</option>\
                                <option value="%">%</option>\
                            </select>\
                        </div>\
                        <div class="form-group col-md-1" style="padding-left: 7px;padding-right: 7px;">\
                            <label class="control-label">Amount :</label>\
                            <input type="text" data-item="'+new_row_id+'" value="0" readonly name="pp_amount-'+new_row_id+'" id="pp_amount-'+new_row_id+'" class="form-control " placeholder="Amount" data-validate="required,maxlength[250]"/>\
                        </div>\
                        <div class="form-group col-md-1" style="padding-left: 7px;padding-right: 7px;">\
                            <div class="clearfix" style="margin-top: 28px;"></div>\
                            <label class="control-label" style="visibility: hidden;">.</label>\
                            <a href="javascript:;" data-item="'+new_row_id+'" id="remove_item-'+new_row_id+'" class="remove_item"><i class="fa fa-times"></i></a>\
                        </div>\
                    </div>\
                </div>';

            $(".items").append(new_row);
            $("#total_items").val(new_row_id);

            $(".remove_item").unbind("click").bind("click", function(e) {
                e.preventDefault();
                var item = $(this).data('item');
                var total_items = $(".pitem").length;

                $(this).closest('.pitem').remove();

                for (var i = item+1; i <= total_items; i++) {
                    
                    $("#pp_procedure_id-"+i).attr('name', 'pp_procedure_id-'+(i-1)).attr('data-item', (i-1)).attr('id', 'pp_procedure_id-'+(i-1));
                    $("#pp_description-"+i).attr('name', 'pp_description-'+(i-1)).attr('data-item', (i-1)).attr('id', 'pp_description-'+(i-1));
                    $("#pp_rate-"+i).attr('name', 'pp_rate-'+(i-1)).attr('data-item', (i-1)).attr('id', 'pp_rate-'+(i-1));
                    $("#pp_amount-"+i).attr('name', 'pp_amount-'+(i-1)).attr('data-item', (i-1)).attr('id', 'pp_amount-'+(i-1));
                    $("#pp_discount-"+i).attr('name', 'pp_discount-'+(i-1)).attr('data-item', (i-1)).attr('id', 'pp_discount-'+(i-1));
                    $("#pp_discount_type-"+i).attr('name', 'pp_discount_type-'+(i-1)).attr('data-item', (i-1)).attr('id', 'pp_discount_type-'+(i-1));
                    $("#remove_item-"+i).attr('data-item', (i-1)).attr('id', 'remove_item-'+(i-1));
                }
                var minus_item = $("#total_items").val();
                $("#total_items").val(--new_row_id);
                calculatePrice();
            });
            $("#pp_procedure_id-"+new_row_id).select2();
            $("#pp_procedure_id-"+new_row_id).unbind('change').bind("change", function (e) {
                $("#pp_rate-"+new_row_id).val($(this).find('option:selected').data('price'));
                calculatePrice();
            });
            RegisterQtyEvent();
        });
        RegisterQtyEvent();
    });
    function calculateChange() {
        var gt = parseInt($("#grand_total").val());
        var pp_paid = parseInt($("#pp_paid").val());
        if (pp_paid > gt) {
            $("#due").text("0");
            $("#advance").text(pp_paid-gt);
        }
        if (pp_paid < gt) {
            var myString = (gt-pp_paid).toString();
            if( myString.charAt( 0 ) === '-' )
                myString = myString.slice( 1 );
            $("#due").text(myString);
            $("#advance").text("0");
        }
        if (pp_paid == gt) {
            $("#due").text("0");
            $("#advance").text("0");
        }
    }
    function daysBetween(startDate, endDate) {
        var millisecondsPerDay = 24 * 60 * 60 * 1000;
        return (treatAsUTC(endDate) - treatAsUTC(startDate)) / millisecondsPerDay;
    }
    function treatAsUTC(date) {
        var result = new Date(date);
        result.setMinutes(result.getMinutes() - result.getTimezoneOffset());
        return result;
    }
    function clearAlertNotification() {
        $(".responseText").text('');
        $(".responseType").removeClass('alert-danger alert-success');
        $(".responseMessage").text('');
        $(".alertrow").addClass('hidden');
    }
    function showAlertNotification(data) {
        $(".responseText").text(data.alert);
        $(".responseType").addClass(data.responseType);
        $(".responseMessage").text(data.message);
        $(".alertrow").removeClass('hidden');
        $("#invoiceId").val(data.invoiceId);
        $(".printButton").removeClass('hidden');
    }
    function calculateDays(from, to, target) {
        var bf = $("#"+from).val();
        var bt = $("#"+to).val();
        if (bf == "" || bt == "") return;
        var diff = daysBetween(bf, bt);
        $("#"+target).val(diff);
    }
    function calculateAmount(total_days, rate, type, amount) {
        var days = $("#"+total_days).val();
        var rate = $("#"+rate).val();
        if (days == "" || rate == "") return;

        var type = $("#"+type).find('option:selected').data('type');
        if (type == '1') {
            $("#"+amount).val(days*rate);
            calculatePrice();
        }
    }
    function RegisterQtyEvent() {
        $(".item_rate, .item_discount, .item_discount_type").unbind('keyup').bind('keyup', function(e) {
            var item = $(this).data('item');
            var i = $("#pp_procedure_id-"+item).val();
            var price = $("#pp_rate-"+item).val();
            if (price == '' || price == 0 || i == 'Select') {
                return;
            } else {
                calculatePrice();
            }
        });
        calculatePrice();
    }
    function calculatePrice() {
        var total_bill = 0;
        var full_total = 0;
        var discount = 0;
        var item_rows_count = $(".pitem").length;
        for (var i = 1; i <= item_rows_count; i++) {
            total = 0;
            if (!($("#pp_rate-"+i).length)) {
                i++;
            }
            var p = $("#pp_rate-"+i).val();
            var dr = $("#pp_discount-"+i).val();
            var dp = $("#pp_discount_type-"+i).val();
            if (p == undefined) {
            } else {
                total = parseInt(p);
                full_total += parseInt(total);
                if (dp == 'Rs.' && dr > 0) {
                    total -= dr;
                } else if (dp == '%') {
                    total -= (total * (dr/100));
                }
                $("#pp_amount-"+i).val(total);
                total_bill += total;
            }
        }

        if ($(".bedRow").hasClass('shown')) {
            var bed_amount = $("#pp_bed_amount").val();
            var bed_discount = $("#pp_bed_discount").val();
            if (bed_discount > 0) {
                dt = $("#pp_bed_discount_type").val();
                full_total += parseInt(bed_amount);
                if (dt == 'Rs.') {
                    total_bill += parseInt((bed_amount - bed_discount));
                } else if (dt == '%') {
                    var t = parseInt((bed_amount * (bed_discount/100)));
                    total_bill += (bed_amount-t);
                }
            } else {
                total_bill += parseInt(bed_amount);
                full_total += parseInt(bed_amount);
            }
        }

        if ($(".roomRow").hasClass('shown')) {
            var room_amount = $("#pp_room_amount").val();
            var room_discount = $("#pp_room_discount").val();
            if (room_discount > 0) {
                dt = $("#pp_room_discount_type").val();
                full_total += parseInt(room_amount);
                if (dt == 'Rs.') {
                    total_bill += parseInt((room_amount - room_discount));
                } else if (dt == '%') {
                    var t = parseInt((room_amount * (room_discount/100)));
                    total_bill += (room_amount-t);
                }
            } else {
                total_bill += parseInt(room_amount);
                full_total += parseInt(room_amount);
            }
        }

        var tdr = $("#p_total_discount_rs").val();
        var tdp = $("#p_total_discount_percent").val();

        if (tdr != '' && tdr != 0) {
            total_bill -= parseInt(tdr);
        }

        if (tdp && tdp != 0) {
            total_bill -= parseInt((total_bill * (tdp/100)));
        }
        
        $("#grandTotal").text(parseInt(total_bill));
        $("#total").text(parseInt(full_total));
        $("#totalDiscount").text(parseInt(full_total - total_bill));
        $("#grand_total").val(parseInt(total_bill));
        calculateChange();
    }
</script>