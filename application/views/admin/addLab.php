<?php
$crumb2 = "";
if(isset($tbl_data['lab_id'])&&$tbl_data['lab_id']!=""){
	$lab_id = $tbl_data['lab_id'];
    $lab_name = $tbl_data['lab_name'];
    $lab_email = $tbl_data['lab_email'];
    $lab_address = $tbl_data['lab_address'];
    $lab_phone = $tbl_data['lab_phone']; 
    $lab_contact_person = $tbl_data['lab_contact_person'];
    $lab_status = $tbl_data['lab_status'];
    $crumb = "Edit";
    $action = "editRecord/".$tbl_data['lab_id'];
}
else{
	$lab_id = '';
    $lab_name = '';
    $lab_email = '';
    $lab_phone = '';
    $lab_address = '';
    $lab_contact_person = '';
    $lab_status = '';
    $on_home 		= "No";
    $crumb 			= "Add";
    $action 		= "addRecord";
}
?>
<style type="text/css">
    input, select {
        border: 1px solid #a5a4a4 !important;
    }
</style>
<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo ADMIN_URL;?>">
            <i class="entypo-home"></i>Home 
        </a>
    </li>
    <li>
        <a href="<?php echo ADMIN_URL.$this->controller;?>">
            <i></i><?php echo $this->moduleName;?>
        </a>
    </li>
    <li class="active">
        <strong><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></strong>
    </li>
</ol>

<h2><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></h2>
<br />
<div class="panel panel-primary">
    <div class="panel-body">
        <form  id="page_form"
               name="page_form"
               method="post"
               action="<?php echo base_url('manage/'.$this->controller.'/'.$action);?>"
               enctype="multipart/form-data"
               class="validate"
        >

            <div class="form-group col-md-6 col-xs-12 col-lg-6">
                <label class="control-label">Lab Name :<span class="req"> *</span></label>
                <input required type="text" name="lab_name" id="lab_name" value="<?php echo $lab_name;?>" class="form-control " placeholder="Lab Name" data-validate="maxlength[250]"/>
            </div>

            <div class="form-group col-md-6 col-xs-12 col-lg-6">
                <label class="control-label">Phone :<span class="req"> *</span></label>
                <input required type="number" name="lab_phone" id="lab_phone" value="<?php echo $lab_phone;?>" class="form-control " placeholder="Phone" data-validate="maxlength[250]"/>
            </div>

            <div class="form-group col-md-6 col-xs-12 col-lg-6">
                <label class="control-label">Contact Person :<span class="req"> *</span></label>
                <input required type="text" name="lab_contact_person" id="lab_contact_person" value="<?php echo $lab_contact_person;?>" class="form-control " placeholder="Contact Person" data-validate="maxlength[250]"/>
            </div>

            <div class="form-group col-md-6 col-xs-12 col-lg-6">
                <label class="control-label">Email :</label>
                <input type="email" name="lab_email" id="lab_email" value="<?php echo $lab_email;?>" class="form-control " placeholder="Email" data-validate="maxlength[250]"/>
            </div>

            <div class="form-group col-md-6 col-xs-12 col-lg-6">
                <label class="control-label">Address :</label>
                <input type="text" name="lab_address" id="lab_address" value="<?php echo $lab_address;?>" class="form-control " placeholder="Address" data-validate="maxlength[250]"/>
            </div>
            
            <div class="clearfix"></div>

            <div class="form-group col-md-6 col-xs-12 col-lg-6 hidden">
                <label class="control-label">Status :</label>
                <select class="form-control" name="lab_status" id="lab_status">
                    <option value="Enable" <?php if($lab_status=="Enable"){ echo ' selected="selected"';} ?>>Enable</option>
                    <option value="Disable" <?php if($lab_status=="Disable"){ echo ' selected="selected"';} ?>>Disable</option>
                </select>
            </div>
            
            <div class="clearfix"></div>
            
            <div class="row">
                <div class="form-group col-md-12 col-xs-12 col-lg-12">
                    <button type="button" class="btn btn-danger" onclick="window.location='<?php echo ADMIN_URL;?>lab'">Cancel</button>
                    <button id="lad_submit" type="submit"  class="btn btn-success">Submit</button>
                </div>    
            </div>
        </form>
    </div>
</div>