<?php
$crumb2 = "";
if(isset($tbl_data['t_id'])&&$tbl_data['t_id']!=""){
	$t_id = $tbl_data['t_id'];
	$t_name = $tbl_data['t_name'];
	$t_is_deleted = $tbl_data['t_is_deleted'];
	$t_clinic_id = $tbl_data['t_clinic_id'];
	$t_added = $tbl_data['t_added'];
	$t_updated = $tbl_data['t_updated'];
	$t_status = $tbl_data['t_status'];
	$t_created_by = $tbl_data['t_created_by'];
	$t_updated_by = $tbl_data['t_updated_by'];
    $crumb = "Edit";
    $action = "editRecord/".$tbl_data['t_id'];
}
else{
	$t_id = '';
	$t_name = '';
	$t_is_deleted = '';
	$t_clinic_id = '';
	$t_added = '';
	$t_updated = '';
	$t_status = '';
	$t_created_by = '';
	$t_updated_by = '';
    $on_home 		= "No";
    $crumb 			= "Add";
    $action 		= "addRecord";
}
?>
<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo ADMIN_URL;?>">
            <i class="entypo-home"></i>Home
        </a>
    </li>
    <li>
        <a href="<?php echo ADMIN_URL.$this->controller;?>">
            <i></i><?php echo $this->moduleName;?>
        </a>
    </li>
    <li class="active">
        <strong><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></strong>
    </li>
</ol>

<h2><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></h2>
<br />
<div class="panel panel-primary">
    <div class="panel-body">
        <form  id="page_form"
               name="page_form"
               method="post"
               action="<?php echo base_url('manage/'.$this->controller.'/'.$action);?>"
               enctype="multipart/form-data"
               class="validate"
        >
            <div class="form-group">
                <label class="control-label">Treatment Name :</label>
                <input type="text" name="t_name" id="t_name" value="<?php echo $t_name;?>" class="form-control " placeholder="Treatment Name" data-validate="required,maxlength[250]"/>
            </div>

            <div class="form-group">
                <label class="control-label">Status :</label>
                <select class="form-control"  name="t_status" id="t_status">
                    <option selected value="Enable" <?php if($t_status=="Enable"){ echo ' selected="selected"';} ?>>Enable</option>
                    <option value="Disable" <?php if($t_status=="Disable"){ echo ' selected="selected"';} ?>>Disable</option>
                </select>
            </div>

            <div class="form-group">
                <button type="button" class="btn btn-danger" onclick="window.location='<?php echo ADMIN_URL;?>pages'">Cancel</button>
                <button type="submit"  class="btn btn-success">Submit</button>
            </div>
        </form>
    </div>
</div>