<?php
$crumb2 = "";
if(isset($tbl_data['s_id'])&&$tbl_data['s_id']!=""){
	$s_id = $tbl_data['s_id'];
    $s_name = $tbl_data['s_name'];
    $s_email = $tbl_data['s_email'];
    $s_phone1 = $tbl_data['s_phone1'];
    $s_phone2 = $tbl_data['s_phone2'];
    $s_status = $tbl_data['s_status'];
    $s_contact_person = $tbl_data['s_contact_person'];
    $s_address = $tbl_data['s_address'];
    $crumb = "Edit";
    $action = "editRecord/".$tbl_data['s_id'];
}
else{
	$s_id = '';
    $s_name = '';
    $s_email = '';
    $s_phone1 = '';
    $s_phone2 = '';
    $s_status = '';
    $s_contact_person = '';
    $s_address = '';
    $on_home 		= "No";
    $crumb 			= "Add";
    $action 		= "addRecord";
}
?>
<style type="text/css">
    input, select {
        border: 1px solid #a5a4a4 !important;
    }
</style>
<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo ADMIN_URL;?>">
            <i class="entypo-home"></i>Home
        </a>
    </li>
    <li>
        <a href="<?php echo ADMIN_URL.$this->controller;?>">
            <i></i><?php echo $this->moduleName;?>
        </a>
    </li>
    <li class="active">
        <strong><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></strong>
    </li>
</ol>

<h2><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></h2>
<br />
<div class="panel panel-primary">
    <div class="panel-body">
        <form  id="page_form"
               name="page_form"
               method="post"
               action="<?php echo base_url('manage/'.$this->controller.'/'.$action);?>"
               enctype="multipart/form-data"
               class="validate"
        >

            <div class="form-group col-md-6 col-xs-12 col-lg-6">
                <label class="control-label">Suppliers Name <span style="color: red">*</span> :</label>
                <input required type="text" name="s_name" id="s_name" value="<?php echo $s_name;?>" class="form-control " placeholder="Suppliers Name" data-validate="maxlength[250]"/>
            </div>

            <div class="form-group col-md-6 col-xs-12 col-lg-6">
                <label class="control-label">Contact Name <span style="color: red">*</span> :</label>
                <input required type="text" name="s_contact_person" id="s_contact_person" value="<?php echo $s_contact_person;?>" class="form-control " placeholder="Contact Name" data-validate="maxlength[250]"/>
            </div>

            <div class="form-group col-md-6 col-xs-12 col-lg-6">
                <label class="control-label">Phone 1 <span style="color: red">*</span> :</label>
                <input required type="number" name="s_phone1" id="s_phone1" value="<?php echo $s_phone1;?>" class="form-control " placeholder="Phone 1" data-validate="maxlength[250]"/>
            </div>

            <div class="form-group col-md-6 col-xs-12 col-lg-6">
                <label class="control-label">Phone 2 :</label>
                <input type="number" name="s_phone2" id="package_desitnation" value="<?php echo $s_phone2;?>" class="form-control " placeholder="Phone 2" data-validate="maxlength[250]"/>
            </div>

            <div class="form-group col-md-6 col-xs-12 col-lg-6">
                <label class="control-label">Address:</label>
                <input type="text" name="s_address" id="s_address" value="<?php echo $s_address;?>" class="form-control " placeholder="Address" data-validate="maxlength[250]"/>
            </div>

            <div class="form-group col-md-6 col-xs-12 col-lg-6">
                <label class="control-label">Email. :</label>
                <input type="email" name="s_email" id="s_email" value="<?php echo $s_email;?>" class="form-control " placeholder="Email." data-validate="maxlength[250]"/>
            </div>

            <div class="form-group col-md-6 col-xs-12 col-lg-6 hidden">
                <label class="control-label">Address :</label>
                <input type="checkbox" name="s_address" id="s_address" value="<?php ($s_address != '') ? '1' : $s_address;?>" placeholder="Address" />
            </div>
            
            <div class="clearfix"></div>

            <div class="form-group col-md-6 col-xs-12 col-lg-6 hidden">
                <label class="control-label">Status :</label>
                <select class="form-control" name="s_status" id="s_status">
                    <option value="Enable" <?php if($s_status=="Enable"){ echo ' selected="selected"';} ?>>Enable</option>
                    <option value="Disable" <?php if($s_status=="Disable"){ echo ' selected="selected"';} ?>>Disable</option>
                </select>
            </div>
            
            <div class="clearfix"></div>
            
            <div class="row">
                <div class="form-group col-md-12 col-xs-12 col-lg-12">
                    <button type="button" class="btn btn-danger" onclick="window.location='<?php echo ADMIN_URL;?>suppliers'">Cancel</button>
                    <button id="s_submit" type="submit"  class="btn btn-success">Submit</button>
                </div>    
            </div>
        </form>
    </div>
</div>