<?php
$crumb2 = "";
if(isset($tbl_data['i_id'])&&$tbl_data['i_id']!=""){
	$i_id = $tbl_data['i_id'];
	$i_clinic_id = $tbl_data['i_clinic_id'];
	$i_name = $tbl_data['i_name'];
	$i_description = $tbl_data['i_description'];
	$i_unit_price = $tbl_data['i_unit_price'];
	$i_added = $tbl_data['i_added'];
	$i_updated = $tbl_data['i_updated'];
	$i_created_by = $tbl_data['i_created_by']; 
	$i_updated_by = $tbl_data['i_updated_by'];
	$i_is_deleted = $tbl_data['i_is_deleted'];
	$i_status = $tbl_data['i_status'];
    $crumb = "Edit";
    $action = "editRecord/".$tbl_data['i_id'];
}
else{
	$i_id = '';
	$i_clinic_id = '';
	$i_name = '';
	$i_description = '';
	$i_unit_price = '';
	$i_added = '';
	$i_updated = '';
	$i_created_by = '';
	$i_updated_by = '';
	$i_is_deleted = '';
	$i_status = '';
    $on_home 		= "No";
    $crumb 			= "Add";
    $action 		= "addRecord";
}
?>
<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo ADMIN_URL;?>">
            <i class="entypo-home"></i>Home
        </a>
    </li>
    <li>
        <a href="<?php echo ADMIN_URL.$this->controller;?>">
            <i></i><?php echo $this->moduleName;?>
        </a>
    </li>
    <li class="active">
        <strong><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></strong>
    </li>
</ol>

<h2><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></h2>
<br />
<div class="panel panel-primary">
    <div class="panel-body">
        <form  id="page_form"
               name="page_form"
               method="post"
               action="<?php echo base_url('manage/'.$this->controller.'/'.$action);?>"
               enctype="multipart/form-data"
               class="validate"
        >
                  
            <div class="form-group">
                <label class="control-label">Name <span style="color: red">*</span> :</label>
                <input type="text" name="i_name" id="i_name" value="<?php echo $i_name;?>" class="form-control " placeholder="Name" data-validate="required,maxlength[250]"/>
            </div>
                  
            <div class="form-group">
                <label class="control-label">Unit Price <span style="color: red">*</span> :</label>
                <input type="number" name="i_unit_price" id="i_unit_price" value="<?php echo $i_unit_price;?>" class="form-control " placeholder="Unit Price" data-validate="required,maxlength[250]"/>
            </div>


            <div class="form-group" >
                <label class="control-label">Description :</label>
				<?php echo $this->ckeditor->editor("i_description", html_entity_decode($i_description)); ?>
			</div>

            <div class="form-group hidden">
                <label class="control-label">Status :</label>
                <select class="form-control"  name="i_status" id="i_status">
                    <option value="Enable" <?php if($i_status=="Enable"){ echo ' selected="selected"';} ?>>Enable</option>
                    <option value="Disable" <?php if($i_status=="Disable"){ echo ' selected="selected"';} ?>>Disable</option>
                  </select>
            </div>



            <div class="form-group">
                <button type="button" class="btn btn-danger" onclick="window.location='<?php echo ADMIN_URL;?>items'">Cancel</button>
                <button id="item_submit" type="submit"  class="btn btn-success">Submit</button>
            </div>
        </form>
    </div>
</div>