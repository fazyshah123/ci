<ol class="breadcrumb bc-3">
<li><a href="<?= ADMIN_URL;?>"><i class="entypo-home"></i>Home</a></li>
<li class="active"><strong><?= $this->moduleName;?></strong></li>
</ol>

 <?php if($alert=="success") { ?>
 <div class="row alertrow">
	<div class="col-md-12">
    <button class="close alertBox" data-dismiss="alert">x</button>
		<div class="alert alert-success"><strong>Success!</strong> <?= rtrim($this->moduleName,'s');?> added sucessfully.</div>
	</div>
</div>
   <?php } if($alert=="tsuccess") { ?>
 <div class="row alertrow">
	<div class="col-md-12">
    <button class="close alertBox" data-dismiss="alert">x</button>
		<div class="alert alert-success"><strong>Success!</strong> Doctor timing updated sucessfully.</div>
	</div>
</div>
   <?php } if($alert=="deletesuccess") { ?>
 <div class="row alertrow">
	<div class="col-md-12">
    <button class="close alertBox" data-dismiss="alert">x</button>
		<div class="alert alert-success"><strong>Success!</strong> <?= rtrim($this->moduleName,'s');?> deleted sucessfully.</div>
	</div>
</div>
   <?php } if($alert=="deleteerror") { ?>
  <div class="row alertrow">
	<div class="col-md-12">
     <button class="close alertBox" data-dismiss="alert">x</button>
		<div class="alert alert-danger"><strong>Error!!</strong> Error occurred while deleting the record, please try again.</div>
	</div>
</div>
 <?php } if($alert=="adminerror") { ?>
  <div class="row alertrow">
	<div class="col-md-12">
     <button class="close alertBox" data-dismiss="alert">x</button>
		<div class="alert alert-danger"><strong>Error!!</strong> The user name or email you specified is already exist, please use different user name or email.</div>
	</div>
</div>
   <?php } if($alert=="editsuccess") { ?>
   <div class="row alertrow">
	<div class="col-md-12">
    <button class="close alertBox" data-dismiss="alert">x</button>
		<div class="alert alert-success"><strong>Success!</strong> <?= rtrim($this->moduleName,'s');?> updated sucessfully.</div>
	</div>
</div>
   <?php } if($alert=="error") { ?>
  <div class="row alertrow">
	<div class="col-md-12">
     <button class="close alertBox" data-dismiss="alert">x</button>
		<div class="alert alert-danger"><strong>Error!!</strong> Error occurred while saving the record, please try again.</div>
	</div>
</div>
  <?php } if($alert=="permerror") { ?>
  <div class="row alertrow">
	<div class="col-md-12">
     <button class="close alertBox" data-dismiss="alert">x</button>
		<div class="alert alert-danger"><strong>Error!!</strong> Seems like you don't have permission, please try again.</div>
	</div>
+</div>
   <?php } ?>                    


<h2><?= $this->moduleName;?></h2>
<hr />



<div class="row" style="min-height:400px;">
	<div class="col-md-12">
		<?php if($this->SqlModel->checkPermissions('admins', 'delete')==true) {?>
		<button  id="deleteAllRecords" class="btn btn-default btn-icon icon-left" type="button">
            Delete Selected
            <i class="entypo-trash"></i>
        </button> 
		<?php } if($this->SqlModel->checkPermissions('admins', 'create')==true) {?>
        <button  class="btn btn-default btn-icon icon-left" type="button" onclick="javascript:window.location='<?= base_url('manage/'.$this->controller.'/control');?>'">
            Add <?= rtrim($this->moduleName,'s');?>
            <i class="entypo-plus-circled"></i>
        </button>
		<?php } ?>
        
        <span class="selectPerPage pull-right"><br/>Records Per Page: <select class="noselect input-small" name="per_page" id="per_page">
            <option value="0" <?= ((int)$this->per_page===0) ? ' selected="selected" ' : '';?>>All</option>
            <?php
			for($i=10;$i<=100;$i+=10)
			{
				echo '<option value="'.$i.'"';
				echo ((int)$this->per_page===$i) ? ' selected="selected" ' : '';
				echo '>'.$i.'</option>';
			}
			?>
            </select></span>
		<br clear="all" />
        <hr />
      
           <form class="form-inline pull-right">
				<select class="rolesDropdown" style="padding: 5px 8px;" name="d" id="d">
					<option>Select</option>
				<?php
					$items = $this->SqlModel->getRolesDropDown();
					foreach ($items as $key => $value) {
					    echo '<option '.($role == $value["page_id"] ? "selected" : "").' value="'.$value['page_id'].'">'.$value["role_title"].'</option>';
					}
				?>
				</select>
				<input class="input-medium" type="text" value="<?= ($keywords!="-") ? $keywords : "" ;?>"  name="search_keywords" id="search_keywords" placeholder="Search keywords" class=" span11 aplha">
				<select autocomplete="off"  class=" noselect input-medium" name="search_status" id="search_status">
					<option value="-">Select Status</option>
					<option value="Enable" <?php if(isset($status)&&$status=="Enable"){ echo ' selected="selected" ';} ?>>Enable</option>
					<option value="Disable" <?php if(isset($status)&&$status=="Disable"){ echo ' selected="selected" ';} ?>>Disable</option>
				</select>
             </form> 
         
        <strong>&nbsp;</strong>
      
        <hr />
		<form action="<?= ADMIN_URL.$this->controller;?>/deleteall" method="post" name="multiDel" id="multiDel">
		<table class="table table-hover table-striped">
			<thead>
				<tr>
					<th><input type="checkbox" id="all-checkbox" name="all-checkbox" autocomplete="off"></th>
                    <th><a href="<?= base_url('manage/'.$this->controller.'/index/'.$this->pKey.'/'.$order.'/'.$status.'/'.$keywords.'/'.$page_numb);?>">ID</a></th>
                    <th><a href="<?= base_url('manage/'.$this->controller.'/index/role_title/'.$order.'/'.$page_numb);?>">Role</a></th>
					<th><a href="<?= base_url('manage/'.$this->controller.'/index/user_name/'.$order.'/'.$page_numb);?>">User Name</a></th>
                    <th  class="hideCol"><a href="<?= base_url('manage/'.$this->controller.'/index/full_name/'.$order.'/'.$page_numb);?>">Full Name</a></th>
                    <th class="hidden"><a href="<?= base_url('manage/'.$this->controller.'/index/user_role/'.$order.'/'.$status.'/'.$keywords.'/'.$page_numb);?>">Role</a></th>
                    <th><a href="<?= base_url('manage/'.$this->controller.'/index/'.$this->tStatus.'/'.$order.'/'.$status.'/'.$keywords.'/'.$page_numb);?>">Status</a></th>
                    <th  class="hideCol"><a href="<?= base_url('manage/'.$this->controller.'/index/date_created/'.$order.'/'.$page_numb);?>">Added On</a></th>
                    <th>Actions</th>
				</tr>
			</thead>
			
			<tbody>
            <?php

			  if(count($records)>0)
			  {
				foreach($listing1 as $c)
				{
					?>
              <tr>
                  <td><?php if($c['id']>1){ ?><input name="records[]" autocomplete="off" class="cselect" value="<?= $c['id'];?>" type="checkbox" /><?php } ?></td>
                 <td><?= $c['id'];?></td>
                  <td><a  href="<?= base_url();?>manage/admins/control/edit/<?= $c['id'];?>"><?= $c['role_title'];?></a></td>
                  <td><a  href="<?= base_url();?>manage/admins/control/edit/<?= $c['id'];?>"><?= $c['user_name'];?></a></td>
                  <td  class="hideCol"><a  href="<?= base_url();?>manage/admins/control/edit/<?= $c['id'];?>"><?= $c['full_name'];?></a></td>
                  <td  class="hidden"><?= $c['user_role'];?></td>
                  <td><a class="<?= ($c['id']=="1") ? '' : 'changestatus';?>" href="javascript:void(0);" data-controller="<?= $this->controller;?>" id="statusID<?= $c[$this->pKey];?>"><?= $c['status'];?></a></td>
                  <td class="hideCol"><?= date('M d, Y h:i a', strtotime($c['date_created']));?></td>
                  <td>
					<?php if($this->SqlModel->checkPermissions('admins', 'update')==true) {?>
					<a class="icon-left font16" href="<?= base_url('manage/'.$this->controller.'/control/edit/'.$c[$this->pKey]);?>">
						<i class="entypo-pencil"></i>
					</a>
					<?php }?>
					<?php if($this->SqlModel->checkPermissions('doctortiming', 'manage')==true && $c['role_title'] == "Doctor"){ ?><a class="icon-left font16 doctorTiming" href="<?=ADMIN_URL;?>doctortiming/control/?uid=<?=$c['id'];?>" data-controller="<?= $this->controller;?>" id="recordID<?= $c[$this->pKey];?>">
					<i class="entypo-clock"></i></a><?php
				} 
				if($this->SqlModel->checkPermissions('admins', 'delete')==true) {
					if(($c['id'] != $this->session->userdata['admin_id'] && $c['clinic_owner'] != '1') || $this->session->userdata('user_role_id') == '1'){ ?>
						<a class="icon-left font16 delitem" href="javascript:void(0);" data-controller="<?= $this->controller;?>" id="recordID<?= $c[$this->pKey];?>">
							<i class="entypo-cancel"></i>
						</a><?php
					}
				} ?>
                  </td>
                </tr>                    
			<?php		
				}
			}
			else{ ?>
				<tr><td colspan="8">Sorry! No Records.</td></tr>
            <?php  
			}
			?>	
			</tbody>
		</table>
        </form>
         <?php 
 $total_pages = ((int) $per_page === 0 )  ? '1' : ceil($total_rows/$per_page); 
 $current_page = ((int) $per_page === 0 )  ? '1' : ceil($page_numb/$per_page)+1; 
 if($total_pages=="1")
 {
	$showing_from = 1;
	$showing_to = $total_rows;
 }
 else if($total_pages==$current_page)
 {
	$showing_from = ($per_page*($current_page-1))+1;
	$showing_to = $total_rows; 
 }
 else{
	$showing_from = ($per_page*($current_page-1))+1;
	$showing_to = $per_page*$current_page;
 }
 if($total_rows>0)
 {
?>
Showing <?= $showing_from;?> to <?= $showing_to;?> of <?= $total_rows;?> Record<?= ((int)$total_rows>1)? 's' : '';?> (Total <?= $total_pages;?> Page<?= ((int)$total_pages>1)? 's' : '';?>) <?php } echo (isset($paginate)&&$paginate!="") ? $paginate : '';?>  
		
	</div>
	
	
</div>

<script type="text/javascript">
    var controller = '<?=$this->controller;?>';
    var sortby = '<?=$sortby;?>';
    var order = '<?=(($order=="ASC") ? 'DESC' : 'ASC');?>';
    document.addEventListener("DOMContentLoaded", function(event) {
	    $(".rolesDropdown").on('change',function(){
			var search_keywords = $("#search_keywords").val();
			var role = $(".rolesDropdown").val();
			search_keywords = (search_keywords=="") ? "-" : encodeURI(search_keywords);
			window.location = "<?= base_url('manage/'.$this->controller.'/index/'.$sortby.'/'.(($order=="ASC") ? 'DESC' : 'ASC'));?>/"+$("#search_status").val()+"/"+search_keywords+"/d/"+role;
		});
	});
	
</script>


