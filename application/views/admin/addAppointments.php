<?php
$crumb2 = "";
if(isset($tbl_data['appointment_id'])&&$tbl_data['appointment_id']!=""){
	$appointment_id = $tbl_data['appointment_id'];
    $appointment_patient_id = $tbl_data['appointment_patient_id'];
    $appointment_date = $tbl_data['appointment_date'];
    $appointment_start_time = $tbl_data['appointment_start_time'];
    $appointment_end_time = $tbl_data['appointment_end_time'];
    $appointment_status = $tbl_data['appointment_status'];
    $appointment_comment = $tbl_data['appointment_comment'];
    $appointment_dtr_id = $tbl_data['appointment_dtr_id'];
    $appointment_procedure_id = $tbl_data['appointment_procedure_id'];
    $appointment_send_confirmation = $tbl_data['appointment_send_confirmation'];
    $crumb = "Edit";
    $action = "editRecord/".$tbl_data['appointment_id'];
}
else{
    $appointment_id = '';
    $appointment_patient_id = '';
    $appointment_date = date('d F Y', strtotime('now'));
    $appointment_start_time = date('H:i', strtotime('now'));
    $appointment_end_time = date('H:i', strtotime('now + 30 mins'));
    $appointment_status = '';
    $appointment_comment = '';
    $appointment_dtr_id = '';
    $appointment_procedure_id = '';
    $appointment_send_confirmation = '';
    $on_home 		= "No";
    $crumb 			= "Add";
    $action 		= "addRecord";
    if ($tbl_data['patient_id'] != '') {
        $appointment_patient_id = $tbl_data['patient_id'];
    }
}
?>
<style type="text/css">
    input, select {
        border: 1px solid #a5a4a4 !important;
    }
</style>
<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo ADMIN_URL;?>">
            <i class="entypo-home"></i>Home
        </a>
    </li>
    <li>
        <a href="<?php echo ADMIN_URL.$this->controller;?>">
            <i></i><?php echo $this->moduleName;?>
        </a>
    </li>
    <li class="active">
        <strong><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></strong>
    </li>
</ol>

<h2><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></h2>
<br />
<div class="panel panel-primary">
    <div class="panel-body">
        <form  id="page_form"
               name="page_form"
               method="post"
               action="<?php echo base_url('manage/'.$this->controller.'/'.$action);?>"
               enctype="multipart/form-data"
               class="validate"
        >

            <div class="form-group col-md-6 col-xs-12 col-lg-6">
                <label id="appointment-patient" class="control-label">Patient <span class="req">*</span> : (<a href="<?=ADMIN_URL;?>patients/control">*Add New Patient</a>)</label>
                <select required class="form-control" name="appointment_patient_id" id="appointment_patient_id">
                <?php
                foreach ($tbl_data['patients'] as $key => $value) {
                    ?>
                        <option value="<?=$value['patient_id'];?>" <?php if($value['patient_id']==$appointment_patient_id){ echo ' selected="selected"';} ?>><?=$value['patient_name'];?></option>
                    <?php  
                }
                ?>
                </select>
            </div>

            <div class="form-group col-md-6 col-xs-12 col-lg-6">
                <label class="control-label">Date <span class="req">*</span> :</label>
                <div class="date-and-time">
                    <input required style="width: 100%;float: left;"  type="text" name="appointment_date" id="appointment_date" value="<?php echo $appointment_date;?>" autocomplete="off" class="form-control datepicker"  data-format="dd MM yyyy" placeholder="Date"/>
                </div>
            </div>

            <div class="form-group col-md-6 col-xs-12 col-lg-6">
                <label class="control-label">Start Time : <span class="req">*</span> :</label></label>
                <div class="date-and-time">
                    <input required style="width: 100%;float: left;" id="appointment_start_time" name="appointment_start_time" type="text" autocomplete="off" class="form-control timepicker" data-template="dropdown" data-default-time="<?php echo $appointment_start_time;?>" data-show-meridian="true" data-minute-step="1" data-second-step="1" placeholder="Start Time"/>
                </div>
            </div>

            <div class="form-group col-md-6 col-xs-12 col-lg-6">
                <label class="control-label">End Time :</label>
                <div class="date-and-time">
                    <input style="width: 100%;float: left;" id="appointment_end_time" name="appointment_end_time" type="text" autocomplete="off" class="form-control timepicker" data-template="dropdown" data-default-time="<?php echo $appointment_end_time;?>" data-show-meridian="true" data-minute-step="1" data-second-step="1" placeholder="End Time"/>
                </div>
            </div>

            <div class="form-group col-md-6 col-xs-12 col-lg-6">
                <label class="control-label">Doctor <span class="req">*</span> :</label>
                <select class="form-control" required name="appointment_dtr_id" id="appointment_dtr_id">
                <?php
                foreach ($tbl_data['doctors'] as $key => $value) {
                    ?>
                        <option value="<?=$value['dtr_id'];?>" <?php if($value['dtr_id']==$appointment_dtr_id){ echo ' selected="selected"';} ?>><?=$value['dtr_name'];?></option>
                    <?php  
                }
                ?>
                </select>
            </div>

            <div class="form-group col-md-6 col-xs-12 col-lg-6">
                <label class="control-label">Procedure : <span class="req">*</span></label></label>
                <select class="form-control" name="appointment_procedure_id" id="appointment_procedure_id">
                <?php
                foreach ($tbl_data['procedures'] as $key => $value) {
                    ?>
                        <option value="<?=$value['procedure_id'];?>" <?php if($value['procedure_id']==$appointment_procedure_id){ echo ' selected="selected"';} ?>><?=$value['procedure_name'];?> (<?=$value['procedure_price'];?> Rs.)</option>
                    <?php  
                }
                ?>
                </select>
            </div>

            <div class="form-group col-md-6 col-xs-12 col-lg-6">
                <label class="control-label">Send Confirmation :</label>
                <input type="checkbox" name="appointment_send_confirmation" id="appointment_send_confirmation" value="<?php ($appointment_send_confirmation == '') ? '1' : $appointment_send_confirmation;?>" />
            </div>
            <div class="clearfix"></div>

            <div class="form-group col-md-12 col-xs-12 col-lg-12">
                <label id="appointment-textarea" class="control-label">Comments :</label>
                <textarea name="appointment_comment" id="appointment_comment"></textarea>
                <?php //echo $this->ckeditor->editor("appointment_comment", $appointment_comment); ?>
            </div>

            <div class="clearfix"></div>

            <div class="form-group col-md-6 col-xs-12 col-lg-6 hidden">
                <label class="control-label">Status :</label>
                <select class="form-control" name="appointment_status" id="appointment_status">
                    <option value="Enable" <?php if($appointment_status=="Enable"){ echo ' selected="selected"';} ?>>Enable</option>
                    <option value="Disable" <?php if($appointment_status=="Disable"){ echo ' selected="selected"';} ?>>Disable</option>
                </select>
            </div>
            
            <div class="clearfix"></div>
            
            <div class="row">
                <div class="form-group col-md-12 col-xs-12 col-lg-12">
                    <button id="appointment-cancel" type="button" class="btn btn-danger" onclick="window.location='<?php echo ADMIN_URL;?>Appointments'">Cancel</button>
                    <button id="appointment-submit" type="submit"  class="btn btn-success">Submit</button>
                </div>    
            </div>
        </form>
    </div>
</div>