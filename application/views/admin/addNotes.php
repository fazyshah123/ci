<?php
$crumb2 = "";
if(isset($tbl_data['n_id'])&&$tbl_data['n_id']!=""){
	$n_id = $tbl_data['n_id'];
	$n_clinic_id = $tbl_data['n_clinic_id'];
	$n_added = $tbl_data['n_added'];
	$n_updated = $tbl_data['n_updated'];
	$n_created_by = $tbl_data['n_created_by'];
	$n_updated_by = $tbl_data['n_updated_by'];
	$n_is_deleted = $tbl_data['n_is_deleted'];
	$n_status = $tbl_data['n_status'];
	$n_text = $tbl_data['n_text'];
	$n_visibility = $tbl_data['n_visibility'];
    $crumb = "Edit";
    $action = "editRecord/".$tbl_data['n_id'];
}
else{
	$n_id = '';
	$n_clinic_id = '';
	$n_added = '';
	$n_updated = '';
	$n_created_by = '';
	$n_updated_by = '';
	$n_is_deleted = '';
	$n_status = '';
	$n_text = '';
	$n_visibility = '';
    $on_home = "No";
    $crumb = "Add";
    $action = "addRecord";
}
?>
<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo ADMIN_URL;?>">
            <i class="entypo-home"></i>Home</a>
    </li>
    <li>
        <a href="<?php echo ADMIN_URL.$this->controller;?>">
            <i></i><?php echo $this->moduleName;?>
        </a>
    </li>
    <li class="active">
        <strong><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></strong>
    </li>
</ol>

<h2><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></h2>
<br />
<div class="panel panel-primary">
    <div class="panel-body">
        <form  id="page_form"
               name="page_form"
               method="post"
               action="<?php echo base_url('manage/'.$this->controller.'/'.$action);?>"
               enctype="multipart/form-data"
               class="validate"
        >
                        <div class="form-group">
                <label class="control-label">n_clinic_id :</label>
                <input type="text" name="n_clinic_id" id="n_clinic_id" value="<?php echo $n_clinic_id;?>" class="form-control " placeholder="n_clinic_id" data-validate="required,maxlength[250]"/>
            </div>

            <div class="form-group">
                <label class="control-label">n_added :</label>
                <input type="text" name="n_added" id="n_added" value="<?php echo $n_added;?>" class="form-control " placeholder="n_added" data-validate="required,maxlength[250]"/>
            </div>

            <div class="form-group">
                <label class="control-label">n_updated :</label>
                <input type="text" name="n_updated" id="n_updated" value="<?php echo $n_updated;?>" class="form-control " placeholder="n_updated" data-validate="required,maxlength[250]"/>
            </div>

            <div class="form-group">
                <label class="control-label">n_created_by :</label>
                <input type="text" name="n_created_by" id="n_created_by" value="<?php echo $n_created_by;?>" class="form-control " placeholder="n_created_by" data-validate="required,maxlength[250]"/>
            </div>

            <div class="form-group">
                <label class="control-label">n_updated_by :</label>
                <input type="text" name="n_updated_by" id="n_updated_by" value="<?php echo $n_updated_by;?>" class="form-control " placeholder="n_updated_by" data-validate="required,maxlength[250]"/>
            </div>

            <div class="form-group">
                <label class="control-label">n_status :</label>
                <input type="text" name="n_status" id="n_status" value="<?php echo $n_status;?>" class="form-control " placeholder="n_status" data-validate="required,maxlength[250]"/>
            </div>


            <div class="form-group" >
				<?php echo $this->ckeditor->editor("n_text", $n_text); ?>
			</div>



            <div class="form-group">
                <button type="button" class="btn btn-danger" onclick="window.location='<?php echo ADMIN_URL;?>pages'">Cancel</button>
                <button type="submit"  class="btn btn-success">Submit</button>
            </div>
        </form>
    </div>
</div>