<?php
$crumb2 = "";
if(isset($tbl_data['rp_id'])&&$tbl_data['rp_id']!=""){
	$rp_id = $tbl_data['rp_id'];
	$rp_room_id = $tbl_data['rp_room_id'];
	$rp_patient_id = $tbl_data['rp_patient_id'];
	$rp_alloted_by = $tbl_data['rp_alloted_by'];
	$rp_status = $tbl_data['rp_status'];
	$rp_created_by = $tbl_data['rp_created_by'];
	$rp_updated_by = $tbl_data['rp_updated_by'];
	$rp_added = $tbl_data['rp_added'];
	$rp_admission_date = $tbl_data['rp_admission_date'];
	$rp_discharge_date = $tbl_data['rp_discharge_date'];
	$rp_updated = $tbl_data['rp_updated'];
	$rp_is_deleted = $tbl_data['rp_is_deleted'];
	$rp_clinic_id = $tbl_data['rp_clinic_id'];
    $crumb = "Edit";
    $action = "editRecord/".$tbl_data['rp_id'];
}
else{
	$rp_id = '';
	$rp_room_id = '';
	$rp_patient_id = '';
	$rp_alloted_by = ''; 
	$rp_status = '';
	$rp_created_by = '';
	$rp_updated_by = '';
	$rp_added = '';
	$rp_admission_date = date('d F Y', strtotime('now'));
	$rp_discharge_date = '';
	$rp_updated = '';
	$rp_is_deleted = '';
	$rp_clinic_id = '';
    $on_home 		= "No";
    $crumb 			= "Add";
    $action 		= "addRecord";
}
?>
<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo ADMIN_URL;?>">
            <i class="entypo-home"></i>Home
        </a>
    </li>
    <li>
        <a href="<?php echo ADMIN_URL.$this->controller;?>">
            <i></i><?php echo $this->moduleName;?>
        </a>
    </li>
    <li class="active">
        <strong><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></strong>
    </li>
</ol>

<h2><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></h2>
<br />
<div class="panel panel-primary">
    <div class="panel-body">
        <form  id="page_form"
               name="page_form"
               method="post"
               action="<?php echo base_url('manage/'.$this->controller.'/'.$action);?>"
               enctype="multipart/form-data"
               class="validate"
        >
            <div class="form-group">
                <label class="control-label">Patient :<span style="color:red"> *</span></label>
                <select class="form-control patientsDropdown" name="rp_patient_id" id="rp_patient_id">
                    <option value="Select">Select</option>
                <?php
                $patients = $this->SqlModel->getPatientsDropDown();
                foreach ($patients as $key => $value) {
                    $selected = '';
                    if($value['patient_id']==$rp_patient_id) { 
                        $selected = 'selected';
                    } else {
                        $selected = '';
                    }
                    echo '<option '.$selected.' value="'.$value['patient_id'].'">'.$value["patient_name"].'</option>';
                }
                ?>
                </select>
            </div>

            <div class="form-group">
                <label class="control-label">Room :<span style="color:red"> *</span></label>
                <select class="form-control bed_type_dropdown" name="rp_room_id" id="rp_room_id">
                    <option value="Select">Select</option>
              <?php
                $room = $this->SqlModel->getRoomDropDown();
                foreach ($room as $key => $value) {
                    if($value['ro_id']==$rp_room_id) { 
                        $selected = ' selected="selected"';
                    } else {
                        $selected = '';
                    }
                    if($value['ro_is_booked']==1) { 
                        $booked = ' disabled style="background-color: red; color: white;"';
                    } else {
                        $booked = '0';
                    }
                    echo '<option ' . $booked . ' '.$selected.' value="'.$value['ro_id'].'">'.$value["ro_number"].'</option>';
                }
                ?>
                </select>
            </div>

            <div class="form-group">
                <label class="control-label">Alloted By :<span style="color:red"> *</span></label>
                <select class="form-control users_dropdown" name="rp_alloted_by" id="rp_alloted_by">
                    <option value="Select">Select</option>
                <?php
                $users = $this->SqlModel->getAdminUsersDropDown();
                foreach ($users as $key => $value) {
                    if($value['id']==$rp_alloted_by) { 
                        $selected = ' selected="selected"';
                    } else {
                        $selected = '';
                    }
                    echo '<option '.$selected.' value="'.$value['id'].'">'.$value["full_name"].'</option>';
                }
                ?>
                </select>
            </div>


            <div class="form-group" style="height: 53px;">
                <label class="control-label">Admission Date :<span class="req"> *</span></label>
                <div class="date-and-time">
                    <input style="width: 100%;float: left;" required type="text" name="bp_admission_date" id="rp_admission_date" value="<?php echo $rp_admission_date;?>" autocomplete="off" class="form-control datepicker"  data-format="dd MM yyyy" placeholder="Admission Date"/>
                </div>
            </div>

            <div class="form-group" style="height: 53px;">
                <label class="control-label">Discharge Date :</label>
                <div class="date-and-time">
                    <input style="width: 100%;float: left;"  type="text" name="rp_discharge_date" id="rp_discharge_date" value="<?php echo $rp_discharge_date;?>" autocomplete="off" class="form-control datepicker"  data-format="dd MM yyyy" placeholder="Discharge Date"/>
                </div>
            </div>
            

            <div class="form-group">
                <button type="button" class="btn btn-danger" onclick="window.location='<?php echo ADMIN_URL;?>roompatient'">Cancel</button>
                <button type="submit" id="roompasubmit" class="btn btn-success">Submit</button>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    document.addEventListener("DOMContentLoaded", function(event) {
        $("#roompasubmit").on('click', function(e) {
            e.preventDefault();
            var rp_patientid = $("#rp_patient_id").val();
            var rp_roomid = $("#rp_room_id").val();
            var rp_allotedby = $("#rp_alloted_by").val();
            
            if (rp_patientid == 'Select') {
                alert('Please select a Patient ');
                return;
            }
            if (rp_roomid == 'Select') {
                alert('Please enter select a Room');
                return;
            }
            if (rp_allotedby == 'Select') {
                alert('Please select a Alloted By');
                return;
            }
            $("#page_form").submit();
        });
    });
</script>