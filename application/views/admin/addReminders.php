<?php
$crumb2 = "";
if(isset($tbl_data['r_id'])&&$tbl_data['r_id']!=""){
	$r_id = $tbl_data['r_id'];
	$r_clinic_id = $tbl_data['r_clinic_id'];
	$r_added = $tbl_data['r_added'];
	$r_updated = $tbl_data['r_updated'];
	$r_created_by = $tbl_data['r_created_by'];
	$r_updated_by = $tbl_data['r_updated_by'];
	$r_is_deleted = $tbl_data['r_is_deleted'];
	$r_status = $tbl_data['r_status'];
	$r_remind_on = date('d F Y H:i A', strtotime($tbl_data['r_remind_on']));
	$r_type = $tbl_data['r_type'];
	$r_completed = $tbl_data['r_completed'];
	$r_text = htmlspecialchars_decode($tbl_data['r_text']);
	$r_recurring = $tbl_data['r_recurring'];
	$r_report_to = $tbl_data['r_report_to'];
    $r_assigned_to = $tbl_data['r_assigned_to'];
	$r_priority = $tbl_data['r_priority'];
	$r_lock_after = date('d F Y', strtotime($tbl_data['r_lock_after']));
    $crumb = "Edit";
    $action = "editRecord/".$tbl_data['r_id'];
}
else{
	$r_id = '';
	$r_clinic_id = '';
	$r_added = '';
	$r_updated = '';
	$r_created_by = '';
	$r_updated_by = '';
	$r_is_deleted = '';
	$r_status = '';
	$r_remind_on = date('Y-m-d H:i:s', strtotime('now'));
	$r_type = '';
	$r_completed = '';
	$r_text = '';
	$r_recurring = '';
	$r_report_to = 'Select';
    $r_assigned_to = 'Select';
	$r_priority = 'Select';
	$r_lock_after = date('Y-m-d H:i:s', strtotime('now'));;
    $on_home 		= "No";
    $crumb 			= "Add";
    $action 		= "addRecord";
}
?>
<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo ADMIN_URL;?>">
            <i class="entypo-home"></i>Home
        </a>
    </li>
    <li>
        <a href="<?php echo ADMIN_URL.$this->controller;?>">
            <i></i><?php echo $this->moduleName;?>
        </a>
    </li>
    <li class="active">
        <strong><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></strong>
    </li>
</ol>

<h2><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></h2>
<br />
<div class="panel panel-primary">
    <div class="panel-body">
        <form  id="page_form"
               name="page_form"
               method="post"
               action="<?php echo base_url('manage/'.$this->controller.'/'.$action);?>"
               enctype="multipart/form-data"
               class="validate"
        >
            <div class="form-group hidden">
                <label class="control-label">Status :</label>
                <select class="form-control"  name="r_status" id="r_status">
                    <option value="Enable" <?php if($r_status=="Enable"){ echo ' selected="selected"';} ?>>Enable</option>
                    <option value="Disable" <?php if($r_status=="Disable"){ echo ' selected="selected"';} ?>>Disable</option>
                </select>
            </div>

            <div class="form-group col-md-4" style="padding-left: 0px;">
                <label class="control-label">Remind On (Date) : <span class="req"> *</span></label>
                <div class="date-and-time">
                    <input required style="width: 100%;float: left;" tabindex="7" name="r_remind_on_date" id="r_remind_on_date" type="text" autocomplete="off" class="form-control datepicker" value="<?php echo date('d F Y', strtotime($r_remind_on));?>" data-format="dd MM yyyy" placeholder="Remind On (Date)">
                </div>
            </div>

            <div class="form-group col-md-4">
                <label class="control-label">Remind On (Time) : <span class="req"> *</span></label>
                <div class="date-and-time">
                    <input required style="width: 100%;float: left;" tabindex="7" name="r_remind_on_time" id="r_remind_on_time" type="text" autocomplete="off" class="form-control timepicker" value="<?php echo date('H:i A', strtotime($r_remind_on));?>" data-format="dd MM yyyy" placeholder="Remind On (Time)">
                </div>
            </div>

            <div class="form-group col-md-4" style="padding-right: 0px;">
                <label class="control-label">Reminder Type :<span class="req"> *</span></label>
                <select class="form-control"  name="r_type" id="r_type">
                    <option value="Select">Select</option>
                    <option value="Private" <?php if($r_type=="Private"){ echo ' selected="selected"';} ?>>Private</option>
                    <option value="Public" <?php if($r_type=="Public"){ echo ' selected="selected"';} ?>>Public</option>
                </select>
            </div>

            <div class="clearfix"></div>

            <div class="form-group col-md-3" style="padding-left: 0px;">
                <label class="control-label">Priority : <span class="req"> *</span></label>
                <select class="form-control"  name="r_priority" id="r_priority">
                    <option value="Select">Select</option>
                    <option value="Critical" <?php if($r_priority=="Critical"){ echo ' selected="selected"';} ?>>Critical</option>
                    <option value="Important" <?php if($r_priority=="Important"){ echo ' selected="selected"';} ?>>Important</option>
                    <option value="Normal" <?php if($r_priority=="Normal"){ echo ' selected="selected"';} ?>>Normal</option>
                    <option value="Low" <?php if($r_priority=="Low"){ echo ' selected="selected"';} ?>>Low</option>
                </select>
            </div>

            <div class="form-group col-md-3" style="padding-right: 0px;">
                <label class="control-label">Report To : <span class="req"> *</span></label>
                <select class="form-control check_in_user"  name="r_report_to" id="r_report_to">
                    <option value="Select">Select</option>
                <?php
                $users = $this->SqlModel->getAdminUsersDropDown();
                foreach ($users as $key => $value) {
                    echo '<option value="'.$value['id'].'">'.$value["full_name"].'</option>';
                }
                ?>
                </select>
            </div>

            <div class="form-group col-md-3">
                <label class="control-label">Assigned To :<span class="req"> *</span></label>
                <select class="form-control check_in_user"  name="r_assigned_to" id="r_assigned_to">
                    <option value="Select">Select</option>
                <?php
                $users = $this->SqlModel->getAdminUsersDropDown();
                foreach ($users as $key => $value) {
                    echo '<option value="'.$value['id'].'">'.$value["full_name"].'</option>';
                }
                ?>
                </select>
            </div>

            <div class="form-group col-md-3" style="padding-left: 0px;">
                <label class="control-label">Lock After :</label>
                <div class="date-and-time">
                    <input style="width: 100%;float: left;" tabindex="7" name="r_lock_after" id="r_lock_after" type="text" autocomplete="off" class="form-control datepicker" value="<?php echo date('d F Y', strtotime($r_lock_after));?>" data-format="dd MM yyyy" placeholder="Remind On (Date)">
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="form-group" >
                <label class="control-label">Description :</label>
                <?php echo $this->ckeditor->editor("r_text", $r_text); ?>
            </div>

            <div class="form-group" >
                <label class="control-label">Completed :</label>
                <input type="checkbox" name="r_completed" id="r_completed" value="1" <?php ($r_completed == '') ? '' : 'checked';?>" />
            </div>


            <div class="form-group">
                <button type="button" class="btn btn-danger" onclick="window.location='<?php echo ADMIN_URL;?>reminders'">Cancel</button>
                <button id="remindersubmit" type="submit"  class="btn btn-success">Submit</button>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    document.addEventListener("DOMContentLoaded", function(event) {
        $("#r_report_to").val('<?=$r_report_to;?>').trigger('change');
        $("#r_assigned_to").val('<?=$r_assigned_to;?>').trigger('change');
        $("#r_priority").val('<?=$r_priority;?>').trigger('change');
        $("#remindersubmit").on('click',function(e){
            e.preventDefault();
            var r_typ = $("#r_type").val();
            if ( r_typ == 'Select') {
                alert('Please select Reminder Type');
                return;
            }
            var r_priorit = $("#r_priority").val();
            if ( r_priorit == 'Select') {
                alert('Please select Priority');
                return;
            }
            var r_report_t = $("#r_report_to").val();
            if ( r_report_t == 'Select') {
                alert('Please select ReportTo');
                return;
            }
            var r_assigned_t = $("#r_assigned_to").val();
            if ( r_assigned_t == 'Select') {
                alert('Please select AssignedTo');
                return;
            }

            $('#page_form').submit();
        });
    });
</script>