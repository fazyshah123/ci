<?php
$crumb2 = "";
if(isset($tbl_data['procedure_id'])&&$tbl_data['procedure_id']!=""){
	$procedure_id = $tbl_data['procedure_id'];
    $procedure_name = $tbl_data['procedure_name'];
    $procedure_price = $tbl_data['procedure_price'];
    $procedure_status = $tbl_data['procedure_status'];
    $crumb = "Edit";
    $action = "editRecord/".$tbl_data['procedure_id'];
}
else{
	$procedure_id = '';
    $procedure_name = '';
    $procedure_price = '';
    $procedure_status = '';
    $on_home 		= "No";
    $crumb 			= "Add"; 
    $action 		= "addRecord";
}
?>
<style type="text/css">
    input, select {
        border: 1px solid #a5a4a4 !important;
    }
</style>
<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo ADMIN_URL;?>">
            <i class="entypo-home"></i>Home
        </a>
    </li>
    <li>
        <a href="<?php echo ADMIN_URL.$this->controller;?>">
            <i></i><?php echo $this->moduleName;?>
        </a>
    </li>
    <li class="active">
        <strong><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></strong>
    </li>
</ol>

<h2><?php echo $crumb;?> <?php echo rtrim($this->moduleName,'s');?></h2>
<br />
<div class="panel panel-primary">
    <div class="panel-body">
        <form  id="page_form"
               name="page_form"
               method="post"
               action="<?php echo base_url('manage/'.$this->controller.'/'.$action);?>"
               enctype="multipart/form-data"
               class="validate"
        >

            <div class="form-group col-md-6 col-xs-12 col-lg-6">
                <label class="control-label">Name <span style="color: red">*</span> :</label>
                <input required type="text" name="procedure_name" id="procedure_name" value="<?php echo $procedure_name;?>" class="form-control " placeholder="Name" data-validate="maxlength[250]"/>
            </div>

            <div class="form-group col-md-6 col-xs-12 col-lg-6">
                <label class="control-label">Price: <span style="color: red">*</span></label>
                <input required type="number" name="procedure_price" id="procedure_price" value="<?php echo $procedure_price;?>" class="form-control " placeholder="Price." data-validate="maxlength[250]"/>
            </div>
  
            <div class="clearfix"></div>

            <div class="form-group col-md-6 col-xs-12 col-lg-6 hidden">
                <label class="control-label">Status :</label>
                <select class="form-control" name="procedure_status" id="procedure_status">
                    <option value="Enable" <?php if($procedure_status=="Enable"){ echo ' selected="selected"';} ?>>Enable</option>
                    <option value="Disable" <?php if($procedure_status=="Disable"){ echo ' selected="selected"';} ?>>Disable</option>
                </select>
            </div>
            
            <div class="clearfix"></div>
            
            <div class="row">
                <div class="form-group col-md-12 col-xs-12 col-lg-12">
                    <button type="button" class="btn btn-danger" onclick="window.location='<?php echo ADMIN_URL;?>procedures'">Cancel</button>
                    <button id="procedures_submit" type="submit"  class="btn btn-success">Submit</button>
                </div>    
            </div>
        </form>
    </div>
</div>