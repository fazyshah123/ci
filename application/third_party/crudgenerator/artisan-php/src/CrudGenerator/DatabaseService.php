<?php

define('HOST', 'localhost');
define('USER_NAME', 'root');
define('PASSWORD', '');
define('DATABASE', 'bitcloud_usa');

class DatabaseService {

	public 	$con,
		$where = '';

	public function DatabaseService()
	{
		$this->con = mysqli_connect(HOST,USER_NAME,PASSWORD,DATABASE);
		if(mysqli_connect_errno()) {
		  	echo "Failed to connect to MySQL: " . mysqli_connect_error();
		  	exit();
		}
	}

	public function beginTransactionWithWrite()
	{
		return mysqli_begin_transaction($this->con, MYSQLI_TRANS_START_READ_WRITE);
	}

	public function beginTransactionWithReadOnly()
	{
		return mysqli_begin_transaction($this->con, MYSQLI_TRANS_START_READ_ONLY);
	}

	public function commit()
	{
		/* commit insert */
		return mysqli_commit($this->con);
	}

	public function rollback()
	{
		/* Rollback */
		return mysqli_rollback($this->con);
	}

	public function disableAutoCommit()
	{
		/* disable autocommit */
		mysqli_autocommit($this->con, FALSE);
	}

	public function query($query)
	{

		$r = mysqli_query($this->con,$query);
		if(!$r) {
			echo $query . ' :: Query failed due to <br />';
			echo mysqli_error($this->con) . '<--';
			return false;
		}
		return $r;
	}

	public function getResult($query)
	{

		if($row = $this->query($query)) {
			return mysqli_fetch_array($row, MYSQL_ASSOC);
		}
		return null;
	}

	public function getResults($query)
	{
		$recordsArray = array();
		if($rows = $this->query($query)) {
			while($records = mysqli_fetch_array($rows, MYSQL_ASSOC)) {
				$recordsArray[] = $records;
			}
			return $recordsArray;
		}
		return null;
	}

	public function getLastInsertId()
	{
		return $this->con->insert_id;
	}

	public function where()
	{
		$this->where = '';
	}

	public function andWhereEqual($params = array())
	{
		if(count($params) < 1) {
			return false;
		}
		if($this->where === '') {
			$this->where = ' where (';
		} else {
			$this->where = ' and (';
		}
		foreach($params as $key => $value) {
			$this->where .= $key . " = " . "'".$value."' ";
			if(array_search($key, array_keys($params)) < (count($params)-1)) {
				$this->where .= ' and ';
			}
		}
		return $this->where.')';
	}

	public function andWhereLike($params = array())
	{
		if(count($params) < 1) {
			return false;
		}
		if($this->where === '') {
			$this->where = ' where (';
		} else {
			$this->where = ' and (';
		}
		foreach($params as $key => $value) {
			$this->where .= $key . " like " . "'%".$value."%'";
			if(array_search($key, array_keys($params)) < (count($params)-1)) {
				$this->where .= ' and ';
			}
		}
		return $this->where.')';
	}

	public function andWhereNotLike($params = array())
	{
		if(count($params) < 1) {
			return false;
		}
		if($this->where === '') {
			$this->where = ' where (';
		} else {
			$this->where = ' and (';
		}
		for ($i=0; $i < count($params); $i++) {
			foreach($params[$i] as $key => $value) {
				$this->where .= $key . " not like " . "'%".$value."%'";
				if($i < count($params)-1) {
					$this->where .= ' and ';
				}
			}
		}
		return $this->where.')';
	}

	public function andWhereBtw($params = array())
	{
		if(count($params) < 1) {
			return false;
		}
		if($this->where === '') {
			$this->where = ' where (';
		} else {
			$this->where = ' and (';
		}
		foreach($params as $key => $value) {
			$this->where .= $key . " between " . "'".$value['value1']."' and '".$value['value2']."'";
			if(array_search($key, array_keys($params)) < (count($params)-1)) {
				$this->where .= ' and ';
			}
			return $this->where.')';
		}
	}


	public function orWhereEqual($params = array())
	{
		if(count($params) < 1) {
			return false;
		}
		if($this->where === '') {
			$this->where = ' where (';
		} else {
			$this->where = ' or (';
		}
		foreach($params as $key => $value) {
			$this->where .= $key . " = " . "'".$value."' ";
			if(array_search($key, array_keys($params)) < (count($params)-1)) {
				$this->where .= ' or ';
			}
		}
		return $this->where.')';
	}

	public function orWhereLike($params = array())
	{
		if(count($params) < 1) {
			return false;
		}
		if($this->where === '') {
			$this->where = ' where (';
		} else {
			$this->where = ' or (';
		}
		foreach($params as $key => $value) {
			$this->where .= $key . " like " . "'%".$value."%'";
			if(array_search($key, array_keys($params)) < (count($params)-1)) {
				$this->where .= ' or ';
			}
		}
		return $this->where.')';
	}

	public function orWhereBtw($params = array())
	{
		if(count($params) < 1) {
			return false;
		}
		if($this->where === '') {
			$this->where = ' where (';
		} else {
			$this->where = ' or (';
		}
		foreach($params as $key => $value) {
			$this->where .= $key . " between " . "'".$value['value1']."' and '".$value['value2']."'";
			if(array_search($key, array_keys($params)) < (count($params)-1)) {
				$this->where .= ' or ';
			}
			return $this->where.')';
		}
	}

	public function getWhereClause($params=array())
	{
		$where = '';
		if(isset($params['andParamsEqual'])) {
			if (count($params['andParamsEqual']) > 0) {
				$this->where();
				$where .= $this->andWhereEqual($params['andParamsEqual']);
			}
		}

		if(isset($params['andParamsLike'])) {
			if (count($params['andParamsLike']) > 0) {
				$where .= $this->andWhereLike($params['andParamsLike']);
			}
		}

		if (isset($params['andParamsNotLike'])) {
			if (count($params['andParamsNotLike'])) {
				$where .= $this->andWhereNotLike($params['andParamsNotLike']);
			}
		}

		if (isset($params['data']['sort_by'])) {
			$where .= ' order by '.$params['data']['sort_by'].' '.$params['data']['order_by'];
		}

		if(isset($params['data']['page_position']) && isset($params['data']['limit'])){
			$where .= ' limit '. $params['data']['page_position'] . ', ' . $params['data']['limit'];
		}

		return $where;
	}
}