<?php
/**
 *
 */
class FormGenerator
{
    public $dbService = null;

    public function __construct()
    {
        $this->dbService = new DatabaseService();
    }

    public function CreateNewFormGenerator($data=array())
    {
        extract($data);
        // Gets controller template content
        $form_content = file_get_contents(TEMPLATE_ROOT.'php_templates/ci_add_entity.php-template');
        $field_content = file_get_contents(TEMPLATE_ROOT.'php_templates/ci_formfieldtype.php-template');

        $form_fields_array = preg_split('/\/\/FIELD/', $field_content);

        $res = $this->dbService->query("SHOW COLUMNS FROM ". $table_name);

        // Create Fields
        if ($res) {

            $edit_fields = '';
            $add_fields = '';
            $form_fields = '';
            $id = '';
            $i = 1;
            $files = '';
            while($row = $res->fetch_assoc()) {
                $i++;
                $edit_fields .= "\t\$".$row['Field']." = \$tbl_data['".$row['Field']."'];".PHP_EOL;
                $add_fields .=  "\t\$".$row['Field']." = '';".PHP_EOL;

                if ($row['Extra'] == 'auto_increment' && ($row['Key'] == 'PRI' || $row['Key'] == 'MUL')) {
                    $id = $row['Field'];
                    continue;
                }

                if (strpos($row['Type'], 'enum(') !== false) {
                    preg_match("/^enum\(\'(.*)\'\)$/", $row['Type'], $matches);
                    $enum = explode("','", $matches[1]);
                    $select_option = '';
                    $str = '';
                    $hold = '';
                    foreach ($enum as $key => $value) {
                        //$str .= str_replace('{{COLUMN}}', $row['Field'], $form_fields_array[3]);
                        $str .= str_replace('{{SELECT_VALUE}}', $value, $form_fields_array[4]);
                        $str = str_replace('{{COLUMN}}', $row['Field'], $str);
                        $select_option = $str;
                    }
                    $hold = str_replace('{{SELECT_OPTIONS}}', $select_option, $form_fields_array[3]);
                    $form_fields .= str_replace('{{COLUMN}}', $row['Field'], $hold);
                }

                if (
                    strpos($row['Type'], 'int(') !== false ||
                    strpos($row['Type'], 'datetime') !== false ||
                    strpos($row['Type'], 'varchar(') !== false
                ) {
                    $form_fields .= str_replace('{{COLUMN}}', $row['Field'], $form_fields_array[0]);
                } else if (strpos($row['Field'], '_file') !== false) {
                    $form_fields .= str_replace('{{COLUMN}}', $row['Field'], $form_fields_array[1]);
                } else if (strpos($row['Type'], 'text') !== false) {
                    $form_fields .= str_replace('{{COLUMN}}', $row['Field'], $form_fields_array[2]);
                }
            }
            // Replaces template values to entity values
            $form_content = str_replace('{{DATE_CREATED}}', $date, $form_content);
            $form_content = str_replace('{{ENTITY}}', $entity_name, $form_content);
            $form_content = str_replace('{{MODULE_NAME}}', $module_name, $form_content);
            $form_content = str_replace('{{TABLE_NAME}}', strtolower($table_name), $form_content);
            $form_content = str_replace('{{CONTROLLER_NAME}}', $controller_name, $form_content);
            $form_content = str_replace('{{PRIMARY_KEY}}', $id, $form_content);
            $form_content = str_replace('{{EDIT_FIELDS}}', $edit_fields, $form_content);
            $form_content = str_replace('{{ADD_FIELDS}}', $add_fields, $form_content);
            $form_content = str_replace('{{FORM_FIELDS}}', $form_fields, $form_content);

            // Updates author information
            $form_content = str_replace('{{AUTHOR_NAME}}', $author_name, $form_content);
            $form_content = str_replace('{{AUTHOR_EMAIL}}', $author_email, $form_content);
        }

        $file_path = 'application/views/admin/add'.ucfirst($controller_name).'.php';

        // Creates new controller
        $controller_file = fopen($file_path, "w") or die("Unable to open file!");

        // Writes content to file and confirms
        if(fwrite($controller_file, $form_content)){
            // Sends output to user terminal
            echo PHP_EOL.$file_path. ' File created!';
        }

        // Closes file handler
        fclose($controller_file);
    }
}