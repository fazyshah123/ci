<?php
/**
 *
*/
class RoleManagementGenerator
{
    public $dbService = null;

    public function __construct()
    {
        $this->dbService = new DatabaseService();
    }

    public function CreateRoleManagementGenerator($data=array())
    {
        extract($data);
        
        $file_path = 'application/controllers/manage/rolemanagement.php';

        // Gets controller template content
        $new_role = file_get_contents(TEMPLATE_ROOT.'php_templates/ci_rolemanagement_controller.php-template');
        $rm_controller_content = file_get_contents($file_path);
        

        $nrc = str_replace('{{CONTROLLER_NAME}}', $controller_name, $new_role);
            
        // Replaces template values to entity values
        $rmc = str_replace('// {{ROLEMANAGEMENT_CONTROLLER}}', $nrc, $rm_controller_content);


        // Creates new controller
        $controller_file = fopen($file_path, "w") or die("Unable to open file!");

        // Writes content to file and confirms
        if(fwrite($controller_file, $rmc)){
            // Sends output to user terminal
            echo PHP_EOL.$file_path. ' updated role management content!';
        }

        // Closes file handler
        fclose($controller_file);
    }

    public function AddRoleManagementForm($data=array())
    {
        extract($data);
        
        $file_path = 'application/views/admin/addRolemanagement.php';

        // Gets controller template content
        $new_role = file_get_contents(TEMPLATE_ROOT.'php_templates/ci_add_rolemanagement.php-template');
        $rm_controller_content = file_get_contents($file_path);
        

        $nrc = str_replace('{{CONTROLLER_NAME}}', $controller_name, $new_role);
        $nrc = str_replace('{{MODULE_NAME}}', $module_name, $nrc);
            
        // Replaces template values to entity values
        $rmc = str_replace('<!-- {{ROLEMANAGEMENT_CHECKBOX}} -->', $nrc, $rm_controller_content);


        // Creates new controller
        $controller_file = fopen($file_path, "w") or die("Unable to open file!");

        // Writes content to file and confirms
        if(fwrite($controller_file, $rmc)){
            // Sends output to user terminal
            echo PHP_EOL.$file_path. ' add role management form!';
        }

        // Closes file handler
        fclose($controller_file);
    }
}