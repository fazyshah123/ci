<?php
include_once('ControllerGenerator.php');
include_once('DatabaseService.php');
include_once('AdminDashboardGenerator.php');
include_once('FormGenerator.php');
include_once('RoleManagementGenerator.php');
include_once('SqlModelGenerator.php');
include_once('HelperFunctions.php');

define('TEMPLATE_ROOT', 'application/third_party/crudgenerator/artisan-php/src/CrudGenerator/');
/* Timezones constants */
define('DEFAULT_TIMEZONE', 'Asia/Karachi');
date_default_timezone_set(DEFAULT_TIMEZONE);


/**
*
*/
class CrudGenerator
{
    public $helper = null;
    public $controller = null;
    public $adminDashboad = null;
    public $form = null;
    public $rolemanagement = null;
    public $sqlmodel = null;
    public $data = array();

    public function __construct()
    {
        $this->helper = new HelperFunctions();
        $this->controller = new ControllerGenerator();
        $this->adminDashboad = new AdminDashboardGenerator();
        $this->form = new FormGenerator();
        $this->rolemanagement = new RoleManagementGenerator();
        $this->sqlmodel = new SqlModelGenerator();

        // Collects Crud Info
        $this->data = $this->helper->GatherCrudInfo();

        // Calls Controller Generator
        $this->controller->CreateNewGenerator($this->data);

        // Calls Entity Admin Dashboard Generator
        $this->adminDashboad->CreateNewAdminDashboard($this->data);

        // Calls Entity Form Generator
        $this->form->CreateNewFormGenerator($this->data);

        // Calls Role Management Generator
        $this->rolemanagement->CreateRoleManagementGenerator($this->data);

        // Calls Role Management Generator
        $this->rolemanagement->AddRoleManagementForm($this->data);

        // Calls Role Management Generator
        $this->sqlmodel->CreateSqlModelGenerator($this->data);
        /*
        // Calls View Generator
        $this->controller->CreateNewGenerator($this->data);*/
    }
}
$new_crud = new CrudGenerator();