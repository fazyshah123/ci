<?php
/**
 *
*/
class SqlModelGenerator
{
    public $dbService = null;

    public function __construct()
    {
        $this->dbService = new DatabaseService();
    }

    public function CreateSqlModelGenerator($data=array())
    {
        extract($data);
        
        $file_path = 'application/models/sqlmodel.php';

        $res = $this->dbService->query("SHOW FULL COLUMNS FROM ". $table_name);

        // Create Fields
        if ($res) {

            $edit_fields = '';
            $add_fields = '';
            $form_fields = '';
            $id = '';
            $i = 1;
            $files = '';
            while($row = $res->fetch_assoc()) {
                $i++;
                
                if (!empty($row['Comment'])) {
                    $comment = explode(',', $row['Comment']);

                    if (isset($comment[1]) && $comment[1] == 'foreign') {
                        if (strpos($row['Field'], 'clinic_id') !== false) {
                            continue;
                        }
                        
                        // Gets controller template content
                        $new_dropdown = file_get_contents(TEMPLATE_ROOT.'php_templates/ci_sqlmodel_dropdown_method.php-template');
                        $sql_model_content = file_get_contents($file_path);
                        
                        $t = explode('_', $comment[2]);
                        $nsm = str_replace('{{TABLE_NAME}}', $comment[2], $new_dropdown);
                        $nsm = str_replace('{{TABLE_PREFIX}}', $comment[3], $nsm);
                        $table = '';
                        foreach ($t as $key => $value) {
                            $table .= ucfirst($value);
                        }
                        $new_method_name = 'get'.$table.'DropDown()';
                        $check_method_exist = strpos($sql_model_content, $new_method_name) !== false;
                        if ($check_method_exist) {
                            continue;
                        }

                        $nsm = str_replace('{{TABLE}}', $table, $nsm);
                        
                        // Replaces template values to entity values
                        $sql_model_content = str_replace('// {{DROPDOWN_METHODS}}', $nsm, $sql_model_content);

                        // Creates new controller
                        $sql_file = fopen($file_path, "w") or die("Unable to open file!");

                        // Writes content to file and confirms
                        if(fwrite($sql_file, $sql_model_content)){
                            // Sends output to user terminal
                            echo PHP_EOL.$file_path. ' updated role management content!';
                        }

                        // Closes file handler
                        fclose($sql_file);
                    }
                }
            }
        }
    }
}