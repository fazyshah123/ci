<?php
/**
 *
*/
class ControllerGenerator
{
    public $dbService = null;

    public function __construct()
    {
        $this->dbService = new DatabaseService();
    }

    public function CreateNewGenerator($data=array())
    {
        extract($data);
        // Gets controller template content
        $controller_content = file_get_contents(TEMPLATE_ROOT.'php_templates/ci_controller.php-template');

        $res = $this->dbService->query("SHOW COLUMNS FROM ". $table_name);

        // Create Fields
        if ($res) {

            $created_fields = '';
            $updated_fields = '';
            $id = '';
            $i = 1;
            $files = '';
            $prefix = '$this->colPrefix.\'';
            while($row = $res->fetch_assoc()) {
                if (
                    $i > 1 && 
                    strpos($row['Field'], 'added') === false
                ) {
                    $created_fields .= "\t\t\t";

                    if (
                        strpos($row['Field'], 'is_deleted') === false &&
                        strpos($row['Field'], 'clinic_id') === false &&
                        strpos($row['Field'], 'created_by') === false &&
                        strpos($row['Field'], 'added') === false &&
                        strpos($row['Field'], 'updated') === false &&
                        strpos($row['Field'], 'updated_by') === false &&
                        $row['Key'] != 'PRI'
                    ) {
                        $updated_fields .= "\t\t\t";
                    }
                }

                if (strpos($row['Field'], '_file') === false) {
                    $field = str_replace($table_prefix, '', $row['Field']);
                    if ($row['Key'] != 'PRI') {
                        $created_fields .= $prefix . $field . '\' => ';
                    }
                    if (
                        strpos($row['Field'], 'is_deleted') === false &&
                        strpos($row['Field'], 'clinic_id') === false &&
                        strpos($row['Field'], 'created_by') === false &&
                        strpos($row['Field'], 'added') === false &&
                        $row['Key'] != 'PRI'
                    ) {

                        $updated_fields .= $prefix . $field . '\' => ';
                    }
                }

                if (strpos($row['Field'], 'is_deleted') !== false) {

                    $created_fields .= '\'0\'';
                
                } else if (strpos($row['Field'], 'clinic_id') !== false) {
                  
                    $created_fields .= '$this->session->userdata(\'clinic_id\')';
                
                } else if (strpos($row['Field'], 'created_by') !== false) {
                  
                    $created_fields .= '$this->session->userdata(\'admin_id\')';
                
                } else if (strpos($row['Field'], 'updated_by') !== false) {
                  
                    $created_fields .= '$this->session->userdata(\'admin_id\')';
                    $updated_fields .= '$this->session->userdata(\'admin_id\')';
                
                } else if (strpos($row['Field'], 'status') !== false) {
                  
                    $created_fields .= "'Enabled'";
                    $updated_fields .= "'Enabled',";
                
                } else if (strpos($row['Field'], 'added') !== false) {
                  
                    $created_fields .= 'date(\'Y-m-d H:i:s\', strtotime(\'now\'))';
                
                } else if (strpos($row['Field'], 'updated') !== false) {
                  
                    $created_fields .= 'date(\'Y-m-d H:i:s\', strtotime(\'now\'))';
                    $updated_fields .= 'date(\'Y-m-d H:i:s\', strtotime(\'now\'))';
                
                } else if (strpos($row['Field'], '_file') !== false) {
                  
                    $file_content = file_get_contents(TEMPLATE_ROOT.'php_templates/ci_file.php-template');
                    $file_content = str_replace('{{FIELD_NAME}}', $row['Field'], $file_content);
                    $file_content = str_replace('{{TABLE_NAME}}', $table_name, $file_content);
                    $files .= $file_content;
                
                } else if (strpos($row['Field'], '_json') !== false) {
                
                    $field = str_replace($table_prefix, '', $row['Field']);
                    $created_fields .= 'json_encode($this->input->post($this->colPrefix.\''.$field.'\'))';
                    $updated_fields .= 'json_encode($this->input->post($this->colPrefix.\''.$field.'\'))';
                
                } else if ($row['Type'] == 'text') {
                
                    $field = str_replace($table_prefix, '', $row['Field']);
                    $created_fields .= 'htmlspecialchars($this->input->post($this->colPrefix.\''.$field.'\'))';
                    $updated_fields .= 'htmlspecialchars($this->input->post($this->colPrefix.\''.$field.'\'))';
                
                } else if (  
                    $row['Type'] == 'timestamp' ||
                    $row['Type'] == 'datetime'
                ) {

                    $created_fields .= 'date(\'Y-m-d H:i:s\', strtotime(\'now\'))';
                    $updated_fields .= 'date(\'Y-m-d H:i:s\', strtotime(\'now\'))';
                
                } else if (strpos($row['Type'], 'varchar') !== false) {
                  
                    $field = str_replace($table_prefix, '', $row['Field']);
                    $created_fields .= 'addslashes($this->input->post($this->colPrefix.\''.$field.'\'))';
                    $updated_fields .= 'addslashes($this->input->post($this->colPrefix.\''.$field.'\'))';
                
                } else if ($row['Key'] != 'PRI' && $row['Key'] != 'MUL' && strpos($row['Field'], 'added') === false) {
                  
                    $field = str_replace($table_prefix, '', $row['Field']);
                    $created_fields .= '$this->input->post($this->colPrefix.\''.$field.'\')';
                    $updated_fields .= '$this->input->post($this->colPrefix.\''.$field.'\')';
                }

                if ($res->num_rows != $i && strpos($row['Field'], '_file') === false) {
                    if ($row['Key'] != 'PRI') {
                        $created_fields .= ','.PHP_EOL;
                    }
                    if (
                        strpos($row['Field'], 'is_deleted') === false &&
                        strpos($row['Field'], 'clinic_id') === false &&
                        strpos($row['Field'], 'created_by') === false &&
                        strpos($row['Field'], 'added') === false &&
                        strpos($row['Field'], 'status') === false &&
                        strpos($row['Field'], 'status') === false &&
                        $row['Key'] != 'PRI'
                    ) {
                        $updated_fields .= ','.PHP_EOL;
                    }
                }

                if ($row['Extra'] == 'auto_increment' && ($row['Key'] == 'PRI' || $row['Key'] == 'MUL')) {
                    $id = $row['Field'];
                }
                $i++;
            }
            // Replaces template values to entity values
            $controller_content = str_replace('{{DATE_CREATED}}', $date, $controller_content);
            $controller_content = str_replace('{{ENTITY}}', $entity_name, $controller_content);
            $controller_content = str_replace('{{MODULE_NAME}}', $module_name, $controller_content);
            $controller_content = str_replace('{{TABLE_NAME}}', strtolower($table_name), $controller_content);
            $controller_content = str_replace('{{CONTROLLER_NAME}}', $controller_name, $controller_content);
            $controller_content = str_replace('{{CONTROLLER_NAME_UC}}', ucfirst($controller_name), $controller_content);
            $controller_content = str_replace('{{PRIMARY_KEY}}', $id, $controller_content);
            $controller_content = str_replace('{{CREATED_POSTED_FIELDS}}', $created_fields, $controller_content);
            $controller_content = str_replace('{{UPDATED_POSTED_FIELDS}}', $updated_fields, $controller_content);
            $controller_content = str_replace('{{TABLE_PREFIX}}', $table_prefix, $controller_content);
            $controller_content = str_replace('//{{FILE_FIELDS}}', $files, $controller_content);

            // Updates author information
            $controller_content = str_replace('{{AUTHOR_NAME}}', $author_name, $controller_content);
            $controller_content = str_replace('{{AUTHOR_EMAIL}}', $author_email, $controller_content);
        }

        $file_path = 'application/controllers/manage/'.$controller_name.'.php';

        // Creates new controller
        $controller_file = fopen($file_path, "w") or die("Unable to open file!");

        // Writes content to file and confirms
        if(fwrite($controller_file, $controller_content)){
            // Sends output to user terminal
            echo PHP_EOL.$file_path. ' File created!';
        }

        // Closes file handler
        fclose($controller_file);
    }
}