<?php
/**
*
*/
class AdminDashboardGenerator
{
    public $dbService = null;

    public function __construct()
    {
        $this->dbService = new DatabaseService();
    }

    public function CreateNewAdminDashboard($data=array())
    {
        extract($data);

        $res = $this->dbService->query("SHOW COLUMNS FROM ". $table_name);

        $entity_name = ucfirst($entity_name);

        // Gets model template content
        $dashboard_content = file_get_contents(TEMPLATE_ROOT.'php_templates/ci_admin_dashboard.php-template');
        $admin_table_content = file_get_contents(TEMPLATE_ROOT.'php_templates/ci_admin_table.php-template');

        $head_and_body = preg_split('/\/\/TBODY/', $admin_table_content);
        $head_and_body['HEAD'] = $head_and_body[0];
        $head_and_body['BODY'] = $head_and_body[1];
        $head_and_body['BODY_IMAGE'] = $head_and_body[2];

        unset($head_and_body[0]);
        unset($head_and_body[1]);
        unset($head_and_body[2]);

        // Create Fields
        if ($res) {

            $thead = '';
            $tbody = '';
            $id = '';
            $i = 1;
            while($row = $res->fetch_assoc()) {
                if ($i == 6)
                    break;

                if ($row['Extra'] == 'auto_increment' && ($row['Key'] == 'PRI' || $row['Key'] == 'MUL')) {
                    $id = $row['Field'];
                }

                $thead .= str_replace('{{COLUMN}}', $row['Field'], $head_and_body['HEAD']);
                if (strpos($row['Field'], '_file') !== false) {
                    $tbody .= str_replace('{{COLUMN}}', $row['Field'], $head_and_body['BODY_IMAGE']);
                } else {
                    $tbody .= str_replace('{{COLUMN}}', $row['Field'], $head_and_body['BODY']);
                }

                $i++;
            }
        }

        // Replaces template values to entity values
        $dashboard_content = str_replace('//{{TABLE_HEAD}}', $thead, $dashboard_content);
        $dashboard_content = str_replace('//{{TABLE_BODY}}', $tbody, $dashboard_content);
        $dashboard_content = str_replace('{{PRIMARY_KEY}}', $id, $dashboard_content);
        $dashboard_content = str_replace('{{CONTROLLER_NAME}}', $controller_name, $dashboard_content);

        $file_path = 'application/views/admin/'.$controller_name.'.php';

        // Creates new file
        $admin_dashboard_file = fopen($file_path, "w") or die("Unable to open file!");

        // Writes content to file and confirms
        if(fwrite($admin_dashboard_file, $dashboard_content)){

            // Sends output to user terminal
            echo PHP_EOL.$file_path. ' File created!';
        }

        // Closes file handler
        fclose($admin_dashboard_file);
    }
}