<?php
class HelperFunctions
{
    public function read($length='255')
    {
       if (!isset ($GLOBALS['StdinPointer']))
       {
          $GLOBALS['StdinPointer'] = fopen ("php://stdin","r");
       }
       $line = fgets ($GLOBALS['StdinPointer'],$length);
       return trim ($line);
    }

    public function GatherCrudInfo()
    {
        $date = array();

        // PROCESSING
        // reads entity name to be generated
        echo "Enter the entity name (e.g. Person, User): ";
        $data['entity_name'] = $this->read();

        echo "Enter the Module name (e.g. Person, Web Pages, User Settings): ";
        $data['module_name'] = $this->read();

        // reads author name
        echo "Enter the author name: ";
        $data['author_name'] = 'Talib Allauddin';//$this->read();

        // reads author email
        echo "Enter the author email: ";
        $data['author_email'] = 'taliballauddin@gmail.com';//$this->read();

        // asks for table name if user wants to generate CRUD
        echo "Please enter table name if you want to generate CRUD: ";
        $data['table_name'] = $this->read();

        // asks for table name if user wants to generate CRUD
        echo "Please enter table prefix: ";
        $data['table_prefix'] = $this->read();

        // Generate values for entity
        $data['controller_name'] = strtolower(preg_replace('/\_/', '', $data['entity_name']));
        $data['service_name'] = $data['entity_name'].'Service';
        $data['model_name'] = ucfirst($data['entity_name']);
        $data['date'] = 'Date Created: '.date("F d, Y @ H:i:s");

        return $data;
    }
}