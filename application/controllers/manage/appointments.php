<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * This file is part of the CrudGenerator package.
 *
 * @author subhan shah
 *
 * (c) subhan shah <subhan.shah@bitcloudglobal.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
class Appointments extends CI_Controller {

    public $tblName 	= 'appointments';
    public $pKey 		= 'appointment_id';
    public $moduleName 	= "Appointments";
    public $controller 	= "appointments";
    public $per_page 	= '10';
    public $tStatus 	= "page_status";
    public $colPrefix	= "appointment_";
	public $addEditView = "addAppointments";

    public function __construct() {
        // Call the Model constructor
        parent::__construct();

        $this->user_data = $this->SqlModel->authAdmin($this->session->userdata('admin_auth'),$this->session->userdata('admin_id'));

        if (!isset($this->user_data) || empty($this->user_data)) {
            redirect(base_url('manage/login'));
        }

        if($this->SqlModel->checkPermissions('appointments', 'read')!==true) {
            redirect(base_url('manage'));
        }
    }

    public function index($sortby="appointment_id", $order="DESC", $status="-",$keywords="-", $pg_no="") {
        //PER_PAGE_START
        if($this->input->get('per_page')!="" && is_numeric($this->input->get('per_page')) && (int)$this->input->get('per_page')<=100) {
            $this->session->set_userdata('per_page', $this->input->get('per_page'));
        }

        if($this->session->userdata('per_page')!="") {
            $this->per_page = $this->session->userdata('per_page');
        }

        //PER_PAGE_END
        $data['alert'] = $this->session->flashdata('alert');

        $where =  array();

        $keywords = urldecode($keywords);

        if($status=="Published" || $status=="Un-Published") {
            $where[$this->tStatus] = $status;
        }
        $where[$this->colPrefix.'is_deleted'] = '0';
        $where[$this->colPrefix.'is_canceled'] = '0';
        $where[$this->colPrefix.'clinic_id'] = $this->session->userdata('clinic_id');

        $search 			= ($keywords!="-") ? array('cols'=>$this->colPrefix.'patient_id','value'=>urldecode($keywords)) : array();
        $base_url           = base_url() . 'manage/' . $this->controller . '/index/' . $sortby . "/" . $order . "/" . $status . "/" . $keywords;
        $total_rows 		= $data['total_rows'] =$this->SqlModel->countRecords($this->tblName,$where,$search);
        $per_page 			= $data['per_page'] = $this->per_page;
        $uri_segment 		= 8;

        $data['page_title'] = PROJECT_TITLE." | ".$this->moduleName;
        $data['userdata'] = $this->user_data;
        $data['pagesActive'] = 1;

        //Pagination START
        $pconfig['base_url'] = $base_url;
        $pconfig['total_rows'] = $data['total_rows'] =  $total_rows;
        $pconfig["uri_segment"] = $uri_segment ;
        $pconfig['per_page'] = $data['per_page'] = $this->per_page;
        $pconfig['num_links'] = 1;
        $pconfig['prev_link'] = '<i class="entypo-left-open-mini"></i>';
        $pconfig['next_link'] = '<i class="entypo-right-open-mini"></i>';
        $pconfig['cur_tag_open'] = '<li  class="active"><a href="javascript:void(0)">';
        $pconfig['cur_tag_close'] = '</a></li>';
        $pconfig['full_tag_open'] = '<ul class="pagination pull-right">';
        $pconfig['full_tag_close'] = '</ul>';
        $pconfig['num_tag_open'] = "<li>";
        $pconfig['num_tag_close']= "</li>";
        $pconfig['next_tag_open'] = "<li>";
        $pconfig['next_tag_close']= "</li>";
        $pconfig['prev_tag_open'] = "<li>";
        $pconfig['prev_tag_close']= "</li>";
        $pconfig['last_tag_open'] = "<li>";
        $pconfig['last_tag_close']= "</li>";
        $pconfig['first_tag_open'] = "<li>";
        $pconfig['first_tag_close']= "</li>";
        $offset = ($this->uri->segment($uri_segment )) ? $this->uri->segment($uri_segment ) : 0;
        $this->pagination->initialize($pconfig);

        $data['listing'] = $this->SqlModel->getRecordsWithThreeJoin('appointment_id, patient_name, full_name dtr_name, appointment_date, appointment_start_time, appointment_end_time, appointment_added', $this->tblName, 'patients', 'patient_id', 'appointment_patient_id', 
            'admin_users', 'id', 'appointment_dtr_id', 
            'procedures', 'procedure_id', 'appointment_procedure_id', 
            $sortby, $order,  $where, $search, $per_page, $offset,false);

        $data['paginate'] = $this->pagination->create_links();
        //Pagination END
        $data['sortby'] 	= 	$sortby;
        $data['order'] 		= 	($order=="ASC") ? "DESC" : "ASC";
        $data['page_numb'] 	= 	$offset;
        $data['status']		=	$status;
        $data['keywords']	=	$keywords;

        $this->load->view('admin/header',$data);
        $this->load->view('admin/navigation');
        $this->load->view('admin/'.$this->controller);
        $this->load->view('admin/footer');
    }

    //For adding/edting colors
    public function control($type = "",$editID="") {
        if ($type == 'edit' && $editID != "") {
            if($this->SqlModel->checkPermissions($this->controller, 'update')==false) {
                header('location: ./manage/'.$this->controller.'?alert=permerror');
            }
        }
        $data['type'] = (($type == "") ? ($type == "new") ? "Add" : "Add" : ucfirst($type));
       // echo $data['type'];exit;
        $alert = $this->session->flashdata('alert');
        $data['datePicker'] = 1;
        $data['pagesActive'] = 1;
        $data['alert'] = $alert;
        $data['page_title'] = PROJECT_TITLE." | ".$type." ".$this->moduleName;
        //CKEditor
        $this->load->library('ckeditor');
        $this->load->library('ckfinder');
        $this->ckeditor->basePath = base_url().'assets/ckeditor/';
        $this->ckeditor->config['removePlugins'] ='save, preview, newpage, forms';
        $this->ckeditor->config['height'] = '340px';

        //Add Ckfinder to Ckeditor
        $this->ckfinder->SetupCKEditor($this->ckeditor,'../../../../assets/ckfinder/');

        //CKEdtior
        if($editID=="" || $type == "new") {
            $data['tbl_data'] = $this->session->userdata($this->controller.'_data');
        } else {
            $data['tbl_data'] = $this->SqlModel->getSingleRecord($this->tblName, array($this->pKey=>$editID));
            if(empty($data['tbl_data'])) {
                redirect(base_url().'manage/'.$this->controller,'location');
            }
        }
        $data['tbl_data']['patient_id'] = $editID;
        $data['tbl_data']['patients'] = $this->SqlModel->getRecords('patient_id, patient_name', 'patients', '', '', ['patient_is_deleted'=>'0',
            'patient_clinic_id'=>$this->session->userdata('clinic_id')]);
        $data['tbl_data']['doctors'] = $this->SqlModel->getRecords('id dtr_id, full_name dtr_name', 'admin_users', '', '', [
            'is_deleted'=>'0',
            'clinic_id'=>$this->session->userdata('clinic_id'),
            'user_role_id'=>$this->session->userdata('doctor_role_id')
        ]);
        $data['tbl_data']['procedures'] = $this->SqlModel->getRecords('procedure_id, procedure_name, procedure_price', 'procedures', '', '', ['procedure_is_deleted'=>'0',
            'procedure_clinic_id'=>$this->session->userdata('clinic_id'),]);

        $data['userdata'] = $this->user_data;
        $this->load->view('admin/header',$data);
        $this->load->view('admin/navigation');
        $this->load->view('admin/' . $this->addEditView);
        $this->load->view('admin/footer');
    }

    //For add record form post
    public function addRecord() {
        $this->AppointmentModel->addAppointment($this->controller, $this->tblName, $this->colPrefix);
    }

    //For add record form post
    
    public function editRecord($editID="") {
        if($this->input->post($this->colPrefix.'patient_id')=="" || $editID=="") {
            $this->session->set_flashdata('alert','error');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
            exit();
        }
        $dtr_id = $this->input->post($this->colPrefix.'dtr_id');
        $patient_id = $this->input->post($this->colPrefix.'patient_id');
        $oldInfo = $this->SqlModel->getSingleRecord($this->tblName, ['appointment_id' => $editID]);

        if ($oldInfo['appointment_patient_id'] != $patient_id) {
            $dtr_name = $this->SqlModel->getSingleField('full_name', 'admin_users', ['id'=>$oldInfo['appointment_dtr_id']]);
            $this->PatientLogs->CreatePatientAppointmentCanceledLog($oldInfo['appointment_patient_id'], $dtr_name, date('d-M-Y h:i A', strtotime($oldInfo['appointment_date'] . ' ' . $oldInfo['appointment_start_time'])));
        }

        $data = array(
            $this->colPrefix.'patient_id' => $patient_id,
            $this->colPrefix.'dtr_id' => $dtr_id,
            $this->colPrefix.'status' => $this->input->post($this->colPrefix.'status'),
            $this->colPrefix.'start_time' => date('h:i:s', strtotime($this->input->post($this->colPrefix.'start_time'))),
            $this->colPrefix.'end_time' => date('h:i:s', strtotime($this->input->post($this->colPrefix.'end_time'))),
            $this->colPrefix.'date' => date('Y-m-d', strtotime($this->input->post($this->colPrefix.'date'))),
            $this->colPrefix.'procedure_id' => $this->input->post($this->colPrefix.'procedure_id'),
            $this->colPrefix.'comment' => $this->input->post($this->colPrefix.'comment'),
            $this->colPrefix.'send_confirmation' => $this->input->post($this->colPrefix.'send_confirmation'),
            $this->colPrefix.'procedure_id' => $this->input->post($this->colPrefix.'procedure_id'),
            $this->colPrefix.'updated' => date('Y-m-d H:i:s', strtotime('now')),
            $this->colPrefix.'modified_by' => $this->session->userdata('admin_id'),
        );

        $q = $this->SqlModel->updateRecord($this->tblName, $data, array($this->pKey=>$editID));
        if($q==true) {
            $dtr = $this->SqlModel->getSingleField('full_name', 'admin_users', ['id'=>$dtr_id]);
            if ($oldInfo['appointment_patient_id'] != $patient_id) {
                $this->PatientLogs->CreatePatientAppointmentLog($patient_id, $this->input->post($this->colPrefix.'start_time'), $this->input->post($this->colPrefix.'date'), $dtr);
            }
            if ($oldInfo['appointment_dtr_id'] != $dtr_id && $oldInfo['appointment_patient_id'] == $patient_id) {
                $this->PatientLogs->CreatePatientAppointmentRescheduledLog($patient_id, date('d-M-Y h:i A', strtotime($this->input->post($this->colPrefix.'date'). ' ' . $this->input->post($this->colPrefix.'start_time'))), $dtr);
            }
            $this->session->set_flashdata('alert','editsuccess');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        }
        else{
            $this->session->set_flashdata('alert','error');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        }
    }

    public function markChecked($id='') {
        $data = array(
            'appointment_checked' => '1',
            $this->colPrefix.'updated' => date('Y-m-d H:i:s', strtotime('now')),
            $this->colPrefix.'modified_by' => $this->session->userdata('admin_id')
        );

        $q = $this->SqlModel->updateRecord($this->tblName, $data, array($this->pKey=>$id));
        if($q==true) {
            $this->session->set_flashdata('alert','editsuccess');
            redirect(base_url().'manage/','location');
        } else {
            $this->session->set_flashdata('alert','error');
            redirect(base_url().'manage/','location');
        }
    }

    //For add record form post
    public function rescheduleAppointment() {
        $editID = $this->input->post('appointment_id');
        if($this->input->post($this->colPrefix.'patient_id')=="" || $editID=="") {
            $this->session->set_flashdata('alert','error');
            redirect(base_url().'manage/dashboard','location');
            exit();
        }
        $dtr_id = $this->input->post($this->colPrefix.'dtr_id');
        $patient_id = $this->input->post($this->colPrefix.'patient_id');
        $oldInfo = $this->SqlModel->getSingleRecord($this->tblName, ['appointment_id' => $editID]);

        if ($oldInfo['appointment_patient_id'] != $patient_id) {
            $dtr_name = $this->SqlModel->getSingleField('full_name', 'admin_users', ['id'=>$oldInfo['appointment_dtr_id']]);
            $this->PatientLogs->CreatePatientAppointmentCanceledLog($oldInfo['appointment_patient_id'], $dtr_name, date('d-M-Y h:i A', strtotime($oldInfo['appointment_date'] . ' ' . $oldInfo['appointment_start_time'])));
        }

        $data = array(
            $this->colPrefix.'patient_id' => $patient_id,
            $this->colPrefix.'dtr_id' => $dtr_id,
            $this->colPrefix.'start_time' => date('h:i:s', strtotime($this->input->post($this->colPrefix.'start_time'))),
            $this->colPrefix.'end_time' => date('h:i:s', strtotime($this->input->post($this->colPrefix.'end_time'))),
            $this->colPrefix.'date' => date('Y-m-d', strtotime($this->input->post($this->colPrefix.'date'))),
            $this->colPrefix.'updated' => date('Y-m-d H:i:s', strtotime('now')),
            $this->colPrefix.'modified_by' => $this->session->userdata('admin_id'),
        );

        $q = $this->SqlModel->updateRecord($this->tblName, $data, array($this->pKey=>$editID));
        if($q==true) {
            $dtr = $this->SqlModel->getSingleField('full_name', 'admin_users', ['id'=>$dtr_id]);
            if ($oldInfo['appointment_patient_id'] != $patient_id) {
                $this->PatientLogs->CreatePatientAppointmentLog($patient_id, $this->input->post($this->colPrefix.'start_time'), $this->input->post($this->colPrefix.'date'), $dtr);
            }
            if ($oldInfo['appointment_dtr_id'] != $dtr_id && $oldInfo['appointment_patient_id'] == $patient_id) {
                $this->PatientLogs->CreatePatientAppointmentRescheduledLog($patient_id, date('d-M-Y h:i A', strtotime($this->input->post($this->colPrefix.'date'). ' ' . $this->input->post($this->colPrefix.'start_time'))), $dtr);
            }
            $this->session->set_flashdata('alert','editsuccess');
        }
        else{
            $this->session->set_flashdata('alert','error');
        }
        $data['userdata'] = $this->user_data;
        $this->load->view('admin/header',$data);
        $this->load->view('admin/navigation');
        $this->load->view('admin/dashboard');
        $this->load->view('admin/footer');
    }

    public function delete($deleteID = "") {
        $data = array(
            $this->colPrefix.'is_deleted' => '1',
        );

        $q = $this->SqlModel->updateRecord($this->tblName, $data, array($this->pKey=>$deleteID));
        if ($q == true) {
            $this->session->set_flashdata('alert', 'deletesuccess');
        } else {
            $this->session->set_flashdata('alert', 'deleteerror');
        }
        redirect(base_url('manage/' . $this->controller));
    }

    public function cancelAppointment($appointment_id = "") {
        $data = array(
            $this->colPrefix.'is_canceled' => '1',
        );

        $q = $this->SqlModel->updateRecord($this->tblName, $data, array($this->pKey=>$appointment_id));
        $singleRec = $this->SqlModel->getSingleRecord('appointments', ['appointment_id' => $appointment_id]);
        $data['alert'] = $this->session->flashdata('alert');
        if ($q == true) {
            $dtr_name = $this->SqlModel->getSingleField('full_name', 'admin_users', ['id'=>$singleRec['appointment_dtr_id']]);
            $this->PatientLogs->CreatePatientAppointmentCanceledLog($singleRec['appointment_patient_id'], $dtr_name, date('d-M-Y h:i A', strtotime($singleRec['appointment_date'] . ' ' . $singleRec['appointment_start_time'])));
            $this->session->set_flashdata('alert', 'cancelsuccess');
        } else {
            $this->session->set_flashdata('alert', 'cancelerror');
        }
        $data['userdata'] = $this->user_data;
        $this->load->view('admin/header',$data);
        $this->load->view('admin/navigation');
        $this->load->view('admin/dashboard');
        $this->load->view('admin/footer');
    }

    //For delete selected colors
    public function deleteall() {
        $data = array(
            $this->colPrefix.'is_deleted' => '1',
        );
        $ids = $this->input->post('records');
        if (!empty($ids)) {
            foreach ($ids as $id) {
                $this->SqlModel->updateRecord($this->tblName, $data, array($this->pKey=>$id));
            }
        }
        $this->session->set_flashdata('alert', 'deletesuccess');
        redirect(base_url('manage/' . $this->controller));
    }

    public function changestatus($id="0",$status="Published") {
        if(
            $status=="Published" ||
            $status=="Un-Published"
        ) {
            $this->SqlModel->updateRecord($this->tblName, array($this->tStatus=>$status), array($this->pKey=>$id));
            echo json_encode(array('status'=>'true',
                'id'=>$id,
                'currentStatus'=>$this->SqlModel->getSingleField($this->tStatus,
                    $this->tblName,
                    array($this->pKey=>$id)
                )
            ));
        }
        else {
            echo json_encode(array('status'=>'false'));
        }
    }

    public function getAppointments() {
        ini_set('display_errors', '1');
        $start_date = $this->input->get('start_date');
        $end_date = $this->input->get('end_date');
        echo json_encode($this->SqlModel->getAppointmentsRange($start_date, $end_date));
    }
}