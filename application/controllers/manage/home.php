<?php ob_start(); if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
	public $moduleName = "Home";
	public $controller = "home";
	public $user_data = array();


	public function __construct() {
	    // Call the Model constructor
	   	parent::__construct();
	   	session_start();
	   	//print_r($this->session->userdata); exit;
		$this->user_data = $this->SqlModel->authAdmin($this->session->userdata('admin_auth'),$this->session->userdata('admin_id'));
		if (!isset($this->user_data) || empty($this->user_data)) {
			redirect(base_url('manage/login'));
		}
    }

	public function index() {
		$data['dashBoard'] = 1;
		$data['page_title'] = PROJECT_TITLE." | Dashboard";
		$data['userdata'] = $this->user_data;
        $data['datePicker'] = 1;
		$this->load->view('admin/header',$data);
		$this->load->view('admin/navigation');
		$this->load->view('admin/dashboard');
		$this->load->view('admin/footer');
	}

	public function settings($alert="") {
		if($this->session->userdata('admin_auth')!="allow") {
			redirect(base_url().'manage/login','location');
		}
		$data['page_title'] = PROJECT_TITLE." | Account Settings";
		$data['alert'] = $alert;
		$data['userdata'] = $this->user_data;
		$this->load->view('admin/header',$data);
		$this->load->view('admin/navigation');
		$this->load->view('admin/adminSettings');
		$this->load->view('admin/footer');
	}

	public function savesettings() {
		if(
			$this->input->post('admin_name')=="" ||
			$this->input->post('admin_email')==""
		) {
			redirect(base_url().'manage/home/settings/error', 'location');
			exit();
		}

		if($this->SqlModel->countRecords('admin_users' , array('email'=>$this->input->post('admin_email'),'id !='=>$this->user_data['id']))>0) {
			redirect(base_url().'manage/home/settings/exist', 'location');
			exit();
		}

		$this->SqlModel->updateRecord('admin_users' , array('full_name'=>$this->input->post('admin_name'), 'email'=>$this->input->post('admin_email')) , array('id'=>$this->user_data['id']));

		if($this->input->post('admin_current_pwd')!="") {
			if(md5($this->input->post('admin_current_pwd'))!=$this->user_data['pwd']) {
				redirect(base_url().'manage/home/settings/perror', 'location');
				exit();
			} else {
				$this->SqlModel->updateRecord('admin_users' , array('pwd'=>md5($this->input->post('admin_new_pwd'))) , array('id'=>$this->user_data['id']));
			}
		}

		redirect(base_url().'manage/home/settings/success', 'location');
	}

	public function logout() {
		$this->session->sess_destroy();
		redirect(base_url().'manage','location');
	}

	public function websettings($alert="") {
		$data['wsettingActive'] = 1;
		$data['page_title'] = PROJECT_TITLE." | Website Settings";

		$data['alert'] = $alert;
		$data['userdata'] = $this->user_data;
		$data['web'] = $this->SqlModel->getSingleRecord('site_settings',array('clinic_id'=>$this->session->userdata('clinic_id')));
 
        //CKEditor
        $this->load->library('ckeditor');
        $this->load->library('ckfinder');
        $this->ckeditor->basePath = base_url().'assets/ckeditor/';
        $this->ckeditor->config['removePlugins'] ='save, preview, newpage, forms';
        $this->ckeditor->config['height'] = '340px';

        //Add Ckfinder to Ckeditor
        $this->ckfinder->SetupCKEditor($this->ckeditor,'../../../../assets/ckfinder/');

		$this->load->view('admin/header',$data);
		$this->load->view('admin/navigation');
		$this->load->view('admin/webSettings');
		$this->load->view('admin/footer');
	}

	public function savewebsettings() {
		$clinic_id = $this->session->userdata('clinic_id');
		$data = array(
			'website_title'		=> htmlspecialchars($this->input->post('website_title')),
			'website_url'		=> $this->input->post('website_url'),
			'address'		=> $this->input->post('address'),
			'full_address'		=> $this->input->post('full_address'),
			'tel'		=> $this->input->post('tel'),
			'tag_line'		=> $this->input->post('tag_line'),
			'clinic_id'	=> $clinic_id,
		);

		$config['upload_path'] = './assets/frontend/images/logo/';
		$config['allowed_types'] = 'jpg|png|jpeg|ico|x-icon|icon';
		$config['max_size']	= '102400';
		$config['remove_spaces'] = true;
		$this->load->library('upload', $config);
		$this->load->library('image_lib');

		if(isset($_FILES['uploadfile'])) {
		 	if ($this->upload->do_upload('uploadfile')) {
			    $filename_ephoto = $this->upload->data('uploadfile');
				$data['logo'] = $filename_ephoto['file_name'];
			}
		}

		$check = $this->SqlModel->getSingleRecord('site_settings',array('clinic_id'=>$clinic_id));
		if ($check == null) {
			$q = $this->SqlModel->insertRecord('site_settings' , $data);
 		} else {
 			$q = $this->SqlModel->updateRecord('site_settings' , $data , array('clinic_id'=>$clinic_id));
 		}

 		if($q) {
			redirect(base_url().'manage/home/websettings/success', 'location');
		} else {
			redirect(base_url().'manage/home/websettings/error', 'location');
		}
	}
}