<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * This file is part of the CrudGenerator package.
 *
 * @author Talib Allauddin
 *
 * (c) Talib Allauddin <taliballauddin@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
class Invoices extends CI_Controller {

    public $tblName 	= 'patient_invoices';
    public $pKey 		= 'pi_id';
    public $moduleName 	= "Invoices";
    public $controller 	= "invoices";
    public $per_page 	= '10';
    public $colPrefix	= "pi_";
    public $tStatus     = "pi_status";
	public $addEditView = "addInvoices";

    public function __construct() {
        // Call the Model constructor
        parent::__construct();

        $this->user_data = $this->SqlModel->authAdmin($this->session->userdata('admin_auth'),$this->session->userdata('admin_id'));

        if (!isset($this->user_data) || empty($this->user_data)) {
            redirect(base_url('manage/login'));
        }

        if($this->SqlModel->checkPermissions('invoices', 'read')!==true) {
            redirect(base_url('manage'));
        }
    }

    public function index($sortby="pi_id", $order="DESC", $status="-",$keywords="-", $pg_no="") {
        //PER_PAGE_START
        if($this->input->get('per_page')!="" && is_numeric($this->input->get('per_page')) && (int)$this->input->get('per_page')<=100) {
            $this->session->set_userdata('per_page', $this->input->get('per_page'));
        }

        if($this->session->userdata('per_page')!="") {
            $this->per_page = $this->session->userdata('per_page');
        }

        //PER_PAGE_END
        $data['alert'] = $this->session->flashdata('alert');

        $where =  array();

        $keywords = urldecode($keywords);

        if($status=="Published" || $status=="Un-Published") {
            $where[$this->tStatus] = $status;
        }

        $search 			= ($keywords!="-") ? array('cols'=> $this->colPrefix.'id','value'=>urldecode($keywords)) : array();
        $base_url           = base_url() . 'manage/' . $this->controller . '/index/' . $sortby . "/" . $order . "/" . $status . "/" . $keywords;
        $total_rows 		= $data['total_rows'] =$this->SqlModel->countRecords($this->tblName,$where,$search);
        $per_page 			= $data['per_page'] = $this->per_page;
        $uri_segment 		= 9;

        $data['page_title'] = PROJECT_TITLE." | ".$this->moduleName;
        $data['userdata'] = $this->user_data;
        $data['pagesActive'] = 1;

        //Pagination START
        $pconfig['base_url'] = $base_url;
        $pconfig['total_rows'] = $data['total_rows'] =  $total_rows;
        $pconfig["uri_segment"] = $uri_segment ;
        $pconfig['per_page'] = $data['per_page'] = $this->per_page;
        $pconfig['num_links'] = 1;
        $pconfig['prev_link'] = '<i class="entypo-left-open-mini"></i>';
        $pconfig['next_link'] = '<i class="entypo-right-open-mini"></i>';
        $pconfig['cur_tag_open'] = '<li  class="active"><a href="javascript:void(0)">';
        $pconfig['cur_tag_close'] = '</a></li>';
        $pconfig['full_tag_open'] = '<ul class="pagination pull-right">';
        $pconfig['full_tag_close'] = '</ul>';
        $pconfig['num_tag_open'] = "<li>";
        $pconfig['num_tag_close']= "</li>";
        $pconfig['next_tag_open'] = "<li>";
        $pconfig['next_tag_close']= "</li>";
        $pconfig['prev_tag_open'] = "<li>";
        $pconfig['prev_tag_close']= "</li>";
        $pconfig['last_tag_open'] = "<li>";
        $pconfig['last_tag_close']= "</li>";
        $pconfig['first_tag_open'] = "<li>";
        $pconfig['first_tag_close']= "</li>";
        $offset = ($this->uri->segment($uri_segment )) ? $this->uri->segment($uri_segment ) : 0;
        $this->pagination->initialize($pconfig);

        $data['listing'] = $this->SqlModel->getRecordsWithSingleJoin('patient_invoices.*, patients.patient_name', $this->tblName, 'patients', 'patient_id', 'pi_patient_id', $sortby, $order,  $where, $search, $per_page, $offset,false);

        $data['paginate'] = $this->pagination->create_links();
        //Pagination END
        $data['sortby'] 	= 	$sortby;
        $data['order'] 		= 	($order=="ASC") ? "DESC" : "ASC";
        $data['page_numb'] 	= 	$offset;
        $data['status']		=	$status;
        $data['keywords']	=	$keywords;

        $this->load->view('admin/header',$data);
        $this->load->view('admin/navigation');
        $this->load->view('admin/'.$this->controller);
        $this->load->view('admin/footer');
    }

    //For adding/edting colors
    public function control($type = "",$editID="") {
        if ($type == 'edit' && $editID != "") {
            if($this->SqlModel->checkPermissions($this->controller, 'update')==false) {
                header('location: ./manage/'.$this->controller.'?alert=permerror');
            }
        }
        $data['type'] = (($type == "") ? "Add" : ucfirst($type));
        $alert = $this->session->flashdata('alert');
        $data['datePicker'] = 1;
        $data['pagesActive'] = 1;
        $data['alert'] = $alert;
        $type = ($editID=="") ? "Add" : "Edit";
        $data['page_title'] = PROJECT_TITLE." | ".$type." ".$this->moduleName;
        //CKEditor
        $this->load->library('ckeditor');
        $this->load->library('ckfinder');
        $this->ckeditor->basePath = base_url().'assets/ckeditor/';
        $this->ckeditor->config['removePlugins'] ='save, preview, newpage, forms';
        $this->ckeditor->config['height'] = '340px';

        //Add Ckfinder to Ckeditor
        $this->ckfinder->SetupCKEditor($this->ckeditor,'../../../../assets/ckfinder/');

        //CKEdtior
        if($editID=="") {
            $data['tbl_data'] = $this->session->userdata($this->controller.'_data');
        } else {
            $data['tbl_data'] = $this->SqlModel->getSingleRecord($this->tblName, array($this->pKey=>$editID));
            if(empty($data['tbl_data'])) {
                redirect(base_url().'manage/'.$this->controller,'location');
            }
        }

        $data['userdata'] = $this->user_data;
        $this->load->view('admin/header',$data);
        $this->load->view('admin/navigation');
        $this->load->view('admin/' . $this->addEditView);
        $this->load->view('admin/footer');
    }

    //For add record form post
    public function addRecord() {

        // if($this->SqlModel->checkPermissions('invoices', 'create')!==true) {
        //     $this->session->set_flashdata('alert','permerror');
        //     redirect(base_url().'manage/'.$this->controller.'/index','location');
        // }
        parse_str($_POST['data'], $formdata);

        $name = $formdata['patient_name'];
        $phone = $formdata['patient_phone1'];
        $patient_id = $formdata['pi_patient_id'];

        if ($name != '' && $phone != '') {
            $pd = array(
                'patient_name' => $name,
                'patient_mr_id' => $this->PatientModel->generateNextID(),
                'patient_status' => 'Enabled',
                'patient_phone1' => $phone1,
                'patient_gender' => $formdata['patient_gender'],
                'patient_age' => $formdata['patient_age'],
                'patient_address' => $formdata['patient_address'],
                'patient_added' => date('Y-m-d H:i:s', strtotime('now')),
                'patient_updated' => date('Y-m-d H:i:s', strtotime('now')),
                'patient_created_by' => $this->session->userdata('admin_id'),
                'patient_modified_by' => $this->session->userdata('admin_id'),
                'patient_is_deleted' => '0',
                'patient_clinic_id' => $this->session->userdata('clinic_id'),
            );
            $patient_id = $this->PatientModel->save($pd);
        }

        $data = array(
            $this->colPrefix.'clinic_id' => $this->session->userdata('clinic_id'),
            $this->colPrefix.'patient_id' => $patient_id,
            $this->colPrefix.'payment_method' => addslashes($formdata[$this->colPrefix.'payment_method']),
            $this->colPrefix.'amount' => $formdata['grand_total'],
            $this->colPrefix.'remarks' => $formdata[$this->colPrefix.'remarks'],
            $this->colPrefix.'date' => date('Y-m-d H:i:s', strtotime($formdata[$this->colPrefix.'date'])),
            $this->colPrefix.'discount_rs' => $formdata['p_total_discount_rs'],
            $this->colPrefix.'discount_percent' => $formdata['p_total_discount_percent'],
            $this->colPrefix.'type' => $formdata[$this->colPrefix.'type'],
            $this->colPrefix.'added' => date('Y-m-d H:i:s', strtotime('now')),
            $this->colPrefix.'updated' => date('Y-m-d H:i:s', strtotime('now')),
            $this->colPrefix.'created_by' => $this->session->userdata('admin_id'),
            $this->colPrefix.'updated_by' => $this->session->userdata('admin_id'),
            $this->colPrefix.'is_deleted' => '0',
            $this->colPrefix.'status' => 'Enabled',

        );

        $invoice_id = $this->SqlModel->insertRecord($this->tblName, $data);

        // if ($formdata['make_appointment']) == '1') {
        //     $this->AppointmentModel->addAppointment('appointments', 'appointments', 'appointment_');
        // }
        if ($formdata['total_items'] > 0) {
            $pp_id = [];
            for ($i=1; $i <= $formdata['total_items']; $i++) { 
                if ($formdata['pp_procedure_id-'.$i] != "Select") {
                    $data = array(
                        'pp_clinic_id' => $this->session->userdata('clinic_id'),
                        'pp_patient_id' => $patient_id,
                        'pp_invoice_id' => $invoice_id,
                        'pp_procedure_id' => $formdata['pp_procedure_id-'.$i],
                        'pp_date' => date('Y-m-d H:i:s', strtotime($formdata["pi_date"])),
                        'pp_amount' => $formdata['pp_amount-'.$i],
                        'pp_discount' => $formdata['pp_discount-'.$i],
                        'pp_discount_type' => $formdata['pp_discount_type-'.$i],
                        'pp_added' => date('Y-m-d H:i:s', strtotime('now')),
                        'pp_updated' => date('Y-m-d H:i:s', strtotime('now')),
                        'pp_created_by' => $this->session->userdata('admin_id'),
                        'pp_updated_by' => $this->session->userdata('admin_id'),
                        'pp_is_deleted' => '0',
                        'pp_status' => 'Enabled',
                        'pp_description' => addslashes($formdata['pp_description-'.$i]),
                        'pp_appointment_id' => $formdata['appointment_id']
                    );
                    $pp_id = $this->PatientProceduresModel->save($data);
                }
            }
        }
        if (!empty($formdata['pp_bed_number']) && $formdata['pp_bed_number'] != 'Select') {
            $bpd = array(
                'bp_bed_id' => $formdata['pp_bed_number'],
                'bp_patient_id' => $patient_id,
                'bp_alloted_by' => $formdata['pp_bed_alloted_by'],
                'bp_amount' => $formdata['pp_bed_amount'],
                'bp_discount' => $formdata['pp_bed_discount'],
                'bp_discount_type' => $formdata['pp_bed_discount_type'],
                'bp_status' => 'Enabled',
                'bp_created_by' => $this->session->userdata('admin_id'),
                'bp_updated_by' => $this->session->userdata('admin_id'),
                'bp_added' => date('Y-m-d H:i:s', strtotime('now')),
                'bp_admission_date' => date('Y-m-d H:i:s', strtotime($formdata['bed_from'])),
                'bp_discharge_date' => date('Y-m-d H:i:s', strtotime($formdata['bed_to'])),
                'bp_updated' => date('Y-m-d H:i:s', strtotime('now')),
                'bp_is_deleted' => '0',
                'bp_clinic_id' => $this->session->userdata('clinic_id'),
                'bp_invoice_id' => $invoice_id,
                'bp_payment_status' => 'paid'
            );
            $this->BedPatientModel->save($bpd);
        }
        if (!empty($formdata['pp_room_number']) && $formdata['pp_room_number'] != 'Select') {
            $rpd = array(
                'rp_room_id' => $formdata['pp_room_number'],
                'rp_patient_id' => $patient_id,
                'rp_alloted_by' => $formdata['pp_room_alloted_by'],
                'rp_amount' => $formdata['pp_room_amount'],
                'rp_discount' => $formdata['pp_room_discount'],
                'rp_discount_type' => $formdata['pp_room_discount_type'],
                'rp_status' => 'Enabled',
                'rp_created_by' => $this->session->userdata('admin_id'),
                'rp_updated_by' => $this->session->userdata('admin_id'),
                'rp_added' => date('Y-m-d H:i:s', strtotime('now')),
                'rp_admission_date' => date('Y-m-d H:i:s', strtotime($formdata['room_from'])),
                'rp_discharge_date' => date('Y-m-d H:i:s', strtotime($formdata['room_to'])),
                'rp_updated' => date('Y-m-d H:i:s', strtotime('now')),
                'rp_is_deleted' => '0',
                'rp_clinic_id' => $this->session->userdata('clinic_id'),
                'rp_payment_status' => 'paid',
                'rp_invoice_id' => $invoice_id
            );
            $this->RoomPatientModel->save($rpd);
        }

        $data = [];
        header("Content-Type: application/json");

        $this->session->unset_userdata($this->controller.'_data');
        if($invoice_id!="") {
            $data['alert'] = 'Success';
            $data['responseType'] = 'alert-success';
            $data['invoiceId'] = $invoice_id;
            $data['message'] = 'Invoice created successfully!';
        } else {
            $data['alert'] = 'Error';
            $data['reponseType'] = 'alert-danger';
            $data['message'] = 'Failed creating invoice, please try again!';
        }
        echo json_encode($data);
        exit();
    }

    public function printInvoice(){
        $invoiceId = $this->input->get('invoiceId');
        $patientId = $this->input->get('patientId');
        if ($invoiceId != '' && $invoiceId > 0 && $patientId != "" && $patientId > 0) {
            $data['procedures'] = $this->PatientProceduresModel->getProceduresByInvoiceId($invoiceId);
            $data['bed'] = $this->BedPatientModel->getBedByInvoiceId($invoiceId);
            $data['room'] = $this->RoomPatientModel->getRoomByInvoiceId($invoiceId);
            $data['invoice'] = $this->InvoicesModel->getInvoiceById($invoiceId);
            $data['patient'] = $this->PatientModel->getPatientById($patientId);
        }
        $logo = $this->SqlModel->getSingleField('logo','site_settings',array('clinic_id'=>$this->session->userdata('clinic_id')));

        $web = $this->SqlModel->getSingleRecord('site_settings',array('clinic_id'=>$this->session->userdata('clinic_id')));
        
        define("HOSPITAL_ADDRESS", $web['full_address']);

        $this->load->library('Pdf');
        $pdf = new Pdf('P', 'mm', 'A4', $logo);
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(true);
        $pdf->AddPage();

        $pdf->Rect(0, 0, 2000, 20,'F',[],[255, 255, 255]);
        $pdf->SetTextColor(0,0,0);
        
        $logo = './assets/frontend/images/logo/'.$this->SqlModel->getSingleField('logo','site_settings',array('clinic_id'=>$this->session->userdata('clinic_id')));
        if(file_exists($logo)) {
            $pdf->Image($logo, 8, 3, 23, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
        }

        $pdf->Ln();

        $pdf->SetFont("HelveticaI", '', 14);
        $pdf->Cell(190, 8, $web['website_title'], 0, 1, 'C');

        $pdf->SetFont("Helvetica", '', 8);
        $pdf->Cell(190, 5, $web['address'] . " Tel: " . $web['tel'], 0, 1, 'C');

        $pdf->SetFont("HelveticaB", '', 9);
        $pdf->Cell(190, 5, $web['tag_line'], 0, 1, 'C');
        
        $pdf->writeHTML("<hr>", true, false, false, false, '');

        $pdf->SetFont("HelveticaB", '', 10);
        $pdf->Cell(22, 5, "MR NO.:");
        $pdf->Cell(35, 5, $data['patient']['patient_mr_id']);

        $pdf->Cell(90, 5, $data['patient']['patient_name']);

        $pdf->Cell(43, 5, $data['patient']['patient_gender'].'/'.$data['patient']['patient_age'] . ' Years', 0, 0, 'R');

        $pdf->Ln();

        $pdf->Cell(22, 5, "Date: ");
        $pdf->Cell(35, 5, date("d-M-Y", strtotime($data['invoice']['pi_date'])));
        $pdf->Cell(92, 5, "Payment: " . $data['invoice']['pi_payment_method'], 0, 0, 'L');
        $pdf->Cell(35, 5, "Visit: " . $data['invoice']['pi_type'], 0, 0, 'R');
        // $pdf->Cell(90, 5, "Consultant: -");

        $pdf->Ln();
        $pdf->writeHTML("<br>", 1, 0, 0, 0, '');

        $pdf->SetFont("HelveticaB", '', 9);
        $pdf->Cell(18, 5, "S No.", 1, 0, 'C');
        $pdf->Cell(90, 5, "Description", 1, 0, 'C');
        $pdf->Cell(27, 5, "Amount", 1, 0, 'C');
        $pdf->Cell(27, 5, "Discount", 1, 0, 'C');
        $pdf->Cell(27, 5, "Total", 1, 0, 'C');
        $pdf->Ln();
        $srno = 1;
        $full_total = 0;
        $pdf->SetFont("Helvetica", '', 9);
        //print_r($data['procedures']);exit;
        foreach ($data['procedures'] as $procedure) {
            // print_r($data['procedures']);exit;
            $total = 0;
            if ($procedure['pp_discount'] != '0') {
                if ($procedure['pp_discount_type'] == 'Rs.') {
                    $discount = $procedure['pp_discount'];
                } else if ($procedure['pp_discount_type'] == '%') {
                    $discount = (($procedure['pp_amount'] * $procedure['pp_discount'])/100);
                }
                $total = $procedure['pp_amount'] - $discount;
            } else {
                $discount = '-';
                $total = $procedure['pp_amount'];
            }
            $pdf->Cell(18, 5, $srno.".", 1, 0, 'R');
            $pdf->Cell(90, 5, $procedure['procedure_name'], 1, 0, 'L');
            $pdf->Cell(27, 5, ' Rs.' . $procedure['pp_amount'], 1, 0, 'C');
            $pdf->Cell(27, 5, ($discount == '-' ? '-' : 'Rs. '. $discount), 1, 0, 'C');
            $pdf->Cell(27, 5, ' Rs.' . $total, 1, 0, 'C');
            $srno++;
            $pdf->Ln();
            $full_total += $total;
        }

        //print_r($data['bed']);exit;
        if (count($data['bed']) > 0) {
            $total = 0;
            if ($data['bed'][0]['bp_discount'] != '0') {
                if ($data['bed'][0]['bp_discount_type'] == 'Rs.') {
                    $discount = $data['bed'][0]['bp_discount'];
                } else if ($data['bed'][0]['bp_discount_type'] == '%') {
                    $discount = (($data['bed'][0]['bp_amount'] * $data['bed'][0]['bp_discount'])/100);
                }
                $total = $data['bed'][0]['bp_amount'] - $discount;
            } else {
                $discount = '-';
                $total = $data['bed'][0]['bp_amount'];
            }
            $pdf->Cell(18, 5, $srno.".", 1, 0, 'R');
            $pdf->Cell(90, 5, "Bed Charges", 1, 0, 'L');
            $pdf->Cell(27, 5, ' Rs.' . $data['bed'][0]['bp_amount'], 1, 0, 'C');
            $pdf->Cell(27, 5, ($discount == '-' ? '-' : 'Rs. '. $discount), 1, 0, 'C');
            $pdf->Cell(27, 5, ' Rs.' . $total, 1, 0, 'C');
            $srno++;
            $pdf->Ln();
            $full_total += $total;
        }

        //print_r($data['room']);exit;
        if (count($data['room']) > 0) {
            $total = 0;
            if ($data['room'][0]['rp_discount'] != '0') {
                if ($data['room'][0]['rp_discount_type'] == 'Rs.') {
                    $discount = $data['room'][0]['rp_discount'];
                } else if ($data['room'][0]['rp_discount_type'] == '%') {
                    $discount = (($data['room'][0]['rp_amount'] * $data['room'][0]['rp_discount'])/100);
                }
                $total = $data['room'][0]['rp_amount'] - $discount;
            } else {
                $discount = '-';
                $total = $data['room'][0]['rp_amount'];
            }
            $pdf->Cell(18, 5, $srno.".", 1, 0, 'R');
            $pdf->Cell(90, 5, "Room Charges", 1, 0, 'L');
            $pdf->Cell(27, 5, ' Rs.' . $data['room'][0]['rp_amount'], 1, 0, 'C');
            $pdf->Cell(27, 5, ($discount == '-' ? '-' : 'Rs. '. $discount), 1, 0, 'C');
            $pdf->Cell(27, 5, ' Rs.' . $total, 1, 0, 'C');
            $srno++;
            $pdf->Ln();
            $full_total += $total;
        }


        /* Extra Lines */
        $pdf->Cell(18, 5, "", 1, 0, 'C');
        $pdf->Cell(90, 5, "", 1, 0, 'C');
        $pdf->Cell(27, 5, "", 1, 0, 'C');
        $pdf->Cell(27, 5, "", 1, 0, 'C');
        $pdf->Cell(27, 5, "", 1, 0, 'C');
        $pdf->Ln();

        $pdf->Cell(18, 5, "", 1, 0, 'C');
        $pdf->Cell(90, 5, "", 1, 0, 'C');
        $pdf->Cell(27, 5, "", 1, 0, 'C');
        $pdf->Cell(27, 5, "", 1, 0, 'C');
        $pdf->Cell(27, 5, "", 1, 0, 'C');
        $pdf->Ln();

        $pdf->Cell(18, 5, "", 1, 0, 'C');
        $pdf->Cell(90, 5, "", 1, 0, 'C');
        $pdf->Cell(27, 5, "", 1, 0, 'C');
        $pdf->Cell(27, 5, "", 1, 0, 'C');
        $pdf->Cell(27, 5, "", 1, 0, 'C');
        $pdf->Ln();

        $pdf->Cell(18, 5, "", 1, 0, 'C');
        $pdf->Cell(90, 5, "", 1, 0, 'C');
        $pdf->Cell(27, 5, "", 1, 0, 'C');
        $pdf->Cell(27, 5, "", 1, 0, 'C');
        $pdf->Cell(27, 5, "", 1, 0, 'C');
        $pdf->Ln();

        $pdf->Cell(18, 5, "", 1, 0, 'C');
        $pdf->Cell(90, 5, "", 1, 0, 'C');
        $pdf->Cell(27, 5, "", 1, 0, 'C');
        $pdf->Cell(27, 5, "", 1, 0, 'C');
        $pdf->Cell(27, 5, "", 1, 0, 'C');
        $pdf->Ln();

        $pdf->SetFont("HelveticaB", '', 9);
        $pdf->Cell(18, 5, "", 1, 0, 'C');
        $pdf->Cell(90, 5, "", 1, 0, 'C');
        $pdf->Cell(27, 5, "", 1, 0, 'C');
        $pdf->Cell(27, 5, "Grand Total", 1, 0, 'L');
        $pdf->Cell(27, 5, "Rs. ". $full_total, 1, 0, 'C');
        $pdf->Ln();

        if ($data['invoice']['pi_discount_rs'] != '0') {
            $total_discount = $data['invoice']['pi_discount_rs'];
        } else if ($data['invoice']['pi_discount_percent'] != '0') {
            $total_discount = (($data['invoice']['pi_discount_percent'] * $full_total)/100);
        } else {
            $total_discount = '-';
        }

        $pdf->Cell(18, 5, "", 1, 0, 'C');
        $pdf->Cell(90, 5, "", 1, 0, 'C');
        $pdf->Cell(27, 5, "", 1, 0, 'C');
        $pdf->Cell(27, 5, "Discount", 1, 0, 'L');
        $pdf->Cell(27, 5, ($total_discount != '-' ? 'Rs. '. $total_discount : '-'), 1, 0, 'C');
        $pdf->Ln();

        $pdf->Cell(18, 5, "", 1, 0, 'C');
        $pdf->Cell(90, 5, "", 1, 0, 'C');
        $pdf->Cell(27, 5, "", 1, 0, 'C');
        $pdf->Cell(27, 5, "Total", 1, 0, 'L');
        $pdf->Cell(27, 5, 'Rs. ' . ($full_total - $total_discount), 1, 0, 'C');
        $pdf->Ln();

        $pdf->SetFont("HelveticaB", '', 9);
        $pdf->Cell(20, 10, "Remarks: ");
        $pdf->SetFont("Helvetica", '', 9);
        $pdf->Cell(130, 10, $data['invoice']['pi_remarks']);

        $pdf->Output();
    }

    //For add record form post
    public function editRecord($editID="") {
        if($this->SqlModel->checkPermissions('invoices', 'update')!==true) {
            $this->session->set_flashdata('alert','permerror');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        }
        if($this->input->post($this->colPrefix.'name')=="" || $editID=="") {
            $this->session->set_flashdata('alert','error');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
            exit();
        }
        $data = array(
			$this->colPrefix.'patient_id' => $this->input->post($this->colPrefix.'patient_id'),
			$this->colPrefix.'pai_id' => $this->input->post($this->colPrefix.'pai_id'),
			$this->colPrefix.'payment_method' => addslashes($this->input->post($this->colPrefix.'payment_method')),
			$this->colPrefix.'amount' => $this->input->post($this->colPrefix.'amount'),
            $this->colPrefix.'updated' => date('Y-m-d H:i:s', strtotime('now')),
            $this->colPrefix.'updated_by' => $this->session->userdata('admin_id'),
			$this->colPrefix.'status' => 'Enabled',
        );

        $q = $this->SqlModel->updateRecord($this->tblName, $data, array($this->pKey=>$editID));
        if($q==true) {
            $this->session->set_flashdata('alert','editsuccess');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        } else {
            $this->session->set_flashdata('alert','error');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        }
    }

    public function delete($deleteID = "") {
        if($this->SqlModel->checkPermissions('invoices', 'delete')!==true) {
            $this->session->set_flashdata('alert','permerror');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        }
        $q = $this->SqlModel->deleteRecord($this->tblName, array($this->pKey => $deleteID));
        if ($q == true) {
            $this->session->set_flashdata('alert', 'deletesuccess');
        } else {
            $this->session->set_flashdata('alert', 'deleteerror');
        }
        redirect(base_url('manage/' . $this->controller));
    }

    //For delete selected colors
    public function deleteall() {
        if($this->SqlModel->checkPermissions('invoices', 'delete')!==true) {
            $this->session->set_flashdata('alert','permerror');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        }
        $ids = $this->input->post('records');
        if (!empty($ids)) {
            foreach ($ids as $id) {
                $this->SqlModel->deleteRecord($this->tblName, array($this->pKey => $id));
            }
        }
        $this->session->set_flashdata('alert', 'deletesuccess');
        redirect(base_url('manage/' . $this->controller));
    }

    public function changestatus($id="0",$status="Published") {
        if(
            $status=="Published" ||
            $status=="Un-Published"
        ) {
            $this->SqlModel->updateRecord($this->tblName, array($this->tStatus=>$status), array($this->pKey=>$id));
            echo json_encode(array('status'=>'true',
                'id'=>$id,
                'currentStatus'=>$this->SqlModel->getSingleField($this->tStatus,
                    $this->tblName,
                    array($this->pKey=>$id)
                )
            ));
        } else {
            echo json_encode(array('status'=>'false'));
        }
    }
}