<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * This file is part of the CrudGenerator package.
 *
 * @author subhan shah
 *
 * (c) subhan shah <subhan.shah@bitcloudglobal.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
class Patients extends CI_Controller {

    public $tblName 	= 'patients';
    public $pKey 		= 'patient_id';
    public $moduleName 	= "Patients";
    public $controller 	= "patients";
    public $per_page 	= '10';
    public $tStatus 	= "page_status";
    public $colPrefix	= "patient_";
	public $addEditView = "addPatients";

    public function __construct() {
        // Call the Model constructor
        parent::__construct();

        $this->user_data = $this->SqlModel->authAdmin($this->session->userdata('admin_auth'),$this->session->userdata('admin_id'));

        if (!isset($this->user_data) || empty($this->user_data)) {
            redirect(base_url('manage/login'));
        }

        if($this->SqlModel->checkPermissions('patients', 'read')!==true) {
            redirect(base_url('manage'));
        }
    }

    public function index($sortby="patient_id", $order="DESC", $status="-",$keywords="-", $pg_no="") {
        //PER_PAGE_START
        if($this->input->get('per_page')!="" && is_numeric($this->input->get('per_page')) && (int)$this->input->get('per_page')<=100) {
            $this->session->set_userdata('per_page', $this->input->get('per_page'));
        }

        if($this->session->userdata('per_page')!="") {
            $this->per_page = $this->session->userdata('per_page');
        }

        //PER_PAGE_END
        $data['alert'] = $this->session->flashdata('alert');

        $where =  array();

        $keywords = urldecode($keywords);

        if($status=="Published" || $status=="Un-Published") {
            $where[$this->tStatus] = $status;
        }
        $where[$this->colPrefix.'is_deleted'] = '0';
        $where[$this->colPrefix.'clinic_id'] = $this->session->userdata('clinic_id');

        $search 			= ($keywords!="-") ? array('cols'=>$this->colPrefix.'name','value'=>urldecode($keywords)) : array();
        $base_url           = base_url() . 'manage/' . $this->controller . '/index/' . $sortby . "/" . $order . "/" . $status . "/" . $keywords;
        $total_rows 		= $data['total_rows'] =$this->SqlModel->countRecords($this->tblName,$where,$search);
        $per_page 			= $data['per_page'] = $this->per_page;
        $uri_segment 		= 8;

        $data['page_title'] = PROJECT_TITLE." | ".$this->moduleName;
        $data['userdata'] = $this->user_data;
        $data['pagesActive'] = 1;

        //Pagination START
        $pconfig['base_url'] = $base_url;
        $pconfig['total_rows'] = $data['total_rows'] =  $total_rows;
        $pconfig["uri_segment"] = $uri_segment ;
        $pconfig['per_page'] = $data['per_page'] = $this->per_page;
        $pconfig['num_links'] = 1;
        $pconfig['prev_link'] = '<i class="entypo-left-open-mini"></i>';
        $pconfig['next_link'] = '<i class="entypo-right-open-mini"></i>';
        $pconfig['cur_tag_open'] = '<li  class="active"><a href="javascript:void(0)">';
        $pconfig['cur_tag_close'] = '</a></li>';
        $pconfig['full_tag_open'] = '<ul class="pagination pull-right">';
        $pconfig['full_tag_close'] = '</ul>';
        $pconfig['num_tag_open'] = "<li>";
        $pconfig['num_tag_close']= "</li>";
        $pconfig['next_tag_open'] = "<li>";
        $pconfig['next_tag_close']= "</li>";
        $pconfig['prev_tag_open'] = "<li>";
        $pconfig['prev_tag_close']= "</li>";
        $pconfig['last_tag_open'] = "<li>";
        $pconfig['last_tag_close']= "</li>";
        $pconfig['first_tag_open'] = "<li>";
        $pconfig['first_tag_close']= "</li>";
        $offset = ($this->uri->segment($uri_segment )) ? $this->uri->segment($uri_segment ) : 0;
        $this->pagination->initialize($pconfig);

        $data['listing'] = $this->SqlModel->getRecords('*', $this->tblName, $sortby, $order,  $where, $search, $per_page, $offset,false);

        $data['paginate'] = $this->pagination->create_links();
        //Pagination END
        $data['sortby'] 	= 	$sortby;
        $data['order'] 		= 	($order=="ASC") ? "DESC" : "ASC";
        $data['page_numb'] 	= 	$offset;
        $data['status']		=	$status;
        $data['keywords']	=	$keywords;

        $this->load->view('admin/header',$data);
        $this->load->view('admin/navigation');
        $this->load->view('admin/'.$this->controller);
        $this->load->view('admin/footer');
    }

    public function profile($editID='') {

        if($this->SqlModel->checkPermissions('patients', 'profile')!==true) {
            redirect(base_url('manage').'/patients');
        }
        $data = [];
        $data['alert'] = $this->session->flashdata('alert');
        $data['alertmsg'] = $this->session->flashdata('alertmsg');
        $data['patient_info'] = $this->SqlModel->getSingleRecord($this->tblName, ['patient_id' => $editID, 'patient_clinic_id' => $this->session->userdata('clinic_id')]);
        $data['userdata'] = $this->user_data;
        $this->load->view('admin/header',$data);
        $this->load->view('admin/navigation');
        $this->load->view('admin/profile');
        $this->load->view('admin/footer');
    }

    //For adding/edting colors
    public function control($type = "",$editID="") {
        if ($type == 'edit' && $editID != "") {
            if($this->SqlModel->checkPermissions($this->controller, 'update')==false) {
                header('location: ./manage/'.$this->controller.'?alert=permerror');
            }
        }
        $data['type'] = (($type == "") ? "Add" : ucfirst($type));
        $alert = $this->session->flashdata('alert');
        $data['datePicker'] = 1;
        $data['pagesActive'] = 1;
        $data['alert'] = $alert;
        $data['nextID'] = $this->PatientModel->generateNextID();
        $type = ($editID=="") ? "Add" : "Edit";
        $data['page_title'] = PROJECT_TITLE." | ".$type." ".$this->moduleName;
        //CKEditor
        $this->load->library('ckeditor');
        $this->load->library('ckfinder');
        $this->ckeditor->basePath = base_url().'assets/ckeditor/';
        $this->ckeditor->config['removePlugins'] ='save, preview, newpage, forms';
        $this->ckeditor->config['height'] = '340px';

        //Add Ckfinder to Ckeditor
        $this->ckfinder->SetupCKEditor($this->ckeditor,'../../../../assets/ckfinder/');

        //CKEdtior
        if($editID=="") {
            $data['tbl_data'] = $this->session->userdata($this->controller.'_data');
        }
        else {
            $data['tbl_data'] = $this->SqlModel->getSingleRecord($this->tblName, array($this->pKey=>$editID));
            if(empty($data['tbl_data'])) {
                redirect(base_url().'manage/'.$this->controller,'location');
            }
        }

        $data['userdata'] = $this->user_data;
        $this->load->view('admin/header',$data);
        $this->load->view('admin/navigation');
        $this->load->view('admin/' . $this->addEditView);
        $this->load->view('admin/footer');
    }

    //For add record form post
    public function addRecord() {
        $this->PatientModel->addPatient();
    }

    //For add record form post
    public function editRecord($editID="") {
        if($this->SqlModel->checkPermissions('patients', 'update')!==true) {
            $this->session->set_flashdata('alert','permerror');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        }
        if($this->input->post($this->colPrefix.'name')=="" || $editID=="") {
            $this->session->set_flashdata('alert','error');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
            exit();
        }
        $data = array(
            $this->colPrefix.'name' => $this->input->post($this->colPrefix.'name'),
            $this->colPrefix.'email' => $this->input->post($this->colPrefix.'email'),
            $this->colPrefix.'status' => $this->input->post($this->colPrefix.'status'),
            $this->colPrefix.'phone1' => $this->input->post($this->colPrefix.'phone1'),
            $this->colPrefix.'phone2' => $this->input->post($this->colPrefix.'phone2'),
            $this->colPrefix.'phone3' => $this->input->post($this->colPrefix.'phone3'),
            $this->colPrefix.'gender' => $this->input->post($this->colPrefix.'gender'),
            $this->colPrefix.'industry_id' => $this->input->post($this->colPrefix.'industry'),
            $this->colPrefix.'profession_id' => $this->input->post($this->colPrefix.'profession'),
            $this->colPrefix.'subindustry_id' => $this->input->post($this->colPrefix.'subindustry'),
            $this->colPrefix.'jobfunction_id' => $this->input->post($this->colPrefix.'jobfunction'),
            $this->colPrefix.'job_level_id' => $this->input->post($this->colPrefix.'job_level_id'),
            $this->colPrefix.'birth_date' => date('Y-m-d H:i:s', strtotime($this->input->post($this->colPrefix.'birth_date'))),
            $this->colPrefix.'age' => $this->input->post($this->colPrefix.'age'),
            $this->colPrefix.'address' => $this->input->post($this->colPrefix.'address'),
            $this->colPrefix.'refered_by' => $this->input->post($this->colPrefix.'refered_by'),
            $this->colPrefix.'updated' => date('Y-m-d H:i:s', strtotime('now')),
            $this->colPrefix.'modified_by' => $this->session->userdata('admin_id'),
        );

        $config['upload_path'] = './assets/frontend/images/patients/';
        $config['allowed_types'] = 'jpg|png|jpeg|ico|x-icon|icon';
        $config['max_size'] = '102400';
        $config['remove_spaces'] = true;
        $this->load->library('upload', $config);
        $this->load->library('image_lib');

        if(isset($_FILES['patient_image'])) {
            if ($this->upload->do_upload('patient_image')) {
                $filename_ephoto = $this->upload->data('patient_image');
                $data['patient_image'] = $filename_ephoto['file_name'];
            }
        }

        $q = $this->SqlModel->updateRecord($this->tblName, $data, array($this->pKey=>$editID));
        if($q==true) {
            $this->PatientLogs->CreatePatientUpdatedLog($editID);
            $this->session->set_flashdata('alert','editsuccess');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        }
        else{
            $this->session->set_flashdata('alert','error');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        }
    }

    public function delete($deleteID = "") {
        if($this->SqlModel->checkPermissions('patients', 'delete')!==true) {
            $this->session->set_flashdata('alert','permerror');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        }
        $data = array(
            $this->colPrefix.'is_deleted' => '1',
        );

        $q = $this->SqlModel->updateRecord($this->tblName, $data, array($this->pKey=>$deleteID));
        if ($q == true) {
            $this->PatientLogs->CreatePatientDeletedLog($deleteID);
            $this->session->set_flashdata('alert', 'deletesuccess');
        } else {
            $this->session->set_flashdata('alert', 'deleteerror');
        }
        redirect(base_url('manage/' . $this->controller));
    }

    //For delete selected colors
    public function deleteall() {
        if($this->SqlModel->checkPermissions('patients', 'delete')!==true) {
            $this->session->set_flashdata('alert','permerror');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        }
        $data = array(
            $this->colPrefix.'is_deleted' => '1',
        );
        $ids = $this->input->post('records');
        if (!empty($ids)) {
            foreach ($ids as $id) {
                $this->SqlModel->updateRecord($this->tblName, $data, array($this->pKey=>$id));
                $this->PatientLogs->CreatePatientDeletedLog($id);
            }
        }
        $this->session->set_flashdata('alert', 'deletesuccess');
        redirect(base_url('manage/' . $this->controller));
    }

    public function changestatus($id="0",$status="Published") {
        if(
            $status=="Published" ||
            $status=="Un-Published"
        ) {
            $this->SqlModel->updateRecord($this->tblName, array($this->tStatus=>$status), array($this->pKey=>$id));
            echo json_encode(array('status'=>'true',
                'id'=>$id,
                'currentStatus'=>$this->SqlModel->getSingleField($this->tStatus,
                    $this->tblName,
                    array($this->pKey=>$id)
                )
            ));
        }
        else {
            echo json_encode(array('status'=>'false'));
        }
    }

    

    public function qadd() {
        if($this->SqlModel->checkPermissions('patients', 'create')!==true) {
            $this->session->set_flashdata('alert','permerror');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        }
        if($this->input->post($this->colPrefix.'name')=="") {
            $this->session->set_flashdata('alert','error');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
            exit();
        }

        $mr_id = $this->PatientModel->generateNextID();

        $data = array(
            $this->colPrefix.'name' => $this->input->post($this->colPrefix.'name'),
            $this->colPrefix.'mr_id' => $mr_id,
            $this->colPrefix.'status' => 'Enabled',
            $this->colPrefix.'email' => $this->input->post($this->colPrefix.'email'),
            $this->colPrefix.'phone1' => $this->input->post($this->colPrefix.'phone1'),
            $this->colPrefix.'gender' => $this->input->post($this->colPrefix.'gender'),
            $this->colPrefix.'age' => $this->input->post($this->colPrefix.'age'),
            $this->colPrefix.'address' => $this->input->post($this->colPrefix.'address'),
            $this->colPrefix.'added' => date('Y-m-d H:i:s', strtotime('now')),
            $this->colPrefix.'updated' => date('Y-m-d H:i:s', strtotime('now')),
            $this->colPrefix.'created_by' => $this->session->userdata('admin_id'),
            $this->colPrefix.'modified_by' => $this->session->userdata('admin_id'),
            $this->colPrefix.'is_deleted' => '0',
            $this->colPrefix.'clinic_id' => $this->session->userdata('clinic_id'),
        );

        $q = $this->SqlModel->insertRecord($this->tblName, $data);
        $this->session->unset_userdata($this->controller.'_data');

        $data = [];
        header("Content-Type: application/json");
        if($q!="") {
            $this->PatientLogs->CreatePatientAddedLog($q);
            $data['alert'] = 'Success';
            $data['responseType'] = 'alert-success';
            $data['message'] = 'Patient created with MR ID = '. $mr_id;
        }
        else {
            $data['alert'] = 'Error';
            $data['reponseType'] = 'alert-danger';
            $data['message'] = 'Unable to create patient, please contact support 0343-0505-458';
        }
        echo json_encode($data);
        exit();
    }

    public function get_patient_by_mr_id() {
        $mr_id = $this->input->post('mr_id');
        $data = [];
        header("Content-Type: application/json");
        if ($mr_id == '') {
            $data['alert'] = 'Error';
            $data['responseType'] = 'alert-danger';
            $data['message'] = 'Please enter mater record id';
        } else {
            $patient = $this->SqlModel->getSingleRecord($this->tblName, ['patient_mr_id' => $mr_id, 'patient_clinic_id' => $this->session->userdata('clinic_id'), 'patient_is_deleted' => '0']);
            

            if (count($patient) == 0) {
                $data['alert'] = 'Error';
                $data['responseType'] = 'alert-danger';
                $data['message'] = 'Patient not found for given master record number!';
            } else {
                $data['patient_id'] = $patient['patient_id'];
                $data['patient_name'] = $patient['patient_name'];
                $data['patient_mr_id'] = $patient['patient_mr_id'];
            }
        }

        echo json_encode($data);
        exit();
    }

    public function addPatientFiles($patient_id) {
        $alert = $this->session->flashdata('alert');
        $data['alert'] = $alert;
        if($this->SqlModel->checkPermissions('patientfiles', 'create')!==true) {
            $this->session->set_flashdata('alert','permerror');
            redirect(base_url().'manage/'.$this->controller.'/profile/'.$patient_id,'location');
        }
        if($patient_id=="") {
            $this->session->set_flashdata('alert','error');
            redirect(base_url().'manage/'.$this->controller.'/profile/'.$patient_id,'location');
            exit();
        }
        $done = true;

        // Add Purchase Details
        $total_items = $this->input->post('total_items');
        //echo '<pre>'.$total_items.'<br/>';print_r($_FILES);exit;
        for ($i=1; $i<=$total_items; $i++) {
            
            $config['upload_path'] = './assets/frontend/images/patientfiles/';
            $config['allowed_types'] = 'jpg|png|jpeg|ico|x-icon|icon|pdf|bmp|jpeg';
            $config['max_size'] = '102400';
            $config['remove_spaces'] = true;
            $config['file_name'] = $patient_id . '_' . time() . '_' . $_FILES['pf_file-'.$i]['name'];
            $this->load->library('upload', $config);
            $this->load->library('image_lib');

            $data = array(
                'pf_description' => $this->input->post('pf_description-'.$i),
                'pf_reporttype_id' => $this->input->post('pf_reporttype-'.$i),
                'pf_clinic_id' => $this->session->userdata('clinic_id'),
                'pf_patient_id' => $patient_id,
                'pf_added' => date('Y-m-d H:i:s', strtotime('now')),
                'pf_updated' => date('Y-m-d H:i:s', strtotime('now')),
                'pf_created_by' => $this->session->userdata('admin_id'),
                'pf_updated_by' => $this->session->userdata('admin_id'),
                'pf_is_deleted' => '0'
            );
            if(isset($_FILES['pf_file-'.$i])) {
                if (!$this->upload->do_upload('pf_file-'.$i)) {
                    $error = array('error' => $this->upload->display_errors());
                } else {

                    $filename_ephoto = $this->upload->data('pf_file-'.$i);
                    $data['pf_file'] = $filename_ephoto['file_name'];
                }
            }
            $q = $this->SqlModel->insertRecord('patient_files', $data);
            if ($q != '') {
                $this->PatientLogs->CreatePatientFileAddedLog($patient_id, $_FILES['pf_file-'.$i]['name']);
            } else {
                $done = false;
            }
        }
        if ($done == true) {
            $this->session->set_flashdata('alert','filesuccess');
        } else {
            $this->session->set_flashdata('alert','fileerror');
            $this->session->set_flashdata('alertmsg',$error['error']);
        }
        redirect(base_url('manage/patients/profile/' . $patient_id));
    }

    public function getPatientTimelineItems() {
        $patient_id = $this->input->post('patient_id');
        $activity_id = $this->input->post('item_id');
        
        $data = [];
        header("Content-Type: application/json");
        if ($patient_id == '' && $activity_id == '') {
            $data['alert'] = 'Error';
            $data['responseType'] = 'alert-danger';
            $data['message'] = 'Something went wrong, Please try again!';
        } else {
            $timeline_items = $this->SqlModel->getPatientTimelineItems($activity_id, $patient_id);
            $timeline_items_count = $this->SqlModel->getPatientTimelineItemsCount($patient_id);
            $data['timeline_items'] = $timeline_items;
            
            $remaining_items_count = $this->SqlModel->getPatientTimelineItemsRemaining($data['timeline_items'][count($data['timeline_items'])-1]['pa_added'], $patient_id);
            //print_r($remaining_items_count);exit;
            $data['timeline_items_count'] = $timeline_items_count[0]['item_count'];
            $data['remaining_item_count'] = $remaining_items_count[0]['remaining_item_count'];

        }

        echo json_encode($data);
        exit();
    }
}