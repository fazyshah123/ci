<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * This file is part of the CrudGenerator package.
 *
 * @author subhan shah
 *
 * (c) subhan shah <subhan.shah@bitcloudglobal.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
class Labtracking extends CI_Controller {

    public $tblName 	= 'labtracking';
    public $pKey 		= 'lt_id';
    public $moduleName 	= "Lab Tracking";
    public $controller 	= "labtracking";
    public $per_page 	= '10';
    public $tStatus 	= "page_status";
    public $colPrefix	= "lt_";
	public $addEditView = "addLabtracking";

    public function __construct() {
        // Call the Model constructor
        parent::__construct();

        $this->user_data = $this->SqlModel->authAdmin($this->session->userdata('admin_auth'),$this->session->userdata('admin_id'));

        if (!isset($this->user_data) || empty($this->user_data)) {
            redirect(base_url('manage/login'));
        }

        if($this->SqlModel->checkPermissions('labtracking', 'read')!==true) {
            redirect(base_url('manage'));
        }
    }

    public function index($sortby="lt_id", $order="ASC", $status="-",$keywords="-", $pg_no="") {
        //PER_PAGE_START
        if($this->input->get('per_page')!="" && is_numeric($this->input->get('per_page')) && (int)$this->input->get('per_page')<=100) {
            $this->session->set_userdata('per_page', $this->input->get('per_page'));
        }

        if($this->session->userdata('per_page')!="") {
            $this->per_page = $this->session->userdata('per_page');
        }

        //PER_PAGE_END
        $data['alert'] = $this->session->flashdata('alert');

        $where =  array();

        $keywords = urldecode($keywords);

        if($status=="Published" || $status=="Un-Published") {
            $where[$this->tStatus] = $status;
        }
        $where[$this->colPrefix.'is_deleted'] = '0';
        $where[$this->colPrefix.'clinic_id'] = $this->session->userdata('clinic_id');

        $search 			= ($keywords!="-") ? array('cols'=>$this->colPrefix.'lab_id','value'=>urldecode($keywords)) : array();
        $base_url           = base_url() . 'manage/' . $this->controller . '/index/' . $sortby . "/" . $order . "/" . $status . "/" . $keywords;
        $total_rows 		= $data['total_rows'] =$this->SqlModel->countRecords($this->tblName,$where,$search);
        $per_page 			= $data['per_page'] = $this->per_page;
        $uri_segment 		= 8;

        $data['page_title'] = PROJECT_TITLE." | ".$this->moduleName;
        $data['userdata'] = $this->user_data;
        $data['pagesActive'] = 1;

        //Pagination START
        $pconfig['base_url'] = $base_url;
        $pconfig['total_rows'] = $data['total_rows'] =  $total_rows;
        $pconfig["uri_segment"] = $uri_segment ;
        $pconfig['per_page'] = $data['per_page'] = $this->per_page;
        $pconfig['num_links'] = 1;
        $pconfig['prev_link'] = '<i class="entypo-left-open-mini"></i>';
        $pconfig['next_link'] = '<i class="entypo-right-open-mini"></i>';
        $pconfig['cur_tag_open'] = '<li  class="active"><a href="javascript:void(0)">';
        $pconfig['cur_tag_close'] = '</a></li>';
        $pconfig['full_tag_open'] = '<ul class="pagination pull-right">';
        $pconfig['full_tag_close'] = '</ul>';
        $pconfig['num_tag_open'] = "<li>";
        $pconfig['num_tag_close']= "</li>";
        $pconfig['next_tag_open'] = "<li>";
        $pconfig['next_tag_close']= "</li>";
        $pconfig['prev_tag_open'] = "<li>";
        $pconfig['prev_tag_close']= "</li>";
        $pconfig['last_tag_open'] = "<li>";
        $pconfig['last_tag_close']= "</li>";
        $pconfig['first_tag_open'] = "<li>";
        $pconfig['first_tag_close']= "</li>";
        $offset = ($this->uri->segment($uri_segment )) ? $this->uri->segment($uri_segment ) : 0;
        $this->pagination->initialize($pconfig);

        $data['listing'] = $this->SqlModel->getLabtrackingRecords('labtracking.*, patients.patient_name, lab.lab_name, work.w_name, work_type.wt_name', $this->tblName, 'patients', 'patient_id', 'lt_patient_id', 
            'work', 'w_id', 'lt_work_id', 
            'work_type', 'wt_id', 'lt_work_type_id', 
            'lab', 'lab_id', 'lt_lab_id', 
            $sortby, $order,  $where, $search, $per_page, $offset,false);

        $data['paginate'] = $this->pagination->create_links();
        //Pagination END
        $data['sortby'] 	= 	$sortby;
        $data['order'] 		= 	($order=="ASC") ? "DESC" : "ASC";
        $data['page_numb'] 	= 	$offset;
        $data['status']		=	$status;
        $data['keywords']	=	$keywords;

        $this->load->view('admin/header',$data);
        $this->load->view('admin/navigation');
        $this->load->view('admin/'.$this->controller);
        $this->load->view('admin/footer');
    }

    //For adding/edting colors
    public function control($type = "",$editID="") {
        if ($type == 'edit' && $editID != "") {
            if($this->SqlModel->checkPermissions($this->controller, 'update')==false) {
                header('location: ./manage/'.$this->controller.'?alert=permerror');
            }
        }
        $data['type'] = (($type == "") ? "Add" : ucfirst($type));
        $alert = $this->session->flashdata('alert');
        $data['datePicker'] = 1;
        $data['pagesActive'] = 1;
        $data['alert'] = $alert;
        $type = ($editID=="") ? "Add" : "Edit";
        $data['page_title'] = PROJECT_TITLE." | ".$type." ".$this->moduleName;
        //CKEditor
        $this->load->library('ckeditor');
        $this->load->library('ckfinder');
        $this->ckeditor->basePath = base_url().'assets/ckeditor/';
        $this->ckeditor->config['removePlugins'] ='save, preview, newpage, forms';
        $this->ckeditor->config['height'] = '340px';

        //Add Ckfinder to Ckeditor
        $this->ckfinder->SetupCKEditor($this->ckeditor,'../../../../assets/ckfinder/');

        //CKEdtior
        if($editID=="") {
            $data['tbl_data'] = $this->session->userdata($this->controller.'_data');
        }
        else {
            $data['tbl_data'] = $this->SqlModel->getSingleRecord($this->tblName, array($this->pKey=>$editID));
            if(empty($data['tbl_data'])) {
                redirect(base_url().'manage/'.$this->controller,'location');
            }
        }

        $data['tbl_data']['patients'] = $this->SqlModel->getRecords('patient_id, patient_name', 'patients', '', '', ['patient_is_deleted' => '0', 'patient_clinic_id' => $this->session->userdata('clinic_id')]);
        $data['tbl_data']['work'] = $this->SqlModel->getRecords('w_id, w_name', 'work');
        $data['tbl_data']['work_type'] = $this->SqlModel->getRecords('wt_id, wt_name', 'work_type');
        $data['tbl_data']['lab'] = $this->SqlModel->getRecords('lab_id, lab_name', 'lab');

        $data['userdata'] = $this->user_data;
        $this->load->view('admin/header',$data);
        $this->load->view('admin/navigation');
        $this->load->view('admin/' . $this->addEditView);
        $this->load->view('admin/footer');
    }

    //For add record form post
    public function addRecord() {
        if($this->SqlModel->checkPermissions('labtracking', 'create')!==true) {
            $this->session->set_flashdata('alert','permerror');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        }
        if($this->input->post($this->colPrefix.'lab_id')=="") {
            $this->session->set_flashdata('alert','error');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
            exit();
        }

        $data = array(
            $this->colPrefix.'lab_id' => $this->input->post($this->colPrefix.'lab_id'),
            $this->colPrefix.'status' => $this->input->post($this->colPrefix.'status'),
            $this->colPrefix.'due_date' => date('Y-m-d', strtotime($this->input->post($this->colPrefix.'due_date'))),
            $this->colPrefix.'shade' => $this->input->post($this->colPrefix.'shade'),
            $this->colPrefix.'patient_id' => $this->input->post($this->colPrefix.'patient_id'),
            $this->colPrefix.'comment' => $this->input->post($this->colPrefix.'comment'),
            $this->colPrefix.'work_id' => $this->input->post($this->colPrefix.'work_id'),
            $this->colPrefix.'work_type_id' => $this->input->post($this->colPrefix.'work_type_id'),
            $this->colPrefix.'added' => date('Y-m-d H:i:s', strtotime('now')),
			$this->colPrefix.'updated' => date('Y-m-d H:i:s', strtotime('now')),
            $this->colPrefix.'created_by' => $this->session->userdata('admin_id'),
            $this->colPrefix.'updated_by' => $this->session->userdata('admin_id'),
            $this->colPrefix.'is_deleted' => '0',
            $this->colPrefix.'clinic_id' => $this->session->userdata('clinic_id'),
        );

        $q = $this->SqlModel->insertRecord($this->tblName, $data);
        $this->session->unset_userdata($this->controller.'_data');
        if($q!="") {
            $w = $this->SqlModel->getSingleField('w_name', 'work', ['w_id'=>$this->input->post($this->colPrefix.'work_id')]);
            $wt = $this->SqlModel->getSingleField('wt_name', 'work_type', ['wt_id'=>$this->input->post($this->colPrefix.'work_type_id')]);
            $lab = $this->SqlModel->getSingleField('lab_name', 'lab', ['lab_id'=>$this->input->post($this->colPrefix.'lab_id')]);
            $this->PatientLogs->CreatePatientLabTrackingLog($this->input->post($this->colPrefix.'patient_id'), $lab, $wt, $w, date('d-M-Y', strtotime($this->colPrefix.'due_date')));
            $this->session->set_flashdata('alert','success');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        }
        else {
            $this->session->set_flashdata('alert','error');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        }
    }

    //For add record form post
    public function editRecord($editID="") {
        if($this->SqlModel->checkPermissions('labtracking', 'update')!==true) {
            $this->session->set_flashdata('alert','permerror');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        }
        if($this->input->post($this->colPrefix.'lab_id')=="" || $editID=="") {
            $this->session->set_flashdata('alert','error');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
            exit();
        }
        $data = array(
            $this->colPrefix.'lab_id' => $this->input->post($this->colPrefix.'lab_id'),
            $this->colPrefix.'due_date' => $this->input->post($this->colPrefix.'due_date'),
            $this->colPrefix.'status' => $this->input->post($this->colPrefix.'status'),
            $this->colPrefix.'shade' => $this->input->post($this->colPrefix.'shade'),
            $this->colPrefix.'comment' => $this->input->post($this->colPrefix.'comment'),
            $this->colPrefix.'work_id' => $this->input->post($this->colPrefix.'work_id'),
            $this->colPrefix.'work_type_id' => $this->input->post($this->colPrefix.'work_type_id'),
            $this->colPrefix.'updated' => date('Y-m-d H:i:s', strtotime('now')),
            $this->colPrefix.'updated_by' => $this->session->userdata('admin_id'),
        );

        $q = $this->SqlModel->updateRecord($this->tblName, $data, array($this->pKey=>$editID));
        if($q==true) {
            $this->session->set_flashdata('alert','editsuccess');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        }
        else{
            $this->session->set_flashdata('alert','error');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        }
    }

    public function delete($deleteID = "") {
        if($this->SqlModel->checkPermissions('labtracking', 'delete')!==true) {
            $this->session->set_flashdata('alert','permerror');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        }
        $data = array(
            $this->colPrefix.'is_deleted' => '1',
        );

        $q = $this->SqlModel->updateRecord($this->tblName, $data, array($this->pKey=>$editID));
        if ($q == true) {
            $this->session->set_flashdata('alert', 'deletesuccess');
        } else {
            $this->session->set_flashdata('alert', 'deleteerror');
        }
        redirect(base_url('manage/' . $this->controller));
    }

    //For delete selected colors
    public function deleteall() {
        if($this->SqlModel->checkPermissions('labtracking', 'delete')!==true) {
            $this->session->set_flashdata('alert','permerror');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        }
        $data = array(
            $this->colPrefix.'is_deleted' => '1',
        );
        $ids = $this->input->post('records');
        if (!empty($ids)) {
            foreach ($ids as $id) {
                $this->SqlModel->updateRecord($this->tblName, $data, array($this->pKey=>$id));
            }
        }
        $this->session->set_flashdata('alert', 'deletesuccess');
        redirect(base_url('manage/' . $this->controller));
    }

    public function changestatus($id="0",$status="Published") {
        if(
            $status=="Published" ||
            $status=="Un-Published"
        ) {
            $this->SqlModel->updateRecord($this->tblName, array($this->tStatus=>$status), array($this->pKey=>$id));
            echo json_encode(array('status'=>'true',
                'id'=>$id,
                'currentStatus'=>$this->SqlModel->getSingleField($this->tStatus,
                    $this->tblName,
                    array($this->pKey=>$id)
                )
            ));
        }
        else {
            echo json_encode(array('status'=>'false'));
        }
    }
}