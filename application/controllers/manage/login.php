<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Login extends CI_Controller {
    public function __construct() {
        parent::__construct();
        session_start();
        $this->SqlModel->setTitle();
    }
    public function Index() {
        $this->user_data = $this->SqlModel->authAdmin($this->session->userdata('admin_auth'), $this->session->userdata('admin_id'));
        if (isset($this->user_data) && !empty($this->user_data)) {
            redirect(base_url('manage'));
        } else {
            $this->login();
        }
    }
    public function login($msg = "") {
        $data['loginSection'] = 1;
        $data['msg'] = $msg;
        $data['page_title'] = PROJECT_TITLE . " | Login";
        $data['logo'] = './assets/frontend/images/logo/BCG-Logo-Facebook.png';
        $this->load->view('admin/header', $data);
        $this->load->view('admin/loginScreen');
        $this->load->view('admin/footer');
    }
    public function forgot($msg = "") {
        $data['loginSection'] = 1;
        $data['msg'] = $msg;
        $data['page_title'] = PROJECT_TITLE . " | Forgot Your Password";
        $data['logo'] = './assets/frontend/images/logo/BCG-Logo-Facebook.png';
        $this->load->view('admin/header', $data);
        $this->load->view('admin/forgotScreen');
        $this->load->view('admin/footer');
    }
    public function forgotpwd() {
        if ($this->input->post('email') == "") {
            $this->forgot($msg = "Dear user, Email address is required to reset the password.");
            exit();
        }
        $forgot = $this->SqlModel->getSingleRecord('admin_users', array('email' => htmlspecialchars($this->input->post('email'))));
        if (!empty($forgot)) {
            if ($forgot['email'] == htmlspecialchars($this->input->post('email'))) {
                $pwd = substr(uniqid(), 1, 6);
                $this->SqlModel->updateRecord('admin_users', array('pwd' => md5($pwd)), array('id' => $forgot['id']));
                $body = "Hello " . $forgot['full_name'] . ":<br/><br/>As requested, we're sending you a new password. Use the information below to sign in to your administration account:<br/><br/>Username: " . $forgot['user_name'] . "<br/>New Password: " . $pwd . "<br/><br/>Admin Team,</br>" . PROJECT_TITLE;
                $ws = $this->SqlModel->getSingleRecord('site_settings',['clinic_id'=>$this->session->userdata('clinic_id')]);
                $config = array('mailtype' => 'html', 'useragent' => trim($ws['website_title']),);
                $this->load->library('email', $config);
                $this->email->from(trim('support@bitcloudglobal.com'), 'Talib Allauddin' );
                $this->email->to(trim($forgot['email']));
                $this->email->subject("Your new " . $ws['website_title'] . " CMS password.");
                $this->email->message($body);
                if (@$this->email->send()) {
                    $this->login($msg = "Dear user, your new password has been sent to your email address, please check your email and login with your new password.");
                } else {
                    $this->login($msg = "Unable to reset your password, please try again.");
                }
            } else {
                $this->forgot($msg = "Dear user, Your email address not found, please try again.");
            }
        } else {
            $this->forgot($msg = "Dear user, Your email address not found, please try again.");
        }
    }
    public function auth() {
        $un = ($this->input->post('username') ? htmlspecialchars($this->input->post('username')) : '');
        $pwd = ($this->input->post('password') ? htmlspecialchars($this->input->post('password')) : '');
        if ($un == "" || $pwd == "") {
            $this->login($mgs = "Username/Password can't be empty");
            return;
        }
        $data = $this->SqlModel->getUserPermission('admin_users', array('user_name' => $un, 'status' => 'Enable'));
        $doctor_id = $this->SqlModel->getSingleField('page_id', 'user_role', ['role_title'=>'Doctor']);

        if (empty($data)) {
            $this->login("Invalid Username/Password, please try again.");
            return;
        }
        if (md5($pwd) == $data['pwd']) {
            $ip = $_SERVER['REMOTE_ADDR'];
            $this->session->set_userdata('admin_auth', 'allow');
            $this->session->set_userdata('admin_role', $data['user_role']);
            $this->session->set_userdata('admin_user_name', $data['user_name']);
            $this->session->set_userdata('admin_full_name', $data['full_name']);
            $this->session->set_userdata('admin_id', $data['id']);
            $this->session->set_userdata('last_login', $data['last_login']);
            $this->session->set_userdata('last_ip', $data['ip']);
            $this->session->set_userdata('user_role_id',$data['user_role_id']);
            $this->session->set_userdata('clinic_id',$data['clinic_id']);
            $this->session->set_userdata('permission', $data['permission']);
            $this->session->set_userdata('doctor_role_id', $doctor_id);
            $this->session->set_userdata('role_title', $data['role_title']);
            $this->SqlModel->updateRecord('admin_users', array('last_login' => date('Y-m-d H:i:s'), 'ip' => $ip, 'user_agent' => $this->session->userdata('user_agent')), array('id' => $data['id']));
            redirect(base_url() . 'manage', 'location');
        } else {
            $this->login("Invalid Username/Password, please try again.");
        }
    }
}
