<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * This file is part of the CrudGenerator package.
 *
 * @author Talib Allauddin
 *
 * (c) Talib Allauddin <taliballauddin@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
class Reminders extends CI_Controller {

    public $tblName 	= 'reminders';
    public $pKey 		= 'r_id';
    public $moduleName 	= "Reminders";
    public $controller 	= "reminders";
    public $per_page 	= '10';
    public $colPrefix	= "r_";
    public $tStatus     = "r_status";
	public $addEditView = "addReminders";

    public function __construct() {
        // Call the Model constructor
        parent::__construct();

        $this->user_data = $this->SqlModel->authAdmin($this->session->userdata('admin_auth'),$this->session->userdata('admin_id'));

        if (!isset($this->user_data) || empty($this->user_data)) {
            redirect(base_url('manage/login'));
        }

        if($this->SqlModel->checkPermissions('reminders', 'read')!==true) {
            redirect(base_url('manage'));
        }
    }

    public function index($sortby="r_id", $order="ASC", $status="0",$keywords="-", $pg_no="") {
        // echo $pg_no;exit;
        //PER_PAGE_START
        if($this->input->get('per_page')!="" && is_numeric($this->input->get('per_page')) && (int)$this->input->get('per_page')<=100) {
            $this->session->set_userdata('per_page', $this->input->get('per_page'));
        }

        if($this->session->userdata('per_page')!="") {
            $this->per_page = $this->session->userdata('per_page');
        }

        //PER_PAGE_END
        $data['alert'] = $this->session->flashdata('alert');

        $where =  '';

        $keywords = urldecode($keywords);

        if($status=="1") {
            $where .= 'r_completed = 1 AND ';
        } else if($status != '-') {
            $status = 0;
            $where .= 'r_completed = 0 AND ';
        }
        $where .= $this->colPrefix.'is_deleted = "0" AND (' . $this->colPrefix.'created_by = ' . $this->session->userdata('admin_id') . ' OR ' . $this->colPrefix.'report_to = ' . $this->session->userdata('admin_id') . ' OR ' . $this->colPrefix.'assigned_to = ' . $this->session->userdata('admin_id') . ' OR ' . $this->colPrefix.'type = "Public") AND ' . $this->colPrefix.'clinic_id = ' . $this->session->userdata('clinic_id');
        
        $search 			= ($keywords!="-") ? array('cols'=> $this->colPrefix.'id','value'=>urldecode($keywords)) : array();
        $base_url           = base_url() . 'manage/' . $this->controller . '/index/' . $sortby . "/" . $order . "/" . $status . "/" . $keywords;
        $total_rows 		= $data['total_rows'] =$this->SqlModel->countRecords($this->tblName,$where,$search);
        $per_page 			= $data['per_page'] = $this->per_page;
        $uri_segment 		= 8;

        $data['page_title'] = PROJECT_TITLE." | ".$this->moduleName;
        $data['userdata'] = $this->user_data;
        $data['pagesActive'] = 1;

        //Pagination START
        $pconfig['base_url'] = $base_url;
        $pconfig['total_rows'] = $data['total_rows'] =  $total_rows;
        $pconfig["uri_segment"] = $uri_segment ;
        $pconfig['per_page'] = $data['per_page'] = $this->per_page;
        $pconfig['num_links'] = 1;
        $pconfig['prev_link'] = '<i class="entypo-left-open-mini"></i>';
        $pconfig['next_link'] = '<i class="entypo-right-open-mini"></i>';
        $pconfig['cur_tag_open'] = '<li  class="active"><a href="javascript:void(0)">';
        $pconfig['cur_tag_close'] = '</a></li>';
        $pconfig['full_tag_open'] = '<ul class="pagination pull-right">';
        $pconfig['full_tag_close'] = '</ul>';
        $pconfig['num_tag_open'] = "<li>";
        $pconfig['num_tag_close']= "</li>";
        $pconfig['next_tag_open'] = "<li>";
        $pconfig['next_tag_close']= "</li>";
        $pconfig['prev_tag_open'] = "<li>";
        $pconfig['prev_tag_close']= "</li>";
        $pconfig['last_tag_open'] = "<li>";
        $pconfig['last_tag_close']= "</li>";
        $pconfig['first_tag_open'] = "<li>";
        $pconfig['first_tag_close']= "</li>";
        $offset = ($this->uri->segment($uri_segment )) ? $this->uri->segment($uri_segment ) : 0;
        $this->pagination->initialize($pconfig);

        // $data['listing'] = $this->SqlModel->getRecords('*', $this->tblName, $sortby, $order,  $where, $search, $per_page, $offset,false);
        $data['listing'] = $this->SqlModel->getRecordsWithTwoJoinSameTable('r_remind_on, r_id, r_text, r_completed, r_priority, r_assigned_to, r_report_to, au.full_name assigned_to, au2.full_name report_to', 'reminders', 'admin_users au', 'id', 'r_assigned_to', 'au', 'admin_users au2', 'id', 'r_report_to', 'au2', $sortby, $order,  $where, $search, $per_page, $offset,true);

        $data['paginate'] = $this->pagination->create_links();
        //Pagination END
        $data['sortby'] 	= 	$sortby;
        $data['order'] 		= 	($order=="ASC") ? "DESC" : "ASC";
        $data['page_numb'] 	= 	$offset;
        $data['status']		=	$status;
        $data['keywords']	=	$keywords;

        $this->load->view('admin/header',$data);
        $this->load->view('admin/navigation');
        $this->load->view('admin/'.$this->controller);
        $this->load->view('admin/footer');
    }

    //For adding/edting colors
    public function control($type = "",$editID="") {
        if ($type == 'edit' && $editID != "") {
            if($this->SqlModel->checkPermissions($this->controller, 'update')==false) {
                header('location: ./manage/'.$this->controller.'?alert=permerror');
            }
        }
        $data['type'] = (($type == "") ? "Add" : ucfirst($type));
        $alert = $this->session->flashdata('alert');
        $data['datePicker'] = 1;
        $data['pagesActive'] = 1;
        $data['alert'] = $alert;
        $type = ($editID=="") ? "Add" : "Edit";
        $data['page_title'] = PROJECT_TITLE." | ".$type." ".$this->moduleName;
        //CKEditor
        $this->load->library('ckeditor');
        $this->load->library('ckfinder');
        $this->ckeditor->basePath = base_url().'assets/ckeditor/';
        $this->ckeditor->config['removePlugins'] ='save, preview, newpage, forms';
        $this->ckeditor->config['height'] = '340px';

        //Add Ckfinder to Ckeditor
        $this->ckfinder->SetupCKEditor($this->ckeditor,'../../../../assets/ckfinder/');

        //CKEdtior
        if($editID=="") {
            $data['tbl_data'] = $this->session->userdata($this->controller.'_data');
        } else {
            $data['tbl_data'] = $this->SqlModel->getSingleRecord($this->tblName, array($this->pKey=>$editID));
            if(empty($data['tbl_data'])) {
                redirect(base_url().'manage/'.$this->controller,'location');
            }
        }

        $data['userdata'] = $this->user_data;
        $this->load->view('admin/header',$data);
        $this->load->view('admin/navigation');
        $this->load->view('admin/' . $this->addEditView);
        $this->load->view('admin/footer');
    }

    //For add record form post
    public function addRecord() {
        if($this->SqlModel->checkPermissions('reminders', 'create')!==true) {
            $this->session->set_flashdata('alert','permerror');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        }
        if($this->input->post($this->colPrefix.'remind_on_date')=="") {
            $this->session->set_flashdata('alert','error');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
            exit();
        }

        $data = array(
			$this->colPrefix.'clinic_id' => $this->session->userdata('clinic_id'),
            $this->colPrefix.'added' => date('Y-m-d H:i:s', strtotime('now')),
			$this->colPrefix.'updated' => date('Y-m-d H:i:s', strtotime('now')),
			$this->colPrefix.'created_by' => $this->session->userdata('admin_id'),
			$this->colPrefix.'updated_by' => $this->session->userdata('admin_id'),
			$this->colPrefix.'is_deleted' => '0',
			$this->colPrefix.'status' => 'Enabled',
			$this->colPrefix.'remind_on' => date('Y-m-d H:i:s', strtotime($this->input->post($this->colPrefix.'remind_on_date') . ' ' . $this->input->post($this->colPrefix.'remind_on_time'))),
			$this->colPrefix.'type' => addslashes($this->input->post($this->colPrefix.'type')),
			$this->colPrefix.'completed' => $this->input->post($this->colPrefix.'completed'),
			$this->colPrefix.'text' => htmlspecialchars($this->input->post($this->colPrefix.'text')),
			$this->colPrefix.'recurring' => $this->input->post($this->colPrefix.'recurring'),
			$this->colPrefix.'report_to' => $this->input->post($this->colPrefix.'report_to'),
            $this->colPrefix.'assigned_to' => $this->input->post($this->colPrefix.'assigned_to'),
			$this->colPrefix.'priority' => $this->input->post($this->colPrefix.'priority'),
			$this->colPrefix.'lock_after' => date('Y-m-d H:i:s', strtotime($this->input->post($this->colPrefix.'lock_after')))
        );

        

        $q = $this->SqlModel->insertRecord($this->tblName, $data);
        $this->session->unset_userdata($this->controller.'_data');
        if($q!="") {
            $this->session->set_flashdata('alert','success');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        } else {
            $this->session->set_flashdata('alert','error');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        }
    }

    //For add record form post
    public function editRecord($editID="") {
        if($this->SqlModel->checkPermissions('reminders', 'update')!==true) {
            $this->session->set_flashdata('alert','permerror');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        }
        if($this->input->post($this->colPrefix.'remind_on_date')=="" || $editID=="") {
            $this->session->set_flashdata('alert','error');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
            exit();
        }
        $data = array(
            $this->colPrefix.'updated' => date('Y-m-d H:i:s', strtotime('now')),
            $this->colPrefix.'updated_by' => $this->session->userdata('admin_id'),
			$this->colPrefix.'status' => 'Enabled',			
            $this->colPrefix.'remind_on' => date('Y-m-d H:i:s', strtotime($this->input->post($this->colPrefix.'remind_on_date') . ' ' . $this->input->post($this->colPrefix.'remind_on_time'))),
			$this->colPrefix.'type' => addslashes($this->input->post($this->colPrefix.'type')),
			$this->colPrefix.'completed' => $this->input->post($this->colPrefix.'completed'),
			$this->colPrefix.'text' => htmlspecialchars($this->input->post($this->colPrefix.'text')),
			$this->colPrefix.'recurring' => $this->input->post($this->colPrefix.'recurring'),
			$this->colPrefix.'report_to' => $this->input->post($this->colPrefix.'report_to'),
            $this->colPrefix.'assigned_to' => $this->input->post($this->colPrefix.'assigned_to'),
			$this->colPrefix.'priority' => $this->input->post($this->colPrefix.'priority'),
			$this->colPrefix.'lock_after' => date('Y-m-d H:i:s', strtotime($this->input->post($this->colPrefix.'lock_after')))
        );

        $q = $this->SqlModel->updateRecord($this->tblName, $data, array($this->pKey=>$editID));
        if($q==true) {
            $this->session->set_flashdata('alert','editsuccess');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        } else {
            $this->session->set_flashdata('alert','error');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        }
    }

    public function delete($deleteID = "") {
        if($this->SqlModel->checkPermissions('reminders', 'delete')!==true) {
            $this->session->set_flashdata('alert','permerror');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        }
        $q = $this->SqlModel->deleteRecord($this->tblName, array($this->pKey => $deleteID));
        if ($q == true) {
            $this->session->set_flashdata('alert', 'deletesuccess');
        } else {
            $this->session->set_flashdata('alert', 'deleteerror');
        }
        redirect(base_url('manage/' . $this->controller));
    }

    //For delete selected colors
    public function deleteall() {
        if($this->SqlModel->checkPermissions('reminders', 'delete')!==true) {
            $this->session->set_flashdata('alert','permerror');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        }
        $ids = $this->input->post('records');
        if (!empty($ids)) {
            foreach ($ids as $id) {
                $this->SqlModel->deleteRecord($this->tblName, array($this->pKey => $id));
            }
        }
        $this->session->set_flashdata('alert', 'deletesuccess');
        redirect(base_url('manage/' . $this->controller));
    }

    public function changestatus($id="0",$status="Published") {
        if(
            $status=="Published" ||
            $status=="Un-Published"
        ) {
            $this->SqlModel->updateRecord($this->tblName, array($this->tStatus=>$status), array($this->pKey=>$id));
            echo json_encode(array('status'=>'true',
                'id'=>$id,
                'currentStatus'=>$this->SqlModel->getSingleField($this->tStatus,
                    $this->tblName,
                    array($this->pKey=>$id)
                )
            ));
        } else {
            echo json_encode(array('status'=>'false'));
        }
    }

    public function markCompleted($id='', $homepage='') {
        $data = [];
        if($this->SqlModel->checkPermissions('reminders', 'updatestatus')!==true) {
            $this->session->set_flashdata('alert','permerror');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        }
        if($id=="") {
            $this->session->set_flashdata('alert','error');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
            exit();
        }
        $data = array(
            $this->colPrefix.'updated' => date('Y-m-d H:i:s', strtotime('now')),
            $this->colPrefix.'updated_by' => $this->session->userdata('admin_id'),
            $this->colPrefix.'completed' => '1',
        );

        $q = $this->SqlModel->updateRecord($this->tblName, $data, array($this->pKey=>$id));
        if($q==true) {
            $this->session->set_flashdata('alert','editsuccess');
        } else {
            $this->session->set_flashdata('alert','error');
        }
        redirect(base_url().'manage/'.$this->controller.'/index','location');
    }

    public function markCompletedAJAX() {
        $data = [];
        header("Content-Type: application/json");
        $id = $this->input->post('id');
        if($this->SqlModel->checkPermissions('reminders', 'updatestatus')!==true) {
            $data['alert'] = 'Error';
            $data['responseType'] = 'alert-danger';
            $data['message'] = 'Seems like you don\'t have the permission';
            echo json_encode($data);
            exit();
        }
        if($id=="") {
            $data['alert'] = 'Error';
            $data['responseType'] = 'alert-danger';
            $data['message'] = 'Token number missing, please try again';
            echo json_encode($data);
            exit();
        }
        $data = array(
            $this->colPrefix.'updated' => date('Y-m-d H:i:s', strtotime('now')),
            $this->colPrefix.'updated_by' => $this->session->userdata('admin_id'),
            $this->colPrefix.'completed' => '1',
        );

        $q = $this->SqlModel->updateRecord($this->tblName, $data, array($this->pKey=>$id));
        if($q==true) {
            $data['alert'] = 'Success';
            $data['responseType'] = 'alert-success';
            $data['message'] = 'Task completed successfully!';
        } else {
            $data['alert'] = 'Error';
            $data['responseType'] = 'alert-danger';
            $data['message'] = 'Unable to update, please try again';
        }
        echo json_encode($data);
        exit();
    }
}