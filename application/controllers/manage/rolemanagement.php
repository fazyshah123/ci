<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * This file is part of the CrudGenerator package.
 *
 * @author Talib Allauddin
 *
 * (c) Talib Allauddin <ceo@bitcloudglobal.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
class Rolemanagement extends CI_Controller {

    public $tblName 	= 'user_role';
    public $pKey 		= 'page_id';
    public $moduleName 	= "Role Management";
    public $controller 	= "rolemanagement";
    public $per_page 	= '10';
    public $tStatus 	= "page_status";
    public $colPrefix	= "page_";
	public $addEditView = "addRolemanagement";
    public $arr =  ['permissions'=>[
        'admins' => ['create' => 'false' , 'read' => 'false','update' => 'false','delete' => 'false','fullaccess' => 'false'],
        'clinics' => ['create' => 'false' , 'read' => 'false','update' => 'false','delete' => 'false'],
        'rolemanagement' => ['create' => 'false' , 'read' => 'false','update' => 'false','delete' => 'false'],
        'appointments' => ['create' => 'false' , 'read' => 'false','update' => 'false','delete' => 'false'],
        'patients' => ['create' => 'false' , 'read' => 'false','update' => 'false','delete' => 'false'],
        'procedures' => ['create' => 'false' , 'read' => 'false','update' => 'false','delete' => 'false'],
        'labtracking' => ['create' => 'false' , 'read' => 'false','update' => 'false','delete' => 'false'],
        'reports' => ['create' => 'false' , 'read' => 'false','update' => 'false','delete' => 'false'],
        'treatments' => ['create' => 'false' , 'read' => 'false','update' => 'false','delete' => 'false'],
        'invoices' => ['create' => 'false' , 'read' => 'false','update' => 'false','delete' => 'false'],
        'healthrecord' => ['create' => 'false' , 'read' => 'false','update' => 'false','delete' => 'false'],
        'dentalchart' => ['create' => 'false' , 'read' => 'false','update' => 'false','delete' => 'false'],
        'doctortiming' => ['manage' => 'false'],
        'useractivitylog' => ['create' => 'false' , 'read' => 'false','update' => 'false','delete' => 'false'],
        'worktype' => ['create' => 'false' , 'read' => 'false','update' => 'false','delete' => 'false'],
        'work' => ['create' => 'false' , 'read' => 'false','update' => 'false','delete' => 'false'],
        'suppliers' => ['create' => 'false' , 'read' => 'false','update' => 'false','delete' => 'false'],
        'lab' => ['create' => 'false' , 'read' => 'false','update' => 'false','delete' => 'false'],
        'site' => ['create' => 'false' , 'read' => 'false','update' => 'false','delete' => 'false'],
        'patientfiles' => ['create' => 'false' , 'read' => 'false','update' => 'false','delete' => 'false'],
        'expenses' => ['create' => 'false' , 'read' => 'false','update' => 'false','delete' => 'false'],
        'purchases' => ['create' => 'false' , 'read' => 'false','update' => 'false','delete' => 'false'],
        'items' => ['create' => 'false' , 'read' => 'false','update' => 'false','delete' => 'false','issue' => 'false'],
        'stocks' => ['create' => 'false' , 'read' => 'false','update' => 'false','delete' => 'false'],
        'accounts' => ['create' => 'false' , 'read' => 'false','update' => 'false','delete' => 'false'],
        'purchasedetails' => ['create' => 'false' , 'read' => 'false','update' => 'false','delete' => 'false'],
        'stockreleases' => ['create' => 'false' , 'read' => 'false','update' => 'false','delete' => 'false'],
        'employeepayroll' => ['create' => 'false' , 'read' => 'false','update' => 'false','delete' => 'false'],
        'employeeattendance' => ['create' => 'false' , 'read' => 'false','update' => 'false','delete' => 'false'],
        'tokens' => ['create' => 'false' , 'read' => 'false','update' => 'false','delete' => 'false'],
        'backup' => ['create' => 'false' , 'read' => 'false','update' => 'false','delete' => 'false'],
        'reporttype' => ['create' => 'false' , 'read' => 'false','update' => 'false','delete' => 'false'],
        'roomtype' => ['create' => 'false' , 'read' => 'false','update' => 'false','delete' => 'false'],
        'room' => ['create' => 'false' , 'read' => 'false','update' => 'false','delete' => 'false'],
        'bedtype' => ['create' => 'false' , 'read' => 'false','update' => 'false','delete' => 'false'],
        'bed' => ['create' => 'false' , 'read' => 'false','update' => 'false','delete' => 'false'],
        'bedpatient' => ['create' => 'false' , 'read' => 'false','update' => 'false','delete' => 'false'],
        'roompatient' => ['create' => 'false' , 'read' => 'false','update' => 'false','delete' => 'false'],
        'doctordays' => ['create' => 'false' , 'read' => 'false','update' => 'false','delete' => 'false'],
        'doctortiming' => ['create' => 'false' , 'read' => 'false','update' => 'false','delete' => 'false'],
        'invoices' => ['create' => 'false' , 'read' => 'false','update' => 'false','delete' => 'false'],
        'patientprocedures' => ['create' => 'false' , 'read' => 'false','update' => 'false','delete' => 'false'],
        'facilities' => ['create' => 'false' , 'read' => 'false','update' => 'false','delete' => 'false'],
        'patientcase' => ['create' => 'false' , 'read' => 'false','update' => 'false','delete' => 'false'],
        'relations' => ['create' => 'false' , 'read' => 'false','update' => 'false','delete' => 'false'],
        'patientfacility' => ['create' => 'false' , 'read' => 'false','update' => 'false','delete' => 'false'],
        'treatments' => ['create' => 'false' , 'read' => 'false','update' => 'false','delete' => 'false'],
        'patienttreatment' => ['create' => 'false' , 'read' => 'false','update' => 'false','delete' => 'false'],
        'patientvisitlog' => ['create' => 'false' , 'read' => 'false','update' => 'false','delete' => 'false'],
        'patientsurgicalnote' => ['create' => 'false' , 'read' => 'false','update' => 'false','delete' => 'false'],
        'patientcaseexamination' => ['create' => 'false' , 'read' => 'false','update' => 'false','delete' => 'false'],
        'patientcaseexaminations' => ['create' => 'false' , 'read' => 'false','update' => 'false','delete' => 'false'],
        'patientoperationrecord' => ['create' => 'false' , 'read' => 'false','update' => 'false','delete' => 'false'],
        'patientcasebirth' => ['create' => 'false' , 'read' => 'false','update' => 'false','delete' => 'false'],
        'patientmedicalhistories' => ['create' => 'false' , 'read' => 'false','update' => 'false','delete' => 'false'],
        'patientmedicalcondition' => ['create' => 'false' , 'read' => 'false','update' => 'false','delete' => 'false'],
        'industries' => ['create' => 'false' , 'read' => 'false','update' => 'false','delete' => 'false'],
        'professions' => ['create' => 'false' , 'read' => 'false','update' => 'false','delete' => 'false'],
        'notes' => ['create' => 'false' , 'read' => 'false','update' => 'false','delete' => 'false'],
        'reminders' => ['create' => 'false' , 'read' => 'false','update' => 'false','delete' => 'false'],
        'test' => ['create' => 'false' , 'read' => 'false','update' => 'false','delete' => 'false'],
        // {{ROLEMANAGEMENT_CONTROLLER}}
        ]];

    public function __construct() {
        // Call the Model constructor
        parent::__construct();

        $this->user_data = $this->SqlModel->authAdmin($this->session->userdata('admin_auth'),$this->session->userdata('admin_id'));

        if (!isset($this->user_data) || empty($this->user_data)) {
            redirect(base_url('manage/login'));
        }

        if($this->SqlModel->checkPermissions('rolemanagement', 'read')!==true) {
            redirect(base_url('manage'));
        }
    }

    public function index($sortby="page_id", $order="ASC", $status="-",$keywords="-", $pg_no="") {
        //PER_PAGE_START
        if($this->input->get('per_page')!="" && is_numeric($this->input->get('per_page')) && (int)$this->input->get('per_page')<=100) {
            $this->session->set_userdata('per_page', $this->input->get('per_page'));
        }

        if($this->session->userdata('per_page')!="") {
            $this->per_page = $this->session->userdata('per_page');
        }

        //PER_PAGE_END
        $data['alert'] = $this->session->flashdata('alert');

        $where =  array();

        $keywords = urldecode($keywords);

        if($status=="Published" || $status=="Un-Published") {
            $where[$this->tStatus] = $status;
        }
        $where['user_role.is_deleted'] = '0';

        $search 			= ($keywords!="-") ? array('cols'=>'role_title','value'=>urldecode($keywords)) : array();
        $base_url           = base_url() . 'manage/' . $this->controller . '/index/' . $sortby . "/" . $order . "/" . $status . "/" . $keywords;
        $total_rows 		= $data['total_rows'] =$this->SqlModel->countRecords($this->tblName,$where,$search);
        $per_page 			= $data['per_page'] = $this->per_page;
        $uri_segment 		= 9;

        $data['page_title'] = PROJECT_TITLE." | ".$this->moduleName;
        $data['userdata'] = $this->user_data;
        $data['pagesActive'] = 1;

        if ($this->session->userdata('user_role_id') != '1') {
            $where['user_role.clinic_id'] = $this->session->userdata('clinic_id');
        }
        //Pagination START
        $pconfig['base_url'] = $base_url;
        $pconfig['total_rows'] = $data['total_rows'] =  $total_rows;
        $pconfig["uri_segment"] = $uri_segment ;
        $pconfig['per_page'] = $data['per_page'] = $this->per_page;
        $pconfig['num_links'] = 1;
        $pconfig['prev_link'] = '<i class="entypo-left-open-mini"></i>';
        $pconfig['next_link'] = '<i class="entypo-right-open-mini"></i>';
        $pconfig['cur_tag_open'] = '<li  class="active"><a href="javascript:void(0)">';
        $pconfig['cur_tag_close'] = '</a></li>';
        $pconfig['full_tag_open'] = '<ul class="pagination pull-right">';
        $pconfig['full_tag_close'] = '</ul>';
        $pconfig['num_tag_open'] = "<li>";
        $pconfig['num_tag_close']= "</li>";
        $pconfig['next_tag_open'] = "<li>";
        $pconfig['next_tag_close']= "</li>";
        $pconfig['prev_tag_open'] = "<li>";
        $pconfig['prev_tag_close']= "</li>";
        $pconfig['last_tag_open'] = "<li>";
        $pconfig['last_tag_close']= "</li>";
        $pconfig['first_tag_open'] = "<li>";
        $pconfig['first_tag_close']= "</li>";
        $offset = ($this->uri->segment($uri_segment )) ? $this->uri->segment($uri_segment ) : 0;
        $this->pagination->initialize($pconfig);

        $data['listing'] = $this->SqlModel->getRecordsWithSingleJoin('user_role.*, admin_users.full_name', $this->tblName, 'admin_users', 'id', 'created_by', $sortby, $order,  $where, $search, $per_page, $offset,false);

        $data['paginate'] = $this->pagination->create_links();
        //Pagination END
        $data['sortby'] 	= 	$sortby;
        $data['order'] 		= 	($order=="ASC") ? "DESC" : "ASC";
        $data['page_numb'] 	= 	$offset;
        $data['status']		=	$status;
        $data['keywords']	=	$keywords;

        $data['rolePage']  =   1;

        $this->load->view('admin/header',$data);
        $this->load->view('admin/navigation');
        $this->load->view('admin/'.$this->controller);
        $this->load->view('admin/footer');
    }

    //For adding/edting colors
    public function control($type = "",$editID="") {
        if ($type == 'edit' && $editID != "") {
            if($this->SqlModel->checkPermissions($this->controller, 'update')==false) {
                header('location: ./manage/'.$this->controller.'?alert=permerror');
            }
        }
        $data['type'] = (($type == "") ? "Add" : ucfirst($type));
        $alert = $this->session->flashdata('alert');
        $data['datePicker'] = 1;
        $data['pagesActive'] = 1;
        $data['alert'] = $alert;
        $type = ($editID=="") ? "Add" : "Edit";
        $data['page_title'] = PROJECT_TITLE." | ".$type." ".$this->moduleName;
        //CKEditor
        $this->load->library('ckeditor');
        $this->load->library('ckfinder');
        $this->ckeditor->basePath = base_url().'assets/ckeditor/';
        $this->ckeditor->config['removePlugins'] ='save, preview, newpage, forms';
        $this->ckeditor->config['height'] = '340px';

        //Add Ckfinder to Ckeditor
        $this->ckfinder->SetupCKEditor($this->ckeditor,'../../../../assets/ckfinder/');

        //CKEdtior
        if($editID=="") {
            $data['tbl_data'] = $this->session->userdata($this->controller.'_data');
        }
        else {
            $data['tbl_data'] = $this->SqlModel->getSingleRecord($this->tblName, array($this->pKey=>$editID));
            if(empty($data['tbl_data'])) {
                redirect(base_url().'manage/'.$this->controller,'location');
            }
        }

        $data['userdata'] = $this->user_data;
        $data['rolePage']  =   1;
        $this->load->view('admin/header',$data);
        $this->load->view('admin/navigation');
        $this->load->view('admin/' . $this->addEditView);
        $this->load->view('admin/footer');
    }

    //For add record form post
    public function addRecord() {
        $arr_POST = $_POST['permission'];
        foreach ($arr_POST as $key => $value) {
            $data = preg_split('/\_/', $key);
            $this->arr['permissions'][$data[0]][$data[1]] = 'true';
        }
        $json = json_encode($this->arr['permissions']);
        $data = array(
			'role_title' => addslashes($this->input->post('role_title')),
			'permission' => $json,
			'page_status' => $this->input->post('page_status'),
			'created_by' => $this->session->userdata('created_by'),
            'is_deleted' => 0,
			'modified_by' => $this->input->post('modified_by'),
            'clinic_id' => $this->session->userdata('clinic_id'),
			'date_created' => date('Y-m-d H:i:s', strtotime('now')),
			'date_updated' => date('Y-m-d H:i:s', strtotime('now'))
        );

        $q = $this->SqlModel->insertRecord($this->tblName, $data);
        $this->session->unset_userdata($this->controller.'_data');
        if($q!="") {
            $this->session->set_flashdata('alert','success');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        }
        else {
            $this->session->set_flashdata('alert','error');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        }
    }

    //For add record form post
    public function editRecord($editID="") {
        
        $arr_POST = $_POST['permission'];
        foreach ($arr_POST as $key => $value) {
            $data = preg_split('/\_/', $key);
            $this->arr['permissions'][$data[0]][$data[1]] = 'true';
        }
        $json = json_encode($this->arr['permissions']);
        $data = array(
            'role_title' => addslashes($this->input->post('role_title')),
            'permission' => $json,
            'page_status' => $this->input->post('page_status'),
            'modified_by' => $this->input->post('modified_by'),
            'date_created' => date('Y-m-d H:i:s', strtotime('now')),
            'date_updated' => date('Y-m-d H:i:s', strtotime('now'))
        );

        $q = $this->SqlModel->updateRecord($this->tblName, $data, array($this->pKey=>$editID));
        if($q==true) {
            $this->session->set_flashdata('alert','editsuccess');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        }
        else{
            $this->session->set_flashdata('alert','error');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        }
    }

    public function delete($deleteID = "") {
        $q = $this->SqlModel->deleteRecord($this->tblName, array($this->pKey => $deleteID));
        if ($q == true) {
            $this->session->set_flashdata('alert', 'deletesuccess');
        } else {
            $this->session->set_flashdata('alert', 'deleteerror');
        }
        redirect(base_url('manage/' . $this->controller));
    }

    //For delete selected colors
    public function deleteall() {
        $ids = $this->input->post('records');
        if (!empty($ids)) {
            foreach ($ids as $id) {
                $this->SqlModel->deleteRecord($this->tblName, array($this->pKey => $id));
            }
        }
        $this->session->set_flashdata('alert', 'deletesuccess');
        redirect(base_url('manage/' . $this->controller));
    }

    public function changestatus($id="0",$status="Published") {
        if(
            $status=="Published" ||
            $status=="Un-Published"
        ) {
            $this->SqlModel->updateRecord($this->tblName, array($this->tStatus=>$status), array($this->pKey=>$id));
            echo json_encode(array('status'=>'true',
                'id'=>$id,
                'currentStatus'=>$this->SqlModel->getSingleField($this->tStatus,
                    $this->tblName,
                    array($this->pKey=>$id)
                )
            ));
        }
        else {
            echo json_encode(array('status'=>'false'));
        }
    }
}