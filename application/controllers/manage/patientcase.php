<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * This file is part of the CrudGenerator package.
 *
 * @author Talib Allauddin
 *
 * (c) Talib Allauddin <taliballauddin@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
class PatientCase extends CI_Controller {

    public $tblName 	= 'patient_case';
    public $pKey 		= 'pc_id';
    public $moduleName 	= "Patient case";
    public $controller 	= "patientcase";
    public $per_page 	= '10';
    public $colPrefix	= "pc_";
    public $tStatus     = "pc_status";
	public $addEditView = "addPatientcase";

    public function __construct() {
        // Call the Model constructor
        parent::__construct();

        $this->user_data = $this->SqlModel->authAdmin($this->session->userdata('admin_auth'),$this->session->userdata('admin_id'));

        if (!isset($this->user_data) || empty($this->user_data)) {
            redirect(base_url('manage/login'));
        }

        if($this->SqlModel->checkPermissions('patientcase', 'read')!==true) {
            redirect(base_url('manage'));
        }
    }

    public function index($sortby="", $order="ASC", $status="-",$keywords="-", $pg_no="") {
        //PER_PAGE_START
        if($this->input->get('per_page')!="" && is_numeric($this->input->get('per_page')) && (int)$this->input->get('per_page')<=100) {
            $this->session->set_userdata('per_page', $this->input->get('per_page'));
        }

        if($this->session->userdata('per_page')!="") {
            $this->per_page = $this->session->userdata('per_page');
        }

        //PER_PAGE_END
        $data['alert'] = $this->session->flashdata('alert');

        $where =  array();

        $keywords = urldecode($keywords);

        if($status=="Published" || $status=="Un-Published") {
            $where[$this->tStatus] = $status;
        }

        $search 			= ($keywords!="-") ? array('cols'=> $this->colPrefix.'case_number','value'=>urldecode($keywords)) : array();
        $base_url           = base_url() . 'manage/' . $this->controller . '/index/' . $sortby . "/" . $order . "/" . $status . "/" . $keywords;
        $total_rows 		= $data['total_rows'] =$this->SqlModel->countRecords($this->tblName,$where,$search);
        $per_page 			= $data['per_page'] = $this->per_page;
        $uri_segment 		= 9;

        $data['page_title'] = PROJECT_TITLE." | ".$this->moduleName;
        $data['userdata'] = $this->user_data;
        $data['pagesActive'] = 1;

        //Pagination START
        $pconfig['base_url'] = $base_url;
        $pconfig['total_rows'] = $data['total_rows'] =  $total_rows;
        $pconfig["uri_segment"] = $uri_segment ;
        $pconfig['per_page'] = $data['per_page'] = $this->per_page;
        $pconfig['num_links'] = 1;
        $pconfig['prev_link'] = '<i class="entypo-left-open-mini"></i>';
        $pconfig['next_link'] = '<i class="entypo-right-open-mini"></i>';
        $pconfig['cur_tag_open'] = '<li  class="active"><a href="javascript:void(0)">';
        $pconfig['cur_tag_close'] = '</a></li>';
        $pconfig['full_tag_open'] = '<ul class="pagination pull-right">';
        $pconfig['full_tag_close'] = '</ul>';
        $pconfig['num_tag_open'] = "<li>";
        $pconfig['num_tag_close']= "</li>";
        $pconfig['next_tag_open'] = "<li>";
        $pconfig['next_tag_close']= "</li>";
        $pconfig['prev_tag_open'] = "<li>";
        $pconfig['prev_tag_close']= "</li>";
        $pconfig['last_tag_open'] = "<li>";
        $pconfig['last_tag_close']= "</li>";
        $pconfig['first_tag_open'] = "<li>";
        $pconfig['first_tag_close']= "</li>";
        $offset = ($this->uri->segment($uri_segment )) ? $this->uri->segment($uri_segment ) : 0;
        $this->pagination->initialize($pconfig);

        $data['listing'] = $this->SqlModel->getRecordsWithThreeJoin('patient_case.*, patients.patient_name, admin_users.full_name surgeon_name, room.ro_number', $this->tblName, 'patients', 'patient_id', 'pc_patient_id', 'admin_users', 'id', 'pc_dtr_id', 'room', 'ro_id', 'pc_room_id', $sortby, $order,  $where, $search, $per_page, $offset,false);

        $data['paginate'] = $this->pagination->create_links();
        //Pagination END
        $data['sortby'] 	= 	$sortby;
        $data['order'] 		= 	($order=="ASC") ? "DESC" : "ASC";
        $data['page_numb'] 	= 	$offset;
        $data['status']		=	$status;
        $data['keywords']	=	$keywords;

        $this->load->view('admin/header',$data);
        $this->load->view('admin/navigation');
        $this->load->view('admin/'.$this->controller);
        $this->load->view('admin/footer');
    }

    //For adding/edting colors
    public function control($type = "",$editID="") {
        if ($type == 'edit' && $editID != "") {
            if($this->SqlModel->checkPermissions($this->controller, 'update')==false) {
                header('location: ./manage/'.$this->controller.'?alert=permerror');
            }
        }
        $data['type'] = (($type == "") ? "Add" : ucfirst($type));
        $alert = $this->session->flashdata('alert');
        $data['datePicker'] = 1;
        $data['pagesActive'] = 1;
        $data['alert'] = $alert;
        $type = ($editID=="") ? "Add" : "Edit";
        $data['page_title'] = PROJECT_TITLE." | ".$type." ".$this->moduleName;
        //CKEditor
        $this->load->library('ckeditor');
        $this->load->library('ckfinder');
        $this->ckeditor->basePath = base_url().'assets/ckeditor/';
        $this->ckeditor->config['removePlugins'] ='save, preview, newpage, forms';
        $this->ckeditor->config['height'] = '340px';

        //Add Ckfinder to Ckeditor
        $this->ckfinder->SetupCKEditor($this->ckeditor,'../../../../assets/ckfinder/');

        //CKEdtior
        if($editID=="") {
            $data['tbl_data'] = $this->session->userdata($this->controller.'_data');
        } else {
            $data['tbl_data'] = $this->SqlModel->getSingleRecordWithThreeJoin('patient_case.*, patients.patient_name, admin_users.id, room_patient.rp_room_id', $this->tblName, 'patients', 'patient_id', 'pc_patient_id', 'room_patient', 'rp_room_id', 'pc_room_id', 'admin_users', 'id', 'pc_dtr_id', ["patient_case.pc_id" => $editID]);
         
            if(empty($data['tbl_data'])) {
                redirect(base_url().'manage/'.$this->controller,'location');
            }

            $data['patient_facilities'] = $this->SqlModel->getRecords('pf_facility_id', 
                'patient_facilities', 
                '', 
                '', 
                [
                    'pf_is_deleted' => '0', 
                    'pf_clinic_id'=> $this->session->userdata('clinic_id'),
                    'pf_pc_id' => $editID
                ], 
                ''
            );
        };

        $data['userdata'] = $this->user_data;
        $this->load->view('admin/header',$data);
        $this->load->view('admin/navigation');
        $this->load->view('admin/' . $this->addEditView);
        $this->load->view('admin/footer');
    }

    //For add record form post
    public function addRecord() {
        $data['alert'] = $this->session->flashdata('alert');
        $case = $this->SqlModel->getSingleField('pc_case_number', $this->tblName, [$this->colPrefix.'case_number'=>$this->input->post($this->colPrefix.'case_number')]);
        if ($case != '') {
            $this->session->set_flashdata('alert','caseexist');
            redirect(base_url().'manage/'.$this->controller.'/control','location');
        }
        if($this->SqlModel->checkPermissions('patientcase', 'create')!==true) {
            $this->session->set_flashdata('alert','permerror');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        }
        if($this->input->post($this->colPrefix.'case_number')=="") {
            $this->session->set_flashdata('alert','error');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
            exit();
        }
        if($this->input->post($this->colPrefix.'room_number')=="") {
            $this->session->set_flashdata('alert','error');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
            exit();
        }
        
        $name = $this->input->post('patient_name');
        $phone = $this->input->post('patient_phone1');
        $patient_id = $this->input->post('pc_patient_id');

        if ($name != '' && $phone != '') {
            $pd = array(
                'patient_name' => $name,
                'patient_mr_id' => $this->PatientModel->generateNextID(),
                'patient_status' => 'Enabled',
                'patient_phone1' => $phone1,
                'patient_gender' => $formdata['patient_gender'],
                'patient_age' => $formdata['patient_age'],
                'patient_address' => $formdata['patient_address'],
                'patient_added' => date('Y-m-d H:i:s', strtotime('now')),
                'patient_updated' => date('Y-m-d H:i:s', strtotime('now')),
                'patient_created_by' => $this->session->userdata('admin_id'),
                'patient_modified_by' => $this->session->userdata('admin_id'),
                'patient_is_deleted' => '0',
                'patient_clinic_id' => $this->session->userdata('clinic_id'),
            );
            $patient_id = $this->PatientModel->save($pd);
        }

        if ($patient_id != '' || $patient_id != '0') {
            $case_details = array(
                $this->colPrefix.'case_number' => $this->input->post($this->colPrefix.'case_number'),
                $this->colPrefix.'room_id' => $this->input->post($this->colPrefix.'room_number'),
                $this->colPrefix.'patient_id' => $patient_id,
                $this->colPrefix.'dtr_id' => $this->input->post($this->colPrefix.'dtr_id'),
                $this->colPrefix.'charges' => $this->input->post($this->colPrefix.'charges'),
                $this->colPrefix.'relative_id' => $this->input->post($this->colPrefix.'relative_id'),
                $this->colPrefix.'relative_name' => $this->input->post($this->colPrefix.'relative_name'),
                $this->colPrefix.'relative_phone' => $this->input->post($this->colPrefix.'relative_phone'),
                $this->colPrefix.'relative_cnic' => $this->input->post($this->colPrefix.'relative_cnic'),
                $this->colPrefix.'is_deleted' => '0',
                $this->colPrefix.'status' => 'Enabled',
                $this->colPrefix.'created' => date('Y-m-d H:i:s', strtotime('now')),
                $this->colPrefix.'created_by' => $this->session->userdata('admin_id'),
                $this->colPrefix.'updated' => date('Y-m-d H:i:s', strtotime('now')),
                $this->colPrefix.'updated_by' => $this->session->userdata('admin_id'),
                $this->colPrefix.'clinic_id' => $this->session->userdata('clinic_id')
            );

            $this->session->unset_userdata($this->controller.'_data');

            if($patient_case_id = $this->PatientCaseModel->save($case_details)) {

                $room_data = array(
                    'rp_room_id' => $this->input->post($this->colPrefix.'room_number'),
                    'rp_patient_id' => $patient_id,
                    'rp_alloted_by' => $this->input->post($this->colPrefix.'alloted_by'),
                    'rp_status' => 'Enabled',
                    'rp_created_by' => $this->session->userdata('admin_id'),
                    'rp_updated_by' => $this->session->userdata('admin_id'),
                    'rp_added' => date('Y-m-d H:i:s', strtotime('now')),
                    'rp_admission_date' => date('Y-m-d H:i:s', strtotime($this->input->post($this->colPrefix.'admission_date'). ' ' . $this->input->post($this->colPrefix.'admission_time'))),
                    'rp_discharge_date' => date('Y-m-d H:i:s', strtotime($this->input->post($this->colPrefix.'discharge_date'). ' ' . $this->input->post($this->colPrefix.'discharge_time'))),
                    'rp_updated' => date('Y-m-d H:i:s', strtotime('now')),
                    'rp_is_deleted' => '0',
                    'rp_clinic_id' => $this->session->userdata('clinic_id'),
                    'rp_payment_status' => 'unpaid'
                );
                $this->RoomPatientModel->save($room_data);

                foreach($_POST as $key=>$value){
                  if(strpos($key, "facility-") !== false){
                    $number = explode('-', $key);
                    $facilities_data = array(
                        'pf_patient_id' => $patient_id,
                        'pf_facility_id' => $number[1],
                        'pf_pc_id' => $patient_case_id,
                        'pf_clinic_id' => $this->session->userdata('clinic_id'),
                        'pf_is_deleted' => '0'
                    );
                    $this->PatientFacilitiesModel->save($facilities_data);
                  }
                }

                $this->session->set_flashdata('alert','success');
                redirect(base_url().'manage/'.$this->controller.'/index','location');
            } else {
                $this->session->set_flashdata('alert','error');
                redirect(base_url().'manage/'.$this->controller.'/index','location');
            }
        } else {
            $this->session->set_flashdata('alert','error');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        }
    }

    //For add record form post
    public function editRecord($editID="") {
        if($this->SqlModel->checkPermissions('patientcase', 'update')!==true) {
            $this->session->set_flashdata('alert','permerror');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        }
        if($this->input->post($this->colPrefix.'name')=="" || $editID=="") {
            $this->session->set_flashdata('alert','error');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
            exit();
        }
        $data = array(
            $this->colPrefix.'id' => $this->input->post($this->colPrefix.'id'),
			$this->colPrefix.'case_number' => $this->input->post($this->colPrefix.'case_number'),
			$this->colPrefix.'patient_id' => $this->input->post($this->colPrefix.'patient_id'),
			$this->colPrefix.'dtr_id' => $this->input->post($this->colPrefix.'dtr_id'),
			$this->colPrefix.'charges' => $this->input->post($this->colPrefix.'charges'),
			$this->colPrefix.'status' => 'Enabled',			$this->colPrefix.'created' => date('Y-m-d H:i:s', strtotime('now')),
            $this->colPrefix.'updated' => date('Y-m-d H:i:s', strtotime('now')),
            $this->colPrefix.'updated_by' => $this->session->userdata('admin_id')
        );

        

        $q = $this->SqlModel->updateRecord($this->tblName, $data, array($this->pKey=>$editID));
        if($q==true) {
            $this->session->set_flashdata('alert','editsuccess');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        } else {
            $this->session->set_flashdata('alert','error');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        }
    }

    public function delete($deleteID = "") {
        if($this->SqlModel->checkPermissions('patientcase', 'delete')!==true) {
            $this->session->set_flashdata('alert','permerror');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        }
        $q = $this->SqlModel->deleteRecord($this->tblName, array($this->pKey => $deleteID));
        if ($q == true) {
            $this->session->set_flashdata('alert', 'deletesuccess');
        } else {
            $this->session->set_flashdata('alert', 'deleteerror');
        }
        redirect(base_url('manage/' . $this->controller));
    }

    //For delete selected colors
    public function deleteall() {
        if($this->SqlModel->checkPermissions('patientcase', 'delete')!==true) {
            $this->session->set_flashdata('alert','permerror');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        }
        $ids = $this->input->post('records');
        if (!empty($ids)) {
            foreach ($ids as $id) {
                $this->SqlModel->deleteRecord($this->tblName, array($this->pKey => $id));
            }
        }
        $this->session->set_flashdata('alert', 'deletesuccess');
        redirect(base_url('manage/' . $this->controller));
    }

    public function changestatus($id="0",$status="Published") {
        if(
            $status=="Published" ||
            $status=="Un-Published"
        ) {
            $this->SqlModel->updateRecord($this->tblName, array($this->tStatus=>$status), array($this->pKey=>$id));
            echo json_encode(array('status'=>'true',
                'id'=>$id,
                'currentStatus'=>$this->SqlModel->getSingleField($this->tStatus,
                    $this->tblName,
                    array($this->pKey=>$id)
                )
            ));
        } else {
            echo json_encode(array('status'=>'false'));
        }
    }

    public function details($case_number='') {
        $data['alert'] = $this->session->flashdata('alert');
        $data['alertmsg'] = $this->session->flashdata('alertmsg');
        if ($case_number == '') {
            $this->session->set_flashdata('alert','error');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        }
        if($this->SqlModel->checkPermissions('patientcase', 'details')!==true) {
            $this->session->set_flashdata('alert','permerror');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        }
        $data['datePicker'] = 1;
        $data['userdata'] = $this->user_data;
        $data['case'] = $this->SqlModel->getSingleRecord($this->tblName, ['pc_case_number' => $case_number, 'pc_clinic_id' => $this->session->userdata('clinic_id')]);

        $data['patient_info'] = $this->SqlModel->getSingleRecord('patients', ['patient_id' => $data['case']['pc_patient_id'], 'patient_clinic_id' => $this->session->userdata('clinic_id')]);

        $this->load->view('admin/header',$data);
        $this->load->view('admin/navigation');
        $this->load->view('admin/casedetails');
        $this->load->view('admin/footer');
    }
}