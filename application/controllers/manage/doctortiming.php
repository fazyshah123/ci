<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * This file is part of the CrudGenerator package.
 *
 * @author Talib Allauddin
 *
 * (c) Talib Allauddin <taliballauddin@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
class DoctorTiming extends CI_Controller {

    public $tblName 	= 'doctor_timing';
    public $moduleName 	= "Doctor Timing";
    public $controller 	= "doctortiming";
    public $colPrefix	= "dt_";
    public $addEditView = "addDoctortiming";

    public function __construct() {
        // Call the Model constructor
        parent::__construct();

        $this->user_data = $this->SqlModel->authAdmin($this->session->userdata('admin_auth'),$this->session->userdata('admin_id'));

        if (!isset($this->user_data) || empty($this->user_data)) {
            redirect(base_url('manage/login'));
        }

        if($this->SqlModel->checkPermissions('doctortiming', 'manage')!==true) {
            redirect(base_url('manage'));
        }
    }

    //For adding/edting colors
    public function control($type = "",$editID="") {
        if ($type == 'edit' && $editID != "") {
            if($this->SqlModel->checkPermissions($this->controller, 'manage')==false) {
                header('location: ./manage/admins?alert=permerror');
            }
        }
        $uid = $this->input->get('uid');
        if ($uid != "" && $uid > 0) {
            $editID = $uid;
        }
        $type = ($editID=="") ? "Add" : "Edit";
        $data['type'] = (($type == "") ? "Add" : ucfirst($type));
        $alert = $this->session->flashdata('alert');
        $data['datePicker'] = 1;
        $data['pagesActive'] = 1;
        $data['alert'] = $alert;
        $data['page_title'] = PROJECT_TITLE." | ".$type." ".$this->moduleName;
        $where=[];
        $where['dt_is_deleted'] = '0';
        $where['dt_doctor_id'] = $editID;
        $where['dt_clinic_id'] = $this->session->userdata('clinic_id');
        
        $data['tbl_data'] = $this->SqlModel->getRecords('*', $this->tblName, 'dt_day_id', 'ASC',  $where);
        if(empty($data['tbl_data'])) {
            $data['tbl_data'] = $this->session->userdata($this->controller.'_data');
        }

        $data['userdata'] = $this->user_data;
        $this->load->view('admin/header',$data);
        $this->load->view('admin/navigation');
        $this->load->view('admin/' . $this->addEditView);
        $this->load->view('admin/footer');
    }

    //For add record form post
    public function addDoctorTiming() {
        if($this->SqlModel->checkPermissions('doctortiming', 'manage')!==true) {
            $this->session->set_flashdata('alert','permerror');
            redirect(base_url().'manage/admins/index','location');
        }
        if($this->input->post($this->colPrefix.'doctor_id')=="") {
            $this->session->set_flashdata('alert','error');
            redirect(base_url().'manage/admins/index','location');
            exit();
        }

        $doctor_id = $this->input->post($this->colPrefix.'doctor_id');
        $data = array(
            $this->colPrefix.'doctor_id' => $doctor_id,
            $this->colPrefix.'created_by' => $this->session->userdata('admin_id'),
            $this->colPrefix.'updated_by' => $this->session->userdata('admin_id'),
            $this->colPrefix.'added' => date('Y-m-d H:i:s', strtotime('now')),
            $this->colPrefix.'updated' => date('Y-m-d H:i:s', strtotime('now')),
            $this->colPrefix.'is_deleted' => '0',
            $this->colPrefix.'clinic_id' => $this->session->userdata('clinic_id')
        );

        if ($this->input->post('monday_status') == 'Available') {
            $exist = $this->SqlModel->getSingleRecord($this->tblName, ['dt_day_id'=>'1', 'dt_doctor_id'=>$doctor_id, 'dt_is_deleted'=> '0', 'dt_clinic_id'=>$this->session->userdata['clinic_id']]);
            if (count($exist) > 0) {
                $where = [];
                $where['dt_is_deleted'] = '0';
                $where['dt_day_id'] = '1';
                $where['dt_clinic_id'] = $this->session->userdata('clinic_id');
                $where['dt_doctor_id'] = $doctor_id;

                $udata[$this->colPrefix.'start_time'] = $this->input->post('mondayStartTime');
                $udata[$this->colPrefix.'end_time'] = $this->input->post('mondayEndTime');
                $udata[$this->colPrefix.'status'] = 'Available';
                $q = $this->DoctorTimingModel->update($udata, $where);
            } else {
                $data[$this->colPrefix.'day_id'] = '1';
                $data[$this->colPrefix.'start_time'] = $this->input->post('mondayStartTime');
                $data[$this->colPrefix.'end_time'] = $this->input->post('mondayEndTime');
                $data[$this->colPrefix.'status'] = 'Available';
                $q = $this->DoctorTimingModel->save($data);
            }
        } else {
            $exist = $this->SqlModel->getSingleRecord($this->tblName, ['dt_day_id'=>'1', 'dt_doctor_id'=>$doctor_id, 'dt_is_deleted'=> '0', 'dt_clinic_id'=>$this->session->userdata['clinic_id']]);
            if (count($exist) > 0) {
                $where = [];
                $where['dt_is_deleted'] = '0';
                $where['dt_day_id'] = '1';
                $where['dt_clinic_id'] = $this->session->userdata('clinic_id');
                $where['dt_doctor_id'] = $doctor_id;

                $udata[$this->colPrefix.'status'] = 'Not Available';
                $q = $this->DoctorTimingModel->update($udata, $where);
            } else {
                $data[$this->colPrefix.'day_id'] = '1';
                $data[$this->colPrefix.'status'] = 'Not Available';
                $q = $this->DoctorTimingModel->save($data);
            }
        }

        if ($this->input->post('tuesday_status') == 'Available') {
            $exist = $this->SqlModel->getSingleRecord($this->tblName, ['dt_day_id'=>'2', 'dt_doctor_id'=>$doctor_id, 'dt_is_deleted'=> '0', 'dt_clinic_id'=>$this->session->userdata['clinic_id']]);
            if (count($exist) > 0) {
                $where = [];
                $where['dt_is_deleted'] = '0';
                $where['dt_day_id'] = '2';
                $where['dt_clinic_id'] = $this->session->userdata('clinic_id');
                $where['dt_doctor_id'] = $doctor_id;

                $udata[$this->colPrefix.'start_time'] = $this->input->post('tuesdayStartTime');
                $udata[$this->colPrefix.'end_time'] = $this->input->post('tuesdayEndTime');
                $udata[$this->colPrefix.'status'] = 'Available';
                $q = $this->DoctorTimingModel->update($udata, $where);
            } else {
                $data[$this->colPrefix.'day_id'] = '2';
                $data[$this->colPrefix.'start_time'] = $this->input->post('tuesdayStartTime');
                $data[$this->colPrefix.'end_time'] = $this->input->post('tuesdayEndTime');
                $data[$this->colPrefix.'status'] = 'Available';
                
                $q = $this->DoctorTimingModel->save($data);
            }
        } else {
            $exist = $this->SqlModel->getSingleRecord($this->tblName, ['dt_day_id'=>'2', 'dt_doctor_id'=>$doctor_id, 'dt_is_deleted'=> '0', 'dt_clinic_id'=>$this->session->userdata['clinic_id']]);
            if (count($exist) > 0) {
                $where = [];
                $where['dt_is_deleted'] = '0';
                $where['dt_day_id'] = '2';
                $where['dt_clinic_id'] = $this->session->userdata('clinic_id');
                $where['dt_doctor_id'] = $doctor_id;

                $udata[$this->colPrefix.'status'] = 'Not Available';
                $q = $this->DoctorTimingModel->update($udata, $where);
            } else {
                $data[$this->colPrefix.'day_id'] = '2';
                $data[$this->colPrefix.'status'] = 'Not Available';
                
                $q = $this->DoctorTimingModel->save($data);
            }
        }

        if ($this->input->post('wednesday_status') == 'Available') {
            $exist = $this->SqlModel->getSingleRecord($this->tblName, ['dt_day_id'=>'3', 'dt_doctor_id'=>$doctor_id, 'dt_is_deleted'=> '0', 'dt_clinic_id'=>$this->session->userdata['clinic_id']]);
            if (count($exist) > 0) {
                $where = [];
                $where['dt_is_deleted'] = '0';
                $where['dt_day_id'] = '3';
                $where['dt_clinic_id'] = $this->session->userdata('clinic_id');
                $where['dt_doctor_id'] = $doctor_id;

                $udata[$this->colPrefix.'start_time'] = $this->input->post('wednesdayStartTime');
                $udata[$this->colPrefix.'end_time'] = $this->input->post('wednesdayEndTime');
                $udata[$this->colPrefix.'status'] = 'Available';
                $q = $this->DoctorTimingModel->update($udata, $where);
            } else {
                $data[$this->colPrefix.'day_id'] = '3';
                $data[$this->colPrefix.'start_time'] = $this->input->post('wednesdayStartTime');
                $data[$this->colPrefix.'end_time'] = $this->input->post('wednesdayEndTime');
                $data[$this->colPrefix.'status'] = 'Available';
                
                $q = $this->DoctorTimingModel->save($data);
            }
        } else {
            $exist = $this->SqlModel->getSingleRecord($this->tblName, ['dt_day_id'=>'3', 'dt_doctor_id'=>$doctor_id, 'dt_is_deleted'=> '0', 'dt_clinic_id'=>$this->session->userdata['clinic_id']]);
            if (count($exist) > 0) {
                $where = [];
                $where['dt_is_deleted'] = '0';
                $where['dt_day_id'] = '3';
                $where['dt_clinic_id'] = $this->session->userdata('clinic_id');
                $where['dt_doctor_id'] = $doctor_id;

                $udata[$this->colPrefix.'status'] = 'Not Available';
                $q = $this->DoctorTimingModel->update($udata, $where);
            } else {
                $data[$this->colPrefix.'day_id'] = '3';
                $data[$this->colPrefix.'status'] = 'Not Available';
                
                $q = $this->DoctorTimingModel->save($data);
            }
        }

        if ($this->input->post('thursday_status') == 'Available') {
            $exist = $this->SqlModel->getSingleRecord($this->tblName, ['dt_day_id'=>'4', 'dt_doctor_id'=>$doctor_id, 'dt_is_deleted'=> '0', 'dt_clinic_id'=>$this->session->userdata['clinic_id']]);
            if (count($exist) > 0) {
                $where = [];
                $where['dt_is_deleted'] = '0';
                $where['dt_day_id'] = '4';
                $where['dt_clinic_id'] = $this->session->userdata('clinic_id');
                $where['dt_doctor_id'] = $doctor_id;

                $udata[$this->colPrefix.'start_time'] = $this->input->post('thursdayStartTime');
                $udata[$this->colPrefix.'end_time'] = $this->input->post('thursdayEndTime');
                $udata[$this->colPrefix.'status'] = 'Available';
                $q = $this->DoctorTimingModel->update($udata, $where);
            } else {
                $data[$this->colPrefix.'day_id'] = '4';
                $data[$this->colPrefix.'start_time'] = $this->input->post('thursdayStartTime');
                $data[$this->colPrefix.'end_time'] = $this->input->post('thursdayEndTime');
                $data[$this->colPrefix.'status'] = 'Available';
                
                $q = $this->DoctorTimingModel->save($data);
            }
        } else {
            $exist = $this->SqlModel->getSingleRecord($this->tblName, ['dt_day_id'=>'4', 'dt_doctor_id'=>$doctor_id, 'dt_is_deleted'=> '0', 'dt_clinic_id'=>$this->session->userdata['clinic_id']]);
            if (count($exist) > 0) {
                $where = [];
                $where['dt_is_deleted'] = '0';
                $where['dt_day_id'] = '4';
                $where['dt_clinic_id'] = $this->session->userdata('clinic_id');
                $where['dt_doctor_id'] = $doctor_id;

                $udata[$this->colPrefix.'status'] = 'Not Available';
                $q = $this->DoctorTimingModel->update($udata, $where);
            } else {
                $data[$this->colPrefix.'day_id'] = '4';
                $data[$this->colPrefix.'status'] = 'Not Available';
                
                $q = $this->DoctorTimingModel->save($data);
            }
        }

        if ($this->input->post('friday_status') == 'Available') {
            $exist = $this->SqlModel->getSingleRecord($this->tblName, ['dt_day_id'=>'5', 'dt_doctor_id'=>$doctor_id, 'dt_is_deleted'=> '0', 'dt_clinic_id'=>$this->session->userdata['clinic_id']]);
            if (count($exist) > 0) {
                $where = [];
                $where['dt_is_deleted'] = '0';
                $where['dt_day_id'] = '5';
                $where['dt_clinic_id'] = $this->session->userdata('clinic_id');
                $where['dt_doctor_id'] = $doctor_id;

                $udata[$this->colPrefix.'start_time'] = $this->input->post('fridayStartTime');
                $udata[$this->colPrefix.'end_time'] = $this->input->post('fridayEndTime');
                $udata[$this->colPrefix.'status'] = 'Available';
                $q = $this->DoctorTimingModel->update($udata, $where);
            } else {
                $data[$this->colPrefix.'day_id'] = '5';
                $data[$this->colPrefix.'start_time'] = $this->input->post('fridayStartTime');
                $data[$this->colPrefix.'end_time'] = $this->input->post('fridayEndTime');
                $data[$this->colPrefix.'status'] = 'Available';
                
                $q = $this->DoctorTimingModel->save($data);
            }
        } else {
            $exist = $this->SqlModel->getSingleRecord($this->tblName, ['dt_day_id'=>'5', 'dt_doctor_id'=>$doctor_id, 'dt_is_deleted'=> '0', 'dt_clinic_id'=>$this->session->userdata['clinic_id']]);
            if (count($exist) > 0) {
                $where = [];
                $where['dt_is_deleted'] = '0';
                $where['dt_day_id'] = '5';
                $where['dt_clinic_id'] = $this->session->userdata('clinic_id');
                $where['dt_doctor_id'] = $doctor_id;

                $udata[$this->colPrefix.'status'] = 'Not Available';
                $q = $this->DoctorTimingModel->update($udata, $where);
            } else {
                $data[$this->colPrefix.'day_id'] = '5';
                $data[$this->colPrefix.'status'] = 'Not Available';
                
                $q = $this->DoctorTimingModel->save($data);
            }
        }

        if ($this->input->post('saturday_status') == 'Available') {
            $exist = $this->SqlModel->getSingleRecord($this->tblName, ['dt_day_id'=>'6', 'dt_doctor_id'=>$doctor_id, 'dt_is_deleted'=> '0', 'dt_clinic_id'=>$this->session->userdata['clinic_id']]);
            if (count($exist) > 0) {
                $where = [];
                $where['dt_is_deleted'] = '0';
                $where['dt_day_id'] = '6';
                $where['dt_clinic_id'] = $this->session->userdata('clinic_id');
                $where['dt_doctor_id'] = $doctor_id;

                $udata[$this->colPrefix.'start_time'] = $this->input->post('saturdayStartTime');
                $udata[$this->colPrefix.'end_time'] = $this->input->post('saturdayEndTime');
                $udata[$this->colPrefix.'status'] = 'Available';
                $q = $this->DoctorTimingModel->update($udata, $where);
            } else {
                $data[$this->colPrefix.'day_id'] = '6';
                $data[$this->colPrefix.'start_time'] = $this->input->post('saturdayStartTime');
                $data[$this->colPrefix.'end_time'] = $this->input->post('saturdayEndTime');
                $data[$this->colPrefix.'status'] = 'Available';
                
                $q = $this->DoctorTimingModel->save($data);
            }
        } else {
            $exist = $this->SqlModel->getSingleRecord($this->tblName, ['dt_day_id'=>'6', 'dt_doctor_id'=>$doctor_id, 'dt_is_deleted'=> '0', 'dt_clinic_id'=>$this->session->userdata['clinic_id']]);
            if (count($exist) > 0) {
                $where = [];
                $where['dt_is_deleted'] = '0';
                $where['dt_day_id'] = '6';
                $where['dt_clinic_id'] = $this->session->userdata('clinic_id');
                $where['dt_doctor_id'] = $doctor_id;

                $udata[$this->colPrefix.'status'] = 'Not Available';
                $q = $this->DoctorTimingModel->update($udata, $where);
            } else {
                $data[$this->colPrefix.'day_id'] = '6';
                $data[$this->colPrefix.'status'] = 'Not Available';
                
                $q = $this->DoctorTimingModel->save($data);
            }
        }

        if ($this->input->post('sunday_status') == 'Available') {
            $exist = $this->SqlModel->getSingleRecord($this->tblName, ['dt_day_id'=>'7', 'dt_doctor_id'=>$doctor_id, 'dt_is_deleted'=> '0', 'dt_clinic_id'=>$this->session->userdata['clinic_id']]);
            if (count($exist) > 0) {
                $where = [];
                $where['dt_is_deleted'] = '0';
                $where['dt_day_id'] = '7';
                $where['dt_clinic_id'] = $this->session->userdata('clinic_id');
                $where['dt_doctor_id'] = $doctor_id;

                $udata[$this->colPrefix.'start_time'] = $this->input->post('sundayStartTime');
                $udata[$this->colPrefix.'end_time'] = $this->input->post('sundayEndTime');
                $udata[$this->colPrefix.'status'] = 'Available';
                $q = $this->DoctorTimingModel->update($udata, $where);
            } else {
                $data[$this->colPrefix.'day_id'] = '7';
                $data[$this->colPrefix.'start_time'] = $this->input->post('sundayStartTime');
                $data[$this->colPrefix.'end_time'] = $this->input->post('sundayEndTime');
                $data[$this->colPrefix.'status'] = 'Available';
                
                $q = $this->DoctorTimingModel->save($data);
            }
        } else {
            $exist = $this->SqlModel->getSingleRecord($this->tblName, ['dt_day_id'=>'7', 'dt_doctor_id'=>$doctor_id, 'dt_is_deleted'=> '0', 'dt_clinic_id'=>$this->session->userdata['clinic_id']]);
            if (count($exist) > 0) {
                $where = [];
                $where['dt_is_deleted'] = '0';
                $where['dt_day_id'] = '7';
                $where['dt_clinic_id'] = $this->session->userdata('clinic_id');
                $where['dt_doctor_id'] = $doctor_id;

                $udata[$this->colPrefix.'status'] = 'Not Available';
                $q = $this->DoctorTimingModel->update($udata, $where);
            } else {
                $data[$this->colPrefix.'day_id'] = '7';
                $data[$this->colPrefix.'status'] = 'Not Available';
                
                $q = $this->DoctorTimingModel->save($data);
            }
        }

        $this->session->unset_userdata($this->controller.'_data');
        if($q!="") {
            $this->session->set_flashdata('alert','tsuccess');
            redirect(base_url().'manage/admins/index','location');
        } else {
            $this->session->set_flashdata('alert','error');
            redirect(base_url().'manage/admins/index','location');
        }
    }
}