<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * This file is part of the CrudGenerator package.
 *
 * @author Talib Allauddin
 *
 * (c) Talib Allauddin <taliballauddin@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
class Tokens extends CI_Controller {

    public $tblName 	= 'tokens';
    public $pKey 		= 't_id';
    public $moduleName 	= "Tokens";
    public $controller 	= "tokens";
    public $per_page 	= '10';
    public $colPrefix	= "t_";
    public $tStatus     = "t_status";
	public $addEditView = "addTokens";

    public function __construct() {
        // Call the Model constructor
        parent::__construct();

        $this->user_data = $this->SqlModel->authAdmin($this->session->userdata('admin_auth'),$this->session->userdata('admin_id'));

        if (!isset($this->user_data) || empty($this->user_data)) {
            redirect(base_url('manage/login'));
        }

        if($this->SqlModel->checkPermissions('tokens', 'read')!==true) {
            redirect(base_url('manage'));
        }
    }

    public function index($sortby="t_id", $order="ASC", $status="-",$keywords="-", $pg_no="") {
        //PER_PAGE_START
        if($this->input->get('per_page')!="" && is_numeric($this->input->get('per_page')) && (int)$this->input->get('per_page')<=100) {
            $this->session->set_userdata('per_page', $this->input->get('per_page'));
        }

        if($this->session->userdata('per_page')!="") {
            $this->per_page = $this->session->userdata('per_page');
        }

        //PER_PAGE_END
        $data['alert'] = $this->session->flashdata('alert');

        $where =  array();

        $keywords = urldecode($keywords);

        if($status=="Published" || $status=="Un-Published") {
            $where[$this->tStatus] = $status;
        }
        $where[$this->colPrefix.'is_deleted'] = '0';
        $where[$this->colPrefix.'clinic_id'] = $this->session->userdata('clinic_id');

        $search 			= ($keywords!="-") ? array('cols'=> $this->colPrefix.'id','value'=>urldecode($keywords)) : array();
        $base_url           = base_url() . 'manage/' . $this->controller . '/index/' . $sortby . "/" . $order . "/" . $status . "/" . $keywords;
        $total_rows 		= $data['total_rows'] =$this->SqlModel->countRecords($this->tblName,$where,$search);
        $per_page 			= $data['per_page'] = $this->per_page;
        $uri_segment 		= 9;

        $data['page_title'] = PROJECT_TITLE." | ".$this->moduleName;
        $data['userdata'] = $this->user_data;
        $data['pagesActive'] = 1;

        //Pagination START
        $pconfig['base_url'] = $base_url;
        $pconfig['total_rows'] = $data['total_rows'] =  $total_rows;
        $pconfig["uri_segment"] = $uri_segment ;
        $pconfig['per_page'] = $data['per_page'] = $this->per_page;
        $pconfig['num_links'] = 1;
        $pconfig['prev_link'] = '<i class="entypo-left-open-mini"></i>';
        $pconfig['next_link'] = '<i class="entypo-right-open-mini"></i>';
        $pconfig['cur_tag_open'] = '<li  class="active"><a href="javascript:void(0)">';
        $pconfig['cur_tag_close'] = '</a></li>';
        $pconfig['full_tag_open'] = '<ul class="pagination pull-right">';
        $pconfig['full_tag_close'] = '</ul>';
        $pconfig['num_tag_open'] = "<li>";
        $pconfig['num_tag_close']= "</li>";
        $pconfig['next_tag_open'] = "<li>";
        $pconfig['next_tag_close']= "</li>";
        $pconfig['prev_tag_open'] = "<li>";
        $pconfig['prev_tag_close']= "</li>";
        $pconfig['last_tag_open'] = "<li>";
        $pconfig['last_tag_close']= "</li>";
        $pconfig['first_tag_open'] = "<li>";
        $pconfig['first_tag_close']= "</li>";
        $offset = ($this->uri->segment($uri_segment )) ? $this->uri->segment($uri_segment ) : 0;
        $this->pagination->initialize($pconfig);

        $data['listing'] = $this->SqlModel->getRecordsWithTwoJoin('tokens.*, patients.patient_name, admin_users.full_name', $this->tblName, 'patients', 'patient_id', 't_patient_id', 
            'admin_users', 'id', 't_doctor_id', 
            $sortby, $order,  $where, $search, $per_page, $offset,false);

        $data['paginate'] = $this->pagination->create_links();
        //Pagination END
        $data['sortby'] 	= 	$sortby;
        $data['order'] 		= 	($order=="ASC") ? "DESC" : "ASC";
        $data['page_numb'] 	= 	$offset;
        $data['status']		=	$status;
        $data['keywords']	=	$keywords;

        $this->load->view('admin/header',$data);
        $this->load->view('admin/navigation');
        $this->load->view('admin/'.$this->controller);
        $this->load->view('admin/footer');
    }

    //For adding/edting colors
    public function control($type = "",$editID="") {
        $data['type'] = (($type == "") ? "Add" : ucfirst($type));
        $alert = $this->session->flashdata('alert');
        $data['datePicker'] = 1;
        $data['pagesActive'] = 1;
        $data['alert'] = $alert;
        $type = ($editID=="") ? "Add" : "Edit";
        $data['page_title'] = PROJECT_TITLE." | ".$type." ".$this->moduleName;
        //CKEditor
        $this->load->library('ckeditor');
        $this->load->library('ckfinder');
        $this->ckeditor->basePath = base_url().'assets/ckeditor/';
        $this->ckeditor->config['removePlugins'] ='save, preview, newpage, forms';
        $this->ckeditor->config['height'] = '340px';
        
        //Add Ckfinder to Ckeditor
        $this->ckfinder->SetupCKEditor($this->ckeditor,'../../../../assets/ckfinder/');

        //CKEdtior
        if($editID=="") {
            $data['tbl_data'] = $this->session->userdata($this->controller.'_data');
        } else {
            $data['tbl_data'] = $this->SqlModel->getSingleRecord($this->tblName, array($this->pKey=>$editID));
            if(empty($data['tbl_data'])) {
                redirect(base_url().'manage/'.$this->controller,'location');
            }
        }
        $data['tbl_data']['patients'] = $this->SqlModel->getRecords('patient_id, patient_name', 'patients', '', '', ['patient_is_deleted'=>'0']);

        $data['userdata'] = $this->user_data;
        $this->load->view('admin/header',$data);
        $this->load->view('admin/navigation');
        $this->load->view('admin/' . $this->addEditView);
        $this->load->view('admin/footer');
    }

    //For add record form post
    public function addRecord() {
        if($this->SqlModel->checkPermissions('tokens', 'create')!==true) {
            $this->session->set_flashdata('alert','permerror');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        }
        if($this->input->post($this->colPrefix.'patient_id')=="") {
            $this->session->set_flashdata('alert','error');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
            exit();
        }

        $data = array(
			$this->colPrefix.'patient_id' => $this->input->post($this->colPrefix.'patient_id'),
			$this->colPrefix.'doctor_id' => $this->input->post($this->colPrefix.'doctor_id'),
			$this->colPrefix.'number' => $this->input->post($this->colPrefix.'number'),
			$this->colPrefix.'date' => date('Y-m-d', strtotime('now')),
            $this->colPrefix.'added' => date('Y-m-d H:i:s', strtotime('now')),
			$this->colPrefix.'updated' => date('Y-m-d H:i:s', strtotime('now')),
            'checked' => '0',
			$this->colPrefix.'created_by' => $this->session->userdata('admin_id'),
			$this->colPrefix.'updated_by' => $this->session->userdata('admin_id'),
			$this->colPrefix.'status' => 'Enabled',
            $this->colPrefix.'clinic_id' => $this->session->userdata('clinic_id'),
            $this->colPrefix.'is_deleted' => '0',

        );


        

        $q = $this->SqlModel->insertRecord($this->tblName, $data);
        $this->session->unset_userdata($this->controller.'_data');
        if($q!="") {
            $this->session->set_flashdata('alert','success');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        } else {
            $this->session->set_flashdata('alert','error');
            redirect(base_url().'manage/'.$this->controller.'/index','location');

        }
    }


    //For add record form post
    public function editRecord($editID="") {
        if($this->SqlModel->checkPermissions('tokens', 'update')!==true) {
            $this->session->set_flashdata('alert','permerror');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        }
        if($this->input->post($this->colPrefix.'name')=="" || $editID=="") {
            $this->session->set_flashdata('alert','error');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
            exit();
        }
        $data = array(
			$this->colPrefix.'patienid' => $this->input->post($this->colPrefix.'patienid'),
			$this->colPrefix.'doctor_id' => $this->input->post($this->colPrefix.'doctor_id'),
			$this->colPrefix.'number' => $this->input->post($this->colPrefix.'number'),
			$this->colPrefix.'date' => $this->input->post($this->colPrefix.'date'),
            $this->colPrefix.'updated' => date('Y-m-d H:i:s', strtotime('now')),
            $this->colPrefix.'updated_by' => $this->session->userdata('admin_id'),
			$this->colPrefix.'status' => 'Enabled',
            $this->colPrefix.'clinic_id' => $this->session->userdata('clinic_id')
        );

        $q = $this->SqlModel->updateRecord($this->tblName, $data, array($this->pKey=>$editID));
        if($q==true) {
            $this->session->set_flashdata('alert','editsuccess');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        } else {
            $this->session->set_flashdata('alert','error');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        }
    }

    public function delete($deleteID = "") {
        if($this->SqlModel->checkPermissions('tokens', 'delete')!==true) {
            $this->session->set_flashdata('alert','permerror');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        }
        $q = $this->SqlModel->deleteRecord($this->tblName, array($this->pKey => $deleteID));
        if ($q == true) {
            $this->session->set_flashdata('alert', 'deletesuccess');
        } else {
            $this->session->set_flashdata('alert', 'deleteerror');
        }
        redirect(base_url('manage/' . $this->controller));
    }

    //For delete selected colors
    public function deleteall() {
        if($this->SqlModel->checkPermissions('tokens', 'delete')!==true) {
            $this->session->set_flashdata('alert','permerror');
            redirect(base_url().'manage/'.$this->controller.'/index','location');
        }
        $ids = $this->input->post('records');
        if (!empty($ids)) {
            foreach ($ids as $id) 
{                $this->SqlModel->deleteRecord($this->tblName, array($this->pKey => $id));
            }
        }
        $this->session->set_flashdata('alert', 'deletesuccess');
        redirect(base_url('manage/' . $this->controller));
    }

    public function changestatus($id="0",$status="Published") {
        if(
            $status=="Published" ||
            $status=="Un-Published"
        ) {
            $this->SqlModel->updateRecord($this->tblName, array($this->tStatus=>$status), array($this->pKey=>$id));
            echo json_encode(array('status'=>'true',
                'id'=>$id,
                'currentStatus'=>$this->SqlModel->getSingleField($this->tStatus,
                    $this->tblName,
                    array($this->pKey=>$id)
                )
            ));
        } else {
            echo json_encode(array('status'=>'false'));
        }
    }

    public function getUpcomingTokens() {
        $data = [];
        header("Content-Type: application/json");
        $token = $this->SqlModel->getUpcomingTokens();

        if (count($token) > 0) {
            $data['tokens'] = $token;
        }
        echo json_encode($data);
        exit();
    }

    public function getlatesttoken() {
        $data = [];
        header("Content-Type: application/json");
        $token = $this->SqlModel->getLastToken();
        if (count($token) == 0) {
            $data['token_number'] = 1;
        } else {
            $data['token_number'] = ++$token[0]['t_number'];
        }
        echo json_encode($data);
        exit();
    }

    // public function marquee() {
    //     return $this->SqlModel->getUpcomingTokens();
    // }

    public function generateToken() {
        $data = [];
        header("Content-Type: application/json");
        
        if($this->SqlModel->checkPermissions('tokens', 'create')!==true) {
            $data['alert'] = 'Error';
            $data['responseType'] = 'alert-danger';
            $data['message'] = 'Seems like you don\'t have the permission';
        }
        if($this->input->post($this->colPrefix.'number')=="") {
            $data['alert'] = 'Error';
            $data['responseType'] = 'alert-danger';
            $data['message'] = 'Token number missing, please try again';
            exit();
        }

        $data = array(
            $this->colPrefix.'patient_id' => $this->input->post($this->colPrefix.'patient_id'),
            $this->colPrefix.'doctor_id' => $this->input->post($this->colPrefix.'dtr_id'),
            $this->colPrefix.'number' => $this->input->post($this->colPrefix.'number'),
            $this->colPrefix.'date' => date('Y-m-d', strtotime('now')),
            'checked' => '0',
            $this->colPrefix.'added' => date('Y-m-d H:i:s', strtotime('now')),
            $this->colPrefix.'updated' => date('Y-m-d H:i:s', strtotime('now')),
            $this->colPrefix.'created_by' => $this->session->userdata('admin_id'),
            $this->colPrefix.'updated_by' => $this->session->userdata('admin_id'),
            $this->colPrefix.'clinic_id' => $this->session->userdata('clinic_id'),
            $this->colPrefix.'status' => 'Enabled',
            $this->colPrefix.'is_deleted' => '0'
        );

        $q = $this->SqlModel->insertRecord($this->tblName, $data);
        $this->session->unset_userdata($this->controller.'_data');
        if($q!="") {
            $data['alert'] = 'Success';
            $data['responseType'] = 'alert-success';
            $data['message'] = 'Patient '. $this->input->post('t_patient_name') .' now has token number '. $this->input->post($this->colPrefix.'number');
        } else {
            $data['alert'] = 'Error';
            $data['responseType'] = 'alert-danger';
            $data['message'] = 'Unable to generate token, please try again';
        }

        echo json_encode($data);
        exit();
    }

    public function markChecked($id='') {
        $data = array(
            'checked' => '1',
            $this->colPrefix.'updated' => date('Y-m-d H:i:s', strtotime('now')),
            $this->colPrefix.'updated_by' => $this->session->userdata('admin_id')
        );

        $q = $this->SqlModel->updateRecord($this->tblName, $data, array($this->pKey=>$id));
        if($q==true) {
            $this->session->set_flashdata('alert','editsuccess');
            redirect(base_url().'manage/','location');
        } else {
            $this->session->set_flashdata('alert','error');
            redirect(base_url().'manage/','location');
        }
    }

    public function markCanceled($id='') {
        $data = array(
            $this->colPrefix.'is_deleted' => '1',
            $this->colPrefix.'updated' => date('Y-m-d H:i:s', strtotime('now')),
            $this->colPrefix.'updated_by' => $this->session->userdata('admin_id')
        );

        $q = $this->SqlModel->updateRecord($this->tblName, $data, array($this->pKey=>$id));
        if($q==true) {
            $this->session->set_flashdata('alert','editsuccess');
            redirect(base_url().'manage/','location');
        } else {
            $this->session->set_flashdata('alert','error');
            redirect(base_url().'manage/','location');
        }
    }

    public function getTokensByDoctorId() {
        header("Content-Type: application/json");
        
        if($this->SqlModel->checkPermissions('tokens', 'read')!==true) {
            $data['alert'] = 'Error';
            $data['responseType'] = 'alert-danger';
            $data['message'] = 'Seems like you don\'t have the permission';
        }

        $doctor_id = $this->input->post('doctor_id');
        
        $q = $this->SqlModel->getRecordsWithSingleJoin('tokens.t_number, tokens.t_id, patients.patient_name', $this->tblName, 'patients', 'patient_id', 't_patient_id', 'tokens.t_number', 'ASC', ['tokens.t_is_deleted' => '0', 'tokens.t_doctor_id' => $doctor_id, 'tokens.t_date' => date('Y-m-d', strtotime('now')), 'checked'=>'0' ]);
        $this->session->unset_userdata($this->controller.'_data');
        if($q!="") {
            $data = $q;
        } else {
            $data = [];
        }

        echo json_encode($data);
        exit();
    }
}