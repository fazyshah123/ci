<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * This file is part of the CrudGenerator package.
 *
 * @author Talib Allauddin
 *
 * (c) Talib Allauddin <ceo@bitcloudglobal.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
class Reports extends CI_Controller {

    public $pKey 		= 'clinic_id';
    public $moduleName 	= "Reports";
    public $controller 	= "reports";

    public function __construct() {
        // Call the Model constructor
        parent::__construct();

        $this->user_data = $this->SqlModel->authAdmin($this->session->userdata('admin_auth'),$this->session->userdata('admin_id'));

        if (!isset($this->user_data) || empty($this->user_data)) {
            redirect(base_url('manage/login'));
        }

        if($this->SqlModel->checkPermissions('reports', 'read')!==true) {
            redirect(base_url('manage'));
        }
    }

    public function index() {
        //PER_PAGE_START
        $data['reportsPage']  =   1;
        $data['datePicker'] = 1;
        $data['userdata'] = $this->user_data;
        $this->load->view('admin/header',$data);
        $this->load->view('admin/navigation');
        $this->load->view('admin/'.$this->controller);
        $this->load->view('admin/footer');
    }

    public function reportdata() {
        $data = [];
        header("Content-Type: application/json");

        $key = $this->input->post('key');

        if ($key == 'today') {
            $start_date = date('Y-m-d', strtotime('now'));
            $end_date = date('Y-m-d', strtotime('now'));
        } else if ($key == 'yesterday') {
            $start_date = date('Y-m-d', strtotime('yesterday'));
            $end_date = date('Y-m-d', strtotime('yesterday'));
        } else if ($key == 'week') {
            if(date('l') == 'Monday') {
                $start_date = date('Y-m-d', strtotime('now'));
            } else {
                $start_date = date('Y-m-d', strtotime('last monday'));
            }
            $end_date = date('Y-m-d', strtotime('now'));
        } else if ($key == 'month') {
            $start_date = date('Y-m-01', strtotime('now'));
            $end_date = date('Y-m-d', strtotime('now'));
        } else if ($key == 'range') {
            $start_date = date('Y-m-d', strtotime($this->input->post('start_date')));
            $end_date = date('Y-m-d', strtotime($this->input->post('end_date')));
        }

        if ($this->checkmydate($start_date) && $this->checkmydate($end_date)) {
            $data['appointmentCount'] = $this->SqlModel->getAppointments($start_date, $end_date);
            $data['expensesCount'] = $this->SqlModel->getExpenses($start_date, $end_date);
            //$data['incomeCount'] = $this->SqlModel->getIncome($start_date, $end_date);
            $data['issuedItemsCount'] = $this->SqlModel->getIssuedItems($start_date, $end_date);
            $data['supplierPayableCount'] = $this->SqlModel->getSupplierPayable();
            $data['newPatientCount'] = $this->SqlModel->getNewPatients($start_date, $end_date);
            $data['purchasesCount'] = $this->SqlModel->getPurchases($start_date, $end_date);
            $data['staffAttendance'] = $this->SqlModel->getStaffAttendance($start_date, $end_date);
            $data['expenseDetails'] = $this->SqlModel->getExpenseDetails($start_date, $end_date);
            $data['purchaseDetails'] = $this->SqlModel->getPurchaseDetails($start_date, $end_date);
            $data['issuedItemDetails'] = $this->SqlModel->getIssuedItemDetails($start_date, $end_date);
            $data['totalEarnings'] = $this->SqlModel->getTotalEarnings($start_date, $end_date);
        } else {
            $data['alert'] = 'Error';
            $data['responseType'] = 'alert-danger';
            $data['message'] = 'Invalid date, please try again!';
        }

        echo json_encode($data);
        exit;
    }

    function checkmydate($date) {
      $tempDate = explode('-', $date);
      // checkdate(month, day, year)
      return checkdate($tempDate[1], $tempDate[2], $tempDate[0]);
    }
}