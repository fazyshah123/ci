var fs = require('fs'),
  mkdirp = require('mkdirp'),
  exports = module.exports = {},
  exec = require('child_process').exec,
  Constants = require('./testing_constants.js');

exports.takeScreenshot = function(name){
  browser.takeScreenshot().then(function(data) {
    browser.getCapabilities().then(function (capabilities) {
       var browserName = capabilities.caps_.browserName,
         currentSpec = jasmine.getEnv().currentSpec,
         folderName = './screenshots/' + browserName,
               filename = folderName + '/[MANUAL] ' + currentSpec.description + '-' + name + '.png';

          mkdirp(folderName, function(err) { 
          console.log(err);

        var stream = fs.createWriteStream(filename);
          stream.write(new Buffer(data, 'base64'));
          stream.end();
      });
    });
  });
};

exports.reset_testing_database = function() {
  exec('mysql -e "source test/config/injection.sql" -u root -p LuyepBCPLJuTPzFe -D digitemb_testing', function(error, stdout, stderr) {
    if(error) {
      console.error('Unable to reset the database.' + error);
    } else {
      console.log('db Reset!');
    }
  });
  browser.sleep(Constants.explicitWait.twoSeconds); // db is often not reset completely and test executes.
};

exports.helperFunctions =  {
  moveCusrorToTheElement : function(elementId) {
    /*  This is because the field above or below the given field has a tooltip which covers this field.
      Hence this field becomes unclickable.
        Moving the cursor to this field, makes hover disappear. 
      */

    browser.actions().mouseMove(element(by.id(elementId))).perform();
    browser.sleep(Constants.explicitWait.oneSecond); // wait for cursor movement.
  }
};
