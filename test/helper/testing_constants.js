var constants = {
  explicitWait : {
    oneSecond : '1000',
    twoSeconds: '2000',
    threeSeconds : '3000',
  },
  baseURL: 'http://localhost/medical-information-management-system/manage',
  titleBeforeLogin: 'SehatWise | BitCloudGlobal | Login',
};

module.exports = constants;