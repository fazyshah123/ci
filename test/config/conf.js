exports.config = { 
  baseUrl: 'http://localhost/medical-information-management-system',
  seleniumAddress: 'http://localhost:4444/wd/hub',

  // location of E2E test specs
  specs: [
    '../test.js'
  ],

  // configure multiple browsers to run tests
  multiCapabilities: [
  // {
  //   'browserName': 'firefox'
  // }, 
  {
    'browserName': 'chrome'
  },
  //  {
  //   'browserName': 'internet explorer',
  //   'ignoreProtectedModeSettings': true
  // }
  // , {
  //   'browserName': 'safari'
  // }
  ],

  // or configure a single browser

  // capabilities: {
  //   'browserName': 'chrome'
  // },

  // testing framework, jasmine is the default
  framework: 'jasmine',

  jasmineNodeOpts: {
    showColors: true,
    displaySpecDuration: true,
    // overrides jasmine's print method to report dot syntax for custom reports
    defaultTimeoutInterval: 50000
  }
};