var exports = module.exports = {};
exports.users = {
  ValidClinicUser: {
    email: "a.rafay@machotics.com",
    fullname: "Abdur Rafay",
    username: "rafay",
    password: "123123"
  },
  InvalidClinicUser: {
    email: "bachajhamora@machotics.com",
    fullname: "Bacha Jhamora",
    username: "bacha",
    password: "jhamora"
  },
  ValidPatient: {
    patient_name: "Danish",
    patient_age: "22",
    patient_address: "la pata",
    patient_gender: "Female",
    patient_phone1: "031112345678",
    patient_phone2: "031112345678",
    patient_phone3: "031112345678",
    patient_email: "maingogole@gmail.com",
    patient_birth_date: "01 November 1990"
  },

  InvalidPatient1: {
    patient_name: "2435245",
    patient_age: "224352",
    patient_address: "2345245",
    patient_gender: "245245",
    patient_phone1: "safasdf",
    patient_phone2: "asdfasdf",
    patient_phone3: "adsfasdfasdf",
    patient_email: "maingogolegmail.com",
    patient_dob: "01 November 2050"
  },

  InvalidPatient2: {
    patient_name: "!@#$%^&*!*&^%$#@!",
    patient_age: "!@#$%^&*@*&^%$#@!",
    patient_address: "!@#$%^&**&^%$#@!",
    patient_gender: "!@#$%^&**#&^%$#@!",
    patient_phone1: "!@#$%^&**$&^%$#@!",
    patient_phone2: "!@#$%^&**$^&^%$#@!",
    patient_phone3: "!@#$%^&&**&^%$#@!",
    patient_email: "!@#$%^&(**&^%$#@!",
    patient_dob: "!@#$%^&**&^%)$#@!0"
  },

  ValidAppointment: {
    appointment_patient_id: "Muhammad Jazib Aleem",
    appointment_date: "11 December 2019",
    appointment_start_time: "12:04",
    appointment_end_time: "12:34",
    appointment_dtr_id: "10",
    appointment_procedure_id: "1",
  },
    
  InvalidAppointment1: {
    appointment_patient_id: "2",
    appointment_date: "dfasdfas",
    appointment_start_time: "asdf",
    appointment_end_time: "adsf",
    appointment_dtr_id: "10",
    appointment_procedure_id: "1",
  },

  InvalidAppointment2: {
    appointment_patient_id: "2",
    appointment_date: "^%$#&%^$%^$",
    appointment_start_time: "&&%&%^%$#%",
    appointment_end_time: "*^%$$%^",
    appointment_dtr_id: "%$$$^%",
    appointment_procedure_id: "^%&*(*",
  },

  ValidPatientCase: {
    pc_case_number: "1237",
    pc_room_number: "Room # 5 ( | Per Day | Rs.)",
    pc_charges: "4000",
    Add_Patient: "Abdur Rafay",
    pc_relative_id: "3",
    pc_relative_name: "mushtaq",
    pc_relative_cnic: "0444-675-1-7",
    pc_relative_phone: "0333987876",
    pc_admission_date: "12 December 2019",
    pc_admission_time: "5:30 PM",
    pc_discharge_date: "13 December 2019",
    pc_discharge_time: "5:30 PM",
    pc_alloted_by: "Najeeb",
  },

  InvalidPatientCase1: {
    pc_case_number: "assdf",
    pc_room_number: "Room # 5 ( | Per Day | Rs.)",
    pc_charges: "dsafasdf",
    Add_Patient: "Abdur Rafay",
    pc_relative_id: "3",
    pc_relative_name: "2435",
    pc_relative_cnic: "23452345",
    pc_relative_phone: "afgafga",
    pc_admission_date: "sdsfasfds",
    pc_admission_time: "adfadfadf", 
    pc_discharge_date: "asdfasdfasdf",
    pc_discharge_time: "asdfsdf",
    pc_alloted_by: "123412341234",
  },

  InvalidPatientCase2: {
    pc_case_number: "!@#$%^&*",
    pc_room_number: "Room # 5 ( | Per Day | Rs.)",
    pc_charges: "@#$%^%$",
    Add_Patient: "Abdur Rafay",
    pc_relative_id: "3",
    pc_relative_name: "#$^&*%^#$",
    pc_relative_cnic: "&^%*&",
    pc_relative_phone: "*^%*",
    pc_admission_date: "$#%&&^&",
    pc_admission_time: "@#$%^%&*",
    pc_discharge_date: "*&^^%$%",
    pc_discharge_time: "(%%^&^$)",
    pc_alloted_by: "(&^%#%&)",
  },

  Validladtracking: {
    lt_patient_id: "2",
    lt_lab_id: "1",
    lt_due_date: "11 December 2019",
    lt_shade: "gold",
    lt_work_id: "2",
    lt_work_type_id: "2",
  },

  Invalidlabtracking1: {
    lt_patient_id: "2",
    lt_lab_id: "1",
    lt_due_date: "adfs2314",
    lt_shade: "129856789",
    lt_work_id: "2",
    lt_work_type_id: "2",
  },

  Invalidlabtracking2: {
    lt_patient_id: "2",
    lt_lab_id: "1",
    lt_due_date: "@#^&*(*",
    lt_shade: "@#$%^&*&^",
    lt_work_id: "2",
    lt_work_type_id: "2",
  },

  Validtoken: {
    t_patient_id: "1 ",
    t_doctor_id: "10",
    t_number: "234",
  },

  Invalidtoken1: {
    t_patient_id: "1",
    t_doctor_id: "10",
    t_number: "jdfglasd",
  },

  Invalidtoken2: {
    t_patient_id: "1",
    t_doctor_id: "10",
    t_number: "!@#$%^&&",
  },

  Validreminder: {
    r_remind_on_date: "13 December 2019 ",
    r_remind_on_time: "10:14 AM",
    r_type: "Private",
    r_priority: "Important",
    r_report_to: "Jamal",
    r_assigned_to: "Najeeb",
    r_lock_after: "14 December 2019",
  },

  Invalidreminder1: {
    r_remind_on_date: "as31qwe423fdg ",
    r_remind_on_time: "3lkh5646hgl256",
    r_type: "Private",
    r_priority: "Important",
    r_report_to: "Jamal",
    r_assigned_to: "Najeeb",
    r_lock_after: "0875Df987sdf5g",
  },

  Invalidreminder2: {
    r_remind_on_date: "!@#$%^&*()",
    r_remind_on_time: "!@#$%^&*&^",
    r_type: "!@#$%^&**",
    r_priority: "#$%^&**&&",
    r_report_to: "!@#$%^&*(&$",
    r_assigned_to: "@#$%^&*(*%",
    r_lock_after: "@#$%^&*(&$",
  },

  ValidEmployeeAttendance: {
    ea_user_id: "4",
    ea_date: "12/14/2019",
    attendance_type_in: "Check-In",
    attendance_type_out: "Check-Out",
    ea_checkin: "11:23 AM",
    ea_checkout: "",
    ea_attendance: "P",
  },

  InvalidEmployeeAttendance1: {
    ea_user_id: "4",
    ea_date: "356KJHG4567KJ",
    attendance_type_in: "Check-In",
    attendance_type_out: "Check-Out",
    ea_checkin: "11:23 AM",
    ea_checkout: "",
    ea_attendance: "P",
  },

  InvalidEmployeeAttendance2: {
    ea_user_id: "4",
    ea_date: "!@#$%^&*&*(&",
    attendance_type_in: "Check-In",
    attendance_type_out: "Check-Out",
    ea_checkin: "11:23 AM",
    ea_checkout: "",
    ea_attendance: "P",
  },

  ValidEmployeePayroll: {
    ea_user_id: "4",
    ea_date: "12/14/2019",
    attendance_type_in: "Check-In",
    attendance_type_out: "Check-Out",
    ea_checkin: "11:23 AM",
    ea_checkout: "",
    ea_attendance: "P",
  },

  InvalidEmployeePayroll1: {
    ea_user_id: "4",
    ea_date: "356KJHG4567KJ",
    attendance_type_in: "Check-In",
    attendance_type_out: "Check-Out",
    ea_checkin: "11:23 AM",
    ea_checkout: "",
    ea_attendance: "P",
  },

  InvalidEmployeePayroll2: {
    ea_user_id: "4",
    ea_date: "!@#$%^&*(*(",
    attendance_type_in: "Check-In",
    attendance_type_out: "Check-Out",
    ea_checkin: "11:23 AM",
    ea_checkout: "",
    ea_attendance: "P",
  },

  ValidAccount: {
    a_name: "alhaaj",
  },

  InvalidAccount1: {
    a_name: "h5j6k4n5m6",
  },

  InvalidAccount2: {
    a_name: "@#$%^&*(&",
  },

  ValidSuppliers: {
    s_name: "medical tablet",
    s_contact_person: "alafis khan",
    s_phone1: "03339488787",
    package_desitnation: "033386799654",
    s_email: "alasif@gmail.com",
    s_address: "plot DHA city ",
  },

  InvalidSuppliers1: {
    s_name: "24352345",
    s_contact_person: "kwdgfaefug",
    s_phone1: "shfgh",
    package_desitnation: "shshsdghsh",
    s_email: "sfsdth445m",
    s_address: "sdfgsdfgs54yrty ",
  },

  InvalidSuppliers2: {
    s_name: "!@#$%^&*(",
    s_contact_person: "@#$%^&^%",
    s_phone1: "$%^&*(*^&",
    package_desitnation: "@#$%^&&^*&",
    s_email: "@#$%^&*(&*(",
    s_address: "!@#$%^&^%&",
  },

  ValidExpenses: {
   e_account_id: "1",
   e_amount: "100",
   e_date: "18 December 2019",
  },

  InvalidExpenses1: {
    e_account_id: "1",
    e_amount: "adsgag",
    e_date: "4j5h6j7h5jk",
  },

  InvalidExpenses2: {
    e_account_id: "1",
    e_amount: "!@#$%^&&%",
    e_date: "@#$%^&*&%",
  },

  ValidPurchase: {
   p_account_id: "1",
   p_remarks: "hood",
   p_supplier_id: "1",
   pd_item_id: "1",
   pd_qty: "34",
   pd_unit_price: "500",
   pd_discount_rs: "",
   pd_discount_percent: "50",
   supplier_prev_bal: "",
   p_total: "",
   p_balance: "",
   p_credit_change: "",
   p_total_discount_rs: "",
   p_total_discount_percent: "",
   p_paid: "1000",
   p_change_to_return: "",
   p_date: "12/18/2019",
  },

  InvalidPurchase1: {
   p_account_id: "1",
   p_remarks: "2345",
   p_supplier_id: "1",
   pd_item_id: "1",
   pd_qty: "sdfgsfdg",
   pd_unit_price: "sdfg",
   pd_discount_rs: "",
   pd_discount_percent: "sfdgsdfg",
   supplier_prev_bal: "",
   p_total: "",
   p_balance: "",
   p_credit_change: "",
   p_total_discount_rs: "",
   p_total_discount_percent: "",
   p_paid: "sfdgsdfg",
   p_change_to_return: "",
   p_date: "778eddfg8",
  },

  InvalidPurchase2: {
   p_account_id: "1",
   p_remarks: "!@#$%^&^&",
   p_supplier_id: "1",
   pd_item_id: "1",
   pd_qty: "!@#$%^&*^&*",
   pd_unit_price: "@#$%^&*^*",
   pd_discount_rs: "",
   pd_discount_percent: "%$^&*()&*^",
   supplier_prev_bal: "",
   p_total: "",
   p_balance: "",
   p_credit_change: "",
   p_total_discount_rs: "",
   p_total_discount_percent: "",
   p_paid: "!@#$%^&^^",
   p_change_to_return: "",
   p_date: "@#$%^&*)(*",
  },

  ValidItem: {
   i_name:"GOLIYA",
   i_unit:"80",
  },

  InvalidItem1: {
   i_name: "0985620596",
   i_unit: "IGHSLGLKF",
  },

  InvalidItem2: {
   i_name: "!@#$%^&^&",
   i_unit: "@#$%^&&^",
  },

  ValidIssudesItem: {
   sr_item_id: "1",
   sr_qty: "1000",
   sr_issued_to: "alhaaj",
  },

  InvalidIssudesItem1: {
   sr_item_id: "1",
   sr_qty: "JHLSKHFG",
   sr_issued_to: "alhaaj",
  },

  InvalidIssudesItem2: {
   sr_item_id: "1",
   sr_qty: "!@#$%^&^*&",
   sr_issued_to: "$%&*%$",
  },

  ValidStock: {
   s_item_id: "1",
   s_qty: "1000",
   s_min_qty: "500",
  },

  InvalidStock1: {
   s_item_id: "1",
   s_qty: "sfdgffgs",
   s_min_qty: "gsdfssd", 
  },

  InvalidStock2: {
   s_item_id: "1",
   s_qty: "!@#$%^&*",
   s_min_qty: "@#$%^&*%$", 
  },

  ValidBedPatient: {
   bp_patient_id: "3",
   bp_bed_id: "3",
   bp_alloted_by: "10",
   bp_admission_date: "18 December 2019",
   bp_discharge_date: "19 December 2019",
   bp_payment_status: "Paid",
  },

  InvalidBedPatient1: {
   bp_patient_id: "3",
   bp_bed_id: "3",
   bp_alloted_by: "10",
   bp_admission_date: "3g33k3k43k4",
   bp_discharge_date: "3k4jke4g3k4",
   bp_payment_status: "Paid",
  },

  InvalidBedPatient2: {
   bp_patient_id: "3",
   bp_bed_id: "3",
   bp_alloted_by: "10",
   bp_admission_date: "!@#$%^&*(",
   bp_discharge_date: "!@#$%^&*",
   bp_payment_status: "@#$%^&&^",
  },

  ValidRoomPatient: {
   rp_patient_id: "19",
   rp_room_id: "5",
   rp_alloted_by: "12",
   rp_admission_date: "18 December 2019",
   rp_discharge_date: "19 December 2019",
  },

  InvalidRoomPatient1: {
   rp_patient_id: "19",
   rp_room_id: "5",
   rp_alloted_by: "12",
   rp_admission_date: "2346jhf234",
   rp_discharge_date: "s87g5fd87sd8", 
  },

  InvalidRoomPatient2: {
   rp_patient_id: "19",
   rp_room_id: "5",
   rp_alloted_by: "12",
   rp_admission_date: "!@#$%^&*^%",
   rp_discharge_date: "@#$%^&%&(", 
  },

  ValidBed: {
   b_number: "1256",
   b_bed_type_id: "3",
  },

  InvalidBed1: {
   b_number: "aksjfgkh",
   b_bed_type_id: "3",
  },

  InvalidBed2: {
   b_number: "@#$%^&*^&*",
   b_bed_type_id: "3",
  },

  ValidBedType: {
   bt_name: "with tv and a.c" ,
   bt_per_day_charges: "3000000",
   bt_billing_type: "1", 
  },

  InvalidBedType1: {
   bt_name: "39459345n4m7nb",
   bt_per_day_charges: "dghdghdghd",
   bt_billing_type: "0",
  },

  InvalidBedType2: {
   bt_name: "!@#$%^&*(&",
   bt_per_day_charges: ")(*&^%$",
   bt_billing_type: "0",
  },

  ValidRoom: {
   ro_number: "12444",
   ro_room_type_id: "4",
  },

  InvalidRoom1: {
   ro_number: "!@#$%^&&^",
   ro_room_type_id: "4", 
  },

  InvalidRoom2: {
   ro_number: "kjfdgksdf",
   ro_room_type_id: "4", 
  },

  ValidRoomType: {
   rt_name: "with a.c  and ",
   rt_per_day_charges: "100000",
   rt_billing_type: "1",
  },

  InvalidRoomType1: { 
   rt_name: "sdf9876srrt",
   rt_per_day_charges: "sfdgsdfg",
   rt_billing_type: "0",
  },

  InvalidRoomType2: { 
   rt_name: "!@#$%^&%$#",
   rt_per_day_charges: "!@#$%^&*&^%",
   rt_billing_type: "0",
  },

  ValidProcedure: {
   procedure_name: "Operation ",
   procedure_price: "300000",
  },

  InvalidProcedure1: {
   procedure_name: "3464e67467 ",
   procedure_price: "sdfgsdfgsd",
  },

  InvalidProcedure2: {
   procedure_name: "@#$%&*()(*) ",
   procedure_price: "sdfgsdfgsd",
  },

  ValidWorkType: {
   wt_name: "tooth shape ",
  },

  InvalidWorkType1: {
   wt_name: "664764565",
  },

  InvalidWorkType2: {
   wt_name: "!@#$%^&^%",
  },

  ValidWork: {
   w_name: "Plastic",
  },

  InvalidWork1: {
   w_name: "3563456345",
  },

  InvalidWork2: {
   w_name: "!@%$#*&&&^%$",
  },

  ValidLab: {
   lab_name: "dow lab",
   lab_phone: "03339877654",
   lab_contact_person: "raheel batt",
   lab_email: "info@dowlab.com",
   lab_address: "Gulistan-e-johar opposite site of aladin",
  },

  InvalidLab1: { 
   lab_name: "345633456",
   lab_phone: "sdghdghdfg",
   lab_contact_person: "32452345234",
   lab_email: "b235b2345b245bm",
   lab_address: "2562456lkjlh356hk",
  },

  InvalidLab2: { 
   lab_name: "!@#$%^&#*%$",
   lab_phone: "!@#$$%^&)*%$",
   lab_contact_person: "!@(#$$%^&*%$",
   lab_email: "!@#$%^!$&*%$",
   lab_address: "!@#$%%^$&*%$",
  },

  ValidReportType: {
   r_name: "surgery",
  },

  InvalidReportType1: {
   r_name: "154hjvgj43h5g9",
  },

  InvalidReportType2: {
   r_name: "!@#$%^&*&^",
  },

  ValidRelation: {
   relative_name: "son",
  },

  InvalidRelation1: {
   relative_name: "4536554",
  },

  InvalidRelation2: {
   relative_name: "!@#$%^&*(",
  },

  ValidFacility: {
   facility_name: "Television",
  },

  InvalidFacility1: {
   facility_name: "46745675467",
  },

  InvalidFacility2: {
   facility_name: "!@#$%^)",
  },

  ValidWebsiteSettings: {
   website_title: "Muhammadi Hospital 60000",
   tag_line: "Preventation is better than Cure! 60000",
   address: "Plot No C-53, Shah Jahan Ave, Block 17, Federal B Area, Karachi. 60000",
   full_address: "Plot No C-53, Shah Jahan Ave, Block 17, Federal B Area, Karachi. 60000",
   tel: "03333333333",
   website_url: "www.mohammadihospital.com",
  },

  InvalidWebsiteSettings1: {
   website_title: "3khl4k56hl34",
   tag_line: "3563464356ljh",
   address: "36jh43g63k4g6k45",
   full_address: "36jh43g6k43j",
   tel: "hghdghdghdgh",
   website_url: "fghdfghghjhhhhhhke",
  },

  InvalidWebsiteSettings2: {
   website_title: ")#*$(#*)$#*$)",
   tag_line: "&#&$#&%&#",
   address: ")^&^^$)(&^%!@#$%^(",
   full_address: "!@#$%^&*()",
   tel: "!@#$$%^&*()",
   website_url: "!@#$%$^&*()",
  },

};