var constants = browser.params;

var loginPage = {

  // Gets URL
  get: function() {
    browser.get('');
  },

  setUsername: function(username) {
    element(by.id("username")).click();
    element(by.id("username")).clear();
    element(by.id("username")).sendKeys(username);
  },

  setUserPassword: function(password) {
    element(by.id("password")).click();
    element(by.id("password")).clear();
    element(by.id("password")).sendKeys(password);
  }, 

  setForgotEmail: function(email) {
    element(by.id("email")).click();
    element(by.id("email")).clear();
    element(by.id("email")).sendKeys(email);
  },

  hitLoginButton : function () {
      element(by.className('btn-login')).click();
  }, 

  hitForgotButton : function () {
      element(by.id("submitForgot")).click();
  },

  hitActivationButton : function () {
      element(by.id("submitActivate")).click();
  },

  hitForgotBackButton : function () {
      element(by.id("backForgot")).click();
  },

  hitActivationBackButton : function () {
      element(by.id("backActivate")).click();
  },

  switchForgotButton : function () {
      element(by.id("forgot")).click();
  },

  switchActivationButton : function () {
      element(by.id("activate")).click();
  },

  assertions : {
        expectInvalidCredentials : function () {
            expect(element(by.className("description")).getText()).toBe("Invalid Username/Password, please try again.");
        },
        expectDoesNotExist : function () {
            expect(element(by.css('.alert-danger')).getText()).toBe('The user provided does not exist on the system.');
        },
        expectLoggedIn: function(fullname) {
            expect(element(by.id('welcome-user')).getText()).toBe('Welcome! ' + fullname);
        },
        expectAlertForPasswordReset : function(email) {
            expect(element(by.className("alert-success")).getText()).toBe("Password for " + email + " has been reset. Please check your email for the new password.");
        },
        expectAlertForSuccessfullActivationEmail : function(email) {
            expect(element(by.className("alert-success")).getText()).toBe("An activation link has been sent. Please check your email.");
        }
  }
};

module.exports = loginPage;