var constants = browser.params;

var roomPage = {

  // Gets URL
  get: function() {
    browser.get('');
  },

  setNumber: function(role_title) {
    element(by.id("role_title")).click();
    element(by.id("role_title")).clear();
    element(by.id("role_title")).sendKeys(role_title);
  },

  setRoomtypeId: function(ro_room_type_id) {
    element(by.id("ro_room_type_id")).click();
    element(by.id("ro_room_type_id")).clear();
    element(by.id("ro_room_type_id")).sendKeys(ro_room_type_id);
  }, 

  hitSubmitButton : function () {
      element(by.id('r_submit')).click();
  },

  hitCancelButton : function () {
      element(by.className("btn btn-danger")).click();
  },

  hitDeletButton : function () {
      element(by.id('deleteAllRecords')).click();
  },

  hitAddRoomButton : function () {
      element(by.id("ADD_R")).click();
  },

  assertions : {
    expectRecordSaved : function () {
        expect(element(by.className("description")).getText()).toBe("Success! Room added sucessfully.");
    },
    expectDoesNotExist : function () {
        expect(element(by.css('.alert-danger')).getText()).toBe('The user provided does not exist on the system.');
    },
  }
};

module.exports = roomPage;