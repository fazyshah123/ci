var constants = browser.params;

var RoomPatitentPage = {

  // Gets URL
  get: function() {
    browser.get('');
  },

  setPatientId: function(rp_patient_id) {
    element(by.id("rp_patient_id")).click();
    element(by.id("rp_patient_id")).clear();
    element(by.id("rp_patient_id")).sendKeys(rp_patient_id);
  },

  setRoomId: function(rp_room_id) {
    element(by.className("rp_room_id")).click(); 
    element(by.className("rp_room_id")).clear(); 
    element(by.className("rp_room_id")).sendKeys(rp_room_id);
  }, 

  setAllotedBy: function(rp_alloted_by) {
    element(by.id("rp_alloted_by")).click();
    element(by.id("rp_alloted_by")).clear();
    element(by.id("rp_alloted_by")).sendKeys(rp_alloted_by);
  },
     
  setAdmissionDate: function(rp_admission_date) {
    element(by.id("rp_admission_date")).click();
    element(by.id("rp_admission_date")).clear();
    element(by.id("rp_admission_date")).sendKeys(rp_admission_date);
  },

  setDischargeDate: function(rp_discharge_date) {
    element(by.id("rp_discharge_date")).click();
    element(by.id("rp_discharge_date")).clear();
    element(by.id("rp_discharge_date")).sendKeys(rp_discharge_date);
  },

  hitSubmitButton : function () {
      element(by.id('roompasubmit')).click();
  },

  hitCancelButton : function () {
      element(by.className("btn btn-danger")).click();
  },

  hitDeletButton : function () {
      element(by.id('deleteAllRecords')).click();
  },

  hitRoomPatitentButton : function () {
      element(by.id("add_roomp")).click();
  },

  assertions : {
    expectRecordSaved : function () {
        expect(element(by.className("description")).getText()).toBe("Success! Room Patitent added sucessfully.");
    },
    expectDoesNotExist : function () {
        expect(element(by.css('.alert-danger')).getText()).toBe('The user provided does not exist on the system.');
    },
  }
};

module.exports = RoomPatitentPage;