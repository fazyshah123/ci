var constants = browser.params;

var PatientCasePage = {

  // Gets URL
  get: function() {
    browser.get('');
  },

  setCaseNumber: function(pc_case_number) {
    element(by.id("pc_case_number")).click();
    element(by.id("pc_case_number")).clear();
    element(by.id("pc_case_number")).sendKeys(pc_case_number);
  },

  setRoomNumber: function(pc_room_number) {
    element(by.className("pc_room_number")).click();
    element(by.className("pc_room_number")).clear(); 
    element(by.className("pc_room_number")).sendKeys(pc_room_number);
  }, 

  setCharges: function(pc_charges) {
    element(by.id("pc_charges")).click();
    element(by.id("pc_charges")).clear();
    element(by.id("pc_charges")).sendKeys(pc_charges);
  },
     
  setAddPatient: function(Add_Patient) {
    element(by.id("Add_Patient")).click();
    element(by.id("Add_Patient")).clear();
    element(by.id("Add_Patient")).sendKeys(Add_Patient);
  },

  setRelativeId: function(pc_relative_id) {
    element(by.id("pc_relative_id")).click();
    element(by.id("pc_relative_id")).clear();
    element(by.id("pc_relative_id")).sendKeys(pc_relative_id);
  },

  setRelativeName: function(pc_relative_name) {
    element(by.id("pc_relative_name")).click();
    element(by.id("pc_relative_name")).clear();
    element(by.id("pc_relative_name")).sendKeys(pc_relative_name);
  },

  setRelativeCnic: function(pc_relative_cnic) {
    element(by.id("pc_relative_cnic")).click();
    element(by.id("pc_relative_cnic")).clear();
    element(by.id("pc_relative_cnic")).sendKeys(pc_relative_cnic);
  },

  setRelativPhone: function(pc_relative_phone) {
    element(by.id("pc_relative_phone")).click();
    element(by.id("pc_relative_phone")).clear();
    element(by.id("pc_relative_phone")).sendKeys(pc_relative_phone);
  },

  setAdmissionDate: function(pc_admission_date) {
    element(by.id("pc_admission_date")).click();
    element(by.id("pc_admission_date")).clear();
    element(by.id("pc_admission_date")).sendKeys(pc_admission_date);
  },

  setAdmissionTime: function(pc_admission_time) {
    element(by.id("pc_admission_time")).click();
    element(by.id("pc_admission_time")).clear();
    element(by.id("pc_admission_time")).sendKeys(pc_admission_time);
  },

  setDtr_Id: function(pc_dtr_id) {
    element(by.id("pc_dtr_id")).click();
    element(by.id("pc_dtr_id")).clear();
    element(by.id("pc_dtr_id")).sendKeys(pc_dtr_id);
  },

  setDischargeDate: function(pc_discharge_date) {
    element(by.id("pc_discharge_date")).click();
    element(by.id("pc_discharge_date")).clear();
    element(by.id("pc_discharge_date")).sendKeys(pc_discharge_date);
  },

  setDischarge_Time: function(pc_discharge_time) {
    element(by.id("pc_discharge_time")).click();
    element(by.id("pc_discharge_time")).clear();
    element(by.id("pc_discharge_time")).sendKeys(pc_discharge_time);
  },

  setAllotedBy: function(pc_alloted_by) {
    element(by.id("pc_alloted_by")).click();
    element(by.id("pc_alloted_by")).clear();
    element(by.id("pc_alloted_by")).sendKeys(pc_alloted_by); 
  },

  hitSubmitButton : function () {
      element(by.id('patientcase_submit')).click();
  },

  hitCancelButton : function () {
      element(by.className("btn-danger")).click();
  },

  hitDeletButton : function () {
      element(by.id('deleteAllRecords')).click();
  },

  hitAddPatientcaseButton : function () {
      element(by.id("add_Pateintcase")).click();
  },

  assertions : {
    expectRecordSaved : function () {
        expect(element(by.className("description")).getText()).toBe("Success! Patient case added sucessfully.");
    },
    expectDoesNotExist : function () {
        expect(element(by.css('.alert-danger')).getText()).toBe('The user provided does not exist on the system.');
    },
  }
};

module.exports = PatientCasePage;