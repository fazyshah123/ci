var constants = browser.params;

var BedPage = {

  // Gets URL
  get: function() {
    browser.get('');
  },

  setPatientId: function(b_number) {
    element(by.id("b_number")).click();
    element(by.id("b_number")).clear();
    element(by.id("b_number")).sendKeys(b_number);
  },

  setLabId: function(b_bed_type_id) {
    element(by.className("b_bed_type_id")).click();
    element(by.className("b_bed_type_id")).clear(); 
    element(by.className("b_bed_type_id")).sendKeys(b_bed_type_id);
  }, 

  hitSubmitButton : function () {
      element(by.id('bedsubmit')).click();
  },

  hitCancelButton : function () {
      element(by.className("lt_cancel")).click();
  },

  hitDeletButton : function () {
      element(by.id('deleteAllRecords')).click();
  },

  hitAddBedButton : function () {
      element(by.id("bed_add")).click();
  },

  assertions : {
    expectRecordSaved : function () {
        expect(element(by.className("description")).getText()).toBe("Success! Bed added sucessfully.");
    },
    expectDoesNotExist : function () {
        expect(element(by.css('.alert-danger')).getText()).toBe('The user provided does not exist on the system.');
    },
  }
};

module.exports = BedPage;
















