var constants = browser.params;

var roomPage = {

  // Gets URL
  get: function() {
    browser.get('');
  },

  setNumber: function(ro_number) {
    element(by.id("ro_number")).click();
    element(by.id("ro_number")).clear();
    element(by.id("ro_number")).sendKeys(ro_number);
  },

  setRoomtypeId: function(ro_room_type_id) {
    element(by.id("ro_room_type_id")).click();
    element(by.id("ro_room_type_id")).clear();
    element(by.id("ro_room_type_id")).sendKeys(ro_room_type_id);
  }, 

  hitSubmitButton : function () {
      element(by.id('roomsubmit')).click();
  },

  hitCancelButton : function () {
      element(by.className("btn btn-danger")).click();
  },

  hitDeletButton : function () {
      element(by.id('deleteAllRecords')).click();
  },

  hitAddRoomButton : function () {
      element(by.id("add_room")).click();
  },

  assertions : {
    expectRecordSaved : function () {
        expect(element(by.className("description")).getText()).toBe("Success! Room added sucessfully.");
    },
    expectDoesNotExist : function () {
        expect(element(by.css('.alert-danger')).getText()).toBe('The user provided does not exist on the system.');
    },
  }
};

module.exports = roomPage;