var constants = browser.params;

var EmployeePayrollPage = {

  // Gets URL
  get: function() {
    browser.get('');
  },

  setUserId: function(ea_user_id) {
    element(by.id("ea_user_id")).click();
    element(by.id("ea_user_id")).clear();
    element(by.id("ea_user_id")).sendKeys(ea_user_id);
  },
 
  setDate: function(ea_date) {
    element(by.className("ea_date")).click();
    element(by.className("ea_date")).clear(); 
    element(by.className("ea_date")).sendKeys(ea_date);
  },

  setTotalSalary: function(ep_total_salary) {
    element(by.id("ep_total_salary")).click();
    element(by.id("ep_total_salary")).clear();
    element(by.id("ep_total_salary")).sendKeys(ep_total_salary);
  },
     
  setNetSalary: function(ep_net_salary) {
    element(by.id("ep_net_salary")).click();
    element(by.id("ep_net_salary")).clear();
    element(by.id("ep_net_salary")).sendKeys(ep_net_salary);
  },

  setDeduction: function(ep_deduction) {
    element(by.id("ep_deduction")).click();
    element(by.id("ep_deduction")).clear();
    element(by.id("ep_deduction")).sendKeys(ep_deduction);
  },

  setCommission: function(ep_commission) {
    element(by.id("ep_commission")).click();
    element(by.id("ep_commission")).clear();
    element(by.id("ep_commission")).sendKeys(ep_commission);
  },

  hitSubmitButton : function () {
      element(by.id('payrollsubmit')).click();
  },

  hitCancelButton : function () {
      element(by.className("btn btn-danger")).click();
  },

  hitDeletButton : function () {
      element(by.id('deleteAllRecords')).click();
  },

  hitAddEmployeePayrollButton : function () {
      element(by.id("addpayroll")).click();
  },

  assertions : {
    expectRecordSaved : function () {
        expect(element(by.className("description")).getText()).toBe("Success! Employee Payroll added sucessfully.");
    },
    expectDoesNotExist : function () {
        expect(element(by.css('.alert-danger')).getText()).toBe('The user provided does not exist on the system.');
    },
  }
};

module.exports = EmployeePayrollPage;