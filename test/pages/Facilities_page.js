var constants = browser.params;

var FacilitiesPage = {

  // Gets URL
  get: function() {
    browser.get('');
  },

  setName:function(facility_name) {
    element(by.id("facility_name")).click();
    element(by.id("facility_name")).clear();
    element(by.id("facility_name")).sendKeys(facility_name);
  }, 

  hitSubmitButton : function () {
      element(by.id('facilsubmit')).click();
  },

  hitCancelButton : function () {
      element(by.className("btn-danger")).click();
  },
 
  hitDeletButton : function () {
      element(by.id('deleteAllRecords')).click();
  },

  hitAddFacilitiesButton : function () {
      element(by.id("add_facilities")).click();
  },    

  assertions : {           
    expectRecordSaved : function () {      
        expect(element(by.className("description")).getText()).toBe("Success, Facility added successfully.");                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
    },
    expectDoesNotExist : function () {
        expect(element(by.css('.alert-danger')).getText()).toBe('The user provided does not exist on the system.');                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
    },         
  }                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
};                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               

module.exports = FacilitiesPage;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
 