var constants = browser.params;

var SuppliersPage = {

  // Gets URL
  get: function() {
    browser.get('');
  },

  setName: function(s_name) {
    element(by.id("s_name")).click();
    element(by.id("s_name")).clear();
    element(by.id("s_name")).sendKeys(s_name);
  },

  setContactPerson: function(s_contact_person) {
    element(by.id("s_contact_person")).click();
    element(by.id("s_contact_person")).clear();
    element(by.id("s_contact_person")).sendKeys(s_contact_person);
  },
  
  setPhone1: function(s_phone1) {
    element(by.id("s_phone1")).click();
    element(by.id("s_phone1")).clear();
    element(by.id("s_phone1")).sendKeys(s_phone1);
  },

  setPackageDesitnation: function(package_desitnation) {
    element(by.id("package_desitnation")).click();
    element(by.id("package_desitnation")).clear();
    element(by.id("package_desitnation")).sendKeys(package_desitnation);
  },

  setEmail: function(s_email) {
    element(by.id("s_email")).click();
    element(by.id("s_email")).clear();
    element(by.id("s_email")).sendKeys(s_email);
  },

  setAddress: function(s_address) {
    element(by.id("s_address")).click();
    element(by.id("s_address")).clear();
    element(by.id("s_address")).sendKeys(s_address);
  },

  hitSubmitButton : function () {
      element(by.id('s_submit')).click();
  },

  hitCancelButton : function () {
      element(by.className("btn btn-danger")).click();
  },

  hitDeletButton : function () {
      element(by.id('deleteAllRecords')).click();
  },

  hitAddSuppliersButton : function () {
      element(by.id("addsuppliers")).click();
  },

  assertions : {
    expectRecordSaved : function () {
        expect(element(by.className("description")).getText()).toBe("Success! Suppliers added sucessfully.");
    },
    expectDoesNotExist : function () {
        expect(element(by.css('.alert-danger')).getText()).toBe('The user provided does not exist on the system.');
    },
  }
};

module.exports = SuppliersPage;