var constants = browser.params;

var ReminderPage = {

  // Gets URL
  get: function() {
    browser.get('');
  },

  setRemindOnDate: function(r_remind_on_date) {
    element(by.id("r_remind_on_date")).click();
    element(by.id("r_remind_on_date")).clear();
    element(by.id("r_remind_on_date")).sendKeys(r_remind_on_date);
  }, 

  setremindOnTime: function(r_remind_on_time) {
    element(by.className("r_remind_on_time")).click();
    element(by.className("r_remind_on_time")).clear(); 
    element(by.className("r_remind_on_time")).sendKeys(r_remind_on_time);
  },                                                                                                                   
  
  setType: function(r_type) {
    element(by.id("r_type")).click();
    element(by.id("r_type")).clear();
    element(by.id("r_type")).sendKeys(r_type);
  },
     
  setPriority: function(r_priority) {
    element(by.id("r_priority")).click();
    element(by.id("r_priority")).clear();
    element(by.id("r_priority")).sendKeys(r_priority);
  },

  setReportTo: function(r_report_to) {
    element(by.id("r_report_to")).click();
    element(by.id("r_report_to")).clear();
    element(by.id("r_report_to")).sendKeys(r_report_to);
  },

  setAssignedTo: function(r_assigned_to) {
    element(by.id("r_assigned_to")).click();
    element(by.id("r_assigned_to")).clear();
    element(by.id("r_assigned_to")).sendKeys(r_assigned_to);
  },

  setLockAfter: function(r_lock_after) {
    element(by.id("pc_relative_cnic")).click();
    element(by.id("pc_relative_cnic")).clear();
    element(by.id("pc_relative_cnic")).sendKeys(pc_relative_cnic);
  },

  setCompleted: function(r_completed) {
    element(by.id("r_completed")).click();
    element(by.id("r_completed")).clear();
    element(by.id("r_completed")).sendKeys(r_completed);
  },

  hitSubmitButton : function () {
      element(by.id('remindersubmit')).click();
  },

  hitCancelButton : function () {
      element(by.className("btn btn-danger")).click();
  },

  hitDeletButton : function () {
      element(by.id('deleteAllRecords')).click();
  },

  hitAddreminderButton : function () {
      element(by.id("addreminders")).click();
  },

  assertions : {
    expectRecordSaved : function () {
        expect(element(by.className("description")).getText()).toBe("Success! Reminder added sucessfully.");
    },
    expectDoesNotExist : function () {
        expect(element(by.css('.alert-danger')).getText()).toBe('The user provided does not exist on the system.');
    },
  }
};

module.exports = ReminderPage;