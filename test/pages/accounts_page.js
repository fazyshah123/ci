var constants = browser.params;

var AccountsPage = {

  // Gets URL
  get: function() {
    browser.get('');
  },
 
  setName: function(a_name) {
    element(by.id("a_name")).click();
    element(by.id("a_name")).clear();
    element(by.id("a_name")).sendKeys(a_name);
  },

  setDescription: function(text) {
    browser.switchTo().frame(element(by.css('#cke_a_description iframe')).click().sendKeys(text));
  },

  hitSubmitButton : function () {
      element(by.id('account_submit')).click();
  },

  hitCancelButton : function () {
      element(by.className("btn btn-danger")).click();
  },

  hitDeletButton : function () {
      element(by.id('deleteAllRecords')).click();
  },

  hitAddAccountsButton : function () {
      element(by.id("addaccounts")).click();
  },

  assertions : {
    expectRecordSaved : function () {
        expect(element(by.className("description")).getText()).toBe("Success! Accounts added sucessfully.");
    },
    expectDoesNotExist : function () {
        expect(element(by.css('.alert-danger')).getText()).toBe('The user provided does not exist on the system.');
    },
  },
};

module.exports = AccountsPage;