var constants = browser.params;

var EmployeeAttendancePage = {

  // Gets URL
  get: function() {
    browser.get('');
  },

  setUserId: function(ea_user_id) {
    element(by.id("ea_user_id")).click();
    element(by.id("ea_user_id")).clear();
    element(by.id("ea_user_id")).sendKeys(ea_user_id);
  },
 
  setDate: function(ea_date) {
    element(by.className("ea_date")).click();
    element(by.className("ea_date")).clear(); 
    element(by.className("ea_date")).sendKeys(ea_date);
  },

  setTypeIn: function(attendance_type_in) {
    element(by.id("attendance_type_in")).click();
    element(by.id("attendance_type_in")).clear();
    element(by.id("attendance_type_in")).sendKeys(attendance_type_in);
  },
     
  setTypeOut: function(attendance_type_out) {
    element(by.id("attendance_type_out")).click();
    element(by.id("attendance_type_out")).clear();
    element(by.id("attendance_type_out")).sendKeys(attendance_type_out);
  },

  setCheckin: function(ea_checkin) {
    element(by.id("ea_checkin")).click();
    element(by.id("ea_checkin")).clear();
    element(by.id("ea_checkin")).sendKeys(ea_checkin);
  },

  setCheckout: function(ea_checkout) {
    element(by.id("ea_checkout")).click();
    element(by.id("ea_checkout")).clear();
    element(by.id("ea_checkout")).sendKeys(ea_checkout);
  },

   setAttendance: function(ea_attendance) {
    element(by.id("ea_attendance")).click();
    element(by.id("ea_attendance")).clear();
    element(by.id("ea_attendance")).sendKeys(ea_attendance);
  },

  hitSubmitButton : function () {
      element(by.id('empattsubmit')).click();
  },

  hitCancelButton : function () {
      element(by.className("btn btn-danger")).click();
  },

  hitDeletButton : function () {
      element(by.id('deleteAllRecords')).click();
  },

  hitAddEmployeeButton : function () {
      element(by.id("addemployee")).click();
  },

  assertions : {
    expectRecordSaved : function () {
        expect(element(by.className("description")).getText()).toBe("Success! Employee Attendance added sucessfully.");
    },
    expectDoesNotExist : function () {
        expect(element(by.css('.alert-danger')).getText()).toBe('The user provided does not exist on the system.');
    },
  }
};

module.exports = EmployeeAttendancePage;