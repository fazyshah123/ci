var constants = browser.params;
 
var WorkTypePage = {

  // Gets URL
  get: function() {
    browser.get('');
  },

  setName: function(wt_name) {
    element(by.id("wt_nam")).click();
    element(by.id("wt_nam")).clear();
    element(by.id("wt_nam")).sendKeys(wt_name);
  },

  hitSubmitButton : function () {
    element(by.id('workt_status')).click();
  },

  hitCancelButton : function () {
    element(by.className("btn btn-danger")).click();
  },

  hitDeletButton : function () {
    element(by.id('deleteAllRecords')).click();
  },

  hitAddWorktypeButton : function () {
    element(by.id("add_workt")).click();
  },

  assertions : {
    expectRecordSaved : function () {
        expect(element(by.className("description")).getText()).toBe("Success! Work Type added sucessfully.");
    },
    expectDoesNotExist : function () {
        expect(element(by.css('.alert-danger')).getText()).toBe('The user provided does not exist on the system.');
    },
  }
};

module.exports = WorkTypePage;