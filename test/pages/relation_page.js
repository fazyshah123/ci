var constants = browser.params;

var relationPage = {

  // Gets URL
  get: function() {
    browser.get('');
  },

  setName: function(relative_name) {
    element(by.id("relative_name")).click();
    element(by.id("relative_name")).clear();
    element(by.id("relative_name")).sendKeys(relative_name);
  },

  hitSubmitButton : function () {
      element(by.id('relation_submit')).click();
  },

  hitCancelButton : function () {
      element(by.className("btn-danger")).click();
  }, 

  hitPatientCountButton : function () {
      element(by.id('deleteAllRecords')).click();
  },

  hitAddPatientButton : function () {
      element(by.id("add_relation")).click();
  },

  assertions : {
    expectRecordSaved : function () {
        expect(element(by.className("description")).getText()).toBe("Success, Relation added successfully.");
    },
    expectDoesNotExist : function () {
        expect(element(by.css('.alert-danger')).getText()).toBe('The user provided does not exist on the system.');
    },
  }
};

module.exports = relationPage;