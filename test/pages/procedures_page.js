var constants = browser.params;

var ProdcedurePage = {

  // Gets URL
  get: function() {
    browser.get('');
  },

  setName: function(procedure_name) {
    element(by.id("procedure_name")).click();
    element(by.id("procedure_name")).clear();
    element(by.id("procedure_name")).sendKeys(procedure_name);
  },

  setPrice: function(procedure_price) {
    element(by.className("procedure_price")).click();
    element(by.className("procedure_price")).clear(); 
    element(by.className("procedure_price")).sendKeys(procedure_price);
  }, 
     
  hitSubmitButton : function () {
      element(by.id('procedures_submit')).click();
  },

  hitCancelButton : function () {
      element(by.className("btn btn-danger")).click();
  },
 
  hitDeletButton : function () {
      element(by.id('deleteAllRecords')).click();
  },

  hitAddProdcedureButton : function () {
      element(by.id("add_procedures")).click();
  },

  assertions : {
    expectRecordSaved : function () {
        expect(element(by.className("description")).getText()).toBe("Success! Prodcedure added sucessfully.");
    },
    expectDoesNotExist : function () {
        expect(element(by.css('.alert-danger')).getText()).toBe('The user provided does not exist on the system.');
    },
  }
};

module.exports = ProdcedurePage;