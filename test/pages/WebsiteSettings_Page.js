var constants = browser.params;

var WebsiteSettingsPage = {

  // Gets URL
  get: function() {
    browser.get('');
  },

  setTitle: function(website_title) {
    element(by.id("website_title")).click();
    element(by.id("website_title")).clear();
    element(by.id("website_title")).sendKeys(website_title);
  },

  setLine: function(tag_line) {
    element(by.id("tag_line")).click();
    element(by.id("tag_line")).clear();
    element(by.id("tag_line")).sendKeys(tag_line);
  }, 

  setAddress: function(address) {
    element(by.id("address")).click();
    element(by.id("address")).clear();
    element(by.id("address")).sendKeys(address);
  },

  setfullAddress: function(full_address) {
    element(by.id("full_address")).click();
    element(by.id("full_address")).clear();
    element(by.id("full_address")).sendKeys(full_address);
  },

  setTel: function(tel) {
    element(by.id("tel")).click();
    element(by.id("tel")).clear();
    element(by.id("tel")).sendKeys(tel);
  },

  setWebsiteUrl: function(website_url) {
    element(by.id("website_url")).click();
    element(by.id("website_url")).clear();
    element(by.id("website_url")).sendKeys(website_url);
  },

  hitSubmitButton : function () {
      element(by.id('websitesetting_submit')).click();
  },

  hitCancelButton : function () {
      element(by.className("btn-danger")).click();
  },

  assertions : {
    expectRecordSaved : function () {
        expect(element(by.className("description")).getText()).toBe("Success, Website Settings added successfully.");
    },
    expectDoesNotExist : function () {
        expect(element(by.css('.alert-danger')).getText()).toBe('The user provided does not exist on the system.');
    },
  }
};

module.exports = WebsiteSettingsPage;