var constants = browser.params;

var StockPage = {

  // Gets URL
  get: function() {
    browser.get('');
  },

  setItemId: function(s_item_id) {
    element(by.id("s_item_id")).click();
    element(by.id("s_item_id")).clear();
    element(by.id("s_item_id")).sendKeys(s_item_id);
  },

  setQty: function(s_qty) {
    element(by.id("s_qty")).click();
    element(by.id("s_qty")).clear(); 
    element(by.id("s_qty")).sendKeys(s_qty);
  }, 

  setMinQty: function(s_min_qty) {
    element(by.id("s_min_qty")).click();
    element(by.id("s_min_qty")).clear(); 
    element(by.id("s_min_qty")).sendKeys(s_min_qty);
  },

  hitSubmitButton : function () {
      element(by.id('stocksubmit')).click();
  },

  hitCancelButton : function () {
      element(by.className("btn-danger")).click();
  },

  hitPatientCountButton : function () {
      element(by.id('deleteAllRecords')).click();
  },

  hitAddPatientButton : function () {
      element(by.id("addstock")).click();
  },

  assertions : {
    expectRecordSaved : function () {
        expect(element(by.className("description")).getText()).toBe("Success, Stock added successfully.");
    },
    expectDoesNotExist : function () {
        expect(element(by.css('.alert-danger')).getText()).toBe('The user provided does not exist on the system.');
    },
  }
};

module.exports = StockPage;