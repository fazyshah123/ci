var constants = browser.params;

var PaurchasesPage = {

  // Gets URL
  get: function() {
    browser.get('');
  },

  setAccountId: function(p_account_id) {
    element(by.id("p_account_id")).click();
    element(by.id("p_account_id")).clear();
    element(by.id("p_account_id")).sendKeys(p_account_id);
  },

  setRemarks: function(p_remarks) {
    element(by.className("p_remarks")).click();
    element(by.className("p_remarks")).clear(); 
    element(by.className("p_remarks")).sendKeys(p_remarks);
  }, 

  setDate: function(p_date) {
    element(by.id("p_date")).click();
    element(by.id("p_date")).clear();
    element(by.id("p_date")).sendKeys(p_date);
  },
     
  setSupplierId: function(p_supplier_id) {
    element(by.id("p_supplier_id")).click();
    element(by.id("p_supplier_id")).clear();
    element(by.id("p_supplier_id")).sendKeys(p_supplier_id);
  },

  setItemId: function(pd_item_id) {
    element(by.id("pd_item_id")).click();
    element(by.id("pd_item_id")).clear();
    element(by.id("pd_item_id")).sendKeys(pd_item_id);
  },

  setQty: function(pd_qty) {
    element(by.id("pd_qty")).click();
    element(by.id("pd_qty")).clear();
    element(by.id("pd_qty")).sendKeys(pd_qty);
  },

  setUnitPrice: function(pd_unit_price) {
    element(by.id("pd_unit_price")).click();
    element(by.id("pd_unit_price")).clear();
    element(by.id("pd_unit_price")).sendKeys(pd_unit_price);
  },

  setDiscountRs: function(pd_discount_rs) {
    element(by.id("pd_discount_rs")).click();
    element(by.id("pd_discount_rs")).clear();
    element(by.id("pd_discount_rs")).sendKeys(pd_discount_rs);
  },

  setDiscountPercent: function(pd_discount_percent) {
    element(by.id("pd_discount_percent")).click();
    element(by.id("pd_discount_percent")).clear();
    element(by.id("pd_discount_percent")).sendKeys(pd_discount_percent);
  },

  setPrevBal: function(supplier_prev_bal){
    element(by.id("supplier_prev_bal")).click();
    element(by.id("supplier_prev_bal")).clear();
    element(by.id("supplier_prev_bal")).sendKeys(supplier_prev_bal);
  },

  setTotal: function(p_total) {
    element(by.id("p_total")).click();
    element(by.id("p_total")).clear();
    element(by.id("p_total")).sendKeys(p_total);
  },

  setBalance: function(p_balance) {
    element(by.id("p_balance")).click();
    element(by.id("p_balance")).clear();
    element(by.id("p_balance")).sendKeys(p_balance);
  },

  setCreditChange: function(p_credit_change) {
    element(by.id("p_credit_change")).click();
    element(by.id("p_credit_change")).clear();
    element(by.id("p_credit_change")).sendKeys(p_credit_change);
  },

  setTotalDiscountRs: function(p_total_discount_rs) {
    element(by.id("p_total_discount_rs")).click();
    element(by.id("p_total_discount_rs")).clear();
    element(by.id("p_total_discount_rs")).sendKeys(p_total_discount_rs);
  },

  setTotalDiscountPercent: function(p_total_discount_percent) {
    element(by.id("p_total_discount_percent")).click();
    element(by.id("p_total_discount_percent")).clear();
    element(by.id("p_total_discount_percent")).sendKeys(p_total_discount_percent);
  },

  setPaid: function(p_paid) {
    element(by.id("p_paid")).click();
    element(by.id("p_paid")).clear();
    element(by.id("p_paid")).sendKeys(p_paid);
  },

  setChangeToReturn: function(p_change_to_return) {
    element(by.id("p_change_to_return")).click();
    element(by.id("p_change_to_return")).clear();
    element(by.id("p_change_to_return")).sendKeys(p_change_to_return);
  },

  hitSubmitButton : function () {
      element(by.id('purchcesubmit')).click();
  },

  hitCancelButton : function () {
      element(by.className("btn btn-danger")).click();
  },

  hitDeletButton : function () {
      element(by.id('deleteAllRecords')).click();
  },

  hitAddPaurchasesButton : function () {
      element(by.id("addpaurchases")).click();
  },

  assertions : {
    expectRecordSaved : function () {
        expect(element(by.className("description")).getText()).toBe("Success! Paurchases case added sucessfully.");
    },
    expectDoesNotExist : function () {
        expect(element(by.css('.alert-danger')).getText()).toBe('The user provided does not exist on the system.');
    },
  }
};

module.exports = PaurchasesPage;