var constants = browser.params;

var ExpensesPage = {

  // Gets URL
  get: function() {
    browser.get('');
  },

  setAccountId: function(e_account_id) {
    element(by.id("e_account_id")).click();
    element(by.id("e_account_id")).clear();
    element(by.id("e_account_id")).sendKeys(e_account_id);
  }, 

  setAmount: function(e_amount) {
    element(by.id("e_amount")).click();
    element(by.id("e_amount")).clear();
    element(by.id("e_amount")).sendKeys(e_amount);
  },

  setDate: function(e_date) {
    element(by.id("e_date")).click();
    element(by.id("e_date")).clear();
    element(by.id("e_date")).sendKeys(e_date);
  },

  hitSubmitButton : function () {
      element(by.id('expsubmit')).click();
  },

  hitCancelButton : function () {
      element(by.className("btn btn-danger")).click();
  },

  hitDeletButton : function () {
      element(by.id('deleteAllRecords')).click();
  },

  hitaddexpensesButton : function () {
      element(by.id("addexpenses")).click();
  },

  assertions : {
    expectRecordSaved : function () {
        expect(element(by.className("description")).getText()).toBe("Success! Expenses added sucessfully.");
    },
    expectDoesNotExist : function () {
        expect(element(by.css('.alert-danger')).getText()).toBe('The user provided does not exist on the system.');
    },
  }
};

module.exports = ExpensesPage;