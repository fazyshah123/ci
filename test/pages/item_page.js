var constants = browser.params;

var ItemPage = {

  // Gets URL
  get: function() {
    browser.get('');
  },

  setName: function(i_name) {
    element(by.id("i_name")).click();
    element(by.id("i_name")).clear();
    element(by.id("i_name")).sendKeys(i_name);
  },

  setUnitPrice: function(i_unit_price) {
    element(by.className("i_unit_price")).click();
    element(by.className("i_unit_price")).clear(); 
    element(by.className("i_unit_price")).sendKeys(i_unit_price);
  }, 

  hitSubmitButton : function () {
      element(by.id('item_submit')).click();
  }, 

  hitCancelButton : function () {
      element(by.className("btn btn-danger")).click();
  },

  hitDeletButton : function () {
      element(by.id('deleteAllRecords')).click();
  },

  hitAddInvoiceButton : function () {
      element(by.id("add_item")).click();
  },

  assertions : {
    expectRecordSaved : function () {
        expect(element(by.className("description")).getText()).toBe("Success! Item case added sucessfully.");
    },
    expectDoesNotExist : function () {
        expect(element(by.css('.alert-danger')).getText()).toBe('The user provided does not exist on the system.');
    },
  }
};

module.exports = ItemPage;