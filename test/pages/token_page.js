var constants = browser.params;

var tokenPage = {

  // Gets URL
  get: function() {
    browser.get('');
  },

  setPatientId: function(t_patient_id) {
    element(by.id("t_patient_id")).click();
    element(by.id("t_patient_id")).clear();
    element(by.id("t_patient_id")).sendKeys(t_patient_id);
  },

  setDoctorId: function(t_doctor_id) {
    element(by.id("t_doctor_id")).click();
    element(by.id("t_doctor_id")).clear(); 
    element(by.id("t_doctor_id")).sendKeys(t_doctor_id);
  }, 

  setNumber: function(t_number) {
    element(by.id("t_number")).click();
    element(by.id("t_number")).clear();
    element(by.id("t_number")).sendKeys(t_number);
  },

  hitSubmitButton : function () {
      element(by.id('token_submit')).click();
  },

  hitCancelButton : function () {
      element(by.className("btn btn-danger")).click();
  },

  hitDeletButton : function () {
      element(by.id('deleteAllRecords')).click();
  },

  hitAddTokenButton : function () {
      element(by.id("addtoken")).click();
  },

  assertions : {
    expectRecordSaved : function () {
        expect(element(by.className("description")).getText()).toBe("Success! token added sucessfully.");
    },
    expectDoesNotExist : function () {
        expect(element(by.css('.alert-danger')).getText()).toBe('The user provided does not exist on the system.');
    },
  }
};

module.exports = TokenPage;