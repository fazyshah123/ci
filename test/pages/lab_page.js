var constants = browser.params;

var LadbPage = {

  // Gets URL
  get: function() {
    browser.get('');
  },

  setName: function(lab_name) {
    element(by.id("lab_name")).click();
    element(by.id("lab_name")).clear();
    element(by.id("lab_name")).sendKeys(lab_name);
  },

  setPhone: function(lab_phone) {
    element(by.id("lab_phone")).click();
    element(by.id("lab_phone")).clear();
    element(by.id("lab_phone")).sendKeys(lab_phone);
  }, 

   setContactPerson: function(lab_contact_person) {
    element(by.id("lab_contact_person")).click();
    element(by.id("lab_contact_person")).clear();
    element(by.id("lab_contact_person")).sendKeys(lab_contact_person);
  },

   setEmail: function(lab_email) {
    element(by.id("lab_email")).click();
    element(by.id("lab_email")).clear();
    element(by.id("lab_email")).sendKeys(lab_email);
  },

   setAddress: function(lab_address) { 
    element(by.id("lab_address")).click();
    element(by.id("lab_address")).clear();
    element(by.id("lab_address")).sendKeys(lab_address);
  },

  hitSubmitButton : function () {
      element(by.id('lab_submit')).click();
  },
 
  hitCancelButton : function () {
      element(by.className("btn btn-danger")).click();
  },

  hitDeletButton : function () {
      element(by.id('deleteAllRecords')).click();
  },

  hitAddLabButton : function () {
      element(by.id("add_Lab")).click();
  },

  assertions : {
    expectRecordSaved : function () {
        expect(element(by.className("description")).getText()).toBe("Success! Lab added sucessfully.");
    },
    expectDoesNotExist : function () {
        expect(element(by.css('.alert-danger')).getText()).toBe('The user provided does not exist on the system.');
    },
  }
};

module.exports = LadbPage;