var constants = browser.params;

var BedPatientPage = {

  // Gets URL
  get: function() {
    browser.get('');
  },

  setPatient_id: function(bp_patient_id) {
    eLement(by.id("bp_patient_id")).click();
    element(by.id("bp_patient_id")).clear();
    element(by.id("bp_patient_id")).sendKeys(bp_patient_id);
  },

  setBedId: function(bp_bed_id) {
    element(by.id("bp_bed_id")).click();
    element(by.id("bp_bed_id")).clear();
    element(by.id("bp_bed_id")).sendKeys(bp_bed_id);
  },

  setAllotedBy: function(bp_alloted_by) {
    element(by.id("bp_alloted_by")).click();
    element(by.id("bp_alloted_by")).clear();
    element(by.id("bp_alloted_by")).sendKeys(bp_alloted_by);
  },

  setAdmissionDate: function(bp_admission_date) {
    element(by.id("bp_admission_date")).click();
    element(by.id("bp_admission_date")).clear();
    element(by.id("bp_admission_date")).sendKeys(bp_admission_date);
  },

  setDischargeDate: function(bp_discharge_date) {
    element(by.id("bp_discharge_date")).click();
    element(by.id("bp_discharge_date")).clear();
    element(by.id("bp_discharge_date")).sendKeys(bp_discharge_date);
  },

  setPaymentStatus: function(bp_payment_status) {
    element(by.id("bp_payment_status")).click();
    element(by.id("bp_payment_status")).clear();
    element(by.id("bp_payment_status")).sendKeys(bp_payment_status);
  },

  hitSubmitButton : function () {
      element(by.id('bedpasubmit')).click();
  },

  hitCancelButton : function () {
      element(by.className("btn-danger")).click();
  },

  hitDeleteButton : function () {
      element(by.id('deleteAllRecords')).click();
  },

  hitAddbBedPatientButton : function () {
      element(by.id("add_bedpa")).click();
  },

  assertions : {
    expectRecordSaved : function () {
        expect(element(by.className("description")).getText()).toBe("Success, Bed Patient added successfully.");
    },
    expectDoesNotExist : function () {
        expect(element(by.css('.alert-danger')).getText()).toBe('The user provided does not exist on the system.');
    },
  }
};

module.exports = BedPatientPage;