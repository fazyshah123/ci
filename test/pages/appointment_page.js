var constants = browser.params;

var AppointmentPage = {

  // Gets URL
  get: function() {
    browser.get('');
  },

  setAppointmentPatient: function(appointment_patient_id) {
    element(by.id("appointment_patient_id")).click();
    element(by.id("appointment_patient_id")).sendKeys(appointment_patient_id);
  },

  setAppointmentDate: function(appointment_date) {
    element(by.id("appointment_date")).click();
    element(by.id("appointment_date")).clear(); 
    element(by.id("appointment_date")).sendKeys(appointment_date); 
  },

  setAppointmentStartTime: function(appointment_start_time) {
    element(by.id("appointment_start_time")).click();
    element(by.id("appointment_start_time")).clear();
    element(by.id("appointment_start_time")).sendKeys(appointment_start_time);
  }, 

  setAppointmentEndTime: function(appointment_end_time) {
    element(by.id("appointment_end_time")).click();
    element(by.id("appointment_end_time")).clear();
    element(by.id("appointment_end_time")).sendKeys(appointment_end_time);
  },

  setAppointmentDtrId: function(appointment_dtr_id) {
    element(by.id("appointment_dtr_id")).click();
    element(by.id("appointment_dtr_id")).clear();
    element(by.id("appointment_dtr_id")).sendKeys(appointment_dtr_id);
  },

  setAppointmentProcedureId: function(appointment_procedure_id) {
    element(by.id("appointment_procedure_id")).click();
    element(by.id("appointment_procedure_id")).clear();
    element(by.id("appointment_procedure_id")).sendKeys(appointment_procedure_id);
  },

  // setDescription: function(text) {
  //   browser.switchTo().frame(element(by.css('#cke_appointment_comment iframe')).click().sendKeys(text));
  // },

  hitSubmitButton : function () {
      element(by.id('appointment-submit')).click();
  },

  hitCancelButton : function () {
      element(by.className("btn-danger")).click();
  },

  hitAppointmentButton : function () {
      element(by.id('appointment')).click();
  },

  hitAddAppointmentButton : function () {
      element(by.id("addAppointment")).click();
  },

  assertions : {
    expectRecordSaved : function () {
        expect(element(by.className("description")).getText()).toBe("Success! Appointment added sucessfully.");
    },
    expectDoesNotExist : function () {
        expect(element(by.css('.alert-danger')).getText()).toBe('The user provided does not exist on the system.');
    },
  }
};

module.exports = AppointmentPage;