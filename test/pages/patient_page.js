var constants = browser.params;

var patientPage = {

  // Gets URL
  get: function() {
    browser.get('');
  },

  setPatientName: function(patient_name) {
    element(by.id("patient_name")).click();
    element(by.id("patient_name")).clear();
    element(by.id("patient_name")).sendKeys(patient_name);
  },

  setPhone1: function(patient_phone1) {
    element(by.id("patient_phone1")).click();
    element(by.id("patient_phone1")).clear();
    element(by.id("patient_phone1")).sendKeys(patient_phone1);
  },

  setPhone2: function(patient_phone2) {
    element(by.id("patient_phone2")).click();
    element(by.id("patient_phone2")).clear();
    element(by.id("patient_phone2")).sendKeys(patient_phone2);
  },

  setPhone3: function(patient_phone3) {
    element(by.id("patient_phone3")).click();
    element(by.id("patient_phone3")).clear();
    element(by.id("patient_phone3")).sendKeys(patient_phone3);
  },

  setPatientAge: function(patient_age) {
    element(by.id("patient_age")).click();
    element(by.id("patient_age")).clear();
    element(by.id("patient_age")).sendKeys(patient_age);
  },

  setPatientAddress: function(patient_address) {
    element(by.id("patient_address")).click();
    element(by.id("patient_address")).clear();
    element(by.id("patient_address")).sendKeys(patient_address);
  },

  setPatientGender: function(patient_gender) {
    element(by.id("patient_gender")).click();
    element(by.id("patient_gender")).sendKeys(patient_gender);
  },

  setPatientEmail: function(patient_email) {
    element(by.id("patient_email")).click();
    element(by.id("patient_email")).clear();
    element(by.id("patient_email")).sendKeys(patient_email);
  },

  setPatientBirthDate: function(patient_birth_date) {
    element(by.id("patient_birth_date")).click();
    element(by.id("patient_birth_date")).clear();
    element(by.id("patient_birth_date")).sendKeys(patient_birth_date);
  },

  hitSubmitButton : function () {
      element(by.id('submit')).click();
  },

  hitCancelButton : function () {
      element(by.className("btn-danger")).click();
  },

  hitPatientCountButton : function () {
      element(by.id('patientCount')).click();
  },

  hitAddPatientButton : function () {
      element(by.id("addPatient")).click();
  },

  assertions : {
    expectRecordSaved : function () {
        expect(element(by.className("description")).getText()).toBe("Success, Patient added successfully.");
    },
    expectDoesNotExist : function () {
        expect(element(by.css('.alert-danger')).getText()).toBe('The user provided does not exist on the system.');
    },
  }
};

module.exports = patientPage;