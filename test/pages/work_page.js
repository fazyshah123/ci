var constants = browser.params;
 
var WorkTypePage = {

  // Gets URL
  get: function() {
    browser.get('');
  },

  setName: function(w_name) {
    element(by.id("w_name")).click();
    element(by.id("w_name")).clear();
    element(by.id("w_name")).sendKeys(w_name);
  },

  hitSubmitButton : function () {
    element(by.id('work_submit')).click();
  },

  hitCancelButton : function () {
    element(by.className("btn btn-danger")).click();
  },

  hitDeletButton : function () {
    element(by.id('deleteAllRecords')).click();
  },

  hitAddWorktypeButton : function () {
    element(by.id("add_work")).click();
  },

  assertions : {
    expectRecordSaved : function () {
        expect(element(by.className("description")).getText()).toBe("Success! Work Type added sucessfully.");
    },
    expectDoesNotExist : function () {
        expect(element(by.css('.alert-danger')).getText()).toBe('The user provided does not exist on the system.');
    },
  }
};

module.exports = WorkTypePage; 