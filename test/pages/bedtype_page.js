var constants = browser.params;

var bedtypePage = {

  // Gets URL
  get: function() {
    browser.get('');
  },

  setName: function(bt_name) {
    element(by.id("bt_name")).click();
    element(by.id("bt_name")).clear();
    element(by.id("bt_name")).sendKeys(bt_name);
  }, 

  setPeRDayCharges: function(bt_per_day_charges) {
    element(by.className("bt_per_day_charges")).click();
    element(by.className("bt_per_day_charges")).clear(); 
    element(by.className("bt_per_day_charges")).sendKeys(bt_per_day_charges);
  },                                                                                                                  
  
  setBillingType: function(bt_billing_type) {
    element(by.id("bt_billing_type")).click();
    element(by.id("bt_billing_type")).clear();
    element(by.id("bt_billing_type")).sendKeys(bt_billing_type);
  },
     
  setBillingType: function(bt_billing_type) {
    element(by.id("bt_billing_type")).click();
    element(by.id("bt_billing_type")).clear();
    element(by.id("bt_billing_type")).sendKeys(bt_billing_type);
  },

  hitSubmitButton : function () {
      element(by.id('bedtype_submit')).click();
  },

  hitCancelButton : function () {
      element(by.className("btn btn-danger")).click();
  },

  hitDeletButton : function () {
      element(by.id('deleteAllRecords')).click();
  },

  hitAddBedTypeButton : function () {
      element(by.id("add_bedtype")).click();
  },

  assertions : {
    expectRecordSaved : function () {
        expect(element(by.className("description")).getText()).toBe("Success! Bed Type added sucessfully.");
    },
    expectDoesNotExist : function () {
        expect(element(by.css('.alert-danger')).getText()).toBe('The user provided does not exist on the system.');
    },
  }
};

module.exports = bedtypePage;