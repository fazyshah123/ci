var constants = browser.params;

var ProdcedurePage = {

  // Gets URL
  get: function() {
    browser.get('');
  },

  setName: function(r_name) {
    element(by.id("r_name")).click();
    element(by.id("r_name")).clear();
    element(by.id("r_name")).sendKeys(r_name);
  },

  hitSubmitButton : function () {
      element(by.id('reporttype_submit')).click();
  },

  hitCancelButton : function () {
      element(by.className("btn btn-danger")).click();
  }, 

  hitDeletButton : function () {
      element(by.id('deleteAllRecords')).click();
  },

  hitAddReportTypeButton : function () {
      element(by.id("add_reporttype")).click();
  },

  assertions : {
    expectRecordSaved : function () {
        expect(element(by.className("description")).getText()).toBe("Success! ReportType added sucessfully.");
    },
    expectDoesNotExist : function () {
        expect(element(by.css('.alert-danger')).getText()).toBe('The user provided does not exist on the system.');
    },
  }
};

module.exports = ReportTypePage;