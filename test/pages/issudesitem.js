var constants = browser.params;

var stockrelecesPage = {

  // Gets URL
  get: function() {
    browser.get('');
  },

  setItemId: function(sr_item_id) {
    element(by.id("sr_item_id")).click();
    element(by.id("sr_item_id")).clear();
    element(by.id("sr_item_id")).sendKeys(sr_item_id);
  }, 

  setQty: function(sr_qty) {
    element(by.id("sr_qty")).click();
    element(by.id("sr_qty")).clear(); 
    element(by.id("sr_qty")).sendKeys(sr_qty);
  },

  setIssuedTo: function(sr_issued_to) {
    element(by.id("sr_issued_to")).click();
    element(by.id("sr_issued_to")).clear();
    element(by.id("sr_issued_to")).sendKeys(sr_issued_to);
  },

  hitSubmitButton : function () {
      element(by.id('stockrelsubmit')).click();
  },

  hitCancelButton : function () {
      element(by.className("btn-danger")).click(); 
  },

  hitPatientCountButton : function () {
      element(by.id('deleteAllRecords')).click();
  },

  hitAddPatientButton : function () {
      element(by.id("add_stockrel")).click();
  },

  assertions : {
    expectRecordSaved : function () {
        expect(element(by.className("description")).getText()).toBe("Success, Stock Releces added successfully.");
    },
    expectDoesNotExist : function () {
        expect(element(by.css('.alert-danger')).getText()).toBe('The user provided does not exist on the system.');
    },
  }
};

module.exports = stockrelecesPage;