var constants = browser.params;

var invoicePage = {

  // Gets URL
  get: function() {
    browser.get('');
  },

  setPatientId: function(pi_patient_id) {
    element(by.id("pi_patient_id")).click();
    element(by.id("pi_patient_id")).clear();
    element(by.id("pi_patient_id")).sendKeys(pi_patient_id);
  }, 

  setLabId: function(pi_remarks) {
    element(by.className("pi_remarks")).click();
    element(by.className("pi_remarks")).clear(); 
    element(by.className("pi_remarks")).sendKeys(pi_remarks);
  },

  setDueDate: function(pi_date) {
    element(by.id("pi_date")).click();
    element(by.id("pi_date")).clear();
    element(by.id("pi_date")).sendKeys(pi_date);
  },
     
  setShade: function(pi_payment_method) {
    element(by.id("pi_payment_method")).click();
    element(by.id("pi_payment_method")).clear();
    element(by.id("pi_payment_method")).sendKeys(pi_payment_method);
  },

  hitSubmitButton : function () {
      element(by.id('labtracking_submit')).click();
  },

  hitCancelButton : function () {
      element(by.className("lt_cancel")).click();
  },

  hitDeletButton : function () {
      element(by.id('deleteAllRecords')).click();
  },

  hitAddPatientcaseButton : function () {
      element(by.id("addLabtracking")).click();
  },

  assertions : {
    expectRecordSaved : function () {
        expect(element(by.className("description")).getText()).toBe("Success! ladtracking added sucessfully.");
    },
    expectDoesNotExist : function () {
        expect(element(by.css('.alert-danger')).getText()).toBe('The user provided does not exist on the system.');
    },
    expectLoggedIn: function(ful  lname) {
        expect(element(by.id('welcome-user')).getText()).toBe('Welcome! ' + fullname);
    },
    expectAlertForPasswordReset : function(email) {
        expect(element(by.className("alert-success")).getText()).toBe("Password for " + email + " has been reset. Please check your email for the new password.");
    },
    expectAlertForSuccessfullActivationEmail : function(email) {
        expect(element(by.className("alert-success")).getText()).toBe("An activation link has been sent. Please check your email.");
    }
  }
};

module.exports = LabtrackingPage;