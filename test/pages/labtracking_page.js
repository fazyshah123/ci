var constants = browser.params;

var LabtrackingPage = {

  // Gets URL
  get: function() {
    browser.get(''); 
  },

  setPatientId: function(lt_patient_id) {
    element(by.id("lt_patient_id")).click();
    element(by.id("lt_patient_id")).clear();
    element(by.id("lt_patient_id")).sendKeys(lt_patient_id);
  },

  setLabId: function(lt_lab_id) {
    element(by.className("lt_lab_id")).click();
    element(by.className("lt_lab_id")).clear(); 
    element(by.className("lt_lab_id")).sendKeys(lt_lab_id);
  }, 

  setDueDate: function(lt_due_date) {
    element(by.id("lt_due_date")).click();
    element(by.id("lt_due_date")).clear();
    element(by.id("lt_due_date")).sendKeys(lt_due_date);
  },
     
  setShade: function(lt_shade) {
    element(by.id("lt_shade")).click();
    element(by.id("lt_shade")).clear();
    element(by.id("lt_shade")).sendKeys(lt_shade);
  },

  setWorkId: function(lt_work_id) {
    element(by.id("lt_work_id")).click();
    element(by.id("lt_work_id")).clear();
    element(by.id("lt_work_id")).sendKeys(lt_work_id);
  },

  setWorkTypeId: function(lt_work_type_id) {
    element(by.id("lt_work_type_id")).click();
    element(by.id("lt_work_type_id")).clear();
    element(by.id("lt_work_type_id")).sendKeys(lt_work_type_id);
  },

  hitSubmitButton : function () {
      element(by.id('labtracking_submit')).click();
  },

  hitCancelButton : function () {
      element(by.className("lt_cancel")).click();
  },

  hitDeletButton : function () {
      element(by.id('deleteAllRecords')).click();
  },

  hitAddLabtrackingButton : function () {
      element(by.id("add_Labtracking")).click();
  },

  assertions : {
    expectRecordSaved : function () {
        expect(element(by.className("description")).getText()).toBe("Success! ladtracking added sucessfully.");
    },
    expectDoesNotExist : function () {
        expect(element(by.css('.alert-danger')).getText()).toBe('The user provided does not exist on the system.');
    },
  }
};

module.exports = LabtrackingPage;