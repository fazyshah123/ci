var testData = require('./config/testdata.js'),
    LoginPage = require('./pages/login_page.js'),
    PatientPage = require('./pages/patient_page.js'),
    AppointmentPage = require('./pages/appointment_page.js'),
    PatientCasePage = require('./pages/patientCase_Page.js'),
    LabtrackingPage = require('./pages/labtracking_Page.js'),
    DashboardPage = require('./pages/dashboard_page.js'), 
    Utils = require('./helper/utils.js'),
    Constants = require('./helper/testing_constants.js'),
    waitReady = require('./lib/waitReady.js'),      // waits until element is visible or present.
    waitAbsent = require('./lib/waitAbsent.js');    // waits until element is disappeard.

describe('medical-information-management-systems', function() {

    beforeEach(function() {
        browser.ignoreSynchronization = true;
        browser.get(Constants.baseURL);
    });

    // it('should have a title', function() {
    //     browser.get(Constants.baseURL);
    //     expect(browser.getTitle()).toEqual(Constants.titleBeforeLogin);
    // }); 

    // Login Page Test Cases - Start
    // it("should not login", function() {
    //     LoginPage.setUsername(testData.users.InvalidClinicUser.username);
    //     LoginPage.setUserPassword(testData.users.InvalidClinicUser.password);
    //     LoginPage.hitLoginButton();
    //     browser.sleep(5000);
    //     LoginPage.assertions.expectInvalidCredentials();
    // });
    it("should login", function() {
        LoginPage.setUsername(testData.users.ValidClinicUser.username);
        LoginPage.setUserPassword(testData.users.ValidClinicUser.password);
        LoginPage.hitLoginButton();
        browser.sleep(5000);
        LoginPage.assertions.expectLoggedIn(testData.users.ValidClinicUser.fullname);
    });
    // Login Page Test Cases - End

    // //Patient Page Test Cases - Start
    // it("should add new patient", function() {
    //     PatientPage.hitPatientCountButton();
    //     PatientPage.hitAddPatientButton();
    //     PatientPage.setPatientName(testData.users.ValidPatient.patient_name);
    //     PatientPage.setPhone1(testData.users.ValidPatient.patient_phone1);
    //     PatientPage.setPhone2(testData.users.ValidPatient.patient_phone2);
    //     PatientPage.setPhone3(testData.users.ValidPatient.patient_phone3);
    //     PatientPage.setPatientAge(testData.users.ValidPatient.patient_age);
    //     PatientPage.setPatientAddress(testData.users.ValidPatient.patient_address);
    //     PatientPage.setPatientGender(testData.users.ValidPatient.patient_gender);
    //     PatientPage.setPatientEmail(testData.users.ValidPatient.patient_email);
    //     PatientPage.setPatientBirthDate(testData.users.ValidPatient.patient_birth_date);
    //     PatientPage.hitSubmitButton();
    //     PatientPage.assertions.expectRecordSaved(testData.users.ValidClinicUser.fullname);
    // });
    // // Patient Page Test Cases - End

    // // patient cancel
    // it("should add new patient", function() {
    //     PatientPage.hitPatientCountButton();
    //     PatientPage.hitAddPatientButton();
    //     PatientPage.setPatientName(testData.users.ValidPatient.patient_name);
    //     PatientPage.setpatientEmail(testData.users.ValidPatient.patient_email);
    //     PatientPage.setPhone1(testData.users.ValidPatient.patient_phone1);
    //     PatientPage.setPhone2(testData.users.ValidPatient.patient_phone2);
    //     PatientPage.setPhone3(testData.users.ValidPatient.patient_phone3);
    //     PatientPage.setPatientGender(testData.users.ValidPatient.patient_gender);
    //     PatientPage.setPatientdob(testData.users.ValidPatient.patient_dob);
    //     PatientPage.setPatientAge(testData.users.ValidPatient.patient_age);
    //     PatientPage.PatientAddress(testData.users.ValidPatient.patient_address);
    //     PatientPage.hitSubmitButton();
    //     browser.sleep(5000);
    // }); 
    // // patient cancel

    // Valid Appointment Page Test Cases - Start
    it("should add new Appointment", function(){
        AppointmentPage.hitAppointmentButton();
        AppointmentPage.hitAddAppointmentButton();
        AppointmentPage.setAppointmentPatient(testData.users.ValidAppointment.appointment_patient_id);
        AppointmentPage.setAppointmentDate(testData.users.ValidAppointment.appointment_date);
        AppointmentPage.setAppointmentStartTime(testData.users.ValidAppointment.appointment_start_time);
        AppointmentPage.setAppointmentEndTime(testData.users.ValidAppointment.appointment_end_time);
        AppointmentPage.setAppointmentDtrId(testData.users.ValidAppointment.appointment_dtr_id);
        AppointmentPage.setAppointmentProcedureId(testData.users.ValidAppointment.appointment_procedure_id);
        AppointmentPage.hitSubmitButton();
        browser.sleep(5000);
    });
    // Valid Appointment Page Test Cases - end

    // // Invalid Appointment Page Test Cases - Start
    // it("should add new Appointment", function(){
    //     AppointmentPage.hitAppointmentButton();
    //     AppointmentPage.hitAddAppointmentButton();
    //     AppointmentPage.setAppointmentPatient(testData.users.InvalidAppointment1.appointment_patient_id);
    //     AppointmentPage.setAppointmentDate(testData.users.InvalidAppointment1.appointment_date);
    //     AppointmentPage.setAppointmentStartTime(testData.users.InvalidAppointment1.appointment_start_time);
    //     AppointmentPage.setAppointmentEndTime(testData.users.InvalidAppointment1.appointment_end_time);
    //     AppointmentPage.setAppointmentDtrId(testData.users.InvalidAppointment1.appointment_dtr_id);
    //     AppointmentPage.setAppointmentProcedureId(testData.users.InvalidAppointment1.appointment_procedure_id);
    //     AppointmentPage.hitSubmitButton();
    //     browser.sleep(5000);
    // });
    // // Invalid Appointment Page Test Cases - End

    // // Valid PatientCase Page Test Cases - Start
    // it("should add new PatientCase", function(){
    //     PatientCasePage.hitappointmentButton();
    //     PatientCasePage.hitaddappointmentButton();
    //     PatientCasePage.setCaseNumber(testData.users.ValidPatientCase.pc_case_number);
    //     PatientCasePage.setRoomNumber(testData.users.ValidPatientCase.pc_room_number);
    //     PatientCasePage.setCharges(testData.users.ValidPatientCase.pc_charges);
    //     PatientCasePage.setAddPatient(testData.users.ValidPatientCase.Add_Patient);
    //     PatientCasePage.setRelativeId(testData.users.ValidPatientCase.pc_relative_id);
    //     PatientCasePage.setRelativeName(testData.users.ValidPatientCase.pc_relative_name);
    //     PatientCasePage.setRelativeCnic(testData.users.ValidPatientCase.pc_relative_cnic);
    //     PatientCasePage.setRelativPhone(testData.users.ValidPatientCase.pc_relative_phone);
    //     PatientCasePage.setAdmissionDate(testData.users.ValidPatientCase.pc_admission_date);
    //     PatientCasePage.setAdmissionTime(testData.users.ValidPatientCase.pc_admission_time);
    //     PatientCasePage.setDtr_Id(testData.users.ValidPatientCase.pc_dtr_id);
    //     PatientCasePage.setDischargeDate(testData.users.ValidPatientCase.pc_discharge_date);
    //     PatientCasePage.setDischarge_Time(testData.users.ValidPatientCase.pc_discharge_time);
    //     PatientCasePage.setAllotedBy(testData.users.ValidPatientCase.pc_alloted_by);
    //     PatientCasePage.hitSubmitButton();
    //     browser.sleep(5000);
    // });
    // // Valid PatientCase Page Test Cases - end

    // // Invalid PatientCase Page Test Cases - Start
    // it("should add new PatientCase", function(){
    //     PatientCasePage.hitappointmentButton();
    //     PatientCasePage.hitaddappointmentButton();
    //     PatientCasePage.setCaseNumber(testData.users.InvalidPatientCase1.pc_case_number);
    //     PatientCasePage.setRoomNumber(testData.users.InvalidPatientCase1.pc_room_number);
    //     PatientCasePage.setCharges(testData.users.InvalidPatientCase1.pc_charges);
    //     PatientCasePage.setAddPatient(testData.users.InvalidPatientCase1.Add_Patient);
    //     PatientCasePage.setRelativeId(testData.users.InvalidPatientCase1.pc_relative_id);
    //     PatientCasePage.setRelativeName(testData.users.InvalidPatientCase1.pc_relative_name);
    //     PatientCasePage.setRelativeCnic(testData.users.InvalidPatientCase1.pc_relative_cnic);
    //     PatientCasePage.setRelativPhone(testData.users.InvalidPatientCase1.pc_relative_phone);
    //     PatientCasePage.setAdmissionDate(testData.users.InvalidPatientCase1.pc_admission_date);
    //     PatientCasePage.setAdmissionTime(testData.users.InvalidPatientCase1.pc_admission_time);
    //     PatientCasePage.setDtr_Id(testData.users.InvalidPatientCase1.pc_dtr_id);
    //     PatientCasePage.setDischargeDate(testData.users.InvalidPatientCase1.pc_discharge_date);
    //     PatientCasePage.setDischarge_Time(testData.users.InvalidPatientCase1.pc_discharge_time);
    //     PatientCasePage.setAllotedBy(testData.users.InvalidPatientCase1.pc_alloted_by);
    //     PatientCasePage.hitSubmitButton();
    //     browser.sleep(5000);
    // });
    // // Invalid Labtracking Page Test Cases - End

    // // Valid Labtracking Page Test Cases - Start
    // it("should add new Labtracking", function(){
    //     LabtrackingPage.hitappointmentButton();
    //     LabtrackingPage.hitaddappointmentButton();
    //     LabtrackingPage.setPatientId(testData.users.Validladtracking.lt_patient_id);
    //     LabtrackingPage.setLabId(testData.users.Validladtracking.lt_lab_id);
    //     LabtrackingPage.setDueDate(testData.users.Validladtracking.lt_due_date);
    //     LabtrackingPage.setShade(testData.users.Validladtracking.lt_shade);
    //     LabtrackingPage.setWorkId(testData.users.Validladtracking.lt_work_id);
    //     LabtrackingPage.setWorkTypeId(testData.users.Validladtracking.lt_work_type_id);
    //     LabtrackingPage.hitSubmitButton();
    //     browser.sleep(5000);
    // });
    // // Valid Labtracking Page Test Cases - end

    // // Invalid Labtracking Page Test Cases - Start
    // it("should add new Labtracking", function(){
    //     LabtrackingPage.hitappointmentButton();
    //     LabtrackingPage.hitaddappointmentButton();
    //     LabtrackingPage.setPatientId(testData.users.Invalidlabtracking1.lt_patient_id);
    //     LabtrackingPage.setLabId(testData.users.Invalidlabtracking1.lt_lab_id);
    //     LabtrackingPage.setDueDate(testData.users.Invalidlabtracking1.lt_due_date);
    //     LabtrackingPage.setShade(testData.users.Invalidlabtracking1.lt_shade);
    //     LabtrackingPage.setWorkId(testData.users.Invalidlabtracking1.lt_work_id);
    //     LabtrackingPage.setWorkTypeId(testData.users.Invalidlabtracking1.lt_work_type_id);
    //     LabtrackingPage.hitSubmitButton();
    //     browser.sleep(5000);
    // });
    // // Invalid Labtracking Page Test Cases - End


});