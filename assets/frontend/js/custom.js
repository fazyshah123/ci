$(document).ready(function() {
	$(".forgotLink").click(function(e) {
		e.preventDefault();
		var password = $("#password").val();
		var username = $("#username").val();
		if (username === '' || password === '') {
			$("#username").addClass('field_incorrect');
			$("#password").addClass('field_incorrect');
			$(".message").text('Please enter sign in fields!');
			$(".altRow").removeClass('hidden').addClass('animated shake');
			return;
		}
		$(".forgotForm").removeClass("hidden");
		$(".loginForm").addClass("hidden");
	});

	$("#forgot-form-submit").click(function(e) {
		e.preventDefault();
		var email = $("#forgot-email").val();
		if (email !== "" && isValidEmailAddress(email)) {
			$.post('home/forgotEmail', {
				'email': email
			}, function(data) {
				$(".message").text('Please check your email for new password!');
				$(".altRow").removeClass('hidden').addClass('animated shake');
			}, 'json').fail(function(data) {
				$(".message").text(data.message);
				$(".altRow").removeClass('hidden').addClass('animated shake');
			});
		} else {
			$("#forgot-email").addClass('field_incorrect');
			$(".message").text('Please enter a valid email!');
			$(".altRow").removeClass('hidden').addClass('animated shake');
			return;
		}
	});

	$(".loginBack").click(function(e) {
		e.preventDefault();
		$(".forgotForm").addClass("hidden");
		$(".loginForm").removeClass("hidden");
	});

	$("#register-form-submit").click(function(e) {
		e.preventDefault();
		var name = $("#register-form-name").val();
		var email = $("#register-form-email").val();
		var password = $("#register-form-password").val();
		var repassword = $("#register-form-repassword").val();
		var username = $("#register-form-username").val();
		var phone = $("#register-form-phone").val();

		if (name === '' || 
			email === '' || 
			password === '' || 
			repassword === '' || 
			username === '' ||
			phone === ''
		) {
				$("#register-form-username").addClass('field_incorrect');
				$("#register-form-password").addClass('field_incorrect');
				$("#register-form-repassword").addClass('field_incorrect');
				$("#register-form-phone").addClass('field_incorrect');
				$("#register-form-email").addClass('field_incorrect');
				$("#register-form-name").addClass('field_incorrect');
				$(".message, #reg_results").text('Please fill in the required fields!');
				$(".alertrow").removeClass('hidden').addClass('animated shake error');
				return;
		}

		if (
				password.length < 8 ||
				repassword.length < 8
		) {
			$("#register-form-password").addClass('field_incorrect');
			$("#register-form-repassword").addClass('field_incorrect');
			$(".message, #reg_results").text('Password length should be greater than or equal to 8!');
			$(".alertrow").removeClass('hidden').addClass('animated shake error');
			return;
		}
		$(".registration_form_div").addClass('hidden');
		$(".loader").removeClass('hidden');
		$.post('home/signup', {
			name: name,
			email: email,
			password: password,
			repassword: repassword,
			username: username,
			phone: phone
		}, function(data) {
			$(".registration_form_div").removeClass('hidden');
			$(".loader").addClass('hidden');
			$(".message").text(data.message);
			$(".alertrow").removeClass('hidden error').addClass('animated shake success');
			$(".alert").removeClass('alert-danger error').addClass('alert-success animated fadeIn success');
			$(".alert strong").text('Success!');
			$("#register-form-name").val('').addClass('field_default');
			$("#register-form-email").val('').addClass('field_default');
			$("#register-form-password").val('').addClass('field_default');
			$("#register-form-repassword").val('').addClass('field_default');
			$("#register-form-username").val('').addClass('field_default');
			$("#register-form-phone").val('').addClass('field_default');
		}, 'json').fail(function(response) {
			$(".loader").addClass('hidden');
			$(".registration_form_div").removeClass('hidden');
			message = JSON.parse(response.responseText);
			$(".message, #reg_results").text(message.failure);
			$(".alertrow").removeClass('hidden').addClass('animated shake error');
			
		});
	});

	$("#register-form-name").blur(function() {
		if ( $("#register-form-name").val() != "") {
			$("#register-form-name").addClass('field_correct');
		}
	});

	$("#register-form-phone").blur(function() {
		if ( $("#register-form-phone").val() == "") {
			$("#register-form-phone").addClass('field_incorrect');
			$(".alertrow").removeClass('hidden').addClass('animated shake error');
		}
	});

	$("#register-form-password").blur(function() {
		if ( $("#register-form-password").val() < 8) {
			$("#register-form-password").addClass('field_incorrect');
			$(".message, #reg_results").text("Password should be greater than 8!");
			$(".alertrow").removeClass('hidden').addClass('animated shake error');
		}
	});

	$("#register-form-repassword").blur(function() {
		if ( $("#register-form-repassword").val() < 8) {
			$("#register-form-repassword").addClass('field_incorrect');
			$(".message, #reg_results").text("Password should be greater than 8!");
			$(".alertrow").removeClass('hidden').addClass('animated shake error');
		}
	});

	$(".close").click(function() {
		$('.alertrow').addClass('hidden animated fadeOutDownBig');
	});

	$("#register-form-username").change(function(){
		var username = $("#register-form-username").val();
		if (username !== "") {
			$.post('home/checkUsername', {
				username: username
			}, function(response) {
				$("#register-form-username").addClass('field_correct');
				$(".alertrow").addClass('hidden');
			}, 'json').fail(function(response) {
				message = JSON.parse(response.responseText);
				$("#register-form-username").addClass('field_incorrect');
				$(".message, #reg_results").text(message.failure);
				$(".alertrow").removeClass('hidden').addClass('animated shake error');
			});
		}
	});

	$("#register-form-email").change(function(){
		var email = $("#register-form-email").val();
		if (email !== "") {

			if (!isValidEmailAddress(email)) {
				$("#register-form-email").addClass('field_incorrect');
				$(".message, #reg_results").text('Invalid email address!');
				$(".alertrow").removeClass('hidden').addClass('animated shake error');
				return;
			}

			$.post('home/checkEmail', {
				email: email
			}, function(response) {
				$("#register-form-email").addClass('field_correct');
				$(".alertrow").addClass('hidden');
			}, 'json').fail(function(response) {
				message = JSON.parse(response.responseText);
				$("#register-form-email").addClass('field_incorrect');
				$(".message, #reg_results").text(message.failure);
				$(".alertrow").removeClass('hidden').addClass('animated shake error');
			});
		}
	});

	$("#register-form-password").change(function(){
		var password = $("#register-form-password").val();
		if (password.length < 8 || $("#register-form-repassword").val() < 8) {
			$("#register-form-password").addClass('field_incorrect');
			$(".message, #reg_results").text('Password length should be greater than 8!');
			$(".alertrow").removeClass('hidden').addClass('animated shake error');
			return;
		}

		if ($("#register-form-repassword").val() !== password) {
			$("#register-form-password").addClass('field_incorrect');
			$("#register-form-repassword").addClass('field_incorrect');
			$(".message, #reg_results").text('Password do not match!');
			$(".alertrow").removeClass('hidden').addClass('animated shake error');
		} else {
			$("#register-form-password").addClass('field_correct');
			$("#register-form-repassword").addClass('field_correct');
			$(".alertrow").addClass('hidden');
		}
	});

	$("#register-form-repassword").change(function(){
		var repassword = $("#register-form-repassword").val();
		if (repassword.length < 8 || $("#register-form-password").val() < 8) {
			$("#register-form-repassword").addClass('field_incorrect');
			$(".message, #reg_results").text('Password length should be greater than 8!');
			$(".alertrow").removeClass('hidden').addClass('animated shake error');
			return;
		} else {
			$("#register-form-password").addClass('field_correct');
			$("#register-form-repassword").addClass('field_correct');
			$(".alertrow").addClass('hidden');
		}

		if ($("#register-form-password").val() !== repassword) {
			$("#register-form-password").addClass('field_incorrect');
			$("#register-form-repassword").addClass('field_incorrect');
			$(".message, #reg_results").text('Password do not match!');
			$(".alertrow").removeClass('hidden').addClass('animated shake error');
		} else {
			$("#register-form-password").addClass('field_correct');
			$("#register-form-repassword").addClass('field_correct');
			$(".alertrow").addClass('hidden');
		}
	});
	$(".add-to-cart").click(function(e) {
		e.preventDefault();
		var productId = $(".add-to-cart").data('prod-id'),
			output = "";
		$.post(base_url+'home/addtocart/'+productId,{}, function(data) {
			generateItems(data);
			toastr.options = {"positionClass": "toast-bottom-right"};
			toastr['success'](data.msg);
		}, 'json').fail(function(data){
			toastr.options = {"positionClass": "toast-bottom-right"};
            toastr['error'](data.msg);
		});
	});
});

function isValidEmailAddress(emailAddress) {
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test(emailAddress);
};

function getCartItems() {
	$.get(base_url+'home/getCartItems',{}, function(data) {
		generateItems(data);
	}, 'json').fail(function(data){
		toastr.options = {"positionClass": "toast-bottom-right"};
        toastr['error'](data.msg);
	});
}
function generateItems(data) {
	var output = '';
	if (data) {
		var totalPrice = 0;
		$.each(data.items, function(index, value) {
			output += "<div class=\"top-cart-item clearfix\">\
					   <div class=\"top-cart-item-image\">\
	                   <a href=\"#\"><img src=\""+base_url+'assets/frontend/images/products/'+JSON.parse(value.prod_images_json).product_images[0]+"\"\
	                   alt=\"Blue Round-Neck Tshirt\"/></a>\
	                   </div>\
	                   <div class=\"top-cart-item-desc\">\
	                   <a href=\"#\" id=\"top-cart-desc\">"+value.prod_name+"</a>\
	                   <span class=\"top-cart-item-price\"> $"+value.prod_price+"</span>\
	                   </div></div>";
	        totalPrice += parseFloat(value.prod_price);
		});
	}
	$(".top-checkout-price").text('$ ' + totalPrice);
	$(".top-cart-items").html(output);
}
getCartItems();