function renderTimelineItems(data) {
	if (data.timeline_items.length > 0) {
		$.each(data.timeline_items, function(k, v) {
			var output = '';
			if(v.pa_message == 'Patient created.') {
				output += '\
						<div class="feed-item feed-item-idea">\
							<div class="feed-icon">\
								<i class="fa fa-lightbulb-o"></i>\
							</div>\
							<div class="feed-subject">\
								<p><strong>'+v.pa_message+'</strong></p>\
							</div>\
							<div class="feed-actions">\
								<a href="javascript:;" class="pull-left"><i class="fa fa-user"></i> '+v.full_name+'</a>\
								<a href="javascript:;" class="pull-right"><i class="fa fa-clock-o"></i> '+moment(v.pa_added).fromNow()+'</a>\
							</div>\
						</div>';
			}
			if(v.pa_message.includes('Information has been updated')) {
				output += '\
						<div class="feed-item feed-item-idea">\
							<div class="feed-icon">\
								<i class="fa fa-pencil"></i>\
							</div>\
							<div class="feed-subject">\
								<p><strong>'+v.pa_message+'</strong></p>\
							</div>\
							<div class="feed-actions">\
								<a href="javascript:;" class="pull-left"><i class="fa fa-user"></i> '+v.full_name+'</a>\
								<a href="javascript:;" class="pull-right"><i class="fa fa-clock-o"></i> '+moment(v.pa_added).fromNow()+'</a>\
							</div>\
						</div>';
			}
			if(v.pa_message.includes('New case has been created')) {
				output += '\
						<div class="feed-item feed-item-idea">\
							<div class="feed-icon">\
								<i class="fa fa-briefcase"></i>\
							</div>\
							<div class="feed-subject">\
								<p><strong>'+v.pa_message+'</strong></p>\
							</div>\
							<div class="feed-actions">\
								<a href="javascript:;" class="pull-left"><i class="fa fa-user"></i> '+v.full_name+'</a>\
								<a href="javascript:;" class="pull-right"><i class="fa fa-clock-o"></i> '+moment(v.pa_added).fromNow()+'</a>\
							</div>\
						</div>';
			}
			if(v.pa_message.includes('New lab work ordered')) {
				output += '\
						<div class="feed-item feed-item-image">\
							<div class="feed-icon">\
								<i class="fa fa-building-o"></i>\
							</div>\
							<div class="feed-subject">\
								<p><strong>'+v.pa_message+'</strong></p>\
							</div>\
							<div class="feed-actions">\
								<a href="javascript:;" class="pull-left"><i class="fa fa-user"></i> '+v.full_name+'</a>\
								<a href="javascript:;" class="pull-right"><i class="fa fa-clock-o"></i> '+moment(v.pa_added).fromNow()+'</a>\
							</div>\
						</div>';
			}
			if(v.pa_message.includes('New appointment')) {
				output += '\
						<div class="feed-item feed-item-question">\
							<div class="feed-icon">\
								<i class="fa fa-calendar"></i>\
							</div>\
							<div class="feed-subject">\
								<p><strong>'+v.pa_message+'</strong></p>\
							</div>\
							<div class="feed-actions">\
								<a href="javascript:;" class="pull-left"><i class="fa fa-user"></i> '+v.full_name+'</a>\
								<a href="javascript:;" class="pull-right"><i class="fa fa-clock-o"></i> '+moment(v.pa_added).fromNow()+'</a>\
							</div>\
						</div>';
			}
			if(v.pa_message.includes('New report added')) {
				output += '\
						<div class="feed-item feed-item-image">\
							<div class="feed-icon">\
								<i class="fa fa-file"></i>\
							</div>\
							<div class="feed-subject">\
								<p><strong>'+v.pa_message+'</strong></p>\
							</div>\
							<div class="feed-actions">\
								<a href="javascript:;" class="pull-left"><i class="fa fa-user"></i> '+v.full_name+'</a>\
								<a href="javascript:;" class="pull-right"><i class="fa fa-clock-o"></i> '+moment(v.pa_added).fromNow()+'</a>\
							</div>\
						</div>';
			}
			if(v.pa_message.includes('Appointment rescheduled')) {
				output += '\
						<div class="feed-item feed-item-question">\
							<div class="feed-icon">\
								<i class="fa fa-calendar"></i>\
							</div>\
							<div class="feed-subject">\
								<p><strong>'+v.pa_message+'</strong></p>\
							</div>\
							<div class="feed-actions">\
								<a href="javascript:;" class="pull-left"><i class="fa fa-user"></i> '+v.full_name+'</a>\
								<a href="javascript:;" class="pull-right"><i class="fa fa-clock-o"></i> '+moment(v.pa_added).fromNow()+'</a>\
							</div>\
						</div>';
			}
			if(v.pa_message.includes('Appointment canceled')) {
				output += '\
						<div class="feed-item feed-item-question">\
							<div class="feed-icon">\
								<i class="fa fa-calendar"></i>\
							</div>\
							<div class="feed-subject">\
								<p><strong>'+v.pa_message+'</strong></p>\
							</div>\
							<div class="feed-actions">\
								<a href="javascript:;" class="pull-left"><i class="fa fa-user"></i> '+v.full_name+'</a>\
								<a href="javascript:;" class="pull-right"><i class="fa fa-clock-o"></i> '+moment(v.pa_added).fromNow()+'</a>\
							</div>\
						</div>';
			}
			$(".timeline_items").append(output);
		});
		if (data.remaining_item_count>0) {
			$("#input_id").val(data.timeline_items[data.timeline_items.length-1].pa_added);
		} else {
			$(".timeline_show_more_link").addClass("hidden");
		}
	}
}
function getTimelineItems(item_id='0', patient_id) {
	$.post(ADMIN_URL + 'patients/getPatientTimelineItems', {
		item_id: item_id,
		patient_id: patient_id,
	}, function(data) {
		renderTimelineItems(data);
	}, "json");
}
$(document).ready(function() {
	$("#show_more_items").on("click", function(e) {
		e.preventDefault();
		var activity_id = $("#input_id").val();
		getTimelineItems(activity_id, patient_id);
	});

	getTimelineItems('0', patient_id);

    $("#add_item_row").on("click", function(e) {
        e.preventDefault();
        var item_rows_count = $(".item").length;
        var new_row_id = ++item_rows_count;
        $("#total_items").val(new_row_id);
        var new_row = '\
                	<div class="item">\
		                <div class="form-group">\
			                <div class="row" style="height: 60px;">\
		            			<label style="padding: 0px 15px;" for="pf_file-'+new_row_id+'" class="control-label" id="file_lbl-'+new_row_id+'">Document '+new_row_id+':</label>\
		            			<div class="clearfix"></div>\
				                <div class="col-md-10">\
				                	<input type="file" name="pf_file-'+new_row_id+'" id="pf_file-'+new_row_id+'" class="form-control"/>\
				                </div>\
				                <div class="col-md-2">\
				                	<a href="javascript:;" style="min-height: 15px !important;padding: 3px !important;min-width: 15px !important;width: 30px;color: white;padding-top: 5px;" id="remove-'+new_row_id+'" data-item="'+new_row_id+'" class="btn btn-danger ri pull-left"><i class="fa fa-times"></i></a>\
				                </div>\
			                </div>\
			                <div class="row" style="height: 60px;">\
		            			<label style="padding: 0px 15px;" for="pf_file-'+new_row_id+'" class="control-label" id="reporttype_lbl-'+new_row_id+'">Report Type '+new_row_id+':</label>\
		            			<div class="clearfix"></div>\
				                <div class="col-md-10">\
				                	<select name="pf_reporttype-'+new_row_id+'" id="pf_reporttype-'+new_row_id+'" class="form-control">\
				                		'+reportTypeList+'\
				                	</select>\
				                </div>\
			                </div>\
			                <div class="row" style="height: 60px;">\
		            			<label style="padding: 0px 15px;" for="pf_description-'+new_row_id+'" class="control-label" id="description_lbl-'+new_row_id+'">Description '+new_row_id+':</label>\
		            			<div class="clearfix"></div>\
				                <div class="col-md-10">\
				                	<textarea name="pf_description-'+new_row_id+'" id="pf_description-'+new_row_id+'" class="form-control" rows="2"></textarea>\
				                </div>\
			                </div>\
		            	</div>\
		            </div>';

        $(".items").append(new_row);

        $(".ri").unbind("click").bind("click", function(e) {
            e.preventDefault();
            var item = $(this).data('item');

            var total_items = $(".item").length;

            $(this).closest('.item').remove();

            for (var i = item+1; i <= total_items; i++) {
                $("#pf_file-"+i).attr('name', 'pf_file-'+(i-1)).attr('data-item', (i-1)).attr('id', 'pf_file-'+(i-1));
                $("#file_lbl-"+i).text("File "+(i-1)).attr('id', 'file_lbl-'+(i-1));
                $("#reporttype_lbl-"+i).text("Report Type "+(i-1)).attr('id', 'reporttype_lbl-'+(i-1));
                $("#description_lbl-"+i).text("Description "+(i-1)).attr('id', 'description_lbl-'+(i-1));
                $("#pf_reporttype-"+i).attr('name', 'pf_reporttype-'+(i-1)).attr('data-item', (i-1)).attr('id', 'pf_reporttype-'+(i-1));
                $("#pf_description-"+i).attr('name', 'pf_description-'+(i-1)).attr('data-item', (i-1)).attr('id', 'pf_description-'+(i-1));
                $("#remove-"+i).attr('id', 'remove-'+(i-1)).attr('data-item', (i-1));
                $("#lblItem-"+i).attr('for', 'pf_file-'+(i-1)).text('File '+(i-1)).attr('id', 'lblItem-'+(i-1));
            }
            $("#total_items").val(--new_row_id);
        });
    });
});
$("#deleteLink").on('click',function(e){
	e.preventDefault();
	var pf_reporttype = $("#pf_reporttype-1").val();

	if ( pf_reporttype == 'Select') {
		alert('Please enter Report Type');
		return;
	}
	$("#page_form").submit();
});