function clearBirthRecordForm() {
	$("#pcb_weight").val('');
	$("#pcb_rbs").val('');
	$("#pcb_sex").val('Male');
	$("#pcb_obs_duration").val('');
	$("#pcb_section").val('Yes');
	$("#pcb_nebulization").val('Yes');
	$("#pcb_temperature").val('');
	$("#pcb_2saturation").val('');
	$("#pcb_other_details").val('');
	$("#pcb_respiration_rate").val('');
	$("#pcb_birth_date").val('');
	$("#pcb_birth_time").val('');
	$("#pcb_heart_rate").val('');
	$("#pcb_stomach_wash").val('Yes');
	$("#pcb_receiver_name").val('');
	$("#pcb_receiver_cnic").val('');
	$("#pcb_receiver_phone").val('');
}
function clearOxygenForm() {
	$("#oi_note").val('');
	$("#oi_date").val('');
	$("#oi_start_time").val('12:00 AM');
	$("#oi_end_time").val('12:00 AM');
	$("#oi_dtr_id").select2("val", 'Select');
}
function clearVisitForm() {
	$("#pvl_note").val('');
	$("#pvl_visit_time").val('12:00 AM');
	$("#pvl_visit_date").val('');
	$("#pvl_dtr_id").select2("val", 'Select');
}
function clearExaminationForm() {
	$("#pce_note").val('');
	$("#pce_visit_time").val('12:00 AM');
	$("#pce_visit_date").val('');
	$("#pce_dtr_id").select2("val", 'Select');
}
function clearOperationRecordForm() {
	$("#por_provisional_diagnosis").val('');
	$("#por_assistant_id").select2("val", 'Select');
	$("#por_anaesthetist_id").select2("val", 'Select');
	$("#por_surgeon_id").select2("val", 'Select');
	$("#por_visit_date").val('');
	$("#por_visit_time").val('12:00 AM');
	$("#por_indication_of_surgery").val('');
	$("#por_procedure_id").select2("val", 'Select');
}
function clearSurgicalNoteForm() {
	$("#psn_blood_loss").val('');
	$("#psn_surgical_notes").val('');
	$("#psn_date").val('');
	$("#psn_time").val('12:00 AM');
	$("#psn_start_time").val('12:00 AM');
	$("#psn_end_time").val('12:00 AM');
}
function renderTimelineItems(data) {
	if (data.timeline_items.length > 0) {
		$.each(data.timeline_items, function(k, v) {
			var output = '';
			if(v.pa_message.includes('visited patient at')) {
				output += '\
						<div class="feed-item feed-item-purple">\
							<div class="feed-icon">\
								<i class="fa fa-share"></i>\
							</div>\
							<div class="feed-subject">\
								<p><strong>'+v.pa_message+'</strong></p>\
							</div>\
							<div class="feed-actions">\
								<a href="javascript:;" class="pull-left"><i class="fa fa-user"></i> '+v.full_name+'</a>\
								<a href="javascript:;" class="pull-right"><i class="fa fa-clock-o"></i> '+moment(v.pa_added).fromNow()+'</a>\
							</div>\
						</div>';
			}
			if(v.pa_message.includes('New case has been created')) {
				output += '\
						<div class="feed-item feed-item-idea">\
							<div class="feed-icon">\
								<i class="fa fa-briefcase"></i>\
							</div>\
							<div class="feed-subject">\
								<p><strong>'+v.pa_message+'</strong></p>\
							</div>\
							<div class="feed-actions">\
								<a href="javascript:;" class="pull-left"><i class="fa fa-user"></i> '+v.full_name+'</a>\
								<a href="javascript:;" class="pull-right"><i class="fa fa-clock-o"></i> '+moment(v.pa_added).fromNow()+'</a>\
							</div>\
						</div>';
			}
			if(v.pa_message.includes('New case examination')) {
				output += '\
						<div class="feed-item feed-item-green">\
							<div class="feed-icon">\
								<i class="medical-icon-i-inpatient"></i>\
							</div>\
							<div class="feed-subject">\
								<p><strong>'+v.pa_message+'</strong></p>\
							</div>\
							<div class="feed-actions">\
								<a href="javascript:;" class="pull-left"><i class="fa fa-user"></i> '+v.full_name+'</a>\
								<a href="javascript:;" class="pull-right"><i class="fa fa-clock-o"></i> '+moment(v.pa_added).fromNow()+'</a>\
							</div>\
						</div>';
			}
			if(v.pa_message.includes('New child added.')) {
				output += '\
						<div class="feed-item feed-item-question">\
							<div class="feed-icon">\
								<i class="medical-icon-i-labor-delivery"></i>\
							</div>\
							<div class="feed-subject">\
								<p><strong>'+v.pa_message+'</strong></p>\
							</div>\
							<div class="feed-actions">\
								<a href="javascript:;" class="pull-left"><i class="fa fa-user"></i> '+v.full_name+'</a>\
								<a href="javascript:;" class="pull-right"><i class="fa fa-clock-o"></i> '+moment(v.pa_added).fromNow()+'</a>\
							</div>\
						</div>';
			}
			if(v.pa_message.includes('Surgical note has been created.')) {
				output += '\
						<div class="feed-item feed-item-idea">\
							<div class="feed-icon">\
								<i class="medical-icon-i-alternative-complementary"></i>\
							</div>\
							<div class="feed-subject">\
								<p><strong>'+v.pa_message+'</strong></p>\
							</div>\
							<div class="feed-actions">\
								<a href="javascript:;" class="pull-left"><i class="fa fa-user"></i> '+v.full_name+'</a>\
								<a href="javascript:;" class="pull-right"><i class="fa fa-clock-o"></i> '+moment(v.pa_added).fromNow()+'</a>\
							</div>\
						</div>';
			}
			if(v.pa_message.includes('Operation record created.')) {
				output += '\
						<div class="feed-item feed-item-idea">\
							<div class="feed-icon">\
								<i class="medical-icon-i-alternative-complementary"></i>\
							</div>\
							<div class="feed-subject">\
								<p><strong>'+v.pa_message+'</strong></p>\
							</div>\
							<div class="feed-actions">\
								<a href="javascript:;" class="pull-left"><i class="fa fa-user"></i> '+v.full_name+'</a>\
								<a href="javascript:;" class="pull-right"><i class="fa fa-clock-o"></i> '+moment(v.pa_added).fromNow()+'</a>\
							</div>\
						</div>';
			}
			if(v.pa_message.includes('oxygen inhalation treatment')) {
				output += '\
						<div class="feed-item feed-item-green">\
							<div class="feed-icon">\
								<i class="medical-icon-i-anesthesia"></i>\
							</div>\
							<div class="feed-subject">\
								<p><strong>'+v.pa_message+'</strong></p>\
							</div>\
							<div class="feed-actions">\
								<a href="javascript:;" class="pull-left"><i class="fa fa-user"></i> '+v.full_name+'</a>\
								<a href="javascript:;" class="pull-right"><i class="fa fa-clock-o"></i> '+moment(v.pa_added).fromNow()+'</a>\
							</div>\
						</div>';
			}
			$(".timeline_items").append(output);
		});
		if (data.remaining_item_count>0) {
			$("#input_id").val(data.timeline_items[data.timeline_items.length-1].pa_added);
		} else {
			$(".timeline_show_more_link").addClass("hidden");
		}
	}
}
function getTimelineItems(item_id='0', patient_id) {
	$.post(ADMIN_URL + 'patients/getPatientTimelineItems', {
		item_id: item_id,
		patient_id: patient_id,
	}, function(data) {
		renderTimelineItems(data);
	}, "json");
}
function getPatientVisitLogs(last_visit_log_id, patient_id, case_id) {
	$.post(ADMIN_URL + 'patientvisitlog/getPatientVisitLogs', {
		last_visit_log_id: last_visit_log_id,
		patient_id: patient_id,
		case_id: case_id
	}, function(data) {
		renderVisitLogs(data);
	}, "json");
}
$(document).ready(function() {
	$("#show_more_items").on("click", function(e) {
		e.preventDefault();
		var activity_id = $("#input_id").val();
		getTimelineItems(activity_id, patient_id);
	});

	$("#show_more_visit_log").on("click", function(e) {
		e.preventDefault();
		var last_visit_log_id = $("#last_visit_log_id").val();
		getPatientVisitLogs(last_visit_log_id, patient_id, case_number);
	});

	if (patient_id != undefined) {
		getTimelineItems('0', patient_id);
		getPatientVisitLogs('0', patient_id, case_number);
	}

	$("#openOxygenDialouge, #openMedicalExamDialouge, #openOperationRecordDialouge, #openSurgicalNoteDialouge, #openVisitLogDialouge, #openBirthRecordDialouge").unbind("click").bind("click", function(e) {
		e.preventDefault();
		clearAlertNotification();
	});

	$("#submitVisitLogForm").unbind('click').bind('click', function(e) {
		e.preventDefault();
		var note = $("#pvl_note").val();
		var time = $("#pvl_visit_time").val();
		var date = $("#pvl_visit_date").val();
		var dtr_id = $("#pvl_dtr_id").val();
		var patient_id = $("#patient_id").val();
		var case_id = $("#case_id").val();

		if (date == '' || time == '' || dtr_id == 'Select') {
			var data = {
				alert: 'Error, ',
				responseType: 'alert-danger',
				message: 'Please fill in the required fields!'
			};
			showAlertNotification(data);
			return false;
		}

		$.post(ADMIN_URL + "patientvisitlog/save", {
			pvl_note: note,
			pvl_visit_date: date,
			pvl_visit_time: time,
			pvl_dtr_id: dtr_id,
			pvl_case_id: case_id,
			pvl_patient_id: patient_id
		}, function(data) {
			showAlertNotification(data);
			clearVisitForm();
		}, "json");
	});

	$("#openOxygenSubmit").unbind('click').bind('click', function(e) {
		e.preventDefault();
		var note = $("#oi_note").val();
		var date = $("#oi_date").val();
		var start_time = $("#oi_start_time").val();
		var end_time = $("#oi_end_time").val();
		var dtr_id = $("#oi_dtr_id").val();
		var patient_id = $("#patient_id").val();
		var case_id = $("#case_id").val();

		if (date == '' || start_time == '' || end_time == '' || dtr_id == 'Select') {
			var data = {
				alert: 'Error, ',
				responseType: 'alert-danger',
				message: 'Please fill in the required fields!'
			};
			showAlertNotification(data);
			return false;
		}

		$.post(ADMIN_URL + "patienttreatment/save", {
			pt_note: note,
			pt_date: date,
			pt_start_time: start_time,
			pt_end_time: end_time,
			pt_dtr_id: dtr_id,
			pt_case_id: case_id,
			pt_patient_id: patient_id
		}, function(data) {
			showAlertNotification(data);
			clearOxygenForm();
		}, "json");
	});

	$("#operationRecordSubmit").unbind('click').bind('click', function(e) {
		e.preventDefault();

		var por_provisional_diagnosis = $("#por_provisional_diagnosis").val();
		var por_assistant_id = $("#por_assistant_id").val();
		var por_anaesthetist_id = $("#por_anaesthetist_id").val();
		var por_surgeon_id = $("#por_surgeon_id").val();
		var por_visit_date = $("#por_visit_date").val();
		var por_visit_time = $("#por_visit_time").val();
		var por_procedure_id = $("#por_procedure_id").val();
		var por_indication_of_surgery = $("#por_indication_of_surgery").val();
		var patient_id = $("#patient_id").val();
		var case_id = $("#case_id").val();

		if (por_surgeon_id == 'Select' || por_visit_date == '' || por_visit_time == '' || por_procedure_id == 'Select' || por_anaesthetist_id == 'Select') {
			var data = {
				alert: 'Error, ',
				responseType: 'alert-danger',
				message: 'Please fill in the required fields!'
			};
			showAlertNotification(data);
			return false;
		}

		$.post(ADMIN_URL + "patientoperationrecord/save", {
			por_provisional_diagnosis: por_provisional_diagnosis,
			por_assistant_id: por_assistant_id,
			por_anaesthetist_id: por_anaesthetist_id,
			por_surgeon_id: por_surgeon_id,
			por_visit_date: por_visit_date,
			por_visit_time: por_visit_time,
			por_procedure_id: por_procedure_id,
			por_indication_of_surgery: por_indication_of_surgery,
			por_case_id: case_id,
			por_patient_id: patient_id
		}, function(data) {
			showAlertNotification(data);
			clearOperationRecordForm();
		}, "json");
	});

	$("#sugricalNoteSubmit").unbind('click').bind('click', function(e) {
		e.preventDefault();

		var psn_blood_loss = $("#psn_blood_loss").val();
		var psn_note = $("#psn_surgical_notes").val();
		var psn_date = $("#psn_date").val();
		var psn_time = $("#psn_time").val();
		var psn_start_time = $("#psn_start_time").val();
		var psn_end_time = $("#psn_end_time").val();
		var patient_id = $("#patient_id").val();
		var case_id = $("#case_id").val();

		if (psn_start_time == '' || psn_date == '' || psn_time == '' || psn_end_time == '') {
			var data = {
				alert: 'Error, ',
				responseType: 'alert-danger',
				message: 'Please fill in the required fields!'
			};
			showAlertNotification(data);
			return false;
		}

		$.post(ADMIN_URL + "patientsurgicalnote/save", {
			psn_blood_loss: psn_blood_loss,
			psn_note: psn_note,
			psn_date: psn_date,
			psn_time: psn_time,
			psn_start: psn_start_time,
			psn_finish: psn_end_time,
			psn_case_id: case_id,
			psn_patient_id: patient_id
		}, function(data) {
			showAlertNotification(data);
			clearSurgicalNoteForm();
		}, "json");
	});

	$("#birthRecordSubmit").unbind('click').bind('click', function(e) {
		e.preventDefault();

		var pcb_weight = $("#pcb_weight").val();
		var pcb_rbs = $("#pcb_rbs").val();
		var pcb_sex = $("#pcb_sex").val();
		var pcb_obs_duration = $("#pcb_obs_duration").val();
		var pcb_section = $("#pcb_section").val();
		var pcb_nebulization = $("#pcb_nebulization").val();
		var pcb_temperature = $("#pcb_temperature").val();
		var pcb_2saturation = $("#pcb_2saturation").val();
		var pcb_other_details = $("#pcb_other_details").val();
		var pcb_respiration_rate = $("#pcb_respiration_rate").val();
		var pcb_birth_date = $("#pcb_birth_date").val();
		var pcb_birth_time = $("#pcb_birth_time").val();
		var pcb_heart_rate = $("#pcb_heart_rate").val();
		var pcb_stomach_wash = $("#pcb_stomach_wash").val();
		var pcb_receiver_name = $("#pcb_receiver_name").val();
		var pcb_receiver_cnic = $("#pcb_receiver_cnic").val();
		var pcb_receiver_phone = $("#pcb_receiver_phone").val();
		var patient_id = $("#patient_id").val();
		var case_id = $("#case_id").val();

		if (pcb_weight == '' || pcb_sex == '' || pcb_birth_date == '' || pcb_birth_time == '' || pcb_receiver_name == '' || pcb_receiver_cnic == '' || pcb_receiver_phone == '') {
			var data = {
				alert: 'Error, ',
				responseType: 'alert-danger',
				message: 'Please fill in the required fields!'
			};
			showAlertNotification(data);
			return false;
		}

		$.post(ADMIN_URL + "patientcasebirth/save", {
			pcb_other_details: pcb_other_details,
			pcb_receiver_phone: pcb_receiver_phone,
			pcb_receiver_cnic: pcb_receiver_cnic,
			pcb_rbs: pcb_rbs,
			pcb_weight: pcb_weight,
			pcb_temperature: pcb_temperature,
			pcb_stomach_wash: pcb_stomach_wash,
			pcb_nebulization: pcb_nebulization,
			pcb_sex: pcb_sex,
			pcb_receiver_name: pcb_receiver_name,
			pcb_2saturation: pcb_2saturation,
			pcb_section: pcb_section,
			pcb_birth_time: pcb_birth_time,
			pcb_birth_date: pcb_birth_date,
			pcb_obs_duration: pcb_obs_duration,
			pcb_respiration_rate: pcb_respiration_rate,
			pcb_heart_rate: pcb_heart_rate,
			pcb_case_id: case_id,
			pcb_patient_id: patient_id
		}, function(data) {
			showAlertNotification(data);
			clearBirthRecordForm();
		}, "json");
	});

	$("#medicalSubmit").unbind('click').bind('click', function(e) {
		e.preventDefault();
		var pce_visit_time = $("#pce_visit_time").val();
		var pce_examination = $("#pce_examination").val();
		var pce_visit_date = $("#pce_visit_date").val();
		var pce_dtr_id = $("#pce_dtr_id").val();
		var patient_id = $("#patient_id").val();
		var case_id = $("#case_id").val();

		if (pce_visit_time == '' || pce_dtr_id == 'Select' || pce_visit_date == '') {
			var data = {
				alert: 'Error, ',
				responseType: 'alert-danger',
				message: 'Please fill in the required fields!'
			};
			showAlertNotification(data);
			return false;
		}
		var conditions = [];
		var i = 0;
		$.each($("#medical_form input[type=checkbox]:checked"), function(k, v) {
			conditions[i] = {value: $(v).val(), id: $(v).data('id')};
			i++;
		});

		$.post(ADMIN_URL + "patientcaseexaminations/save", {
			pce_visit_time: pce_visit_time,
			pce_dtr_id: pce_dtr_id,
			pce_visit_date: pce_visit_date,
			pce_note: pce_examination,
			conditions: conditions,
			pce_case_id: case_id,
			pce_patient_id: patient_id
		}, function(data) {
			showAlertNotification(data);
			clearExaminationForm();
		}, "json");
	});
});
function clearAlertNotification() {
	$(".responseText").text('');
	$(".responseType").removeClass('alert-danger alert-success');
	$(".responseMessage").text('');
	$(".alertrow").addClass('hidden');
}
function showAlertNotification(data) {
	$(".responseText").text(data.alert);
	$(".responseType").removeClass().addClass('alert responseType').addClass(data.responseType);
	$(".responseMessage").text(data.message);
	$(".alertrow").removeClass('hidden');
}