function clearAlertNotification() {
	$(".responseText").text('');
	$(".responseType").removeClass('alert-danger alert-success');
	$(".responseMessage").text('');
	$(".alertrow").addClass('hidden');
}
function showAlertNotification(data) {
	$(".responseText").text(data.alert);
	$(".responseType").addClass(data.responseType);
	$(".responseMessage").text(data.message);
	$(".alertrow").removeClass('hidden');
}
function renderStaffAttendance(data) {
	var output = '';
	if (data.length > 0) {
		$.each(data, function(i, v) {
			if (v.ea_attendance == 'A') {
				var color = 'style="background-color: #ffc9c9 !important;"';
			} else {
				var color = '';
			}
			output += '<tr>\
						<td '+color+'>'+v.full_name+'</td>\
						<td '+color+'><span data-toggle="tooltip" title="'+v.ea_absent_reason+'">'+v.ea_attendance+'</span></td>\
						<td '+color+'>'+getDateFixed(v.ea_date)+'</td>\
						<td '+color+'>'+tConvert(v.ea_checkin)+'</td>\
						<td '+color+'>'+tConvert(v.ea_checkout)+'</td>\
					</tr>';
					
		});	
	} else {
		output = '<tr colspan="4"><td></td></tr>';
	}
	$(".attendanceTBody").html(output);
  	$('[data-toggle="tooltip"]').tooltip();
}
function renderExpenses(data) {
	var output = '';
	if (data.length > 0) {
		$.each(data, function(i, v) {
			output += '<tr>\
						<td>'+v.a_name+'</td>\
						<td>'+Encoder.htmlDecode(v.e_description)+'</span></td>\
						<td>'+v.e_amount+' Rs.</td>\
						<td>'+v.full_name+'</td>\
						<td>'+tConvert(v.e_added)+'</td>\
					</tr>';
		});	
	} else {
		output = '<tr colspan="4"><td></td></tr>';
	}
	$(".expenseTBody").html(output);
}
function renderPurchases(data) {
	var output = '';
	if (data.length > 0) {
		$.each(data, function(i, v) {
			output += '<tr>\
						<td>'+v.a_name+'</td>\
						<td>'+Encoder.htmlDecode(v.p_remarks)+'</span></td>\
						<td>'+v.p_total+' Rs.</td>\
						<td>'+v.full_name+'</td>\
						<td>'+getDateFixed(v.p_added) + ' ' + formatAMPM(v.p_added) +'</td>\
					</tr>';
		});	
	} else {
		output = '<tr colspan="4"><td></td></tr>';
	}
	$(".purchaseTBody").html(output);
}
function renderItemsIssued(data) {
	var output = '';
	if (data.length > 0) {
		$.each(data, function(i, v) {
			output += '<tr>\
						<td>'+v.i_name+'</td>\
						<td>'+Encoder.htmlDecode(v.sr_description)+'</span></td>\
						<td>'+v.sr_qty+'</td>\
						<td>'+v.full_name+'</td>\
						<td>'+getDateFixed(v.sr_added) + ' ' + formatAMPM(v.sr_added) +'</td>\
					</tr>';
		});	
	} else {
		output = '<tr colspan="4"><td></td></tr>';
	}
	$(".itemsTBody").html(output);
}
function tConvert (time) {
  // Check correct time format and split into components
  time = time.toString ().match (/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

  if (time.length > 1) { // If time format correct
    time = time.slice (1);  // Remove full string match value
    time[5] = +time[0] < 12 ? ' AM' : ' PM'; // Set AM/PM
    time[0] = +time[0] % 12 || 12; // Adjust hours
  }
  return time.join (''); // return adjusted time or original string
}
function getReportData(key) {
	var start_date = $("#start_date").val();
	var end_date = $("#end_date").val();
	$.post(ADMIN_URL + 'reports/reportdata', {
		start_date: start_date,
		end_date: end_date,
		key: key
	}, function(data) {
		if (data.message) {
			showAlertNotification(data);
		} else {
			clearAlertNotification();
			$(".appointmentCount").text((data.appointmentCount.total_appointments == null ? 0 : data.appointmentCount.total_appointments));
			$(".newPatientCount").text((data.newPatientCount.new_patients == null ? 0 : data.newPatientCount.new_patients));
			$(".purchasesCount").text((data.purchasesCount.total_purchase == null ? 0 : data.purchasesCount.total_purchase));
			$(".incomeCount").text(data.totalEarnings.earnings == null ? 0 : data.totalEarnings.earnings);
			$(".itemsIssuedCount").text((data.issuedItemsCount.total_items_issued == null ? 0 : data.issuedItemsCount.total_items_issued));
			$(".supplierPayable").text((data.supplierPayableCount.total_payable == null ? 0 : data.supplierPayableCount.total_payable));
			// $(".dueInvoices").text(data..);
			$(".expenseCount").text((data.expensesCount.total_expenses == null ? 0 : data.expensesCount.total_expenses));
			renderStaffAttendance(data.staffAttendance);
			renderExpenses(data.expenseDetails);
			renderPurchases(data.purchaseDetails);
			renderItemsIssued(data.issuedItemDetails);
		}
	}, "json");
}
function formatAMPM(date) {
	var date = new Date(date);
  var hours = date.getHours();
  var minutes = date.getMinutes();
  var ampm = hours >= 12 ? ' PM' : ' AM';
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? '0'+minutes : minutes;
  var strTime = hours + ':' + minutes + ' ' + ampm;
  return strTime;
}
function getDateFixed(date) {
	const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
	  "Jul", "Au", "Sep", "Oct", "Nov", "Dec"
	];
    var date = new Date(date);
    date.setDate(date.getDate()-1);
    return date.getDate() + '-' + monthNames[(date.getMonth())] + '-' + date.getFullYear();
}
function getYesterdaysDate() {
	const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
	  "Jul", "Au", "Sep", "Oct", "Nov", "Dec"
	];
    var date = new Date();
    date.setDate(date.getDate()-1);
    return date.getDate() + '-' + monthNames[(date.getMonth())] + '-' + date.getFullYear();
}
function getTodaysDate() {
	const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
	  "Jul", "Au", "Sep", "Oct", "Nov", "Dec"
	];
    var date = new Date();
    date.setDate(date.getDate());
    return date.getDate() + '-' + monthNames[(date.getMonth())] + '-' + date.getFullYear();
}
function getMonday( date ) {
	const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
	  "Jul", "Au", "Sep", "Oct", "Nov", "Dec"
	];
	var todayObj = new Date();
	var today = todayObj.getDate() + '-' + monthNames[(todayObj.getMonth())] + '-' + todayObj.getFullYear();
    var day = date.getDay() || 7;  
    if( day !== 1 ) 
        date.setHours(-24 * (day - 1)); 
    return date.getDate() + '-' + monthNames[(date.getMonth())] + '-' + date.getFullYear() + ' - '+ today;
}
function getFirstDayOfMonth() {
	const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
	  "Jul", "Au", "Sep", "Oct", "Nov", "Dec"
	];
	var todayObj = new Date();
	var today = todayObj.getDate() + '-' + monthNames[(todayObj.getMonth())] + '-' + todayObj.getFullYear();
	var date = new Date(new Date());
	return '1-' + monthNames[(date.getMonth())] + '-' + date.getFullYear() + ' - '+ today;
}
function getRangeDate() {
	const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
	  "Jul", "Au", "Sep", "Oct", "Nov", "Dec"
	];
	var start_date = $("#start_date").val();
	var end_date = $("#end_date").val();
	if (start_date == '' || end_date == '') {
		return;
	}
	var todayObj = new Date();
	var sdate = new Date(start_date);
	var edate = new Date(end_date);
	var ed = edate.getDate() + '-' + monthNames[(edate.getMonth())] + '-' + edate.getFullYear();
	var sd = sdate.getDate() + '-' + monthNames[(sdate.getMonth())] + '-' + sdate.getFullYear();
	return sd + ' - '+ ed;
}
$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();

  $("#start_date, #end_date").on('change', function(e) {
  	e.preventDefault();
	$(".reportDate").text(getRangeDate());
	var key = $(".reportBtn.active").data('key');
  	getReportData(key);
  });


  $(".reportBtn").on("click", function(e) {
  	e.preventDefault();
  	$(".reportBtn").removeClass("active");
  	$(this).addClass("active");
  	var key = $(this).data('key');
  	if (key == 'today') {
		$(".reportDate").text(getTodaysDate());
  	} else if (key == 'yesterday') {
		$(".reportDate").text(getYesterdaysDate());
  	} else if (key == 'week') {
		$(".reportDate").text(getMonday(new Date()));
  	} else if (key == 'month') {
		$(".reportDate").text(getFirstDayOfMonth());
  	} else if (key == 'range') {
  		if ($("#start_date").val() == "" || $("#end_date").val() == "") {
  			return false;
  		}
		$(".reportDate").text(getRangeDate());
  	}
  	getReportData(key);
  });

  $(".rangeReport").on("click", function(e) {
  	e.preventDefault();
  	$(".rangeReportFields").removeClass('hidden');
  });
  $(".todayReport, .yesterdayReport, .weekReport, .monthReport").on("click", function(e) {
  	e.preventDefault();
  	$(".rangeReportFields").addClass("hidden");
  });
  getReportData('today');
});