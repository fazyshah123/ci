$(document).ready(function(e) {
	$.each(doctorsList, function(i, v) {
		getDoctorTokens(v.id, v.full_name);
	});
	$(".markCompleted").unbind("click").bind("click", function(e) {
		e.preventDefault();
		var id = $(this).data('id');
		$.post(ADMIN_URL + 'reminders/markCompletedAJAX', {
			id: id
		}, function(data) {
			$(".reminderResponseText").text(data.alert);
	        $(".reminderResponseType").addClass(data.responseType);
	        $(".reminderResponseMessage").text(data.message);
	        $(".alertrow").removeClass('hidden');
	        $("#r-"+id).remove();
		}, "json");
	});
});
function getDoctorTokens(doctor_id, doctor_name) {
	var output = ''; 
	$.post(ADMIN_URL + 'tokens/getTokensByDoctorId', {
		doctor_id: doctor_id
	}, function(data) {
		output = '';
		if (data.length > 0) {
			output+= '<div class="col-md-4 dashboard_table" style="height: 350px !important;">\
				<h4 class="dashboard-widget-header"><strong>Tokens - '+doctor_name+'</strong></h4>\
				<table class="table table-hover table-striped table-condensed">\
					<thead>\
						<tr>\
							<th>Patient Name</th>\
							<th>Token</th>\
							<th>Actions</th>\
						</tr>\
					</thead>\
					<tbody>';
			$.each(data, function(i,v) {
				output+='<tr>\
					<td>'+v['patient_name']+'</td>\
					<td>'+v['t_number']+'</td>\
					<td width="150">\
						<a data-toggle="tooltip" title="PatientChecked" class="btn btn-xs tile-stats tile-cyan pull-left tokenChecked"  data-id="'+v["t_id"]+'" style="margin-right: 8px;min-height: 15px !important;padding: 3px !important;min-width: 15px !important;width: 30px;color: white;" href="javascript:;"><i class="fa fa-check"></i></a>\
						<a data-toggle="tooltip" title="Prescription" class="btn btn-xs tile-stats tile-new-greene pull-left addPrescription"  data-id="'+v["t_id"]+'" style="margin-right: 8px;min-height: 15px !important;padding: 3px !important;min-width: 15px !important;width: 30px;color: white;" href="javascript:;"><i class="fa fa-list"></i></a>\
						<a data-toggle="tooltip" title="Cancel Token" class="btn btn-xs tile-stats tile-red cancel_token_modal" data-id="'+v["t_id"]+'" style="min-height: 15px !important;padding: 3px !important;min-width: 15px !important;width: 30px;color: white;" href="javascript:;"><i class="fa fa-times"></i></a>\
					</td>\
				</tr>';
			});
			output += '</tbody></table></div>';
			$(".tokenTables").append(output);
			tokenTableEvents();
		}
	}, "json");
}