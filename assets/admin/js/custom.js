jQuery( document ).ready(function( $ ) {
	$("#search_status").on('change',function(){
    	var search_keywords = $("#search_keywords").val();
        var status = $("#search_status").val();
    	search_keywords = (search_keywords=="") ? "-" : encodeURI(search_keywords);
    	window.location = controller + "index/"+sortby+"/"+order+"/"+status+"/"+search_keywords;
    });
    $("#search_keywords").keypress(function(event) {
    	if (event.keyCode == ENTER_KEYCODE) {
    		var search_keywords = $("#search_keywords").val();
            var status = $("#search_status").val();
        	search_keywords = (search_keywords=="") ? "-" : encodeURI(search_keywords);
    		window.location = controller + 'index/' + sortby + '/' + order + '/' + status + '/' + search_keywords;
    		return false;
    	}
    });  

// Ctrl + a add shortcut script
	document.body.addEventListener("keydown", function (event) {
		if ((event.ctrlKey && event.keyCode === 97) || (event.ctrlKey && event.keyCode === 65)) {
			window.location = controller + 'control';
		}
		if ((event.ctrlKey && event.keyCode === 113) || (event.ctrlKey && event.keyCode === 83)) {
			window.location = controller + 'addRecord';
		}
	});

	$("#bedsubmit").on('click', function(e) {
        e.preventDefault();
        var b_bed_typeid = $("#b_bed_type_id").val();

        if (b_bed_typeid == 'Select') {
            alert('Please select a Bed Type');
            return;
        }

        $("#page_form").submit();

    });
	//For hiding the alert div
	$(".alertrow").on('click',function(){
		$(this).fadeOut(500,function(){
			$(this).remove();	
		});
	});
	$('.aplha').keyup(function () { 
     this.value = this.value.replace(/[^a-zA-Z0-9\- ]/g,'');
	});
	$('.uname').keyup(function () { 
     this.value = this.value.replace(/[^a-z0-9\-\.\_]/g,'');
	});
	
	$('.dec').keyup(function () { 
     this.value = this.value.replace(/[^0-9\.]/g,'');
	});
	$('.numb').keyup(function () { 
     this.value = this.value.replace(/[^0-9]/g,'');
	});
	$('.aplhasmall').keyup(function () { 
     this.value = this.value.replace(/[^a-z0-9\-]/g,'');
	});
	
	$("#page_name").on('keyup',function(){
		var input = this.value;
		input = input.replace(/^\s\s*/, '') // Trim start
        .replace(/\s\s*$/, '') // Trim end
        .toLowerCase() // Camel case is bad
        .replace(/[^a-z0-9_\-~\+\s]+/g, '') // Exchange invalid chars
        .replace(/[\s]+/g, '-'); // Swap whitespace for single hyphen
		
		$("#page_uri").val(input);
	});

	$("#openPatientFileUploadDialouge").on("click", function(e) {
		$("#uploadPatientFile").appendTo("body").modal('show');
	});
	
	
	//For launching the delete confirmation modal
	$(".delitem").on('click',function(){
		var recordID = this.id;
		var controller = $(this).attr('data-controller');
		recordID = recordID.replace("recordID","");
		var delLink = ADMIN_URL+controller+'/delete/'+recordID;
		$("#deleteLink").attr("onclick","window.location='"+delLink+"'");
		$('#deleteModal').modal('show');
	});
	
	$(".changestatus").on('click',function(){
		var recordID = this.id;
		var controller = $(this).attr('data-controller');
		
		if(controller=="pages" || controller=="trainings" || controller=="activities"  || controller=="events")
		{
			var status = ($(this).html() == "Published") ? "Un-Published" : "Published";
		}
		else{
			var status = ($(this).html() == "Enable") ? "Disable" : "Enable";
		}
		$("#modStatus").html(status);
		recordID = recordID.replace("statusID","");
		var delLink = ADMIN_URL+controller+'/changestatus/'+recordID+'/'+status;
		$("#statusLink").attr("data-status-url",delLink);
		$('#statusModal').modal('show');
	});
	var statusReq = null;
	$("#statusLink").on('click',function(){
		$('#statusModal').modal('hide');
		var statusLink = $(this).attr('data-status-url');
		if(statusReq!=null)
		{
			statusReq.abort();
		}
		statusReq = $.getJSON(statusLink,function(d){
			if(d.status=="true")
			{
				$("#statusID"+d.id).html(d.currentStatus);	
			}
		});
		
	});
	//Admin Settings Javascript START
	$("#adm_setting").validate({
		rules:{
			admin_name:{
				required:true
			},
			admin_email:{
				required:true,
				email: true
			},
			
			
			admin_current_pwd:{
				required: function(){
                        return $("#admin_current_pwd").val() != "";
                  },
				minlength:6,
				maxlength:20
			},
			
			admin_new_pwd:{
					required: function(){
                        return $("#admin_current_pwd").val() != "";
                  },
				minlength:6,
				maxlength:20
			},
			admin_new_pwd2:{
				required: function(){
                        return $("#admin_current_pwd").val() != "";
                  },
				minlength:6,
				maxlength:20,
				equalTo:"#admin_new_pwd"
			}
			
		},
		messages: {
			admin_new_pwd2: {
			equalTo: "Password didn't matched with new password"
			},
			
		},
		errorClass: "validate-has-error",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).closest('.form-group').addClass('validate-has-error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).closest('.form-group').removeClass('validate-has-error');
		}
	});
	
	//For select all checkbox
	$("#all-checkbox").on('click',function(){
		var checkedStatus = this.checked;
		var checkbox = $("#multiDel").find('tr td:first-child input:checkbox');		
		checkbox.each(function() {
			this.checked = checkedStatus;
		});
		
	});
	
	$("#deleteAllRecords").on('click',function(){
		var r = 0;
		$(".cselect").each(function() {
			if(this.checked==true)
			{
			r = 1;
			}
		});
			if(r==1)
			{
			$('#delAllModal').modal();	
			}
			else{
				alert("Please select one or more records to delete");	
			}
	});
	
	$("#delAllSubmit").on('click',function(){
		$("#multiDel").submit();	
	});
	$("#per_page").on('change',function(){
		
		window.location = controller+'?per_page='+this.value;	
	
	});
	
	//Add User Validation
	$("#admin_form").validate({
		rules:{
			full_name:{
				required:true
			},
			email:{
				required:true,
				email: true
			},
			user_name:{
				required: true,
				minlength:3,
				maxlength:20
			},
			pwd:{
				required: true,
				minlength:6,
				
			},
			pwd2:{
				required: true,
				minlength:6,
				
				equalTo:"#pwd"
			}
			
		},
		messages: {
			admin_new_pwd2: {
			equalTo: "Password didn't matched with new password"
			},
			
		},
		
		errorClass: "validate-has-error",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).closest('.form-group').addClass('validate-has-error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).closest('.form-group').removeClass('validate-has-error');
		}
	});
	
	CKEDITOR.replace( 'appointment_comment' );
	
});//End of document ready

$.fn.hasExtension = function(exts) {
    return (new RegExp('(' + exts.join('|').replace(/\./g, '\\.') + ')$')).test($(this).val());
}
function validateURL(value) {
    return /^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(value);
}